/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.CustomComponent;

public abstract class AbstractComplianceDetailsPanel extends CustomComponent {

	private static final long serialVersionUID = 4944830488705411462L;
	

	protected void setChildrenAllowed (HierarchicalContainer complianceDetailsContainer) {
		for (Object itemId : complianceDetailsContainer.getItemIds()) {
			if (complianceDetailsContainer.hasChildren(itemId)) {
				complianceDetailsContainer.setChildrenAllowed(itemId, true);
			} else {
				complianceDetailsContainer.setChildrenAllowed(itemId, false);
			}
		}
	}
	
	
	
	
		
}
