/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.util;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.net4j.CDONet4jUtil;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.net4j.Net4jUtil;
import org.eclipse.net4j.tcp.TCPUtil;
import org.eclipse.net4j.util.container.IPluginContainer;
import org.eclipse.net4j.util.om.OMPlatform;

public class CdoConnection {
	private CDOSession currentSession;
	private final String repositoryName;
	private final String protocol;
	private final String host;
	private Map<String, CDOTransaction> transactions;
	private Map<String, CDOView> views;

	public CdoConnection() {
		if (!OMPlatform.INSTANCE.isOSGiRunning()) {
			Net4jUtil.prepareContainer(IPluginContainer.INSTANCE);
			TCPUtil.prepareContainer(IPluginContainer.INSTANCE);
			CDONet4jUtil.prepareContainer(IPluginContainer.INSTANCE);
		}

		repositoryName = PreferenceConstants.getRepositoryName();
		protocol = PreferenceConstants.getProtocol();
		host = PreferenceConstants.getServerName();

		views = new HashMap<String, CDOView>();
		transactions = new HashMap<String, CDOTransaction>();
	}

	/**
	 * Gets the current session or opens a new one.
	 * 
	 * @return
	 */
	public CDOSession getSession() {
		if (currentSession == null || currentSession.isClosed()) {
			currentSession = openSession();
		}

		return currentSession;
	}

	private CDOSession openSession() {
		currentSession = (CDOSession) IPluginContainer.INSTANCE.getElement(
				"org.eclipse.emf.cdo.sessions", "cdo", protocol + "://" + host
						+ "?repositoryName=" + repositoryName);

		return currentSession;
	}

	/**
	 * Gets the transaction with the specified id or creates a new transaction
	 * on a new EMF resource set.
	 * 
	 * @param id
	 * @return
	 */
	public CDOTransaction getTransaction(String id) {
		if (id == null) {
			return null;
		}

		CDOTransaction result = transactions.get(id);
		if (result == null || result.isClosed()) {
			transactions.remove(id);
			result = null;
		}

		if (result == null) {
			CDOSession session = getSession();
			result = session.openTransaction();
			transactions.put(id, result);
		}

		return result;
	}

	/**
	 * Gets the view with the specified id or creates a new transaction on a new
	 * EMF resource set.
	 * 
	 * @param id
	 * @return
	 */
	public CDOView getView(String id) {
		if (id == null) {
			return null;
		}

		CDOView result = views.get(id);
		if (result == null || result.isClosed()) {
			views.remove(id);
			result = null;
		}

		if (result == null) {
			CDOSession session = getSession();
			result = session.openView();
			views.put(id, result);
		}
		
		return result;
	}

	public void dispose() {
		for (CDOView view : views.values()) {
			if (view != null && !view.isClosed()) {
				view.close();
			}
		}
		views.clear();

		for (CDOTransaction transaction : transactions.values()) {
			if (transaction != null && !transaction.isClosed()) {
				transaction.close();
			}
		}
		transactions.clear();

		// This messes with sessions opened somewhere else.
		// if (currentSession != null && !currentSession.isClosed()) {
		// currentSession.close();
		// }
		// currentSession = null;
	}
}
