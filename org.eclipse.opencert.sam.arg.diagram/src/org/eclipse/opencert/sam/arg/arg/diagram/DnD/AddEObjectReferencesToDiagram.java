/*
 * <copyright>
 * Copyright (c) 2013 TECNALIA Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the term conditions of the Eclipse Public Licen v1.0
 * which accompanish this distribution, and is available at
  * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *  
 * Contributors 
 *  Alejandra Ruiz (TECNALIA): meta model design, development and implementation
 *  M� Carmen Palacios (TECNALIA): Development, implementation and maintenance
 * </copyright>
 * 
 * 
 */
/*******************************************************************************
 * Copyright (c) 2008 Conselleria de Infraestructuras y Transporte,
 * Generalitat de la Comunitat Valenciana .
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors: Francisco Javier Cano Muñoz (Prodevelop) - initial API implementation
 *
 ******************************************************************************/

package org.eclipse.opencert.sam.arg.arg.diagram.DnD;


import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;


/**
 * @generated NOT
 */

/**
 * The Class AddEObjectReferencesToDiagram.
 * 
 * @author <a href="mailto:fjcano@prodevelop.es">Francisco Javier Cano Mu�oz</a>
 */
public class AddEObjectReferencesToDiagram extends AbstractTransactionalCommand {
	/** EAnnotation Source for elements that belong to this <Diagram> */
	public static final String BelongToDiagramSource = "es.cv.gvcase.mdt.uml2.diagram.common.Belongs_To_This_Diagram";
	
	// Start MCP
    private DiagramImpl diagramImpl = null;
	// End MCP
	
	/** The diagram. */
	private Diagram diagram = null;

	/** The e objects. */
	private List<EObject> eObjects = null;

	/**
	 * Flag to add the EObject's contents.
	 */
	protected boolean addContents = true;

	/**
	 * Instantiates a new adds the e object references to diagram.
	 * 
	 * @param domain
	 *        the domain
	 * @param diagram
	 *        the diagram
	 * @param eObjects
	 *        the e objects
	 */
	public AddEObjectReferencesToDiagram(TransactionalEditingDomain domain, Diagram diagram, DiagramImpl diagramImpl, List<EObject> eObjects) {
		super(domain, "Add EObject references to Diagram", null);
		this.diagram = diagram;
		this.eObjects = eObjects;
		// Start MCP
		this.diagramImpl = diagramImpl;
		// End MCP
	}

	// @unused
	public AddEObjectReferencesToDiagram(TransactionalEditingDomain domain, Diagram diagram, DiagramImpl diagramImpl, List<EObject> eObjects, boolean addContents) {
		super(domain, "Add EObject references to Diagram", null);
		this.diagram = diagram;
		this.eObjects = eObjects;
		this.addContents = addContents;
		// Start MCP
		this.diagramImpl = diagramImpl;
		// End MCP
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.operations.AbstractOperation#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return diagram != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gmf.runtime.emf.commands.core.command. AbstractTransactionalCommand
	 * #doExecuteWithResult(org.eclipse.core.runtime.IProgressMonitor,
	 * org.eclipse.core.runtime.IAdaptable)
	 */
	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		for(EObject eObject : eObjects) {
			addReferences(eObject);

			// Start MCP
			Case cs = (Case) diagramImpl.basicGetElement();
			if(eObject instanceof ArgumentElement)
				cs.getArgument().add((ArgumentElement)eObject);
			else if(eObject instanceof Argumentation)
			    cs.getArgumentation().add((Argumentation)eObject);
			else if(eObject instanceof Agreement)			
			  cs.getAgreement().add((Agreement)eObject);
			else if(eObject instanceof ArgumentElementCitation)			
			  cs.getCited().add((ArgumentElementCitation) eObject);
			else if(eObject instanceof InformationElementCitation)			
			  cs.getInformation().add((InformationElementCitation)eObject);
			// End MCP
		}
		return CommandResult.newOKCommandResult();
	}

	/**
	 * Adds the references.
	 * 
	 * @param eObject
	 *        the e object
	 */
	private void addReferences(EObject eObject) {
		if(addContents) {
			for(EObject e : eObject.eContents()) {
				addReferences(e);
			}
		}
		//MultiDiagramUtil.AddEAnnotationReferenceToDiagram(diagram, eObject);
		AddEAnnotationReferenceToDiagram(diagram, eObject);
	}
	
	/**
	 * Adds the e annotation reference to diagram.
	 * 
	 * @param diagram
	 *            the diagram
	 * @param eObject
	 *            the e object
	 * 
	 * @return true, if successful
	 */
	public static boolean AddEAnnotationReferenceToDiagram(Diagram diagram,
			EObject eObject) {

		if (diagram != null) {
			EAnnotation eAnnotation = diagram.getEAnnotation(BelongToDiagramSource);
			if (eAnnotation == null) {
				eAnnotation = EcoreFactory.eINSTANCE.createEAnnotation();
				eAnnotation.setSource(BelongToDiagramSource);
				diagram.getEAnnotations().add(eAnnotation);
			}
			// if (eAnnotation.getReferences().contains(eObject) == false) {
			eAnnotation.getReferences().add(eObject);
			// }
			return true;
		}
		return false;
	}


}
