/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.views.properties.tabbed.ISection;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;
import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;
import org.eclipse.opencert.pkm.refframework.refframework.utils.MappingSet;

// End of user code

/**
 * 
 * 
 */
public class RefFrameworkPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, RefFrameworkPropertiesEditionPart {

	
	private static final String ClassDiagram = "org.eclipse.opencert.pkm.refframework.refframework.diagram.part.DawnRefframeworkDiagramEditor";
	
	protected Text id;
	protected Text name;
	protected Text description;
	protected Text scope;
	protected Text rev;
	protected Text purpose;
	protected Text publisher;
	protected Text issued;
	protected ReferencesTable ownedActivities;
	protected List<ViewerFilter> ownedActivitiesBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedActivitiesFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedArtefact;
	protected List<ViewerFilter> ownedArtefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedArtefactFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedRequirement;
	protected List<ViewerFilter> ownedRequirementBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRequirementFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedApplicLevel;
	protected List<ViewerFilter> ownedApplicLevelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedApplicLevelFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedCriticLevel;
	protected List<ViewerFilter> ownedCriticLevelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedCriticLevelFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedRole;
	protected List<ViewerFilter> ownedRoleBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRoleFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedTechnique;
	protected List<ViewerFilter> ownedTechniqueBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedTechniqueFilters = new ArrayList<ViewerFilter>();


	
	
	// Start IRR
	
	public static String ID = "org.eclipse.opencert.pkm.refframework.presentation.DawnRefframeworkEditorID";
	
	protected Button buttonLR;
	protected Button buttonEM;

	// End IRR
	
	

	/**
	 * For {@link ISection} use only.
	 */
	public RefFrameworkPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public RefFrameworkPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence refFramework_Step = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = refFramework_Step.addStep(RefframeworkViewsRepository.RefFramework_.Properties.class);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.id);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.name);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.description);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.scope);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.rev);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.purpose);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.publisher);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.issued);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.ownedRole);
		propertiesStep.addStep(RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique);
		
		
		composer = new PartComposer(refFramework_Step) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.class) {
					
					
					// Start IRR
					
					IEditorPart editor;
					editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage().getActiveEditor();

					if (!(editor.getClass().getName().contentEquals(ClassDiagram))) {
						createGroupButton(parent);
					}
				
					// End IRR
					
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.description) {
					return createDescriptionTextarea(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.scope) {
					return createScopeTextarea(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.rev) {
					return createRevTextarea(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.purpose) {
					return createPurposeText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.publisher) {
					return createPublisherText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.issued) {
					return createIssuedText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities) {
					return createOwnedActivitiesTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact) {
					return createOwnedArtefactTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement) {
					return createOwnedRequirementTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel) {
					return createOwnedApplicLevelTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel) {
					return createOwnedCriticLevelTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.ownedRole) {
					return createOwnedRoleTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique) {
					return createOwnedTechniqueTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(RefframeworkMessages.RefFrameworkPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefFramework_.Properties.id, RefframeworkMessages.RefFrameworkPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefFrameworkPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefFramework_.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefFramework_.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, RefframeworkViewsRepository.RefFramework_.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.id, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefFramework_.Properties.name, RefframeworkMessages.RefFrameworkPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefFrameworkPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefFramework_.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefFramework_.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, RefframeworkViewsRepository.RefFramework_.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.name, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionTextarea(FormToolkit widgetFactory, Composite parent) {
		Label descriptionLabel = createDescription(parent, RefframeworkViewsRepository.RefFramework_.Properties.description, RefframeworkMessages.RefFrameworkPropertiesEditionPart_DescriptionLabel);
		GridData descriptionLabelData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionLabelData.horizontalSpan = 3;
		descriptionLabel.setLayoutData(descriptionLabelData);
		description = widgetFactory.createText(parent, "", SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL); //$NON-NLS-1$
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionData.horizontalSpan = 2;
		descriptionData.heightHint = 80;
		descriptionData.widthHint = 200;
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefFrameworkPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefFramework_.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefFramework_.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		EditingUtils.setID(description, RefframeworkViewsRepository.RefFramework_.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Textarea"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.description, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createScopeTextarea(FormToolkit widgetFactory, Composite parent) {
		Label scopeLabel = createDescription(parent, RefframeworkViewsRepository.RefFramework_.Properties.scope, RefframeworkMessages.RefFrameworkPropertiesEditionPart_ScopeLabel);
		GridData scopeLabelData = new GridData(GridData.FILL_HORIZONTAL);
		scopeLabelData.horizontalSpan = 3;
		scopeLabel.setLayoutData(scopeLabelData);
		scope = widgetFactory.createText(parent, "", SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL); //$NON-NLS-1$
		GridData scopeData = new GridData(GridData.FILL_HORIZONTAL);
		scopeData.horizontalSpan = 2;
		scopeData.heightHint = 80;
		scopeData.widthHint = 200;
		scope.setLayoutData(scopeData);
		scope.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefFrameworkPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefFramework_.Properties.scope,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, scope.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefFramework_.Properties.scope,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, scope.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		EditingUtils.setID(scope, RefframeworkViewsRepository.RefFramework_.Properties.scope);
		EditingUtils.setEEFtype(scope, "eef::Textarea"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.scope, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createScopeTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createRevTextarea(FormToolkit widgetFactory, Composite parent) {
		Label revLabel = createDescription(parent, RefframeworkViewsRepository.RefFramework_.Properties.rev, RefframeworkMessages.RefFrameworkPropertiesEditionPart_RevLabel);
		GridData revLabelData = new GridData(GridData.FILL_HORIZONTAL);
		revLabelData.horizontalSpan = 3;
		revLabel.setLayoutData(revLabelData);
		rev = widgetFactory.createText(parent, "", SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL); //$NON-NLS-1$
		GridData revData = new GridData(GridData.FILL_HORIZONTAL);
		revData.horizontalSpan = 2;
		revData.heightHint = 80;
		revData.widthHint = 200;
		rev.setLayoutData(revData);
		rev.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefFrameworkPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefFramework_.Properties.rev,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, rev.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefFramework_.Properties.rev,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, rev.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		EditingUtils.setID(rev, RefframeworkViewsRepository.RefFramework_.Properties.rev);
		EditingUtils.setEEFtype(rev, "eef::Textarea"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.rev, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createRevTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createPurposeText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefFramework_.Properties.purpose, RefframeworkMessages.RefFrameworkPropertiesEditionPart_PurposeLabel);
		purpose = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		purpose.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData purposeData = new GridData(GridData.FILL_HORIZONTAL);
		purpose.setLayoutData(purposeData);
		purpose.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefFrameworkPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefFramework_.Properties.purpose,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, purpose.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefFramework_.Properties.purpose,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, purpose.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		purpose.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.purpose, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, purpose.getText()));
				}
			}
		});
		EditingUtils.setID(purpose, RefframeworkViewsRepository.RefFramework_.Properties.purpose);
		EditingUtils.setEEFtype(purpose, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.purpose, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createPurposeText

		// End of user code
		return parent;
	}

	
	protected Composite createPublisherText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefFramework_.Properties.publisher, RefframeworkMessages.RefFrameworkPropertiesEditionPart_PublisherLabel);
		publisher = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		publisher.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData publisherData = new GridData(GridData.FILL_HORIZONTAL);
		publisher.setLayoutData(publisherData);
		publisher.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefFrameworkPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefFramework_.Properties.publisher,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, publisher.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefFramework_.Properties.publisher,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, publisher.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		publisher.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.publisher, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, publisher.getText()));
				}
			}
		});
		EditingUtils.setID(publisher, RefframeworkViewsRepository.RefFramework_.Properties.publisher);
		EditingUtils.setEEFtype(publisher, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.publisher, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createPublisherText

		// End of user code
		return parent;
	}

	
	protected Composite createIssuedText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefFramework_.Properties.issued, RefframeworkMessages.RefFrameworkPropertiesEditionPart_IssuedLabel);
		issued = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		issued.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData issuedData = new GridData(GridData.FILL_HORIZONTAL);
		issued.setLayoutData(issuedData);
		issued.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefFrameworkPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefFramework_.Properties.issued,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, issued.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefFramework_.Properties.issued,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, issued.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefFrameworkPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		issued.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.issued, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, issued.getText()));
				}
			}
		});
		EditingUtils.setID(issued, RefframeworkViewsRepository.RefFramework_.Properties.issued);
		EditingUtils.setEEFtype(issued, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.issued, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIssuedText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedActivitiesTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedActivities = new ReferencesTable(getDescription(RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities, RefframeworkMessages.RefFrameworkPropertiesEditionPart_OwnedActivitiesLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedActivities.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedActivities.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedActivities.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedActivities.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedActivitiesFilters) {
			this.ownedActivities.addFilter(filter);
		}
		this.ownedActivities.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities, RefframeworkViewsRepository.FORM_KIND));
		this.ownedActivities.createControls(parent, widgetFactory);
		this.ownedActivities.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedActivitiesData = new GridData(GridData.FILL_HORIZONTAL);
		ownedActivitiesData.horizontalSpan = 3;
		this.ownedActivities.setLayoutData(ownedActivitiesData);
		this.ownedActivities.setLowerBound(0);
		this.ownedActivities.setUpperBound(-1);
		ownedActivities.setID(RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities);
		ownedActivities.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedActivitiesTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedArtefactTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedArtefact = new ReferencesTable(getDescription(RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact, RefframeworkMessages.RefFrameworkPropertiesEditionPart_OwnedArtefactLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedArtefact.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedArtefact.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedArtefact.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedArtefact.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedArtefactFilters) {
			this.ownedArtefact.addFilter(filter);
		}
		this.ownedArtefact.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact, RefframeworkViewsRepository.FORM_KIND));
		this.ownedArtefact.createControls(parent, widgetFactory);
		this.ownedArtefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedArtefactData = new GridData(GridData.FILL_HORIZONTAL);
		ownedArtefactData.horizontalSpan = 3;
		this.ownedArtefact.setLayoutData(ownedArtefactData);
		this.ownedArtefact.setLowerBound(0);
		this.ownedArtefact.setUpperBound(-1);
		ownedArtefact.setID(RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact);
		ownedArtefact.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedArtefactTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRequirementTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedRequirement = new ReferencesTable(getDescription(RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement, RefframeworkMessages.RefFrameworkPropertiesEditionPart_OwnedRequirementLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRequirement.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRequirement.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRequirement.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRequirement.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRequirementFilters) {
			this.ownedRequirement.addFilter(filter);
		}
		this.ownedRequirement.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement, RefframeworkViewsRepository.FORM_KIND));
		this.ownedRequirement.createControls(parent, widgetFactory);
		this.ownedRequirement.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRequirementData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRequirementData.horizontalSpan = 3;
		this.ownedRequirement.setLayoutData(ownedRequirementData);
		this.ownedRequirement.setLowerBound(0);
		this.ownedRequirement.setUpperBound(-1);
		ownedRequirement.setID(RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement);
		ownedRequirement.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRequirementTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedApplicLevelTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedApplicLevel = new ReferencesTable(getDescription(RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel, RefframeworkMessages.RefFrameworkPropertiesEditionPart_OwnedApplicLevelLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedApplicLevel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedApplicLevel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedApplicLevel.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedApplicLevel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedApplicLevelFilters) {
			this.ownedApplicLevel.addFilter(filter);
		}
		this.ownedApplicLevel.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel, RefframeworkViewsRepository.FORM_KIND));
		this.ownedApplicLevel.createControls(parent, widgetFactory);
		this.ownedApplicLevel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedApplicLevelData = new GridData(GridData.FILL_HORIZONTAL);
		ownedApplicLevelData.horizontalSpan = 3;
		this.ownedApplicLevel.setLayoutData(ownedApplicLevelData);
		this.ownedApplicLevel.setLowerBound(0);
		this.ownedApplicLevel.setUpperBound(-1);
		ownedApplicLevel.setID(RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel);
		ownedApplicLevel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedApplicLevelTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedCriticLevelTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedCriticLevel = new ReferencesTable(getDescription(RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel, RefframeworkMessages.RefFrameworkPropertiesEditionPart_OwnedCriticLevelLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedCriticLevel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedCriticLevel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedCriticLevel.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedCriticLevel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedCriticLevelFilters) {
			this.ownedCriticLevel.addFilter(filter);
		}
		this.ownedCriticLevel.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel, RefframeworkViewsRepository.FORM_KIND));
		this.ownedCriticLevel.createControls(parent, widgetFactory);
		this.ownedCriticLevel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedCriticLevelData = new GridData(GridData.FILL_HORIZONTAL);
		ownedCriticLevelData.horizontalSpan = 3;
		this.ownedCriticLevel.setLayoutData(ownedCriticLevelData);
		this.ownedCriticLevel.setLowerBound(0);
		this.ownedCriticLevel.setUpperBound(-1);
		ownedCriticLevel.setID(RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel);
		ownedCriticLevel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedCriticLevelTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRoleTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedRole = new ReferencesTable(getDescription(RefframeworkViewsRepository.RefFramework_.Properties.ownedRole, RefframeworkMessages.RefFrameworkPropertiesEditionPart_OwnedRoleLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedRole, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRole.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedRole, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRole.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedRole, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRole.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedRole, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRole.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRoleFilters) {
			this.ownedRole.addFilter(filter);
		}
		this.ownedRole.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.ownedRole, RefframeworkViewsRepository.FORM_KIND));
		this.ownedRole.createControls(parent, widgetFactory);
		this.ownedRole.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedRole, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRoleData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRoleData.horizontalSpan = 3;
		this.ownedRole.setLayoutData(ownedRoleData);
		this.ownedRole.setLowerBound(0);
		this.ownedRole.setUpperBound(-1);
		ownedRole.setID(RefframeworkViewsRepository.RefFramework_.Properties.ownedRole);
		ownedRole.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRoleTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedTechniqueTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedTechnique = new ReferencesTable(getDescription(RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique, RefframeworkMessages.RefFrameworkPropertiesEditionPart_OwnedTechniqueLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedTechnique.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedTechnique.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedTechnique.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedTechnique.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedTechniqueFilters) {
			this.ownedTechnique.addFilter(filter);
		}
		this.ownedTechnique.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique, RefframeworkViewsRepository.FORM_KIND));
		this.ownedTechnique.createControls(parent, widgetFactory);
		this.ownedTechnique.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefFrameworkPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedTechniqueData = new GridData(GridData.FILL_HORIZONTAL);
		ownedTechniqueData.horizontalSpan = 3;
		this.ownedTechnique.setLayoutData(ownedTechniqueData);
		this.ownedTechnique.setLowerBound(0);
		this.ownedTechnique.setUpperBound(-1);
		ownedTechnique.setID(RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique);
		ownedTechnique.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedTechniqueTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setBackground(description.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			description.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#getScope()
	 * 
	 */
	public String getScope() {
		return scope.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#setScope(String newValue)
	 * 
	 */
	public void setScope(String newValue) {
		if (newValue != null) {
			scope.setText(newValue);
		} else {
			scope.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.scope);
		if (eefElementEditorReadOnlyState && scope.isEnabled()) {
			scope.setEnabled(false);
			scope.setBackground(scope.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			scope.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !scope.isEnabled()) {
			scope.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#getRev()
	 * 
	 */
	public String getRev() {
		return rev.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#setRev(String newValue)
	 * 
	 */
	public void setRev(String newValue) {
		if (newValue != null) {
			rev.setText(newValue);
		} else {
			rev.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.rev);
		if (eefElementEditorReadOnlyState && rev.isEnabled()) {
			rev.setEnabled(false);
			rev.setBackground(rev.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			rev.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !rev.isEnabled()) {
			rev.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#getPurpose()
	 * 
	 */
	public String getPurpose() {
		return purpose.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#setPurpose(String newValue)
	 * 
	 */
	public void setPurpose(String newValue) {
		if (newValue != null) {
			purpose.setText(newValue);
		} else {
			purpose.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.purpose);
		if (eefElementEditorReadOnlyState && purpose.isEnabled()) {
			purpose.setEnabled(false);
			purpose.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !purpose.isEnabled()) {
			purpose.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#getPublisher()
	 * 
	 */
	public String getPublisher() {
		return publisher.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#setPublisher(String newValue)
	 * 
	 */
	public void setPublisher(String newValue) {
		if (newValue != null) {
			publisher.setText(newValue);
		} else {
			publisher.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.publisher);
		if (eefElementEditorReadOnlyState && publisher.isEnabled()) {
			publisher.setEnabled(false);
			publisher.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !publisher.isEnabled()) {
			publisher.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#getIssued()
	 * 
	 */
	public String getIssued() {
		return issued.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#setIssued(String newValue)
	 * 
	 */
	public void setIssued(String newValue) {
		if (newValue != null) {
			issued.setText(newValue);
		} else {
			issued.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.issued);
		if (eefElementEditorReadOnlyState && issued.isEnabled()) {
			issued.setEnabled(false);
			issued.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !issued.isEnabled()) {
			issued.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#initOwnedActivities(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedActivities(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedActivities.setContentProvider(contentProvider);
		ownedActivities.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities);
		if (eefElementEditorReadOnlyState && ownedActivities.isEnabled()) {
			ownedActivities.setEnabled(false);
			ownedActivities.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedActivities.isEnabled()) {
			ownedActivities.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#updateOwnedActivities()
	 * 
	 */
	public void updateOwnedActivities() {
	ownedActivities.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addFilterOwnedActivities(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedActivities(ViewerFilter filter) {
		ownedActivitiesFilters.add(filter);
		if (this.ownedActivities != null) {
			this.ownedActivities.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addBusinessFilterOwnedActivities(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedActivities(ViewerFilter filter) {
		ownedActivitiesBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#isContainedInOwnedActivitiesTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedActivitiesTable(EObject element) {
		return ((ReferencesTableSettings)ownedActivities.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#initOwnedArtefact(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedArtefact.setContentProvider(contentProvider);
		ownedArtefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact);
		if (eefElementEditorReadOnlyState && ownedArtefact.isEnabled()) {
			ownedArtefact.setEnabled(false);
			ownedArtefact.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedArtefact.isEnabled()) {
			ownedArtefact.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#updateOwnedArtefact()
	 * 
	 */
	public void updateOwnedArtefact() {
	ownedArtefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addFilterOwnedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedArtefact(ViewerFilter filter) {
		ownedArtefactFilters.add(filter);
		if (this.ownedArtefact != null) {
			this.ownedArtefact.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addBusinessFilterOwnedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedArtefact(ViewerFilter filter) {
		ownedArtefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#isContainedInOwnedArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedArtefactTable(EObject element) {
		return ((ReferencesTableSettings)ownedArtefact.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#initOwnedRequirement(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRequirement(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRequirement.setContentProvider(contentProvider);
		ownedRequirement.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement);
		if (eefElementEditorReadOnlyState && ownedRequirement.isEnabled()) {
			ownedRequirement.setEnabled(false);
			ownedRequirement.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRequirement.isEnabled()) {
			ownedRequirement.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#updateOwnedRequirement()
	 * 
	 */
	public void updateOwnedRequirement() {
	ownedRequirement.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addFilterOwnedRequirement(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRequirement(ViewerFilter filter) {
		ownedRequirementFilters.add(filter);
		if (this.ownedRequirement != null) {
			this.ownedRequirement.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addBusinessFilterOwnedRequirement(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRequirement(ViewerFilter filter) {
		ownedRequirementBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#isContainedInOwnedRequirementTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRequirementTable(EObject element) {
		return ((ReferencesTableSettings)ownedRequirement.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#initOwnedApplicLevel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedApplicLevel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedApplicLevel.setContentProvider(contentProvider);
		ownedApplicLevel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel);
		if (eefElementEditorReadOnlyState && ownedApplicLevel.isEnabled()) {
			ownedApplicLevel.setEnabled(false);
			ownedApplicLevel.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedApplicLevel.isEnabled()) {
			ownedApplicLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#updateOwnedApplicLevel()
	 * 
	 */
	public void updateOwnedApplicLevel() {
	ownedApplicLevel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addFilterOwnedApplicLevel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedApplicLevel(ViewerFilter filter) {
		ownedApplicLevelFilters.add(filter);
		if (this.ownedApplicLevel != null) {
			this.ownedApplicLevel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addBusinessFilterOwnedApplicLevel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedApplicLevel(ViewerFilter filter) {
		ownedApplicLevelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#isContainedInOwnedApplicLevelTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedApplicLevelTable(EObject element) {
		return ((ReferencesTableSettings)ownedApplicLevel.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#initOwnedCriticLevel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedCriticLevel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedCriticLevel.setContentProvider(contentProvider);
		ownedCriticLevel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel);
		if (eefElementEditorReadOnlyState && ownedCriticLevel.isEnabled()) {
			ownedCriticLevel.setEnabled(false);
			ownedCriticLevel.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedCriticLevel.isEnabled()) {
			ownedCriticLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#updateOwnedCriticLevel()
	 * 
	 */
	public void updateOwnedCriticLevel() {
	ownedCriticLevel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addFilterOwnedCriticLevel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedCriticLevel(ViewerFilter filter) {
		ownedCriticLevelFilters.add(filter);
		if (this.ownedCriticLevel != null) {
			this.ownedCriticLevel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addBusinessFilterOwnedCriticLevel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedCriticLevel(ViewerFilter filter) {
		ownedCriticLevelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#isContainedInOwnedCriticLevelTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedCriticLevelTable(EObject element) {
		return ((ReferencesTableSettings)ownedCriticLevel.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#initOwnedRole(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRole(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRole.setContentProvider(contentProvider);
		ownedRole.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.ownedRole);
		if (eefElementEditorReadOnlyState && ownedRole.isEnabled()) {
			ownedRole.setEnabled(false);
			ownedRole.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRole.isEnabled()) {
			ownedRole.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#updateOwnedRole()
	 * 
	 */
	public void updateOwnedRole() {
	ownedRole.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addFilterOwnedRole(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRole(ViewerFilter filter) {
		ownedRoleFilters.add(filter);
		if (this.ownedRole != null) {
			this.ownedRole.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addBusinessFilterOwnedRole(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRole(ViewerFilter filter) {
		ownedRoleBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#isContainedInOwnedRoleTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRoleTable(EObject element) {
		return ((ReferencesTableSettings)ownedRole.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#initOwnedTechnique(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedTechnique(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedTechnique.setContentProvider(contentProvider);
		ownedTechnique.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique);
		if (eefElementEditorReadOnlyState && ownedTechnique.isEnabled()) {
			ownedTechnique.setEnabled(false);
			ownedTechnique.setToolTipText(RefframeworkMessages.RefFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedTechnique.isEnabled()) {
			ownedTechnique.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#updateOwnedTechnique()
	 * 
	 */
	public void updateOwnedTechnique() {
	ownedTechnique.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addFilterOwnedTechnique(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedTechnique(ViewerFilter filter) {
		ownedTechniqueFilters.add(filter);
		if (this.ownedTechnique != null) {
			this.ownedTechnique.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#addBusinessFilterOwnedTechnique(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedTechnique(ViewerFilter filter) {
		ownedTechniqueBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart#isContainedInOwnedTechniqueTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedTechniqueTable(EObject element) {
		return ((ReferencesTableSettings)ownedTechnique.getInput()).contains(element);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.RefFramework_Part_Title;
	}

	// Start of user code additional methods
	// Start IRR
	protected void createGroupButton(final Composite parent) {
		Composite controlButtons = new Composite(parent, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			controlButtons.setLayoutData(data);
			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1;

			controlButtons.setLayout(layout);
		}

		Group group = new Group(controlButtons, SWT.NULL);
		group.setLayout(new GridLayout(2, false));
		group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		group.setText("Equivalence Map Group");

		
		buttonEM = new Button(group, 0);
		GridData EMGridData = new GridData();
		EMGridData.horizontalAlignment = SWT.FILL;
		EMGridData.verticalAlignment = SWT.FILL;
		
		IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IWorkbenchPage page = workbenchWindow.getActivePage();
		
		IEditorPart editorPart = page.getActiveEditor();
		
		
		URI resourceURI = EditUIUtil.getURI(editorPart.getEditorInput());
		CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		CDOTransaction transaction = sessionCDO.openTransaction();
		CDOResourceNode node=transaction.getResourceNode(resourceURI.path());
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		
		buttonEM.setEnabled(permission.isWritable());		
		
		buttonEM.setLayoutData(EMGridData);
		buttonEM.setText("Mapping Set");
		buttonEM.addSelectionListener(new SelectionAdapter() {
						
			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent e) {

				// Pregunta antes de salvar si es que se han hecho cambios
				PlatformUI.getWorkbench().saveAllEditors(true);

				
				ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
						ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
				BasicCommandStack commandStack = new BasicCommandStack();
				EditingDomain editingDomain = new AdapterFactoryEditingDomain(adapterFactory, commandStack, new HashMap<Resource, Boolean>());
				
				editingDomain.getResourceSet();
				
				
				MappingSet EquivalenceMappingSet = new MappingSet(parent.getShell());

				if (EquivalenceMappingSet.open() == Window.OK) {
					
					// Reload editor to show the changes
					
					IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
					IWorkbenchPage page = workbenchWindow.getActivePage();
					
					IEditorPart editorPart = page.getActiveEditor();
					
					
					URI resourceURI = EditUIUtil.getURI(editorPart.getEditorInput());
					System.out.println(resourceURI.toString());
					/*
					page.closeEditor(editorPart, true);
					
					
					//Open editor
					
					DawnEditorInput dawnEditorInput = new DawnEditorInput(resourceURI);
					try {
						editorPart = page.openEditor(dawnEditorInput, ID);
						
						
					} catch (PartInitException exception) {
						MessageDialog.openError(workbenchWindow.getShell(),
								"Error to reopen the editor", exception.getMessage());
						throw new RuntimeException(exception);
					}
					*/
										
				}
			}
		});
		
		
		
	}
	// End IRR
	
	// End of user code


}
