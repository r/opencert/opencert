/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.evm.evidspec.evidence.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Artefact Rel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactRelImpl#getModificationEffect <em>Modification Effect</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactRelImpl#getRevocationEffect <em>Revocation Effect</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactRelImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactRelImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArtefactRelImpl extends DescribableElementImpl implements ArtefactRel {
	/**
	 * The default value of the '{@link #getModificationEffect() <em>Modification Effect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModificationEffect()
	 * @generated
	 * @ordered
	 */
	protected static final ChangeEffectKind MODIFICATION_EFFECT_EDEFAULT = ChangeEffectKind.NONE;

	/**
	 * The default value of the '{@link #getRevocationEffect() <em>Revocation Effect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRevocationEffect()
	 * @generated
	 * @ordered
	 */
	protected static final ChangeEffectKind REVOCATION_EFFECT_EDEFAULT = ChangeEffectKind.NONE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArtefactRelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EvidencePackage.Literals.ARTEFACT_REL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEffectKind getModificationEffect() {
		return (ChangeEffectKind)eDynamicGet(EvidencePackage.ARTEFACT_REL__MODIFICATION_EFFECT, EvidencePackage.Literals.ARTEFACT_REL__MODIFICATION_EFFECT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModificationEffect(ChangeEffectKind newModificationEffect) {
		eDynamicSet(EvidencePackage.ARTEFACT_REL__MODIFICATION_EFFECT, EvidencePackage.Literals.ARTEFACT_REL__MODIFICATION_EFFECT, newModificationEffect);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEffectKind getRevocationEffect() {
		return (ChangeEffectKind)eDynamicGet(EvidencePackage.ARTEFACT_REL__REVOCATION_EFFECT, EvidencePackage.Literals.ARTEFACT_REL__REVOCATION_EFFECT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRevocationEffect(ChangeEffectKind newRevocationEffect) {
		eDynamicSet(EvidencePackage.ARTEFACT_REL__REVOCATION_EFFECT, EvidencePackage.Literals.ARTEFACT_REL__REVOCATION_EFFECT, newRevocationEffect);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Artefact getSource() {
		return (Artefact)eDynamicGet(EvidencePackage.ARTEFACT_REL__SOURCE, EvidencePackage.Literals.ARTEFACT_REL__SOURCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Artefact basicGetSource() {
		return (Artefact)eDynamicGet(EvidencePackage.ARTEFACT_REL__SOURCE, EvidencePackage.Literals.ARTEFACT_REL__SOURCE, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(Artefact newSource) {
		eDynamicSet(EvidencePackage.ARTEFACT_REL__SOURCE, EvidencePackage.Literals.ARTEFACT_REL__SOURCE, newSource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Artefact getTarget() {
		return (Artefact)eDynamicGet(EvidencePackage.ARTEFACT_REL__TARGET, EvidencePackage.Literals.ARTEFACT_REL__TARGET, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Artefact basicGetTarget() {
		return (Artefact)eDynamicGet(EvidencePackage.ARTEFACT_REL__TARGET, EvidencePackage.Literals.ARTEFACT_REL__TARGET, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(Artefact newTarget) {
		eDynamicSet(EvidencePackage.ARTEFACT_REL__TARGET, EvidencePackage.Literals.ARTEFACT_REL__TARGET, newTarget);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_REL__MODIFICATION_EFFECT:
				return getModificationEffect();
			case EvidencePackage.ARTEFACT_REL__REVOCATION_EFFECT:
				return getRevocationEffect();
			case EvidencePackage.ARTEFACT_REL__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case EvidencePackage.ARTEFACT_REL__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_REL__MODIFICATION_EFFECT:
				setModificationEffect((ChangeEffectKind)newValue);
				return;
			case EvidencePackage.ARTEFACT_REL__REVOCATION_EFFECT:
				setRevocationEffect((ChangeEffectKind)newValue);
				return;
			case EvidencePackage.ARTEFACT_REL__SOURCE:
				setSource((Artefact)newValue);
				return;
			case EvidencePackage.ARTEFACT_REL__TARGET:
				setTarget((Artefact)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_REL__MODIFICATION_EFFECT:
				setModificationEffect(MODIFICATION_EFFECT_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT_REL__REVOCATION_EFFECT:
				setRevocationEffect(REVOCATION_EFFECT_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT_REL__SOURCE:
				setSource((Artefact)null);
				return;
			case EvidencePackage.ARTEFACT_REL__TARGET:
				setTarget((Artefact)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_REL__MODIFICATION_EFFECT:
				return getModificationEffect() != MODIFICATION_EFFECT_EDEFAULT;
			case EvidencePackage.ARTEFACT_REL__REVOCATION_EFFECT:
				return getRevocationEffect() != REVOCATION_EFFECT_EDEFAULT;
			case EvidencePackage.ARTEFACT_REL__SOURCE:
				return basicGetSource() != null;
			case EvidencePackage.ARTEFACT_REL__TARGET:
				return basicGetTarget() != null;
		}
		return super.eIsSet(featureID);
	}

} //ArtefactRelImpl
