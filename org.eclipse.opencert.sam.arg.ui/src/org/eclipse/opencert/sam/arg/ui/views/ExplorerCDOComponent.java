/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.views;



import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;

import java.util.ArrayList;

import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.*;
import org.eclipse.ui.*;
import org.eclipse.swt.SWT;
import org.eclipse.opencert.sam.arg.ui.util.TreeSWTUtil;
import org.eclipse.opencert.sam.arg.ui.util.TreeListFiles;





public class ExplorerCDOComponent {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.eclipse.opencert.sam.arg.ui.views.ExplorerCDOComponent";
	private static final String ARG_extension = ".arg_diagram";
    private CDOView viewCDO=null;
    
	private String sTitle;

	public TreeViewer viewer;
	
	private IViewSite viewSite;
	
	private String searchDir;
	private ArrayList<String> refListDir ;
	
	String getTitle() {
		return sTitle;
	}

		
	class ViewContentProvider implements IStructuredContentProvider, 
										   ITreeContentProvider {
//		private TreeListFiles.TreeParent invisibleRoot;
		
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			/*
			if (parent.equals(getViewSite())) {
				if (invisibleRoot==null) initialize();
				return getChildren(invisibleRoot);
			}
			return getChildren(parent);
			*/
			findArguments(searchDir, refListDir);
			if (refListDir.isEmpty()) refListDir.add(searchDir);
			return refListDir.toArray();

		}
		public Object getParent(Object child) {
			if (child instanceof TreeListFiles.TreeObject) {
				return ((TreeListFiles.TreeObject)child).getParent();
			}
			return null;
		}
		public Object [] getChildren(Object parent) {
			if (parent instanceof TreeListFiles.TreeParent) {
				return ((TreeListFiles.TreeParent)parent).getChildren();
			}
			return new Object[0];
		}
		public boolean hasChildren(Object parent) {
			if (parent instanceof TreeListFiles.TreeParent)
				return ((TreeListFiles.TreeParent)parent).hasChildren();
			return false;
		}
/*
 * We will set up a dummy model to initialize tree heararchy.
 * In a real code, you will connect to a real model and
 * expose its hierarchy.
 */
		public Object initialize() {
			findArguments(searchDir, refListDir);
			return refListDir.get(0);
//			return invisibleRoot;
		}
		
	}
	class ViewLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			return obj.toString();
		}
		public Image getImage(Object obj) {
			String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
			if (obj instanceof TreeListFiles.TreeParent)
			   imageKey = ISharedImages.IMG_OBJ_FOLDER;
			return PlatformUI.getWorkbench().getSharedImages().getImage(imageKey);
		}
	}
	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public ExplorerCDOComponent(IViewSite vs, String sDir, String sTit) {
		viewSite = vs;
		searchDir = sDir;
		sTitle = sTit;
		refListDir = new ArrayList<String>();
	}

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		//DrillDownAdapter drillDownAdapter = new DrillDownAdapter(viewer);
		
		ViewContentProvider cp = new ViewContentProvider();
		viewer.setContentProvider(cp);
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameSorter());
		//viewer.setInput(pgetViewSite());
		//viewer.setInput(cp.initialize());
//		cp.initialize();
		viewer.setInput(viewSite);
		//viewer.expandAll();
		viewer.expandToLevel(AbstractTreeViewer.ALL_LEVELS);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	public void findArguments(String searchDir, ArrayList rListDir){
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		CDOView [] views = sessionCDO.getViews();
		if (views.length!=0) {
			viewCDO = views[0];				
		}
		CDOResourceNode[] listR=  viewCDO.getElements();	
		
		for (int i=0; i<listR.length; i++){
			String aux=listR[i].getPath();
			if(aux.contains(searchDir)){
				if (listR[i].getName().endsWith(ARG_extension)){
					rListDir.add(listR[i].getPath());
				}else{
					if(listR[i] instanceof CDOResourceFolder){ 
						checkFolderContents((CDOResourceFolder)listR[i],searchDir, rListDir);
					}
				}
			}else if(listR[i] instanceof CDOResourceFolder){
					checkFolderContents((CDOResourceFolder)listR[i],searchDir, rListDir);
				}
		}
		
	}

	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	private void checkFolderContents(CDOResourceFolder cdoResourceFolder, String searchDir, ArrayList rListDir) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN=  cdoResourceFolder.getNodes();
			for (int i=0; i<listN.size(); i++){
				String aux=listN.get(i).getPath();
				if(aux.contains(searchDir)){
					if(listN.get(i) instanceof CDOResourceFolder){
						checkFolderContents((CDOResourceFolder)listN.get(i), searchDir, rListDir);
					}
					else if (listN.get(i).getPath().contains(searchDir)){
						String aux1=listN.get(i).getName();
						if (listN.get(i).getName().endsWith(ARG_extension)){
							rListDir.add(listN.get(i).getPath());
						}
					}
				}
			}	
		}
	}
}
