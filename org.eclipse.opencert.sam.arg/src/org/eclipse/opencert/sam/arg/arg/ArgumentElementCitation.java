/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Argument Element Citation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation#getCitedType <em>Cited Type</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation#getArgumentationReference <em>Argumentation Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentElementCitation()
 * @model annotation="gmf.node label='id' label.icon='false' figure='gsnfigures.GSNArgumentModule' tool.small.bundle='org.eclipse.opencert.sam.arg' tool.small.path='GSN_tooling_icons/ArgumentModule.gif' tool.large.bundle='org.eclipse.opencert.sam.arg' tool.large.path='GSN_tooling_icons/ArgumentModule.gif' size='120,80'"
 * @generated
 */
public interface ArgumentElementCitation extends ArgumentElement {
	/**
	 * Returns the value of the '<em><b>Cited Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.sam.arg.arg.CitationElementType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cited Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cited Type</em>' attribute.
	 * @see org.eclipse.opencert.sam.arg.arg.CitationElementType
	 * @see #setCitedType(CitationElementType)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentElementCitation_CitedType()
	 * @model
	 * @generated
	 */
	CitationElementType getCitedType();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation#getCitedType <em>Cited Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cited Type</em>' attribute.
	 * @see org.eclipse.opencert.sam.arg.arg.CitationElementType
	 * @see #getCitedType()
	 * @generated
	 */
	void setCitedType(CitationElementType value);

	/**
	 * Returns the value of the '<em><b>Argumentation Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argumentation Reference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argumentation Reference</em>' attribute.
	 * @see #setArgumentationReference(String)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentElementCitation_ArgumentationReference()
	 * @model
	 * @generated
	 */
	String getArgumentationReference();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation#getArgumentationReference <em>Argumentation Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Argumentation Reference</em>' attribute.
	 * @see #getArgumentationReference()
	 * @generated
	 */
	void setArgumentationReference(String value);

} // ArgumentElementCitation
