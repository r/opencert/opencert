/*******************************************************************************
 * Copyright (c) 2018, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *   Faiz Ul Muram
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.epf.generateArgumentation.transformation.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;

import org.eclipse.swt.widgets.Shell;

import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.CaseEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.CasePersistedCanonicalEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.part.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;

import org.eclipse.emf.cdo.common.model.EMFUtil;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.epf.uma.impl.ProcessComponentImpl;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.gmf.runtime.notation.Diagram;


public class TransformationHandler implements IObjectActionDelegate {

	private Shell shell;
	private IWorkbenchPage page;
	private IWorkbenchWindow window;

	private URI domainModelURI;
	private EObject diagramRoot=null;
	private boolean transfDiagramOk=false;
	private Resource diagramResource;
	private IProgressMonitor monitor2;
	private String name;
	
	
	/**
	 * Constructor for Action1.
	 */
	public TransformationHandler() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
		page = targetPart.getSite().getPage();
		window = targetPart.getSite().getWorkbenchWindow();	
	}


	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		
		MessageDialog dg = new MessageDialog(
	            shell,
	            "Select Directory",
	            null,
	            "Please select the target assurance project from the CDO Repository",
	            MessageDialog.INFORMATION,
	            new String[]{
	                "Browse...", 
	                IDialogConstants.CANCEL_LABEL},
	            -1//SWT.CLOSE
	            );
	    
		if(dg.open()==0){
			
			//Connect to the CDO Repository
			CDOConnectionUtil.instance.init(
					PreferenceConstants.getRepositoryName(),
					PreferenceConstants.getProtocol(),
					PreferenceConstants.getServerName());
			CDOSession session = CDOConnectionUtil.instance.getCurrentSession();
			CDOView view = session.openView();
			CDOTransaction transaction = session.openTransaction();
			
			GUI gui = new GUI(shell,view);
			gui.create();
			gui.open();
			
			String selection = gui.resourceName();
	
			if(selection != null){
				
				//Get the current delivery process
				ISelection sel = page.getSelection();
				TreeSelection tree = (TreeSelection) sel;
				ProcessComponentImpl process = (ProcessComponentImpl) tree.getFirstElement();
						
				name = process.getName();
			
				//Invoke transformation
				Transformation transformation = new Transformation(page);
			
				ProgressMonitorDialog dialog = new ProgressMonitorDialog(shell);
				dialog.open();
				IProgressMonitor monitor = dialog.getProgressMonitor();
				monitor.beginTask("Generating the process-based argumentation model and diagram... ", 15);
				monitor.worked(1);
				monitor.subTask("Performing the transformation...");
	
				transformation.execute(monitor); 
				
				monitor.worked(5);
				monitor.subTask("Creating CDO Resources...");

				
				//Create the model CDO Resource
				CDOResource argModel;
				if (transaction.getOrCreateResource("/"+selection+"/ARGUMENTATION/"+name+".arg").isExisting()) {
					try {
						transaction.getResource("/"+selection+"/ARGUMENTATION/"+name+".arg").delete(Collections.EMPTY_MAP);
						transaction.commit();
					} catch (IOException | CommitException e) {
						e.printStackTrace();
					}
	
					argModel = transaction.getOrCreateResource("/"+selection+"/ARGUMENTATION/"+name+".arg");
				}else{
					argModel = transaction.getOrCreateResource("/"+selection+"/ARGUMENTATION/"+name+".arg");
				}
				//CDOResource argModel = transaction.getOrCreateResource("/"+selection+"/ARGUMENTATION/"+name+".arg");
				
				String argModelFile = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString()+"/Argumentation/"+name+".arg";
				URI argXmiUri = URI.createFileURI(argModelFile);
				URI argCDOUri = argModel.getURI();
			
				ResourceSet resourceSet = new ResourceSetImpl();
				resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
			
				Resource argXmiResource = resourceSet.createResource(argXmiUri);
				File newFile = new File(argModelFile);
				
				monitor.worked(6);
				monitor.subTask("Loading model to CDO Repository...");
	
				//Load the model contents into the created resource
				try {
					FileInputStream argFileInStream = new FileInputStream(newFile);
					argXmiResource.load(argFileInStream,Collections.EMPTY_MAP);
					argXmiResource.setURI(argXmiUri);	
				} catch (IOException e) {
					e.printStackTrace();
				}
			
				EMFUtil.safeResolveAll(resourceSet);
			
				CDOResource argCDOResource = transaction.getOrCreateResource(argCDOUri.path());
				argCDOResource.getContents().addAll(argXmiResource.getContents());
	
				EMFUtil.safeResolveAll(resourceSet);
				
				monitor.worked(7);
			
				try {
					argCDOResource.save(Collections.EMPTY_MAP);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				monitor.worked(8);
				monitor.subTask("Creating diagram...");
				
				//Create diagram
				createArgDiagram();
				
				monitor.worked(9);
				monitor.subTask("Creating CDO Resources...");
				
				
				//Create the diagram CDO Resource
				CDOResource argDiagram;
				
				if (transaction.getOrCreateResource("/"+selection+"/ARGUMENTATION/"+name+".arg_diagram").isExisting()) {
					try {
						transaction.getResource("/"+selection+"/ARGUMENTATION/"+name+".arg_diagram").delete(Collections.EMPTY_MAP);
						transaction.commit();
					} catch (IOException | CommitException e) {
						e.printStackTrace();
					}
	
					argDiagram = transaction.getOrCreateResource("/"+selection+"/ARGUMENTATION/"+name+".arg_diagram");
				}else{
					argDiagram = transaction.getOrCreateResource("/"+selection+"/ARGUMENTATION/"+name+".arg_diagram");
				}
				
				monitor.worked(10);
				
				//Load the diagram contents into the created resource
				String argDiagramFile = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString()+"/Argumentation/"+name+".arg_diagram";
				URI argDiagUri = URI.createFileURI(argDiagramFile);
			    URI argDiagCdoURI = argDiagram.getURI(); 
				
				ResourceSet resourceSet2 = new ResourceSetImpl();
				resourceSet2.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
				
				Resource argDiagramXmiResource = resourceSet.createResource(argDiagUri);
				File newFile2 = new File(argDiagramFile);
				
				monitor.worked(11);
				monitor.subTask("Loading diagram to CDO Repository...");
				
				try {
					FileInputStream argFileInStream2 = new FileInputStream(newFile2);
					argDiagramXmiResource.load(argFileInStream2,Collections.EMPTY_MAP);
					argDiagramXmiResource.setURI(argDiagUri);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				EMFUtil.safeResolveAll(resourceSet2);
				
				CDOResource argDiagramCDOResource = transaction.getOrCreateResource(argDiagCdoURI.path());
				argDiagramCDOResource.getContents().addAll(argDiagramXmiResource.getContents());
				
				EMFUtil.safeResolveAll(resourceSet2);
				
				monitor.worked(12);
				
				try {
					argDiagramCDOResource.save(Collections.EMPTY_MAP);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				monitor.worked(13);
				
				//Commit the transaction
				try {
					transaction.commit();
				} catch (ConcurrentAccessException e) {
					e.printStackTrace();
				} catch (CommitException e) {
					e.printStackTrace();
				}
				
				
				monitor.worked(14);
				monitor.done();
				dialog.close();
				
				if(transfDiagramOk == true){
					try{
						MessageDialog.openInformation(shell, "Transformation Completed", 
									"The process-based argumentation model and diagram are generated under:\n\n"
									+"    - ARGUMENTATION folder of the project "+selection+" in the CDO Repository\n"
									+"    - Argumentation project in the current workspace");
					}catch(Exception e){
						System.out.println(e);
					}
				}
					
				
				ProgressMonitorDialog dialog2 = new ProgressMonitorDialog(shell);
				dialog2.open();
				monitor2 = dialog2.getProgressMonitor();
				monitor2.beginTask("Opening diagram... ", 6);
				monitor2.worked(1);
				
				//Change the perspective
				if (PlatformUI.getWorkbench() != null) {
					IPerspectiveDescriptor descriptor = window.getWorkbench()
					    .getPerspectiveRegistry().findPerspectiveWithId("org.eclipse.ui.resourcePerspective");
		
		    		PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					    .getActivePage().setPerspective(descriptor);
		        }
				
				monitor2.worked(2);
				
				generateDiagram(new NullProgressMonitor());
				
				monitor2.worked(5);
				monitor2.done();
				dialog2.close();
			
			}else{
				MessageDialog.openError(shell, "Selection error", "An assurance project must be selected in order to perform the transformation"); 
			}

		}else{
			MessageDialog.openError(shell, "Selection error", "An assurance project must be selected in order to perform the transformation");
		}
	}
	
	
	/* Copied and modified from org.eclipse.opencert.sam.arg.arg.diagram.part.ArgNewDiagramFileWizard.java */
	public void createArgDiagram(){
		IPath workspace = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		domainModelURI = URI.createPlatformResourceURI("Argumentation/"+name+".arg", true);
		LinkedList<IFile> affectedFiles = new LinkedList<IFile>();
		File file = new File(workspace+"/Argumentation/"+name+".arg_diagram");
		
		try {
			file.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		IPath location= Path.fromOSString(file.getPath());
		IFile diagramFile= ResourcesPlugin.getWorkspace().getRoot().getFileForLocation(location);
				
		setCharset(diagramFile); // setCharset method is added
		
		affectedFiles.add(diagramFile);
		URI diagramModelURI = URI.createPlatformResourceURI("Argumentation/"+name+".arg_diagram", true);
		
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		ResourceSet resourceSet = editingDomain.getResourceSet();
		
		try {
			Resource resource = resourceSet.getResource(domainModelURI, true);
			diagramRoot = (EObject) resource.getContents().get(0);
		} catch (WrappedException ex) {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Unable to load resource: " + domainModelURI, ex); //$NON-NLS-1$
		}
	
		diagramResource = resourceSet.createResource(diagramModelURI);
		AbstractTransactionalCommand command = new AbstractTransactionalCommand(
				editingDomain,
				Messages.ArgNewDiagramFileWizard_InitDiagramCommand,
				affectedFiles) {

			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {
				int diagramVID = ArgVisualIDRegistry
						.getDiagramVisualID(diagramRoot);
				if (diagramVID != CaseEditPart.VISUAL_ID) {
					return CommandResult
							.newErrorCommandResult(Messages.ArgNewDiagramFileWizard_IncorrectRootError);
				}
				Diagram diagram = ViewService.createDiagram(
						diagramRoot,
						CaseEditPart.MODEL_ID,
						ArgDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
				diagramResource.getContents().add(diagram);

				return CommandResult.newOKCommandResult();
			}
		};
		try {
			OperationHistoryFactory.getOperationHistory().execute(command,
					new NullProgressMonitor(), null);
			diagramResource.save(ArgDiagramEditorUtil.getSaveOptions());

		} catch (ExecutionException e) {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Unable to create model and diagram", e); //$NON-NLS-1$
		} catch (IOException ex) {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Save operation failed for: " + diagramModelURI, ex); //$NON-NLS-1$
		}	
		
		transfDiagramOk = true;
	}
	
	
	public void generateDiagram(IProgressMonitor monitor) {
		try {
			generateDiagram0(monitor);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void generateDiagram0(IProgressMonitor monitor) throws IOException {
		if (diagramResource != null) {
			try {
				ArgDiagramEditorUtil.openDiagram(diagramResource);

				IEditorPart editorPart = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.getActiveEditor();
				
					DiagramEditPart editp = ((IDiagramWorkbenchPart) editorPart).getDiagramEditPart();
					
					monitor2.worked(3);

					editp.removeEditPolicy(EditPolicyRoles.CANONICAL_ROLE);
 					editp.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
							new CasePersistedCanonicalEditPolicy());
 					
 					monitor2.worked(4);
 					
	        		ArgDiagramEditor dawnEditorPart = (ArgDiagramEditor)editorPart;
	        		dawnEditorPart.doSave(new NullProgressMonitor());

			} catch (PartInitException e) {
				System.out.println(e);
			}
		}
	}
	
	public static void setCharset(IFile file) {
		if (file == null) {
			return;
		}
		try {
			file.setCharset("UTF-8", new NullProgressMonitor()); //$NON-NLS-1$
		} catch (CoreException e) {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Unable to set charset for file " + file.getFullPath(), e); //$NON-NLS-1$
		}
	}
	
	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {

	}
}