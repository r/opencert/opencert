/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts;

/**
 * 
 * 
 */
public class BaselineViewsRepository {

	public static final int SWT_KIND = 0;

	public static final int FORM_KIND = 1;


	/**
	 * BaseFramework view descriptor
	 * 
	 */
	public static class BaseFramework {
		public static class Properties {
	
			
			public static String id = "baseline::BaseFramework::properties::id";
			
			
			public static String name = "baseline::BaseFramework::properties::name";
			
			
			public static String description = "baseline::BaseFramework::properties::description";
			
			
			public static String scope = "baseline::BaseFramework::properties::scope";
			
			
			public static String rev = "baseline::BaseFramework::properties::rev";
			
			
			public static String purpose = "baseline::BaseFramework::properties::purpose";
			
			
			public static String publisher = "baseline::BaseFramework::properties::publisher";
			
			
			public static String issued = "baseline::BaseFramework::properties::issued";
			
			
			public static String ownedActivities = "baseline::BaseFramework::properties::ownedActivities";
			
			
			public static String ownedArtefact = "baseline::BaseFramework::properties::ownedArtefact";
			
			
			public static String ownedRequirement = "baseline::BaseFramework::properties::ownedRequirement";
			
			
			public static String ownedApplicLevel = "baseline::BaseFramework::properties::ownedApplicLevel";
			
			
			public static String ownedCriticLevel = "baseline::BaseFramework::properties::ownedCriticLevel";
			
			
			public static String ownedRole = "baseline::BaseFramework::properties::ownedRole";
			
			
			public static String ownedTechnique = "baseline::BaseFramework::properties::ownedTechnique";
			
	
		}
	
	}

	/**
	 * BaseRequirement view descriptor
	 * 
	 */
	public static class BaseRequirement {
		public static class Properties {
	
			
			public static String id = "baseline::BaseRequirement::properties::id";
			
			
			public static String name = "baseline::BaseRequirement::properties::name";
			
			
			public static String description = "baseline::BaseRequirement::properties::description";
			
			
			public static String reference = "baseline::BaseRequirement::properties::reference";
			
			
			public static String assumptions = "baseline::BaseRequirement::properties::assumptions";
			
			
			public static String rationale = "baseline::BaseRequirement::properties::rationale";
			
			
			public static String image = "baseline::BaseRequirement::properties::image";
			
			
			public static String annotations = "baseline::BaseRequirement::properties::annotations";
			
			
			public static String ownedRel = "baseline::BaseRequirement::properties::ownedRel";
			
			
			public static String subRequirement = "baseline::BaseRequirement::properties::subRequirement";
			
	
		}
	
	}

	/**
	 * BaseArtefact view descriptor
	 * 
	 */
	public static class BaseArtefact {
		public static class Properties {
	
			
			public static String id = "baseline::BaseArtefact::properties::id";
			
			
			public static String name = "baseline::BaseArtefact::properties::name";
			
			
			public static String description = "baseline::BaseArtefact::properties::description";
			
			
			public static String reference = "baseline::BaseArtefact::properties::reference";
			
			
			public static String constrainingRequirement = "baseline::BaseArtefact::properties::constrainingRequirement";
			
			
			public static String applicableTechnique = "baseline::BaseArtefact::properties::applicableTechnique";
			
			
			public static String ownedRel = "baseline::BaseArtefact::properties::ownedRel";
			
			
			public static String property = "baseline::BaseArtefact::properties::property";
			
	
		}
	
	}

	/**
	 * BaseActivity view descriptor
	 * 
	 */
	public static class BaseActivity {
		public static class Properties {
	
			
			public static String id = "baseline::BaseActivity::properties::id";
			
			
			public static String name = "baseline::BaseActivity::properties::name";
			
			
			public static String description = "baseline::BaseActivity::properties::description";
			
			
			public static String objective = "baseline::BaseActivity::properties::objective";
			
			
			public static String scope = "baseline::BaseActivity::properties::scope";
			
			
			public static String requiredArtefact = "baseline::BaseActivity::properties::requiredArtefact";
			
			
			public static String producedArtefact = "baseline::BaseActivity::properties::producedArtefact";
			
			
			public static String subActivity = "baseline::BaseActivity::properties::subActivity";
			
			
			public static String precedingActivity = "baseline::BaseActivity::properties::precedingActivity";
			
			
			public static String role = "baseline::BaseActivity::properties::role";
			
			
			public static String applicableTechnique = "baseline::BaseActivity::properties::applicableTechnique";
			
			
			public static String ownedRel = "baseline::BaseActivity::properties::ownedRel";
			
	
		}
	
	}

	/**
	 * BaseRequirementRel view descriptor
	 * 
	 */
	public static class BaseRequirementRel {
		public static class Properties {
	
			
			public static String target = "baseline::BaseRequirementRel::properties::target";
			
			
			public static String source = "baseline::BaseRequirementRel::properties::source";
			
			
			public static String type = "baseline::BaseRequirementRel::properties::type";
			
	
		}
	
	}

	/**
	 * BaseRole view descriptor
	 * 
	 */
	public static class BaseRole {
		public static class Properties {
	
			
			public static String id = "baseline::BaseRole::properties::id";
			
			
			public static String name = "baseline::BaseRole::properties::name";
			
			
			public static String description = "baseline::BaseRole::properties::description";
			
	
		}
	
	}

	/**
	 * BaseApplicabilityLevel view descriptor
	 * 
	 */
	public static class BaseApplicabilityLevel {
		public static class Properties {
	
			
			public static String id = "baseline::BaseApplicabilityLevel::properties::id";
			
			
			public static String name = "baseline::BaseApplicabilityLevel::properties::name";
			
			
			public static String description = "baseline::BaseApplicabilityLevel::properties::description";
			
	
		}
	
	}

	/**
	 * BaseCriticalityLevel view descriptor
	 * 
	 */
	public static class BaseCriticalityLevel {
		public static class Properties {
	
			
			public static String id = "baseline::BaseCriticalityLevel::properties::id";
			
			
			public static String name = "baseline::BaseCriticalityLevel::properties::name";
			
			
			public static String description = "baseline::BaseCriticalityLevel::properties::description";
			
	
		}
	
	}

	/**
	 * BaseTechnique view descriptor
	 * 
	 */
	public static class BaseTechnique {
		public static class Properties {
	
			
			public static String id = "baseline::BaseTechnique::properties::id";
			
			
			public static String name = "baseline::BaseTechnique::properties::name";
			
			
			public static String description = "baseline::BaseTechnique::properties::description";
			
			
			public static String criticApplic = "baseline::BaseTechnique::properties::criticApplic";
			
			
			public static String aim = "baseline::BaseTechnique::properties::aim";
			
	
		}
	
	}

	/**
	 * BaseArtefactRel view descriptor
	 * 
	 */
	public static class BaseArtefactRel {
		public static class Properties {
	
			
			public static String id = "baseline::BaseArtefactRel::properties::id";
			
			
			public static String name = "baseline::BaseArtefactRel::properties::name";
			
			
			public static String description = "baseline::BaseArtefactRel::properties::description";
			
			
			public static String maxMultiplicitySource = "baseline::BaseArtefactRel::properties::maxMultiplicitySource";
			
			
			public static String minMultiplicitySource = "baseline::BaseArtefactRel::properties::minMultiplicitySource";
			
			
			public static String maxMultiplicityTarget = "baseline::BaseArtefactRel::properties::maxMultiplicityTarget";
			
			
			public static String minMultiplicityTarget = "baseline::BaseArtefactRel::properties::minMultiplicityTarget";
			
			
			public static String modificationEffect = "baseline::BaseArtefactRel::properties::modificationEffect";
			
			
			public static String revocationEffect = "baseline::BaseArtefactRel::properties::revocationEffect";
			
			
			public static String source = "baseline::BaseArtefactRel::properties::source";
			
			
			public static String target = "baseline::BaseArtefactRel::properties::target";
			
	
		}
	
	}

	/**
	 * BaseCriticalityApplicability view descriptor
	 * 
	 */
	public static class BaseCriticalityApplicability {
		public static class Properties {
	
			
			public static String applicLevel = "baseline::BaseCriticalityApplicability::properties::applicLevel";
			
			
			public static String criticLevel = "baseline::BaseCriticalityApplicability::properties::criticLevel";
			
			
			public static String applicLevelCombo = "baseline::BaseCriticalityApplicability::properties::applicLevelCombo";
			
			
			public static String criticLevelCombo = "baseline::BaseCriticalityApplicability::properties::criticLevelCombo";
			
			
			public static String comment = "baseline::BaseCriticalityApplicability::properties::comment";
			
	
		}
	
	}

	/**
	 * BaseActivityRel view descriptor
	 * 
	 */
	public static class BaseActivityRel {
		public static class Properties {
	
			
			public static String type = "baseline::BaseActivityRel::properties::type";
			
			
			public static String source = "baseline::BaseActivityRel::properties::source";
			
			
			public static String target = "baseline::BaseActivityRel::properties::target";
			
	
		}
	
	}

	/**
	 * BaseIndependencyLevel view descriptor
	 * 
	 */
	public static class BaseIndependencyLevel {
		public static class Properties {
	
			
			public static String id = "baseline::BaseIndependencyLevel::properties::id";
			
			
			public static String name = "baseline::BaseIndependencyLevel::properties::name";
			
			
			public static String description = "baseline::BaseIndependencyLevel::properties::description";
			
	
		}
	
	}

	/**
	 * BaseRecommendationLevel view descriptor
	 * 
	 */
	public static class BaseRecommendationLevel {
		public static class Properties {
	
			
			public static String id = "baseline::BaseRecommendationLevel::properties::id";
			
			
			public static String name = "baseline::BaseRecommendationLevel::properties::name";
			
			
			public static String description = "baseline::BaseRecommendationLevel::properties::description";
			
	
		}
	
	}

	/**
	 * BaseControlCategory view descriptor
	 * 
	 */
	public static class BaseControlCategory {
		public static class Properties {
	
			
			public static String id = "baseline::BaseControlCategory::properties::id";
			
			
			public static String name = "baseline::BaseControlCategory::properties::name";
			
			
			public static String description = "baseline::BaseControlCategory::properties::description";
			
	
		}
	
	}

	/**
	 * BaseApplicability view descriptor
	 * 
	 */
	public static class BaseApplicability {
		public static class Properties {
	
			
			public static String id = "baseline::BaseApplicability::properties::id";
			
			
			public static String name = "baseline::BaseApplicability::properties::name";
			
			
			public static String applicCritic = "baseline::BaseApplicability::properties::applicCritic";
			
			
			public static String applicCriticTable = "baseline::BaseApplicability::properties::applicCriticTable";
			
			
			public static String comments = "baseline::BaseApplicability::properties::comments";
			
			
			public static String applicTarget = "baseline::BaseApplicability::properties::applicTarget";
			
			
			public static String ownedRel = "baseline::BaseApplicability::properties::ownedRel";
			
	
		}
	
	}

	/**
	 * BaseApplicabilityRel view descriptor
	 * 
	 */
	public static class BaseApplicabilityRel {
		public static class Properties {
	
			
			public static String type = "baseline::BaseApplicabilityRel::properties::type";
			
			
			public static String source = "baseline::BaseApplicabilityRel::properties::source";
			
			
			public static String target = "baseline::BaseApplicabilityRel::properties::target";
			
	
		}
	
	}

	/**
	 * BaseEquivalenceMap view descriptor
	 * 
	 */
	public static class BaseEquivalenceMap {
		public static class Properties {
	
			
			public static String id = "baseline::BaseEquivalenceMap::properties::id";
			
			
			public static String name = "baseline::BaseEquivalenceMap::properties::name";
			
			
			public static String mapGroup = "baseline::BaseEquivalenceMap::properties::mapGroup";
			
			
			public static String mapGroupCombo = "baseline::BaseEquivalenceMap::properties::mapGroupCombo";
			
			
			public static String type = "baseline::BaseEquivalenceMap::properties::type";
			
			
			public static String justification = "baseline::BaseEquivalenceMap::properties::justification";
			
			
			public static String target = "baseline::BaseEquivalenceMap::properties::target";
			
	
		}
	
	}

	/**
	 * BaseComplianceMap view descriptor
	 * 
	 */
	public static class BaseComplianceMap {
		public static class Properties {
	
			
			public static String id = "baseline::BaseComplianceMap::properties::id";
			
			
			public static String name = "baseline::BaseComplianceMap::properties::name";
			
			
			public static String mapGroup = "baseline::BaseComplianceMap::properties::mapGroup";
			
			
			public static String mapGroupCombo = "baseline::BaseComplianceMap::properties::mapGroupCombo";
			
			
			public static String type = "baseline::BaseComplianceMap::properties::type";
			
			
			public static String justification = "baseline::BaseComplianceMap::properties::justification";
			
			
			public static String target = "baseline::BaseComplianceMap::properties::target";
			
	
		}
	
	}

	/**
	 * BaseActivitySelection view descriptor
	 * 
	 */
	public static class BaseActivitySelection {
		public static class Properties {
	
			
			public static String isSelected = "baseline::BaseActivitySelection::properties::isSelected";
			
			
			public static String selectionJustification = "baseline::BaseActivitySelection::properties::selectionJustification";
			
	
		}
	
	}

	/**
	 * BaseActivityRequirement view descriptor
	 * 
	 */
	public static class BaseActivityRequirement {
		public static class Properties {
	
			
			public static String ownedRequirement = "baseline::BaseActivityRequirement::properties::ownedRequirement";
			
	
		}
	
	}

	/**
	 * BaseActivityApplicability view descriptor
	 * 
	 */
	public static class BaseActivityApplicability {
		public static class Properties {
	
			
			public static String applicability = "baseline::BaseActivityApplicability::properties::applicability";
			
			
			public static String applicabilityTable = "baseline::BaseActivityApplicability::properties::applicabilityTable";
			
	
		}
	
	}

	/**
	 * BaseActivityEquivalenceMap view descriptor
	 * 
	 */
	public static class BaseActivityEquivalenceMap {
		public static class Properties {
	
			
			public static String equivalenceMap = "baseline::BaseActivityEquivalenceMap::properties::equivalenceMap";
			
			
			public static String activityEquivalenceMap = "baseline::BaseActivityEquivalenceMap::properties::activityEquivalenceMap";
			
	
		}
	
	}

	/**
	 * BaseActivityComplianceMap view descriptor
	 * 
	 */
	public static class BaseActivityComplianceMap {
		public static class Properties {
	
			
			public static String complianceMap = "baseline::BaseActivityComplianceMap::properties::complianceMap";
			
			
			public static String activityComplianceMap = "baseline::BaseActivityComplianceMap::properties::activityComplianceMap";
			
	
		}
	
	}

	/**
	 * BaseArtefactSelection view descriptor
	 * 
	 */
	public static class BaseArtefactSelection {
		public static class Properties {
	
			
			public static String isSelected = "baseline::BaseArtefactSelection::properties::isSelected";
			
			
			public static String selectionJustification = "baseline::BaseArtefactSelection::properties::selectionJustification";
			
	
		}
	
	}

	/**
	 * BaseArtefactEquivalenceMap view descriptor
	 * 
	 */
	public static class BaseArtefactEquivalenceMap {
		public static class Properties {
	
			
			public static String equivalenceMap = "baseline::BaseArtefactEquivalenceMap::properties::equivalenceMap";
			
			
			public static String artefactEquivalenceMap = "baseline::BaseArtefactEquivalenceMap::properties::artefactEquivalenceMap";
			
	
		}
	
	}

	/**
	 * BaseArtefactComplianceMap view descriptor
	 * 
	 */
	public static class BaseArtefactComplianceMap {
		public static class Properties {
	
			
			public static String complianceMap = "baseline::BaseArtefactComplianceMap::properties::complianceMap";
			
			
			public static String artefactComplianceMap = "baseline::BaseArtefactComplianceMap::properties::artefactComplianceMap";
			
	
		}
	
	}

	/**
	 * BaseRolSelection view descriptor
	 * 
	 */
	public static class BaseRolSelection {
		public static class Properties {
	
			
			public static String isSelected = "baseline::BaseRolSelection::properties::isSelected";
			
			
			public static String selectionJustification = "baseline::BaseRolSelection::properties::selectionJustification";
			
	
		}
	
	}

	/**
	 * BaseRoleEquivelanceMap view descriptor
	 * 
	 */
	public static class BaseRoleEquivelanceMap {
		public static class Properties {
	
			
			public static String equivalenceMap = "baseline::BaseRoleEquivelanceMap::properties::equivalenceMap";
			
			
			public static String roleEquivalenceMap = "baseline::BaseRoleEquivelanceMap::properties::roleEquivalenceMap";
			
	
		}
	
	}

	/**
	 * BaseRoleComplianceMap view descriptor
	 * 
	 */
	public static class BaseRoleComplianceMap {
		public static class Properties {
	
			
			public static String complianceMap = "baseline::BaseRoleComplianceMap::properties::complianceMap";
			
			
			public static String roleComplianceMap = "baseline::BaseRoleComplianceMap::properties::roleComplianceMap";
			
	
		}
	
	}

	/**
	 * BaseRequirementSelection view descriptor
	 * 
	 */
	public static class BaseRequirementSelection {
		public static class Properties {
	
			
			public static String isSelected = "baseline::BaseRequirementSelection::properties::isSelected";
			
			
			public static String selectionJustification = "baseline::BaseRequirementSelection::properties::selectionJustification";
			
	
		}
	
	}

	/**
	 * BaseRequirementApplicability view descriptor
	 * 
	 */
	public static class BaseRequirementApplicability {
		public static class Properties {
	
			
			public static String applicability = "baseline::BaseRequirementApplicability::properties::applicability";
			
			
			public static String applicabilityTable = "baseline::BaseRequirementApplicability::properties::applicabilityTable";
			
	
		}
	
	}

	/**
	 * BaseRequirementEquivalenceMap view descriptor
	 * 
	 */
	public static class BaseRequirementEquivalenceMap {
		public static class Properties {
	
			
			public static String equivalenceMap = "baseline::BaseRequirementEquivalenceMap::properties::equivalenceMap";
			
			
			public static String requirementEquivalenceMap = "baseline::BaseRequirementEquivalenceMap::properties::requirementEquivalenceMap";
			
	
		}
	
	}

	/**
	 * BaseRequerimentComplianceMap view descriptor
	 * 
	 */
	public static class BaseRequerimentComplianceMap {
		public static class Properties {
	
			
			public static String complianceMap = "baseline::BaseRequerimentComplianceMap::properties::complianceMap";
			
			
			public static String requirementComplianceMapTable = "baseline::BaseRequerimentComplianceMap::properties::requirementComplianceMapTable";
			
	
		}
	
	}

	/**
	 * BaseTechniqueEquivalenceMap view descriptor
	 * 
	 */
	public static class BaseTechniqueEquivalenceMap {
		public static class Properties {
	
			
			public static String equivalenceMap = "baseline::BaseTechniqueEquivalenceMap::properties::equivalenceMap";
			
			
			public static String techniqueEquivalenceMap = "baseline::BaseTechniqueEquivalenceMap::properties::techniqueEquivalenceMap";
			
	
		}
	
	}

	/**
	 * BaseTechniqueComplianceMap view descriptor
	 * 
	 */
	public static class BaseTechniqueComplianceMap {
		public static class Properties {
	
			
			public static String complianceMap = "baseline::BaseTechniqueComplianceMap::properties::complianceMap";
			
			
			public static String techniqueComplianceMap = "baseline::BaseTechniqueComplianceMap::properties::techniqueComplianceMap";
			
	
		}
	
	}

	/**
	 * BaseTechniqueSelection view descriptor
	 * 
	 */
	public static class BaseTechniqueSelection {
		public static class Properties {
	
			
			public static String isSelected = "baseline::BaseTechniqueSelection::properties::isSelected";
			
			
			public static String selectionJustification = "baseline::BaseTechniqueSelection::properties::selectionJustification";
			
	
		}
	
	}

}
