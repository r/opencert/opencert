/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.diagram.edit.policies;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.notation.View;

import java.util.ArrayList;
import java.util.List;

public class DawnVocabularyCanonicalEditPolicy
		extends
			VocabularyCanonicalEditPolicy {

	public DawnVocabularyCanonicalEditPolicy() {
		super();
	}

	@Override
	protected CreateViewRequest getCreateViewRequest(
			List<ViewDescriptor> descriptors) {
//		List<View> viewChildren = getViewChildren();
//
//		List<ViewDescriptor> tbr = new ArrayList<CreateViewRequest.ViewDescriptor>();
//
//		for (ViewDescriptor desc : descriptors) {
//			EObject obj = (EObject) ((CanonicalElementAdapter) desc
//					.getElementAdapter()).getRealObject();
//
//			boolean found = false;
//
//			for (View view : viewChildren) {
//				if (view.getElement().equals(obj)) {
//					found = true;
//					break;
//				}
//			}
//			if (!found) {
//				tbr.add(desc);
//			}
//		}
//
//		descriptors.removeAll(tbr);

		return new CreateViewRequest(descriptors);
	}
}
