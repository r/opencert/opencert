/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers.visitors;

import java.util.List;

import org.eclipse.opencert.webapp.reports.containers.ComplianceAsset;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetEvaluation;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetResource;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetails;

public class ComplianceDetailsTraverser<PRODUCED_NODE>
{
	private final IComplianceDetailsVisitor<PRODUCED_NODE> _visitor;
	
	public ComplianceDetailsTraverser(IComplianceDetailsVisitor<PRODUCED_NODE> visitor)
	{
		_visitor = visitor;
	}

	public void traverseComplianceDetails(List<? extends ComplianceDetails> complianceDetailsAll) 
	{
		for (ComplianceDetails complianceDetails : complianceDetailsAll) 
		{	
			//Level I:
			PRODUCED_NODE n1 = _visitor.visitComplianceDetails(complianceDetails);
			
			for (ComplianceAsset complianceAsset : complianceDetails.getComplianceAssets()) 
			{								
				//Level II:
				PRODUCED_NODE n2 = _visitor.visitComplianceAsset(complianceAsset, complianceDetails, n1);
				
				if (complianceAsset.getComplianceAssetResources() != null) 
				{
					for (ComplianceAssetResource complianceResource : complianceAsset.getComplianceAssetResources()) 
					{
						//LEVEL III:
						_visitor.visitComplianceResource(complianceResource, complianceDetails, n2);
					}
				}
				
				if (complianceAsset.getComplianceAssetEvaluations() != null) 
				{
					for (ComplianceAssetEvaluation complianceEvaluation : complianceAsset.getComplianceAssetEvaluations()) 
					{
						//LEVEL III:
						_visitor.visitComplianceEvaluation(complianceEvaluation, complianceDetails, n2);
					}
				}
			}
		}
	}
}