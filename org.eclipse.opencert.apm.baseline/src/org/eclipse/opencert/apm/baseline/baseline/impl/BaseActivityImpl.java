/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.BaselineElement;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#isIsSelected <em>Is Selected</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getSelectionJustification <em>Selection Justification</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getObjective <em>Objective</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getRequiredArtefact <em>Required Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getProducedArtefact <em>Produced Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getSubActivity <em>Sub Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getPrecedingActivity <em>Preceding Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getOwnedRequirement <em>Owned Requirement</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getRole <em>Role</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getApplicableTechnique <em>Applicable Technique</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl#getApplicability <em>Applicability</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BaseActivityImpl extends BaseAssurableElementImpl implements BaseActivity {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BaselinePackage.Literals.BASE_ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsSelected() {
		return (Boolean)eGet(BaselinePackage.Literals.BASELINE_ELEMENT__IS_SELECTED, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsSelected(boolean newIsSelected) {
		eSet(BaselinePackage.Literals.BASELINE_ELEMENT__IS_SELECTED, newIsSelected);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSelectionJustification() {
		return (String)eGet(BaselinePackage.Literals.BASELINE_ELEMENT__SELECTION_JUSTIFICATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectionJustification(String newSelectionJustification) {
		eSet(BaselinePackage.Literals.BASELINE_ELEMENT__SELECTION_JUSTIFICATION, newSelectionJustification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return (String)eGet(GeneralPackage.Literals.NAMED_ELEMENT__ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		eSet(GeneralPackage.Literals.NAMED_ELEMENT__ID, newId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eGet(GeneralPackage.Literals.NAMED_ELEMENT__NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eSet(GeneralPackage.Literals.NAMED_ELEMENT__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return (String)eGet(GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		eSet(GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, newDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getObjective() {
		return (String)eGet(BaselinePackage.Literals.BASE_ACTIVITY__OBJECTIVE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjective(String newObjective) {
		eSet(BaselinePackage.Literals.BASE_ACTIVITY__OBJECTIVE, newObjective);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getScope() {
		return (String)eGet(BaselinePackage.Literals.BASE_ACTIVITY__SCOPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(String newScope) {
		eSet(BaselinePackage.Literals.BASE_ACTIVITY__SCOPE, newScope);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseArtefact> getRequiredArtefact() {
		return (EList<BaseArtefact>)eGet(BaselinePackage.Literals.BASE_ACTIVITY__REQUIRED_ARTEFACT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseArtefact> getProducedArtefact() {
		return (EList<BaseArtefact>)eGet(BaselinePackage.Literals.BASE_ACTIVITY__PRODUCED_ARTEFACT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseActivity> getSubActivity() {
		return (EList<BaseActivity>)eGet(BaselinePackage.Literals.BASE_ACTIVITY__SUB_ACTIVITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseActivity> getPrecedingActivity() {
		return (EList<BaseActivity>)eGet(BaselinePackage.Literals.BASE_ACTIVITY__PRECEDING_ACTIVITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseRequirement> getOwnedRequirement() {
		return (EList<BaseRequirement>)eGet(BaselinePackage.Literals.BASE_ACTIVITY__OWNED_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseRole> getRole() {
		return (EList<BaseRole>)eGet(BaselinePackage.Literals.BASE_ACTIVITY__ROLE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseTechnique getApplicableTechnique() {
		return (BaseTechnique)eGet(BaselinePackage.Literals.BASE_ACTIVITY__APPLICABLE_TECHNIQUE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicableTechnique(BaseTechnique newApplicableTechnique) {
		eSet(BaselinePackage.Literals.BASE_ACTIVITY__APPLICABLE_TECHNIQUE, newApplicableTechnique);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseActivityRel> getOwnedRel() {
		return (EList<BaseActivityRel>)eGet(BaselinePackage.Literals.BASE_ACTIVITY__OWNED_REL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseApplicability> getApplicability() {
		return (EList<BaseApplicability>)eGet(BaselinePackage.Literals.BASE_ACTIVITY__APPLICABILITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == BaselineElement.class) {
			switch (derivedFeatureID) {
				case BaselinePackage.BASE_ACTIVITY__IS_SELECTED: return BaselinePackage.BASELINE_ELEMENT__IS_SELECTED;
				case BaselinePackage.BASE_ACTIVITY__SELECTION_JUSTIFICATION: return BaselinePackage.BASELINE_ELEMENT__SELECTION_JUSTIFICATION;
				default: return -1;
			}
		}
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case BaselinePackage.BASE_ACTIVITY__ID: return GeneralPackage.NAMED_ELEMENT__ID;
				case BaselinePackage.BASE_ACTIVITY__NAME: return GeneralPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (derivedFeatureID) {
				case BaselinePackage.BASE_ACTIVITY__DESCRIPTION: return GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == BaselineElement.class) {
			switch (baseFeatureID) {
				case BaselinePackage.BASELINE_ELEMENT__IS_SELECTED: return BaselinePackage.BASE_ACTIVITY__IS_SELECTED;
				case BaselinePackage.BASELINE_ELEMENT__SELECTION_JUSTIFICATION: return BaselinePackage.BASE_ACTIVITY__SELECTION_JUSTIFICATION;
				default: return -1;
			}
		}
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.NAMED_ELEMENT__ID: return BaselinePackage.BASE_ACTIVITY__ID;
				case GeneralPackage.NAMED_ELEMENT__NAME: return BaselinePackage.BASE_ACTIVITY__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION: return BaselinePackage.BASE_ACTIVITY__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //BaseActivityImpl
