/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
	
	public static String convertDateToString(Date date)
	{
		DateFormat df = new SimpleDateFormat(DATE_FORMAT);
		String convertedDate = "";
		try {
			convertedDate = df.format(date.getTime());
		} catch (Exception ex) {
			OpencertLogger.error(ex);
		}
		
		return convertedDate;
	}
	
	public static Date convertStringToDate(String date)
	{
		Date convertedDate = null;
		
		DateFormat df = new SimpleDateFormat(DATE_FORMAT);
		try {
			convertedDate = df.parse(date);
		} catch (Exception ex) {
			OpencertLogger.error(ex);
		}
		
		return convertedDate;
	}
}
