/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import java.util.ArrayList;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.wizards.DawnCreateNewDiagramResourceWizardPage;
import org.eclipse.emf.cdo.dawn.ui.wizards.DawnCreateNewResourceWizardPage;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.opencert.sam.arg.arg.Case;


public class NewContractWizard extends Wizard implements INewWizard {
	private ISelection selection;
	private CDOView view;
	private RequiringSupportWizardPage requiringSupportWizardPage;
	private ProvidingSupportWizardPage providingSupportWizardPage;
	private DawnCreateNewDiagramResourceWizardPage dawnDiagramModelFilePage;
	private DawnCreateNewResourceWizardPage dawnDomainModelFilePage;
	private SelectionJustificationPage selectionJustificationPage;

	public NewContractWizard() {
		super();
		setNeedsProgressMonitor(true);

		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession session = CDOConnectionUtil.instance.getCurrentSession();
		view = CDOConnectionUtil.instance.openView(session);
		
	}
	
	public void addPages() {

		requiringSupportWizardPage = new RequiringSupportWizardPage("Page 1", view);
		requiringSupportWizardPage.setTitle("Select Undeveloped Claim");
		requiringSupportWizardPage.setDescription("Select the cliam from an argumentation model that requires support from other modules by contract");
		addPage(requiringSupportWizardPage);
		
		providingSupportWizardPage = new ProvidingSupportWizardPage("Page 2", view);
		providingSupportWizardPage.setTitle("Select Claims and Evidence");
		providingSupportWizardPage.setDescription("Select the cliams and evidence from argumentation models to support the undevloped claim previously selected");
		addPage(providingSupportWizardPage);
		
		selectionJustificationPage = new SelectionJustificationPage("Page 3");
		selectionJustificationPage.setTitle("Selection Justification");
		selectionJustificationPage.setDescription("Optionally add a justification of how the selected goal(s)/solution(s) satisfy the selected goal requiring support");
		addPage(selectionJustificationPage);
		
		dawnDiagramModelFilePage = new DawnCreateNewDiagramResourceWizardPage(
				"arg_diagram", false, view);
		dawnDiagramModelFilePage
				.setTitle("New Contract Arg Diagram");
		dawnDiagramModelFilePage
				.setDescription("Select the location to store the genertaed contract arg diagram");
		dawnDiagramModelFilePage.setCreateAutomaticResourceName(true);
		addPage(dawnDiagramModelFilePage);

		dawnDomainModelFilePage = new DawnCreateNewResourceWizardPage("arg",
				false, view) {
			@Override
			public void setVisible(boolean visible) {
				if (visible) {
					URI uri = dawnDiagramModelFilePage.getURI();
					String fileName = uri.lastSegment();
					fileName = fileName.substring(0, fileName.length()
							- ".arg_diagram".length()); //$NON-NLS-1$
					fileName += ".arg";
					dawnDomainModelFilePage.setResourceNamePrefix(fileName);
					dawnDomainModelFilePage
							.setResourcePath(dawnDiagramModelFilePage
									.getResourcePath());
				}
				super.setVisible(visible);
			}
		};
		
		dawnDomainModelFilePage
				.setTitle("New Contract Arg Model");
		dawnDomainModelFilePage
				.setDescription("Select a location to store the generated contract arg model");

		dawnDomainModelFilePage
				.setResourceValidationType(DawnCreateNewResourceWizardPage.VALIDATION_WARN);
		addPage(dawnDomainModelFilePage);
		
		
	}

	public boolean performFinish() {
	
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		dialog.open();
		IProgressMonitor monitor = dialog.getProgressMonitor();
		monitor.beginTask("Creating Contract Argumentation Model and Diagram", 20);
		try {			
			doFinish(monitor);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		monitor.worked(1);
		dialog.close();
		return true;
	}

	private void doFinish(IProgressMonitor monitor) throws CoreException {
		
		URI diagramResourceURI = dawnDiagramModelFilePage.getURI();
		URI domainModelResourceURI = dawnDomainModelFilePage.getURI();
		String justification = selectionJustificationPage.getSelectionJustification();	
		ArrayList<ContractModelElement> requiringSupport = new ArrayList<ContractModelElement>();
		ArrayList<ContractModelElement> providingSupport = new ArrayList<ContractModelElement>();
		requiringSupport.addAll(requiringSupportWizardPage.getContractModelElements());
		providingSupport.addAll(providingSupportWizardPage.getContractModelElements());
		ContractModelBuilder builder = new ContractModelBuilder();
		Case assuranceCase = builder.buildContractModel(requiringSupport, providingSupport, justification);
		builder.createDiagram(domainModelResourceURI, diagramResourceURI, monitor, assuranceCase);
		monitor.worked(1);
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
	
	public void dispose() {
		view.close();
	}
	
	public boolean canFinish()
	{
		if(getContainer().getCurrentPage() == dawnDomainModelFilePage)
			return true;
		else 
			return false;
	}
	 
}



