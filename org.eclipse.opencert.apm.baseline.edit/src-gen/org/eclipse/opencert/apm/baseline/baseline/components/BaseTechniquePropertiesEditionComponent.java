/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniquePropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueSelectionPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

// End of user code

/**
 * 
 * 
 */
public class BaseTechniquePropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private BaseTechniquePropertiesEditionPart basePart;

	/**
	 * The BaseTechniqueBasePropertiesEditionComponent sub component
	 * 
	 */
	protected BaseTechniqueBasePropertiesEditionComponent baseTechniqueBasePropertiesEditionComponent;

	/**
	 * The BaseTechniqueEquivalenceMap part
	 * 
	 */
	private BaseTechniqueEquivalenceMapPropertiesEditionPart baseTechniqueEquivalenceMapPart;

	/**
	 * The BaseTechniqueBaseTechniqueEquivalenceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseTechniqueBaseTechniqueEquivalenceMapPropertiesEditionComponent baseTechniqueBaseTechniqueEquivalenceMapPropertiesEditionComponent;

	/**
	 * The BaseTechniqueComplianceMap part
	 * 
	 */
	private BaseTechniqueComplianceMapPropertiesEditionPart baseTechniqueComplianceMapPart;

	/**
	 * The BaseTechniqueBaseTechniqueComplianceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseTechniqueBaseTechniqueComplianceMapPropertiesEditionComponent baseTechniqueBaseTechniqueComplianceMapPropertiesEditionComponent;

	/**
	 * The BaseTechniqueSelection part
	 * 
	 */
	private BaseTechniqueSelectionPropertiesEditionPart baseTechniqueSelectionPart;

	/**
	 * The BaseTechniqueBaseTechniqueSelectionPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseTechniqueBaseTechniqueSelectionPropertiesEditionComponent baseTechniqueBaseTechniqueSelectionPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param baseTechnique the EObject to edit
	 * 
	 */
	public BaseTechniquePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseTechnique, String editing_mode) {
		super(editingContext, editing_mode);
		if (baseTechnique instanceof BaseTechnique) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseTechnique, PropertiesEditingProvider.class);
			baseTechniqueBasePropertiesEditionComponent = (BaseTechniqueBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseTechniqueBasePropertiesEditionComponent.BASE_PART, BaseTechniqueBasePropertiesEditionComponent.class);
			addSubComponent(baseTechniqueBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseTechnique, PropertiesEditingProvider.class);
			baseTechniqueBaseTechniqueEquivalenceMapPropertiesEditionComponent = (BaseTechniqueBaseTechniqueEquivalenceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseTechniqueBaseTechniqueEquivalenceMapPropertiesEditionComponent.BASETECHNIQUEEQUIVALENCEMAP_PART, BaseTechniqueBaseTechniqueEquivalenceMapPropertiesEditionComponent.class);
			addSubComponent(baseTechniqueBaseTechniqueEquivalenceMapPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseTechnique, PropertiesEditingProvider.class);
			baseTechniqueBaseTechniqueComplianceMapPropertiesEditionComponent = (BaseTechniqueBaseTechniqueComplianceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseTechniqueBaseTechniqueComplianceMapPropertiesEditionComponent.BASETECHNIQUECOMPLIANCEMAP_PART, BaseTechniqueBaseTechniqueComplianceMapPropertiesEditionComponent.class);
			addSubComponent(baseTechniqueBaseTechniqueComplianceMapPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseTechnique, PropertiesEditingProvider.class);
			baseTechniqueBaseTechniqueSelectionPropertiesEditionComponent = (BaseTechniqueBaseTechniqueSelectionPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseTechniqueBaseTechniqueSelectionPropertiesEditionComponent.BASETECHNIQUESELECTION_PART, BaseTechniqueBaseTechniqueSelectionPropertiesEditionComponent.class);
			addSubComponent(baseTechniqueBaseTechniqueSelectionPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (BaseTechniqueBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (BaseTechniquePropertiesEditionPart)baseTechniqueBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (BaseTechniqueBaseTechniqueEquivalenceMapPropertiesEditionComponent.BASETECHNIQUEEQUIVALENCEMAP_PART.equals(key)) {
			baseTechniqueEquivalenceMapPart = (BaseTechniqueEquivalenceMapPropertiesEditionPart)baseTechniqueBaseTechniqueEquivalenceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseTechniqueEquivalenceMapPart;
		}
		if (BaseTechniqueBaseTechniqueComplianceMapPropertiesEditionComponent.BASETECHNIQUECOMPLIANCEMAP_PART.equals(key)) {
			baseTechniqueComplianceMapPart = (BaseTechniqueComplianceMapPropertiesEditionPart)baseTechniqueBaseTechniqueComplianceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseTechniqueComplianceMapPart;
		}
		if (BaseTechniqueBaseTechniqueSelectionPropertiesEditionComponent.BASETECHNIQUESELECTION_PART.equals(key)) {
			baseTechniqueSelectionPart = (BaseTechniqueSelectionPropertiesEditionPart)baseTechniqueBaseTechniqueSelectionPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseTechniqueSelectionPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (BaselineViewsRepository.BaseTechnique.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (BaseTechniquePropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseTechniqueEquivalenceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseTechniqueEquivalenceMapPart = (BaseTechniqueEquivalenceMapPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseTechniqueComplianceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseTechniqueComplianceMapPart = (BaseTechniqueComplianceMapPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseTechniqueSelection.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseTechniqueSelectionPart = (BaseTechniqueSelectionPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == BaselineViewsRepository.BaseTechnique.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseTechniqueEquivalenceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseTechniqueComplianceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseTechniqueSelection.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
