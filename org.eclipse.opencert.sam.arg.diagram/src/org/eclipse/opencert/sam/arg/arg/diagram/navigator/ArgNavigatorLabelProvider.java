/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;
import org.eclipse.opencert.sam.arg.arg.AssertedChallenge;
import org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedChallengeEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedContextCardinalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedContextEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedCounterEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedEvidenceCardinalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedInferenceCardinalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedInferenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.CaseEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.Choice2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceOptionality2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceOptionalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorPlugin;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgParserProvider;

/**
 * @generated
 */
public class ArgNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		ArgDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		ArgDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof ArgNavigatorItem
				&& !isOwnView(((ArgNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof ArgNavigatorGroup) {
			ArgNavigatorGroup group = (ArgNavigatorGroup) element;
			return ArgDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof ArgNavigatorItem) {
			ArgNavigatorItem navigatorItem = (ArgNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getImage(view);
			}
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (ArgVisualIDRegistry.getVisualID(view)) {
		case CaseEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?arg?Case", ArgElementTypes.Case_1000); //$NON-NLS-1$
		case ClaimEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?arg?Claim", ArgElementTypes.Claim_2001); //$NON-NLS-1$
		case AgreementEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?arg?Agreement", ArgElementTypes.Agreement_2002); //$NON-NLS-1$
		case ArgumentReasoningEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?arg?ArgumentReasoning", ArgElementTypes.ArgumentReasoning_2003); //$NON-NLS-1$
		case ArgumentationEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?arg?Argumentation", ArgElementTypes.Argumentation_2004); //$NON-NLS-1$
		case InformationElementCitationEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?arg?InformationElementCitation", ArgElementTypes.InformationElementCitation_2005); //$NON-NLS-1$
		case ArgumentElementCitationEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?arg?ArgumentElementCitation", ArgElementTypes.ArgumentElementCitation_2006); //$NON-NLS-1$
		case ChoiceEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?arg?Choice", ArgElementTypes.Choice_2007); //$NON-NLS-1$
		case Choice2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?arg?Choice", ArgElementTypes.Choice_3001); //$NON-NLS-1$
		case AssertedInferenceEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?arg?AssertedInference", ArgElementTypes.AssertedInference_4001); //$NON-NLS-1$
		case AssertedEvidenceEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?arg?AssertedEvidence", ArgElementTypes.AssertedEvidence_4002); //$NON-NLS-1$
		case AssertedContextEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?arg?AssertedContext", ArgElementTypes.AssertedContext_4003); //$NON-NLS-1$
		case AssertedChallengeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?arg?AssertedChallenge", ArgElementTypes.AssertedChallenge_4004); //$NON-NLS-1$
		case AssertedCounterEvidenceEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?arg?AssertedCounterEvidence", ArgElementTypes.AssertedCounterEvidence_4005); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = ArgDiagramEditorPlugin.getInstance()
				.getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& ArgElementTypes.isKnownElementType(elementType)) {
			image = ArgElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof ArgNavigatorGroup) {
			ArgNavigatorGroup group = (ArgNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof ArgNavigatorItem) {
			ArgNavigatorItem navigatorItem = (ArgNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getText(view);
			}
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (ArgVisualIDRegistry.getVisualID(view)) {
		case CaseEditPart.VISUAL_ID:
			return getCase_1000Text(view);
		case ClaimEditPart.VISUAL_ID:
			return getClaim_2001Text(view);
		case AgreementEditPart.VISUAL_ID:
			return getAgreement_2002Text(view);
		case ArgumentReasoningEditPart.VISUAL_ID:
			return getArgumentReasoning_2003Text(view);
		case ArgumentationEditPart.VISUAL_ID:
			return getArgumentation_2004Text(view);
		case InformationElementCitationEditPart.VISUAL_ID:
			return getInformationElementCitation_2005Text(view);
		case ArgumentElementCitationEditPart.VISUAL_ID:
			return getArgumentElementCitation_2006Text(view);
		case ChoiceEditPart.VISUAL_ID:
			return getChoice_2007Text(view);
		case Choice2EditPart.VISUAL_ID:
			return getChoice_3001Text(view);
		case AssertedInferenceEditPart.VISUAL_ID:
			return getAssertedInference_4001Text(view);
		case AssertedEvidenceEditPart.VISUAL_ID:
			return getAssertedEvidence_4002Text(view);
		case AssertedContextEditPart.VISUAL_ID:
			return getAssertedContext_4003Text(view);
		case AssertedChallengeEditPart.VISUAL_ID:
			return getAssertedChallenge_4004Text(view);
		case AssertedCounterEvidenceEditPart.VISUAL_ID:
			return getAssertedCounterEvidence_4005Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getCase_1000Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getClaim_2001Text(View view) {
		IParser parser = ArgParserProvider.getParser(
				ArgElementTypes.Claim_2001,
				view.getElement() != null ? view.getElement() : view,
				ArgVisualIDRegistry.getType(ClaimIdEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAgreement_2002Text(View view) {
		IParser parser = ArgParserProvider.getParser(
				ArgElementTypes.Agreement_2002,
				view.getElement() != null ? view.getElement() : view,
				ArgVisualIDRegistry.getType(AgreementIdEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getArgumentReasoning_2003Text(View view) {
		IParser parser = ArgParserProvider.getParser(
				ArgElementTypes.ArgumentReasoning_2003,
				view.getElement() != null ? view.getElement() : view,
				ArgVisualIDRegistry
						.getType(ArgumentReasoningIdEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5007); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getArgumentation_2004Text(View view) {
		IParser parser = ArgParserProvider.getParser(
				ArgElementTypes.Argumentation_2004,
				view.getElement() != null ? view.getElement() : view,
				ArgVisualIDRegistry.getType(ArgumentationIdEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getInformationElementCitation_2005Text(View view) {
		IParser parser = ArgParserProvider
				.getParser(
						ArgElementTypes.InformationElementCitation_2005,
						view.getElement() != null ? view.getElement() : view,
						ArgVisualIDRegistry
								.getType(InformationElementCitationIdEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5011); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getArgumentElementCitation_2006Text(View view) {
		IParser parser = ArgParserProvider.getParser(
				ArgElementTypes.ArgumentElementCitation_2006,
				view.getElement() != null ? view.getElement() : view,
				ArgVisualIDRegistry
						.getType(ArgumentElementCitationIdEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5013); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getChoice_2007Text(View view) {
		IParser parser = ArgParserProvider.getParser(
				ArgElementTypes.Choice_2007,
				view.getElement() != null ? view.getElement() : view,
				ArgVisualIDRegistry
						.getType(ChoiceOptionalityEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5016); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getChoice_3001Text(View view) {
		IParser parser = ArgParserProvider.getParser(
				ArgElementTypes.Choice_3001,
				view.getElement() != null ? view.getElement() : view,
				ArgVisualIDRegistry
						.getType(ChoiceOptionality2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAssertedInference_4001Text(View view) {
		IParser parser = ArgParserProvider
				.getParser(
						ArgElementTypes.AssertedInference_4001,
						view.getElement() != null ? view.getElement() : view,
						ArgVisualIDRegistry
								.getType(AssertedInferenceCardinalityEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAssertedEvidence_4002Text(View view) {
		IParser parser = ArgParserProvider
				.getParser(
						ArgElementTypes.AssertedEvidence_4002,
						view.getElement() != null ? view.getElement() : view,
						ArgVisualIDRegistry
								.getType(AssertedEvidenceCardinalityEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAssertedContext_4003Text(View view) {
		IParser parser = ArgParserProvider.getParser(
				ArgElementTypes.AssertedContext_4003,
				view.getElement() != null ? view.getElement() : view,
				ArgVisualIDRegistry
						.getType(AssertedContextCardinalityEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAssertedChallenge_4004Text(View view) {
		AssertedChallenge domainModelElement = (AssertedChallenge) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 4004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAssertedCounterEvidence_4005Text(View view) {
		AssertedCounterEvidence domainModelElement = (AssertedCounterEvidence) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			ArgDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 4005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return CaseEditPart.MODEL_ID.equals(ArgVisualIDRegistry
				.getModelID(view));
	}

}
