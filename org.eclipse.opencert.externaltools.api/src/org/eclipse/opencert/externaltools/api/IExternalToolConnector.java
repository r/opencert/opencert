/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.externaltools.api;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface IExternalToolConnector
{
    // Basic common properties:
    String getExternalToolConnectorId();
    String getName();
    String getDescription();
    
    // Custom properties
    List<String> getSettingKeys();
    Map<String, String> getSettingDefaultValues();

    // Result processing settings
    Integer[] getDefaultResultRanges();
    String[] getDefaultResultMsgs();

    // Initialization methods
    ExternalToolQuery bootstrapExternalToolQueryInstance();

    // Service methods
    int connectAndProcessResponse(ExternalToolQuery externalToolQuery) throws IOException;
}
