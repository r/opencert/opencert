/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.search;

import java.util.Iterator;

import org.apache.commons.codec.digest.DigestUtils;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * Generic {@link ObjectResolver} that tries to resolve against the object URI.
 * 
 * @author mauersberger
 *
 */
public class EMFObjectResolver implements ObjectResolver<EObject> {

	@Override
	public EObject resolve(String objectId, Object context) {
		if (context instanceof Resource) {
			context = ((Resource) context).getAllContents();
		}

		if (context instanceof Iterator) {
			@SuppressWarnings("unchecked")
			Iterator<Object> it = (Iterator<Object>) context;
			while (it.hasNext()) {
				Object next = (Object) it.next();
				if (next instanceof EObject) {
					EObject object = (EObject) next;
					Resource resource = object.eResource();
					if (resource != null && objectId.equals(DigestUtils.md5Hex(resource.getURIFragment(object)))) {
						return object;
					}
				}
			}
		} else if (context instanceof Iterable) {
			for (Object next : (Iterable<?>) context) {
				if (next instanceof EObject) {
					EObject object = (EObject) next;
					Resource resource = object.eResource();
					if (resource != null && objectId.equals(DigestUtils.md5Hex(resource.getURIFragment(object)))) {
						return object;
					}
				}
			}
		}

		// TODO Auto-generated method stub
		return null;
	}
}