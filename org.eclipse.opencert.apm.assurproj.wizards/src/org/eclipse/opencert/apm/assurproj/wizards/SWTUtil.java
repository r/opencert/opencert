/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards;

import java.util.Collection;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.osgi.framework.Bundle;

public class SWTUtil {

	public static Button createButton(Composite parent, String caption, int buttonStyle, int style) {
		Button button = new Button(parent, buttonStyle);
		GridData gridData = new GridData(style);
		button.setLayoutData(gridData);
		button.setText(caption);

		return button;
	}
	
	public static Button createButton(Composite parent, Image image) {
		Button button = new Button(parent, SWT.NONE);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		button.setLayoutData(gridData);
		button.setImage(image);

		return button;
	}

	public static Button createButton(Composite parent, String caption) {
		Button button = new Button(parent, SWT.NONE);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		button.setLayoutData(gridData);
		button.setText(caption);

		return button;
	}

	public static void colorComposite(Composite composite, int color) {
		composite.setBackground(composite.getShell().getDisplay().getSystemColor(color));
	}

	public static Button createButton(Composite parent, String caption, int style) {
		Button button = new Button(parent, SWT.NONE);
		GridData gridData = new GridData(style);
		button.setLayoutData(gridData);
		button.setText(caption);

		return button;
	}
	
	public static Button createCheckButton(Composite parent, String caption, int style) {
		Button button = new Button(parent, SWT.CHECK);
		GridData gridData = new GridData(style);
		button.setLayoutData(gridData);
		button.setText(caption);

		return button;
	}
	
	public static Button createCheckButton(Composite parent, String caption, int buttonStyle, int style) {
		Button button = new Button(parent, buttonStyle | SWT.CHECK);
		GridData gridData = new GridData(style);
		button.setLayoutData(gridData);
		button.setText(caption);

		return button;
	}
	
	public static Text createText(Composite parent, int width) {
		Text text = new Text(parent, SWT.BORDER);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.widthHint = width;
		text.setLayoutData(data);
		text.setFont(parent.getFont());

		return text;
	}

	public static Text createText(Composite parent) {
		Text text = new Text(parent, SWT.BORDER);
		text.setFont(parent.getFont());
		return text;
	}

	public static Label createLabel(Composite parent, String caption) {
		Label label = new Label(parent, SWT.WRAP);
		label.setText(caption);

		return label;
	}

	public static Label createLabel(Composite parent, String caption, int style) {
		Label label;
		label = new Label(parent, style);
		label.setText(caption);

		return label;
	}

	public static Label createLabel(Composite parent, Image image) {
		Label label;
		label = new Label(parent, SWT.LEFT);
		label.setImage(image);
		label.setLayoutData(new GridData(GridData.FILL_BOTH));
		return label;
	}

	public static Group createGroup(Composite parent, int cols, String caption) {
		Group group = new Group(parent, SWT.NULL);
		group.setLayout(new GridLayout(cols, false));
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		group.setText(caption);

		return group;
	}

	public static Group createGroup(Composite parent, int cols, String caption, int style) {
		Group group = new Group(parent, SWT.NULL);
		group.setLayout(new GridLayout(cols, false));
		group.setLayoutData(new GridData(style));
		group.setText(caption);

		return group;
	}

	public static Composite createComposite(Composite parent, int cols) {
		Composite composite = new Composite(parent, SWT.None);
		GridLayout layout = new GridLayout(cols, false);
		composite.setLayout(layout);

		return composite;
	}

	public static Composite createComposite(Composite parent, int cols, int style) {
		Composite composite = new Composite(parent, SWT.None);
		GridData gridData = new GridData(style);
		composite.setLayout(new GridLayout(cols, false));
		composite.setLayoutData(gridData);

		return composite;
	}

	public static ScrolledComposite createScrolledComposite(Composite parent, int cols) {
		ScrolledComposite composite = new ScrolledComposite(parent, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		composite.setLayout(new GridLayout(cols, false));

		return composite;
	}

	public static Combo createCombo(Composite parent) {
		return new Combo(parent, SWT.DROP_DOWN | SWT.READ_ONLY);
	}

	public static Dialog createDialogWithTreeViewer(
		final Shell shell,
		final String labelString,
		final ViewerFilter filter,
		final AdapterFactoryContentProvider contentProvider,
		final AdapterFactoryLabelProvider labelProvider,
		final Object input,
		final Collection<EObject> selection) {
		return new Dialog(shell) {

			@Override
			protected Control createDialogArea(final Composite parent) {
				Composite root = (Composite) super.createDialogArea(parent);
				Composite composite = new Composite(root, SWT.NONE);
				composite.setLayout(new GridLayout());
				GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
				gridData.widthHint = 500;
				gridData.heightHint = 300;
				composite.setLayoutData(gridData);

				if ((labelString instanceof String) && labelString.length() > 0) {
					Label label = new Label(composite, SWT.WRAP);
					label.setText(labelString);
					GridData data = new GridData(GridData.FILL_HORIZONTAL);
					label.setLayoutData(data);
				}

				Tree tree = new Tree(composite, SWT.BORDER | SWT.MULTI);
				tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
				TreeViewer treeViewer = new TreeViewer(tree);
				treeViewer.setContentProvider(contentProvider);
				treeViewer.setLabelProvider(labelProvider);
				treeViewer.setInput(input);
				if (filter != null) {
					treeViewer.addFilter(filter);
				}
				if (selection != null) {
					treeViewer.setSelection(new StructuredSelection(selection.toArray()),
						true);
				}
				treeViewer.expandAll();
				return root;
			}

		};

	}

	public static void setEnableCompositeAndAllChildren(Composite composite, boolean enable) {
		composite.setEnabled(enable);
		for (Control control : composite.getChildren()) {
			control.setEnabled(enable);
			if (control instanceof Composite) {
				setEnableCompositeAndAllChildren((Composite) control,
					enable);
			}
		}
	}

//	public static void refreshControl(Composite constraintComposite) {
//		Point size = constraintComposite.getSize();
//		constraintComposite.setSize(constraintComposite.computeSize(size.x - 1,
//			size.y - 1));
//		constraintComposite.setSize(constraintComposite.computeSize(size.x,
//			size.y));
//
//	}

	public static Image captureImage(String pluginId, String projectRelativeImagePath) {
		Bundle bundle = Platform.getBundle(pluginId);
		ImageDescriptor imageDescriptor = ImageDescriptor.createFromURL(bundle.getEntry(projectRelativeImagePath));
		return imageDescriptor.createImage();
	}

	public static final void setBoldFont(Control control) {
		// Font font = label.getFont();
		FontData[] fontDatas = control.getFont().getFontData();
		for (FontData fontData : fontDatas) {
			fontData.setStyle(SWT.BOLD);
		}
		Font newFont = new Font(control.getShell().getDisplay(), fontDatas);
		control.setFont(newFont);
	}

	public static final void setItalicFont(Control control) {
		// Font font = label.getFont();
		FontData[] fontDatas = control.getFont().getFontData();
		for (FontData fontData : fontDatas) {
			fontData.setStyle(SWT.ITALIC);
		}
		Font newFont = new Font(control.getShell().getDisplay(), fontDatas);
		control.setFont(newFont);
	}
}
