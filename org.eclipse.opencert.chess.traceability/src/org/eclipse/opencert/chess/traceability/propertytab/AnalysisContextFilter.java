/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.opencert.chess.traceability.propertytab;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IFilter;
//import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EObjectTreeElement;
import org.eclipse.papyrus.uml.diagram.clazz.edit.parts.ComponentEditPart;
import org.eclipse.uml2.uml.Class;
import org.polarsys.chess.chessmlprofile.util.Constants;


public class AnalysisContextFilter implements IFilter {
	
	public static final String GA_ANALYSIS_CONTEXT = "MARTE::MARTE_AnalysisModel::GaAnalysisContext";
	
	@Override
	public boolean select(Object toTest) {	
		return isSelectionOk(toTest);
	}
	
	public static boolean isSelectionOk(Object selection) {
		if(selection instanceof ComponentEditPart){
		
			Class clazz = (Class) (((ComponentEditPart)selection).resolveSemanticElement());
			
			if(clazz.getAppliedStereotype(GA_ANALYSIS_CONTEXT) != null ||
				clazz.getAppliedStereotype(org.polarsys.chess.contracts.profile.chesscontract.util.Constants.ContractRefinementAnalysis)!= null ||
				clazz.getAppliedStereotype(Constants.STATEBASED_ANALYSIS)!= null ||
				clazz.getAppliedStereotype(Constants.MARTE_SaAnalysisContext)!= null ||
				clazz.getAppliedStereotype(Constants.FLA_ANALYSIS)!= null
					){
	
				return true;
			}
		}
		return false;
	}

}
