/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.search;

import java.util.Map;
import java.util.Set;

/**
 * Access interface to start search queries against Elastic.
 *
 * @author mauersberger
 */
public interface ElasticFinder {

	/**
	 * Search all indexes with the given query and additional attribute filters that
	 * must match.
	 * 
	 * @param query
	 *            string following the Elastic query syntax (see
	 *            https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html)
	 * @param filters
	 *            a set of filters, might be empty or <code>null</code>
	 * @return zero or more hits, never <code>null</code>
	 */
	Set<Hit> search(String query, Map<String, Object> filters);

	/**
	 * Limit this finder to the given index.
	 * 
	 * @param index
	 *            name of the index, or "_all" or <code>null</code> for all indexes
	 * @return this
	 */
	ElasticFinder within(String index);
}
