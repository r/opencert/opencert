/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.test;

import java.util.Date;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.Organization;
import org.eclipse.opencert.pam.procspec.process.Person;
import org.eclipse.opencert.pam.procspec.process.ProcessFactory;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.Case;

public class SampleDataProvider
{
    private static final int ACTIVITIES_PER_PERSON = 1;
    private static final int PEOPLE_PER_ORGANIZATION = 1;
    private static final int ORGANIZATIONS_COUNT = 1;
    private static final int PART_ARTEFACTS_COUNT = 1;
    private static final int COUNT = 20000;
    
    public static void populateWithArtefactHierarchy(Resource resource)
    {
        int artefactId = 20000;
        int artefactRelId = 20000;
        
        ArtefactModel artefactModel = EvidenceFactory.eINSTANCE.createArtefactModel();
        artefactModel.setId("id_100000");
        artefactModel.setName("artefactModel with " + artefactModel.getId());
        ArtefactDefinition artefactDef = EvidenceFactory.eINSTANCE.createArtefactDefinition();
        
        ProcessModel processModel = ProcessFactory.eINSTANCE.createProcessModel();
        processModel.setId("id_100000");
        processModel.setName("processModel with " + processModel.getId());
 
        for (int organizationNr = 0 + COUNT; organizationNr < ORGANIZATIONS_COUNT + COUNT; organizationNr++) {
            
            Organization organization = ProcessFactory.eINSTANCE
                    .createOrganization();
            organization.setId("id_"+organizationNr);
            organization.setAddress("addressOfOrganization"
                    + organizationNr);
            
            
            for (int personNr = 0 + COUNT; personNr < PEOPLE_PER_ORGANIZATION + COUNT; personNr++) {
                
                Person person = ProcessFactory.eINSTANCE.createPerson();
                person.setId("id_" + personNr);
                person.setEmail("email" + personNr + "@organization"
                        + organizationNr);
                
                person.getOrganization().add(organization);
                
                for (int activityNr = 0; activityNr < ACTIVITIES_PER_PERSON; activityNr++) {
                    //Create Artefact:
                    Artefact artefact = DataCreationRoutines.createArtefact("artefact with ", artefactId++);
                    
                    //Create ArtefactPart
                    for (int partArtefactNr = 0; partArtefactNr < PART_ARTEFACTS_COUNT; partArtefactNr++) {
                        Artefact artefactPart = DataCreationRoutines.createArtefact("artefactPart with ", artefactId++);
                        resource.getContents().add(artefactPart);
                        artefact.getArtefactPart().add(artefactPart);
                    }
                    
                    resource.getContents().add(artefact);
                               
                    for (int k=0; k<2; k++) {
                        Artefact sourceArtefact = DataCreationRoutines.createArtefact("sourceArtefact with ", artefactId++);
                        Artefact targetArtefact = DataCreationRoutines.createArtefact("targetArtefact with ", artefactId++);
                        
                        //Create ArtefactRel:
                        ArtefactRel artefactRel = EvidenceFactory.eINSTANCE.createArtefactRel();
                        artefactRel.setId("id_" + artefactRelId++);
                        artefactRel.setName("artefactRel with " + artefactRel.getId());
                        artefactRel.setModificationEffect(ChangeEffectKind.MODIFY);
                        artefactRel.setSource(sourceArtefact);
                        //artefactRel.setSource(artefact);
                        artefactRel.setTarget(targetArtefact);
                        
                        resource.getContents().add(sourceArtefact);
                        resource.getContents().add(targetArtefact);
                        resource.getContents().add(artefactRel);
                        
                        //Add artefactRel to Artefact 
                        artefact.getOwnedRel().add(artefactRel);
                    }

                    
                    //Create Activity:
                    Activity activity = ProcessFactory.eINSTANCE
                            .createActivity();
                    activity.setStartTime(new Date());
                    activity.getProducedArtefact().add(artefact);
                    activity.setEndTime(new Date());
                    activity.getParticipant().add(person);
                    
                    resource.getContents().add(activity);
                    
                    //Add activity objects to ProcessModel
                    processModel.getOwnedActivity().add(activity);
                    //Add artefact objects to ArtefactDefinition
                    artefactDef.getArtefact().add(artefact);
                }
                
                artefactModel.getArtefact().add(artefactDef);
                resource.getContents().add(person);
            }
            
            resource.getContents().add(organization);
        }
        resource.getContents().add(artefactDef);
        resource.getContents().add(artefactModel);
        resource.getContents().add(processModel);
    }
    
    public static void populateAssuranceProject(Resource resource)
    {           
        AssuranceProject assuranceProject = AssuranceprojectFactory.eINSTANCE.createAssuranceProject();
        assuranceProject.setId("AP1");
        assuranceProject.setDescription("AP1");
        assuranceProject.setName("AP1");
        
        AssetsPackage assetsPackage = AssuranceprojectFactory.eINSTANCE.createAssetsPackage();
        assetsPackage.setIsActive(true);
        
        assuranceProject.getAssetsPackage().add(assetsPackage);

//        BaseFramework baselineFramework = BaselineFactory.eINSTANCE.createBaseFramework();
//        baselineFramework.setId("Empty1");
//        baselineFramework.setDescription("Empty1");
//        baselineFramework.setName("Empty1");
        
        BaselineConfig baselineConfig = AssuranceprojectFactory.eINSTANCE.createBaselineConfig();
        baselineConfig.setId("BC1");
        baselineConfig.setDescription("BC1");
        baselineConfig.setName("BC1");
        baselineConfig.setIsActive(true);     
        
        resource.getContents().add(baselineConfig); 
        assuranceProject.getBaselineConfig().add(baselineConfig);
           
        //resource.getContents().add(baselineFramework);  
        //assuranceProject.getBaselineConfig().get(0).getRefFramework().add(baselineFramework);

        resource.getContents().add(assuranceProject);
    }    
    
    
    public static void populateArg(Resource resource)
    {           
        Case aCase = ArgFactory.eINSTANCE.createCase();
        
        resource.getContents().add(aCase);
    }
    
}

