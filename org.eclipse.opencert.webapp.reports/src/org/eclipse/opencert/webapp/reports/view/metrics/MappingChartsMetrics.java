/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.webapp.reports.manager.BaselineManager;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Container.Hierarchical;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;




@Theme("opencerttheme")
public class MappingChartsMetrics 
	extends CustomComponent

{

	private static final long serialVersionUID = 5584494413107586824L;
	private static final String COMPLIANCE_MAPS = "Compliance Maps";
    private static final String NO_DATA_AVAILABLE = "No Data Available";
    
    private byte[] complianceMaps;
    private Hierarchical dataDescription;

    
    protected class ComplianceInfo{
    	protected String id;
    	protected String name;
    	protected List<String> fullMaps;
    	protected List<String> partialMaps;
    	protected List<String> noMaps;
    	
    	private ComplianceInfo(String id, String name){
    		this.id=id;
    		this.name=name;
    		fullMaps=new ArrayList<String>();
    		partialMaps=new ArrayList<String>();
    		noMaps=new ArrayList<String>();	
    	}

    	protected String getId() {
			return id;
		}

		protected String getName() {
			return name;
		}

		protected List<String> getFullMaps() {
			return fullMaps;
		}
		
		protected void addFullMap(String info){
			fullMaps.add(info);
		}

		protected List<String> getPartialMaps() {
			return partialMaps;
		}
		
		protected void addPartialMap(String info){
			partialMaps.add(info);
		}

		protected List<String> getNoMaps() {
			return noMaps;
		}
		
		protected void addNoMap(String info){
			noMaps.add(info);
		}
	
    	
    }
    
    protected class Triplet{
    	protected int full;
    	protected int partial;
    	protected int noMap;
    	protected List<ComplianceInfo> data;
    	
    	protected Triplet(){
    		this.full=0;
    		this.partial=0;
    		this.noMap=0;
    		data=new ArrayList<ComplianceInfo>();
    	}

    	protected int getFull() {
			return full;
		}

    	protected void setFull(int full) {
			this.full = full;
		}

    	protected int getPartial() {
			return partial;
		}

    	protected void setPartial(int partial) {
			this.partial = partial;
		}

    	protected int getNoMap() {
			return noMap;
		}

    	protected void setNoMap(int noMap) {
			this.noMap = noMap;
		}
    	
    	protected List<ComplianceInfo> getData(){
    		return data;
    	}
    	
    	protected void addData(ComplianceInfo e){
    		data.add(e);
    	}
    	
    }
    
                      
    public MappingChartsMetrics(long baselineFrameworkID)
    {      
         
        VerticalLayout mainPanel = new VerticalLayout();
		
		mainPanel.setStyleName("chartsSpacing");
		mainPanel.setSpacing(true);
		
				
		BaselineManager baselineManager = (BaselineManager)SpringContextHelper.getBeanFromWebappContext(BaselineManager.SPRING_NAME);
		
		Triplet req = countMapRequirements(baselineManager.getBaseRequirements(baselineFrameworkID,true,true));
		Triplet art = countMapArtefacts(baselineManager.getBaseArtefacts(baselineFrameworkID,true));
		Triplet roles = countMapRoles(baselineManager.getBaseRoles(baselineFrameworkID,true));
		Triplet tech = countMapTechniques(baselineManager.getBaseTechniques(baselineFrameworkID,true));
		Triplet act = countMapActivities(baselineManager.getBaseActivities(baselineFrameworkID,true,true));
		
		mainPanel.addComponent(newDataSet(populateWithData(req,art,roles,tech,act)));
		mainPanel.addComponent(newTableInfo(req,art,roles,tech,act));
		
		//add mainPanel component
		setCompositionRoot(mainPanel);
		setSizeFull();
        
 
    }

  
    private Component newTableInfo(Triplet req, Triplet art, Triplet rol, Triplet tech, Triplet act) {
    	
    	final TreeTable treetable = new TreeTable();
		treetable.setWidth("100%");
		treetable.addStyleName("resourceEfficiencyTable");

		// Define two columns for the built-in container
		treetable.addContainerProperty("Maps", String.class, "");
		treetable.addContainerProperty("Description",  String.class, "");
		
		Object requirement = treetable.addItem(new Object[]{"Requirements: ", ""},null);
		Object artefacts = treetable.addItem(new Object[]{"Artefacts: ", ""},null);
		Object roles = treetable.addItem(new Object[]{"Roles: ", ""},null);
		Object techniques = treetable.addItem(new Object[]{"Techniques: ", ""},null);
		Object activities = treetable.addItem(new Object[]{"Activities: ", ""},null);
		
		treetable.setCollapsed(requirement, false);
		treetable.setCollapsed(artefacts, false);
		treetable.setCollapsed(roles, false);
		treetable.setCollapsed(techniques, false);
		treetable.setCollapsed(activities, false);
		
		List<ComplianceInfo> reqInfo = req.getData();
		if(!reqInfo.isEmpty()){
			for(ComplianceInfo inf : reqInfo){
				Object r = treetable.addItem(new Object[]{"ID: " + inf.getId() + " Name: " + inf.getName(), ""},null);
				treetable.setParent(r,requirement);
				treetable.setCollapsed(r, false);
				
				Object fullMaps = treetable.addItem(new Object[]{"Full Maps:", ""},null);
				treetable.setParent(fullMaps,r);
				treetable.setCollapsed(fullMaps, false);
				List<String> fm = inf.getFullMaps();
				if(!fm.isEmpty()){
					for(String s : fm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,fullMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
				Object partialMaps = treetable.addItem(new Object[]{"Partial Maps:", ""},null);
				treetable.setParent(partialMaps,r);
				treetable.setCollapsed(partialMaps, false);
				List<String> pm = inf.getPartialMaps();
				if(!pm.isEmpty()){
					for(String s : pm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,partialMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
				Object noMaps = treetable.addItem(new Object[]{"No Maps:", ""},null);
				treetable.setParent(noMaps,r);
				treetable.setCollapsed(noMaps, false);
				List<String> nm = inf.getNoMaps();
				if(!nm.isEmpty()){
					for(String s : nm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,noMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
			}
		}
		
		List<ComplianceInfo> artInfo = art.getData();
		if(!artInfo.isEmpty()){
			for(ComplianceInfo inf : artInfo){
				Object r = treetable.addItem(new Object[]{"ID: " + inf.getId() + " Name: " + inf.getName(), ""},null);
				treetable.setParent(r,artefacts);
				treetable.setCollapsed(r, false);
				
				Object fullMaps = treetable.addItem(new Object[]{"Full Maps:", ""},null);
				treetable.setParent(fullMaps,r);
				treetable.setCollapsed(fullMaps, false);
				List<String> fm = inf.getFullMaps();
				if(!fm.isEmpty()){
					for(String s : fm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,fullMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
				Object partialMaps = treetable.addItem(new Object[]{"Partial Maps:", ""},null);
				treetable.setParent(partialMaps,r);
				treetable.setCollapsed(partialMaps, false);
				List<String> pm = inf.getPartialMaps();
				if(!pm.isEmpty()){
					for(String s : pm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,partialMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
				Object noMaps = treetable.addItem(new Object[]{"No Maps:", ""},null);
				treetable.setParent(noMaps,r);
				treetable.setCollapsed(noMaps, false);
				List<String> nm = inf.getNoMaps();
				if(!nm.isEmpty()){
					for(String s : nm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,noMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
			}
		}
		
		
		List<ComplianceInfo> rolInfo = rol.getData();
		if(!rolInfo.isEmpty()){
			for(ComplianceInfo inf : rolInfo){
				Object r = treetable.addItem(new Object[]{"ID: " + inf.getId() + " Name: " + inf.getName(), ""},null);
				treetable.setParent(r,roles);
				treetable.setCollapsed(r, false);
				
				Object fullMaps = treetable.addItem(new Object[]{"Full Maps:", ""},null);
				treetable.setParent(fullMaps,r);
				treetable.setCollapsed(fullMaps, false);
				List<String> fm = inf.getFullMaps();
				if(!fm.isEmpty()){
					for(String s : fm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,fullMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
				Object partialMaps = treetable.addItem(new Object[]{"Partial Maps:", ""},null);
				treetable.setParent(partialMaps,r);
				treetable.setCollapsed(partialMaps, false);
				List<String> pm = inf.getPartialMaps();
				if(!pm.isEmpty()){
					for(String s : pm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,partialMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
				Object noMaps = treetable.addItem(new Object[]{"No Maps:", ""},null);
				treetable.setParent(noMaps,r);
				treetable.setCollapsed(noMaps, false);
				List<String> nm = inf.getNoMaps();
				if(!nm.isEmpty()){
					for(String s : nm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,noMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
			}
		}
		
		List<ComplianceInfo> techInfo = tech.getData();
		if(!techInfo.isEmpty()){
			for(ComplianceInfo inf : techInfo){
				Object r = treetable.addItem(new Object[]{"ID: " + inf.getId() + " Name: " + inf.getName(), ""},null);
				treetable.setParent(r,techniques);
				treetable.setCollapsed(r, false);
				
				Object fullMaps = treetable.addItem(new Object[]{"Full Maps:", ""},null);
				treetable.setParent(fullMaps,r);
				treetable.setCollapsed(fullMaps, false);
				List<String> fm = inf.getFullMaps();
				if(!fm.isEmpty()){
					for(String s : fm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,fullMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
				Object partialMaps = treetable.addItem(new Object[]{"Partial Maps:", ""},null);
				treetable.setParent(partialMaps,r);
				treetable.setCollapsed(partialMaps, false);
				List<String> pm = inf.getPartialMaps();
				if(!pm.isEmpty()){
					for(String s : pm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,partialMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
				Object noMaps = treetable.addItem(new Object[]{"No Maps:", ""},null);
				treetable.setParent(noMaps,r);
				treetable.setCollapsed(noMaps, false);
				List<String> nm = inf.getNoMaps();
				if(!nm.isEmpty()){
					for(String s : nm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,noMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
			}
		}
		
		List<ComplianceInfo> actInfo = act.getData();
		if(!actInfo.isEmpty()){
			for(ComplianceInfo inf : actInfo){
				Object r = treetable.addItem(new Object[]{"ID: " + inf.getId() + " Name: " + inf.getName(), ""},null);
				treetable.setParent(r,activities);
				treetable.setCollapsed(r, false);
				
				Object fullMaps = treetable.addItem(new Object[]{"Full Maps:", ""},null);
				treetable.setParent(fullMaps,r);
				treetable.setCollapsed(fullMaps, false);
				List<String> fm = inf.getFullMaps();
				if(!fm.isEmpty()){
					for(String s : fm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,fullMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
				Object partialMaps = treetable.addItem(new Object[]{"Partial Maps:", ""},null);
				treetable.setParent(partialMaps,r);
				treetable.setCollapsed(partialMaps, false);
				List<String> pm = inf.getPartialMaps();
				if(!pm.isEmpty()){
					for(String s : pm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,partialMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
				Object noMaps = treetable.addItem(new Object[]{"No Maps:", ""},null);
				treetable.setParent(noMaps,r);
				treetable.setCollapsed(noMaps, false);
				List<String> nm = inf.getNoMaps();
				if(!nm.isEmpty()){
					for(String s : nm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,noMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}
				
			}
		}
		
		dataDescription = treetable.getContainerDataSource();
		return treetable;
	}


	private Component newDataSet(final DefaultCategoryDataset dataset) {

    	 
    	
    	final JFreeChart chart = ChartFactory.createBarChart(
    			COMPLIANCE_MAPS,      // chart title
                "Type of Compliance Map",               // domain axis label
                "Number of Maps", // range axis label
                dataset,                  // data
                PlotOrientation.VERTICAL, // orientation
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
            );

    	// get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED);  
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
      
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
        renderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));
        

        final CategoryItemRenderer categoryRenderer = plot.getRenderer();
        categoryRenderer.setBaseItemLabelsVisible(true);
        categoryRenderer.setSeriesPaint(0, ChartColor.LIGHT_GREEN);
        categoryRenderer.setSeriesPaint(1, ChartColor.LIGHT_YELLOW);
        categoryRenderer.setSeriesPaint(2, ChartColor.LIGHT_RED);

        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
		
       
        try {
			complianceMaps = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		Component chartWrapper = new JFreeChartWrapper(chart);
		chartWrapper.setHeight("80%");
		chartWrapper.setWidth("80%");
		chartWrapper.setStyleName("leftChartsSpacing");
		return chartWrapper;
	
    	
    	
	}


	private DefaultCategoryDataset populateWithData(Triplet requirements,
			Triplet artefacts, Triplet roles, Triplet techniques,
			Triplet activities) {
		
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    	dataset.addValue(requirements.getFull(), "Full Map", "Requirements");
    	dataset.addValue(requirements.getPartial(), "Partial Map", "Requirements");
    	dataset.addValue(requirements.getNoMap(), "No Map", "Requirements");

    	dataset.addValue(artefacts.getFull(), "Full Map", "Artefacts");
    	dataset.addValue(artefacts.getPartial(), "Partial Map", "Artefacts");
    	dataset.addValue(artefacts.getNoMap(), "No Map", "Artefacts");
    	
    	dataset.addValue(roles.getFull(), "Full Map", "Roles");
    	dataset.addValue(roles.getPartial(), "Partial Map", "Roles");
    	dataset.addValue(roles.getNoMap(), "No Map", "Roles");
    	
    	dataset.addValue(techniques.getFull(), "Full Map", "Techniques");
    	dataset.addValue(techniques.getPartial(), "Partial Map", "Techniques");
    	dataset.addValue(techniques.getNoMap(), "No Map", "Techniques");
    	
    	dataset.addValue(activities.getFull(), "Full Map", "Activities");
    	dataset.addValue(activities.getPartial(), "Partial Map", "Activities");
    	dataset.addValue(activities.getNoMap(), "No Map", "Activities");
    	
		return dataset;
	}


	private Triplet countMapActivities(List<BaseActivity> baseActivities) {
		Triplet t = new Triplet();
		
		for(BaseActivity baseActivity : baseActivities){
			boolean hasTarget = false;
			ComplianceInfo e = new ComplianceInfo(baseActivity.getId(),baseActivity.getName());
			List<BaseComplianceMap> mapList = baseActivity.getComplianceMap();
			for(BaseComplianceMap map : mapList){
				EList<AssuranceAsset> elist = map.getTarget();
				if (elist!=null && elist.size()>0){
					if (!hasTarget) hasTarget=true;
					MapKind kind = map.getType();
					String literal = kind.getLiteral();  
					String exp = map.getMapJustification()==null ? " " : map.getMapJustification().getExplanation();
					if(literal.equals("full")) {
						t.setFull(t.getFull()+1);
						e.addFullMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
					else if(literal.equals("partial")){
						t.setPartial(t.getPartial()+1);
						e.addPartialMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
					else if(literal.equals("noMap")){
						t.setNoMap(t.getNoMap()+1);
						e.addNoMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
				}
			}
			if (hasTarget) t.addData(e);
		}
		return t;
	}
    
    private Triplet countMapTechniques(List<BaseTechnique> baseTechniques) {
		Triplet t = new Triplet();
		
		for(BaseTechnique baseTechnique : baseTechniques){
			boolean hasTarget = false;
			ComplianceInfo e = new ComplianceInfo(baseTechnique.getId(),baseTechnique.getName());
			List<BaseComplianceMap> mapList = baseTechnique.getComplianceMap();
			for(BaseComplianceMap map : mapList){
				EList<AssuranceAsset> elist = map.getTarget();
				if (elist!=null && elist.size()>0){
					if (!hasTarget) hasTarget=true;
					MapKind kind = map.getType();
					String literal = kind.getLiteral();  
					String exp = map.getMapJustification()==null ? " " : map.getMapJustification().getExplanation();
					if(literal.equals("full")) {
						t.setFull(t.getFull()+1);
						e.addFullMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
					else if(literal.equals("partial")){
						t.setPartial(t.getPartial()+1);
						e.addPartialMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
					else if(literal.equals("noMap")){
						t.setNoMap(t.getNoMap()+1);
						e.addNoMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
				}
			}
			if (hasTarget) t.addData(e);
		}
		return t;
	}

    private Triplet countMapRoles(List<BaseRole> baseRoles) {
		Triplet t = new Triplet();
		
		for(BaseRole baseRole : baseRoles){
			boolean hasTarget = false;
			ComplianceInfo e = new ComplianceInfo(baseRole.getId(),baseRole.getName());
			List<BaseComplianceMap> mapList = baseRole.getComplianceMap();
			for(BaseComplianceMap map : mapList){
				EList<AssuranceAsset> elist = map.getTarget();
				if (elist!=null && elist.size()>0){
					if (!hasTarget) hasTarget=true;
					MapKind kind = map.getType();
					String literal = kind.getLiteral(); 
					String exp = map.getMapJustification()==null ? " " : map.getMapJustification().getExplanation(); 
					if(literal.equals("full")) {
						t.setFull(t.getFull()+1);
						e.addFullMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
					else if(literal.equals("partial")){
						t.setPartial(t.getPartial()+1);
						e.addPartialMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
					else if(literal.equals("noMap")){
						t.setNoMap(t.getNoMap()+1);
						e.addNoMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
				}
			}
			if (hasTarget) t.addData(e);
		}
		return t;
	}
    
	private Triplet countMapArtefacts(List<BaseArtefact> baseArtefacts) {
		Triplet t = new Triplet();
		
		for(BaseArtefact baseArtefact : baseArtefacts){
			boolean hasTarget = false;
			ComplianceInfo e = new ComplianceInfo(baseArtefact.getId(),baseArtefact.getName());
			List<BaseComplianceMap> mapList = baseArtefact.getComplianceMap();
			for(BaseComplianceMap map : mapList){
				EList<AssuranceAsset> elist = map.getTarget();
				if (elist!=null && elist.size()>0){
					if (!hasTarget) hasTarget=true;
					MapKind kind = map.getType();
					String literal = kind.getLiteral(); 
					String exp = map.getMapJustification()==null ? " " : map.getMapJustification().getExplanation(); 
					if(literal.equals("full")) {
						t.setFull(t.getFull()+1);
						e.addFullMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
					else if(literal.equals("partial")){
						t.setPartial(t.getPartial()+1);
						e.addPartialMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
					else if(literal.equals("noMap")){
						t.setNoMap(t.getNoMap()+1);
						e.addNoMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
				}
			}
			if (hasTarget) t.addData(e);
		}
		return t;
	}
	
	private Triplet countMapRequirements(List<BaseRequirement> baseRequirements) {
		Triplet t = new Triplet();
		for(BaseRequirement baseRequirement : baseRequirements){
			boolean hasTarget = false;
			ComplianceInfo e = new ComplianceInfo(baseRequirement.getId(),baseRequirement.getName());
			List<BaseComplianceMap> mapList = baseRequirement.getComplianceMap();
			for(BaseComplianceMap map : mapList){
				EList<AssuranceAsset> elist = map.getTarget();
				if (elist!=null && elist.size()>0){
					if (!hasTarget) hasTarget=true;
					MapKind kind = map.getType();
					String literal = kind.getLiteral(); 
					String exp = map.getMapJustification()==null ? " " : map.getMapJustification().getExplanation();
					if(literal.equals("full")) {
						t.setFull(t.getFull()+1);
						e.addFullMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
					else if(literal.equals("partial")){
						t.setPartial(t.getPartial()+1);
						e.addPartialMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
					else if(literal.equals("noMap")){
						t.setNoMap(t.getNoMap()+1);
						e.addNoMap(map.getId()+"@"+map.getName()+"@"+exp);
					}
				}
			}
			if (hasTarget) t.addData(e);
		}
		return t;
	}



	
	public byte[] getComplianceMapsChart(){
		return complianceMaps;
	}
	
    
    public String getComplianceMapChartTitle(){
    	return COMPLIANCE_MAPS;
    }


	public Hierarchical getDataContainer() {
		return dataDescription;
	}
    
	
}

