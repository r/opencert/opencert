/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.providers;

import org.eclipse.osgi.util.NLS;

/**
 * 
 * 
 */
public class BaselineMessages extends NLS {
	
	private static final String BUNDLE_NAME = "org.eclipse.opencert.apm.baseline.baseline.providers.baselineMessages"; //$NON-NLS-1$

	
	public static String BaseFrameworkPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRequirementPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseArtefactPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseActivityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRequirementRelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRolePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseApplicabilityLevelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseCriticalityLevelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseTechniquePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseCriticalityApplicabilityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseActivityRelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseIndependencyLevelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRecommendationLevelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseControlCategoryPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseApplicabilityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseApplicabilityRelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseComplianceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseActivitySelectionPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseActivityRequirementPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseActivityApplicabilityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseActivityEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseActivityComplianceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseArtefactSelectionPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseArtefactEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseArtefactComplianceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRolSelectionPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRoleEquivelanceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRoleComplianceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRequirementSelectionPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRequirementApplicabilityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRequirementEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseRequerimentComplianceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseTechniqueEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseTechniqueComplianceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaseTechniqueSelectionPropertiesEditionPart_PropertiesGroupLabel;


	
	public static String BaseFramework_ReadOnly;

	
	public static String BaseFramework_Part_Title;

	
	public static String BaseRequirement_ReadOnly;

	
	public static String BaseRequirement_Part_Title;

	
	public static String BaseArtefact_ReadOnly;

	
	public static String BaseArtefact_Part_Title;

	
	public static String BaseActivity_ReadOnly;

	
	public static String BaseActivity_Part_Title;

	
	public static String BaseRequirementRel_ReadOnly;

	
	public static String BaseRequirementRel_Part_Title;

	
	public static String BaseRole_ReadOnly;

	
	public static String BaseRole_Part_Title;

	
	public static String BaseApplicabilityLevel_ReadOnly;

	
	public static String BaseApplicabilityLevel_Part_Title;

	
	public static String BaseCriticalityLevel_ReadOnly;

	
	public static String BaseCriticalityLevel_Part_Title;

	
	public static String BaseTechnique_ReadOnly;

	
	public static String BaseTechnique_Part_Title;

	
	public static String BaseArtefactRel_ReadOnly;

	
	public static String BaseArtefactRel_Part_Title;

	
	public static String BaseCriticalityApplicability_ReadOnly;

	
	public static String BaseCriticalityApplicability_Part_Title;

	
	public static String BaseActivityRel_ReadOnly;

	
	public static String BaseActivityRel_Part_Title;

	
	public static String BaseIndependencyLevel_ReadOnly;

	
	public static String BaseIndependencyLevel_Part_Title;

	
	public static String BaseRecommendationLevel_ReadOnly;

	
	public static String BaseRecommendationLevel_Part_Title;

	
	public static String BaseControlCategory_ReadOnly;

	
	public static String BaseControlCategory_Part_Title;

	
	public static String BaseApplicability_ReadOnly;

	
	public static String BaseApplicability_Part_Title;

	
	public static String BaseApplicabilityRel_ReadOnly;

	
	public static String BaseApplicabilityRel_Part_Title;

	
	public static String BaseEquivalenceMap_ReadOnly;

	
	public static String BaseEquivalenceMap_Part_Title;

	
	public static String BaseComplianceMap_ReadOnly;

	
	public static String BaseComplianceMap_Part_Title;

	
	public static String BaseActivitySelection_ReadOnly;

	
	public static String BaseActivitySelection_Part_Title;

	
	public static String BaseActivityRequirement_ReadOnly;

	
	public static String BaseActivityRequirement_Part_Title;

	
	public static String BaseActivityApplicability_ReadOnly;

	
	public static String BaseActivityApplicability_Part_Title;

	
	public static String BaseActivityEquivalenceMap_ReadOnly;

	
	public static String BaseActivityEquivalenceMap_Part_Title;

	
	public static String BaseActivityComplianceMap_ReadOnly;

	
	public static String BaseActivityComplianceMap_Part_Title;

	
	public static String BaseArtefactSelection_ReadOnly;

	
	public static String BaseArtefactSelection_Part_Title;

	
	public static String BaseArtefactEquivalenceMap_ReadOnly;

	
	public static String BaseArtefactEquivalenceMap_Part_Title;

	
	public static String BaseArtefactComplianceMap_ReadOnly;

	
	public static String BaseArtefactComplianceMap_Part_Title;

	
	public static String BaseRolSelection_ReadOnly;

	
	public static String BaseRolSelection_Part_Title;

	
	public static String BaseRoleEquivelanceMap_ReadOnly;

	
	public static String BaseRoleEquivelanceMap_Part_Title;

	
	public static String BaseRoleComplianceMap_ReadOnly;

	
	public static String BaseRoleComplianceMap_Part_Title;

	
	public static String BaseRequirementSelection_ReadOnly;

	
	public static String BaseRequirementSelection_Part_Title;

	
	public static String BaseRequirementApplicability_ReadOnly;

	
	public static String BaseRequirementApplicability_Part_Title;

	
	public static String BaseRequirementEquivalenceMap_ReadOnly;

	
	public static String BaseRequirementEquivalenceMap_Part_Title;

	
	public static String BaseRequerimentComplianceMap_ReadOnly;

	
	public static String BaseRequerimentComplianceMap_Part_Title;

	
	public static String BaseTechniqueEquivalenceMap_ReadOnly;

	
	public static String BaseTechniqueEquivalenceMap_Part_Title;

	
	public static String BaseTechniqueComplianceMap_ReadOnly;

	
	public static String BaseTechniqueComplianceMap_Part_Title;

	
	public static String BaseTechniqueSelection_ReadOnly;

	
	public static String BaseTechniqueSelection_Part_Title;


	
	public static String BaseFrameworkPropertiesEditionPart_IdLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_NameLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_DescriptionLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_ScopeLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_RevLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_PurposeLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_PublisherLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_IssuedLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_OwnedActivitiesLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_OwnedArtefactLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_OwnedRequirementLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_OwnedApplicLevelLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_OwnedCriticLevelLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_OwnedRoleLabel;

	
	public static String BaseFrameworkPropertiesEditionPart_OwnedTechniqueLabel;

	
	public static String BaseRequirementPropertiesEditionPart_IdLabel;

	
	public static String BaseRequirementPropertiesEditionPart_NameLabel;

	
	public static String BaseRequirementPropertiesEditionPart_DescriptionLabel;

	
	public static String BaseRequirementPropertiesEditionPart_ReferenceLabel;

	
	public static String BaseRequirementPropertiesEditionPart_AssumptionsLabel;

	
	public static String BaseRequirementPropertiesEditionPart_RationaleLabel;

	
	public static String BaseRequirementPropertiesEditionPart_ImageLabel;

	
	public static String BaseRequirementPropertiesEditionPart_AnnotationsLabel;

	
	public static String BaseRequirementPropertiesEditionPart_OwnedRelLabel;

	
	public static String BaseRequirementPropertiesEditionPart_SubRequirementLabel;

	
	public static String BaseArtefactPropertiesEditionPart_IdLabel;

	
	public static String BaseArtefactPropertiesEditionPart_NameLabel;

	
	public static String BaseArtefactPropertiesEditionPart_DescriptionLabel;

	
	public static String BaseArtefactPropertiesEditionPart_ReferenceLabel;

	
	public static String BaseArtefactPropertiesEditionPart_ConstrainingRequirementLabel;

	
	public static String BaseArtefactPropertiesEditionPart_ApplicableTechniqueLabel;

	
	public static String BaseArtefactPropertiesEditionPart_OwnedRelLabel;

	
	public static String BaseArtefactPropertiesEditionPart_PropertyLabel;

	
	public static String BaseActivityPropertiesEditionPart_IdLabel;

	
	public static String BaseActivityPropertiesEditionPart_NameLabel;

	
	public static String BaseActivityPropertiesEditionPart_DescriptionLabel;

	
	public static String BaseActivityPropertiesEditionPart_ObjectiveLabel;

	
	public static String BaseActivityPropertiesEditionPart_ScopeLabel;

	
	public static String BaseActivityPropertiesEditionPart_RequiredArtefactLabel;

	
	public static String BaseActivityPropertiesEditionPart_ProducedArtefactLabel;

	
	public static String BaseActivityPropertiesEditionPart_SubActivityLabel;

	
	public static String BaseActivityPropertiesEditionPart_PrecedingActivityLabel;

	
	public static String BaseActivityPropertiesEditionPart_RoleLabel;

	
	public static String BaseActivityPropertiesEditionPart_ApplicableTechniqueLabel;

	
	public static String BaseActivityPropertiesEditionPart_OwnedRelLabel;

	
	public static String BaseRequirementRelPropertiesEditionPart_TargetLabel;

	
	public static String BaseRequirementRelPropertiesEditionPart_SourceLabel;

	
	public static String BaseRequirementRelPropertiesEditionPart_TypeLabel;

	
	public static String BaseRolePropertiesEditionPart_IdLabel;

	
	public static String BaseRolePropertiesEditionPart_NameLabel;

	
	public static String BaseRolePropertiesEditionPart_DescriptionLabel;

	
	public static String BaseApplicabilityLevelPropertiesEditionPart_IdLabel;

	
	public static String BaseApplicabilityLevelPropertiesEditionPart_NameLabel;

	
	public static String BaseApplicabilityLevelPropertiesEditionPart_DescriptionLabel;

	
	public static String BaseCriticalityLevelPropertiesEditionPart_IdLabel;

	
	public static String BaseCriticalityLevelPropertiesEditionPart_NameLabel;

	
	public static String BaseCriticalityLevelPropertiesEditionPart_DescriptionLabel;

	
	public static String BaseTechniquePropertiesEditionPart_IdLabel;

	
	public static String BaseTechniquePropertiesEditionPart_NameLabel;

	
	public static String BaseTechniquePropertiesEditionPart_DescriptionLabel;

	
	public static String BaseTechniquePropertiesEditionPart_CriticApplicLabel;

	
	public static String BaseTechniquePropertiesEditionPart_AimLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_IdLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_NameLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_DescriptionLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_MaxMultiplicitySourceLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_MinMultiplicitySourceLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_MaxMultiplicityTargetLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_MinMultiplicityTargetLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_ModificationEffectLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_RevocationEffectLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_SourceLabel;

	
	public static String BaseArtefactRelPropertiesEditionPart_TargetLabel;

	
	public static String BaseCriticalityApplicabilityPropertiesEditionPart_ApplicLevelLabel;

	
	public static String BaseCriticalityApplicabilityPropertiesEditionPart_CriticLevelLabel;

	
	public static String BaseCriticalityApplicabilityPropertiesEditionPart_ApplicLevelComboLabel;

	
	public static String BaseCriticalityApplicabilityPropertiesEditionPart_CriticLevelComboLabel;

	
	public static String BaseCriticalityApplicabilityPropertiesEditionPart_CommentLabel;

	
	public static String BaseActivityRelPropertiesEditionPart_TypeLabel;

	
	public static String BaseActivityRelPropertiesEditionPart_SourceLabel;

	
	public static String BaseActivityRelPropertiesEditionPart_TargetLabel;

	
	public static String BaseIndependencyLevelPropertiesEditionPart_IdLabel;

	
	public static String BaseIndependencyLevelPropertiesEditionPart_NameLabel;

	
	public static String BaseIndependencyLevelPropertiesEditionPart_DescriptionLabel;

	
	public static String BaseRecommendationLevelPropertiesEditionPart_IdLabel;

	
	public static String BaseRecommendationLevelPropertiesEditionPart_NameLabel;

	
	public static String BaseRecommendationLevelPropertiesEditionPart_DescriptionLabel;

	
	public static String BaseControlCategoryPropertiesEditionPart_IdLabel;

	
	public static String BaseControlCategoryPropertiesEditionPart_NameLabel;

	
	public static String BaseControlCategoryPropertiesEditionPart_DescriptionLabel;

	
	public static String BaseApplicabilityPropertiesEditionPart_IdLabel;

	
	public static String BaseApplicabilityPropertiesEditionPart_NameLabel;

	
	public static String BaseApplicabilityPropertiesEditionPart_ApplicCriticLabel;

	
	public static String BaseApplicabilityPropertiesEditionPart_ApplicCriticTableLabel;

	
	public static String BaseApplicabilityPropertiesEditionPart_CommentsLabel;

	
	public static String BaseApplicabilityPropertiesEditionPart_ApplicTargetLabel;

	
	public static String BaseApplicabilityPropertiesEditionPart_OwnedRelLabel;

	
	public static String BaseApplicabilityRelPropertiesEditionPart_TypeLabel;

	
	public static String BaseApplicabilityRelPropertiesEditionPart_SourceLabel;

	
	public static String BaseApplicabilityRelPropertiesEditionPart_TargetLabel;

	
	public static String BaseEquivalenceMapPropertiesEditionPart_IdLabel;

	
	public static String BaseEquivalenceMapPropertiesEditionPart_NameLabel;

	
	public static String BaseEquivalenceMapPropertiesEditionPart_MapGroupLabel;

	
	public static String BaseEquivalenceMapPropertiesEditionPart_MapGroupComboLabel;

	
	public static String BaseEquivalenceMapPropertiesEditionPart_TypeLabel;

	
	public static String BaseEquivalenceMapPropertiesEditionPart_JustificationLabel;

	
	public static String BaseEquivalenceMapPropertiesEditionPart_TargetLabel;

	
	public static String BaseComplianceMapPropertiesEditionPart_IdLabel;

	
	public static String BaseComplianceMapPropertiesEditionPart_NameLabel;

	
	public static String BaseComplianceMapPropertiesEditionPart_MapGroupLabel;

	
	public static String BaseComplianceMapPropertiesEditionPart_MapGroupComboLabel;

	
	public static String BaseComplianceMapPropertiesEditionPart_TypeLabel;

	
	public static String BaseComplianceMapPropertiesEditionPart_JustificationLabel;

	
	public static String BaseComplianceMapPropertiesEditionPart_TargetLabel;

	
	public static String BaseActivitySelectionPropertiesEditionPart_IsSelectedLabel;

	
	public static String BaseActivitySelectionPropertiesEditionPart_SelectionJustificationLabel;

	
	public static String BaseActivityRequirementPropertiesEditionPart_OwnedRequirementLabel;

	
	public static String BaseActivityApplicabilityPropertiesEditionPart_ApplicabilityLabel;

	
	public static String BaseActivityApplicabilityPropertiesEditionPart_ApplicabilityTableLabel;

	
	public static String BaseActivityEquivalenceMapPropertiesEditionPart_EquivalenceMapLabel;

	
	public static String BaseActivityEquivalenceMapPropertiesEditionPart_ActivityEquivalenceMapLabel;

	
	public static String BaseActivityComplianceMapPropertiesEditionPart_ComplianceMapLabel;

	
	public static String BaseActivityComplianceMapPropertiesEditionPart_ActivityComplianceMapLabel;

	
	public static String BaseArtefactSelectionPropertiesEditionPart_IsSelectedLabel;

	
	public static String BaseArtefactSelectionPropertiesEditionPart_SelectionJustificationLabel;

	
	public static String BaseArtefactEquivalenceMapPropertiesEditionPart_EquivalenceMapLabel;

	
	public static String BaseArtefactEquivalenceMapPropertiesEditionPart_ArtefactEquivalenceMapLabel;

	
	public static String BaseArtefactComplianceMapPropertiesEditionPart_ComplianceMapLabel;

	
	public static String BaseArtefactComplianceMapPropertiesEditionPart_ArtefactComplianceMapLabel;

	
	public static String BaseRolSelectionPropertiesEditionPart_IsSelectedLabel;

	
	public static String BaseRolSelectionPropertiesEditionPart_SelectionJustificationLabel;

	
	public static String BaseRoleEquivelanceMapPropertiesEditionPart_EquivalenceMapLabel;

	
	public static String BaseRoleEquivelanceMapPropertiesEditionPart_RoleEquivalenceMapLabel;

	
	public static String BaseRoleComplianceMapPropertiesEditionPart_ComplianceMapLabel;

	
	public static String BaseRoleComplianceMapPropertiesEditionPart_RoleComplianceMapLabel;

	
	public static String BaseRequirementSelectionPropertiesEditionPart_IsSelectedLabel;

	
	public static String BaseRequirementSelectionPropertiesEditionPart_SelectionJustificationLabel;

	
	public static String BaseRequirementApplicabilityPropertiesEditionPart_ApplicabilityLabel;

	
	public static String BaseRequirementApplicabilityPropertiesEditionPart_ApplicabilityTableLabel;

	
	public static String BaseRequirementEquivalenceMapPropertiesEditionPart_EquivalenceMapLabel;

	
	public static String BaseRequirementEquivalenceMapPropertiesEditionPart_RequirementEquivalenceMapLabel;

	
	public static String BaseRequerimentComplianceMapPropertiesEditionPart_ComplianceMapLabel;

	
	public static String BaseRequerimentComplianceMapPropertiesEditionPart_RequirementComplianceMapTableLabel;

	
	public static String BaseTechniqueEquivalenceMapPropertiesEditionPart_EquivalenceMapLabel;

	
	public static String BaseTechniqueEquivalenceMapPropertiesEditionPart_TechniqueEquivalenceMapLabel;

	
	public static String BaseTechniqueComplianceMapPropertiesEditionPart_ComplianceMapLabel;

	
	public static String BaseTechniqueComplianceMapPropertiesEditionPart_TechniqueComplianceMapLabel;

	
	public static String BaseTechniqueSelectionPropertiesEditionPart_IsSelectedLabel;

	
	public static String BaseTechniqueSelectionPropertiesEditionPart_SelectionJustificationLabel;


	
	public static String PropertiesEditionPart_DocumentationLabel;

	
	public static String PropertiesEditionPart_IntegerValueMessage;

	
	public static String PropertiesEditionPart_FloatValueMessage;

	
	public static String PropertiesEditionPart_ShortValueMessage;

	
	public static String PropertiesEditionPart_LongValueMessage;

	
	public static String PropertiesEditionPart_ByteValueMessage;

	
	public static String PropertiesEditionPart_BigIntegerValueMessage;

	
	public static String PropertiesEditionPart_BigDecimalValueMessage;

	
	public static String PropertiesEditionPart_DoubleValueMessage;

	
	public static String PropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PropertiesEditionPart_RequiredFeatureMessage;

	
	public static String PropertiesEditionPart_AddTableViewerLabel;

	
	public static String PropertiesEditionPart_EditTableViewerLabel;

	
	public static String PropertiesEditionPart_RemoveTableViewerLabel;

	
	public static String PropertiesEditionPart_AddListViewerLabel;

	
	public static String PropertiesEditionPart_RemoveListViewerLabel;

	// Start of user code for additionnal NLS Constants
	
	// End of user code

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, BaselineMessages.class);
	}

	
	private BaselineMessages() {
		//protect instanciation
	}
}
