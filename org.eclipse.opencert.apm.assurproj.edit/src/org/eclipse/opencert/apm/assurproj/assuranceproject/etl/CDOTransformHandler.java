/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.assuranceproject.etl;


import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.common.model.EMFUtil;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.provider.AssuranceprojectEditPlugin;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl;
import org.eclipse.opencert.pam.procspec.process.ProcessFactory;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.pam.procspec.process.impl.ProcessModelImpl;


public class CDOTransformHandler {
	// configuration params
	// coming from class AssurProjInitializationHelper 
	private static final String UTF_8 = "UTF-8";
	
	private static String etlFile = "epsilon/epf2assetspackage.etl";
	
	private static String eviModelURI = "eviModel.evidence";	
	private static String procModelURI = "procModel.process";
	
	private static String epfMetamodelFileURI = "model/EPFSchema.xsd";
	private static String eviMetamodelFileURI = "model/evidence.ecore";
	private static String procMetamodelFileURI = "model/process.ecore";
	
	private static String pluginInWorkSpace = "";
	private static String pluginPath = "";
	private static String epfMetamodelFile = "";
	
	private static String eviMetamodelFile = "";
	private static String procMetamodelFile = "";

	
	private CDOResource eviModel;
	private CDOResource processModel;
	
	private CDOResourceFolder project;
	private EMFPlugin pluginProject;
	private CDOTransaction transaction;	
	
	String methodFilePath="";
	String configurationFilePath="";
	
	// files
	private String eviModelFile;
	private String procModelFile;	
		
	private Resource eviModelResource = null;
	private Resource procModelResource = null;
	
	public CDOTransformHandler(CDOResourceFolder project, String methodFilePath, String configurationFilePath, CDOResource evidenceModel, CDOResource processModel, CDOTransaction transaction) {
		
		try {
			this.project = project; // MCP: proyecto creado por el usuario 
			this.pluginProject = AssuranceprojectEditPlugin.INSTANCE;
			this.configurationFilePath = configurationFilePath;
			this.methodFilePath = methodFilePath;
			this.eviModel=evidenceModel;
			this.processModel=processModel;
			//this.sourceModelURI = sourceModelURI;

			this.transaction = transaction;

			URL entry = null;
			URL fileURL = null;
			String file  ="";
			try {
				// init config data
				// String spluginPath = Platform.getBundle(Activator.PLUGIN_ID).getLocation();
				String spluginPath = AssuranceprojectEditPlugin.getPlugin().getBundle().getLocation();
				RedirectSystemErr.out("spluginPath:= ." + spluginPath + ".");
				if(spluginPath.contains("plugins")) // si estoy en modo jar
				{
					//entry = Platform.getBundle(Activator.PLUGIN_ID).getEntry(fileIURI);
					entry = AssuranceprojectEditPlugin.getPlugin().getBundle().getEntry(eviMetamodelFileURI);
					fileURL = FileLocator.resolve(entry);
					file = fileURL.getFile();
					RedirectSystemErr.out("file:= ." + file + ".");
					pluginPath = file.substring("file:/".length(), file.indexOf("plugins"))+ "configuration";
					RedirectSystemErr.out("pluginPath:= ." + pluginPath + ".");
					//String sname = Platform.getBundle(Activator.PLUGIN_ID).getSymbolicName();
					String sname = AssuranceprojectEditPlugin.getPlugin().getBundle().getSymbolicName();
					RedirectSystemErr.out("sname:= ." + sname + ".");
					pluginInWorkSpace = "";
				}	
				else
				{
					pluginPath = spluginPath.substring("reference:file:/".length(), spluginPath.length()-1); // eliminar ultimo caracter "/"
					RedirectSystemErr.out("pluginPath:= ." + pluginPath + ".");
					//String sname = Platform.getBundle(Activator.PLUGIN_ID).getSymbolicName();
					String sname = AssuranceprojectEditPlugin.getPlugin().getBundle().getSymbolicName();
					int index0 = pluginPath.lastIndexOf("/", pluginPath.length()-(sname.length()+2));
					pluginInWorkSpace = pluginPath.substring(index0+1, pluginPath.lastIndexOf("/"));
				}
				RedirectSystemErr.out("pluginInWorkSpace:= ." + pluginInWorkSpace + ".");				
				epfMetamodelFile = pluginPath + "/" + epfMetamodelFileURI;
				eviMetamodelFile = pluginPath + "/" + eviMetamodelFileURI;
				procMetamodelFile = pluginPath + "/" + procMetamodelFileURI;

				// path ficheros temporales de modelo input y modelo output
	            String modelsPath = System.getProperty("user.home");
	            RedirectSystemErr.out("modelsPath:= ." + modelsPath + ".");
				
				eviModelFile = modelsPath  + IPath.SEPARATOR + eviModelURI;
				procModelFile = modelsPath  + IPath.SEPARATOR + procModelURI;
				URI eviModelLocalURI = URI.createFileURI(eviModelFile);
				URI procModelLocalURI = URI.createFileURI(procModelFile);

				eviModelResource = createEviModel(eviModelLocalURI, null);
				procModelResource = createProcModel(procModelLocalURI, null);
				
				
			} catch (IOException e) {
				e.printStackTrace();
				RedirectSystemErr.out("CDOTransformHandler Exception 1 ." + e.toString());
			} 
		}
		catch (Exception e)
		{
			e.printStackTrace();
			RedirectSystemErr.out("CDOTransformHandler Exception 3 ." + e.toString());
		}
		
	}
	
	/* copied from BaselineDiagramEditorUtil */
	private void attachModelToResource(EObject model,
			Resource resource) {
		resource.getContents().add(model);
	}
	
	/* copied from BaselineDiagramEditorUtil */	
	private void setCharset(IFile file) {
		if (file == null) {
			return;
		}
		try {
			file.setCharset("UTF-8", new NullProgressMonitor()); //$NON-NLS-1$
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}


	private ArtefactModel createEviInitialModel() {
		return EvidenceFactory.eINSTANCE.createArtefactModel();
	}

	private Resource createEviModel(URI modelURI, final CDOResource modelCDO) {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		final Resource modelResource = editingDomain.getResourceSet()
				.createResource(modelURI);

		AbstractTransactionalCommand command = new AbstractTransactionalCommand(
				editingDomain,
				"Create Evidence Model File",
				Collections.EMPTY_LIST) {
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {
				ArtefactModelImpl model = (ArtefactModelImpl)createEviInitialModel();
				if(modelCDO != null)
				{
					model = (ArtefactModelImpl) modelCDO.getContents().get(0);
				}
				attachModelToResource(model, modelResource);

				try {
					modelResource.save(null);
				} catch (IOException e) {

					e.printStackTrace();
				}
				return CommandResult.newOKCommandResult();
			}
		};
		try {
			OperationHistoryFactory.getOperationHistory().execute(command, null, null);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		setCharset(WorkspaceSynchronizer.getFile(modelResource));
		return modelResource;
	}
	
	
	private ProcessModel createProcInitialModel() {
		return ProcessFactory.eINSTANCE.createProcessModel();
	}

	private Resource createProcModel(URI modelURI, final CDOResource modelCDO) {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		final Resource modelResource = editingDomain.getResourceSet()
				.createResource(modelURI);

		AbstractTransactionalCommand command = new AbstractTransactionalCommand(
				editingDomain,
				"Create Process Model File",
				Collections.EMPTY_LIST) {
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {
				ProcessModelImpl model = (ProcessModelImpl)createProcInitialModel();
				if(modelCDO != null)
				{
					model = (ProcessModelImpl) modelCDO.getContents().get(0);
				}
				attachModelToResource(model, modelResource);

				try {
					modelResource.save(null);
				} catch (IOException e) {

					e.printStackTrace();
				}
				return CommandResult.newOKCommandResult();
			}
		};
		try {
			OperationHistoryFactory.getOperationHistory().execute(command, null, null);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		setCharset(WorkspaceSynchronizer.getFile(modelResource));
		return modelResource;
	}
	

	// MCP
	public Object execute() throws ExecutionException, IOException {
		Object res = null;
		EpfTransformParameters parameters = createTransformParameters(new EpfTransformParameters());
		EpfTransformation service = new EpfTransformation();
		service.createJob(parameters);
		try {
			RedirectSystemErr.out(" before service.execute() ");
			// ejecutar transformacion
			service.execute();
			res = service.getResult();
			RedirectSystemErr.out(" res :=." + res +".");
			
			
		    RedirectSystemErr.out(" before refreshing file content ");
		    // guardar el modelo transformado y disponible en fichero en CDO
		    // primero refrescar contenido creado por rules
			if (eviModelResource.isLoaded()) {
				eviModelResource.unload();
				try {
					eviModelResource.load(Collections.EMPTY_MAP);
				}
				catch (IOException exception) {
					exception.printStackTrace();
				}
			}

		    // guardar el modelo transformado y disponible en fichero en CDO
		    // primero refrescar contenido creado por rules
			if (procModelResource.isLoaded()) {
				procModelResource.unload();
				try {
					procModelResource.load(Collections.EMPTY_MAP);
				}
				catch (IOException exception) {
					exception.printStackTrace();
				}
			}
			
			
		    
		    RedirectSystemErr.out(" before creating CDO Evidence and Process models. ");
		    
		    URI eviXmiUri = URI.createFileURI(eviModelFile);
		    URI eviCdoURI = eviModel.getURI();
		    
		    URI procXmiUri = URI.createFileURI(procModelFile);
		    URI procCdoURI = processModel.getURI();
		    
			ResourceSet resourceSet = new ResourceSetImpl();
			resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());

			Resource eviXmiResource = resourceSet.createResource(eviXmiUri);
			Resource procXmiResource = resourceSet.createResource(procXmiUri);
			
			FileInputStream eviFileInStream = new FileInputStream(eviXmiUri.path());
			eviXmiResource.load(eviFileInStream, Collections.EMPTY_MAP);
			
			FileInputStream procFileInStream = new FileInputStream(procXmiUri.path());
			procXmiResource.load(procFileInStream, Collections.EMPTY_MAP);

			eviXmiResource.setURI(eviXmiUri);
			procXmiResource.setURI(procXmiUri);
			
			EMFUtil.safeResolveAll(resourceSet);
           
			CDOResource eviCdoResource = transaction.getOrCreateResource(eviCdoURI.path());
			eviCdoResource.getContents().addAll(eviXmiResource.getContents());
              
			CDOResource procCdoResource = transaction.getOrCreateResource(procCdoURI.path());
			procCdoResource.getContents().addAll(procXmiResource.getContents());
			
			EMFUtil.safeResolveAll(resourceSet);
			
			eviCdoResource.save(Collections.EMPTY_MAP);
			procCdoResource.save(Collections.EMPTY_MAP);
			
			transaction.commit();
    
		} catch (Exception e) {
			e.printStackTrace();
		}
	    RedirectSystemErr.end();		
		return res;
	}

		
	// MCP
	public EpfTransformParameters createTransformParameters(EpfTransformParameters epfTransformParameters) 
	{
		//etlTransformParameters.setPluginID(Activator.PLUGIN_ID);
		
		// reglas
		epfTransformParameters.setEtlTransform(etlFile);
		
		//input
		epfTransformParameters.setEPFMetaModelFilePath(epfMetamodelFile);
		epfTransformParameters.setConfModelFilePath(configurationFilePath);
		epfTransformParameters.setLibModelFilePath(methodFilePath);
		epfTransformParameters.setConfName("CONF");
		epfTransformParameters.setLibName("LIB");
		epfTransformParameters.setConfReadOnLoad(true);
		epfTransformParameters.setLibReadOnLoad(true);
		epfTransformParameters.setConfStoreOnDisposal(true);
		epfTransformParameters.setLibStoreOnDisposal(true);

		//output
		epfTransformParameters.setArtMetaModelFilePath(eviMetamodelFile);
		epfTransformParameters.setProcessMetaModelFilePath(procMetamodelFile);
		
		epfTransformParameters.setArtMetamodelURI(EvidencePackage.eINSTANCE.getNsURI());
		epfTransformParameters.setProcessMetaModelURI(ProcessPackage.eINSTANCE.getNsURI());
		epfTransformParameters.setArtModelFilePath(eviModelFile);
		epfTransformParameters.setProcessModelFilePath(procModelFile);
		epfTransformParameters.setArtName("ART");
		epfTransformParameters.setProcessName("PRO");
		epfTransformParameters.setArtReadOnLoad(false);
		epfTransformParameters.setProcessReadOnLoad(false);
		epfTransformParameters.setArtStoreOnDisposal(true);
		epfTransformParameters.setProcessStoreOnDisposal(true);

		return epfTransformParameters;
	}
}
