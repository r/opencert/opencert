/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.commands;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.LayoutConstraint;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.opencert.sam.arg.arg.ModelElement;

// Start MCP
public class LoadViewLocations {
    private CDOResource sourceFile;
	//Map<String, Point> objectLocations = new HashMap<String, Point>();    
	Map<String, LayoutConstraint> objectLocations = new HashMap<String, LayoutConstraint>();
	
    /**
     * @generated NOT
     */
    public LoadViewLocations (
					CDOResource sourceFile) {
            this.sourceFile = sourceFile;
    }

	/**
	 * @generated NOT
	 */
	public void LoadObjectLocations()
	{
		CDOResource aa = sourceFile;
		DiagramImpl d = (DiagramImpl)aa.getContents().get(0);
		for(Object elem0 : d.getChildren()) 
		{
			if(elem0 instanceof Node)
			{       
				Node view= (Node)elem0;
				
				//String objectId = aa.getID(view);

        		//<layoutConstraint xmi:type="notation:Bounds" xmi:id="_HxWc8zZfEeOqz6ZKAHqxFQ" x="440" y="200" width="96" height="91"/>    					
				LayoutConstraint lc = view.getLayoutConstraint();    					

				// hay varios elementos con el mismo UUID 
				// Guardo no el id de la view sino del modelo
				/* PROBLEMA los nuevos ficheros (diagrama y modelo tienen UUIDs muy diferentes!!!!)
				System.out.println("key = "+ objectId+view.getElement().getClass().getName());    					
				objectLocations.put(objectId+view.getElement().getClass().getName(), pt);
				*/
				ModelElement me = (ModelElement)view.getElement();
				//MCP: OJO los UUIDs han cambiado porque he copiado los modelos y ademas dan problemas las superclases en el caso de AgreementImpl!!!!
				// solo si guardo traza transformacion podre hacerlo correctamente 
				//me.eClass().getName()
				System.out.println("key = "+ me.getId()+me.getName()+me.getClass().getName());    					
				objectLocations.put(me.getId()+me.getName()+me.eClass().getName(), lc);
			}
		}
	}
	
	
	public LayoutConstraint getObjectLocation (String key){
		return objectLocations.get(key);			
	}

}
// End MCP