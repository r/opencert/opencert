/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.util;

import java.nio.file.FileSystems;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.EnumSet;


public class TreeSWTUtil
{
	private TreeListFiles pf;
	
	public TreeListFiles.TreeParent getListFiles(String dir, int deep)
	{
		try {
			// limitando la profundidad
			
			//para un File Path path = FileSystems.getDefault().getPath("logs", "access.log");
			Path startingDir = FileSystems.getDefault().getPath(dir);
			// poner Integer.MAX_VALUE en lugar de deep para que sea sin limite de profundidad
			EnumSet<FileVisitOption> opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS);
			pf = new TreeListFiles(dir);
			Files.walkFileTree(startingDir, opts, deep, pf);
			TreeListFiles.TreeParent root = pf.getRoot();
			
			return root;
		}
		catch (Exception e)
		{
			 System.out.println("ListarFicheros: " + e.toString());
		}
		
		return null;
	}

	
//	public void ListarFicheros2()
//	{
//		try {
//		//para un File Path path = FileSystems.getDefault().getPath("logs", "access.log");
//		Path startingDir = FileSystems.getDefault().getPath("D:\\Personal\\Carmen\\Proassurance\\trabajo\\runtime-EclipseApplication");
//
//		System.out.println("ListarFicheros: sin profundidad");
//		// sin limite de profundidad
//		ListFiles pf = new ListFiles();
//		Files.walkFileTree(startingDir, pf);
//
//
//		System.out.println("ListarFicheros: con profundidad de 5");		
//		// limitando la profundidad
//		// poner Integer.MAX_VALUE en lugar de 5 para que sea sin limite de profundidad
//		EnumSet<FileVisitOption> opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS);
//		Files.walkFileTree(startingDir, opts, 5, pf);
//		}
//		catch (Exception e)
//		{
//			 System.out.println("ListarFicheros: " + e.toString());
//		}
//	}
	
    /*
	public void listRecursive(String dir, int depth)
	{
        char[] indentCh = new char[++depth];
        java.util.Arrays.fill(indentCh, '\t');
        String indent = new String(indentCh);

        NodePrx[] contents = dir.list();

        for (int i = 0; i < contents.length; ++i) {
            DirectoryPrx subdir = DirectoryPrxHelper.checkedCast(contents[i]);
            FilePrx file = FilePrxHelper.uncheckedCast(contents[i]);
            System.out.println(indent + contents[i].name() +
                (subdir != null ? " (directory):" : " (file):"));
            if (subdir != null) {
                listRecursive(subdir, depth);
            } else {
                String[] text = file.read();
                for (int j = 0; j < text.length; ++j)
                    System.out.println(indent + "\t" + text[j]);
            }
        }
    }
    */
		

//	public void ListarFicheros3()
//    {
//        System.out.println("File system roots returned by FileSystemView.getFileSystemView():");
//        FileSystemView fsv = FileSystemView.getFileSystemView();
//        File[] roots = fsv.getRoots();
//        for (int i = 0; i < roots.length; i++)
//        {
//            System.out.println("Root: " + roots[i]);
//        }
//
//        System.out.println("Home directory: " + fsv.getHomeDirectory());
//
//        System.out.println("File system roots returned by File.listRoots():");
//        File[] f = File.listRoots();
//        for (int i = 0; i < f.length; i++)
//        {
//            System.out.println("Drive: " + f[i]);
//            System.out.println("Display name: " + fsv.getSystemDisplayName(f[i]));
//            System.out.println("Is drive: " + fsv.isDrive(f[i]));
//            System.out.println("Is floppy: " + fsv.isFloppyDrive(f[i]));
//            System.out.println("Readable: " + f[i].canRead());
//            System.out.println("Writable: " + f[i].canWrite());
//            System.out.println("Total space: " + f[i].getTotalSpace());
//            System.out.println("Usable space: " + f[i].getUsableSpace());
//        }
//    }

}
