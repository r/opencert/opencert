/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.emf.common.notify.AdapterFactory;

public class AdapterFactoryHandler implements IAdapterFactoryProvider {

	public static final AdapterFactoryHandler eINSTANCE = new AdapterFactoryHandler();

	private static final Log logger = LogFactory.getLog(AdapterFactoryHandler.class);

	private AdapterFactory adapterFactory = null;

	private AdapterFactoryHandler() {
		super();
	}

	public AdapterFactory getAdapterFactory() {
		if (this.adapterFactory == null) {
			this.adapterFactory = AdapterFactoryFactory.eINSTANCE.createAdapterFactory();
		}
		return this.adapterFactory;
	}

	public static Log getLogger() {
		return logger;
	}

}
