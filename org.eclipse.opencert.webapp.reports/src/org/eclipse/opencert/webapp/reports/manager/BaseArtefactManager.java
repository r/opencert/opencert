/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager;

import java.util.Collection;
import java.util.List;

import org.eclipse.opencert.webapp.reports.dao.BaseArtefactDAO;
import org.eclipse.opencert.webapp.reports.dao.BaselineDAO;
import org.eclipse.opencert.webapp.reports.util.IdNameDesc;
import org.eclipse.opencert.webapp.reports.view.administration.ProjectBaselineArtefactTable.BaselineArtafectChangePack;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BaseArtefactManager 
{
    public static final String SPRING_NAME = "baseArtefactManager";
    
    @Autowired
    private BaseArtefactDAO baseArtefactDAO;
    
    @Autowired
    private BaselineDAO baselineDAO;
    
	public BaseArtefact getBaseArtefact(Long baseArtefactUid) 
	{   
	    BaseArtefact baseArtefact = baseArtefactDAO.getBaseArtefact(baseArtefactUid);
	    return baseArtefact;
	}
	
	public List<BaseArtefact> getAfftectedBaseArtefacts(final long baseArtefactID, ChangeEffectKind changeEffectKind)
	{
	   List<BaseArtefact> result = baseArtefactDAO.getAfftectedBaseArtefacts(baseArtefactID, changeEffectKind);
	   return result;
	}
	
    public void updateBaseArtefacts(Long baseFrameworkUid, BaselineArtafectChangePack currentElementChanges)
    {
        if (currentElementChanges == null) {
            return;
        }
        Collection<IdNameDesc> changedBaseArtefacts = currentElementChanges.getChangedBaseArtefacts();
        if (changedBaseArtefacts != null) {
            for (IdNameDesc chArt : changedBaseArtefacts)
            {
                baseArtefactDAO.updateBaseArtefact(chArt);
            }
        }
        Collection<IdNameDesc> addedBaseArtefacts = currentElementChanges.getAddedBaseArtefacts();
        if (addedBaseArtefacts != null) {
            for (IdNameDesc addedArt : addedBaseArtefacts)
            {
                baselineDAO.addBaseArtefactToBaseline(baseFrameworkUid, addedArt);
            }
        }
        Collection<Long> removedBaseArtefacts = currentElementChanges.getRemovedBaseArtefacts();
        if (removedBaseArtefacts != null) {
            for (Long baseArtefactID : removedBaseArtefacts)
            {
                // not nice because it is not an atomic operation!
                // protection against integrity problems - before deleting the BaseArtefact, remove all BaseArtefactRels in which it is a target or source.
                baseArtefactDAO.deleteBaseArtefactRelsForBaseArtefact(baseArtefactID);
                baselineDAO.removeBaseArtefactFromBaseline(baseFrameworkUid, baseArtefactID);
            }
        }
    }
}
