/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension;
import org.eclipse.opencert.sam.arg.arg.AssertedEvidence;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asserted Evidence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedEvidenceImpl#getMultiextension <em>Multiextension</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedEvidenceImpl#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedEvidenceImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedEvidenceImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssertedEvidenceImpl extends AssertedRelationshipImpl implements AssertedEvidence {
	/**
	 * The default value of the '{@link #getMultiextension() <em>Multiextension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiextension()
	 * @generated
	 * @ordered
	 */
	protected static final AssertedByMultiplicityExtension MULTIEXTENSION_EDEFAULT = AssertedByMultiplicityExtension.NORMAL;

	/**
	 * The default value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardinality()
	 * @generated
	 * @ordered
	 */
	protected static final String CARDINALITY_EDEFAULT = "";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertedEvidenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.ASSERTED_EVIDENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertedByMultiplicityExtension getMultiextension() {
		return (AssertedByMultiplicityExtension)eDynamicGet(ArgPackage.ASSERTED_EVIDENCE__MULTIEXTENSION, ArgPackage.Literals.ASSERTED_EVIDENCE__MULTIEXTENSION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiextension(AssertedByMultiplicityExtension newMultiextension) {
		eDynamicSet(ArgPackage.ASSERTED_EVIDENCE__MULTIEXTENSION, ArgPackage.Literals.ASSERTED_EVIDENCE__MULTIEXTENSION, newMultiextension);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCardinality() {
		return (String)eDynamicGet(ArgPackage.ASSERTED_EVIDENCE__CARDINALITY, ArgPackage.Literals.ASSERTED_EVIDENCE__CARDINALITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCardinality(String newCardinality) {
		eDynamicSet(ArgPackage.ASSERTED_EVIDENCE__CARDINALITY, ArgPackage.Literals.ASSERTED_EVIDENCE__CARDINALITY, newCardinality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ArgumentElement> getSource() {
		return (EList<ArgumentElement>)eDynamicGet(ArgPackage.ASSERTED_EVIDENCE__SOURCE, ArgPackage.Literals.ASSERTED_EVIDENCE__SOURCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ArgumentElement> getTarget() {
		return (EList<ArgumentElement>)eDynamicGet(ArgPackage.ASSERTED_EVIDENCE__TARGET, ArgPackage.Literals.ASSERTED_EVIDENCE__TARGET, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.ASSERTED_EVIDENCE__MULTIEXTENSION:
				return getMultiextension();
			case ArgPackage.ASSERTED_EVIDENCE__CARDINALITY:
				return getCardinality();
			case ArgPackage.ASSERTED_EVIDENCE__SOURCE:
				return getSource();
			case ArgPackage.ASSERTED_EVIDENCE__TARGET:
				return getTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.ASSERTED_EVIDENCE__MULTIEXTENSION:
				setMultiextension((AssertedByMultiplicityExtension)newValue);
				return;
			case ArgPackage.ASSERTED_EVIDENCE__CARDINALITY:
				setCardinality((String)newValue);
				return;
			case ArgPackage.ASSERTED_EVIDENCE__SOURCE:
				getSource().clear();
				getSource().addAll((Collection<? extends ArgumentElement>)newValue);
				return;
			case ArgPackage.ASSERTED_EVIDENCE__TARGET:
				getTarget().clear();
				getTarget().addAll((Collection<? extends ArgumentElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.ASSERTED_EVIDENCE__MULTIEXTENSION:
				setMultiextension(MULTIEXTENSION_EDEFAULT);
				return;
			case ArgPackage.ASSERTED_EVIDENCE__CARDINALITY:
				setCardinality(CARDINALITY_EDEFAULT);
				return;
			case ArgPackage.ASSERTED_EVIDENCE__SOURCE:
				getSource().clear();
				return;
			case ArgPackage.ASSERTED_EVIDENCE__TARGET:
				getTarget().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.ASSERTED_EVIDENCE__MULTIEXTENSION:
				return getMultiextension() != MULTIEXTENSION_EDEFAULT;
			case ArgPackage.ASSERTED_EVIDENCE__CARDINALITY:
				return CARDINALITY_EDEFAULT == null ? getCardinality() != null : !CARDINALITY_EDEFAULT.equals(getCardinality());
			case ArgPackage.ASSERTED_EVIDENCE__SOURCE:
				return !getSource().isEmpty();
			case ArgPackage.ASSERTED_EVIDENCE__TARGET:
				return !getTarget().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AssertedEvidenceImpl
