/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package claimtypes;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see claimtypes.ClaimtypesPackage
 * @generated
 */
public interface ClaimtypesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClaimtypesFactory eINSTANCE = claimtypes.impl.ClaimtypesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Claim Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Claim Type</em>'.
	 * @generated
	 */
	ClaimType createClaimType();

	/**
	 * Returns a new object of class '<em>Claim Type Repository</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Claim Type Repository</em>'.
	 * @generated
	 */
	ClaimTypeRepository createClaimTypeRepository();

	/**
	 * Returns a new object of class '<em>Sentence Structure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sentence Structure</em>'.
	 * @generated
	 */
	SentenceStructure createSentenceStructure();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ClaimtypesPackage getClaimtypesPackage();

} //ClaimtypesFactory
