/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;

import org.eclipse.opencert.pam.procspec.process.providers.ProcessMessages;

// End of user code

/**
 * 
 * 
 */
public class OrganizationPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, OrganizationPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected ReferencesTable ownedArtefact;
	protected List<ViewerFilter> ownedArtefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedArtefactFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable triggeredAssetEvent;
	protected List<ViewerFilter> triggeredAssetEventBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> triggeredAssetEventFilters = new ArrayList<ViewerFilter>();
	protected Text address;
	protected ReferencesTable subOrganization;
	protected List<ViewerFilter> subOrganizationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> subOrganizationFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public OrganizationPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public OrganizationPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence organizationStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = organizationStep.addStep(ProcessViewsRepository.Organization.Properties.class);
		propertiesStep.addStep(ProcessViewsRepository.Organization.Properties.id);
		propertiesStep.addStep(ProcessViewsRepository.Organization.Properties.name);
		propertiesStep.addStep(ProcessViewsRepository.Organization.Properties.description);
		propertiesStep.addStep(ProcessViewsRepository.Organization.Properties.ownedArtefact);
		propertiesStep.addStep(ProcessViewsRepository.Organization.Properties.triggeredAssetEvent);
		propertiesStep.addStep(ProcessViewsRepository.Organization.Properties.address);
		propertiesStep.addStep(ProcessViewsRepository.Organization.Properties.subOrganization);
		
		
		composer = new PartComposer(organizationStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ProcessViewsRepository.Organization.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Organization.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Organization.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Organization.Properties.description) {
					return createDescriptionText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Organization.Properties.ownedArtefact) {
					return createOwnedArtefactReferencesTable(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Organization.Properties.triggeredAssetEvent) {
					return createTriggeredAssetEventReferencesTable(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Organization.Properties.address) {
					return createAddressText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Organization.Properties.subOrganization) {
					return createSubOrganizationTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(ProcessMessages.OrganizationPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Organization.Properties.id, ProcessMessages.OrganizationPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							OrganizationPropertiesEditionPartForm.this,
							ProcessViewsRepository.Organization.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									OrganizationPropertiesEditionPartForm.this,
									ProcessViewsRepository.Organization.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									OrganizationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, ProcessViewsRepository.Organization.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Organization.Properties.id, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Organization.Properties.name, ProcessMessages.OrganizationPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							OrganizationPropertiesEditionPartForm.this,
							ProcessViewsRepository.Organization.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									OrganizationPropertiesEditionPartForm.this,
									ProcessViewsRepository.Organization.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									OrganizationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, ProcessViewsRepository.Organization.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Organization.Properties.name, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Organization.Properties.description, ProcessMessages.OrganizationPropertiesEditionPart_DescriptionLabel);
		description = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		description.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							OrganizationPropertiesEditionPartForm.this,
							ProcessViewsRepository.Organization.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									OrganizationPropertiesEditionPartForm.this,
									ProcessViewsRepository.Organization.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									OrganizationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		description.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}
		});
		EditingUtils.setID(description, ProcessViewsRepository.Organization.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Organization.Properties.description, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createOwnedArtefactReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.ownedArtefact = new ReferencesTable(getDescription(ProcessViewsRepository.Organization.Properties.ownedArtefact, ProcessMessages.OrganizationPropertiesEditionPart_OwnedArtefactLabel), new ReferencesTableListener	() {
			public void handleAdd() { addOwnedArtefact(); }
			public void handleEdit(EObject element) { editOwnedArtefact(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveOwnedArtefact(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromOwnedArtefact(element); }
			public void navigateTo(EObject element) { }
		});
		this.ownedArtefact.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Organization.Properties.ownedArtefact, ProcessViewsRepository.FORM_KIND));
		this.ownedArtefact.createControls(parent, widgetFactory);
		this.ownedArtefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.ownedArtefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedArtefactData = new GridData(GridData.FILL_HORIZONTAL);
		ownedArtefactData.horizontalSpan = 3;
		this.ownedArtefact.setLayoutData(ownedArtefactData);
		this.ownedArtefact.disableMove();
		ownedArtefact.setID(ProcessViewsRepository.Organization.Properties.ownedArtefact);
		ownedArtefact.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createOwnedArtefactReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addOwnedArtefact() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(ownedArtefact.getInput(), ownedArtefactFilters, ownedArtefactBusinessFilters,
		"ownedArtefact", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.ownedArtefact,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				ownedArtefact.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveOwnedArtefact(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		ownedArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromOwnedArtefact(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		ownedArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void editOwnedArtefact(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				ownedArtefact.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createTriggeredAssetEventReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.triggeredAssetEvent = new ReferencesTable(getDescription(ProcessViewsRepository.Organization.Properties.triggeredAssetEvent, ProcessMessages.OrganizationPropertiesEditionPart_TriggeredAssetEventLabel), new ReferencesTableListener	() {
			public void handleAdd() { addTriggeredAssetEvent(); }
			public void handleEdit(EObject element) { editTriggeredAssetEvent(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveTriggeredAssetEvent(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromTriggeredAssetEvent(element); }
			public void navigateTo(EObject element) { }
		});
		this.triggeredAssetEvent.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Organization.Properties.triggeredAssetEvent, ProcessViewsRepository.FORM_KIND));
		this.triggeredAssetEvent.createControls(parent, widgetFactory);
		this.triggeredAssetEvent.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.triggeredAssetEvent, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData triggeredAssetEventData = new GridData(GridData.FILL_HORIZONTAL);
		triggeredAssetEventData.horizontalSpan = 3;
		this.triggeredAssetEvent.setLayoutData(triggeredAssetEventData);
		this.triggeredAssetEvent.disableMove();
		triggeredAssetEvent.setID(ProcessViewsRepository.Organization.Properties.triggeredAssetEvent);
		triggeredAssetEvent.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createTriggeredAssetEventReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addTriggeredAssetEvent() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(triggeredAssetEvent.getInput(), triggeredAssetEventFilters, triggeredAssetEventBusinessFilters,
		"triggeredAssetEvent", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.triggeredAssetEvent,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				triggeredAssetEvent.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveTriggeredAssetEvent(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.triggeredAssetEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		triggeredAssetEvent.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromTriggeredAssetEvent(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.triggeredAssetEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		triggeredAssetEvent.refresh();
	}

	/**
	 * 
	 */
	protected void editTriggeredAssetEvent(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				triggeredAssetEvent.refresh();
			}
		}
	}

	
	protected Composite createAddressText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Organization.Properties.address, ProcessMessages.OrganizationPropertiesEditionPart_AddressLabel);
		address = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		address.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData addressData = new GridData(GridData.FILL_HORIZONTAL);
		address.setLayoutData(addressData);
		address.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							OrganizationPropertiesEditionPartForm.this,
							ProcessViewsRepository.Organization.Properties.address,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, address.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									OrganizationPropertiesEditionPartForm.this,
									ProcessViewsRepository.Organization.Properties.address,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, address.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									OrganizationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		address.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.address, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, address.getText()));
				}
			}
		});
		EditingUtils.setID(address, ProcessViewsRepository.Organization.Properties.address);
		EditingUtils.setEEFtype(address, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Organization.Properties.address, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createAddressText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createSubOrganizationTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.subOrganization = new ReferencesTable(getDescription(ProcessViewsRepository.Organization.Properties.subOrganization, ProcessMessages.OrganizationPropertiesEditionPart_SubOrganizationLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.subOrganization, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				subOrganization.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.subOrganization, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				subOrganization.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.subOrganization, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				subOrganization.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.subOrganization, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				subOrganization.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.subOrganizationFilters) {
			this.subOrganization.addFilter(filter);
		}
		this.subOrganization.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Organization.Properties.subOrganization, ProcessViewsRepository.FORM_KIND));
		this.subOrganization.createControls(parent, widgetFactory);
		this.subOrganization.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(OrganizationPropertiesEditionPartForm.this, ProcessViewsRepository.Organization.Properties.subOrganization, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData subOrganizationData = new GridData(GridData.FILL_HORIZONTAL);
		subOrganizationData.horizontalSpan = 3;
		this.subOrganization.setLayoutData(subOrganizationData);
		this.subOrganization.setLowerBound(0);
		this.subOrganization.setUpperBound(-1);
		subOrganization.setID(ProcessViewsRepository.Organization.Properties.subOrganization);
		subOrganization.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createSubOrganizationTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Organization.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(ProcessMessages.Organization_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Organization.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(ProcessMessages.Organization_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Organization.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(ProcessMessages.Organization_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#initOwnedArtefact(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initOwnedArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedArtefact.setContentProvider(contentProvider);
		ownedArtefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Organization.Properties.ownedArtefact);
		if (eefElementEditorReadOnlyState && ownedArtefact.getTable().isEnabled()) {
			ownedArtefact.setEnabled(false);
			ownedArtefact.setToolTipText(ProcessMessages.Organization_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedArtefact.getTable().isEnabled()) {
			ownedArtefact.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#updateOwnedArtefact()
	 * 
	 */
	public void updateOwnedArtefact() {
	ownedArtefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#addFilterOwnedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedArtefact(ViewerFilter filter) {
		ownedArtefactFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#addBusinessFilterOwnedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedArtefact(ViewerFilter filter) {
		ownedArtefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#isContainedInOwnedArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedArtefactTable(EObject element) {
		return ((ReferencesTableSettings)ownedArtefact.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#initTriggeredAssetEvent(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initTriggeredAssetEvent(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		triggeredAssetEvent.setContentProvider(contentProvider);
		triggeredAssetEvent.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Organization.Properties.triggeredAssetEvent);
		if (eefElementEditorReadOnlyState && triggeredAssetEvent.getTable().isEnabled()) {
			triggeredAssetEvent.setEnabled(false);
			triggeredAssetEvent.setToolTipText(ProcessMessages.Organization_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !triggeredAssetEvent.getTable().isEnabled()) {
			triggeredAssetEvent.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#updateTriggeredAssetEvent()
	 * 
	 */
	public void updateTriggeredAssetEvent() {
	triggeredAssetEvent.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#addFilterTriggeredAssetEvent(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTriggeredAssetEvent(ViewerFilter filter) {
		triggeredAssetEventFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#addBusinessFilterTriggeredAssetEvent(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTriggeredAssetEvent(ViewerFilter filter) {
		triggeredAssetEventBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#isContainedInTriggeredAssetEventTable(EObject element)
	 * 
	 */
	public boolean isContainedInTriggeredAssetEventTable(EObject element) {
		return ((ReferencesTableSettings)triggeredAssetEvent.getInput()).contains(element);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#getAddress()
	 * 
	 */
	public String getAddress() {
		return address.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#setAddress(String newValue)
	 * 
	 */
	public void setAddress(String newValue) {
		if (newValue != null) {
			address.setText(newValue);
		} else {
			address.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Organization.Properties.address);
		if (eefElementEditorReadOnlyState && address.isEnabled()) {
			address.setEnabled(false);
			address.setToolTipText(ProcessMessages.Organization_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !address.isEnabled()) {
			address.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#initSubOrganization(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initSubOrganization(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		subOrganization.setContentProvider(contentProvider);
		subOrganization.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Organization.Properties.subOrganization);
		if (eefElementEditorReadOnlyState && subOrganization.isEnabled()) {
			subOrganization.setEnabled(false);
			subOrganization.setToolTipText(ProcessMessages.Organization_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !subOrganization.isEnabled()) {
			subOrganization.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#updateSubOrganization()
	 * 
	 */
	public void updateSubOrganization() {
	subOrganization.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#addFilterSubOrganization(ViewerFilter filter)
	 * 
	 */
	public void addFilterToSubOrganization(ViewerFilter filter) {
		subOrganizationFilters.add(filter);
		if (this.subOrganization != null) {
			this.subOrganization.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#addBusinessFilterSubOrganization(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToSubOrganization(ViewerFilter filter) {
		subOrganizationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.OrganizationPropertiesEditionPart#isContainedInSubOrganizationTable(EObject element)
	 * 
	 */
	public boolean isContainedInSubOrganizationTable(EObject element) {
		return ((ReferencesTableSettings)subOrganization.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ProcessMessages.Organization_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
