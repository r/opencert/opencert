/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.utils;

import org.eclipse.swt.program.Program;

public class ProgramUtils {
	private Program programExplorer;
	
	public void initExplorer(){
		Program [] listOfPrograms = Program.getPrograms();
		for (int i=0; i<listOfPrograms.length; i++){
			if (listOfPrograms[i].getName().equalsIgnoreCase("folder")){
				programExplorer = listOfPrograms[i];
				break;
			}
		}
	}
	public void launchExplorer(String param){
		programExplorer.launch(param);
	}

	public void launchFile(String thefile){
		int dot = thefile.lastIndexOf('.');
		if (dot != -1){
			String extension = thefile.substring(dot);
			Program program = Program.findProgram(extension);
			if (program != null){
				Program.launch(thefile);
			}else{
				launchExplorer(thefile);
			}
		}else{
			launchExplorer(thefile);
		}
	}

}
