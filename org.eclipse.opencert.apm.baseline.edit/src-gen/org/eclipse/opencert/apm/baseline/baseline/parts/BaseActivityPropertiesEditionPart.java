/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;

import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface BaseActivityPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);


	/**
	 * @return the objective
	 * 
	 */
	public String getObjective();

	/**
	 * Defines a new objective
	 * @param newValue the new objective to set
	 * 
	 */
	public void setObjective(String newValue);


	/**
	 * @return the scope
	 * 
	 */
	public String getScope();

	/**
	 * Defines a new scope
	 * @param newValue the new scope to set
	 * 
	 */
	public void setScope(String newValue);




	/**
	 * Init the requiredArtefact
	 * @param settings settings for the requiredArtefact ReferencesTable 
	 */
	public void initRequiredArtefact(ReferencesTableSettings settings);

	/**
	 * Update the requiredArtefact
	 * @param newValue the requiredArtefact to update
	 * 
	 */
	public void updateRequiredArtefact();

	/**
	 * Adds the given filter to the requiredArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToRequiredArtefact(ViewerFilter filter);

	/**
	 * Adds the given filter to the requiredArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToRequiredArtefact(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the requiredArtefact table
	 * 
	 */
	public boolean isContainedInRequiredArtefactTable(EObject element);




	/**
	 * Init the producedArtefact
	 * @param settings settings for the producedArtefact ReferencesTable 
	 */
	public void initProducedArtefact(ReferencesTableSettings settings);

	/**
	 * Update the producedArtefact
	 * @param newValue the producedArtefact to update
	 * 
	 */
	public void updateProducedArtefact();

	/**
	 * Adds the given filter to the producedArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToProducedArtefact(ViewerFilter filter);

	/**
	 * Adds the given filter to the producedArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToProducedArtefact(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the producedArtefact table
	 * 
	 */
	public boolean isContainedInProducedArtefactTable(EObject element);




	/**
	 * Init the subActivity
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initSubActivity(ReferencesTableSettings settings);

	/**
	 * Update the subActivity
	 * @param newValue the subActivity to update
	 * 
	 */
	public void updateSubActivity();

	/**
	 * Adds the given filter to the subActivity edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToSubActivity(ViewerFilter filter);

	/**
	 * Adds the given filter to the subActivity edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToSubActivity(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the subActivity table
	 * 
	 */
	public boolean isContainedInSubActivityTable(EObject element);




	/**
	 * Init the precedingActivity
	 * @param settings settings for the precedingActivity ReferencesTable 
	 */
	public void initPrecedingActivity(ReferencesTableSettings settings);

	/**
	 * Update the precedingActivity
	 * @param newValue the precedingActivity to update
	 * 
	 */
	public void updatePrecedingActivity();

	/**
	 * Adds the given filter to the precedingActivity edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToPrecedingActivity(ViewerFilter filter);

	/**
	 * Adds the given filter to the precedingActivity edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToPrecedingActivity(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the precedingActivity table
	 * 
	 */
	public boolean isContainedInPrecedingActivityTable(EObject element);




	/**
	 * Init the role
	 * @param settings settings for the role ReferencesTable 
	 */
	public void initRole(ReferencesTableSettings settings);

	/**
	 * Update the role
	 * @param newValue the role to update
	 * 
	 */
	public void updateRole();

	/**
	 * Adds the given filter to the role edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToRole(ViewerFilter filter);

	/**
	 * Adds the given filter to the role edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToRole(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the role table
	 * 
	 */
	public boolean isContainedInRoleTable(EObject element);


	/**
	 * @return the applicableTechnique
	 * 
	 */
	public EObject getApplicableTechnique();

	/**
	 * Init the applicableTechnique
	 * @param settings the combo setting
	 */
	public void initApplicableTechnique(EObjectFlatComboSettings settings);

	/**
	 * Defines a new applicableTechnique
	 * @param newValue the new applicableTechnique to set
	 * 
	 */
	public void setApplicableTechnique(EObject newValue);

	/**
	 * Defines the button mode
	 * @param newValue the new mode to set
	 * 
	 */
	public void setApplicableTechniqueButtonMode(ButtonsModeEnum newValue);

	/**
	 * Adds the given filter to the applicableTechnique edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToApplicableTechnique(ViewerFilter filter);

	/**
	 * Adds the given filter to the applicableTechnique edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToApplicableTechnique(ViewerFilter filter);




	/**
	 * Init the ownedRel
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedRel(ReferencesTableSettings settings);

	/**
	 * Update the ownedRel
	 * @param newValue the ownedRel to update
	 * 
	 */
	public void updateOwnedRel();

	/**
	 * Adds the given filter to the ownedRel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedRel(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedRel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedRel(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedRel table
	 * 
	 */
	public boolean isContainedInOwnedRelTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
