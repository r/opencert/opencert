/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.forms;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.providers.EMFListContentProvider;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;

// End of user code

/**
 * 
 * 
 */
public class BaseCriticalityApplicabilityPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, BaseCriticalityApplicabilityPropertiesEditionPart {

	protected EObjectFlatComboViewer applicLevel;
	protected EObjectFlatComboViewer criticLevel;
	protected EMFComboViewer applicLevelCombo;
	protected EMFComboViewer criticLevelCombo;
	protected Text comment;



	/**
	 * For {@link ISection} use only.
	 */
	public BaseCriticalityApplicabilityPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseCriticalityApplicabilityPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence baseCriticalityApplicabilityStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseCriticalityApplicabilityStep.addStep(BaselineViewsRepository.BaseCriticalityApplicability.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevel);
		// propertiesStep.addStep(BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevel);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevelCombo);
		propertiesStep.addStep(BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevelCombo);
		propertiesStep.addStep(BaselineViewsRepository.BaseCriticalityApplicability.Properties.comment);
		
		
		composer = new PartComposer(baseCriticalityApplicabilityStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseCriticalityApplicability.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevel) {
					return createApplicLevelFlatComboViewer(parent, widgetFactory);
				}
				if (key == BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevel) {
					return createCriticLevelFlatComboViewer(parent, widgetFactory);
				}
				if (key == BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevelCombo) {
					return createApplicLevelComboEMFComboViewer(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevelCombo) {
					return createCriticLevelComboEMFComboViewer(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseCriticalityApplicability.Properties.comment) {
					return createCommentTextarea(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(BaselineMessages.BaseCriticalityApplicabilityPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param parent the parent composite
	 * @param widgetFactory factory to use to instanciante widget of the form
	 * 
	 */
	protected Composite createApplicLevelFlatComboViewer(Composite parent, FormToolkit widgetFactory) {
		createDescription(parent, BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevel, BaselineMessages.BaseCriticalityApplicabilityPropertiesEditionPart_ApplicLevelLabel);
		applicLevel = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevel, BaselineViewsRepository.FORM_KIND));
		widgetFactory.adapt(applicLevel);
		applicLevel.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		GridData applicLevelData = new GridData(GridData.FILL_HORIZONTAL);
		applicLevel.setLayoutData(applicLevelData);
		applicLevel.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseCriticalityApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getApplicLevel()));
			}

		});
		applicLevel.setID(BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevel);
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevel, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createApplicLevelFlatComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * @param widgetFactory factory to use to instanciante widget of the form
	 * 
	 */
	protected Composite createCriticLevelFlatComboViewer(Composite parent, FormToolkit widgetFactory) {
		createDescription(parent, BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevel, BaselineMessages.BaseCriticalityApplicabilityPropertiesEditionPart_CriticLevelLabel);
		criticLevel = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevel, BaselineViewsRepository.FORM_KIND));
		widgetFactory.adapt(criticLevel);
		criticLevel.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		GridData criticLevelData = new GridData(GridData.FILL_HORIZONTAL);
		criticLevel.setLayoutData(criticLevelData);
		criticLevel.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseCriticalityApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getCriticLevel()));
			}

		});
		criticLevel.setID(BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevel);
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevel, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createCriticLevelFlatComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createApplicLevelComboEMFComboViewer(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevelCombo, BaselineMessages.BaseCriticalityApplicabilityPropertiesEditionPart_ApplicLevelComboLabel);
		applicLevelCombo = new EMFComboViewer(parent);
		GridData applicLevelComboData = new GridData(GridData.FILL_HORIZONTAL);
		applicLevelCombo.getCombo().setLayoutData(applicLevelComboData);
		applicLevelCombo.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		applicLevelCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseCriticalityApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevelCombo, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getApplicLevelCombo()));
			}

		});
		applicLevelCombo.setContentProvider(new EMFListContentProvider());
		EditingUtils.setID(applicLevelCombo.getCombo(), BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevelCombo);
		EditingUtils.setEEFtype(applicLevelCombo.getCombo(), "eef::Combo");
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevelCombo, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createApplicLevelComboEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createCriticLevelComboEMFComboViewer(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevelCombo, BaselineMessages.BaseCriticalityApplicabilityPropertiesEditionPart_CriticLevelComboLabel);
		criticLevelCombo = new EMFComboViewer(parent);
		GridData criticLevelComboData = new GridData(GridData.FILL_HORIZONTAL);
		criticLevelCombo.getCombo().setLayoutData(criticLevelComboData);
		criticLevelCombo.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		criticLevelCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseCriticalityApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevelCombo, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getCriticLevelCombo()));
			}

		});
		criticLevelCombo.setContentProvider(new EMFListContentProvider());
		EditingUtils.setID(criticLevelCombo.getCombo(), BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevelCombo);
		EditingUtils.setEEFtype(criticLevelCombo.getCombo(), "eef::Combo");
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevelCombo, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createCriticLevelComboEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createCommentTextarea(FormToolkit widgetFactory, Composite parent) {
		Label commentLabel = createDescription(parent, BaselineViewsRepository.BaseCriticalityApplicability.Properties.comment, BaselineMessages.BaseCriticalityApplicabilityPropertiesEditionPart_CommentLabel);
		GridData commentLabelData = new GridData(GridData.FILL_HORIZONTAL);
		commentLabelData.horizontalSpan = 3;
		commentLabel.setLayoutData(commentLabelData);
		comment = widgetFactory.createText(parent, "", SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL); //$NON-NLS-1$
		GridData commentData = new GridData(GridData.FILL_HORIZONTAL);
		commentData.horizontalSpan = 2;
		commentData.heightHint = 80;
		commentData.widthHint = 200;
		comment.setLayoutData(commentData);
		comment.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							BaseCriticalityApplicabilityPropertiesEditionPartForm.this,
							BaselineViewsRepository.BaseCriticalityApplicability.Properties.comment,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, comment.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseCriticalityApplicabilityPropertiesEditionPartForm.this,
									BaselineViewsRepository.BaseCriticalityApplicability.Properties.comment,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, comment.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseCriticalityApplicabilityPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		EditingUtils.setID(comment, BaselineViewsRepository.BaseCriticalityApplicability.Properties.comment);
		EditingUtils.setEEFtype(comment, "eef::Textarea"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseCriticalityApplicability.Properties.comment, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createCommentTextArea

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#getApplicLevel()
	 * 
	 */
	public EObject getApplicLevel() {
		if (applicLevel.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) applicLevel.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#initApplicLevel(EObjectFlatComboSettings)
	 */
	public void initApplicLevel(EObjectFlatComboSettings settings) {
		applicLevel.setInput(settings);
		if (current != null) {
			applicLevel.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevel);
		if (eefElementEditorReadOnlyState && applicLevel.isEnabled()) {
			applicLevel.setEnabled(false);
			applicLevel.setToolTipText(BaselineMessages.BaseCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicLevel.isEnabled()) {
			applicLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#setApplicLevel(EObject newValue)
	 * 
	 */
	public void setApplicLevel(EObject newValue) {
		if (newValue != null) {
			applicLevel.setSelection(new StructuredSelection(newValue));
		} else {
			applicLevel.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevel);
		if (eefElementEditorReadOnlyState && applicLevel.isEnabled()) {
			applicLevel.setEnabled(false);
			applicLevel.setToolTipText(BaselineMessages.BaseCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicLevel.isEnabled()) {
			applicLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#setApplicLevelButtonMode(ButtonsModeEnum newValue)
	 */
	public void setApplicLevelButtonMode(ButtonsModeEnum newValue) {
		applicLevel.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#addFilterApplicLevel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicLevel(ViewerFilter filter) {
		applicLevel.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#addBusinessFilterApplicLevel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToApplicLevel(ViewerFilter filter) {
		applicLevel.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#getCriticLevel()
	 * 
	 */
	public EObject getCriticLevel() {
		if (criticLevel.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) criticLevel.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#initCriticLevel(EObjectFlatComboSettings)
	 */
	public void initCriticLevel(EObjectFlatComboSettings settings) {
		criticLevel.setInput(settings);
		if (current != null) {
			criticLevel.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevel);
		if (eefElementEditorReadOnlyState && criticLevel.isEnabled()) {
			criticLevel.setEnabled(false);
			criticLevel.setToolTipText(BaselineMessages.BaseCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !criticLevel.isEnabled()) {
			criticLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#setCriticLevel(EObject newValue)
	 * 
	 */
	public void setCriticLevel(EObject newValue) {
		if (newValue != null) {
			criticLevel.setSelection(new StructuredSelection(newValue));
		} else {
			criticLevel.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevel);
		if (eefElementEditorReadOnlyState && criticLevel.isEnabled()) {
			criticLevel.setEnabled(false);
			criticLevel.setToolTipText(BaselineMessages.BaseCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !criticLevel.isEnabled()) {
			criticLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#setCriticLevelButtonMode(ButtonsModeEnum newValue)
	 */
	public void setCriticLevelButtonMode(ButtonsModeEnum newValue) {
		criticLevel.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#addFilterCriticLevel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToCriticLevel(ViewerFilter filter) {
		criticLevel.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#addBusinessFilterCriticLevel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToCriticLevel(ViewerFilter filter) {
		criticLevel.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#getApplicLevelCombo()
	 * 
	 */
	public Object getApplicLevelCombo() {
		if (applicLevelCombo.getSelection() instanceof StructuredSelection) {
			return ((StructuredSelection) applicLevelCombo.getSelection()).getFirstElement();
		}
		return "";
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#initApplicLevelCombo(Object input, Object currentValue)
	 */
	public void initApplicLevelCombo(Object input, Object currentValue) {
		applicLevelCombo.setInput(input);
		if (currentValue != null) {
			applicLevelCombo.setSelection(new StructuredSelection(currentValue));
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#setApplicLevelCombo(Object newValue)
	 * 
	 */
	public void setApplicLevelCombo(Object newValue) {
		if (newValue != null) {
			applicLevelCombo.modelUpdating(new StructuredSelection(newValue));
		} else {
			applicLevelCombo.modelUpdating(new StructuredSelection("")); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseCriticalityApplicability.Properties.applicLevelCombo);
		if (eefElementEditorReadOnlyState && applicLevelCombo.isEnabled()) {
			applicLevelCombo.setEnabled(false);
			applicLevelCombo.setToolTipText(BaselineMessages.BaseCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicLevelCombo.isEnabled()) {
			applicLevelCombo.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#addFilterApplicLevelCombo(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicLevelCombo(ViewerFilter filter) {
		applicLevelCombo.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#getCriticLevelCombo()
	 * 
	 */
	public Object getCriticLevelCombo() {
		if (criticLevelCombo.getSelection() instanceof StructuredSelection) {
			return ((StructuredSelection) criticLevelCombo.getSelection()).getFirstElement();
		}
		return "";
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#initCriticLevelCombo(Object input, Object currentValue)
	 */
	public void initCriticLevelCombo(Object input, Object currentValue) {
		criticLevelCombo.setInput(input);
		if (currentValue != null) {
			criticLevelCombo.setSelection(new StructuredSelection(currentValue));
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#setCriticLevelCombo(Object newValue)
	 * 
	 */
	public void setCriticLevelCombo(Object newValue) {
		if (newValue != null) {
			criticLevelCombo.modelUpdating(new StructuredSelection(newValue));
		} else {
			criticLevelCombo.modelUpdating(new StructuredSelection("")); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseCriticalityApplicability.Properties.criticLevelCombo);
		if (eefElementEditorReadOnlyState && criticLevelCombo.isEnabled()) {
			criticLevelCombo.setEnabled(false);
			criticLevelCombo.setToolTipText(BaselineMessages.BaseCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !criticLevelCombo.isEnabled()) {
			criticLevelCombo.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#addFilterCriticLevelCombo(ViewerFilter filter)
	 * 
	 */
	public void addFilterToCriticLevelCombo(ViewerFilter filter) {
		criticLevelCombo.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#getComment()
	 * 
	 */
	public String getComment() {
		return comment.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseCriticalityApplicabilityPropertiesEditionPart#setComment(String newValue)
	 * 
	 */
	public void setComment(String newValue) {
		if (newValue != null) {
			comment.setText(newValue);
		} else {
			comment.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseCriticalityApplicability.Properties.comment);
		if (eefElementEditorReadOnlyState && comment.isEnabled()) {
			comment.setEnabled(false);
			comment.setBackground(comment.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			comment.setToolTipText(BaselineMessages.BaseCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !comment.isEnabled()) {
			comment.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseCriticalityApplicability_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
