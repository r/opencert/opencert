/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.predicates;

import org.eclipse.amalgam.explorer.activity.ui.api.editor.predicates.IPredicate;

/**
 * Predicate implementation to specify when the dashboard is available.
 */
public class DashboardPredicate implements IPredicate {

	/**
	 * Default constructor.
	 */
	public DashboardPredicate() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOk() {
		// TODO Validate this predicate only if the current session is linked to OpenCert resource.
		return true;
	}
}
