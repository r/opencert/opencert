/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;

// End of user code

/**
 * 
 * 
 */
public class BaseActivityEquivalenceMapPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseActivityEquivalenceMapPropertiesEditionPart {

	protected ReferencesTable equivalenceMap;
	protected List<ViewerFilter> equivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> equivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected TableViewer activityEquivalenceMap;
	protected List<ViewerFilter> activityEquivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> activityEquivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addActivityEquivalenceMap;
	protected Button removeActivityEquivalenceMap;
	protected Button editActivityEquivalenceMap;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseActivityEquivalenceMapPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseActivityEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseActivityEquivalenceMapStep.addStep(BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap);
		
		
		composer = new PartComposer(baseActivityEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap) {
					return createEquivalenceMapAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap) {
					return createActivityEquivalenceMapTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseActivityEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEquivalenceMapAdvancedTableComposition(Composite parent) {
		this.equivalenceMap = new ReferencesTable(getDescription(BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap, BaselineMessages.BaseActivityEquivalenceMapPropertiesEditionPart_EquivalenceMapLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				equivalenceMap.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				equivalenceMap.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				equivalenceMap.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				equivalenceMap.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.equivalenceMapFilters) {
			this.equivalenceMap.addFilter(filter);
		}
		this.equivalenceMap.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap, BaselineViewsRepository.SWT_KIND));
		this.equivalenceMap.createControls(parent);
		this.equivalenceMap.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData equivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		equivalenceMapData.horizontalSpan = 3;
		this.equivalenceMap.setLayoutData(equivalenceMapData);
		this.equivalenceMap.setLowerBound(0);
		this.equivalenceMap.setUpperBound(-1);
		equivalenceMap.setID(BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap);
		equivalenceMap.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEquivalenceMapAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createActivityEquivalenceMapTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableActivityEquivalenceMap = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableActivityEquivalenceMap.setHeaderVisible(true);
		GridData gdActivityEquivalenceMap = new GridData();
		gdActivityEquivalenceMap.grabExcessHorizontalSpace = true;
		gdActivityEquivalenceMap.horizontalAlignment = GridData.FILL;
		gdActivityEquivalenceMap.grabExcessVerticalSpace = true;
		gdActivityEquivalenceMap.verticalAlignment = GridData.FILL;
		tableActivityEquivalenceMap.setLayoutData(gdActivityEquivalenceMap);
		tableActivityEquivalenceMap.setLinesVisible(true);

		// Start of user code for columns definition for ActivityEquivalenceMap
				//Start IRR
				/*TableColumn name = new TableColumn(tableActivityEquivalenceMap, SWT.NONE);
								name.setWidth(80);
								name.setText("Label"); //$NON-NLS-1$
				 */
				TableColumn name = new TableColumn(tableActivityEquivalenceMap, SWT.NONE);
				name.setWidth(80);
				name.setText("Map"); //$NON-NLS-1$
				TableColumn nameGroup = new TableColumn(tableActivityEquivalenceMap, SWT.NONE);
				nameGroup.setWidth(100);
				nameGroup.setText("Map Group"); //$NON-NLS-1$
				TableColumn nameArtf = new TableColumn(tableActivityEquivalenceMap, SWT.NONE);
				nameArtf.setWidth(200);
				nameArtf.setText("Activities"); //$NON-NLS-1$
				// End IRR
				// End of user code

		activityEquivalenceMap = new TableViewer(tableActivityEquivalenceMap);
		activityEquivalenceMap.setContentProvider(new ArrayContentProvider());
		activityEquivalenceMap.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for ActivityEquivalenceMap
			public String getColumnText(Object object, int columnIndex) {
				// Start IRR
				/*
				 * AdapterFactoryLabelProvider labelProvider = new
				 * AdapterFactoryLabelProvider(adapterFactory); if (object
				 * instanceof EObject) { switch (columnIndex) { case 0: return
				 * labelProvider.getText(object); } }
				 */
				if (object instanceof EObject) {
					BaseEquivalenceMap baseEquivalenceMap = (BaseEquivalenceMap) object;
					switch (columnIndex) {
					case 0:
						return baseEquivalenceMap.getId();
					case 1:
						if (baseEquivalenceMap.getMapGroup() == null)
							return "";
						else
							return baseEquivalenceMap.getMapGroup().getName();
					case 2:
						if (baseEquivalenceMap.getTarget() == null)
							return "";
						else {
							EList<RefAssurableElement> LstElement = baseEquivalenceMap
									.getTarget();
							Iterator<RefAssurableElement> iter = LstElement
									.iterator();
							String sElement = "[";
							while (iter.hasNext()) {
								RefAssurableElement assElement = iter.next();
								if (assElement instanceof RefActivity) {
									RefActivity refAct = (RefActivity) assElement;
									sElement = sElement + refAct.getName()
											+ ",";
								}
							}

							sElement = sElement.substring(0,
									sElement.length() - 1);
							sElement = sElement + "]";
							return sElement;
						}

					}
				}
				// End IRR
				return ""; //$NON-NLS-1$
			}

			public Image getColumnImage(Object element, int columnIndex) {
				return null;
			}

			// End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		activityEquivalenceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (activityEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) activityEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						activityEquivalenceMap.refresh();
					}
				}
			}

		});
		GridData activityEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		activityEquivalenceMapData.minimumHeight = 120;
		activityEquivalenceMapData.heightHint = 120;
		activityEquivalenceMap.getTable().setLayoutData(activityEquivalenceMapData);
		for (ViewerFilter filter : this.activityEquivalenceMapFilters) {
			activityEquivalenceMap.addFilter(filter);
		}
		EditingUtils.setID(activityEquivalenceMap.getTable(), BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap);
		EditingUtils.setEEFtype(activityEquivalenceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createActivityEquivalenceMapPanel(tableContainer);
		// Start of user code for createActivityEquivalenceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createActivityEquivalenceMapPanel(Composite container) {
		Composite activityEquivalenceMapPanel = new Composite(container, SWT.NONE);
		GridLayout activityEquivalenceMapPanelLayout = new GridLayout();
		activityEquivalenceMapPanelLayout.numColumns = 1;
		activityEquivalenceMapPanel.setLayout(activityEquivalenceMapPanelLayout);
		addActivityEquivalenceMap = new Button(activityEquivalenceMapPanel, SWT.NONE);
		addActivityEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addActivityEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addActivityEquivalenceMap.setLayoutData(addActivityEquivalenceMapData);
		addActivityEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				activityEquivalenceMap.refresh();
			}
		});
		EditingUtils.setID(addActivityEquivalenceMap, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap);
		EditingUtils.setEEFtype(addActivityEquivalenceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeActivityEquivalenceMap = new Button(activityEquivalenceMapPanel, SWT.NONE);
		removeActivityEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeActivityEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeActivityEquivalenceMap.setLayoutData(removeActivityEquivalenceMapData);
		removeActivityEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (activityEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) activityEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						activityEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeActivityEquivalenceMap, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap);
		EditingUtils.setEEFtype(removeActivityEquivalenceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editActivityEquivalenceMap = new Button(activityEquivalenceMapPanel, SWT.NONE);
		editActivityEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editActivityEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editActivityEquivalenceMap.setLayoutData(editActivityEquivalenceMapData);
		editActivityEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (activityEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) activityEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						activityEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editActivityEquivalenceMap, BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap);
		EditingUtils.setEEFtype(editActivityEquivalenceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createActivityEquivalenceMapPanel

		// End of user code
		return activityEquivalenceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart#initEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		equivalenceMap.setContentProvider(contentProvider);
		equivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.equivalenceMap);
		if (eefElementEditorReadOnlyState && equivalenceMap.isEnabled()) {
			equivalenceMap.setEnabled(false);
			equivalenceMap.setToolTipText(BaselineMessages.BaseActivityEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !equivalenceMap.isEnabled()) {
			equivalenceMap.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart#updateEquivalenceMap()
	 * 
	 */
	public void updateEquivalenceMap() {
	equivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart#addFilterEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEquivalenceMap(ViewerFilter filter) {
		equivalenceMapFilters.add(filter);
		if (this.equivalenceMap != null) {
			this.equivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart#addBusinessFilterEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEquivalenceMap(ViewerFilter filter) {
		equivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart#isContainedInEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)equivalenceMap.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart#initActivityEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initActivityEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		activityEquivalenceMap.setContentProvider(contentProvider);
		activityEquivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseActivityEquivalenceMap.Properties.activityEquivalenceMap);
		if (eefElementEditorReadOnlyState && activityEquivalenceMap.getTable().isEnabled()) {
			activityEquivalenceMap.getTable().setEnabled(false);
			activityEquivalenceMap.getTable().setToolTipText(BaselineMessages.BaseActivityEquivalenceMap_ReadOnly);
			addActivityEquivalenceMap.setEnabled(false);
			addActivityEquivalenceMap.setToolTipText(BaselineMessages.BaseActivityEquivalenceMap_ReadOnly);
			removeActivityEquivalenceMap.setEnabled(false);
			removeActivityEquivalenceMap.setToolTipText(BaselineMessages.BaseActivityEquivalenceMap_ReadOnly);
			editActivityEquivalenceMap.setEnabled(false);
			editActivityEquivalenceMap.setToolTipText(BaselineMessages.BaseActivityEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !activityEquivalenceMap.getTable().isEnabled()) {
			activityEquivalenceMap.getTable().setEnabled(true);
			addActivityEquivalenceMap.setEnabled(true);
			removeActivityEquivalenceMap.setEnabled(true);
			editActivityEquivalenceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart#updateActivityEquivalenceMap()
	 * 
	 */
	public void updateActivityEquivalenceMap() {
	activityEquivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart#addFilterActivityEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToActivityEquivalenceMap(ViewerFilter filter) {
		activityEquivalenceMapFilters.add(filter);
		if (this.activityEquivalenceMap != null) {
			this.activityEquivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart#addBusinessFilterActivityEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToActivityEquivalenceMap(ViewerFilter filter) {
		activityEquivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart#isContainedInActivityEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInActivityEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)activityEquivalenceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseActivityEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
