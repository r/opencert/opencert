/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.Choice2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgModelingAssistantProvider;

/**
 * @generated
 */
public class ArgModelingAssistantProviderOfChoiceEditPart extends
		ArgModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSource((ChoiceEditPart) sourceEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSource(ChoiceEditPart source) {
		List<IElementType> types = new ArrayList<IElementType>(3);
		types.add(ArgElementTypes.AssertedInference_4001);
		types.add(ArgElementTypes.AssertedEvidence_4002);
		types.add(ArgElementTypes.AssertedContext_4003);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSourceAndTarget((ChoiceEditPart) sourceEditPart,
				targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSourceAndTarget(
			ChoiceEditPart source, IGraphicalEditPart targetEditPart) {
		List<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof ClaimEditPart) {
			types.add(ArgElementTypes.AssertedInference_4001);
		}
		if (targetEditPart instanceof AgreementEditPart) {
			types.add(ArgElementTypes.AssertedInference_4001);
		}
		if (targetEditPart instanceof ArgumentReasoningEditPart) {
			types.add(ArgElementTypes.AssertedInference_4001);
		}
		if (targetEditPart instanceof ArgumentationEditPart) {
			types.add(ArgElementTypes.AssertedInference_4001);
		}
		if (targetEditPart instanceof InformationElementCitationEditPart) {
			types.add(ArgElementTypes.AssertedInference_4001);
		}
		if (targetEditPart instanceof ArgumentElementCitationEditPart) {
			types.add(ArgElementTypes.AssertedInference_4001);
		}
		if (targetEditPart instanceof ChoiceEditPart) {
			types.add(ArgElementTypes.AssertedInference_4001);
		}
		if (targetEditPart instanceof Choice2EditPart) {
			types.add(ArgElementTypes.AssertedInference_4001);
		}
		if (targetEditPart instanceof ClaimEditPart) {
			types.add(ArgElementTypes.AssertedEvidence_4002);
		}
		if (targetEditPart instanceof ArgumentReasoningEditPart) {
			types.add(ArgElementTypes.AssertedEvidence_4002);
		}
		if (targetEditPart instanceof InformationElementCitationEditPart) {
			types.add(ArgElementTypes.AssertedEvidence_4002);
		}
		if (targetEditPart instanceof ArgumentElementCitationEditPart) {
			types.add(ArgElementTypes.AssertedEvidence_4002);
		}
		if (targetEditPart instanceof ChoiceEditPart) {
			types.add(ArgElementTypes.AssertedEvidence_4002);
		}
		if (targetEditPart instanceof Choice2EditPart) {
			types.add(ArgElementTypes.AssertedEvidence_4002);
		}
		if (targetEditPart instanceof ClaimEditPart) {
			types.add(ArgElementTypes.AssertedContext_4003);
		}
		if (targetEditPart instanceof AgreementEditPart) {
			types.add(ArgElementTypes.AssertedContext_4003);
		}
		if (targetEditPart instanceof ArgumentReasoningEditPart) {
			types.add(ArgElementTypes.AssertedContext_4003);
		}
		if (targetEditPart instanceof ArgumentationEditPart) {
			types.add(ArgElementTypes.AssertedContext_4003);
		}
		if (targetEditPart instanceof InformationElementCitationEditPart) {
			types.add(ArgElementTypes.AssertedContext_4003);
		}
		if (targetEditPart instanceof ArgumentElementCitationEditPart) {
			types.add(ArgElementTypes.AssertedContext_4003);
		}
		if (targetEditPart instanceof ChoiceEditPart) {
			types.add(ArgElementTypes.AssertedContext_4003);
		}
		if (targetEditPart instanceof Choice2EditPart) {
			types.add(ArgElementTypes.AssertedContext_4003);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForTarget((ChoiceEditPart) sourceEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForTarget(ChoiceEditPart source,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == ArgElementTypes.AssertedInference_4001) {
			types.add(ArgElementTypes.Claim_2001);
			types.add(ArgElementTypes.Agreement_2002);
			types.add(ArgElementTypes.ArgumentReasoning_2003);
			types.add(ArgElementTypes.Argumentation_2004);
			types.add(ArgElementTypes.InformationElementCitation_2005);
			types.add(ArgElementTypes.ArgumentElementCitation_2006);
			types.add(ArgElementTypes.Choice_2007);
			types.add(ArgElementTypes.Choice_3001);
		} else if (relationshipType == ArgElementTypes.AssertedEvidence_4002) {
			types.add(ArgElementTypes.Claim_2001);
			types.add(ArgElementTypes.ArgumentReasoning_2003);
			types.add(ArgElementTypes.InformationElementCitation_2005);
			types.add(ArgElementTypes.ArgumentElementCitation_2006);
			types.add(ArgElementTypes.Choice_2007);
			types.add(ArgElementTypes.Choice_3001);
		} else if (relationshipType == ArgElementTypes.AssertedContext_4003) {
			types.add(ArgElementTypes.Claim_2001);
			types.add(ArgElementTypes.Agreement_2002);
			types.add(ArgElementTypes.ArgumentReasoning_2003);
			types.add(ArgElementTypes.Argumentation_2004);
			types.add(ArgElementTypes.InformationElementCitation_2005);
			types.add(ArgElementTypes.ArgumentElementCitation_2006);
			types.add(ArgElementTypes.Choice_2007);
			types.add(ArgElementTypes.Choice_3001);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((ChoiceEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(ChoiceEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(3);
		types.add(ArgElementTypes.AssertedInference_4001);
		types.add(ArgElementTypes.AssertedEvidence_4002);
		types.add(ArgElementTypes.AssertedContext_4003);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((ChoiceEditPart) targetEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(ChoiceEditPart target,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == ArgElementTypes.AssertedInference_4001) {
			types.add(ArgElementTypes.Claim_2001);
			types.add(ArgElementTypes.Agreement_2002);
			types.add(ArgElementTypes.ArgumentReasoning_2003);
			types.add(ArgElementTypes.Argumentation_2004);
			types.add(ArgElementTypes.InformationElementCitation_2005);
			types.add(ArgElementTypes.ArgumentElementCitation_2006);
			types.add(ArgElementTypes.Choice_2007);
			types.add(ArgElementTypes.Choice_3001);
		} else if (relationshipType == ArgElementTypes.AssertedEvidence_4002) {
			types.add(ArgElementTypes.Claim_2001);
			types.add(ArgElementTypes.ArgumentReasoning_2003);
			types.add(ArgElementTypes.InformationElementCitation_2005);
			types.add(ArgElementTypes.ArgumentElementCitation_2006);
			types.add(ArgElementTypes.Choice_2007);
			types.add(ArgElementTypes.Choice_3001);
		} else if (relationshipType == ArgElementTypes.AssertedContext_4003) {
			types.add(ArgElementTypes.Claim_2001);
			types.add(ArgElementTypes.Agreement_2002);
			types.add(ArgElementTypes.ArgumentReasoning_2003);
			types.add(ArgElementTypes.Argumentation_2004);
			types.add(ArgElementTypes.InformationElementCitation_2005);
			types.add(ArgElementTypes.ArgumentElementCitation_2006);
			types.add(ArgElementTypes.Choice_2007);
			types.add(ArgElementTypes.Choice_3001);
		}
		return types;
	}

}
