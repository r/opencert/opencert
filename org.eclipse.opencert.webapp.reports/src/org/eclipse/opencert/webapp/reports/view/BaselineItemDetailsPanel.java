/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import org.eclipse.opencert.webapp.reports.listeners.ExternalToolResultOrQueryAddedListener;
import org.eclipse.opencert.webapp.reports.listeners.ProjectBaselineComplianceTableListener;
import org.eclipse.opencert.webapp.reports.listeners.util.ClickType;
import org.eclipse.opencert.webapp.reports.listeners.util.SelectionEvent;
import org.eclipse.opencert.webapp.reports.manager.BaseActivityManager;
import org.eclipse.opencert.webapp.reports.manager.BaseArtefactManager;
import org.eclipse.opencert.webapp.reports.manager.BaseRequirementManager;
import org.eclipse.opencert.webapp.reports.util.BaselineElementType;
import org.eclipse.opencert.webapp.reports.util.HtmlProcessor;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.infra.general.general.DescribableElement;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

public class BaselineItemDetailsPanel
        extends Panel
        implements ProjectBaselineComplianceTableListener
{
    private static final long serialVersionUID = -8796534301901780023L;

    private static final String BASELINE_ITEM_PANEL_LABEL_STYLE = "baselineItemPanelLabel";

    private VerticalLayout descriptionTabPanel;
    private ExternalToolTabContent externalToolTabPanel = null;

    private Label descriptionLabel = new Label("", ContentMode.HTML);
    
    private TabSheet tabsheet;
 
    public BaselineItemDetailsPanel(boolean isShowExternalToolsPanels)
    {
        descriptionLabel.setStyleName(BASELINE_ITEM_PANEL_LABEL_STYLE);
        
        tabsheet = new TabSheet();
        descriptionTabPanel = new VerticalLayout();
        descriptionTabPanel.addComponent(descriptionLabel);
        TabSheet.Tab descTab = tabsheet.addTab(descriptionTabPanel, "Description");
        descTab.setDescription("Description of the selected baseline element.");

        if (isShowExternalToolsPanels)
        {
            externalToolTabPanel = new ExternalToolTabContent();
            TabSheet.Tab extToolTab = tabsheet.addTab(externalToolTabPanel, "External Tools Configuration");
            extToolTab.setDescription("External Tool Configuration for the selected baseline element.");
        }

        setContent(tabsheet);
    }

    @Override
    public void baselineElementClicked(SelectionEvent selectionEvent)
    {        
        if (selectionEvent.getClickType().equals(ClickType.UNSELECTED)) {
            descriptionLabel.setVisible(false);
            setExtToolTabVisible(false);
            return;
        }

        final long clickedCdoId = selectionEvent.getCdoId();
        if (externalToolTabPanel != null)
        {
            externalToolTabPanel.setCurrentBaseElementCdoId(clickedCdoId);
            externalToolTabPanel.generateConnectorLabelsForExternalToolTab(clickedCdoId);
            externalToolTabPanel.refreshVisibilityOfAddOrEditConnectorButtonAndLabels(clickedCdoId);
        }

        if (BaselineElementType.BASE_ARTEFACT.equals(selectionEvent.getBaselineElementType())) 
        {
            BaseArtefactManager baseArtefactManager = (BaseArtefactManager) SpringContextHelper.getBeanFromWebappContext(BaseArtefactManager.SPRING_NAME);
            BaseArtefact baseArtefact = baseArtefactManager.getBaseArtefact(clickedCdoId);

            if (baseArtefact != null) 
            {
                setExtToolTabVisible(true);
                generateBaseElementDescription(baseArtefact);
            }
        } else if (BaselineElementType.BASE_ACTIVITY.equals(selectionEvent.getBaselineElementType())) {
            BaseActivityManager baseActivityManager = (BaseActivityManager) SpringContextHelper.getBeanFromWebappContext(BaseActivityManager.SPRING_NAME);
            BaseActivity baseActivity = baseActivityManager.getBaseActivity(clickedCdoId);

            if (baseActivity != null) 
            {
                setExtToolTabVisible(true);
                generateBaseElementDescription(baseActivity);
            }
        } else if (BaselineElementType.BASE_REQUIREMENT.equals(selectionEvent.getBaselineElementType())) {
            BaseRequirementManager baseRequirementManager = (BaseRequirementManager) SpringContextHelper.getBeanFromWebappContext(BaseRequirementManager.SPRING_NAME);
            BaseRequirement baseRequirement = baseRequirementManager.getBaseRequirement(clickedCdoId);

            if (baseRequirement != null) 
            {
                setExtToolTabVisible(true);
                generateBaseElementDescription(baseRequirement);
            }
        }
        
        if (selectionEvent.getBaseAssetComplianceStatus().isExternalToolResultColumnClicked()) {
            tabsheet.setSelectedTab(externalToolTabPanel);
        } else {
            tabsheet.setSelectedTab(descriptionTabPanel);
        }
    }

    private void setExtToolTabVisible(boolean isVisible)
    {
        if (externalToolTabPanel != null) {
            externalToolTabPanel.setVisible(isVisible);
        }
    }

    private void generateBaseElementDescription(DescribableElement baseElement)
    {
        descriptionLabel.setVisible(true);
        String desc = generateDescription(baseElement);
        descriptionLabel.setValue(desc);
    }

    private String generateDescription(DescribableElement baseElement)
    {
        String description = baseElement.getDescription();
        if (description == null) {
            return "";
        }
        return HtmlProcessor.processHtmlLinks(description);
    }

    public void addExternalToolResultOrQueryAddedListener(ExternalToolResultOrQueryAddedListener listener)
    {
        if (externalToolTabPanel != null) {
            externalToolTabPanel.addExternalToolResultOrQueryAddedListener(listener);
        }
    }
}
