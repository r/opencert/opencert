/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.AnalysisContextArtefactLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractArtefactLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelFactory;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage;

import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.RequirementClaimLink;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;

import org.eclipse.papyrus.sysml.requirements.RequirementsPackage;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.chess.contracts.profile.chesscontract.CHESSContractPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OpenCertTraceLinkMetaModelPackageImpl extends EPackageImpl implements OpenCertTraceLinkMetaModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contractClaimLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contractArtefactLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass formalPropertyClaimLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contractAgreementLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass analysisContextArtefactLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentArgumentationElementLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requirementClaimLinkEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OpenCertTraceLinkMetaModelPackageImpl() {
		super(eNS_URI, OpenCertTraceLinkMetaModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OpenCertTraceLinkMetaModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OpenCertTraceLinkMetaModelPackage init() {
		if (isInited) return (OpenCertTraceLinkMetaModelPackage)EPackage.Registry.INSTANCE.getEPackage(OpenCertTraceLinkMetaModelPackage.eNS_URI);

		// Obtain or create and register package
		OpenCertTraceLinkMetaModelPackageImpl theOpenCertTraceLinkMetaModelPackage = (OpenCertTraceLinkMetaModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OpenCertTraceLinkMetaModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OpenCertTraceLinkMetaModelPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CHESSContractPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOpenCertTraceLinkMetaModelPackage.createPackageContents();

		// Initialize created meta-data
		theOpenCertTraceLinkMetaModelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOpenCertTraceLinkMetaModelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OpenCertTraceLinkMetaModelPackage.eNS_URI, theOpenCertTraceLinkMetaModelPackage);
		return theOpenCertTraceLinkMetaModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContractClaimLink() {
		return contractClaimLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContractClaimLink_Sources() {
		return (EReference)contractClaimLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContractClaimLink_Targets() {
		return (EReference)contractClaimLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContractArtefactLink() {
		return contractArtefactLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContractArtefactLink_Sources() {
		return (EReference)contractArtefactLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContractArtefactLink_Targets() {
		return (EReference)contractArtefactLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFormalPropertyClaimLink() {
		return formalPropertyClaimLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFormalPropertyClaimLink_Sources() {
		return (EReference)formalPropertyClaimLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFormalPropertyClaimLink_Targets() {
		return (EReference)formalPropertyClaimLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContractAgreementLink() {
		return contractAgreementLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContractAgreementLink_Sources() {
		return (EReference)contractAgreementLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContractAgreementLink_Targets() {
		return (EReference)contractAgreementLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnalysisContextArtefactLink() {
		return analysisContextArtefactLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnalysisContextArtefactLink_Sources() {
		return (EReference)analysisContextArtefactLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnalysisContextArtefactLink_Targets() {
		return (EReference)analysisContextArtefactLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentArgumentationElementLink() {
		return componentArgumentationElementLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentArgumentationElementLink_Sources() {
		return (EReference)componentArgumentationElementLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentArgumentationElementLink_Targets() {
		return (EReference)componentArgumentationElementLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequirementClaimLink() {
		return requirementClaimLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementClaimLink_Sources() {
		return (EReference)requirementClaimLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementClaimLink_Targets() {
		return (EReference)requirementClaimLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpenCertTraceLinkMetaModelFactory getOpenCertTraceLinkMetaModelFactory() {
		return (OpenCertTraceLinkMetaModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		contractClaimLinkEClass = createEClass(CONTRACT_CLAIM_LINK);
		createEReference(contractClaimLinkEClass, CONTRACT_CLAIM_LINK__SOURCES);
		createEReference(contractClaimLinkEClass, CONTRACT_CLAIM_LINK__TARGETS);

		contractArtefactLinkEClass = createEClass(CONTRACT_ARTEFACT_LINK);
		createEReference(contractArtefactLinkEClass, CONTRACT_ARTEFACT_LINK__SOURCES);
		createEReference(contractArtefactLinkEClass, CONTRACT_ARTEFACT_LINK__TARGETS);

		formalPropertyClaimLinkEClass = createEClass(FORMAL_PROPERTY_CLAIM_LINK);
		createEReference(formalPropertyClaimLinkEClass, FORMAL_PROPERTY_CLAIM_LINK__SOURCES);
		createEReference(formalPropertyClaimLinkEClass, FORMAL_PROPERTY_CLAIM_LINK__TARGETS);

		contractAgreementLinkEClass = createEClass(CONTRACT_AGREEMENT_LINK);
		createEReference(contractAgreementLinkEClass, CONTRACT_AGREEMENT_LINK__SOURCES);
		createEReference(contractAgreementLinkEClass, CONTRACT_AGREEMENT_LINK__TARGETS);

		analysisContextArtefactLinkEClass = createEClass(ANALYSIS_CONTEXT_ARTEFACT_LINK);
		createEReference(analysisContextArtefactLinkEClass, ANALYSIS_CONTEXT_ARTEFACT_LINK__SOURCES);
		createEReference(analysisContextArtefactLinkEClass, ANALYSIS_CONTEXT_ARTEFACT_LINK__TARGETS);

		componentArgumentationElementLinkEClass = createEClass(COMPONENT_ARGUMENTATION_ELEMENT_LINK);
		createEReference(componentArgumentationElementLinkEClass, COMPONENT_ARGUMENTATION_ELEMENT_LINK__SOURCES);
		createEReference(componentArgumentationElementLinkEClass, COMPONENT_ARGUMENTATION_ELEMENT_LINK__TARGETS);

		requirementClaimLinkEClass = createEClass(REQUIREMENT_CLAIM_LINK);
		createEReference(requirementClaimLinkEClass, REQUIREMENT_CLAIM_LINK__SOURCES);
		createEReference(requirementClaimLinkEClass, REQUIREMENT_CLAIM_LINK__TARGETS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CHESSContractPackage theCHESSContractPackage = (CHESSContractPackage)EPackage.Registry.INSTANCE.getEPackage(CHESSContractPackage.eNS_URI);
		ArgPackage theArgPackage = (ArgPackage)EPackage.Registry.INSTANCE.getEPackage(ArgPackage.eNS_URI);
		EvidencePackage theEvidencePackage = (EvidencePackage)EPackage.Registry.INSTANCE.getEPackage(EvidencePackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		RequirementsPackage theRequirementsPackage = (RequirementsPackage)EPackage.Registry.INSTANCE.getEPackage(RequirementsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(contractClaimLinkEClass, ContractClaimLink.class, "ContractClaimLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContractClaimLink_Sources(), theCHESSContractPackage.getContract(), null, "sources", null, 1, 1, ContractClaimLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getContractClaimLink_Targets(), theArgPackage.getClaim(), null, "targets", null, 1, -1, ContractClaimLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(contractArtefactLinkEClass, ContractArtefactLink.class, "ContractArtefactLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContractArtefactLink_Sources(), theCHESSContractPackage.getContract(), null, "sources", null, 1, 1, ContractArtefactLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getContractArtefactLink_Targets(), theEvidencePackage.getArtefact(), null, "targets", null, 1, -1, ContractArtefactLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(formalPropertyClaimLinkEClass, FormalPropertyClaimLink.class, "FormalPropertyClaimLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFormalPropertyClaimLink_Sources(), theCHESSContractPackage.getFormalProperty(), null, "sources", null, 1, 1, FormalPropertyClaimLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFormalPropertyClaimLink_Targets(), theArgPackage.getClaim(), null, "targets", null, 1, -1, FormalPropertyClaimLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(contractAgreementLinkEClass, ContractAgreementLink.class, "ContractAgreementLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContractAgreementLink_Sources(), theCHESSContractPackage.getContract(), null, "sources", null, 1, 1, ContractAgreementLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getContractAgreementLink_Targets(), theArgPackage.getAgreement(), null, "targets", null, 1, -1, ContractAgreementLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(analysisContextArtefactLinkEClass, AnalysisContextArtefactLink.class, "AnalysisContextArtefactLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnalysisContextArtefactLink_Sources(), theUMLPackage.getClass_(), null, "sources", null, 1, 1, AnalysisContextArtefactLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAnalysisContextArtefactLink_Targets(), theEvidencePackage.getArtefact(), null, "targets", null, 1, -1, AnalysisContextArtefactLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentArgumentationElementLinkEClass, ComponentArgumentationElementLink.class, "ComponentArgumentationElementLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentArgumentationElementLink_Sources(), theUMLPackage.getClass_(), null, "sources", null, 1, 1, ComponentArgumentationElementLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentArgumentationElementLink_Targets(), theArgPackage.getArgumentationElement(), null, "targets", null, 1, -1, ComponentArgumentationElementLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(requirementClaimLinkEClass, RequirementClaimLink.class, "RequirementClaimLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRequirementClaimLink_Sources(), theRequirementsPackage.getRequirement(), null, "sources", null, 1, 1, RequirementClaimLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirementClaimLink_Targets(), theArgPackage.getClaim(), null, "targets", null, 1, -1, RequirementClaimLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //OpenCertTraceLinkMetaModelPackageImpl
