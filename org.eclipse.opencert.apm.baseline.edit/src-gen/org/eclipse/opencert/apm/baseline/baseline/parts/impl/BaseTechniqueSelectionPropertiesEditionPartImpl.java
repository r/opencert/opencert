/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueSelectionPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;

// End of user code

/**
 * 
 * 
 */
public class BaseTechniqueSelectionPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseTechniqueSelectionPropertiesEditionPart {

	protected Button isSelected;
	protected Text selectionJustification;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseTechniqueSelectionPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseTechniqueSelectionStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseTechniqueSelectionStep.addStep(BaselineViewsRepository.BaseTechniqueSelection.Properties.class);
		propertiesStep.addStep(BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected);
		propertiesStep.addStep(BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification);
		
		
		composer = new PartComposer(baseTechniqueSelectionStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseTechniqueSelection.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected) {
					return createIsSelectedCheckbox(parent);
				}
				if (key == BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification) {
					return createSelectionJustificationText(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseTechniqueSelectionPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIsSelectedCheckbox(Composite parent) {
		isSelected = new Button(parent, SWT.CHECK);
		isSelected.setText(getDescription(BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected, BaselineMessages.BaseTechniqueSelectionPropertiesEditionPart_IsSelectedLabel));
		isSelected.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 *
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 	
			 */
			public void widgetSelected(SelectionEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueSelectionPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, new Boolean(isSelected.getSelection())));
			}

		});
		GridData isSelectedData = new GridData(GridData.FILL_HORIZONTAL);
		isSelectedData.horizontalSpan = 2;
		isSelected.setLayoutData(isSelectedData);
		EditingUtils.setID(isSelected, BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected);
		EditingUtils.setEEFtype(isSelected, "eef::Checkbox"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIsSelectedCheckbox

		// End of user code
		return parent;
	}

	
	protected Composite createSelectionJustificationText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification, BaselineMessages.BaseTechniqueSelectionPropertiesEditionPart_SelectionJustificationLabel);
		selectionJustification = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData selectionJustificationData = new GridData(GridData.FILL_HORIZONTAL);
		selectionJustification.setLayoutData(selectionJustificationData);
		selectionJustification.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueSelectionPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, selectionJustification.getText()));
			}

		});
		selectionJustification.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueSelectionPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, selectionJustification.getText()));
				}
			}

		});
		EditingUtils.setID(selectionJustification, BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification);
		EditingUtils.setEEFtype(selectionJustification, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createSelectionJustificationText

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueSelectionPropertiesEditionPart#getIsSelected()
	 * 
	 */
	public Boolean getIsSelected() {
		return Boolean.valueOf(isSelected.getSelection());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueSelectionPropertiesEditionPart#setIsSelected(Boolean newValue)
	 * 
	 */
	public void setIsSelected(Boolean newValue) {
		if (newValue != null) {
			isSelected.setSelection(newValue.booleanValue());
		} else {
			isSelected.setSelection(false);
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected);
		if (eefElementEditorReadOnlyState && isSelected.isEnabled()) {
			isSelected.setEnabled(false);
			isSelected.setToolTipText(BaselineMessages.BaseTechniqueSelection_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !isSelected.isEnabled()) {
			isSelected.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueSelectionPropertiesEditionPart#getSelectionJustification()
	 * 
	 */
	public String getSelectionJustification() {
		return selectionJustification.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueSelectionPropertiesEditionPart#setSelectionJustification(String newValue)
	 * 
	 */
	public void setSelectionJustification(String newValue) {
		if (newValue != null) {
			selectionJustification.setText(newValue);
		} else {
			selectionJustification.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification);
		if (eefElementEditorReadOnlyState && selectionJustification.isEnabled()) {
			selectionJustification.setEnabled(false);
			selectionJustification.setToolTipText(BaselineMessages.BaseTechniqueSelection_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !selectionJustification.isEnabled()) {
			selectionJustification.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseTechniqueSelection_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
