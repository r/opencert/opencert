/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;

import org.eclipse.opencert.pkm.refframework.refframework.parts.RefRequirementPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementApplicabilityPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart;

// End of user code

/**
 * 
 * 
 */
public class RefRequirementPropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private RefRequirementPropertiesEditionPart basePart;

	/**
	 * The RefRequirementBasePropertiesEditionComponent sub component
	 * 
	 */
	protected RefRequirementBasePropertiesEditionComponent refRequirementBasePropertiesEditionComponent;

	/**
	 * The RequirementApplicability part
	 * 
	 */
	private RequirementApplicabilityPropertiesEditionPart requirementApplicabilityPart;

	/**
	 * The RefRequirementRequirementApplicabilityPropertiesEditionComponent sub component
	 * 
	 */
	protected RefRequirementRequirementApplicabilityPropertiesEditionComponent refRequirementRequirementApplicabilityPropertiesEditionComponent;

	/**
	 * The RequirementEquivalenceMap part
	 * 
	 */
	private RequirementEquivalenceMapPropertiesEditionPart requirementEquivalenceMapPart;

	/**
	 * The RefRequirementRequirementEquivalenceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected RefRequirementRequirementEquivalenceMapPropertiesEditionComponent refRequirementRequirementEquivalenceMapPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param refRequirement the EObject to edit
	 * 
	 */
	public RefRequirementPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refRequirement, String editing_mode) {
		super(editingContext, editing_mode);
		if (refRequirement instanceof RefRequirement) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refRequirement, PropertiesEditingProvider.class);
			refRequirementBasePropertiesEditionComponent = (RefRequirementBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefRequirementBasePropertiesEditionComponent.BASE_PART, RefRequirementBasePropertiesEditionComponent.class);
			addSubComponent(refRequirementBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refRequirement, PropertiesEditingProvider.class);
			refRequirementRequirementApplicabilityPropertiesEditionComponent = (RefRequirementRequirementApplicabilityPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefRequirementRequirementApplicabilityPropertiesEditionComponent.REQUIREMENTAPPLICABILITY_PART, RefRequirementRequirementApplicabilityPropertiesEditionComponent.class);
			addSubComponent(refRequirementRequirementApplicabilityPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refRequirement, PropertiesEditingProvider.class);
			refRequirementRequirementEquivalenceMapPropertiesEditionComponent = (RefRequirementRequirementEquivalenceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefRequirementRequirementEquivalenceMapPropertiesEditionComponent.REQUIREMENTEQUIVALENCEMAP_PART, RefRequirementRequirementEquivalenceMapPropertiesEditionComponent.class);
			addSubComponent(refRequirementRequirementEquivalenceMapPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (RefRequirementBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (RefRequirementPropertiesEditionPart)refRequirementBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (RefRequirementRequirementApplicabilityPropertiesEditionComponent.REQUIREMENTAPPLICABILITY_PART.equals(key)) {
			requirementApplicabilityPart = (RequirementApplicabilityPropertiesEditionPart)refRequirementRequirementApplicabilityPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)requirementApplicabilityPart;
		}
		if (RefRequirementRequirementEquivalenceMapPropertiesEditionComponent.REQUIREMENTEQUIVALENCEMAP_PART.equals(key)) {
			requirementEquivalenceMapPart = (RequirementEquivalenceMapPropertiesEditionPart)refRequirementRequirementEquivalenceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)requirementEquivalenceMapPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (RefframeworkViewsRepository.RefRequirement.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (RefRequirementPropertiesEditionPart)propertiesEditionPart;
		}
		if (RefframeworkViewsRepository.RequirementApplicability.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			requirementApplicabilityPart = (RequirementApplicabilityPropertiesEditionPart)propertiesEditionPart;
		}
		if (RefframeworkViewsRepository.RequirementEquivalenceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			requirementEquivalenceMapPart = (RequirementEquivalenceMapPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == RefframeworkViewsRepository.RefRequirement.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == RefframeworkViewsRepository.RequirementApplicability.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == RefframeworkViewsRepository.RequirementEquivalenceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
