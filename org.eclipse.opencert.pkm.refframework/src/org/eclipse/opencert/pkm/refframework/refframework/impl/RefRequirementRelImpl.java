/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;
import org.eclipse.opencert.infra.general.general.RequirementRelKind;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ref Requirement Rel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementRelImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementRelImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementRelImpl#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RefRequirementRelImpl extends CDOObjectImpl implements RefRequirementRel {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final RequirementRelKind TYPE_EDEFAULT = RequirementRelKind.AND;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefRequirementRelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RefframeworkPackage.Literals.REF_REQUIREMENT_REL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefRequirement getTarget() {
		return (RefRequirement)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT_REL__TARGET, RefframeworkPackage.Literals.REF_REQUIREMENT_REL__TARGET, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefRequirement basicGetTarget() {
		return (RefRequirement)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT_REL__TARGET, RefframeworkPackage.Literals.REF_REQUIREMENT_REL__TARGET, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(RefRequirement newTarget) {
		eDynamicSet(RefframeworkPackage.REF_REQUIREMENT_REL__TARGET, RefframeworkPackage.Literals.REF_REQUIREMENT_REL__TARGET, newTarget);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefRequirement getSource() {
		return (RefRequirement)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT_REL__SOURCE, RefframeworkPackage.Literals.REF_REQUIREMENT_REL__SOURCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefRequirement basicGetSource() {
		return (RefRequirement)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT_REL__SOURCE, RefframeworkPackage.Literals.REF_REQUIREMENT_REL__SOURCE, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(RefRequirement newSource) {
		eDynamicSet(RefframeworkPackage.REF_REQUIREMENT_REL__SOURCE, RefframeworkPackage.Literals.REF_REQUIREMENT_REL__SOURCE, newSource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementRelKind getType() {
		return (RequirementRelKind)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT_REL__TYPE, RefframeworkPackage.Literals.REF_REQUIREMENT_REL__TYPE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(RequirementRelKind newType) {
		eDynamicSet(RefframeworkPackage.REF_REQUIREMENT_REL__TYPE, RefframeworkPackage.Literals.REF_REQUIREMENT_REL__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RefframeworkPackage.REF_REQUIREMENT_REL__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case RefframeworkPackage.REF_REQUIREMENT_REL__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case RefframeworkPackage.REF_REQUIREMENT_REL__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RefframeworkPackage.REF_REQUIREMENT_REL__TARGET:
				setTarget((RefRequirement)newValue);
				return;
			case RefframeworkPackage.REF_REQUIREMENT_REL__SOURCE:
				setSource((RefRequirement)newValue);
				return;
			case RefframeworkPackage.REF_REQUIREMENT_REL__TYPE:
				setType((RequirementRelKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_REQUIREMENT_REL__TARGET:
				setTarget((RefRequirement)null);
				return;
			case RefframeworkPackage.REF_REQUIREMENT_REL__SOURCE:
				setSource((RefRequirement)null);
				return;
			case RefframeworkPackage.REF_REQUIREMENT_REL__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_REQUIREMENT_REL__TARGET:
				return basicGetTarget() != null;
			case RefframeworkPackage.REF_REQUIREMENT_REL__SOURCE:
				return basicGetSource() != null;
			case RefframeworkPackage.REF_REQUIREMENT_REL__TYPE:
				return getType() != TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //RefRequirementRelImpl
