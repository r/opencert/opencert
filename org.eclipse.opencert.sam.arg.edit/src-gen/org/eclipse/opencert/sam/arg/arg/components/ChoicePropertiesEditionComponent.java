/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension;
import org.eclipse.opencert.sam.arg.arg.Choice;

import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;
import org.eclipse.opencert.sam.arg.arg.parts.ChoicePropertiesEditionPart;


// End of user code

/**
 * 
 * 
 */
public class ChoicePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	
	/**
	 * Default constructor
	 * 
	 */
	public ChoicePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject choice, String editing_mode) {
		super(editingContext, choice, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = ArgViewsRepository.class;
		partKey = ArgViewsRepository.Choice.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Choice choice = (Choice)elt;
			final ChoicePropertiesEditionPart basePart = (ChoicePropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ArgViewsRepository.Choice.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, choice.getId()));
			
			if (isAccessible(ArgViewsRepository.Choice.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, choice.getName()));
			
			if (isAccessible(ArgViewsRepository.Choice.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, choice.getDescription()));
			
			if (isAccessible(ArgViewsRepository.Choice.Properties.content))
				basePart.setContent(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, choice.getContent()));
			
			if (isAccessible(ArgViewsRepository.Choice.Properties.sourceMultiextension)) {
				basePart.initSourceMultiextension(EEFUtils.choiceOfValues(choice, ArgPackage.eINSTANCE.getChoice_SourceMultiextension()), choice.getSourceMultiextension());
			}
			if (isAccessible(ArgViewsRepository.Choice.Properties.sourceCardinality))
				basePart.setSourceCardinality(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, choice.getSourceCardinality()));
			
			if (isAccessible(ArgViewsRepository.Choice.Properties.optionality))
				basePart.setOptionality(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, choice.getOptionality()));
			
			// init filters
			
			
			
			
			
			
			
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}










	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ArgViewsRepository.Choice.Properties.id) {
			return ArgPackage.eINSTANCE.getModelElement_Id();
		}
		if (editorKey == ArgViewsRepository.Choice.Properties.name) {
			return ArgPackage.eINSTANCE.getModelElement_Name();
		}
		if (editorKey == ArgViewsRepository.Choice.Properties.description) {
			return ArgPackage.eINSTANCE.getArgumentationElement_Description();
		}
		if (editorKey == ArgViewsRepository.Choice.Properties.content) {
			return ArgPackage.eINSTANCE.getArgumentationElement_Content();
		}
		if (editorKey == ArgViewsRepository.Choice.Properties.sourceMultiextension) {
			return ArgPackage.eINSTANCE.getChoice_SourceMultiextension();
		}
		if (editorKey == ArgViewsRepository.Choice.Properties.sourceCardinality) {
			return ArgPackage.eINSTANCE.getChoice_SourceCardinality();
		}
		if (editorKey == ArgViewsRepository.Choice.Properties.optionality) {
			return ArgPackage.eINSTANCE.getChoice_Optionality();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Choice choice = (Choice)semanticObject;
		if (ArgViewsRepository.Choice.Properties.id == event.getAffectedEditor()) {
			choice.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Choice.Properties.name == event.getAffectedEditor()) {
			choice.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Choice.Properties.description == event.getAffectedEditor()) {
			choice.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Choice.Properties.content == event.getAffectedEditor()) {
			choice.setContent((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Choice.Properties.sourceMultiextension == event.getAffectedEditor()) {
			choice.setSourceMultiextension((AssertedByMultiplicityExtension)event.getNewValue());
		}
		if (ArgViewsRepository.Choice.Properties.sourceCardinality == event.getAffectedEditor()) {
			choice.setSourceCardinality((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Choice.Properties.optionality == event.getAffectedEditor()) {
			choice.setOptionality((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ChoicePropertiesEditionPart basePart = (ChoicePropertiesEditionPart)editingPart;
			if (ArgPackage.eINSTANCE.getModelElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Choice.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (ArgPackage.eINSTANCE.getModelElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Choice.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentationElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Choice.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentationElement_Content().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Choice.Properties.content)) {
				if (msg.getNewValue() != null) {
					basePart.setContent(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setContent("");
				}
			}
			if (ArgPackage.eINSTANCE.getChoice_SourceMultiextension().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && isAccessible(ArgViewsRepository.Choice.Properties.sourceMultiextension))
				basePart.setSourceMultiextension((AssertedByMultiplicityExtension)msg.getNewValue());
			
			if (ArgPackage.eINSTANCE.getChoice_SourceCardinality().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Choice.Properties.sourceCardinality)) {
				if (msg.getNewValue() != null) {
					basePart.setSourceCardinality(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setSourceCardinality("");
				}
			}
			if (ArgPackage.eINSTANCE.getChoice_Optionality().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Choice.Properties.optionality)) {
				if (msg.getNewValue() != null) {
					basePart.setOptionality(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setOptionality("");
				}
			}
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			ArgPackage.eINSTANCE.getModelElement_Id(),
			ArgPackage.eINSTANCE.getModelElement_Name(),
			ArgPackage.eINSTANCE.getArgumentationElement_Description(),
			ArgPackage.eINSTANCE.getArgumentationElement_Content(),
			ArgPackage.eINSTANCE.getChoice_SourceMultiextension(),
			ArgPackage.eINSTANCE.getChoice_SourceCardinality(),
			ArgPackage.eINSTANCE.getChoice_Optionality()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (ArgViewsRepository.Choice.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Choice.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Choice.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentationElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentationElement_Description().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Choice.Properties.content == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentationElement_Content().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentationElement_Content().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Choice.Properties.sourceMultiextension == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getChoice_SourceMultiextension().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getChoice_SourceMultiextension().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Choice.Properties.sourceCardinality == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getChoice_SourceCardinality().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getChoice_SourceCardinality().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Choice.Properties.optionality == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getChoice_Optionality().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getChoice_Optionality().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
