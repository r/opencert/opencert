/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.views.properties.tabbed.ISection;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;

// End of user code

/**
 * 
 * 
 */
public class RequirementEquivalenceMapPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, RequirementEquivalenceMapPropertiesEditionPart {

	protected ReferencesTable equivalence;
	protected List<ViewerFilter> equivalenceBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> equivalenceFilters = new ArrayList<ViewerFilter>();
	protected TableViewer requirementEquivalenceMap;
	protected List<ViewerFilter> requirementEquivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> requirementEquivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addRequirementEquivalenceMap;
	protected Button removeRequirementEquivalenceMap;
	protected Button editRequirementEquivalenceMap;



	/**
	 * For {@link ISection} use only.
	 */
	public RequirementEquivalenceMapPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public RequirementEquivalenceMapPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence requirementEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = requirementEquivalenceMapStep.addStep(RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence);
		// End IRR
		propertiesStep.addStep(RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_);
		
		
		composer = new PartComposer(requirementEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence) {
					return createEquivalenceTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_) {
					return createRequirementEquivalenceMapTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(RefframeworkMessages.RequirementEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEquivalenceTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.equivalence = new ReferencesTable(getDescription(RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence, RefframeworkMessages.RequirementEquivalenceMapPropertiesEditionPart_EquivalenceLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RequirementEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				equivalence.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RequirementEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				equivalence.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RequirementEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				equivalence.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RequirementEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				equivalence.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.equivalenceFilters) {
			this.equivalence.addFilter(filter);
		}
		this.equivalence.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence, RefframeworkViewsRepository.FORM_KIND));
		this.equivalence.createControls(parent, widgetFactory);
		this.equivalence.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RequirementEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData equivalenceData = new GridData(GridData.FILL_HORIZONTAL);
		equivalenceData.horizontalSpan = 3;
		this.equivalence.setLayoutData(equivalenceData);
		this.equivalence.setLowerBound(0);
		this.equivalence.setUpperBound(-1);
		equivalence.setID(RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence);
		equivalence.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEquivalenceTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRequirementEquivalenceMapTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableRequirementEquivalenceMap = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableRequirementEquivalenceMap.setHeaderVisible(true);
		GridData gdRequirementEquivalenceMap = new GridData();
		gdRequirementEquivalenceMap.grabExcessHorizontalSpace = true;
		gdRequirementEquivalenceMap.horizontalAlignment = GridData.FILL;
		gdRequirementEquivalenceMap.grabExcessVerticalSpace = true;
		gdRequirementEquivalenceMap.verticalAlignment = GridData.FILL;
		tableRequirementEquivalenceMap.setLayoutData(gdRequirementEquivalenceMap);
		tableRequirementEquivalenceMap.setLinesVisible(true);

		// Start of user code for columns definition for
		// RequirementEquivalenceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableRequirementEquivalenceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */

		TableColumn name = new TableColumn(tableRequirementEquivalenceMap,
				SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableRequirementEquivalenceMap,
				SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableRequirementEquivalenceMap,
				SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Requirements"); //$NON-NLS-1$
		// End IRR
		// End of user code

		requirementEquivalenceMap = new TableViewer(tableRequirementEquivalenceMap);
		requirementEquivalenceMap.setContentProvider(new ArrayContentProvider());
		requirementEquivalenceMap.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for RequirementEquivalenceMap
			public String getColumnText(Object object, int columnIndex) {
				//Start IRR
				/*
				AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
				if (object instanceof EObject) {
					switch (columnIndex) {
					case 0:
						return labelProvider.getText(object);
					}
				}
				*/
				if (object instanceof EObject) {
					RefEquivalenceMap refEquivalenceMap = (RefEquivalenceMap)object;
					switch (columnIndex) {
					case 0:							
						return refEquivalenceMap.getId();
					case 1:							
						if (refEquivalenceMap.getMapGroup() == null)
							return "";
						else
							return refEquivalenceMap.getMapGroup().getName();
					case 2:							
						if (refEquivalenceMap.getTarget() == null)
							return "";
						else{
							EList<RefAssurableElement> LstElement = refEquivalenceMap.getTarget();
							Iterator<RefAssurableElement> iter = LstElement.iterator();
							String sElement = "[";
							
							// Start IRR 20141205
							
							EObject object1 = null;
							while (iter.hasNext()) {
								object1 = iter.next();
								if (object1 instanceof RefRequirement) {
									RefRequirement refElement = (RefRequirement) object1;
									if (!(refEquivalenceMap.cdoResource().toString().equals(refElement.cdoResource().toString()))) {
										sElement = sElement + refElement.getName() + ","; 
									}
								}
							}
							
							// End IRR 20141205
							
							//delete ,
							sElement = sElement.substring(0, sElement.length()-1);
							sElement = sElement + "]";
							return sElement;
						}
							
					}
				}
				//End IRR
				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		requirementEquivalenceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (requirementEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) requirementEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RequirementEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						requirementEquivalenceMap.refresh();
					}
				}
			}

		});
		GridData requirementEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		requirementEquivalenceMapData.minimumHeight = 120;
		requirementEquivalenceMapData.heightHint = 120;
		requirementEquivalenceMap.getTable().setLayoutData(requirementEquivalenceMapData);
		for (ViewerFilter filter : this.requirementEquivalenceMapFilters) {
			requirementEquivalenceMap.addFilter(filter);
		}
		EditingUtils.setID(requirementEquivalenceMap.getTable(), RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_);
		EditingUtils.setEEFtype(requirementEquivalenceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createRequirementEquivalenceMapPanel(widgetFactory, tableContainer);
		// Start of user code for createRequirementEquivalenceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRequirementEquivalenceMapPanel(FormToolkit widgetFactory, Composite container) {
		Composite requirementEquivalenceMapPanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout requirementEquivalenceMapPanelLayout = new GridLayout();
		requirementEquivalenceMapPanelLayout.numColumns = 1;
		requirementEquivalenceMapPanel.setLayout(requirementEquivalenceMapPanelLayout);
		addRequirementEquivalenceMap = widgetFactory.createButton(requirementEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addRequirementEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addRequirementEquivalenceMap.setLayoutData(addRequirementEquivalenceMapData);
		addRequirementEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RequirementEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				requirementEquivalenceMap.refresh();
			}
		});
		EditingUtils.setID(addRequirementEquivalenceMap, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_);
		EditingUtils.setEEFtype(addRequirementEquivalenceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeRequirementEquivalenceMap = widgetFactory.createButton(requirementEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeRequirementEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeRequirementEquivalenceMap.setLayoutData(removeRequirementEquivalenceMapData);
		removeRequirementEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (requirementEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) requirementEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RequirementEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						requirementEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeRequirementEquivalenceMap, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_);
		EditingUtils.setEEFtype(removeRequirementEquivalenceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editRequirementEquivalenceMap = widgetFactory.createButton(requirementEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editRequirementEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editRequirementEquivalenceMap.setLayoutData(editRequirementEquivalenceMapData);
		editRequirementEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (requirementEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) requirementEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RequirementEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						requirementEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editRequirementEquivalenceMap, RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_);
		EditingUtils.setEEFtype(editRequirementEquivalenceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createRequirementEquivalenceMapPanel

		// End of user code
		return requirementEquivalenceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart#initEquivalence(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEquivalence(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		equivalence.setContentProvider(contentProvider);
		equivalence.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.equivalence);
		if (eefElementEditorReadOnlyState && equivalence.isEnabled()) {
			equivalence.setEnabled(false);
			equivalence.setToolTipText(RefframeworkMessages.RequirementEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !equivalence.isEnabled()) {
			equivalence.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart#updateEquivalence()
	 * 
	 */
	public void updateEquivalence() {
	equivalence.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart#addFilterEquivalence(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEquivalence(ViewerFilter filter) {
		equivalenceFilters.add(filter);
		if (this.equivalence != null) {
			this.equivalence.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart#addBusinessFilterEquivalence(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEquivalence(ViewerFilter filter) {
		equivalenceBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart#isContainedInEquivalenceTable(EObject element)
	 * 
	 */
	public boolean isContainedInEquivalenceTable(EObject element) {
		return ((ReferencesTableSettings)equivalence.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart#initRequirementEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initRequirementEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		requirementEquivalenceMap.setContentProvider(contentProvider);
		requirementEquivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RequirementEquivalenceMap.Properties.requirementEquivalenceMap_);
		if (eefElementEditorReadOnlyState && requirementEquivalenceMap.getTable().isEnabled()) {
			requirementEquivalenceMap.getTable().setEnabled(false);
			requirementEquivalenceMap.getTable().setToolTipText(RefframeworkMessages.RequirementEquivalenceMap_ReadOnly);
			addRequirementEquivalenceMap.setEnabled(false);
			addRequirementEquivalenceMap.setToolTipText(RefframeworkMessages.RequirementEquivalenceMap_ReadOnly);
			removeRequirementEquivalenceMap.setEnabled(false);
			removeRequirementEquivalenceMap.setToolTipText(RefframeworkMessages.RequirementEquivalenceMap_ReadOnly);
			editRequirementEquivalenceMap.setEnabled(false);
			editRequirementEquivalenceMap.setToolTipText(RefframeworkMessages.RequirementEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !requirementEquivalenceMap.getTable().isEnabled()) {
			requirementEquivalenceMap.getTable().setEnabled(true);
			addRequirementEquivalenceMap.setEnabled(true);
			removeRequirementEquivalenceMap.setEnabled(true);
			editRequirementEquivalenceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart#updateRequirementEquivalenceMap()
	 * 
	 */
	public void updateRequirementEquivalenceMap() {
	requirementEquivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart#addFilterRequirementEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToRequirementEquivalenceMap(ViewerFilter filter) {
		requirementEquivalenceMapFilters.add(filter);
		if (this.requirementEquivalenceMap != null) {
			this.requirementEquivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart#addBusinessFilterRequirementEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToRequirementEquivalenceMap(ViewerFilter filter) {
		requirementEquivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RequirementEquivalenceMapPropertiesEditionPart#isContainedInRequirementEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInRequirementEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)requirementEquivalenceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.RequirementEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
