/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.actions;

import java.util.Objects;

import org.eclipse.amalgam.explorer.activity.ui.api.actions.OpenSessionAction;
import org.eclipse.amalgam.explorer.activity.ui.api.editor.ActivityExplorerEditor;
import org.eclipse.amalgam.explorer.activity.ui.api.editor.pages.helper.SessionHelper;
import org.eclipse.amalgam.explorer.activity.ui.api.manager.ActivityExplorerManager;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.opencert.infra.dashboard.DashboardActivator;
import org.eclipse.opencert.infra.dashboard.helpers.ChessProjectVisitor;
import org.eclipse.opencert.infra.dashboard.helpers.OpenCertExtensions;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;

/**
 * Action to open the Activity Explorer for the current graphical file. This action is overridden to be able to open the
 * activity explorer even if the related session is not opened for the corresponding graphical file.
 */
public class OpenDashboardAction
		extends org.eclipse.amalgam.explorer.activity.ui.api.actions.OpenActivityExplorerAction {

	/**
	 * Default constructor.
	 */
	protected OpenDashboardAction() {
		super();
	}

	/**
	 * {@inheritDoc}
	 *
	 * Overridden to be able to display the action even if no Sirius session is
	 * currently open. Moreover, this action must be displayed if a Session is
	 * opened, but without the corresponding activity explorer. Otherwise, a "Go
	 * To Activity Explorer" action must be used instead.
	 */
	@Override
	public boolean canAddedToMenu() {
		boolean vDisplayAction = false;

		// Try to find an open session
		Session vSession = getSession();

		if (Objects.nonNull(vSession)) {
			// A session is already open, check if the corresponding activity
			// explorer editor is also open
			ActivityExplorerEditor vActivityExplorerEditor = ActivityExplorerManager.INSTANCE
					.getEditorFromSession(vSession);
			vDisplayAction = Objects.isNull(vActivityExplorerEditor);

		} else {
			// If no session is currently open, check if the selection allows to
			// find a graphical representation file
			vDisplayAction = Objects.nonNull(findGraphicalFileFromSelection());
		}

		return vDisplayAction;
	}

	/**
	 * {@inheritDoc}
	 *
	 * Overridden to enable the action as soon as a graphical file can be found
	 * from the selection.
	 */
	@Override
	protected boolean updateSelection(final IStructuredSelection pSelection) {
		return Objects.nonNull(findGraphicalFileFromSelection());
	}

	/**
	 * {@inheritDoc}
	 *
	 * It can be retrieved from an aird file or from an {@link IProject}.
	 *
	 * NB: This must search an already open session to preserve the standard
	 * behaviour of this method, and never create a session automatically here.
	 *
	 * @return The session or <code>null</code> if not found
	 */
	@Override
	protected Session getSession() {
		Session vSession = null;

		// Try to find the graphical file corresponding to the selection
		IFile vSelectedGraphicalFile = findGraphicalFileFromSelection();

		if (Objects.nonNull(vSelectedGraphicalFile)) {
			// Search for an open session
			vSession = SessionHelper.getSessionForDiagramFile(vSelectedGraphicalFile);
		}

		return vSession;
	}

	/**
	 * Find the graphical file related to the current selection. It can be the
	 * selected element directly, or the graphical file contained in a selected
	 * project.
	 *
	 * @return The graphical file found
	 */
	private IFile findGraphicalFileFromSelection() {
		IFile vGraphicalFile = null;

		IStructuredSelection vSelection = getStructuredSelection();
		if (!vSelection.isEmpty()) {
			Object vSelectedElement = vSelection.getFirstElement();

			// If the given selection is a local resource, it should belonged to CHESS project
			if (vSelectedElement instanceof IResource) {
				// If the given selection is a DI graphical file directly
				if (vSelectedElement instanceof IFile
						&& OpenCertExtensions.DI_EXTENSION.equals(((IFile) vSelectedElement).getFileExtension())) {
					vGraphicalFile = (IFile) vSelectedElement;

				} else if (vSelectedElement instanceof IProject && ((IProject) vSelectedElement).isOpen()) {
					// If the selection is a project, search for the graphical file DI at the first level
					ChessProjectVisitor vVisitor = new ChessProjectVisitor();

					try {
						((IProject) vSelectedElement).accept(vVisitor, IResource.DEPTH_ONE, IResource.NONE);
					} catch (final CoreException pException) {
						DashboardActivator.getDefault().getLog().log(pException.getStatus());
					}

					vGraphicalFile = vVisitor.getFirstGraphicalFile();
				}
			} else if (vSelectedElement instanceof CDOResource) {
				// Otherwise, it should belonged to OpenCert project in the CDO Repository

				// TODO: Get the graphical file from the current CDO project
			}
		}

		return vGraphicalFile;
	}

	/**
	 * {@inheritDoc}
	 *
	 * Overridden to be able to create a new session if none is open, and then
	 * display the activity explorer.
	 */
	@Override
	public void run() {
		// First try to find an open session
		Session vSession = getSession();

		// If no session is found, try to create a new one
		if (Objects.isNull(vSession)) {
			IFile vSelectedGraphicalFile = findGraphicalFileFromSelection();

			if (Objects.nonNull(vSelectedGraphicalFile)) {
				URI vGraphicalFileURI = URI.createPlatformResourceURI(vSelectedGraphicalFile.getFullPath().toOSString(),
						true);

				vSession = SessionManager.INSTANCE.openSession(vGraphicalFileURI, new NullProgressMonitor(), null);
			}
		}

		if (Objects.nonNull(vSession)) {
			OpenSessionAction.openActivityExplorer(vSession);
		}
	}
}
