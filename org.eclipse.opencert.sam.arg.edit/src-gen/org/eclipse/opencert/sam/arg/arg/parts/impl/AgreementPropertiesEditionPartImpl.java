/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart;
import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;

import org.eclipse.opencert.sam.arg.arg.providers.ArgMessages;

// End of user code

/**
 * 
 * 
 */
public class AgreementPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, AgreementPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text content;
	protected ReferencesTable evaluation;
	protected List<ViewerFilter> evaluationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> evaluationFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable lifecycleEvent;
	protected List<ViewerFilter> lifecycleEventBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> lifecycleEventFilters = new ArrayList<ViewerFilter>();
	protected Text location;
	protected ReferencesTable argumentation;
	protected List<ViewerFilter> argumentationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> argumentationFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable consistOf;
	protected List<ViewerFilter> consistOfBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> consistOfFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable between;
	protected List<ViewerFilter> betweenBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> betweenFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public AgreementPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence agreementStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = agreementStep.addStep(ArgViewsRepository.Agreement.Properties.class);
		propertiesStep.addStep(ArgViewsRepository.Agreement.Properties.id);
		propertiesStep.addStep(ArgViewsRepository.Agreement.Properties.name);
		propertiesStep.addStep(ArgViewsRepository.Agreement.Properties.description);
		propertiesStep.addStep(ArgViewsRepository.Agreement.Properties.content);
		propertiesStep.addStep(ArgViewsRepository.Agreement.Properties.evaluation);
		propertiesStep.addStep(ArgViewsRepository.Agreement.Properties.lifecycleEvent);
		propertiesStep.addStep(ArgViewsRepository.Agreement.Properties.location);
		propertiesStep.addStep(ArgViewsRepository.Agreement.Properties.argumentation);
		propertiesStep.addStep(ArgViewsRepository.Agreement.Properties.consistOf);
		propertiesStep.addStep(ArgViewsRepository.Agreement.Properties.between);
		
		
		composer = new PartComposer(agreementStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ArgViewsRepository.Agreement.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == ArgViewsRepository.Agreement.Properties.id) {
					return createIdText(parent);
				}
				if (key == ArgViewsRepository.Agreement.Properties.name) {
					return createNameText(parent);
				}
				if (key == ArgViewsRepository.Agreement.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == ArgViewsRepository.Agreement.Properties.content) {
					return createContentText(parent);
				}
				if (key == ArgViewsRepository.Agreement.Properties.evaluation) {
					return createEvaluationAdvancedTableComposition(parent);
				}
				if (key == ArgViewsRepository.Agreement.Properties.lifecycleEvent) {
					return createLifecycleEventAdvancedTableComposition(parent);
				}
				if (key == ArgViewsRepository.Agreement.Properties.location) {
					return createLocationText(parent);
				}
				if (key == ArgViewsRepository.Agreement.Properties.argumentation) {
					return createArgumentationAdvancedReferencesTable(parent);
				}
				if (key == ArgViewsRepository.Agreement.Properties.consistOf) {
					return createConsistOfAdvancedReferencesTable(parent);
				}
				if (key == ArgViewsRepository.Agreement.Properties.between) {
					return createBetweenAdvancedReferencesTable(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(ArgMessages.AgreementPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, ArgViewsRepository.Agreement.Properties.id, ArgMessages.AgreementPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, ArgViewsRepository.Agreement.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.Agreement.Properties.id, ArgViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, ArgViewsRepository.Agreement.Properties.name, ArgMessages.AgreementPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, ArgViewsRepository.Agreement.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.Agreement.Properties.name, ArgViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, ArgViewsRepository.Agreement.Properties.description, ArgMessages.AgreementPropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, ArgViewsRepository.Agreement.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.Agreement.Properties.description, ArgViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createContentText(Composite parent) {
		createDescription(parent, ArgViewsRepository.Agreement.Properties.content, ArgMessages.AgreementPropertiesEditionPart_ContentLabel);
		content = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData contentData = new GridData(GridData.FILL_HORIZONTAL);
		content.setLayoutData(contentData);
		content.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.content, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, content.getText()));
			}

		});
		content.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.content, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, content.getText()));
				}
			}

		});
		EditingUtils.setID(content, ArgViewsRepository.Agreement.Properties.content);
		EditingUtils.setEEFtype(content, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.Agreement.Properties.content, ArgViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createContentText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEvaluationAdvancedTableComposition(Composite parent) {
		this.evaluation = new ReferencesTable(getDescription(ArgViewsRepository.Agreement.Properties.evaluation, ArgMessages.AgreementPropertiesEditionPart_EvaluationLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				evaluation.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				evaluation.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				evaluation.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				evaluation.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.evaluationFilters) {
			this.evaluation.addFilter(filter);
		}
		this.evaluation.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Agreement.Properties.evaluation, ArgViewsRepository.SWT_KIND));
		this.evaluation.createControls(parent);
		this.evaluation.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.evaluation, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData evaluationData = new GridData(GridData.FILL_HORIZONTAL);
		evaluationData.horizontalSpan = 3;
		this.evaluation.setLayoutData(evaluationData);
		this.evaluation.setLowerBound(0);
		this.evaluation.setUpperBound(-1);
		evaluation.setID(ArgViewsRepository.Agreement.Properties.evaluation);
		evaluation.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEvaluationAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createLifecycleEventAdvancedTableComposition(Composite parent) {
		this.lifecycleEvent = new ReferencesTable(getDescription(ArgViewsRepository.Agreement.Properties.lifecycleEvent, ArgMessages.AgreementPropertiesEditionPart_LifecycleEventLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				lifecycleEvent.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				lifecycleEvent.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				lifecycleEvent.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				lifecycleEvent.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.lifecycleEventFilters) {
			this.lifecycleEvent.addFilter(filter);
		}
		this.lifecycleEvent.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Agreement.Properties.lifecycleEvent, ArgViewsRepository.SWT_KIND));
		this.lifecycleEvent.createControls(parent);
		this.lifecycleEvent.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.lifecycleEvent, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData lifecycleEventData = new GridData(GridData.FILL_HORIZONTAL);
		lifecycleEventData.horizontalSpan = 3;
		this.lifecycleEvent.setLayoutData(lifecycleEventData);
		this.lifecycleEvent.setLowerBound(0);
		this.lifecycleEvent.setUpperBound(-1);
		lifecycleEvent.setID(ArgViewsRepository.Agreement.Properties.lifecycleEvent);
		lifecycleEvent.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createLifecycleEventAdvancedTableComposition

		// End of user code
		return parent;
	}

	
	protected Composite createLocationText(Composite parent) {
		createDescription(parent, ArgViewsRepository.Agreement.Properties.location, ArgMessages.AgreementPropertiesEditionPart_LocationLabel);
		location = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData locationData = new GridData(GridData.FILL_HORIZONTAL);
		location.setLayoutData(locationData);
		location.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
			}

		});
		location.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
				}
			}

		});
		EditingUtils.setID(location, ArgViewsRepository.Agreement.Properties.location);
		EditingUtils.setEEFtype(location, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.Agreement.Properties.location, ArgViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createLocationText

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createArgumentationAdvancedReferencesTable(Composite parent) {
		String label = getDescription(ArgViewsRepository.Agreement.Properties.argumentation, ArgMessages.AgreementPropertiesEditionPart_ArgumentationLabel);		 
		this.argumentation = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addArgumentation(); }
			public void handleEdit(EObject element) { editArgumentation(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveArgumentation(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromArgumentation(element); }
			public void navigateTo(EObject element) { }
		});
		this.argumentation.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Agreement.Properties.argumentation, ArgViewsRepository.SWT_KIND));
		this.argumentation.createControls(parent);
		this.argumentation.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.argumentation, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData argumentationData = new GridData(GridData.FILL_HORIZONTAL);
		argumentationData.horizontalSpan = 3;
		this.argumentation.setLayoutData(argumentationData);
		this.argumentation.disableMove();
		argumentation.setID(ArgViewsRepository.Agreement.Properties.argumentation);
		argumentation.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addArgumentation() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(argumentation.getInput(), argumentationFilters, argumentationBusinessFilters,
		"argumentation", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.argumentation,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				argumentation.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveArgumentation(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.argumentation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		argumentation.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromArgumentation(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.argumentation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		argumentation.refresh();
	}

	/**
	 * 
	 */
	protected void editArgumentation(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				argumentation.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createConsistOfAdvancedReferencesTable(Composite parent) {
		String label = getDescription(ArgViewsRepository.Agreement.Properties.consistOf, ArgMessages.AgreementPropertiesEditionPart_ConsistOfLabel);		 
		this.consistOf = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addConsistOf(); }
			public void handleEdit(EObject element) { editConsistOf(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveConsistOf(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromConsistOf(element); }
			public void navigateTo(EObject element) { }
		});
		this.consistOf.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Agreement.Properties.consistOf, ArgViewsRepository.SWT_KIND));
		this.consistOf.createControls(parent);
		this.consistOf.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.consistOf, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData consistOfData = new GridData(GridData.FILL_HORIZONTAL);
		consistOfData.horizontalSpan = 3;
		this.consistOf.setLayoutData(consistOfData);
		this.consistOf.disableMove();
		consistOf.setID(ArgViewsRepository.Agreement.Properties.consistOf);
		consistOf.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addConsistOf() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(consistOf.getInput(), consistOfFilters, consistOfBusinessFilters,
		"consistOf", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.consistOf,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				consistOf.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveConsistOf(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.consistOf, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		consistOf.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromConsistOf(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.consistOf, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		consistOf.refresh();
	}

	/**
	 * 
	 */
	protected void editConsistOf(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				consistOf.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createBetweenAdvancedReferencesTable(Composite parent) {
		String label = getDescription(ArgViewsRepository.Agreement.Properties.between, ArgMessages.AgreementPropertiesEditionPart_BetweenLabel);		 
		this.between = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addBetween(); }
			public void handleEdit(EObject element) { editBetween(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveBetween(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromBetween(element); }
			public void navigateTo(EObject element) { }
		});
		this.between.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Agreement.Properties.between, ArgViewsRepository.SWT_KIND));
		this.between.createControls(parent);
		this.between.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.between, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData betweenData = new GridData(GridData.FILL_HORIZONTAL);
		betweenData.horizontalSpan = 3;
		this.between.setLayoutData(betweenData);
		this.between.disableMove();
		between.setID(ArgViewsRepository.Agreement.Properties.between);
		between.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addBetween() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(between.getInput(), betweenFilters, betweenBusinessFilters,
		"between", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.between,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				between.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveBetween(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.between, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		between.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromBetween(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AgreementPropertiesEditionPartImpl.this, ArgViewsRepository.Agreement.Properties.between, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		between.refresh();
	}

	/**
	 * 
	 */
	protected void editBetween(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				between.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Agreement.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(ArgMessages.Agreement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Agreement.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(ArgMessages.Agreement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Agreement.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(ArgMessages.Agreement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#getContent()
	 * 
	 */
	public String getContent() {
		return content.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#setContent(String newValue)
	 * 
	 */
	public void setContent(String newValue) {
		if (newValue != null) {
			content.setText(newValue);
		} else {
			content.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Agreement.Properties.content);
		if (eefElementEditorReadOnlyState && content.isEnabled()) {
			content.setEnabled(false);
			content.setToolTipText(ArgMessages.Agreement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !content.isEnabled()) {
			content.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#initEvaluation(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEvaluation(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		evaluation.setContentProvider(contentProvider);
		evaluation.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Agreement.Properties.evaluation);
		if (eefElementEditorReadOnlyState && evaluation.isEnabled()) {
			evaluation.setEnabled(false);
			evaluation.setToolTipText(ArgMessages.Agreement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !evaluation.isEnabled()) {
			evaluation.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#updateEvaluation()
	 * 
	 */
	public void updateEvaluation() {
	evaluation.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#addFilterEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEvaluation(ViewerFilter filter) {
		evaluationFilters.add(filter);
		if (this.evaluation != null) {
			this.evaluation.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#addBusinessFilterEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEvaluation(ViewerFilter filter) {
		evaluationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#isContainedInEvaluationTable(EObject element)
	 * 
	 */
	public boolean isContainedInEvaluationTable(EObject element) {
		return ((ReferencesTableSettings)evaluation.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#initLifecycleEvent(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initLifecycleEvent(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		lifecycleEvent.setContentProvider(contentProvider);
		lifecycleEvent.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Agreement.Properties.lifecycleEvent);
		if (eefElementEditorReadOnlyState && lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(false);
			lifecycleEvent.setToolTipText(ArgMessages.Agreement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#updateLifecycleEvent()
	 * 
	 */
	public void updateLifecycleEvent() {
	lifecycleEvent.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#addFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventFilters.add(filter);
		if (this.lifecycleEvent != null) {
			this.lifecycleEvent.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#addBusinessFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#isContainedInLifecycleEventTable(EObject element)
	 * 
	 */
	public boolean isContainedInLifecycleEventTable(EObject element) {
		return ((ReferencesTableSettings)lifecycleEvent.getInput()).contains(element);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#getLocation()
	 * 
	 */
	public String getLocation() {
		return location.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#setLocation(String newValue)
	 * 
	 */
	public void setLocation(String newValue) {
		if (newValue != null) {
			location.setText(newValue);
		} else {
			location.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Agreement.Properties.location);
		if (eefElementEditorReadOnlyState && location.isEnabled()) {
			location.setEnabled(false);
			location.setToolTipText(ArgMessages.Agreement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !location.isEnabled()) {
			location.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#initArgumentation(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initArgumentation(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		argumentation.setContentProvider(contentProvider);
		argumentation.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Agreement.Properties.argumentation);
		if (eefElementEditorReadOnlyState && argumentation.getTable().isEnabled()) {
			argumentation.setEnabled(false);
			argumentation.setToolTipText(ArgMessages.Agreement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !argumentation.getTable().isEnabled()) {
			argumentation.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#updateArgumentation()
	 * 
	 */
	public void updateArgumentation() {
	argumentation.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#addFilterArgumentation(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArgumentation(ViewerFilter filter) {
		argumentationFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#addBusinessFilterArgumentation(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArgumentation(ViewerFilter filter) {
		argumentationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#isContainedInArgumentationTable(EObject element)
	 * 
	 */
	public boolean isContainedInArgumentationTable(EObject element) {
		return ((ReferencesTableSettings)argumentation.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#initConsistOf(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initConsistOf(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		consistOf.setContentProvider(contentProvider);
		consistOf.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Agreement.Properties.consistOf);
		if (eefElementEditorReadOnlyState && consistOf.getTable().isEnabled()) {
			consistOf.setEnabled(false);
			consistOf.setToolTipText(ArgMessages.Agreement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !consistOf.getTable().isEnabled()) {
			consistOf.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#updateConsistOf()
	 * 
	 */
	public void updateConsistOf() {
	consistOf.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#addFilterConsistOf(ViewerFilter filter)
	 * 
	 */
	public void addFilterToConsistOf(ViewerFilter filter) {
		consistOfFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#addBusinessFilterConsistOf(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToConsistOf(ViewerFilter filter) {
		consistOfBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#isContainedInConsistOfTable(EObject element)
	 * 
	 */
	public boolean isContainedInConsistOfTable(EObject element) {
		return ((ReferencesTableSettings)consistOf.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#initBetween(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initBetween(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		between.setContentProvider(contentProvider);
		between.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Agreement.Properties.between);
		if (eefElementEditorReadOnlyState && between.getTable().isEnabled()) {
			between.setEnabled(false);
			between.setToolTipText(ArgMessages.Agreement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !between.getTable().isEnabled()) {
			between.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#updateBetween()
	 * 
	 */
	public void updateBetween() {
	between.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#addFilterBetween(ViewerFilter filter)
	 * 
	 */
	public void addFilterToBetween(ViewerFilter filter) {
		betweenFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#addBusinessFilterBetween(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToBetween(ViewerFilter filter) {
		betweenBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart#isContainedInBetweenTable(EObject element)
	 * 
	 */
	public boolean isContainedInBetweenTable(EObject element) {
		return ((ReferencesTableSettings)between.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ArgMessages.Agreement_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
