/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.properties.property.parts;

/**
 * 
 * 
 */
public class PropertyViewsRepository {

	public static final int SWT_KIND = 0;

	public static final int FORM_KIND = 1;


	/**
	 * PropertyModel view descriptor
	 * 
	 */
	public static class PropertyModel {
		public static class Properties {
	
			
			public static String id = "property::PropertyModel::properties::id";
			
			
			public static String name = "property::PropertyModel::properties::name";
			
			
			public static String description = "property::PropertyModel::properties::description";
			
			
			public static String hasProperty = "property::PropertyModel::properties::hasProperty";
			
	
		}
	
	}

	/**
	 * Property view descriptor
	 * 
	 */
	public static class Property_ {
		public static class Properties {
	
			
			public static String id = "property::Property_::properties::id";
			
			
			public static String name = "property::Property_::properties::name";
			
			
			public static String datatype = "property::Property_::properties::datatype";
			
			
			public static String enumValues = "property::Property_::properties::enumValues";
			
			
			public static String unit = "property::Property_::properties::unit";
			
	
		}
	
	}

}
