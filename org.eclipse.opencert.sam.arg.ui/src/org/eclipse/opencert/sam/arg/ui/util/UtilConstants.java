/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.util;

/**
 * Constant definitions for plug-in Util
 */
public class UtilConstants {

	public static final String Module_PATH = "modulePathPreference";
	public static final String Pattern_PATH = "patternPathPreference";
	public static final String Agreement_PATH = "agreementPathPreference";	

	public final static String ARGFILE_MODEL_ID = ".arg";
	public final static String GSNFILE_MODEL_ID = ".gsn1";
	
	public final static String ARGFILE_DIAGRAM_ID = ".arg_diagram";
	public final static String GSNFILE_DIAGRAM_ID = ".gsn1_diagram";
	
	public final static String ORG_OPENCERT_SAM_AGREE_MODEL_ID = ".agree";
	public final static String ORG_OPENCERT_SAM_AGREE = "org.eclipse.opencert.sam.Agree";
	
	public static final int MAX_SUB_DIR = 100;
	
}
