/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts;

/**
 * 
 * 
 */
public class ProcessViewsRepository {

	public static final int SWT_KIND = 0;

	public static final int FORM_KIND = 1;


	/**
	 * ProcessModel view descriptor
	 * 
	 */
	public static class ProcessModel {
		public static class Properties {
	
			
			public static String id = "process::ProcessModel::properties::id";
			
			
			public static String name = "process::ProcessModel::properties::name";
			
			
			public static String description = "process::ProcessModel::properties::description";
			
			
			public static String ownedActivity = "process::ProcessModel::properties::ownedActivity";
			
			
			public static String ownedParticipant = "process::ProcessModel::properties::ownedParticipant";
			
			
			public static String ownedTechnique = "process::ProcessModel::properties::ownedTechnique";
			
	
		}
	
	}

	/**
	 * Activity view descriptor
	 * 
	 */
	public static class Activity {
		public static class Properties {
	
			
			public static String id = "process::Activity::properties::id";
			
			
			public static String name = "process::Activity::properties::name";
			
			
			public static String description = "process::Activity::properties::description";
			
			
			public static String startTime = "process::Activity::properties::startTime";
			
			
			public static String endTime = "process::Activity::properties::endTime";
			
			
			public static String subActivity = "process::Activity::properties::subActivity";
			
			
			public static String precedingActivity = "process::Activity::properties::precedingActivity";
			
			
			public static String ownedRel = "process::Activity::properties::ownedRel";
			
	
		}
	
	}

	/**
	 * Participant view descriptor
	 * 
	 */
	public static class Participant {
		public static class Properties {
	
			
			public static String id = "process::Participant::properties::id";
			
			
			public static String name = "process::Participant::properties::name";
			
			
			public static String description = "process::Participant::properties::description";
			
			
			public static String ownedArtefact = "process::Participant::properties::ownedArtefact";
			
			
			public static String triggeredAssetEvent = "process::Participant::properties::triggeredAssetEvent";
			
	
		}
	
	}

	/**
	 * Technique view descriptor
	 * 
	 */
	public static class Technique {
		public static class Properties {
	
			
			public static String id = "process::Technique::properties::id";
			
			
			public static String name = "process::Technique::properties::name";
			
			
			public static String description = "process::Technique::properties::description";
			
			
			public static String createdArtefact = "process::Technique::properties::createdArtefact";
			
	
		}
	
	}

	/**
	 * Person view descriptor
	 * 
	 */
	public static class Person {
		public static class Properties {
	
			
			public static String id = "process::Person::properties::id";
			
			
			public static String name = "process::Person::properties::name";
			
			
			public static String description = "process::Person::properties::description";
			
			
			public static String ownedArtefact = "process::Person::properties::ownedArtefact";
			
			
			public static String triggeredAssetEvent = "process::Person::properties::triggeredAssetEvent";
			
			
			public static String email = "process::Person::properties::email";
			
			
			public static String organization = "process::Person::properties::organization";
			
	
		}
	
	}

	/**
	 * Tool view descriptor
	 * 
	 */
	public static class Tool {
		public static class Properties {
	
			
			public static String id = "process::Tool::properties::id";
			
			
			public static String name = "process::Tool::properties::name";
			
			
			public static String description = "process::Tool::properties::description";
			
			
			public static String ownedArtefact = "process::Tool::properties::ownedArtefact";
			
			
			public static String triggeredAssetEvent = "process::Tool::properties::triggeredAssetEvent";
			
			
			public static String version = "process::Tool::properties::version";
			
	
		}
	
	}

	/**
	 * Organization view descriptor
	 * 
	 */
	public static class Organization {
		public static class Properties {
	
			
			public static String id = "process::Organization::properties::id";
			
			
			public static String name = "process::Organization::properties::name";
			
			
			public static String description = "process::Organization::properties::description";
			
			
			public static String ownedArtefact = "process::Organization::properties::ownedArtefact";
			
			
			public static String triggeredAssetEvent = "process::Organization::properties::triggeredAssetEvent";
			
			
			public static String address = "process::Organization::properties::address";
			
			
			public static String subOrganization = "process::Organization::properties::subOrganization";
			
	
		}
	
	}

	/**
	 * ActivityRel view descriptor
	 * 
	 */
	public static class ActivityRel {
		public static class Properties {
	
			
			public static String target = "process::ActivityRel::properties::target";
			
			
			public static String source = "process::ActivityRel::properties::source";
			
			
			public static String type = "process::ActivityRel::properties::type";
			
	
		}
	
	}

	/**
	 * Activity Artefacts view descriptor
	 * 
	 */
	public static class ActivityArtefacts {
		public static class Properties {
	
			
			public static String requiredArtefact = "process::Activity Artefacts::properties::requiredArtefact";
			
			
			public static String producedArtefact = "process::Activity Artefacts::properties::producedArtefact";
			
	
		}
	
	}

	/**
	 * Activity Participants view descriptor
	 * 
	 */
	public static class ActivityParticipants {
		public static class Properties {
	
			
			public static String participant = "process::Activity Participants::properties::participant";
			
	
		}
	
	}

	/**
	 * Activity Techniques view descriptor
	 * 
	 */
	public static class ActivityTechniques {
		public static class Properties {
	
			
			public static String technique = "process::Activity Techniques::properties::technique";
			
	
		}
	
	}

	/**
	 * Activity Evaluation view descriptor
	 * 
	 */
	public static class ActivityEvaluation {
		public static class Properties {
	
			
			public static String evaluation = "process::Activity Evaluation::properties::evaluation";
			
	
		}
	
	}

	/**
	 * Activity Events view descriptor
	 * 
	 */
	public static class ActivityEvents {
		public static class Properties {
	
			
			public static String lifecycleEvent = "process::Activity Events::properties::lifecycleEvent";
			
	
		}
	
	}

	/**
	 * Activity AssuranceAsset view descriptor
	 * 
	 */
	public static class ActivityAssuranceAsset {
		public static class Properties {
	
			
			public static String assetEvent = "process::Activity AssuranceAsset::properties::assetEvent";
			
	
		}
	
	}

}
