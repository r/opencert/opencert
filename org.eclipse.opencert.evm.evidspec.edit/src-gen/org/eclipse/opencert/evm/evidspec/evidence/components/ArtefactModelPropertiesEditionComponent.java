/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class ArtefactModelPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for artefact ReferencesTable
	 */
	protected ReferencesTableSettings artefactSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ArtefactModelPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject artefactModel, String editing_mode) {
		super(editingContext, artefactModel, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = EvidenceViewsRepository.class;
		partKey = EvidenceViewsRepository.ArtefactModel.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final ArtefactModel artefactModel = (ArtefactModel)elt;
			final ArtefactModelPropertiesEditionPart basePart = (ArtefactModelPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, artefactModel.getId()));
			
			if (isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, artefactModel.getName()));
			
			if (isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, artefactModel.getDescription()));
			
			if (isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.artefact)) {
				artefactSettings = new ReferencesTableSettings(artefactModel, EvidencePackage.eINSTANCE.getArtefactModel_Artefact());
				basePart.initArtefact(artefactSettings);
			}
			if (isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.repoUrl))
				basePart.setRepoUrl(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, artefactModel.getRepoUrl()));
			
			if (isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.repoUser))
				basePart.setRepoUser(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, artefactModel.getRepoUser()));
			
			if (isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.repoPassword))
				basePart.setRepoPassword(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, artefactModel.getRepoPassword()));
			
			if (isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath))
				basePart.setRepoLocalPath(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, artefactModel.getRepoLocalPath()));
			
			if (isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal)) {
				basePart.setRepoUsesLocal(artefactModel.isRepoUsesLocal());
			}
			// init filters
			
			
			
			if (isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.artefact)) {
				basePart.addFilterToArtefact(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof ArtefactDefinition); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for artefact
				// End of user code
			}
			
			
			
			
			
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}












	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == EvidenceViewsRepository.ArtefactModel.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactModel.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactModel.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactModel.Properties.artefact) {
			return EvidencePackage.eINSTANCE.getArtefactModel_Artefact();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactModel.Properties.repoUrl) {
			return EvidencePackage.eINSTANCE.getArtefactModel_RepoUrl();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactModel.Properties.repoUser) {
			return EvidencePackage.eINSTANCE.getArtefactModel_RepoUser();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactModel.Properties.repoPassword) {
			return EvidencePackage.eINSTANCE.getArtefactModel_RepoPassword();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath) {
			return EvidencePackage.eINSTANCE.getArtefactModel_RepoLocalPath();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal) {
			return EvidencePackage.eINSTANCE.getArtefactModel_RepoUsesLocal();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		ArtefactModel artefactModel = (ArtefactModel)semanticObject;
		if (EvidenceViewsRepository.ArtefactModel.Properties.id == event.getAffectedEditor()) {
			artefactModel.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (EvidenceViewsRepository.ArtefactModel.Properties.name == event.getAffectedEditor()) {
			artefactModel.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (EvidenceViewsRepository.ArtefactModel.Properties.description == event.getAffectedEditor()) {
			artefactModel.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (EvidenceViewsRepository.ArtefactModel.Properties.artefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, artefactSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				artefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				artefactSettings.move(event.getNewIndex(), (ArtefactDefinition) event.getNewValue());
			}
		}
		if (EvidenceViewsRepository.ArtefactModel.Properties.repoUrl == event.getAffectedEditor()) {
			artefactModel.setRepoUrl((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (EvidenceViewsRepository.ArtefactModel.Properties.repoUser == event.getAffectedEditor()) {
			artefactModel.setRepoUser((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (EvidenceViewsRepository.ArtefactModel.Properties.repoPassword == event.getAffectedEditor()) {
			artefactModel.setRepoPassword((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath == event.getAffectedEditor()) {
			artefactModel.setRepoLocalPath((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal == event.getAffectedEditor()) {
			artefactModel.setRepoUsesLocal((Boolean)event.getNewValue());
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ArtefactModelPropertiesEditionPart basePart = (ArtefactModelPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (EvidencePackage.eINSTANCE.getArtefactModel_Artefact().equals(msg.getFeature()) && isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.artefact))
				basePart.updateArtefact();
			if (EvidencePackage.eINSTANCE.getArtefactModel_RepoUrl().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.repoUrl)) {
				if (msg.getNewValue() != null) {
					basePart.setRepoUrl(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setRepoUrl("");
				}
			}
			if (EvidencePackage.eINSTANCE.getArtefactModel_RepoUser().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.repoUser)) {
				if (msg.getNewValue() != null) {
					basePart.setRepoUser(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setRepoUser("");
				}
			}
			if (EvidencePackage.eINSTANCE.getArtefactModel_RepoPassword().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.repoPassword)) {
				if (msg.getNewValue() != null) {
					basePart.setRepoPassword(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setRepoPassword("");
				}
			}
			if (EvidencePackage.eINSTANCE.getArtefactModel_RepoLocalPath().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath)) {
				if (msg.getNewValue() != null) {
					basePart.setRepoLocalPath(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setRepoLocalPath("");
				}
			}
			if (EvidencePackage.eINSTANCE.getArtefactModel_RepoUsesLocal().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal))
				basePart.setRepoUsesLocal((Boolean)msg.getNewValue());
			
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			EvidencePackage.eINSTANCE.getArtefactModel_Artefact(),
			EvidencePackage.eINSTANCE.getArtefactModel_RepoUrl(),
			EvidencePackage.eINSTANCE.getArtefactModel_RepoUser(),
			EvidencePackage.eINSTANCE.getArtefactModel_RepoPassword(),
			EvidencePackage.eINSTANCE.getArtefactModel_RepoLocalPath(),
			EvidencePackage.eINSTANCE.getArtefactModel_RepoUsesLocal()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (EvidenceViewsRepository.ArtefactModel.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactModel.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactModel.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactModel.Properties.repoUrl == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefactModel_RepoUrl().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefactModel_RepoUrl().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactModel.Properties.repoUser == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefactModel_RepoUser().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefactModel_RepoUser().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactModel.Properties.repoPassword == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefactModel_RepoPassword().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefactModel_RepoPassword().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefactModel_RepoLocalPath().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefactModel_RepoLocalPath().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefactModel_RepoUsesLocal().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefactModel_RepoUsesLocal().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
