/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;

// End of user code

/**
 * 
 * 
 */
public class BaseRoleEquivelanceMapPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseRoleEquivelanceMapPropertiesEditionPart {

	protected ReferencesTable equivalenceMap;
	protected List<ViewerFilter> equivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> equivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected TableViewer roleEquivalenceMap;
	protected List<ViewerFilter> roleEquivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> roleEquivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addRoleEquivalenceMap;
	protected Button removeRoleEquivalenceMap;
	protected Button editRoleEquivalenceMap;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseRoleEquivelanceMapPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseRoleEquivelanceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseRoleEquivelanceMapStep.addStep(BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap);
		
		
		composer = new PartComposer(baseRoleEquivelanceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap) {
					return createEquivalenceMapAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap) {
					return createRoleEquivalenceMapTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseRoleEquivelanceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEquivalenceMapAdvancedTableComposition(Composite parent) {
		this.equivalenceMap = new ReferencesTable(getDescription(BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap, BaselineMessages.BaseRoleEquivelanceMapPropertiesEditionPart_EquivalenceMapLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleEquivelanceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				equivalenceMap.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleEquivelanceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				equivalenceMap.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleEquivelanceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				equivalenceMap.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleEquivelanceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				equivalenceMap.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.equivalenceMapFilters) {
			this.equivalenceMap.addFilter(filter);
		}
		this.equivalenceMap.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap, BaselineViewsRepository.SWT_KIND));
		this.equivalenceMap.createControls(parent);
		this.equivalenceMap.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleEquivelanceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData equivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		equivalenceMapData.horizontalSpan = 3;
		this.equivalenceMap.setLayoutData(equivalenceMapData);
		this.equivalenceMap.setLowerBound(0);
		this.equivalenceMap.setUpperBound(-1);
		equivalenceMap.setID(BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap);
		equivalenceMap.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEquivalenceMapAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRoleEquivalenceMapTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableRoleEquivalenceMap = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableRoleEquivalenceMap.setHeaderVisible(true);
		GridData gdRoleEquivalenceMap = new GridData();
		gdRoleEquivalenceMap.grabExcessHorizontalSpace = true;
		gdRoleEquivalenceMap.horizontalAlignment = GridData.FILL;
		gdRoleEquivalenceMap.grabExcessVerticalSpace = true;
		gdRoleEquivalenceMap.verticalAlignment = GridData.FILL;
		tableRoleEquivalenceMap.setLayoutData(gdRoleEquivalenceMap);
		tableRoleEquivalenceMap.setLinesVisible(true);

		// Start of user code for columns definition for RoleEquivalenceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableRoleEquivalenceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableRoleEquivalenceMap, SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableRoleEquivalenceMap, SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableRoleEquivalenceMap, SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Roles"); //$NON-NLS-1$
		// End IRR
		// End of user code

		roleEquivalenceMap = new TableViewer(tableRoleEquivalenceMap);
		roleEquivalenceMap.setContentProvider(new ArrayContentProvider());
		roleEquivalenceMap.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for RoleEquivalenceMap
			public String getColumnText(Object object, int columnIndex) {
				//Start IRR
				/*
				AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
				if (object instanceof EObject) {
					switch (columnIndex) {
					case 0:
						return labelProvider.getText(object);
					}
				}
				*/
				if (object instanceof EObject) {
					BaseEquivalenceMap baseEquivalenceMap = (BaseEquivalenceMap)object;
					switch (columnIndex) {
					case 0:							
						return baseEquivalenceMap.getId();
					case 1:							
						if (baseEquivalenceMap.getMapGroup() == null)
							return "";
						else
							return baseEquivalenceMap.getMapGroup().getName();
					case 2:							
						if (baseEquivalenceMap.getTarget() == null)
							return "";
						else{
							EList<RefAssurableElement> LstElement = baseEquivalenceMap.getTarget();
							Iterator<RefAssurableElement> iter = LstElement.iterator();
							String sElement = "[";
							while (iter.hasNext()) {
								RefAssurableElement assElement = iter.next();
								if (assElement instanceof  RefRole) {
									RefRole refRole = (RefRole) assElement;
									sElement = sElement + refRole.getName() + ",";
								}
								
							}
							//delete ,
							sElement = sElement.substring(0, sElement.length()-1);
							sElement = sElement + "]";
							return sElement;
						}
							
					}
				}
				//End IRR
				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		roleEquivalenceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (roleEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) roleEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleEquivelanceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						roleEquivalenceMap.refresh();
					}
				}
			}

		});
		GridData roleEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		roleEquivalenceMapData.minimumHeight = 120;
		roleEquivalenceMapData.heightHint = 120;
		roleEquivalenceMap.getTable().setLayoutData(roleEquivalenceMapData);
		for (ViewerFilter filter : this.roleEquivalenceMapFilters) {
			roleEquivalenceMap.addFilter(filter);
		}
		EditingUtils.setID(roleEquivalenceMap.getTable(), BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap);
		EditingUtils.setEEFtype(roleEquivalenceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createRoleEquivalenceMapPanel(tableContainer);
		// Start of user code for createRoleEquivalenceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRoleEquivalenceMapPanel(Composite container) {
		Composite roleEquivalenceMapPanel = new Composite(container, SWT.NONE);
		GridLayout roleEquivalenceMapPanelLayout = new GridLayout();
		roleEquivalenceMapPanelLayout.numColumns = 1;
		roleEquivalenceMapPanel.setLayout(roleEquivalenceMapPanelLayout);
		addRoleEquivalenceMap = new Button(roleEquivalenceMapPanel, SWT.NONE);
		addRoleEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addRoleEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addRoleEquivalenceMap.setLayoutData(addRoleEquivalenceMapData);
		addRoleEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleEquivelanceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				roleEquivalenceMap.refresh();
			}
		});
		EditingUtils.setID(addRoleEquivalenceMap, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap);
		EditingUtils.setEEFtype(addRoleEquivalenceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeRoleEquivalenceMap = new Button(roleEquivalenceMapPanel, SWT.NONE);
		removeRoleEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeRoleEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeRoleEquivalenceMap.setLayoutData(removeRoleEquivalenceMapData);
		removeRoleEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (roleEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) roleEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleEquivelanceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						roleEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeRoleEquivalenceMap, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap);
		EditingUtils.setEEFtype(removeRoleEquivalenceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editRoleEquivalenceMap = new Button(roleEquivalenceMapPanel, SWT.NONE);
		editRoleEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editRoleEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editRoleEquivalenceMap.setLayoutData(editRoleEquivalenceMapData);
		editRoleEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (roleEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) roleEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleEquivelanceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						roleEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editRoleEquivalenceMap, BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap);
		EditingUtils.setEEFtype(editRoleEquivalenceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createRoleEquivalenceMapPanel

		// End of user code
		return roleEquivalenceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart#initEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		equivalenceMap.setContentProvider(contentProvider);
		equivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.equivalenceMap);
		if (eefElementEditorReadOnlyState && equivalenceMap.isEnabled()) {
			equivalenceMap.setEnabled(false);
			equivalenceMap.setToolTipText(BaselineMessages.BaseRoleEquivelanceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !equivalenceMap.isEnabled()) {
			equivalenceMap.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart#updateEquivalenceMap()
	 * 
	 */
	public void updateEquivalenceMap() {
	equivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart#addFilterEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEquivalenceMap(ViewerFilter filter) {
		equivalenceMapFilters.add(filter);
		if (this.equivalenceMap != null) {
			this.equivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart#addBusinessFilterEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEquivalenceMap(ViewerFilter filter) {
		equivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart#isContainedInEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)equivalenceMap.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart#initRoleEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initRoleEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		roleEquivalenceMap.setContentProvider(contentProvider);
		roleEquivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRoleEquivelanceMap.Properties.roleEquivalenceMap);
		if (eefElementEditorReadOnlyState && roleEquivalenceMap.getTable().isEnabled()) {
			roleEquivalenceMap.getTable().setEnabled(false);
			roleEquivalenceMap.getTable().setToolTipText(BaselineMessages.BaseRoleEquivelanceMap_ReadOnly);
			addRoleEquivalenceMap.setEnabled(false);
			addRoleEquivalenceMap.setToolTipText(BaselineMessages.BaseRoleEquivelanceMap_ReadOnly);
			removeRoleEquivalenceMap.setEnabled(false);
			removeRoleEquivalenceMap.setToolTipText(BaselineMessages.BaseRoleEquivelanceMap_ReadOnly);
			editRoleEquivalenceMap.setEnabled(false);
			editRoleEquivalenceMap.setToolTipText(BaselineMessages.BaseRoleEquivelanceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !roleEquivalenceMap.getTable().isEnabled()) {
			roleEquivalenceMap.getTable().setEnabled(true);
			addRoleEquivalenceMap.setEnabled(true);
			removeRoleEquivalenceMap.setEnabled(true);
			editRoleEquivalenceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart#updateRoleEquivalenceMap()
	 * 
	 */
	public void updateRoleEquivalenceMap() {
	roleEquivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart#addFilterRoleEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToRoleEquivalenceMap(ViewerFilter filter) {
		roleEquivalenceMapFilters.add(filter);
		if (this.roleEquivalenceMap != null) {
			this.roleEquivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart#addBusinessFilterRoleEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToRoleEquivalenceMap(ViewerFilter filter) {
		roleEquivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart#isContainedInRoleEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInRoleEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)roleEquivalenceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseRoleEquivelanceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
