/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.mappings.mapping.providers;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.api.providers.IPropertiesEditionPartProvider;

import org.eclipse.opencert.infra.mappings.mapping.parts.MappingViewsRepository;

import org.eclipse.opencert.infra.mappings.mapping.parts.forms.ComplianceMapPropertiesEditionPartForm;
import org.eclipse.opencert.infra.mappings.mapping.parts.forms.EquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.infra.mappings.mapping.parts.forms.MapGroupPropertiesEditionPartForm;
import org.eclipse.opencert.infra.mappings.mapping.parts.forms.MapJustificationPropertiesEditionPartForm;
import org.eclipse.opencert.infra.mappings.mapping.parts.forms.MapModelPropertiesEditionPartForm;
import org.eclipse.opencert.infra.mappings.mapping.parts.forms.MapPropertiesEditionPartForm;

import org.eclipse.opencert.infra.mappings.mapping.parts.impl.ComplianceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.infra.mappings.mapping.parts.impl.EquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.infra.mappings.mapping.parts.impl.MapGroupPropertiesEditionPartImpl;
import org.eclipse.opencert.infra.mappings.mapping.parts.impl.MapJustificationPropertiesEditionPartImpl;
import org.eclipse.opencert.infra.mappings.mapping.parts.impl.MapModelPropertiesEditionPartImpl;
import org.eclipse.opencert.infra.mappings.mapping.parts.impl.MapPropertiesEditionPartImpl;

/**
 * 
 * 
 */
public class MappingPropertiesEditionPartProvider implements IPropertiesEditionPartProvider {

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#provides(java.lang.Object)
	 * 
	 */
	public boolean provides(Object key) {
		return key == MappingViewsRepository.class;
	}

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#getPropertiesEditionPart(java.lang.Object, int, org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(Object key, int kind, IPropertiesEditionComponent component) {
		if (key == MappingViewsRepository.MapModel.class) {
			if (kind == MappingViewsRepository.SWT_KIND)
				return new MapModelPropertiesEditionPartImpl(component);
			if (kind == MappingViewsRepository.FORM_KIND)
				return new MapModelPropertiesEditionPartForm(component);
		}
		if (key == MappingViewsRepository.MapGroup.class) {
			if (kind == MappingViewsRepository.SWT_KIND)
				return new MapGroupPropertiesEditionPartImpl(component);
			if (kind == MappingViewsRepository.FORM_KIND)
				return new MapGroupPropertiesEditionPartForm(component);
		}
		if (key == MappingViewsRepository.Map.class) {
			if (kind == MappingViewsRepository.SWT_KIND)
				return new MapPropertiesEditionPartImpl(component);
			if (kind == MappingViewsRepository.FORM_KIND)
				return new MapPropertiesEditionPartForm(component);
		}
		if (key == MappingViewsRepository.MapJustification.class) {
			if (kind == MappingViewsRepository.SWT_KIND)
				return new MapJustificationPropertiesEditionPartImpl(component);
			if (kind == MappingViewsRepository.FORM_KIND)
				return new MapJustificationPropertiesEditionPartForm(component);
		}
		if (key == MappingViewsRepository.ComplianceMap.class) {
			if (kind == MappingViewsRepository.SWT_KIND)
				return new ComplianceMapPropertiesEditionPartImpl(component);
			if (kind == MappingViewsRepository.FORM_KIND)
				return new ComplianceMapPropertiesEditionPartForm(component);
		}
		if (key == MappingViewsRepository.EquivalenceMap.class) {
			if (kind == MappingViewsRepository.SWT_KIND)
				return new EquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == MappingViewsRepository.FORM_KIND)
				return new EquivalenceMapPropertiesEditionPartForm(component);
		}
		return null;
	}

}
