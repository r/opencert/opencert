/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.eventcreators;

import java.util.Date;

import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetFactory;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.impactanalysis.relations.IArtefactRelationImpact;

public abstract class AbstractChangeDrivenEventsCreator
    implements IChangeDrivenEventsCreator
{
    @Override
    public AssuranceAssetEvent produceEvent(IArtefactRelationImpact artefactRelationImpact, ChangeEffectKind impactedChangeEffectKind)
    {
        AssuranceassetPackage aaPackage = AssuranceassetPackage.eINSTANCE;
        AssuranceassetFactory aaFactory = aaPackage.getAssuranceassetFactory();
        AssuranceAssetEvent assuranceAssetEvent =  aaFactory.createAssuranceAssetEvent();
        
        assuranceAssetEvent.setType(getEventKind());
        
        assuranceAssetEvent.setTime(getAssuranceAssetEventDate());
        
        assuranceAssetEvent.setName(EVENT_CREATED_BY_IMPACT_ANALYSIS);
        
        String userFriendlyImpactingArtefactName = artefactRelationImpact.getUserFriendlyImpactingArtefactName();
        String userFriendlyRelationName = artefactRelationImpact.getUserFriendlyRelationName();
        
        assuranceAssetEvent.setDescription("This event was added on " + new Date() + " as a result of impact propagated from artefact \""
                + userFriendlyImpactingArtefactName + "\" sending event \"" + artefactRelationImpact.getImpactingEventKind() + "\" via relation \""
                + userFriendlyRelationName + "\" with change effect kind: \"" + impactedChangeEffectKind + "\"");
        
        
        return assuranceAssetEvent;
    }
    
    protected abstract Date getAssuranceAssetEventDate();
}
