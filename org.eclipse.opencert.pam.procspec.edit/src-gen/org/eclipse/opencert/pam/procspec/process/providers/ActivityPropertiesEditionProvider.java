/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.providers;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.providers.impl.PropertiesEditingProviderImpl;

import org.eclipse.jface.viewers.IFilter;

import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;

import org.eclipse.opencert.pam.procspec.process.components.ActivityActivityArtefactsPropertiesEditionComponent;
import org.eclipse.opencert.pam.procspec.process.components.ActivityActivityAssuranceAssetPropertiesEditionComponent;
import org.eclipse.opencert.pam.procspec.process.components.ActivityActivityEvaluationPropertiesEditionComponent;
import org.eclipse.opencert.pam.procspec.process.components.ActivityActivityEventsPropertiesEditionComponent;
import org.eclipse.opencert.pam.procspec.process.components.ActivityActivityParticipantsPropertiesEditionComponent;
import org.eclipse.opencert.pam.procspec.process.components.ActivityActivityTechniquesPropertiesEditionComponent;
import org.eclipse.opencert.pam.procspec.process.components.ActivityBasePropertiesEditionComponent;
import org.eclipse.opencert.pam.procspec.process.components.ActivityPropertiesEditionComponent;

/**
 * 
 * 
 */
public class ActivityPropertiesEditionProvider extends PropertiesEditingProviderImpl {

	/**
	 * Constructor without provider for super types.
	 */
	public ActivityPropertiesEditionProvider() {
		super();
	}

	/**
	 * Constructor with providers for super types.
	 * @param superProviders providers to use for super types.
	 */
	public ActivityPropertiesEditionProvider(List<PropertiesEditingProvider> superProviders) {
		super(superProviders);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext) {
		return (editingContext.getEObject() instanceof Activity) 
					&& (ProcessPackage.Literals.ACTIVITY == editingContext.getEObject().eClass());
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext, String part) {
		return (editingContext.getEObject() instanceof Activity) && (ActivityBasePropertiesEditionComponent.BASE_PART.equals(part) || ActivityActivityArtefactsPropertiesEditionComponent.ACTIVITYARTEFACTS_PART.equals(part) || ActivityActivityParticipantsPropertiesEditionComponent.ACTIVITYPARTICIPANTS_PART.equals(part) || ActivityActivityTechniquesPropertiesEditionComponent.ACTIVITYTECHNIQUES_PART.equals(part) || ActivityActivityEvaluationPropertiesEditionComponent.ACTIVITYEVALUATION_PART.equals(part) || ActivityActivityEventsPropertiesEditionComponent.ACTIVITYEVENTS_PART.equals(part) || ActivityActivityAssuranceAssetPropertiesEditionComponent.ACTIVITYASSURANCEASSET_PART.equals(part));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof Activity) && (refinement == ActivityBasePropertiesEditionComponent.class || refinement == ActivityActivityArtefactsPropertiesEditionComponent.class || refinement == ActivityActivityParticipantsPropertiesEditionComponent.class || refinement == ActivityActivityTechniquesPropertiesEditionComponent.class || refinement == ActivityActivityEvaluationPropertiesEditionComponent.class || refinement == ActivityActivityEventsPropertiesEditionComponent.class || refinement == ActivityActivityAssuranceAssetPropertiesEditionComponent.class);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, String part, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof Activity) && ((ActivityBasePropertiesEditionComponent.BASE_PART.equals(part) && refinement == ActivityBasePropertiesEditionComponent.class) || (ActivityActivityArtefactsPropertiesEditionComponent.ACTIVITYARTEFACTS_PART.equals(part) && refinement == ActivityActivityArtefactsPropertiesEditionComponent.class) || (ActivityActivityParticipantsPropertiesEditionComponent.ACTIVITYPARTICIPANTS_PART.equals(part) && refinement == ActivityActivityParticipantsPropertiesEditionComponent.class) || (ActivityActivityTechniquesPropertiesEditionComponent.ACTIVITYTECHNIQUES_PART.equals(part) && refinement == ActivityActivityTechniquesPropertiesEditionComponent.class) || (ActivityActivityEvaluationPropertiesEditionComponent.ACTIVITYEVALUATION_PART.equals(part) && refinement == ActivityActivityEvaluationPropertiesEditionComponent.class) || (ActivityActivityEventsPropertiesEditionComponent.ACTIVITYEVENTS_PART.equals(part) && refinement == ActivityActivityEventsPropertiesEditionComponent.class) || (ActivityActivityAssuranceAssetPropertiesEditionComponent.ACTIVITYASSURANCEASSET_PART.equals(part) && refinement == ActivityActivityAssuranceAssetPropertiesEditionComponent.class));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode) {
		if (editingContext.getEObject() instanceof Activity) {
			return new ActivityPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part) {
		if (editingContext.getEObject() instanceof Activity) {
			if (ActivityBasePropertiesEditionComponent.BASE_PART.equals(part))
				return new ActivityBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityArtefactsPropertiesEditionComponent.ACTIVITYARTEFACTS_PART.equals(part))
				return new ActivityActivityArtefactsPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityParticipantsPropertiesEditionComponent.ACTIVITYPARTICIPANTS_PART.equals(part))
				return new ActivityActivityParticipantsPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityTechniquesPropertiesEditionComponent.ACTIVITYTECHNIQUES_PART.equals(part))
				return new ActivityActivityTechniquesPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityEvaluationPropertiesEditionComponent.ACTIVITYEVALUATION_PART.equals(part))
				return new ActivityActivityEvaluationPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityEventsPropertiesEditionComponent.ACTIVITYEVENTS_PART.equals(part))
				return new ActivityActivityEventsPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityAssuranceAssetPropertiesEditionComponent.ACTIVITYASSURANCEASSET_PART.equals(part))
				return new ActivityActivityAssuranceAssetPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String, java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part, java.lang.Class refinement) {
		if (editingContext.getEObject() instanceof Activity) {
			if (ActivityBasePropertiesEditionComponent.BASE_PART.equals(part)
				&& refinement == ActivityBasePropertiesEditionComponent.class)
				return new ActivityBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityArtefactsPropertiesEditionComponent.ACTIVITYARTEFACTS_PART.equals(part)
				&& refinement == ActivityActivityArtefactsPropertiesEditionComponent.class)
				return new ActivityActivityArtefactsPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityParticipantsPropertiesEditionComponent.ACTIVITYPARTICIPANTS_PART.equals(part)
				&& refinement == ActivityActivityParticipantsPropertiesEditionComponent.class)
				return new ActivityActivityParticipantsPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityTechniquesPropertiesEditionComponent.ACTIVITYTECHNIQUES_PART.equals(part)
				&& refinement == ActivityActivityTechniquesPropertiesEditionComponent.class)
				return new ActivityActivityTechniquesPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityEvaluationPropertiesEditionComponent.ACTIVITYEVALUATION_PART.equals(part)
				&& refinement == ActivityActivityEvaluationPropertiesEditionComponent.class)
				return new ActivityActivityEvaluationPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityEventsPropertiesEditionComponent.ACTIVITYEVENTS_PART.equals(part)
				&& refinement == ActivityActivityEventsPropertiesEditionComponent.class)
				return new ActivityActivityEventsPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ActivityActivityAssuranceAssetPropertiesEditionComponent.ACTIVITYASSURANCEASSET_PART.equals(part)
				&& refinement == ActivityActivityAssuranceAssetPropertiesEditionComponent.class)
				return new ActivityActivityAssuranceAssetPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part, refinement);
	}

	/**
	 * Provides the filter used by the plugin.xml to assign part forms.
	 */
	public static class EditionFilter implements IFilter {
		
		/**
		 * {@inheritDoc}
		 * 
		 * @see org.eclipse.jface.viewers.IFilter#select(java.lang.Object)
		 */
		public boolean select(Object toTest) {
			EObject eObj = EEFUtils.resolveSemanticObject(toTest);
			return eObj != null && ProcessPackage.Literals.ACTIVITY == eObj.eClass();
		}
		
	}

}
