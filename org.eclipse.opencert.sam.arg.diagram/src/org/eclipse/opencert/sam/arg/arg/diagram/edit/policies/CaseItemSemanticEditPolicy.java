/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AgreementCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.ArgumentElementCitationCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.ArgumentReasoningCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.ArgumentationCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssumptionCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AwayClaimCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AwayContextCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AwaySolutionCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.ChoiceCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.ClaimCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.ContextCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.InformationElementCitationCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.JustificationCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.SolutionCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;

/**
 * @generated
 */
public class CaseItemSemanticEditPolicy extends ArgBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public CaseItemSemanticEditPolicy() {
		super(ArgElementTypes.Case_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (ArgElementTypes.Claim_2001 == req.getElementType()) {
			return getGEFWrapper(new ClaimCreateCommand(req));
		}	
		if (ArgElementTypes.Agreement_2002 == req.getElementType()) {
			return getGEFWrapper(new AgreementCreateCommand(req));
		}
		if (ArgElementTypes.ArgumentReasoning_2003 == req.getElementType()) {
			return getGEFWrapper(new ArgumentReasoningCreateCommand(req));
		}
		if (ArgElementTypes.Argumentation_2004 == req.getElementType()) {
			return getGEFWrapper(new ArgumentationCreateCommand(req));
		}
		if (ArgElementTypes.InformationElementCitation_2005 == req
				.getElementType()) {
			return getGEFWrapper(new InformationElementCitationCreateCommand(
					req));
		}
		if (ArgElementTypes.ArgumentElementCitation_2006 == req
				.getElementType()) {
			return getGEFWrapper(new ArgumentElementCitationCreateCommand(req));
		}
		if (ArgElementTypes.Choice_2007 == req.getElementType()) {
			return getGEFWrapper(new ChoiceCreateCommand(req));
		}
		/**
		 * ARL changes
		 */
		if (ArgElementTypes.Assumption_2000 == req.getElementType()) {
			return getGEFWrapper(new AssumptionCreateCommand(req));
		}	
		if (ArgElementTypes.Justification_3002 == req.getElementType()) {
			return getGEFWrapper(new JustificationCreateCommand(req));
		}
		if (ArgElementTypes.Context_3003 == req.getElementType()) {
			return getGEFWrapper(new ContextCreateCommand(req));
		}
		if (ArgElementTypes.Solution_3004 == req.getElementType()) {
			return getGEFWrapper(new SolutionCreateCommand(req));
		}
		if (ArgElementTypes.AwayClaim_5001 == req.getElementType()) {
			return getGEFWrapper(new AwayClaimCreateCommand(req));
		}
		if (ArgElementTypes.AwayContext_5002 == req.getElementType()) {
			return getGEFWrapper(new AwayContextCreateCommand(req));
		}
		if (ArgElementTypes.AwaySolution_5003 == req.getElementType()) {
			return getGEFWrapper(new AwaySolutionCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
