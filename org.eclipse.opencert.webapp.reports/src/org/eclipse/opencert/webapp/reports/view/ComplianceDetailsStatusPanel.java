/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.util.Collection;
import java.util.List;

import org.eclipse.opencert.webapp.reports.containers.ArtefactFileDetails;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.containers.BaseAssetComplianceStatus;
import org.eclipse.opencert.webapp.reports.containers.BaselineElementDetails;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetails;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetailsStatusItem;
import org.eclipse.opencert.webapp.reports.containers.visitors.ComplianceDetailsTraverser;
import org.eclipse.opencert.webapp.reports.listeners.ComplianceDetailsStatusListener;
import org.eclipse.opencert.webapp.reports.listeners.ComplianceStatusListener;
import org.eclipse.opencert.webapp.reports.listeners.EvidenceFileUploaderListener;
import org.eclipse.opencert.webapp.reports.listeners.ProjectBaselineComplianceTableListener;
import org.eclipse.opencert.webapp.reports.listeners.UnassignOrAssignArtefactListener;
import org.eclipse.opencert.webapp.reports.listeners.util.ClickType;
import org.eclipse.opencert.webapp.reports.listeners.util.SelectionEvent;
import org.eclipse.opencert.webapp.reports.manager.BaseActivityManager;
import org.eclipse.opencert.webapp.reports.manager.BaseArtefactManager;
import org.eclipse.opencert.webapp.reports.manager.BaseRequirementManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceDetailsManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceDetailsStatusVaadinVisitor;
import org.eclipse.opencert.webapp.reports.util.BaselineElementType;
import org.eclipse.opencert.webapp.reports.util.ComplianceStatusCalculator;
import org.eclipse.opencert.webapp.reports.util.ComplianceStatusType;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.util.StringUtil;
import org.eclipse.opencert.webapp.reports.view.common.AbstractComplianceDetailsPanel;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;

@Theme("opencerttheme")
public class ComplianceDetailsStatusPanel extends AbstractComplianceDetailsPanel implements ProjectBaselineComplianceTableListener, EvidenceFileUploaderListener,
ComplianceStatusListener {

	private static final long serialVersionUID = 7218252436903758257L;
	
	private static final String NAME = "name";
	private static final String TYPE = "type";
	private static final String UNASSIGN = "unassign";

	private static final String COMPLINACE_BUTTON_STYLE = "complianceSettings";
	private static final String COMPLINACE_LABEL_STYLE = "compliance";
	
	private static final String EXPAND_JUSTIFICATION = "Expand to Justification";
	private static final String EXPAND_ASSET = "Expand to Asset";
	private static final String EXPAND_RESOURCE = "Expand to Resource";
	
	private VerticalLayout titleBar;
	private VerticalLayout selectedBaseAssetInfo;
	private TreeTable complianceDetailsTree;
	private VerticalLayout treeLayout;
	private Label selectedBaselineLabel;
	private Label selectedBaselineComplianceStatusLabel;
	private Button expandToResourceLink;
	private Label noSelectedBaseAssetInfo;
	
	private DragAndDropComplianceEvidencePanel dragAndDropComplianceEvidencePanel;
	private ComplianceDetailsManager _complianceDetailsManager = null;
	
	private ComplianceDetailsStatusListener complianceDetailsStatusListener;
	private UnassignOrAssignArtefactListener unassignOrAssignArtefactListener;
	
	private BaselineElementDetails selectedBaselineElementDetails;
    
	private AbstractComplianceReport complianceEstimationReport;
	
	public ComplianceDetailsStatusPanel(AbstractComplianceReport complianceEstimationReport) 
	{
	    this.complianceEstimationReport = complianceEstimationReport;
		init();
	}
	
	public void init() 
	{
		VerticalLayout mainPanel = new VerticalLayout();
		mainPanel.setWidth("100%");
		mainPanel.setSizeFull();
		
		//Compliance settings:		
		titleBar = createTitleBar(null);
		mainPanel.addComponent(titleBar);
		
		createNoSelectedBaseAssetInfo();
		mainPanel.addComponent(noSelectedBaseAssetInfo);
		
		//Compliance main info:
		selectedBaseAssetInfo = createSelectedBaseAssetInfo();
		mainPanel.addComponent(selectedBaseAssetInfo);
		//mainComplianceLayout.setVisible(false);
		
        dragAndDropComplianceEvidencePanel = new DragAndDropComplianceEvidencePanel(this, complianceEstimationReport.getTitle());
        dragAndDropComplianceEvidencePanel.addUnassignOrAssignArtefactListener(complianceEstimationReport);
        mainPanel.addComponent(dragAndDropComplianceEvidencePanel);

		//Compliance data:
		treeLayout = new VerticalLayout();
		treeLayout.setSizeFull();
		
		HorizontalLayout detailsHeader = new HorizontalLayout();
		Label detailsLabel = new Label("Details:");
		detailsLabel.setStyleName("details");
		detailsHeader.addComponent(detailsLabel);
		addExpantionLinks(detailsHeader);
		treeLayout.addComponent(detailsHeader);
		
		
		
		treeLayout.setMargin(true);
		
		complianceDetailsTree = new TreeTable(null, createEmptyTableDataContainer());
		complianceDetailsTree.setStyleName("complianceDetailsTable");
		complianceDetailsTree.setWidth("100%");
		complianceDetailsTree.setHeight("100%");
		complianceDetailsTree.setPageLength(0);
		complianceDetailsTree.setColumnHeaderMode(TreeTable.ColumnHeaderMode.HIDDEN);
		complianceDetailsTree.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		complianceDetailsTree.setItemCaptionPropertyId(NAME);
		complianceDetailsTree.setSelectable(true);
		complianceDetailsTree.setBuffered(false);
		complianceDetailsTree.setImmediate(true);
        
		treeLayout.addComponent(complianceDetailsTree);
		
		complianceDetailsTree.addValueChangeListener(new ValueChangeListener() 
        {
			private static final long serialVersionUID = -5504391442497895229L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				Object complianceDetailsValue = complianceDetailsTree.getValue();
				if (complianceDetailsValue != null) {
					ComplianceDetailsStatusItem complianceDetailsStatusItem = (ComplianceDetailsStatusItem)complianceDetailsValue;
					long complianceDetailsStatusId = complianceDetailsStatusItem.getItemId();
					ComplianceDetails complianceDetails = complianceDetailsStatusItem.getComplianceDetails();
					
					if (_complianceDetailsManager == null) {
					    OpencertLogger.warn("Illegal state exception! Null _complianceDetailsManager!");
						return;
					}
					complianceDetailsStatusListener.baselineElementSelected(complianceDetailsStatusId, 
							_complianceDetailsManager.findComplianceElementTypeById(complianceDetailsStatusId), complianceDetails);
				}
			}
        });
		
		mainPanel.addComponent(treeLayout);
		
		//when no project in combo:
		dragAndDropComplianceEvidencePanel.setVisible(false);
		noSelectedBaseAssetInfo.setVisible(false);
		selectedBaseAssetInfo.setVisible(false);
		treeLayout.setVisible(false);
		//
		
		//add mainPanel component
		setCompositionRoot(mainPanel);
	}
	
	private void addExpantionLinks(HorizontalLayout detailsHeader)
    {
        Button expandToJustificationLink = new Button("[Expand to Justification]");
        expandToJustificationLink.setStyleName(COMPLINACE_BUTTON_STYLE);
        Button expandToAssetLink = new Button("[Expand to Asset]");
        expandToAssetLink.setStyleName(COMPLINACE_BUTTON_STYLE);

        expandToResourceLink = new Button("[Expand to Resource]");
        expandToResourceLink.setStyleName(COMPLINACE_BUTTON_STYLE);
        
        detailsHeader.addComponent(expandToJustificationLink);
        detailsHeader.addComponent(expandToAssetLink);
        detailsHeader.addComponent(expandToResourceLink);
        
        expandToJustificationLink.addClickListener((e) -> collapseAllItems(true, EXPAND_JUSTIFICATION));
        
        expandToAssetLink.addClickListener((e) ->
        {
            collapseAllItems(true, EXPAND_JUSTIFICATION);
            collapseAllItems(false, EXPAND_ASSET);
        });
        
        expandToResourceLink.addClickListener((e) -> collapseAllItems(false, EXPAND_RESOURCE));
    }

    public DragAndDropComplianceEvidencePanel getDragAndDropComplianceEvidencePanel()
    {
        return dragAndDropComplianceEvidencePanel;
    }
	
    @Override
    public void baselineElementClicked(SelectionEvent sEvent)
    {
       if (sEvent.getClickType().equals(ClickType.SELECTED)) {
           selectedBaselineElementDetails = new BaselineElementDetails(sEvent.getCdoId(), sEvent.getBaselineElementName(), sEvent.getBaselineElementType(), sEvent.getBaseAssetComplianceStatus());
           // important! Re-instantiate ComplianceDetailsManager when displaying details of new base asset
           _complianceDetailsManager = new ComplianceDetailsManager();
           
           handleDradAndDropEvidencePanelOnBaseAssetSelected(sEvent.getBaselineElementType(), sEvent.getBaselineElementName(), sEvent.getBaseAssetComplianceStatus());
           
           noSelectedBaseAssetInfo.setVisible(false);
           selectedBaseAssetInfo.setVisible(true);
           ComplianceStatusCalculator complianceStatusCalculator = new ComplianceStatusCalculator();
           if (BaselineElementType.BASE_ARTEFACT.equals(sEvent.getBaselineElementType())) {
               BaseArtefactManager baseArtefactManager = (BaseArtefactManager)SpringContextHelper.getBeanFromWebappContext(BaseArtefactManager.SPRING_NAME);
               BaseArtefact baseArtefact = baseArtefactManager.getBaseArtefact(sEvent.getCdoId());
                   
               if (baseArtefact != null) {                
                   createMainComplianceInfoData(baseArtefact.getName(), complianceStatusCalculator.calculateComplianceStatusForBaseElement(baseArtefact));
                   List<ComplianceDetails> complianceDetailsData = _complianceDetailsManager.readComplianceDetailsData(baseArtefact, sEvent.getCdoId(), sEvent.getBaselineElementName(), true);
                   createComplianceDetailsTree(complianceDetailsData);
               }
           }
               
           if (BaselineElementType.BASE_ACTIVITY.equals(sEvent.getBaselineElementType())) {
               BaseActivityManager baseActivityManager = (BaseActivityManager)SpringContextHelper.getBeanFromWebappContext(BaseActivityManager.SPRING_NAME);
               BaseActivity baseActivity = baseActivityManager.getBaseActivity(sEvent.getCdoId());
                 
               if (baseActivity != null) {
                   createMainComplianceInfoData(baseActivity.getName(),  complianceStatusCalculator.calculateComplianceStatusForBaseElement(baseActivity));
                   List<ComplianceDetails> complianceDetailsData = _complianceDetailsManager.readComplianceDetailsData(baseActivity, sEvent.getCdoId(), sEvent.getBaselineElementName(), true);
                   createComplianceDetailsTree(complianceDetailsData);
               }
           }
           
           if (BaselineElementType.BASE_REQUIREMENT.equals(sEvent.getBaselineElementType())) {
        	   BaseRequirementManager baseRequirementManager = (BaseRequirementManager)SpringContextHelper.getBeanFromWebappContext(BaseRequirementManager.SPRING_NAME);
        	   BaseRequirement baseRequirement = baseRequirementManager.getBaseRequirement(sEvent.getCdoId());
        	   
        	   if (baseRequirement != null) {
        		   createMainComplianceInfoData(baseRequirement.getName(),  complianceStatusCalculator.calculateComplianceStatusForBaseElement(baseRequirement));
        		   List<ComplianceDetails> complianceDetailsData = _complianceDetailsManager.readComplianceDetailsData(baseRequirement, sEvent.getCdoId(), sEvent.getBaselineElementName(), true);
                   createComplianceDetailsTree(complianceDetailsData);
        	   }
           }
           
           if (BaselineElementType.BASE_ACTIVITY.equals(sEvent.getBaselineElementType())) {
               expandToResourceLink.setVisible(false);
           } else {
               expandToResourceLink.setVisible(true);
           }           
           
           if (complianceDetailsTree.getItemIds() != null && complianceDetailsTree.getItemIds().size() > 0) {
               treeLayout.setVisible(true);
               complianceDetailsTree.setValue(complianceDetailsTree.getItemIds().iterator().next());    //clicks on the first element in details tree
           } else {
               treeLayout.setVisible(false);
               complianceDetailsStatusListener.baselineElementUnselected();
           }
       } else {
           dragAndDropComplianceEvidencePanel.getEvidenceFileUploader().getArtefactFileDetails().setBaseElementName("");
           dragAndDropComplianceEvidencePanel.setVisible(false);      
           
           if (sEvent.getNumberOfBaseAssets() != null && sEvent.getNumberOfBaseAssets() > 0) { 
        	   noSelectedBaseAssetInfo.setVisible(true);
           } else {
        	   noSelectedBaseAssetInfo.setVisible(false);
           }
           selectedBaseAssetInfo.setVisible(false);
           treeLayout.setVisible(false);
           complianceDetailsStatusListener.baselineElementUnselected();
       }
    }
	
	private void handleDradAndDropEvidencePanelOnBaseAssetSelected(String baselineElementType,
			String baselineElementName,
			BaseAssetComplianceStatus baseAssetComplianceStatus) 
	{
        if (baselineElementType.equals(BaselineElementType.BASE_ARTEFACT) || 
        		baselineElementType.equals(BaselineElementType.BASE_REQUIREMENT)) 
        {
        	final ArtefactFileDetails artefactFileDetails = dragAndDropComplianceEvidencePanel.getEvidenceFileUploader().getArtefactFileDetails();
        	artefactFileDetails.setBaseElementName(baselineElementName);
        	artefactFileDetails.setBaseAssetComplianceStatus(baseAssetComplianceStatus);
            
            dragAndDropComplianceEvidencePanel.setInfoMessage(artefactFileDetails.getBaseElementName()); 
                                    
            dragAndDropComplianceEvidencePanel.setVisible(true);
        } else {
        	dragAndDropComplianceEvidencePanel.setVisible(false);
        }

	}
	
	private void createNoSelectedBaseAssetInfo() {
		noSelectedBaseAssetInfo = new Label("Please select specific <b>Base Asset Name</b> in <b>Project Baseline "
				+ "Compliance</b> panel to see <b>Base Asset Compliance Details</b>.", ContentMode.HTML);
		noSelectedBaseAssetInfo.setStyleName("baseAssetHelp");
	}
	
	private void createMainComplianceInfoData(String assetName, ComplianceStatusType complianceStatusType) 
	{
	    assetName = StringUtil.ensureNotNull(assetName);
		selectedBaselineLabel.setValue("<i>Selected Baseline Element Name: </i><b>" + assetName + "</b>");
		
		if (complianceStatusType.getValue().equals(ComplianceStatusType.OK.getValue())) {
		    selectedBaselineComplianceStatusLabel.setValue("<i>Compliance Status: </i><b>The evidence presented below is fully compliant with the selected baseline asset.</b>");
		} else if (complianceStatusType.getValue().equals(ComplianceStatusType.NO.getValue())){
		    selectedBaselineComplianceStatusLabel.setValue("<i>Compliance Status: </i><b>The selected requirement contains evidence which is not compliant with it</b>");
		} else if (complianceStatusType.getValue().equals(ComplianceStatusType.PARTLY.getValue())){
            selectedBaselineComplianceStatusLabel.setValue("<i>Compliance Status: </i><b>The evidence presented below is partially compliant with the selected baseline asset.</b>");
        } else if (complianceStatusType.getValue().equals(ComplianceStatusType.EMPTY.getValue())){
            selectedBaselineComplianceStatusLabel.setValue("<i>Compliance Status: </i><b>The selected requirement has no evidence provided</b>");
        }     	
	}
	
	private VerticalLayout createTitleBar(String baselineElementType) {
		VerticalLayout compliaceLayout = new VerticalLayout();
		GridLayout complianceSettingsLayout = new GridLayout(4, 1);
		complianceSettingsLayout.setWidth("660px");
		
		VerticalLayout complianceDetailsLayout = new VerticalLayout();
		complianceDetailsLayout.setWidth("300px");
		complianceDetailsLayout.setHeight("16px");
		Label complianceLabel = new Label("Base Asset Compliance Details");
		complianceLabel.addStyleName("complianceDetailsTitle");
		complianceDetailsLayout.addComponent(complianceLabel);
		
		
		
		complianceSettingsLayout.addComponent(complianceDetailsLayout);
		complianceSettingsLayout.setComponentAlignment(complianceDetailsLayout, Alignment.TOP_LEFT);
		
		compliaceLayout.addComponent(complianceSettingsLayout);
		compliaceLayout.setWidth("100%");
		return compliaceLayout;
	}
	
	private VerticalLayout createSelectedBaseAssetInfo() {
		VerticalLayout mainComplianceInfoLayout = new VerticalLayout();
		mainComplianceInfoLayout.setWidth("100px");

		selectedBaselineLabel = new Label("", ContentMode.HTML);
		selectedBaselineLabel.setStyleName(COMPLINACE_LABEL_STYLE);
		
		selectedBaselineComplianceStatusLabel = new Label("", ContentMode.HTML);
		selectedBaselineComplianceStatusLabel.setStyleName(COMPLINACE_LABEL_STYLE);
		
		mainComplianceInfoLayout.addComponent(selectedBaselineLabel);
		mainComplianceInfoLayout.addComponent(selectedBaselineComplianceStatusLabel);
		
		mainComplianceInfoLayout.setStyleName("complianceDetailsLayout");
		
		return mainComplianceInfoLayout;
	}
	
	private void createComplianceDetailsTree(List<ComplianceDetails> complianceDetailsAll) 
	{
		HierarchicalContainer complianceDetailsContainer = createEmptyTableDataContainer();
		
		final ComplianceDetailsStatusVaadinVisitor vaadinVisitor = new ComplianceDetailsStatusVaadinVisitor(selectedBaselineElementDetails,
				complianceDetailsContainer, new String[] { NAME, TYPE, UNASSIGN });
		vaadinVisitor.setComplianceEstimationReport(complianceEstimationReport);
		vaadinVisitor.setEvidenceFileUploaderListener(this);
		vaadinVisitor.setComplianceStatusListener(this);
		vaadinVisitor.setUnassignOrAssignArtefactListener(unassignOrAssignArtefactListener);

		ComplianceDetailsTraverser<ComplianceDetailsStatusItem> cdTraverser = new ComplianceDetailsTraverser<ComplianceDetailsStatusItem>(vaadinVisitor);
		cdTraverser.traverseComplianceDetails(complianceDetailsAll);
		
		complianceDetailsContainer = vaadinVisitor.getResult();
		
		complianceDetailsTree.setContainerDataSource(complianceDetailsContainer);
//		complianceDetailsTree.setCaption(COMPLIANCE_DETAILS); /*fixed IE bug: changed caption with label */
		complianceDetailsTree.setImmediate(true);
		
		complianceDetailsTree.setColumnExpandRatio(NAME, 70);
		complianceDetailsTree.setColumnExpandRatio(TYPE, 14);
		complianceDetailsTree.setColumnExpandRatio(UNASSIGN, 16);
		
		setChildrenAllowed(complianceDetailsContainer);

		collapseAllItems(false, "");
	}
	
	protected HierarchicalContainer createEmptyTableDataContainer() 
    {     
        HierarchicalContainer hc = new HierarchicalContainer()
        {
			private static final long serialVersionUID = -4854665965909691819L;

			@Override
            public Collection<?> getSortableContainerPropertyIds()
            {
                return getContainerPropertyIds();
            }
        };
        
        addContainerProperties(hc);
        
        return hc;
    }
	
	protected void addContainerProperties(HierarchicalContainer hc) {
		hc.addContainerProperty(NAME, HorizontalLayout.class, "");
        hc.addContainerProperty(TYPE, Label.class, "");
        hc.addContainerProperty(UNASSIGN, Button.class, "");
	}
    
	private void collapseItem(Object itemId, boolean collapsed, String expandType) {
		collapseItem(itemId, collapsed);
		Collection<?> childs = complianceDetailsTree.getChildren(itemId);
		if (childs != null && !childs.isEmpty()) {
			for (Object item : childs) {
				if (EXPAND_ASSET.equals(expandType)) {
					collapseItem(item, true, expandType);
				} else {
					collapseItem(item, collapsed, expandType);
				}
			}
		}
	}
	
	private void collapseItem(Object itemId, boolean collapsed) {
		complianceDetailsTree.setCollapsed(itemId, collapsed);
	}
	
	private void collapseAllItems(boolean collapsed, String expandType) {
		for (Object itemId : complianceDetailsTree.getItemIds()) {
			collapseItem(itemId, collapsed, expandType);
		}
	}
	
	public void setComplianceDetailsStatusListener(ComplianceDetailsStatusListener complianceDetailsStatusListener) {
		this.complianceDetailsStatusListener = complianceDetailsStatusListener;
	}

	public void setUnassignOrAssignArtefactListener(UnassignOrAssignArtefactListener unassignOrAssignArtefactListener) {
		this.unassignOrAssignArtefactListener = unassignOrAssignArtefactListener;
	}

    @Override
    public void artefactFileUploadingSuccesfully() //invoking after file upload, processing baseline element is necessary to refresh content of table with artefacts
    {           
        SelectionEvent sEvent = new SelectionEvent(ClickType.SELECTED, selectedBaselineElementDetails.getCdoId(),
                selectedBaselineElementDetails.getBaselineElementType(), selectedBaselineElementDetails.getBaselineElementName(),
                selectedBaselineElementDetails.getBaseAssetComplianceStatus());
        baselineElementClicked(sEvent);
    }

	public void setNewProject(AssuranceProjectWrapper newProject) 
	{
		final ArtefactFileDetails artefactFileDetails = dragAndDropComplianceEvidencePanel.getEvidenceFileUploader().getArtefactFileDetails();
		artefactFileDetails.setAssuranceProject(newProject.getAssuranceProject());
        artefactFileDetails.setProjectName(newProject.getName());		
	}

	@Override
	public void selectComplianceDetailsStatusItem(ComplianceDetailsStatusItem item) {
		complianceDetailsTree.setValue(item);
	}

	@Override
	public void updateBaselineDetailsStatus() {
		unassignOrAssignArtefactListener.artefactAssignedOrUnassigned(selectedBaselineElementDetails.getCdoId());
	}
}
