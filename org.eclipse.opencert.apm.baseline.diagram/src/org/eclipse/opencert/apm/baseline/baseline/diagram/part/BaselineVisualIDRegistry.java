/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivity2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityBaseActivitySubActivityCompartment2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityBaseActivitySubActivityCompartmentEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityName2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityNameEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityPrecedingActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityProducedArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRequiredArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactNameEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseFrameworkEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleLabelEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleNameEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.WrappingLabel2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.WrappingLabel3EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.WrappingLabelEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class BaselineVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "org.eclipse.opencert.apm.baseline.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (BaseFrameworkEditPart.MODEL_ID.equals(view.getType())) {
				return BaseFrameworkEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				BaselineDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (BaselinePackage.eINSTANCE.getBaseFramework().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((BaseFramework) domainElement)) {
			return BaseFrameworkEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
				.getModelID(containerView);
		if (!BaseFrameworkEditPart.MODEL_ID.equals(containerModelID)
				&& !"baseline".equals(containerModelID)) { //$NON-NLS-1$
			return -1;
		}
		int containerVisualID;
		if (BaseFrameworkEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = BaseFrameworkEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case BaseFrameworkEditPart.VISUAL_ID:
			if (BaselinePackage.eINSTANCE.getBaseActivity().isSuperTypeOf(
					domainElement.eClass())) {
				return BaseActivityEditPart.VISUAL_ID;
			}
			if (BaselinePackage.eINSTANCE.getBaseArtefact().isSuperTypeOf(
					domainElement.eClass())) {
				return BaseArtefactEditPart.VISUAL_ID;
			}
			if (BaselinePackage.eINSTANCE.getBaseRole().isSuperTypeOf(
					domainElement.eClass())) {
				return BaseRoleEditPart.VISUAL_ID;
			}
			break;
		case BaseActivityBaseActivitySubActivityCompartmentEditPart.VISUAL_ID:
			if (BaselinePackage.eINSTANCE.getBaseActivity().isSuperTypeOf(
					domainElement.eClass())) {
				return BaseActivity2EditPart.VISUAL_ID;
			}
			break;
		case BaseActivityBaseActivitySubActivityCompartment2EditPart.VISUAL_ID:
			if (BaselinePackage.eINSTANCE.getBaseActivity().isSuperTypeOf(
					domainElement.eClass())) {
				return BaseActivity2EditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
				.getModelID(containerView);
		if (!BaseFrameworkEditPart.MODEL_ID.equals(containerModelID)
				&& !"baseline".equals(containerModelID)) { //$NON-NLS-1$
			return false;
		}
		int containerVisualID;
		if (BaseFrameworkEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = BaseFrameworkEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case BaseFrameworkEditPart.VISUAL_ID:
			if (BaseActivityEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (BaseArtefactEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (BaseRoleEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case BaseActivityEditPart.VISUAL_ID:
			if (BaseActivityNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (BaseActivityBaseActivitySubActivityCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case BaseArtefactEditPart.VISUAL_ID:
			if (BaseArtefactNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case BaseRoleEditPart.VISUAL_ID:
			if (BaseRoleNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case BaseActivity2EditPart.VISUAL_ID:
			if (BaseActivityName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (BaseActivityBaseActivitySubActivityCompartment2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case BaseActivityBaseActivitySubActivityCompartmentEditPart.VISUAL_ID:
			if (BaseActivity2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case BaseActivityBaseActivitySubActivityCompartment2EditPart.VISUAL_ID:
			if (BaseActivity2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case BaseActivityRequiredArtefactEditPart.VISUAL_ID:
			if (WrappingLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case BaseActivityProducedArtefactEditPart.VISUAL_ID:
			if (WrappingLabel2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case BaseActivityPrecedingActivityEditPart.VISUAL_ID:
			if (WrappingLabel3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case BaseActivityRoleEditPart.VISUAL_ID:
			if (BaseRoleLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(BaseFramework element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		switch (visualID) {
		case BaseActivityBaseActivitySubActivityCompartmentEditPart.VISUAL_ID:
		case BaseActivityBaseActivitySubActivityCompartment2EditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case BaseFrameworkEditPart.VISUAL_ID:
			return false;
		case BaseArtefactEditPart.VISUAL_ID:
		case BaseRoleEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
					.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
					.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
					.getNodeVisualID(containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
					.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
