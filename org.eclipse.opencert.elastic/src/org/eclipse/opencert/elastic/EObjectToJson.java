/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;
import org.eclipse.emf.ecore.resource.Resource;

import com.google.gson.JsonObject;

/**
 * Helper to "serialize" an EMF object to a JSON object using reflection.
 * 
 * @author mauersberger
 */
public class EObjectToJson {

	/**
	 * Global singleton, no need to create an own instance.
	 */
	public final static EObjectToJson INSTANCE = new EObjectToJson();

	/**
	 * Converts the given {@link EObject} to a {@link JsonObject} using reflection.
	 *
	 * @param object
	 * @return the {@link JsonObject}
	 */
	@SuppressWarnings("nls")
	public JsonObject convert(EObject object) {
		JsonObject jsonObject = new JsonObject();
		String label = null;

		EClass type = object.eClass();
		if (type != null) {

			String nameAttribute = "name";

			// TODO Walk over the meta class and index all native attributes
			EList<EAttribute> attributes = type.getEAllAttributes();
			for (EAttribute attr : attributes) {
				Object value = object.eGet(attr);
				if (value != null) {
					jsonObject.addProperty(attr.getName(), value.toString());

					// is this the "name" attribute?
					if ((label == null) && (nameAttribute.contentEquals(attr.getName()))) {
						label = value.toString();
					}
				}
			}

			// well, if we don't have a label, lets use the type
			if (label == null) {
				// we keep it simple, just prefix with "An" or "A"
				String name = type.getName();
				if ("AEIOUaeiou".indexOf(name.charAt(0)) != -1) {
					label = "An " + name;
				} else {
					label = "A " + name;
				}
			}

			// put a label to each document
			if (!jsonObject.has("label")) {
				jsonObject.addProperty("label", label);
			}
		}

		// add the URI all the time
		Resource resource = object.eResource();
		if (resource != null) {
			String fragment = resource.getURIFragment(object);
			if (object instanceof EAnnotation) {
				// TODO We have problems with annotations
				fragment = "/#";
			}
			else if (object instanceof EStringToStringMapEntryImpl) {
				// TODO We have problems with these maps
				fragment = "/#";
			}
			
			URI uri = resource.getURI().appendFragment(fragment);
			String uriString = uri.toString();
			jsonObject.addProperty("uri", uriString);

			// at least some very very simply categorization of the origin
			if (uri.isPlatform()) {
				jsonObject.addProperty("uriKind", "Platform");
			} else if (uri.isFile()) {
				jsonObject.addProperty("uriKind", "File");
			} else if ("cdo".equalsIgnoreCase(uri.scheme())) {
				jsonObject.addProperty("uriKind", "CDO");
			} else {
				jsonObject.addProperty("uriKind", "Unknown");
			}
		}

		return jsonObject;
	}
}
