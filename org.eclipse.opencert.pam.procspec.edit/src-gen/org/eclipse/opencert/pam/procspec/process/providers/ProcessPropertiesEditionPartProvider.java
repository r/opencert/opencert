/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.providers;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.api.providers.IPropertiesEditionPartProvider;

import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;

import org.eclipse.opencert.pam.procspec.process.parts.forms.ActivityArtefactsPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.ActivityAssuranceAssetPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.ActivityEvaluationPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.ActivityEventsPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.ActivityParticipantsPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.ActivityPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.ActivityRelPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.ActivityTechniquesPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.OrganizationPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.ParticipantPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.PersonPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.ProcessModelPropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.TechniquePropertiesEditionPartForm;
import org.eclipse.opencert.pam.procspec.process.parts.forms.ToolPropertiesEditionPartForm;

import org.eclipse.opencert.pam.procspec.process.parts.impl.ActivityArtefactsPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.ActivityAssuranceAssetPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.ActivityEvaluationPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.ActivityEventsPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.ActivityParticipantsPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.ActivityPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.ActivityRelPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.ActivityTechniquesPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.OrganizationPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.ParticipantPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.PersonPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.ProcessModelPropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.TechniquePropertiesEditionPartImpl;
import org.eclipse.opencert.pam.procspec.process.parts.impl.ToolPropertiesEditionPartImpl;

/**
 * 
 * 
 */
public class ProcessPropertiesEditionPartProvider implements IPropertiesEditionPartProvider {

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#provides(java.lang.Object)
	 * 
	 */
	public boolean provides(Object key) {
		return key == ProcessViewsRepository.class;
	}

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#getPropertiesEditionPart(java.lang.Object, int, org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(Object key, int kind, IPropertiesEditionComponent component) {
		if (key == ProcessViewsRepository.ProcessModel.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ProcessModelPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ProcessModelPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.Activity.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ActivityPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ActivityPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.Participant.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ParticipantPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ParticipantPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.Technique.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new TechniquePropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new TechniquePropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.Person.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new PersonPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new PersonPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.Tool.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ToolPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ToolPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.Organization.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new OrganizationPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new OrganizationPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.ActivityRel.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ActivityRelPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ActivityRelPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.ActivityArtefacts.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ActivityArtefactsPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ActivityArtefactsPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.ActivityParticipants.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ActivityParticipantsPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ActivityParticipantsPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.ActivityTechniques.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ActivityTechniquesPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ActivityTechniquesPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.ActivityEvaluation.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ActivityEvaluationPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ActivityEvaluationPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.ActivityEvents.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ActivityEventsPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ActivityEventsPropertiesEditionPartForm(component);
		}
		if (key == ProcessViewsRepository.ActivityAssuranceAsset.class) {
			if (kind == ProcessViewsRepository.SWT_KIND)
				return new ActivityAssuranceAssetPropertiesEditionPartImpl(component);
			if (kind == ProcessViewsRepository.FORM_KIND)
				return new ActivityAssuranceAssetPropertiesEditionPartForm(component);
		}
		return null;
	}

}
