/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package claimtypes;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.opencert.infra.general.general.GeneralPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see claimtypes.ClaimtypesFactory
 * @model kind="package"
 * @generated
 */
public interface ClaimtypesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "claimtypes";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://opencert.org/claimtypes/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ct";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClaimtypesPackage eINSTANCE = claimtypes.impl.ClaimtypesPackageImpl.init();

	/**
	 * The meta object id for the '{@link claimtypes.impl.ClaimTypeImpl <em>Claim Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see claimtypes.impl.ClaimTypeImpl
	 * @see claimtypes.impl.ClaimtypesPackageImpl#getClaimType()
	 * @generated
	 */
	int CLAIM_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Sentence Structures</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE__SENTENCE_STRUCTURES = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE__COLOR = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Claim Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Claim Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link claimtypes.impl.ClaimTypeRepositoryImpl <em>Claim Type Repository</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see claimtypes.impl.ClaimTypeRepositoryImpl
	 * @see claimtypes.impl.ClaimtypesPackageImpl#getClaimTypeRepository()
	 * @generated
	 */
	int CLAIM_TYPE_REPOSITORY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE_REPOSITORY__ID = GeneralPackage.NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE_REPOSITORY__NAME = GeneralPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Claim Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE_REPOSITORY__CLAIM_TYPES = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Claim Type Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE_REPOSITORY_FEATURE_COUNT = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Claim Type Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_TYPE_REPOSITORY_OPERATION_COUNT = GeneralPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link claimtypes.impl.SentenceStructureImpl <em>Sentence Structure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see claimtypes.impl.SentenceStructureImpl
	 * @see claimtypes.impl.ClaimtypesPackageImpl#getSentenceStructure()
	 * @generated
	 */
	int SENTENCE_STRUCTURE = 2;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENTENCE_STRUCTURE__CONTENT = 0;

	/**
	 * The number of structural features of the '<em>Sentence Structure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENTENCE_STRUCTURE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Sentence Structure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENTENCE_STRUCTURE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link claimtypes.ClaimType <em>Claim Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Claim Type</em>'.
	 * @see claimtypes.ClaimType
	 * @generated
	 */
	EClass getClaimType();

	/**
	 * Returns the meta object for the containment reference list '{@link claimtypes.ClaimType#getSentenceStructures <em>Sentence Structures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sentence Structures</em>'.
	 * @see claimtypes.ClaimType#getSentenceStructures()
	 * @see #getClaimType()
	 * @generated
	 */
	EReference getClaimType_SentenceStructures();

	/**
	 * Returns the meta object for the attribute '{@link claimtypes.ClaimType#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see claimtypes.ClaimType#getColor()
	 * @see #getClaimType()
	 * @generated
	 */
	EAttribute getClaimType_Color();

	/**
	 * Returns the meta object for class '{@link claimtypes.ClaimTypeRepository <em>Claim Type Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Claim Type Repository</em>'.
	 * @see claimtypes.ClaimTypeRepository
	 * @generated
	 */
	EClass getClaimTypeRepository();

	/**
	 * Returns the meta object for the containment reference list '{@link claimtypes.ClaimTypeRepository#getClaimTypes <em>Claim Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Claim Types</em>'.
	 * @see claimtypes.ClaimTypeRepository#getClaimTypes()
	 * @see #getClaimTypeRepository()
	 * @generated
	 */
	EReference getClaimTypeRepository_ClaimTypes();

	/**
	 * Returns the meta object for class '{@link claimtypes.SentenceStructure <em>Sentence Structure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sentence Structure</em>'.
	 * @see claimtypes.SentenceStructure
	 * @generated
	 */
	EClass getSentenceStructure();

	/**
	 * Returns the meta object for the attribute '{@link claimtypes.SentenceStructure#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see claimtypes.SentenceStructure#getContent()
	 * @see #getSentenceStructure()
	 * @generated
	 */
	EAttribute getSentenceStructure_Content();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClaimtypesFactory getClaimtypesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link claimtypes.impl.ClaimTypeImpl <em>Claim Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see claimtypes.impl.ClaimTypeImpl
		 * @see claimtypes.impl.ClaimtypesPackageImpl#getClaimType()
		 * @generated
		 */
		EClass CLAIM_TYPE = eINSTANCE.getClaimType();

		/**
		 * The meta object literal for the '<em><b>Sentence Structures</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLAIM_TYPE__SENTENCE_STRUCTURES = eINSTANCE.getClaimType_SentenceStructures();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLAIM_TYPE__COLOR = eINSTANCE.getClaimType_Color();

		/**
		 * The meta object literal for the '{@link claimtypes.impl.ClaimTypeRepositoryImpl <em>Claim Type Repository</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see claimtypes.impl.ClaimTypeRepositoryImpl
		 * @see claimtypes.impl.ClaimtypesPackageImpl#getClaimTypeRepository()
		 * @generated
		 */
		EClass CLAIM_TYPE_REPOSITORY = eINSTANCE.getClaimTypeRepository();

		/**
		 * The meta object literal for the '<em><b>Claim Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLAIM_TYPE_REPOSITORY__CLAIM_TYPES = eINSTANCE.getClaimTypeRepository_ClaimTypes();

		/**
		 * The meta object literal for the '{@link claimtypes.impl.SentenceStructureImpl <em>Sentence Structure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see claimtypes.impl.SentenceStructureImpl
		 * @see claimtypes.impl.ClaimtypesPackageImpl#getSentenceStructure()
		 * @generated
		 */
		EClass SENTENCE_STRUCTURE = eINSTANCE.getSentenceStructure();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SENTENCE_STRUCTURE__CONTENT = eINSTANCE.getSentenceStructure_Content();

	}

} //ClaimtypesPackage
