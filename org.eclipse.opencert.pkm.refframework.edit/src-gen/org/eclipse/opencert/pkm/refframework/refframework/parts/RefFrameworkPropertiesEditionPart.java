/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface RefFrameworkPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);


	/**
	 * @return the scope
	 * 
	 */
	public String getScope();

	/**
	 * Defines a new scope
	 * @param newValue the new scope to set
	 * 
	 */
	public void setScope(String newValue);


	/**
	 * @return the rev
	 * 
	 */
	public String getRev();

	/**
	 * Defines a new rev
	 * @param newValue the new rev to set
	 * 
	 */
	public void setRev(String newValue);


	/**
	 * @return the purpose
	 * 
	 */
	public String getPurpose();

	/**
	 * Defines a new purpose
	 * @param newValue the new purpose to set
	 * 
	 */
	public void setPurpose(String newValue);


	/**
	 * @return the publisher
	 * 
	 */
	public String getPublisher();

	/**
	 * Defines a new publisher
	 * @param newValue the new publisher to set
	 * 
	 */
	public void setPublisher(String newValue);


	/**
	 * @return the issued
	 * 
	 */
	public String getIssued();

	/**
	 * Defines a new issued
	 * @param newValue the new issued to set
	 * 
	 */
	public void setIssued(String newValue);




	/**
	 * Init the ownedActivities
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedActivities(ReferencesTableSettings settings);

	/**
	 * Update the ownedActivities
	 * @param newValue the ownedActivities to update
	 * 
	 */
	public void updateOwnedActivities();

	/**
	 * Adds the given filter to the ownedActivities edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedActivities(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedActivities edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedActivities(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedActivities table
	 * 
	 */
	public boolean isContainedInOwnedActivitiesTable(EObject element);




	/**
	 * Init the ownedArtefact
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedArtefact(ReferencesTableSettings settings);

	/**
	 * Update the ownedArtefact
	 * @param newValue the ownedArtefact to update
	 * 
	 */
	public void updateOwnedArtefact();

	/**
	 * Adds the given filter to the ownedArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedArtefact(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedArtefact(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedArtefact table
	 * 
	 */
	public boolean isContainedInOwnedArtefactTable(EObject element);




	/**
	 * Init the ownedRequirement
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedRequirement(ReferencesTableSettings settings);

	/**
	 * Update the ownedRequirement
	 * @param newValue the ownedRequirement to update
	 * 
	 */
	public void updateOwnedRequirement();

	/**
	 * Adds the given filter to the ownedRequirement edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedRequirement(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedRequirement edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedRequirement(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedRequirement table
	 * 
	 */
	public boolean isContainedInOwnedRequirementTable(EObject element);




	/**
	 * Init the ownedApplicLevel
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedApplicLevel(ReferencesTableSettings settings);

	/**
	 * Update the ownedApplicLevel
	 * @param newValue the ownedApplicLevel to update
	 * 
	 */
	public void updateOwnedApplicLevel();

	/**
	 * Adds the given filter to the ownedApplicLevel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedApplicLevel(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedApplicLevel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedApplicLevel(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedApplicLevel table
	 * 
	 */
	public boolean isContainedInOwnedApplicLevelTable(EObject element);




	/**
	 * Init the ownedCriticLevel
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedCriticLevel(ReferencesTableSettings settings);

	/**
	 * Update the ownedCriticLevel
	 * @param newValue the ownedCriticLevel to update
	 * 
	 */
	public void updateOwnedCriticLevel();

	/**
	 * Adds the given filter to the ownedCriticLevel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedCriticLevel(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedCriticLevel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedCriticLevel(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedCriticLevel table
	 * 
	 */
	public boolean isContainedInOwnedCriticLevelTable(EObject element);




	/**
	 * Init the ownedRole
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedRole(ReferencesTableSettings settings);

	/**
	 * Update the ownedRole
	 * @param newValue the ownedRole to update
	 * 
	 */
	public void updateOwnedRole();

	/**
	 * Adds the given filter to the ownedRole edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedRole(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedRole edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedRole(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedRole table
	 * 
	 */
	public boolean isContainedInOwnedRoleTable(EObject element);




	/**
	 * Init the ownedTechnique
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedTechnique(ReferencesTableSettings settings);

	/**
	 * Update the ownedTechnique
	 * @param newValue the ownedTechnique to update
	 * 
	 */
	public void updateOwnedTechnique();

	/**
	 * Adds the given filter to the ownedTechnique edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedTechnique(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedTechnique edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedTechnique(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedTechnique table
	 * 
	 */
	public boolean isContainedInOwnedTechniqueTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
