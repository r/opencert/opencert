/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.label;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.opencert.userguidance.labelparser.tokens.Token;

public class WrappingLabel2 extends WrappingLabel {
	private TextFlowEx2 textFlow;

	public WrappingLabel2() {
		super();
		createTextFigures();
		setTextWrap(true);
	}

	private void createTextFigures() {
		textFlow = new TextFlowEx2();
		FlowPage page = (FlowPage) getChildren().get(0);
		page.removeAll();
		page.add(textFlow);
	}

	public Token getTokenAt(int x, int y) {
		return textFlow.getTokenAt(x, y);
	}

	@Override
	public void paint(Graphics graphics) {
		Rectangle bounds = getBounds();
		IFigure flowpage = (IFigure) getChildren().get(0);
		flowpage.setBounds(new Rectangle(bounds));
		IFigure textflow = (IFigure) flowpage.getChildren().get(0);
		textflow.setBounds(new Rectangle(0, 0, bounds.width, bounds.height));

		super.paint(graphics);
	}
}
