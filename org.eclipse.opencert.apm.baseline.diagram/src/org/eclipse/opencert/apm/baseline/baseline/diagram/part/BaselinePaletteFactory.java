/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.part;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes;

/**
 * @generated
 */
public class BaselinePaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createObjects1Group());
		paletteRoot.add(createConnections2Group());
	}

	/**
	 * Creates "Objects" palette tool group
	 * @generated
	 */
	private PaletteContainer createObjects1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Objects1Group_title);
		paletteContainer.setId("createObjects1Group"); //$NON-NLS-1$
		paletteContainer.add(createBaseActivity1CreationTool());
		paletteContainer.add(createBaseArtefact2CreationTool());
		paletteContainer.add(createBaseRole3CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Connections" palette tool group
	 * @generated
	 */
	private PaletteContainer createConnections2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Connections2Group_title);
		paletteContainer.setId("createConnections2Group"); //$NON-NLS-1$
		paletteContainer.add(createPrecedingActivity1CreationTool());
		paletteContainer.add(createProducedArtefact2CreationTool());
		paletteContainer.add(createRequiredArtefact3CreationTool());
		paletteContainer.add(createRole4CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createBaseActivity1CreationTool() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(2);
		types.add(BaselineElementTypes.BaseActivity_2001);
		types.add(BaselineElementTypes.BaseActivity_3001);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.BaseActivity1CreationTool_title,
				Messages.BaseActivity1CreationTool_desc, types);
		entry.setId("createBaseActivity1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(BaselineElementTypes
				.getImageDescriptor(BaselineElementTypes.BaseActivity_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createBaseArtefact2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.BaseArtefact2CreationTool_title,
				Messages.BaseArtefact2CreationTool_desc,
				Collections
						.singletonList(BaselineElementTypes.BaseArtefact_2002));
		entry.setId("createBaseArtefact2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(BaselineElementTypes
				.getImageDescriptor(BaselineElementTypes.BaseArtefact_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createBaseRole3CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.BaseRole3CreationTool_title,
				Messages.BaseRole3CreationTool_desc,
				Collections.singletonList(BaselineElementTypes.BaseRole_2003));
		entry.setId("createBaseRole3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(BaselineElementTypes
				.getImageDescriptor(BaselineElementTypes.BaseRole_2003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createPrecedingActivity1CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.PrecedingActivity1CreationTool_title,
				Messages.PrecedingActivity1CreationTool_desc,
				Collections
						.singletonList(BaselineElementTypes.BaseActivityPrecedingActivity_4003));
		entry.setId("createPrecedingActivity1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(BaselineDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.pkm.refframework/icons/Precedence.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createProducedArtefact2CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ProducedArtefact2CreationTool_title,
				Messages.ProducedArtefact2CreationTool_desc,
				Collections
						.singletonList(BaselineElementTypes.BaseActivityProducedArtefact_4002));
		entry.setId("createProducedArtefact2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(BaselineDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.pkm.refframework/icons/Produce.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRequiredArtefact3CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.RequiredArtefact3CreationTool_title,
				Messages.RequiredArtefact3CreationTool_desc,
				Collections
						.singletonList(BaselineElementTypes.BaseActivityRequiredArtefact_4001));
		entry.setId("createRequiredArtefact3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(BaselineDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.pkm.refframework/icons/Require.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRole4CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Role4CreationTool_title,
				Messages.Role4CreationTool_desc,
				Collections
						.singletonList(BaselineElementTypes.BaseActivityRole_4004));
		entry.setId("createRole4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(BaselineDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.pkm.refframework/icons/Executing.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
