/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.figures;

import java.util.HashMap;
import java.util.Iterator;
 
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PrecisionPoint;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
 
public class ChoiceDefaultSizeNodeFigureWithFixedAnchors extends
		DefaultSizeNodeFigure {
 
	private HashMap<String, FixedConnectionAnchor> anchors = new HashMap<String, FixedConnectionAnchor>();
 
	public ChoiceDefaultSizeNodeFigureWithFixedAnchors(Dimension defSize, HashMap<String, PrecisionPoint> anchorLocations) {
		this(defSize.width, defSize.height, anchorLocations);
	}
 
	public ChoiceDefaultSizeNodeFigureWithFixedAnchors(int width, int height, HashMap<String, PrecisionPoint> anchorLocations) {
		super(width, height);
		if (anchorLocations.size()==0)
			throw new IllegalArgumentException("At least one fixed anchor must be specified");
		Iterator<String> terminals = anchorLocations.keySet().iterator();
		while (terminals.hasNext()) {
			String terminal = terminals.next();
			PrecisionPoint anchorLocation = anchorLocations.get(terminal);
			anchors.put(terminal, new FixedConnectionAnchor(this, anchorLocation));
		}
	}
 
	@Override
	public ConnectionAnchor getSourceConnectionAnchorAt(Point p) {
		// Start MCP
		//return findNearestAnchorFrom(p);
		ConnectionAnchor result = anchors.get("SOUTH");
			
		return result;
		// End MCP
	}
 
	@Override
	public ConnectionAnchor getTargetConnectionAnchorAt(Point p) {
		// Start MCP
		// return findNearestAnchorFrom(p);
		ConnectionAnchor result = anchors.get("NORTH");
			
		return result;
		// End MCP
	}
 
	@Override
	public ConnectionAnchor getConnectionAnchor(String terminal) {
		return anchors.get(terminal);
	}
 
	@Override
	public String getConnectionAnchorTerminal(ConnectionAnchor c) {
		String selectedTerminal = null;
		Iterator<String> terminals = anchors.keySet().iterator();
		while (terminals.hasNext() && selectedTerminal==null) {
			String terminal = terminals.next();
			FixedConnectionAnchor anchor = anchors.get(terminal);
			if (anchor == c) {
				selectedTerminal = terminal;
			}
		}
		return selectedTerminal;
	}
 
	private ConnectionAnchor findNearestAnchorFrom(Point point) {
		ConnectionAnchor result = null;
		if (point == null || anchors.size()==1) {
			result = anchors.values().iterator().next();
		}
		else {
			double minDistance = Double.MAX_VALUE;
			String nearestTerminal = null;
			Iterator<String> terminals = anchors.keySet().iterator();
			while (terminals.hasNext()) {
				String terminal = terminals.next();
				FixedConnectionAnchor anchor = anchors.get(terminal);
				Point anchorPosition = anchor.getLocation();
				double distance = point.getDistance(anchorPosition);
				if (distance < minDistance) {
					minDistance = distance;
					nearestTerminal = terminal;
				}
			}
			result = anchors.get(nearestTerminal);
		}
		return result;
	}
 
}

