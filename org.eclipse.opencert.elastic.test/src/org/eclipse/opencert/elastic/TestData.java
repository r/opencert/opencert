/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.VisibilityKind;

/**
 * Simple factory to quickly create test data.
 * 
 * @author mauersberger
 */
public class TestData {

	static Component createComponent() {
		return createComponent(false);
	}

	static Component createComponent(boolean inResource) {
		Component comp = UMLFactory.eINSTANCE.createComponent();
		comp.setName("Component A");
		comp.setVisibility(VisibilityKind.PUBLIC_LITERAL);

		if (inResource) {
			ResourceImpl res = new ResourceImpl(URI.createURI("custom:somewhere"));
			res.getContents().add(comp);
		}

		return comp;
	}

	static Iterator<EObject> uml() {
		return UMLPackage.eINSTANCE.eAllContents();
	}

	static Iterator<EObject> umlClasses() {
		List<EObject> classes = new ArrayList<>();
		Iterator<EObject> it = uml();
		while (it.hasNext()) {
			EObject object = (EObject) it.next();
			if (object instanceof EClass) {
				classes.add(object);
			}
		}
		
		System.out.println("Classes: " + classes.size());
		return classes.iterator();
	}
	
	static Iterator<EObject> uml(final int max) {
		return new Iterator<EObject>() {
			private Iterator<EObject> delegate = UMLPackage.eINSTANCE.eAllContents();
			private long i = 0;

			@Override
			public boolean hasNext() {
				return i < max && delegate.hasNext();
			}

			@Override
			public EObject next() {
				i++;
				return delegate.next();
			}
		};
	}
}
