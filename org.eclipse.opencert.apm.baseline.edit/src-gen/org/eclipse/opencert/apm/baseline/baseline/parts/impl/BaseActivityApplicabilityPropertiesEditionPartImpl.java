/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;

// End of user code

/**
 * 
 * 
 */
public class BaseActivityApplicabilityPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseActivityApplicabilityPropertiesEditionPart {

	protected ReferencesTable applicability;
	protected List<ViewerFilter> applicabilityBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> applicabilityFilters = new ArrayList<ViewerFilter>();
	protected TableViewer applicabilityTable;
	protected List<ViewerFilter> applicabilityTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> applicabilityTableFilters = new ArrayList<ViewerFilter>();
	protected Button addApplicabilityTable;
	protected Button removeApplicabilityTable;
	protected Button editApplicabilityTable;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseActivityApplicabilityPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseActivityApplicabilityStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseActivityApplicabilityStep.addStep(BaselineViewsRepository.BaseActivityApplicability.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseActivityApplicability.Properties.applicability);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable);
		
		
		composer = new PartComposer(baseActivityApplicabilityStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseActivityApplicability.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseActivityApplicability.Properties.applicability) {
					return createApplicabilityAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable) {
					return createApplicabilityTableTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseActivityApplicabilityPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createApplicabilityAdvancedTableComposition(Composite parent) {
		this.applicability = new ReferencesTable(getDescription(BaselineViewsRepository.BaseActivityApplicability.Properties.applicability, BaselineMessages.BaseActivityApplicabilityPropertiesEditionPart_ApplicabilityLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityApplicabilityPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityApplicability.Properties.applicability, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				applicability.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityApplicabilityPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityApplicability.Properties.applicability, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				applicability.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityApplicabilityPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityApplicability.Properties.applicability, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				applicability.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityApplicabilityPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityApplicability.Properties.applicability, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				applicability.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.applicabilityFilters) {
			this.applicability.addFilter(filter);
		}
		this.applicability.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseActivityApplicability.Properties.applicability, BaselineViewsRepository.SWT_KIND));
		this.applicability.createControls(parent);
		this.applicability.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityApplicabilityPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityApplicability.Properties.applicability, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData applicabilityData = new GridData(GridData.FILL_HORIZONTAL);
		applicabilityData.horizontalSpan = 3;
		this.applicability.setLayoutData(applicabilityData);
		this.applicability.setLowerBound(0);
		this.applicability.setUpperBound(-1);
		applicability.setID(BaselineViewsRepository.BaseActivityApplicability.Properties.applicability);
		applicability.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createApplicabilityAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createApplicabilityTableTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableApplicabilityTable = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableApplicabilityTable.setHeaderVisible(true);
		GridData gdApplicabilityTable = new GridData();
		gdApplicabilityTable.grabExcessHorizontalSpace = true;
		gdApplicabilityTable.horizontalAlignment = GridData.FILL;
		gdApplicabilityTable.grabExcessVerticalSpace = true;
		gdApplicabilityTable.verticalAlignment = GridData.FILL;
		tableApplicabilityTable.setLayoutData(gdApplicabilityTable);
		tableApplicabilityTable.setLinesVisible(true);

		// Start of user code for columns definition for ApplicabilityTable
		// Start IRR
		TableColumn nameID = new TableColumn(tableApplicabilityTable, SWT.NONE);
		nameID.setWidth(80);
		nameID.setText("ID"); //$NON-NLS-1$
		TableColumn nameMethod = new TableColumn(tableApplicabilityTable,
				SWT.NONE);
		nameMethod.setWidth(150);
		nameMethod.setText("Requirements"); //$NON-NLS-1$
		TableColumn nameCA = new TableColumn(tableApplicabilityTable, SWT.NONE);
		nameCA.setWidth(150);
		nameCA.setText("Criticality : Applicability"); //$NON-NLS-1$
		// End IRR
		// End of user code

		applicabilityTable = new TableViewer(tableApplicabilityTable);
		applicabilityTable.setContentProvider(new ArrayContentProvider());
		applicabilityTable.setLabelProvider(new ITableLabelProvider() {
			// Start of user code for label provider definition for
						// ApplicabilityTable
						public String getColumnText(Object object, int columnIndex) {
							// Start IRR
							// AdapterFactoryLabelProvider labelProvider = new
							// AdapterFactoryLabelProvider(adapterFactory);

							/*
							 * if (object instanceof EObject) { switch (columnIndex) { case
							 * 0: return labelProvider.getText(object); } }
							 */

							if (object instanceof EObject) {
								BaseApplicability refApplicability = (BaseApplicability) object;

								switch (columnIndex) {
								case 0:
									return refApplicability.getId();
								case 1:
									if (refApplicability.getApplicTarget() instanceof BaseRequirement) {
										BaseRequirement refRequirement = (BaseRequirement) refApplicability
												.getApplicTarget();
										if (refRequirement != null)
											return refRequirement.getName();
										else
											return "";
									} else {
										return "";
									}

								case 2:
									String sApplicability = "";
									if (refApplicability.getApplicCritic() == null)
										return "";
									else {
										EList<BaseCriticalityApplicability> LstRefCriticalityApplicability = refApplicability
												.getApplicCritic();
										Iterator<BaseCriticalityApplicability> iter = LstRefCriticalityApplicability
												.iterator();
										String sArtefact = "";
										while (iter.hasNext()) {
											BaseCriticalityApplicability refCritAppli = (BaseCriticalityApplicability) iter
													.next();
											if (refApplicability.getApplicTarget() instanceof BaseRequirement) {
												BaseApplicabilityLevel refIndependencyLeve = (BaseApplicabilityLevel) refCritAppli
														.getApplicLevel();
												if (refIndependencyLeve != null)
													sApplicability = refIndependencyLeve
															.getId();
											}

											BaseCriticalityLevel refCriticalityLevel = refCritAppli
													.getCriticLevel();
											if (refCriticalityLevel != null)
												sArtefact = sArtefact + " ("
														+ refCriticalityLevel.getId()
														+ " : " + sApplicability + ") ";
										}

										return sArtefact;
									}

								}
							}

							// End IRR
							return ""; //$NON-NLS-1$
						}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		applicabilityTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (applicabilityTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) applicabilityTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityApplicabilityPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						applicabilityTable.refresh();
					}
				}
			}

		});
		GridData applicabilityTableData = new GridData(GridData.FILL_HORIZONTAL);
		applicabilityTableData.minimumHeight = 120;
		applicabilityTableData.heightHint = 120;
		applicabilityTable.getTable().setLayoutData(applicabilityTableData);
		for (ViewerFilter filter : this.applicabilityTableFilters) {
			applicabilityTable.addFilter(filter);
		}
		EditingUtils.setID(applicabilityTable.getTable(), BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable);
		EditingUtils.setEEFtype(applicabilityTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createApplicabilityTablePanel(tableContainer);
		// Start of user code for createApplicabilityTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createApplicabilityTablePanel(Composite container) {
		Composite applicabilityTablePanel = new Composite(container, SWT.NONE);
		GridLayout applicabilityTablePanelLayout = new GridLayout();
		applicabilityTablePanelLayout.numColumns = 1;
		applicabilityTablePanel.setLayout(applicabilityTablePanelLayout);
		addApplicabilityTable = new Button(applicabilityTablePanel, SWT.NONE);
		addApplicabilityTable.setText(BaselineMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addApplicabilityTableData = new GridData(GridData.FILL_HORIZONTAL);
		addApplicabilityTable.setLayoutData(addApplicabilityTableData);
		addApplicabilityTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityApplicabilityPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				applicabilityTable.refresh();
			}
		});
		EditingUtils.setID(addApplicabilityTable, BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable);
		EditingUtils.setEEFtype(addApplicabilityTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeApplicabilityTable = new Button(applicabilityTablePanel, SWT.NONE);
		removeApplicabilityTable.setText(BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeApplicabilityTableData = new GridData(GridData.FILL_HORIZONTAL);
		removeApplicabilityTable.setLayoutData(removeApplicabilityTableData);
		removeApplicabilityTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (applicabilityTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) applicabilityTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityApplicabilityPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						applicabilityTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeApplicabilityTable, BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable);
		EditingUtils.setEEFtype(removeApplicabilityTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editApplicabilityTable = new Button(applicabilityTablePanel, SWT.NONE);
		editApplicabilityTable.setText(BaselineMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editApplicabilityTableData = new GridData(GridData.FILL_HORIZONTAL);
		editApplicabilityTable.setLayoutData(editApplicabilityTableData);
		editApplicabilityTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (applicabilityTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) applicabilityTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityApplicabilityPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						applicabilityTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editApplicabilityTable, BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable);
		EditingUtils.setEEFtype(editApplicabilityTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createApplicabilityTablePanel

		// End of user code
		return applicabilityTablePanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart#initApplicability(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initApplicability(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		applicability.setContentProvider(contentProvider);
		applicability.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseActivityApplicability.Properties.applicability);
		if (eefElementEditorReadOnlyState && applicability.isEnabled()) {
			applicability.setEnabled(false);
			applicability.setToolTipText(BaselineMessages.BaseActivityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicability.isEnabled()) {
			applicability.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart#updateApplicability()
	 * 
	 */
	public void updateApplicability() {
	applicability.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart#addFilterApplicability(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicability(ViewerFilter filter) {
		applicabilityFilters.add(filter);
		if (this.applicability != null) {
			this.applicability.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart#addBusinessFilterApplicability(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToApplicability(ViewerFilter filter) {
		applicabilityBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart#isContainedInApplicabilityTable(EObject element)
	 * 
	 */
	public boolean isContainedInApplicabilityTable(EObject element) {
		return ((ReferencesTableSettings)applicability.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart#initApplicabilityTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initApplicabilityTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		applicabilityTable.setContentProvider(contentProvider);
		applicabilityTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseActivityApplicability.Properties.applicabilityTable);
		if (eefElementEditorReadOnlyState && applicabilityTable.getTable().isEnabled()) {
			applicabilityTable.getTable().setEnabled(false);
			applicabilityTable.getTable().setToolTipText(BaselineMessages.BaseActivityApplicability_ReadOnly);
			addApplicabilityTable.setEnabled(false);
			addApplicabilityTable.setToolTipText(BaselineMessages.BaseActivityApplicability_ReadOnly);
			removeApplicabilityTable.setEnabled(false);
			removeApplicabilityTable.setToolTipText(BaselineMessages.BaseActivityApplicability_ReadOnly);
			editApplicabilityTable.setEnabled(false);
			editApplicabilityTable.setToolTipText(BaselineMessages.BaseActivityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicabilityTable.getTable().isEnabled()) {
			applicabilityTable.getTable().setEnabled(true);
			addApplicabilityTable.setEnabled(true);
			removeApplicabilityTable.setEnabled(true);
			editApplicabilityTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart#updateApplicabilityTable()
	 * 
	 */
	public void updateApplicabilityTable() {
	applicabilityTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart#addFilterApplicabilityTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicabilityTable(ViewerFilter filter) {
		applicabilityTableFilters.add(filter);
		if (this.applicabilityTable != null) {
			this.applicabilityTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart#addBusinessFilterApplicabilityTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToApplicabilityTable(ViewerFilter filter) {
		applicabilityTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart#isContainedInApplicabilityTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInApplicabilityTableTable(EObject element) {
		return ((ReferencesTableSettings)applicabilityTable.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseActivityApplicability_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
