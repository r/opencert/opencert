/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.baseline.baseline.util;


import org.eclipse.emf.cdo.dawn.resources.DawnWrapperResource;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecoretools.diagram.ui.outline.AdditionalResources;


public class ModelStorage {
	
	private static ModelStorage instance;
	private static URI resources=null;;

	public static ModelStorage getInstance(){
		if(instance==null){
			instance= new ModelStorage();			 
		}
		return instance;
	}
	
	public URI getResourceURI() {
		return resources;
	}
	
	public void setResourceURI(AdditionalResources Aresources) {
		
		DawnWrapperResource myED= (DawnWrapperResource)Aresources.getResources().get(0);		
		if(myED.getResourceSet().getResources().size()>1){
			resources = myED.getResourceSet().getResources().get(1).getURI();
		}		
	}
	
	
}