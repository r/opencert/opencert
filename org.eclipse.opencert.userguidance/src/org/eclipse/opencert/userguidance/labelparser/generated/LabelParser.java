/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
// Generated from Label.g4 by ANTLR 4.1

package org.eclipse.opencert.userguidance.labelparser.generated;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LabelParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, SEPARATOR=2, SQ=3, DQ=4, WS=5, SPECIFIER=6, WORD=7;
	public static final String[] tokenNames = {
		"<INVALID>", "'|'", "':'", "'''", "'\"'", "WS", "SPECIFIER", "WORD"
	};
	public static final int
		RULE_item = 0, RULE_itembody = 1, RULE_text = 2;
	public static final String[] ruleNames = {
		"item", "itembody", "text"
	};

	@Override
	public String getGrammarFileName() { return "Label.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public LabelParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ItemContext extends ParserRuleContext {
		public ItembodyContext itembody() {
			return getRuleContext(ItembodyContext.class,0);
		}
		public TerminalNode SPECIFIER() { return getToken(LabelParser.SPECIFIER, 0); }
		public TerminalNode SEPARATOR() { return getToken(LabelParser.SEPARATOR, 0); }
		public ItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_item; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelListener ) ((LabelListener)listener).enterItem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelListener ) ((LabelListener)listener).exitItem(this);
		}
	}

	public final ItemContext item() throws RecognitionException {
		ItemContext _localctx = new ItemContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_item);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(6); match(SPECIFIER);
			setState(7); match(SEPARATOR);
			setState(8); itembody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ItembodyContext extends ParserRuleContext {
		public List<TerminalNode> WORD() { return getTokens(LabelParser.WORD); }
		public List<TerminalNode> WS() { return getTokens(LabelParser.WS); }
		public TerminalNode DQ(int i) {
			return getToken(LabelParser.DQ, i);
		}
		public TerminalNode SQ(int i) {
			return getToken(LabelParser.SQ, i);
		}
		public TerminalNode SEPARATOR(int i) {
			return getToken(LabelParser.SEPARATOR, i);
		}
		public List<TerminalNode> SEPARATOR() { return getTokens(LabelParser.SEPARATOR); }
		public TerminalNode WS(int i) {
			return getToken(LabelParser.WS, i);
		}
		public List<TerminalNode> DQ() { return getTokens(LabelParser.DQ); }
		public List<TerminalNode> SQ() { return getTokens(LabelParser.SQ); }
		public TerminalNode WORD(int i) {
			return getToken(LabelParser.WORD, i);
		}
		public ItembodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_itembody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelListener ) ((LabelListener)listener).enterItembody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelListener ) ((LabelListener)listener).exitItembody(this);
		}
	}

	public final ItembodyContext itembody() throws RecognitionException {
		ItembodyContext _localctx = new ItembodyContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_itembody);
		int _la;
		try {
			int _alt;
			setState(59);
			switch (_input.LA(1)) {
			case EOF:
			case 1:
			case SEPARATOR:
			case WS:
			case SPECIFIER:
			case WORD:
				enterOuterAlt(_localctx, 1);
				{
				setState(13);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
				while ( _alt!=2 && _alt!=-1 ) {
					if ( _alt==1 ) {
						{
						{
						setState(10);
						_la = _input.LA(1);
						if ( !(_la==SEPARATOR || _la==WORD) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						}
						} 
					}
					setState(15);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
				}
				setState(23);
				_la = _input.LA(1);
				if (_la==1) {
					{
					setState(16); match(1);
					setState(20);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
					while ( _alt!=2 && _alt!=-1 ) {
						if ( _alt==1 ) {
							{
							{
							setState(17);
							_la = _input.LA(1);
							if ( !(_la==SEPARATOR || _la==WORD) ) {
							_errHandler.recoverInline(this);
							}
							consume();
							}
							} 
						}
						setState(22);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
					}
					}
				}

				}
				break;
			case SQ:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(25); match(SQ);
				setState(29);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SEPARATOR) | (1L << WS) | (1L << WORD))) != 0)) {
					{
					{
					setState(26);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SEPARATOR) | (1L << WS) | (1L << WORD))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
					}
					setState(31);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(39);
				_la = _input.LA(1);
				if (_la==1) {
					{
					setState(32); match(1);
					setState(36);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SEPARATOR) | (1L << WS) | (1L << WORD))) != 0)) {
						{
						{
						setState(33);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SEPARATOR) | (1L << WS) | (1L << WORD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						}
						}
						setState(38);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(41); match(SQ);
				}
				}
				break;
			case DQ:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(42); match(DQ);
				setState(46);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SEPARATOR) | (1L << WS) | (1L << WORD))) != 0)) {
					{
					{
					setState(43);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SEPARATOR) | (1L << WS) | (1L << WORD))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
					}
					setState(48);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(56);
				_la = _input.LA(1);
				if (_la==1) {
					{
					setState(49); match(1);
					setState(53);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SEPARATOR) | (1L << WS) | (1L << WORD))) != 0)) {
						{
						{
						setState(50);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SEPARATOR) | (1L << WS) | (1L << WORD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						}
						}
						setState(55);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(58); match(DQ);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TextContext extends ParserRuleContext {
		public List<TerminalNode> WORD() { return getTokens(LabelParser.WORD); }
		public ItemContext item(int i) {
			return getRuleContext(ItemContext.class,i);
		}
		public List<ItemContext> item() {
			return getRuleContexts(ItemContext.class);
		}
		public List<TerminalNode> WS() { return getTokens(LabelParser.WS); }
		public TerminalNode SEPARATOR(int i) {
			return getToken(LabelParser.SEPARATOR, i);
		}
		public List<TerminalNode> SEPARATOR() { return getTokens(LabelParser.SEPARATOR); }
		public TerminalNode WS(int i) {
			return getToken(LabelParser.WS, i);
		}
		public TerminalNode WORD(int i) {
			return getToken(LabelParser.WORD, i);
		}
		public TextContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_text; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelListener ) ((LabelListener)listener).enterText(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelListener ) ((LabelListener)listener).exitText(this);
		}
	}

	public final TextContext text() throws RecognitionException {
		TextContext _localctx = new TextContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_text);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(67);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SEPARATOR) | (1L << WS) | (1L << SPECIFIER) | (1L << WORD))) != 0)) {
				{
				setState(65);
				switch (_input.LA(1)) {
				case WS:
					{
					setState(61); match(WS);
					}
					break;
				case SPECIFIER:
					{
					setState(62); item();
					}
					break;
				case WORD:
					{
					setState(63); match(WORD);
					}
					break;
				case SEPARATOR:
					{
					setState(64); match(SEPARATOR);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(69);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3\tI\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\3\2\3\2\3\2\3\2\3\3\7\3\16\n\3\f\3\16\3\21\13\3\3\3\3\3\7\3"+
		"\25\n\3\f\3\16\3\30\13\3\5\3\32\n\3\3\3\3\3\7\3\36\n\3\f\3\16\3!\13\3"+
		"\3\3\3\3\7\3%\n\3\f\3\16\3(\13\3\5\3*\n\3\3\3\3\3\3\3\7\3/\n\3\f\3\16"+
		"\3\62\13\3\3\3\3\3\7\3\66\n\3\f\3\16\39\13\3\5\3;\n\3\3\3\5\3>\n\3\3\4"+
		"\3\4\3\4\3\4\7\4D\n\4\f\4\16\4G\13\4\3\4\2\5\2\4\6\2\4\4\2\4\4\t\t\5\2"+
		"\4\4\7\7\t\tT\2\b\3\2\2\2\4=\3\2\2\2\6E\3\2\2\2\b\t\7\b\2\2\t\n\7\4\2"+
		"\2\n\13\5\4\3\2\13\3\3\2\2\2\f\16\t\2\2\2\r\f\3\2\2\2\16\21\3\2\2\2\17"+
		"\r\3\2\2\2\17\20\3\2\2\2\20\31\3\2\2\2\21\17\3\2\2\2\22\26\7\3\2\2\23"+
		"\25\t\2\2\2\24\23\3\2\2\2\25\30\3\2\2\2\26\24\3\2\2\2\26\27\3\2\2\2\27"+
		"\32\3\2\2\2\30\26\3\2\2\2\31\22\3\2\2\2\31\32\3\2\2\2\32>\3\2\2\2\33\37"+
		"\7\5\2\2\34\36\t\3\2\2\35\34\3\2\2\2\36!\3\2\2\2\37\35\3\2\2\2\37 \3\2"+
		"\2\2 )\3\2\2\2!\37\3\2\2\2\"&\7\3\2\2#%\t\3\2\2$#\3\2\2\2%(\3\2\2\2&$"+
		"\3\2\2\2&\'\3\2\2\2\'*\3\2\2\2(&\3\2\2\2)\"\3\2\2\2)*\3\2\2\2*+\3\2\2"+
		"\2+>\7\5\2\2,\60\7\6\2\2-/\t\3\2\2.-\3\2\2\2/\62\3\2\2\2\60.\3\2\2\2\60"+
		"\61\3\2\2\2\61:\3\2\2\2\62\60\3\2\2\2\63\67\7\3\2\2\64\66\t\3\2\2\65\64"+
		"\3\2\2\2\669\3\2\2\2\67\65\3\2\2\2\678\3\2\2\28;\3\2\2\29\67\3\2\2\2:"+
		"\63\3\2\2\2:;\3\2\2\2;<\3\2\2\2<>\7\6\2\2=\17\3\2\2\2=\33\3\2\2\2=,\3"+
		"\2\2\2>\5\3\2\2\2?D\7\7\2\2@D\5\2\2\2AD\7\t\2\2BD\7\4\2\2C?\3\2\2\2C@"+
		"\3\2\2\2CA\3\2\2\2CB\3\2\2\2DG\3\2\2\2EC\3\2\2\2EF\3\2\2\2F\7\3\2\2\2"+
		"GE\3\2\2\2\16\17\26\31\37&)\60\67:=CE";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}