/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.forms;

// Start of user code for imports


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.EEFRuntimePlugin;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.providers.EMFListContentProvider;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

// End of user code

/**
 * 
 * 
 */
public class BaseEquivalenceMapPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, BaseEquivalenceMapPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected EObjectFlatComboViewer mapGroup;
	protected EMFComboViewer mapGroupCombo;
	protected EMFComboViewer type;
	protected Text justification;
	protected ReferencesTable target;
	protected List<ViewerFilter> targetBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> targetFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public BaseEquivalenceMapPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseEquivalenceMapPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence baseEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseEquivalenceMapStep.addStep(BaselineViewsRepository.BaseEquivalenceMap.Properties.class);
		propertiesStep.addStep(BaselineViewsRepository.BaseEquivalenceMap.Properties.id);
		propertiesStep.addStep(BaselineViewsRepository.BaseEquivalenceMap.Properties.name);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroup);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroupCombo);
		propertiesStep.addStep(BaselineViewsRepository.BaseEquivalenceMap.Properties.type);
		propertiesStep.addStep(BaselineViewsRepository.BaseEquivalenceMap.Properties.justification);
		propertiesStep.addStep(BaselineViewsRepository.BaseEquivalenceMap.Properties.target);
		
		
		composer = new PartComposer(baseEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseEquivalenceMap.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseEquivalenceMap.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseEquivalenceMap.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroup) {
					return createMapGroupFlatComboViewer(parent, widgetFactory);
				}
				if (key == BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroupCombo) {
					return createMapGroupComboEMFComboViewer(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseEquivalenceMap.Properties.type) {
					return createTypeEMFComboViewer(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseEquivalenceMap.Properties.justification) {
					return createJustificationTextarea(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseEquivalenceMap.Properties.target) {
					return createTargetReferencesTable(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(BaselineMessages.BaseEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseEquivalenceMap.Properties.id, BaselineMessages.BaseEquivalenceMapPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							BaseEquivalenceMapPropertiesEditionPartForm.this,
							BaselineViewsRepository.BaseEquivalenceMap.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseEquivalenceMapPropertiesEditionPartForm.this,
									BaselineViewsRepository.BaseEquivalenceMap.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseEquivalenceMapPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseEquivalenceMap.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, BaselineViewsRepository.BaseEquivalenceMap.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseEquivalenceMap.Properties.id, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseEquivalenceMap.Properties.name, BaselineMessages.BaseEquivalenceMapPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							BaseEquivalenceMapPropertiesEditionPartForm.this,
							BaselineViewsRepository.BaseEquivalenceMap.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseEquivalenceMapPropertiesEditionPartForm.this,
									BaselineViewsRepository.BaseEquivalenceMap.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseEquivalenceMapPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseEquivalenceMap.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, BaselineViewsRepository.BaseEquivalenceMap.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseEquivalenceMap.Properties.name, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * @param widgetFactory factory to use to instanciante widget of the form
	 * 
	 */
	protected Composite createMapGroupFlatComboViewer(Composite parent, FormToolkit widgetFactory) {
		createDescription(parent, BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroup, BaselineMessages.BaseEquivalenceMapPropertiesEditionPart_MapGroupLabel);
		mapGroup = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroup, BaselineViewsRepository.FORM_KIND));
		widgetFactory.adapt(mapGroup);
		mapGroup.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		GridData mapGroupData = new GridData(GridData.FILL_HORIZONTAL);
		mapGroup.setLayoutData(mapGroupData);
		mapGroup.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroup, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getMapGroup()));
			}

		});
		mapGroup.setID(BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroup);
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroup, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createMapGroupFlatComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createMapGroupComboEMFComboViewer(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroupCombo, BaselineMessages.BaseEquivalenceMapPropertiesEditionPart_MapGroupComboLabel);
		mapGroupCombo = new EMFComboViewer(parent);
		GridData mapGroupComboData = new GridData(GridData.FILL_HORIZONTAL);
		mapGroupCombo.getCombo().setLayoutData(mapGroupComboData);
		mapGroupCombo.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		mapGroupCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroupCombo, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getMapGroupCombo()));
			}

		});
		mapGroupCombo.setContentProvider(new EMFListContentProvider());
		EditingUtils.setID(mapGroupCombo.getCombo(), BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroupCombo);
		EditingUtils.setEEFtype(mapGroupCombo.getCombo(), "eef::Combo");
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroupCombo, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createMapGroupComboEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createTypeEMFComboViewer(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseEquivalenceMap.Properties.type, BaselineMessages.BaseEquivalenceMapPropertiesEditionPart_TypeLabel);
		type = new EMFComboViewer(parent);
		type.setContentProvider(new ArrayContentProvider());
		type.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData typeData = new GridData(GridData.FILL_HORIZONTAL);
		type.getCombo().setLayoutData(typeData);
		type.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseEquivalenceMap.Properties.type, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getType()));
			}

		});
		type.setID(BaselineViewsRepository.BaseEquivalenceMap.Properties.type);
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseEquivalenceMap.Properties.type, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createTypeEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createJustificationTextarea(FormToolkit widgetFactory, Composite parent) {
		Label justificationLabel = createDescription(parent, BaselineViewsRepository.BaseEquivalenceMap.Properties.justification, BaselineMessages.BaseEquivalenceMapPropertiesEditionPart_JustificationLabel);
		GridData justificationLabelData = new GridData(GridData.FILL_HORIZONTAL);
		justificationLabelData.horizontalSpan = 3;
		justificationLabel.setLayoutData(justificationLabelData);
		justification = widgetFactory.createText(parent, "", SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL); //$NON-NLS-1$
		GridData justificationData = new GridData(GridData.FILL_HORIZONTAL);
		justificationData.horizontalSpan = 2;
		justificationData.heightHint = 80;
		justificationData.widthHint = 200;
		justification.setLayoutData(justificationData);
		justification.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							BaseEquivalenceMapPropertiesEditionPartForm.this,
							BaselineViewsRepository.BaseEquivalenceMap.Properties.justification,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, justification.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseEquivalenceMapPropertiesEditionPartForm.this,
									BaselineViewsRepository.BaseEquivalenceMap.Properties.justification,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, justification.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseEquivalenceMapPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		EditingUtils.setID(justification, BaselineViewsRepository.BaseEquivalenceMap.Properties.justification);
		EditingUtils.setEEFtype(justification, "eef::Textarea"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseEquivalenceMap.Properties.justification, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createJustificationTextArea

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createTargetReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.target = new ReferencesTable(getDescription(BaselineViewsRepository.BaseEquivalenceMap.Properties.target, BaselineMessages.BaseEquivalenceMapPropertiesEditionPart_TargetLabel), new ReferencesTableListener	() {
			public void handleAdd() { addTarget(); }
			public void handleEdit(EObject element) { editTarget(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveTarget(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromTarget(element); }
			public void navigateTo(EObject element) { }
		});
		this.target.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseEquivalenceMap.Properties.target, BaselineViewsRepository.FORM_KIND));
		this.target.createControls(parent, widgetFactory);
		this.target.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseEquivalenceMap.Properties.target, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData targetData = new GridData(GridData.FILL_HORIZONTAL);
		targetData.horizontalSpan = 3;
		this.target.setLayoutData(targetData);
		this.target.disableMove();
		target.setID(BaselineViewsRepository.BaseEquivalenceMap.Properties.target);
		target.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createTargetReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addTarget() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(target.getInput(), targetFilters, targetBusinessFilters,
		"target", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseEquivalenceMap.Properties.target,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				target.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveTarget(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseEquivalenceMap.Properties.target, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		target.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromTarget(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseEquivalenceMap.Properties.target, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		target.refresh();
	}

	/**
	 * 
	 */
	protected void editTarget(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				target.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseEquivalenceMap.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(BaselineMessages.BaseEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseEquivalenceMap.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(BaselineMessages.BaseEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#getMapGroup()
	 * 
	 */
	public EObject getMapGroup() {
		if (mapGroup.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) mapGroup.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#initMapGroup(EObjectFlatComboSettings)
	 */
	public void initMapGroup(EObjectFlatComboSettings settings) {
		mapGroup.setInput(settings);
		if (current != null) {
			mapGroup.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroup);
		if (eefElementEditorReadOnlyState && mapGroup.isEnabled()) {
			mapGroup.setEnabled(false);
			mapGroup.setToolTipText(BaselineMessages.BaseEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !mapGroup.isEnabled()) {
			mapGroup.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#setMapGroup(EObject newValue)
	 * 
	 */
	public void setMapGroup(EObject newValue) {
		if (newValue != null) {
			mapGroup.setSelection(new StructuredSelection(newValue));
		} else {
			mapGroup.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroup);
		if (eefElementEditorReadOnlyState && mapGroup.isEnabled()) {
			mapGroup.setEnabled(false);
			mapGroup.setToolTipText(BaselineMessages.BaseEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !mapGroup.isEnabled()) {
			mapGroup.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#setMapGroupButtonMode(ButtonsModeEnum newValue)
	 */
	public void setMapGroupButtonMode(ButtonsModeEnum newValue) {
		mapGroup.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#addFilterMapGroup(ViewerFilter filter)
	 * 
	 */
	public void addFilterToMapGroup(ViewerFilter filter) {
		mapGroup.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#addBusinessFilterMapGroup(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToMapGroup(ViewerFilter filter) {
		mapGroup.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#getMapGroupCombo()
	 * 
	 */
	public Object getMapGroupCombo() {
		if (mapGroupCombo.getSelection() instanceof StructuredSelection) {
			return ((StructuredSelection) mapGroupCombo.getSelection()).getFirstElement();
		}
		return "";
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#initMapGroupCombo(Object input, Object currentValue)
	 */
	public void initMapGroupCombo(Object input, Object currentValue) {
		mapGroupCombo.setInput(input);
		if (currentValue != null) {
			mapGroupCombo.setSelection(new StructuredSelection(currentValue));
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#setMapGroupCombo(Object newValue)
	 * 
	 */
	public void setMapGroupCombo(Object newValue) {
		if (newValue != null) {
			mapGroupCombo.modelUpdating(new StructuredSelection(newValue));
		} else {
			mapGroupCombo.modelUpdating(new StructuredSelection("")); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseEquivalenceMap.Properties.mapGroupCombo);
		if (eefElementEditorReadOnlyState && mapGroupCombo.isEnabled()) {
			mapGroupCombo.setEnabled(false);
			mapGroupCombo.setToolTipText(BaselineMessages.BaseEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !mapGroupCombo.isEnabled()) {
			mapGroupCombo.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#addFilterMapGroupCombo(ViewerFilter filter)
	 * 
	 */
	public void addFilterToMapGroupCombo(ViewerFilter filter) {
		mapGroupCombo.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#getType()
	 * 
	 */
	public Enumerator getType() {
		Enumerator selection = (Enumerator) ((StructuredSelection) type.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#initType(Object input, Enumerator current)
	 */
	public void initType(Object input, Enumerator current) {
		type.setInput(input);
		type.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseEquivalenceMap.Properties.type);
		if (eefElementEditorReadOnlyState && type.isEnabled()) {
			type.setEnabled(false);
			type.setToolTipText(BaselineMessages.BaseEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !type.isEnabled()) {
			type.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#setType(Enumerator newValue)
	 * 
	 */
	public void setType(Enumerator newValue) {
		type.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseEquivalenceMap.Properties.type);
		if (eefElementEditorReadOnlyState && type.isEnabled()) {
			type.setEnabled(false);
			type.setToolTipText(BaselineMessages.BaseEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !type.isEnabled()) {
			type.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#getJustification()
	 * 
	 */
	public String getJustification() {
		return justification.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#setJustification(String newValue)
	 * 
	 */
	public void setJustification(String newValue) {
		if (newValue != null) {
			justification.setText(newValue);
		} else {
			justification.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseEquivalenceMap.Properties.justification);
		if (eefElementEditorReadOnlyState && justification.isEnabled()) {
			justification.setEnabled(false);
			justification.setBackground(justification.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			justification.setToolTipText(BaselineMessages.BaseEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !justification.isEnabled()) {
			justification.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#initTarget(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initTarget(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		target.setContentProvider(contentProvider);
		target.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseEquivalenceMap.Properties.target);
		if (eefElementEditorReadOnlyState && target.getTable().isEnabled()) {
			target.setEnabled(false);
			target.setToolTipText(BaselineMessages.BaseEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !target.getTable().isEnabled()) {
			target.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#updateTarget()
	 * 
	 */
	public void updateTarget() {
	target.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#addFilterTarget(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTarget(ViewerFilter filter) {
		targetFilters.add(filter);
		//Start IRR
		if (this.current.eContainer() instanceof RefArtefact ) {
			targetFilters.add(new EObjectFilter(RefframeworkPackage.Literals.REF_ARTEFACT));
		} else if (this.current.eContainer() instanceof RefActivity) {
			targetFilters.add(new EObjectFilter(RefframeworkPackage.Literals.REF_ACTIVITY));
		}else if (this.current.eContainer() instanceof RefRequirement) {
			targetFilters.add(new EObjectFilter(RefframeworkPackage.Literals.REF_REQUIREMENT));
		}else if (this.current.eContainer() instanceof RefRole) {
			targetFilters.add(new EObjectFilter(RefframeworkPackage.Literals.REF_ROLE));
		}else if (this.current.eContainer() instanceof RefTechnique) {
			targetFilters.add(new EObjectFilter(RefframeworkPackage.Literals.REF_TECHNIQUE));
		}	
		//End IRR
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#addBusinessFilterTarget(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTarget(ViewerFilter filter) {
		targetBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseEquivalenceMapPropertiesEditionPart#isContainedInTargetTable(EObject element)
	 * 
	 */
	public boolean isContainedInTargetTable(EObject element) {
		return ((ReferencesTableSettings)target.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
