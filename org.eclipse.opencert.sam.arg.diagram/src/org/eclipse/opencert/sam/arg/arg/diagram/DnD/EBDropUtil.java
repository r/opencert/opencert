/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.DnD;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;

import org.eclipse.opencert.sam.arg.arg.ArgumentReasoning;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;
import org.eclipse.opencert.sam.arg.arg.impl.ArgumentReasoningImpl;
import org.eclipse.opencert.sam.arg.arg.impl.AgreementImpl;
import org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl;
import org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl;
import org.eclipse.opencert.sam.arg.arg.impl.AssertedEvidenceImpl;
import org.eclipse.opencert.sam.arg.arg.impl.AssertedInferenceImpl;
import org.eclipse.opencert.sam.arg.arg.impl.ChoiceImpl;
import org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementCitationImpl;
import org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl;
import org.eclipse.opencert.sam.arg.arg.impl.InformationElementCitationImpl;

/**
 * 
 * @author M� Carmen Palacios
 */
public class EBDropUtil {
	public static IElementType getElementType(EObject element)
	{
		IElementType res=null;

		if(element instanceof Argumentation)							//Nodes Elements
			res = ArgElementTypes.Argumentation_2004;
		else if(element instanceof InformationElementCitation)
			res = ArgElementTypes.InformationElementCitation_2005;
		else if(element instanceof ArgumentElementCitation)
			res = ArgElementTypes.ArgumentElementCitation_2006;
		else if(element instanceof ArgumentReasoning)
			res = ArgElementTypes.ArgumentReasoning_2003;
		else if(element instanceof Claim)
			res = ArgElementTypes.Claim_2001;
		else if(element instanceof Agreement)
			res = ArgElementTypes.Agreement_2002;
		else if(element instanceof ChoiceImpl)
			res = ArgElementTypes.Choice_2007;
		else if(element instanceof AssertedInferenceImpl)       		//Edges Elements
			res = ArgElementTypes.AssertedInference_4001;		
		else if(element instanceof AssertedEvidenceImpl)
			res = ArgElementTypes.AssertedEvidence_4002;		
		else if(element instanceof AssertedContextImpl)
			res = ArgElementTypes.AssertedContext_4003;
		else
			System.out.println("EBDropUtil.getElementType(): Error unkonwn Element Type");
		
		return res;
	}
	
	public static String getEObjectSemanticHint(EObject element)
	{
		String res="";
	    if(element instanceof ArgumentationImpl)						//Nodes Elements
		    res = ((IHintedType)ArgElementTypes.Argumentation_2004).getSemanticHint();
		else if(element instanceof InformationElementCitationImpl)
		    res = ((IHintedType)ArgElementTypes.InformationElementCitation_2005).getSemanticHint();
		else if(element instanceof ArgumentElementCitationImpl)
		    res = ((IHintedType)ArgElementTypes.ArgumentElementCitation_2006).getSemanticHint();			
		else if(element instanceof ArgumentReasoningImpl)
		    res = ((IHintedType)ArgElementTypes.ArgumentReasoning_2003).getSemanticHint();
		else if(element instanceof ClaimImpl)
		    res = ((IHintedType)ArgElementTypes.Claim_2001).getSemanticHint();
		else if(element instanceof AgreementImpl)
		    res = ((IHintedType)ArgElementTypes.Agreement_2002).getSemanticHint();
		else if(element instanceof ChoiceImpl)
			res = ((IHintedType)ArgElementTypes.Choice_2007).getSemanticHint();
		else if(element instanceof AssertedInferenceImpl)       		//Edges Elements
		    res = ((IHintedType)ArgElementTypes.AssertedInference_4001).getSemanticHint();
		else if(element instanceof AssertedEvidenceImpl)
		    res = ((IHintedType)ArgElementTypes.AssertedEvidence_4002).getSemanticHint();
		else if(element instanceof AssertedContextImpl)
		    res = ((IHintedType)ArgElementTypes.AssertedContext_4003).getSemanticHint();

		else
			System.out.println("EBDropUtil.getEObjectSemanticHint(): Error unkonwn Element Type");
	    
	    return res;
	}


}
