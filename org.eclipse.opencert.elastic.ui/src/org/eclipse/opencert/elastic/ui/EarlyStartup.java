/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.ui;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.opencert.elastic.ui.feedback.AnyURLDropHandler;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.URLTransfer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

/**
 * Handles startup code for this bundle.
 */
public class EarlyStartup implements IStartup {

	/**
	 * Just the startup code that ensures the global drop handler is in place. We simply schedule a UI job that does
	 * the necessary registrations.
	 */
	@Override
	public void earlyStartup() {

		UIJob job = new UIJob("Register Drop Handler") { //$NON-NLS-1$

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				Shell shell = window.getShell();

				// register our own drop handler so we can observe URL drops from outside
				DropTarget dropTarget = new DropTarget(shell, DND.DROP_DEFAULT | DND.DROP_LINK);
				dropTarget.setTransfer(new Transfer[] { URLTransfer.getInstance() });
				dropTarget.addDropListener(new AnyURLDropHandler());

				//
				window.getActivePage().addPartListener(new ElasticPartObserver());

				return Status.OK_STATUS;
			}

		};
		job.schedule();

	}
}
