/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.InformationElementType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Information Element Citation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.InformationElementCitationImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.InformationElementCitationImpl#getToBeInstantiated <em>To Be Instantiated</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.InformationElementCitationImpl#getUrl <em>Url</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.InformationElementCitationImpl#getArtefact <em>Artefact</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InformationElementCitationImpl extends ArgumentElementImpl implements InformationElementCitation {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final InformationElementType TYPE_EDEFAULT = InformationElementType.JUSTIFICATION;

	/**
	 * The default value of the '{@link #getToBeInstantiated() <em>To Be Instantiated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToBeInstantiated()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean TO_BE_INSTANTIATED_EDEFAULT = Boolean.FALSE;

	/**
	 * The default value of the '{@link #getUrl() <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrl()
	 * @generated
	 * @ordered
	 */
	protected static final String URL_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InformationElementCitationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.INFORMATION_ELEMENT_CITATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InformationElementType getType() {
		return (InformationElementType)eDynamicGet(ArgPackage.INFORMATION_ELEMENT_CITATION__TYPE, ArgPackage.Literals.INFORMATION_ELEMENT_CITATION__TYPE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(InformationElementType newType) {
		eDynamicSet(ArgPackage.INFORMATION_ELEMENT_CITATION__TYPE, ArgPackage.Literals.INFORMATION_ELEMENT_CITATION__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getToBeInstantiated() {
		return (Boolean)eDynamicGet(ArgPackage.INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED, ArgPackage.Literals.INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToBeInstantiated(Boolean newToBeInstantiated) {
		eDynamicSet(ArgPackage.INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED, ArgPackage.Literals.INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED, newToBeInstantiated);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUrl() {
		return (String)eDynamicGet(ArgPackage.INFORMATION_ELEMENT_CITATION__URL, ArgPackage.Literals.INFORMATION_ELEMENT_CITATION__URL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUrl(String newUrl) {
		eDynamicSet(ArgPackage.INFORMATION_ELEMENT_CITATION__URL, ArgPackage.Literals.INFORMATION_ELEMENT_CITATION__URL, newUrl);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Artefact> getArtefact() {
		return (EList<Artefact>)eDynamicGet(ArgPackage.INFORMATION_ELEMENT_CITATION__ARTEFACT, ArgPackage.Literals.INFORMATION_ELEMENT_CITATION__ARTEFACT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.INFORMATION_ELEMENT_CITATION__TYPE:
				return getType();
			case ArgPackage.INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED:
				return getToBeInstantiated();
			case ArgPackage.INFORMATION_ELEMENT_CITATION__URL:
				return getUrl();
			case ArgPackage.INFORMATION_ELEMENT_CITATION__ARTEFACT:
				return getArtefact();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.INFORMATION_ELEMENT_CITATION__TYPE:
				setType((InformationElementType)newValue);
				return;
			case ArgPackage.INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED:
				setToBeInstantiated((Boolean)newValue);
				return;
			case ArgPackage.INFORMATION_ELEMENT_CITATION__URL:
				setUrl((String)newValue);
				return;
			case ArgPackage.INFORMATION_ELEMENT_CITATION__ARTEFACT:
				getArtefact().clear();
				getArtefact().addAll((Collection<? extends Artefact>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.INFORMATION_ELEMENT_CITATION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case ArgPackage.INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED:
				setToBeInstantiated(TO_BE_INSTANTIATED_EDEFAULT);
				return;
			case ArgPackage.INFORMATION_ELEMENT_CITATION__URL:
				setUrl(URL_EDEFAULT);
				return;
			case ArgPackage.INFORMATION_ELEMENT_CITATION__ARTEFACT:
				getArtefact().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.INFORMATION_ELEMENT_CITATION__TYPE:
				return getType() != TYPE_EDEFAULT;
			case ArgPackage.INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED:
				return TO_BE_INSTANTIATED_EDEFAULT == null ? getToBeInstantiated() != null : !TO_BE_INSTANTIATED_EDEFAULT.equals(getToBeInstantiated());
			case ArgPackage.INFORMATION_ELEMENT_CITATION__URL:
				return URL_EDEFAULT == null ? getUrl() != null : !URL_EDEFAULT.equals(getUrl());
			case ArgPackage.INFORMATION_ELEMENT_CITATION__ARTEFACT:
				return !getArtefact().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InformationElementCitationImpl
