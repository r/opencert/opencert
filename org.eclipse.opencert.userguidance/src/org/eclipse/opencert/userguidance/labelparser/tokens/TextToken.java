/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.labelparser.tokens;

public class TextToken implements Token {
	private String representation;
	private int startIndex;

	public TextToken(String representation, int startIndex) {
		this.representation = representation;
		this.startIndex = startIndex;
	}

	@Override
	public String getRepresentation() {
		return representation;
	}

	@Override
	public int getStartIndex() {
		return startIndex;
	}
}
