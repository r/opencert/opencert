/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.util.List;

import org.eclipse.opencert.webapp.reports.containers.ComplianceAsset;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetProperty;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetResource;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetails;
import org.eclipse.opencert.webapp.reports.listeners.ComplianceDetailsStatusListener;
import org.eclipse.opencert.webapp.reports.util.ComplianceAssetType;
import org.eclipse.opencert.webapp.reports.util.ComplianceElementType;
import org.eclipse.opencert.webapp.reports.util.HtmlProcessor;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

public class ComplianceItemDetailsPanel extends Panel implements ComplianceDetailsStatusListener {
	private static final long serialVersionUID = 473729143471896613L;
	
	private VerticalLayout descriptionTab; 
    private VerticalLayout propertiesTab;
    
    private VerticalLayout descriptionLayout = new VerticalLayout();
    private VerticalLayout propertiesLayout = new VerticalLayout();
    
    private static final String ITEM_LABEL_STYLE = "description";
    private static final String LAYOUT_STYLE = "complianceDetailsItemLayout";
	
	public ComplianceItemDetailsPanel() {
		TabSheet tabsheet = new TabSheet();
        
        descriptionTab = new VerticalLayout();
        tabsheet.addTab(descriptionTab, "Description");
                
        propertiesTab = new VerticalLayout();
        tabsheet.addTab(propertiesTab, "Properties");
        
        createDescriptionLayout();
        createPropertiesLayout();
        
        setContent(tabsheet);
	}

	@Override
	public void baselineElementSelected(long cdoId, String baselineElementType, ComplianceDetails complianceDetails) {
		if (ComplianceElementType.JUSTIFICATION.equals(baselineElementType)) {
			generateComplianceMapDescription(complianceDetails);
            generateComplianceMapProperties(complianceDetails);
		}
		
		if (ComplianceElementType.ARTEFACT.equals(baselineElementType) || 
				ComplianceElementType.ACTIVITY.equals(baselineElementType)) {
			List<ComplianceAsset> complianceAssets = (List<ComplianceAsset>) complianceDetails.getComplianceAssets();
			if (complianceAssets != null) {
				for (ComplianceAsset complianceAsset : complianceAssets) {
					if (cdoId == complianceAsset.getCdoId()) {
						generateComplianceAssetDescription(complianceAsset);
						generateComplianceAssetProperties(complianceAsset);
						break;
					}
				}
			}
		}
		
		if (ComplianceElementType.RESOURCE.equals(baselineElementType)) {
			List<ComplianceAsset> complianceAssets = (List<ComplianceAsset>) complianceDetails.getComplianceAssets();
			if (complianceAssets != null) {
				for (ComplianceAsset complianceAsset : complianceAssets) {
					List<ComplianceAssetResource> complianceAssetResources = (List<ComplianceAssetResource>) complianceAsset.getComplianceAssetResources();
					if (complianceAssetResources != null) {
						for (ComplianceAssetResource complianceAssetResource : complianceAssetResources) {
							long id = complianceAssetResource.getCdoId();
							if (cdoId == id) {
								generateResourceDescription(complianceAssetResource);
								generateResourceProperties(complianceAssetResource);
								break;
							}
						}
					}
				}
			}
		}
	}
	
    @Override
    public void baselineElementUnselected() 
    {
    	descriptionLayout.setVisible(false);
    	propertiesLayout.setVisible(false);
    }
	
    private void generateComplianceMapDescription(ComplianceDetails complianceDetails)
    { 
        descriptionLayout.setVisible(true);
        descriptionLayout.removeAllComponents();
        final String complianceMapJustification = complianceDetails.getComplianceMapJustification();
        String complianceJustification = "<b>Compliance Justification: </b>" + (complianceMapJustification != null ? 
        		complianceMapJustification : "");

        descriptionLayout.addComponent(createLabel(complianceJustification));   
    }
    
    private void generateComplianceAssetDescription(ComplianceAsset complianceAsset)
    { 
        descriptionLayout.setVisible(true);
        descriptionLayout.removeAllComponents();
        String assetType = complianceAsset.getType();
        String description = "<b>" + assetType + " Description:</b> " + (complianceAsset.getDescription() != null ? complianceAsset.getDescription() : "");
        descriptionLayout.addComponent(createLabel(description));   
    }
    
    private void generateResourceDescription(ComplianceAssetResource complianceAssetResource)
    { 
        descriptionLayout.setVisible(true);
        descriptionLayout.removeAllComponents();
        descriptionLayout.addComponent(createLabel("<b>Resource File: </b>" + complianceAssetResource.getLocation()));
        descriptionLayout.addComponent(createLabel("<b>SVN path: </b>" + complianceAssetResource.getRepoUrl()));
    }
    
    private void generateComplianceMapProperties(ComplianceDetails complianceDetails)
    { 
        propertiesLayout.setVisible(true);
        propertiesLayout.removeAllComponents();
        
        String id = "<b>Compliance Id:</b> " + 
        		(complianceDetails.getComplianceMapId() != null ? complianceDetails.getComplianceMapId() : "");
        String name = "<b>Compliance Name:</b> " + 
        		(complianceDetails.getComplianceMapName() != null ? complianceDetails.getComplianceMapName() : "");
        String type = "<b>Compliance Type:</b> " + 
        		(complianceDetails.getComplianceMapType() != null ? complianceDetails.getComplianceMapType() : "");
        
        propertiesLayout.addComponent(createLabel(id));
        propertiesLayout.addComponent(createLabel(name));
        propertiesLayout.addComponent(createLabel(type));
    }
    
    private void generateComplianceAssetProperties(ComplianceAsset complianceAsset)
    { 
        propertiesLayout.setVisible(true);
        propertiesLayout.removeAllComponents();
        
        String id = "<b>" + complianceAsset.getType() + " Id:</b> " + (complianceAsset.getId() != null ? complianceAsset.getId() : "");
        String name = "<b>" + complianceAsset.getType() + " Name:</b> " + (complianceAsset.getName() != null ? complianceAsset.getName() : "");
        String properties = "";
        
        if (ComplianceAssetType.ARTEFACT.equals(complianceAsset.getType())) {
	        properties = "<b>" + complianceAsset.getType() + " Properties: </b>";
	        List<ComplianceAssetProperty> complianceAssetProperties = (List<ComplianceAssetProperty>) complianceAsset.getComplianceAssetProperties();
	        if (complianceAssetProperties != null) {
		        for (ComplianceAssetProperty complianceAssetProperty : complianceAssetProperties) {
		        	properties += "<br>&nbsp;&nbsp;&nbsp;<b><i>name: </i></b>" + complianceAssetProperty.getPropertyName() + "; ";
		        	properties += "<b><i>value: </i></b>" + complianceAssetProperty.getPropertyValue();
		        }
	        }
        }
        
        propertiesLayout.addComponent(createLabel(id));
        propertiesLayout.addComponent(createLabel(name));
        propertiesLayout.addComponent(createLabel(properties));
    }
    
    private void generateResourceProperties(ComplianceAssetResource complianceAssetResource)
    { 
        propertiesLayout.setVisible(true);
        propertiesLayout.removeAllComponents();
        
        String id = "<b>Resource Id:</b> " + (complianceAssetResource.getId() != null ? complianceAssetResource.getId() : "");
        String name = "<b>Resource Name:</b> " + (complianceAssetResource.getName() != null ? complianceAssetResource.getName() : "");
        String description = "<b>Resource Description:</b> " + (complianceAssetResource.getDescription() != null ? complianceAssetResource.getDescription() : "");
        String format = "<b>Resource Format:</b> " + (complianceAssetResource.getFormat() != null ? complianceAssetResource.getFormat() : "");
        
        propertiesLayout.addComponent(createLabel(id));
        propertiesLayout.addComponent(createLabel(name));
        propertiesLayout.addComponent(createLabel(description));
        propertiesLayout.addComponent(createLabel(format));
    }
    
	private Label createLabel(String text) 
	{
		Label label = new Label("", ContentMode.HTML);
		label.setStyleName(ITEM_LABEL_STYLE);
		text = HtmlProcessor.processHtmlLinks(text);
		label.setValue(text);
		
		return label;
	}
    
    private void createDescriptionLayout()
    {
    	descriptionLayout.setWidth("98%");
    	descriptionLayout.setStyleName(LAYOUT_STYLE);
        descriptionTab.addComponent(descriptionLayout);
    }
    
    private void createPropertiesLayout()
    {
    	propertiesLayout.setWidth("98%");
    	propertiesLayout.setStyleName(LAYOUT_STYLE);
        propertiesTab.addComponent(propertiesLayout);
    }
}
