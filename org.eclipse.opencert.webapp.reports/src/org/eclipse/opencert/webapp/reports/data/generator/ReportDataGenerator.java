/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.data.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.util.OPException;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.storage.cdo.StandaloneCDOAccessor;
import org.eclipse.opencert.storage.cdo.property.OpencertPropertiesReader;
import org.eclipse.opencert.storage.cdo.test.DataCreationRoutines;

public class ReportDataGenerator
{
    private AssuranceProjectWrapper assuranceProjectWrapper;
        
    private String baseFrameworkName;
    
    private int numberOfBaseArtefacts;
    
    private int numberOfBaseActivities;
    
    private int rangeOfRandomEObjects;
    
    private List<AssuranceAsset> artefactList = new ArrayList<AssuranceAsset>();
    
    private List<AssuranceAsset> activitiesList = new ArrayList<AssuranceAsset>();
    
    private static final String BASE_ARTEFACT = "Base Artefact ";
    
    private static final String BASE_ACTIVITY = "Base Activity ";
    
    private static final String SUB_ACTIVITY = "Sub Activity ";
    
    private static final int SUB_ACTIVITY_DEPTH = 3;
    
    private CDOTransaction transaction;
         
    public ReportDataGenerator(AssuranceProjectWrapper assuranceProjectWrapper, String baseFrameworkName, int numberOfBaseArtefacts, int numberOfBaseActivities, int rangeOfRandomEObjects) 
    {
        this.assuranceProjectWrapper = assuranceProjectWrapper;
        this.baseFrameworkName = baseFrameworkName;
        this.numberOfBaseArtefacts = numberOfBaseArtefacts;
        this.numberOfBaseActivities = numberOfBaseActivities;
        this.rangeOfRandomEObjects = rangeOfRandomEObjects;
    }
        
    public void populateData() throws OPException 
    {
        StandaloneCDOAccessor standaloneCDOAccessor = StandaloneCDOAccessor.getStandaloneCDOAccessor(OpencertPropertiesReader.getInstance());
        CDOSession cdoSession = null;
        try {
            cdoSession = standaloneCDOAccessor.openSession();
            transaction = cdoSession.openTransaction();
            Resource resource = StandaloneCDOAccessor.getDefaultResource(transaction);
            
            populateBaseArtefactsAndBaseActivities(resource);
            
            transaction.setCommitComment("populateData");
            transaction.commit();           
            transaction.getSession().close();
 
        } catch (CommitException e) {
            e.printStackTrace();
        } finally {
            cdoSession.close();
        }
    }
    
    private void populateBaseArtefactsAndBaseActivities(Resource resource) throws OPException
    {   
        final BaselineConfig baselineConfig = readActiveBaselineConfig();
        if (baselineConfig == null) {
            throw new OPException("There is no active baseline configuration in this project. Data cannot be generated.");
        }
            
        //creating Artefacts & Activities (if needed)
        createEObjectsForBaseElements(resource);
        
        BaseFramework baseFramework = createBaselineFramework(resource);
        
        createBaseArtefacts(resource, baseFramework);

        createBaseActivities(resource, baseFramework);
        
        addBaselineFrameworkToBaselineConfig(resource, baseFramework, baselineConfig); 
    }
    
    private BaselineConfig readActiveBaselineConfig()
    {
        CDOObject aProject = (CDOObject) transaction.getObject(CDOUtil.getCDOObject(assuranceProjectWrapper.getAssuranceProject()));
        
        for (BaselineConfig bConfig : ((AssuranceProject)aProject).getBaselineConfig()) 
        {
            if (bConfig.isIsActive()) {
                return bConfig;
            }
        }
        return null;
    }

    private BaseFramework createBaselineFramework(Resource resource) 
    {
        BaseFramework baselineFramework = BaselineFactory.eINSTANCE.createBaseFramework();
        baselineFramework.setId(baseFrameworkName);
        baselineFramework.setDescription(baseFrameworkName);
        baselineFramework.setName(baseFrameworkName); 
        return baselineFramework;
    }
    
    private void createBaseArtefacts(Resource resource, BaseFramework baselineFramework) 
    {
        List<BaseArtefact> baseArtefactList = new ArrayList<BaseArtefact>();     
        for (int i = 0; i < numberOfBaseArtefacts; ++i) {
            BaseArtefact baseArtefact = BaselineFactory.eINSTANCE.createBaseArtefact();
            baseArtefact.setId(BASE_ARTEFACT + i);
            baseArtefact.setDescription(BASE_ARTEFACT + i);
            baseArtefact.setName(BASE_ARTEFACT + i);
            baseArtefact.setIsSelected(true);
            
            createBaseComplianceMap(resource, baseArtefact, MapKind.FULL, artefactList);
            createBaseComplianceMap(resource, baseArtefact, MapKind.PARTIAL, artefactList);
            
            baseArtefactList.add(baseArtefact);
        }
        resource.getContents().addAll(baseArtefactList);
        baselineFramework.getOwnedArtefact().addAll(baseArtefactList);
    }
    
    private void createBaseActivities(Resource resource, BaseFramework baselineFramework) 
    {
        List<BaseActivity> baseActivityList = new ArrayList<BaseActivity>();
        for (int i = 0; i < numberOfBaseActivities; ++i) {
            BaseActivity baseActivity = generateBaseActivity(resource, BASE_ACTIVITY, i);
            
            createSubActivities(baseActivity, resource, SUB_ACTIVITY_DEPTH);
 
            baseActivityList.add(baseActivity);          
        }           
        resource.getContents().addAll(baseActivityList);
        baselineFramework.getOwnedActivities().addAll(baseActivityList);
    }
    
    private void createSubActivities(BaseActivity baseActivity, Resource resource, int depth)
    {      
        BaseActivity subActivity = null;
        for (int i=0; i<depth; ++i) {
            subActivity = generateBaseActivity(resource, SUB_ACTIVITY, i);
            
            resource.getContents().add(subActivity);
            
            baseActivity.getSubActivity().add(subActivity);
            
            if (depth > 0) {
                createSubActivities(subActivity, resource, --depth);
            }
        }    
    }

    private void addBaselineFrameworkToBaselineConfig(Resource resource, BaseFramework baseFramework, BaselineConfig activeConfig)
    {     
        activeConfig.getRefFramework().add(baseFramework);
        
        resource.getContents().add(baseFramework);    
        resource.setModified(true);
    }

    private void createBaseComplianceMap(Resource resource, BaseAssurableElement baselineElement, MapKind kind, List<AssuranceAsset> elementsList)
    {
        BaseComplianceMap baseComplianceMap = BaselineFactory.eINSTANCE.createBaseComplianceMap();
        baseComplianceMap.setType(kind);
        
        Random generator = new Random();
        
        int rand = rangeOfRandomEObjects > 0 ? generator.nextInt(rangeOfRandomEObjects) : 0;
                
        for (int i = 0; i < rand; ++i) {
            baseComplianceMap.getTarget().add(elementsList.get(i));
        }

        resource.getContents().add(baseComplianceMap);
        
        baselineElement.getComplianceMap().add(baseComplianceMap);    
    }

    private void createEObjectsForBaseElements(Resource resource)
    {
        int numberOfArtefacts;
        int numberOfActivities;
        numberOfArtefacts = rangeOfRandomEObjects;
        numberOfActivities = rangeOfRandomEObjects;
        
        for (EObject eObject : resource.getContents()) {
            if (eObject instanceof Artefact){
                --numberOfArtefacts;
                artefactList.add((Artefact)eObject);
            } else if (eObject instanceof Activity) {
                --numberOfActivities;
                activitiesList.add((Activity)eObject);
            }
        }
        
        for (int i = 0; i < numberOfArtefacts; ++i) {
            artefactList.add(DataCreationRoutines.createArtefact("artefact", i));
        }
        
        for (int i = 0; i < numberOfActivities; ++i) {
            activitiesList.add(DataCreationRoutines.createActivity("activity", i));
        }
        
        resource.getContents().addAll(artefactList); 
        resource.getContents().addAll(activitiesList);      
    }    
    
    private BaseActivity generateBaseActivity(Resource resource, String name, int number) {
        BaseActivity baseActivity = BaselineFactory.eINSTANCE.createBaseActivity();
        baseActivity.setId(name + number);
        baseActivity.setDescription(name + number);
        baseActivity.setName(name + number);
        baseActivity.setIsSelected(true);   
        
        createBaseComplianceMap(resource, baseActivity, MapKind.FULL, activitiesList);
        createBaseComplianceMap(resource, baseActivity, MapKind.PARTIAL, activitiesList);
        
        return baseActivity;
    }
    
}
