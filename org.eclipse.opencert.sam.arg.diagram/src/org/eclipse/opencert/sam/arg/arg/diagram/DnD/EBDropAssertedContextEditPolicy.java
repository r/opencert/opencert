/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.DnD;


import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.impl.ConnectorImpl;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.gmf.runtime.notation.impl.EdgeImpl;
import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.AssertedContext;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;
import org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl;



/**
 * Creates the models and views, based on the dropped template file
 * @author M� Carmen Palacios
 */
public class EBDropAssertedContextEditPolicy {
	private TransactionalEditingDomain editingDomain;
	private EObject source;
	private EObject target;
	private IGraphicalEditPart ihost;

	
	public EBDropAssertedContextEditPolicy(TransactionalEditingDomain ed, EObject sc, EObject tg) {
		editingDomain = ed;
		source = sc;
		target = tg;
	}

	//public AssertedContext createModel(ConnectorImpl obj) {
	public AssertedContext createModel(EdgeImpl obj) {
		try{
			  // crear modelo
			  AssertedContextImpl el = (AssertedContextImpl)obj.getElement();
			  AssertedContext newElement = ArgFactory.eINSTANCE.createAssertedContext();
			  newElement.eSet(ArgPackage.Literals.MODEL_ELEMENT__ID, el.getId());
			  newElement.eSet(ArgPackage.Literals.MODEL_ELEMENT__NAME, el.getName());
			  newElement.eSet(ArgPackage.Literals.ARGUMENTATION_ELEMENT__DESCRIPTION, el.getDescription());
			  newElement.eSet(ArgPackage.Literals.ARGUMENTATION_ELEMENT__CONTENT, el.getContent());
			  newElement.eSet(ArgPackage.Literals.ASSERTED_CONTEXT__MULTIEXTENSION, el.getMultiextension());
			  newElement.eSet(ArgPackage.Literals.ASSERTED_CONTEXT__CARDINALITY, el.getCardinality());
			  newElement.getSource().add((ArgumentationElement)source);
			  newElement.getTarget().add((ArgumentationElement) target);
			  
			  return newElement;

		}catch(Exception e){
			  e.printStackTrace();
		}
		
		return null;
	}
	
	/*
	public CreateViewRequest createViewRequest (DiagramEditor ed, DiagramImpl dg, AssertedContext el, Point location) {

		try{
			final DiagramEditor editor = ed;
			final DiagramImpl diagr = dg;
			final AssertedContext solvedBy = el;
			
			// crear la vista
			CreateViewRequest.ViewDescriptor viewDescriptor = new CreateViewRequest.ViewDescriptor(
						new EObjectAdapter(solvedBy), Edge.class, ((IHintedType)ArgElementTypes.AssertedContext_4003).getSemanticHint(), true,
						editor.getDiagramEditPart().getDiagramPreferencesHint());
	
			viewDescriptor.setPersisted(true);
			  
			CreateViewRequest createRequest = new CreateViewRequest(viewDescriptor);
			createRequest.setLocation(location);
	
			return createRequest;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
    */
	
}

