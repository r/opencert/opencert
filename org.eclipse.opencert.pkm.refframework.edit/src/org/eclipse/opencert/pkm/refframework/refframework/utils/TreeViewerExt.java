/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package  org.eclipse.opencert.pkm.refframework.refframework.utils;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;

public class TreeViewerExt extends TreeViewer {

	public TreeViewerExt(Composite parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	public TreeViewerExt(Tree tree) {
		super(tree);
		// TODO Auto-generated constructor stub
	}

	public TreeViewerExt(Composite parent, int style) {
		super(parent, style);
		// TODO Auto-generated constructor stub
	}

	
	
	
	
}
