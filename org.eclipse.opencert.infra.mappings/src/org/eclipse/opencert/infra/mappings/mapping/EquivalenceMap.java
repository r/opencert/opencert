/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.infra.mappings.mapping;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equivalence Map</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.opencert.infra.mappings.mapping.MappingPackage#getEquivalenceMap()
 * @model
 * @generated
 */
public interface EquivalenceMap extends Map {
} // EquivalenceMap
