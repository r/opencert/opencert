/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.infra.mappings.mapping.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.infra.mappings.mapping.Map;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.infra.mappings.mapping.MapModel;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Map Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapModelImpl#getMapGroupModel <em>Map Group Model</em>}</li>
 *   <li>{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapModelImpl#getMapModel <em>Map Model</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MapModelImpl extends DescribableElementImpl implements MapModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MapModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.MAP_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MapGroup> getMapGroupModel() {
		return (EList<MapGroup>)eDynamicGet(MappingPackage.MAP_MODEL__MAP_GROUP_MODEL, MappingPackage.Literals.MAP_MODEL__MAP_GROUP_MODEL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Map> getMapModel() {
		return (EList<Map>)eDynamicGet(MappingPackage.MAP_MODEL__MAP_MODEL, MappingPackage.Literals.MAP_MODEL__MAP_MODEL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MappingPackage.MAP_MODEL__MAP_GROUP_MODEL:
				return ((InternalEList<?>)getMapGroupModel()).basicRemove(otherEnd, msgs);
			case MappingPackage.MAP_MODEL__MAP_MODEL:
				return ((InternalEList<?>)getMapModel()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MappingPackage.MAP_MODEL__MAP_GROUP_MODEL:
				return getMapGroupModel();
			case MappingPackage.MAP_MODEL__MAP_MODEL:
				return getMapModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MappingPackage.MAP_MODEL__MAP_GROUP_MODEL:
				getMapGroupModel().clear();
				getMapGroupModel().addAll((Collection<? extends MapGroup>)newValue);
				return;
			case MappingPackage.MAP_MODEL__MAP_MODEL:
				getMapModel().clear();
				getMapModel().addAll((Collection<? extends Map>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MappingPackage.MAP_MODEL__MAP_GROUP_MODEL:
				getMapGroupModel().clear();
				return;
			case MappingPackage.MAP_MODEL__MAP_MODEL:
				getMapModel().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MappingPackage.MAP_MODEL__MAP_GROUP_MODEL:
				return !getMapGroupModel().isEmpty();
			case MappingPackage.MAP_MODEL__MAP_MODEL:
				return !getMapModel().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MapModelImpl
