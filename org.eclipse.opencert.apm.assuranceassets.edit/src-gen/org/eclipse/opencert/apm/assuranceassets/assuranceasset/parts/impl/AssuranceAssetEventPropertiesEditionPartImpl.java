/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.impl;

// Start of user code for imports
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.EEFRuntimePlugin;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceassetViewsRepository;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.providers.AssuranceassetMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

// End of user code

/**
 * 
 * 
 */
public class AssuranceAssetEventPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, AssuranceAssetEventPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected EObjectFlatComboViewer resultingEvaluation;
	protected EMFComboViewer type;
	protected Text time;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public AssuranceAssetEventPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence assuranceAssetEventStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = assuranceAssetEventStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.class);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.id);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.name);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.description);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.resultingEvaluation);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.type);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.time);
		
		
		composer = new PartComposer(assuranceAssetEventStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.id) {
					return createIdText(parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.name) {
					return createNameText(parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.resultingEvaluation) {
					return createResultingEvaluationFlatComboViewer(parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.type) {
					return createTypeEMFComboViewer(parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.time) {
					return createTimeText(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(AssuranceassetMessages.AssuranceAssetEventPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.id, AssuranceassetMessages.AssuranceAssetEventPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEventPropertiesEditionPartImpl.this, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEventPropertiesEditionPartImpl.this, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.id, AssuranceassetViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.name, AssuranceassetMessages.AssuranceAssetEventPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEventPropertiesEditionPartImpl.this, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEventPropertiesEditionPartImpl.this, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.name, AssuranceassetViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.description, AssuranceassetMessages.AssuranceAssetEventPropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEventPropertiesEditionPartImpl.this, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEventPropertiesEditionPartImpl.this, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.description, AssuranceassetViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createResultingEvaluationFlatComboViewer(Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.resultingEvaluation, AssuranceassetMessages.AssuranceAssetEventPropertiesEditionPart_ResultingEvaluationLabel);
		resultingEvaluation = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.resultingEvaluation, AssuranceassetViewsRepository.SWT_KIND));
		resultingEvaluation.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		resultingEvaluation.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEventPropertiesEditionPartImpl.this, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.resultingEvaluation, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getResultingEvaluation()));
			}

		});
		GridData resultingEvaluationData = new GridData(GridData.FILL_HORIZONTAL);
		resultingEvaluation.setLayoutData(resultingEvaluationData);
		resultingEvaluation.setID(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.resultingEvaluation);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.resultingEvaluation, AssuranceassetViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createResultingEvaluationFlatComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createTypeEMFComboViewer(Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.type, AssuranceassetMessages.AssuranceAssetEventPropertiesEditionPart_TypeLabel);
		type = new EMFComboViewer(parent);
		type.setContentProvider(new ArrayContentProvider());
		type.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData typeData = new GridData(GridData.FILL_HORIZONTAL);
		type.getCombo().setLayoutData(typeData);
		type.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEventPropertiesEditionPartImpl.this, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.type, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getType()));
			}

		});
		type.setID(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.type);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.type, AssuranceassetViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createTypeEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createTimeText(Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.time, AssuranceassetMessages.AssuranceAssetEventPropertiesEditionPart_TimeLabel);
		time = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData timeData = new GridData(GridData.FILL_HORIZONTAL);
		time.setLayoutData(timeData);
		time.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEventPropertiesEditionPartImpl.this, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.time, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, time.getText()));
			}

		});
		time.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEventPropertiesEditionPartImpl.this, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.time, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, time.getText()));
				}
			}

		});
		EditingUtils.setID(time, AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.time);
		EditingUtils.setEEFtype(time, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.time, AssuranceassetViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createTimeText

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(AssuranceassetMessages.AssuranceAssetEvent_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(AssuranceassetMessages.AssuranceAssetEvent_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(AssuranceassetMessages.AssuranceAssetEvent_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#getResultingEvaluation()
	 * 
	 */
	public EObject getResultingEvaluation() {
		if (resultingEvaluation.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) resultingEvaluation.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#initResultingEvaluation(EObjectFlatComboSettings)
	 */
	public void initResultingEvaluation(EObjectFlatComboSettings settings) {
		resultingEvaluation.setInput(settings);
		if (current != null) {
			resultingEvaluation.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.resultingEvaluation);
		if (eefElementEditorReadOnlyState && resultingEvaluation.isEnabled()) {
			resultingEvaluation.setEnabled(false);
			resultingEvaluation.setToolTipText(AssuranceassetMessages.AssuranceAssetEvent_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !resultingEvaluation.isEnabled()) {
			resultingEvaluation.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#setResultingEvaluation(EObject newValue)
	 * 
	 */
	public void setResultingEvaluation(EObject newValue) {
		if (newValue != null) {
			resultingEvaluation.setSelection(new StructuredSelection(newValue));
		} else {
			resultingEvaluation.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.resultingEvaluation);
		if (eefElementEditorReadOnlyState && resultingEvaluation.isEnabled()) {
			resultingEvaluation.setEnabled(false);
			resultingEvaluation.setToolTipText(AssuranceassetMessages.AssuranceAssetEvent_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !resultingEvaluation.isEnabled()) {
			resultingEvaluation.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#setResultingEvaluationButtonMode(ButtonsModeEnum newValue)
	 */
	public void setResultingEvaluationButtonMode(ButtonsModeEnum newValue) {
		resultingEvaluation.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#addFilterResultingEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addFilterToResultingEvaluation(ViewerFilter filter) {
		resultingEvaluation.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#addBusinessFilterResultingEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToResultingEvaluation(ViewerFilter filter) {
		resultingEvaluation.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#getType()
	 * 
	 */
	public Enumerator getType() {
		Enumerator selection = (Enumerator) ((StructuredSelection) type.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#initType(Object input, Enumerator current)
	 */
	public void initType(Object input, Enumerator current) {
		type.setInput(input);
		type.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.type);
		if (eefElementEditorReadOnlyState && type.isEnabled()) {
			type.setEnabled(false);
			type.setToolTipText(AssuranceassetMessages.AssuranceAssetEvent_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !type.isEnabled()) {
			type.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#setType(Enumerator newValue)
	 * 
	 */
	public void setType(Enumerator newValue) {
		type.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.type);
		if (eefElementEditorReadOnlyState && type.isEnabled()) {
			type.setEnabled(false);
			type.setToolTipText(AssuranceassetMessages.AssuranceAssetEvent_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !type.isEnabled()) {
			type.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#getTime()
	 * 
	 */
	public String getTime() {
		return time.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEventPropertiesEditionPart#setTime(String newValue)
	 * 
	 */
	public void setTime(String newValue) {
		if (newValue != null) {
			time.setText(newValue);
		} else {
			time.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvent.Properties.time);
		if (eefElementEditorReadOnlyState && time.isEnabled()) {
			time.setEnabled(false);
			time.setToolTipText(AssuranceassetMessages.AssuranceAssetEvent_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !time.isEnabled()) {
			time.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceassetMessages.AssuranceAssetEvent_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
