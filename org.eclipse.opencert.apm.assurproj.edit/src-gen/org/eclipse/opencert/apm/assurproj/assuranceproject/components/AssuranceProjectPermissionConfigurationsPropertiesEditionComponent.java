/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.PermissionConfig;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart;


// End of user code

/**
 * 
 * 
 */
public class AssuranceProjectPermissionConfigurationsPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String PERMISSIONCONFIGURATIONS_PART = "PermissionConfigurations"; //$NON-NLS-1$

	
	/**
	 * Settings for permissionConf ReferencesTable
	 */
	protected ReferencesTableSettings permissionConfSettings;
	
	/**
	 * Settings for permissionConfTable ReferencesTable
	 */
	protected ReferencesTableSettings permissionConfTableSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public AssuranceProjectPermissionConfigurationsPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject assuranceProject, String editing_mode) {
		super(editingContext, assuranceProject, editing_mode);
		parts = new String[] { PERMISSIONCONFIGURATIONS_PART };
		repositoryKey = AssuranceprojectViewsRepository.class;
		partKey = AssuranceprojectViewsRepository.PermissionConfigurations.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final AssuranceProject assuranceProject = (AssuranceProject)elt;
			final PermissionConfigurationsPropertiesEditionPart permissionConfigurationsPart = (PermissionConfigurationsPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf)) {
				permissionConfSettings = new ReferencesTableSettings(assuranceProject, AssuranceprojectPackage.eINSTANCE.getAssuranceProject_PermissionConf());
				permissionConfigurationsPart.initPermissionConf(permissionConfSettings);
			}
			if (isAccessible(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable)) {
				permissionConfTableSettings = new ReferencesTableSettings(assuranceProject, AssuranceprojectPackage.eINSTANCE.getAssuranceProject_PermissionConf());
				permissionConfigurationsPart.initPermissionConfTable(permissionConfTableSettings);
			}
			// init filters
			if (isAccessible(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf)) {
				permissionConfigurationsPart.addFilterToPermissionConf(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof PermissionConfig); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for permissionConf
				// End of user code
			}
			if (isAccessible(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable)) {
				permissionConfigurationsPart.addFilterToPermissionConfTable(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof PermissionConfig); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for permissionConfTable
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}





	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf) {
			return AssuranceprojectPackage.eINSTANCE.getAssuranceProject_PermissionConf();
		}
		if (editorKey == AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable) {
			return AssuranceprojectPackage.eINSTANCE.getAssuranceProject_PermissionConf();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		AssuranceProject assuranceProject = (AssuranceProject)semanticObject;
		if (AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, permissionConfSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				permissionConfSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				permissionConfSettings.move(event.getNewIndex(), (PermissionConfig) event.getNewValue());
			}
		}
		if (AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, permissionConfTableSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				permissionConfTableSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				permissionConfTableSettings.move(event.getNewIndex(), (PermissionConfig) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			PermissionConfigurationsPropertiesEditionPart permissionConfigurationsPart = (PermissionConfigurationsPropertiesEditionPart)editingPart;
			if (AssuranceprojectPackage.eINSTANCE.getAssuranceProject_PermissionConf().equals(msg.getFeature()) && isAccessible(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf))
				permissionConfigurationsPart.updatePermissionConf();
			if (AssuranceprojectPackage.eINSTANCE.getAssuranceProject_PermissionConf().equals(msg.getFeature()) && isAccessible(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable))
				permissionConfigurationsPart.updatePermissionConfTable();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			AssuranceprojectPackage.eINSTANCE.getAssuranceProject_PermissionConf(),
			AssuranceprojectPackage.eINSTANCE.getAssuranceProject_PermissionConf()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
