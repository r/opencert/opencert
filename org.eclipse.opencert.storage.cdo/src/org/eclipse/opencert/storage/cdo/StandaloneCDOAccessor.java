/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo;

import org.eclipse.emf.cdo.net4j.CDONet4jSessionConfiguration;
import org.eclipse.emf.cdo.net4j.CDONet4jUtil;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.mapping.MappingPackage;
import org.eclipse.net4j.Net4jUtil;
import org.eclipse.net4j.connector.IConnector;
import org.eclipse.net4j.tcp.TCPUtil;
import org.eclipse.net4j.util.container.ContainerUtil;
import org.eclipse.net4j.util.container.IManagedContainer;
import org.eclipse.net4j.util.om.OMPlatform;
import org.eclipse.net4j.util.om.log.PrintLogHandler;
import org.eclipse.net4j.util.om.trace.PrintTraceHandler;
import org.eclipse.net4j.util.security.IPasswordCredentialsProvider;
import org.eclipse.net4j.util.security.PasswordCredentialsProvider;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.properties.property.PropertyPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.storage.cdo.property.IOpencertClientConfigurationAdapter;
import org.eclipse.opencert.vocabulary.VocabularyPackage;

public class StandaloneCDOAccessor
{
    private CDONet4jSessionConfiguration configuration;
    
    private static volatile StandaloneCDOAccessor standaloneCDOAccessor = null;
    
    
    public static StandaloneCDOAccessor getStandaloneCDOAccessor(IOpencertClientConfigurationAdapter clientConfigurationAdapter)
    {
        if (standaloneCDOAccessor == null) {
            synchronized (StandaloneCDOAccessor.class) {
                if (standaloneCDOAccessor == null) {
                    standaloneCDOAccessor = new StandaloneCDOAccessor(clientConfigurationAdapter);                    
                }
            }
        }
        
        return standaloneCDOAccessor;
    }
    

    private StandaloneCDOAccessor(IOpencertClientConfigurationAdapter clientConfigurationAdapter)
    {
        init(clientConfigurationAdapter);
    }
    
       
    private EPackage[] getEPackages()
    {
        return new EPackage[] {
            AssuranceassetPackage.eINSTANCE
            , AssuranceprojectPackage.eINSTANCE //new    
            , BaselinePackage.eINSTANCE//new
            , EvidencePackage.eINSTANCE
            , MappingPackage.eINSTANCE
            , PropertyPackage.eINSTANCE
            , ProcessPackage.eINSTANCE
            , RefframeworkPackage.eINSTANCE//new
            , ArgPackage.eINSTANCE //new
            , VocabularyPackage.eINSTANCE
            , GeneralPackage.eINSTANCE
        };
    }
    
        
    public CDOSession openSession()
    {
        CDOSession session = configuration.openNet4jSession();
        
        for (EPackage ePackage : getEPackages()) {
            session.getPackageRegistry().putEPackage(ePackage);
        }
        
        session.options().setGeneratedPackageEmulationEnabled(true); 
        return session;
    }
    
    
    public static Resource getDefaultResource(CDOTransaction transaction)
    {
        return transaction.getOrCreateResource("/opencert.evidence");
    }
    

    private void init(IOpencertClientConfigurationAdapter clientConfigurationAdapter)
    { 
        OMPlatform.INSTANCE.setDebugging(false);
        OMPlatform.INSTANCE.addLogHandler(PrintLogHandler.CONSOLE);
        OMPlatform.INSTANCE.addTraceHandler(PrintTraceHandler.CONSOLE);

        IManagedContainer container = ContainerUtil.createContainer();
        Net4jUtil.prepareContainer(container);
        TCPUtil.prepareContainer(container);
        CDONet4jUtil.prepareContainer(container);
        container.activate();

        IConnector connector = TCPUtil.getConnector(container, clientConfigurationAdapter.getCDOServerAddress());

        configuration = CDONet4jUtil.createNet4jSessionConfiguration();
        configuration.setConnector(connector);
        configuration.setRepositoryName("opencert");
             
        
        IPasswordCredentialsProvider credentialsProvider = new PasswordCredentialsProvider("Administrator", "0000");
        configuration.setCredentialsProvider(credentialsProvider);
        
        
    }
}
