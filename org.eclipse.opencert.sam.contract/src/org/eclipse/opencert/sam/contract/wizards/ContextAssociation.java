/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.wizards;

import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;

public class ContextAssociation {
	
	private ArgumentationElement source;
	private ArgumentationElement target;
	
	public ContextAssociation(ArgumentationElement source, ArgumentationElement target)
	{
		this.source = source;
		this.target = target;
	}

	public ArgumentationElement getSource() {
		return source;
	}

	public void setSource(ArgumentationElement source) {
		this.source = source;
	}

	public ArgumentationElement getTarget() {
		return target;
	}

	public void setTarget(ArgumentationElement target) {
		this.target = target;
	}

}
