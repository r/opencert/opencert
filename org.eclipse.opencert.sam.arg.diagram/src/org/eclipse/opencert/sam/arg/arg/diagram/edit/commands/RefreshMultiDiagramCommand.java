/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.arg.arg.diagram.edit.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.sam.arg.arg.Choice;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.ModelElement;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.Choice2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.CaseSemiCanonicalEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramUpdater;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgNodeDescriptor;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;
import org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl;


/**
 * The Class RefreshMultiDiagramCommand.
 * 
 * @author  M� Carmen Palacios
 */
/**
 * @generated NOT
 */
// Start MCP: several diagrams from one model
public class RefreshMultiDiagramCommand extends AbstractTransactionalCommand {
    	private List<EObject> listRootObjects;
        private View parentView;
        private EditPart host;

        /**
         * @generated NOT
         */
        public RefreshMultiDiagramCommand (
        				List<EObject> listObjects,
                        TransactionalEditingDomain editingDomain, View parentView,
                        EditPart host) {
                super(editingDomain, "Refresh element view", getWorkspaceFiles(parentView)); 
                this.listRootObjects = listObjects;
                this.parentView = parentView;
                this.host = host;
        }
        
        /**
         * @generated NOT
         */
        /* Dawn cdo. MCP: ASI NO SE CONSIGUE NADA!!! */
    	private void addSemanticChoiceChildrenList() {
    		
    		if (listRootObjects == null || listRootObjects.isEmpty()) return;
    		
    		List<EObject> c = new ArrayList<EObject>();
    		for(EObject element : listRootObjects)
    		{
        	    if(true == element instanceof ClaimImpl)
        	    {
        	    	ClaimImpl obj = (ClaimImpl)element;
        	    	if(obj.getChoice() != null)
        	    		c.add(obj.getChoice());
        	    }
     		}
    		listRootObjects.addAll(c);
    	}

        
        
        /**
         * @generated NOT
         */
        protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
                        IAdaptable info) throws ExecutionException {

	        	if (listRootObjects == null || listRootObjects.isEmpty()) return  CommandResult.newErrorCommandResult("Objetos no disponibles");
	        	
	        	List<EObject> sublist = listRootObjects;
	        	List<EObject> toRemove = new ArrayList<EObject>();
	        	for( EObject element : sublist)
	        	{
	        		// no se puede copiar desde Case o elementos de otros modelos
	        	    if(false == element instanceof ModelElement)
	        	    	toRemove.add(element);
	        	}
	        	listRootObjects.removeAll(toRemove);
	        	/* Dawn cdo
	        	 * MCP: ASI NO SE CONSIGUE NADA!!! 
	        	addSemanticChoiceChildrenList();
	        	*/
	        	
                /* MCP!!!
                List editPolicies = CanonicalEditPolicy
                        .getRegisteredEditPolicies(rootObject);
                for (Iterator it = editPolicies.iterator(); it.hasNext(); ) {
                        CanonicalEditPolicy nextEditPolicy = (CanonicalEditPolicy) it.next();
                        nextEditPolicy.refresh();
                }
                 */
                
                CaseSemiCanonicalEditPolicy kk = new CaseSemiCanonicalEditPolicy();
                kk.setObjects(listRootObjects);
                kk.setHost(this.host);
                kk.refresh();                
                kk.deactivate(); // para que no invoque refreshSemantic() al eliminar del diagrama uno de los objetos
                
                
                return CommandResult.newOKCommandResult();
        }
}
// End MCP
