/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.ui.DawnEditorInput;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.Window;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.providers.AssuranceprojectMessages;
import org.eclipse.opencert.apm.assurproj.assuranceproject.utils.CrossDomainUI;
import org.eclipse.opencert.apm.assurproj.assuranceproject.utils.CrossProjectUI;
import org.eclipse.opencert.apm.assurproj.assuranceproject.utils.ImportFromEpfUI;
import org.eclipse.opencert.apm.assurproj.assuranceproject.utils.ImportFromFileUI;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;


// End of user code

/**
 * 
 * 
 */
public class AssuranceProjectPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, AssuranceProjectPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text createdBy;
	protected Text responsible;
	protected Text date;
	protected Text version;
	protected ReferencesTable subProject;
	protected List<ViewerFilter> subProjectBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> subProjectFilters = new ArrayList<ViewerFilter>();


	// Start IRR
	public static String ID = "org.eclipse.opencert.apm.baseline.presentation.DawnBaselineEditorID";
	protected Button buttonCrossDomain;
	protected Button buttonCrossProject;
	// Start HEO
	protected Button buttonImportFromEpf;
	// End HEO
	//Start ARL HEO
	protected Button buttonImportFromFile;
	//End ARL HEO
	//protected Button buttonepf2arg;
	// End IRR
	
	

	/**
	 * For {@link ISection} use only.
	 */
	public AssuranceProjectPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public AssuranceProjectPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence assuranceProject_Step = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = assuranceProject_Step.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.class);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.id);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.name);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.description);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.date);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.version);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject);
		
		
		composer = new PartComposer(assuranceProject_Step) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.class) {
					
					// Start IRR
					createGroupButton(parent);
					// End IRR
					
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.description) {
					return createDescriptionText(widgetFactory, parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy) {
					return createCreatedByText(widgetFactory, parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible) {
					return createResponsibleText(widgetFactory, parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.date) {
					return createDateText(widgetFactory, parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.version) {
					return createVersionText(widgetFactory, parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject) {
					return createSubProjectReferencesTable(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.id, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceProjectPropertiesEditionPartForm.this,
							AssuranceprojectViewsRepository.AssuranceProject_.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									AssuranceprojectViewsRepository.AssuranceProject_.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, AssuranceprojectViewsRepository.AssuranceProject_.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.id, AssuranceprojectViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.name, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceProjectPropertiesEditionPartForm.this,
							AssuranceprojectViewsRepository.AssuranceProject_.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									AssuranceprojectViewsRepository.AssuranceProject_.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, AssuranceprojectViewsRepository.AssuranceProject_.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.name, AssuranceprojectViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.description, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_DescriptionLabel);
		description = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		description.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceProjectPropertiesEditionPartForm.this,
							AssuranceprojectViewsRepository.AssuranceProject_.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									AssuranceprojectViewsRepository.AssuranceProject_.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		description.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}
		});
		EditingUtils.setID(description, AssuranceprojectViewsRepository.AssuranceProject_.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.description, AssuranceprojectViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createCreatedByText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_CreatedByLabel);
		createdBy = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		createdBy.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData createdByData = new GridData(GridData.FILL_HORIZONTAL);
		createdBy.setLayoutData(createdByData);
		createdBy.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceProjectPropertiesEditionPartForm.this,
							AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, createdBy.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, createdBy.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		createdBy.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, createdBy.getText()));
				}
			}
		});
		EditingUtils.setID(createdBy, AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy);
		EditingUtils.setEEFtype(createdBy, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy, AssuranceprojectViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createCreatedByText

		// End of user code
		return parent;
	}

	
	protected Composite createResponsibleText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_ResponsibleLabel);
		responsible = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		responsible.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData responsibleData = new GridData(GridData.FILL_HORIZONTAL);
		responsible.setLayoutData(responsibleData);
		responsible.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceProjectPropertiesEditionPartForm.this,
							AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, responsible.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, responsible.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		responsible.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, responsible.getText()));
				}
			}
		});
		EditingUtils.setID(responsible, AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible);
		EditingUtils.setEEFtype(responsible, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible, AssuranceprojectViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createResponsibleText

		// End of user code
		return parent;
	}

	
	protected Composite createDateText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.date, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_DateLabel);
		date = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		date.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData dateData = new GridData(GridData.FILL_HORIZONTAL);
		date.setLayoutData(dateData);
		date.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceProjectPropertiesEditionPartForm.this,
							AssuranceprojectViewsRepository.AssuranceProject_.Properties.date,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, date.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									AssuranceprojectViewsRepository.AssuranceProject_.Properties.date,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, date.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		date.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.date, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, date.getText()));
				}
			}
		});
		EditingUtils.setID(date, AssuranceprojectViewsRepository.AssuranceProject_.Properties.date);
		EditingUtils.setEEFtype(date, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.date, AssuranceprojectViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDateText

		// End of user code
		return parent;
	}

	
	protected Composite createVersionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.version, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_VersionLabel);
		version = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		version.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData versionData = new GridData(GridData.FILL_HORIZONTAL);
		version.setLayoutData(versionData);
		version.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceProjectPropertiesEditionPartForm.this,
							AssuranceprojectViewsRepository.AssuranceProject_.Properties.version,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, version.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									AssuranceprojectViewsRepository.AssuranceProject_.Properties.version,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, version.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceProjectPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		version.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.version, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, version.getText()));
				}
			}
		});
		EditingUtils.setID(version, AssuranceprojectViewsRepository.AssuranceProject_.Properties.version);
		EditingUtils.setEEFtype(version, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.version, AssuranceprojectViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createVersionText

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createSubProjectReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.subProject = new ReferencesTable(getDescription(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_SubProjectLabel), new ReferencesTableListener	() {
			public void handleAdd() { addSubProject(); }
			public void handleEdit(EObject element) { editSubProject(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveSubProject(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromSubProject(element); }
			public void navigateTo(EObject element) { }
		});
		this.subProject.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject, AssuranceprojectViewsRepository.FORM_KIND));
		this.subProject.createControls(parent, widgetFactory);
		this.subProject.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData subProjectData = new GridData(GridData.FILL_HORIZONTAL);
		subProjectData.horizontalSpan = 3;
		this.subProject.setLayoutData(subProjectData);
		this.subProject.disableMove();
		subProject.setID(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject);
		subProject.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createSubProjectReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addSubProject() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(subProject.getInput(), subProjectFilters, subProjectBusinessFilters,
		"subProject", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				subProject.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveSubProject(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		subProject.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromSubProject(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		subProject.refresh();
	}

	/**
	 * 
	 */
	protected void editSubProject(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				subProject.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getCreatedBy()
	 * 
	 */
	public String getCreatedBy() {
		return createdBy.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setCreatedBy(String newValue)
	 * 
	 */
	public void setCreatedBy(String newValue) {
		if (newValue != null) {
			createdBy.setText(newValue);
		} else {
			createdBy.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy);
		if (eefElementEditorReadOnlyState && createdBy.isEnabled()) {
			createdBy.setEnabled(false);
			createdBy.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !createdBy.isEnabled()) {
			createdBy.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getResponsible()
	 * 
	 */
	public String getResponsible() {
		return responsible.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setResponsible(String newValue)
	 * 
	 */
	public void setResponsible(String newValue) {
		if (newValue != null) {
			responsible.setText(newValue);
		} else {
			responsible.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible);
		if (eefElementEditorReadOnlyState && responsible.isEnabled()) {
			responsible.setEnabled(false);
			responsible.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !responsible.isEnabled()) {
			responsible.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getDate()
	 * 
	 */
	public String getDate() {
		return date.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setDate(String newValue)
	 * 
	 */
	public void setDate(String newValue) {
		if (newValue != null) {
			date.setText(newValue);
		} else {
			date.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.date);
		if (eefElementEditorReadOnlyState && date.isEnabled()) {
			date.setEnabled(false);
			date.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !date.isEnabled()) {
			date.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getVersion()
	 * 
	 */
	public String getVersion() {
		return version.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setVersion(String newValue)
	 * 
	 */
	public void setVersion(String newValue) {
		if (newValue != null) {
			version.setText(newValue);
		} else {
			version.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.version);
		if (eefElementEditorReadOnlyState && version.isEnabled()) {
			version.setEnabled(false);
			version.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !version.isEnabled()) {
			version.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#initSubProject(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initSubProject(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		subProject.setContentProvider(contentProvider);
		subProject.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject);
		if (eefElementEditorReadOnlyState && subProject.getTable().isEnabled()) {
			subProject.setEnabled(false);
			subProject.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !subProject.getTable().isEnabled()) {
			subProject.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#updateSubProject()
	 * 
	 */
	public void updateSubProject() {
	subProject.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#addFilterSubProject(ViewerFilter filter)
	 * 
	 */
	public void addFilterToSubProject(ViewerFilter filter) {
		subProjectFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#addBusinessFilterSubProject(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToSubProject(ViewerFilter filter) {
		subProjectBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#isContainedInSubProjectTable(EObject element)
	 * 
	 */
	public boolean isContainedInSubProjectTable(EObject element) {
		return ((ReferencesTableSettings)subProject.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceprojectMessages.AssuranceProject_Part_Title;
	}

	// Start of user code additional methods
	
	// Start IRR
			protected void createGroupButton(final Composite parent) {
				Composite controlButtons = new Composite(parent, SWT.NONE);
				{
					GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
					data.horizontalAlignment = SWT.END;
					controlButtons.setLayoutData(data);
					GridLayout layout = new GridLayout();
					data.horizontalAlignment = SWT.FILL;
					layout.marginHeight = 0;
					layout.marginWidth = 0;
					layout.numColumns = 1;

					controlButtons.setLayout(layout);
				}

				Group group = new Group(controlButtons, SWT.NULL);
				group.setLayout(new GridLayout(3, false));
				group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
				group.setText("Import Data");

				IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				IWorkbenchPage page = workbenchWindow.getActivePage();
				IEditorPart editorPart = page.getActiveEditor();
				
				URI resourceURI = EditUIUtil.getURI(editorPart.getEditorInput());
				CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
				CDOTransaction transaction = sessionCDO.openTransaction();
				CDOResourceNode node=transaction.getResourceNode(resourceURI.path());
				node.cdoReload();
				CDOPermission permission = node.cdoRevision().getPermission();
				
				
				
				buttonCrossDomain = new Button(group, 0);
				GridData CMGridData = new GridData();
				CMGridData.horizontalAlignment = SWT.FILL;
				CMGridData.verticalAlignment = SWT.FILL;
				
				buttonCrossDomain.setEnabled(permission.isWritable());				
				
				buttonCrossDomain.setLayoutData(CMGridData);
				buttonCrossDomain.setText("Cross Standard");
				buttonCrossDomain.addSelectionListener(new SelectionAdapter() {

					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
					 */
					public void widgetSelected(SelectionEvent e) {

						// Pregunta antes de salvar si es que se han hecho cambios
						boolean pp = PlatformUI.getWorkbench().saveAllEditors(true);

						CrossDomainUI CrossDomainSet = new CrossDomainUI(parent.getShell());
						
						CrossDomainSet.open();
												
						
						/*if (CrossDomainSet.open() == Window.CANCEL) {
							
							// Reload editor to show the changes
							
							IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
							IWorkbenchPage page = workbenchWindow.getActivePage();
							
							IEditorPart editorPart = page.getActiveEditor();
							
							
							URI resourceURI = EditUIUtil.getURI(editorPart.getEditorInput());
							System.out.println(resourceURI.toString());
							
							page.closeEditor(editorPart, true);
							
							
							//Open editor
							DawnEditorInput dawnEditorInput = new DawnEditorInput(resourceURI);
							try {
								editorPart = page.openEditor(dawnEditorInput, ID);
							} catch (PartInitException exception) {
								MessageDialog.openError(workbenchWindow.getShell(),
										"Error to reopen the editor", exception.getMessage());
								throw new RuntimeException(exception);
							}  catch (Exception exception) {
								MessageDialog.openError(workbenchWindow.getShell(),
										"Error to reopen the editor", exception.getMessage());
								throw new RuntimeException(exception);
							}
						}*/
					}
				});
			
			// End IRR
				
			//begin ALC
				
							
			buttonCrossProject = new Button(group, 0);
			
			buttonCrossProject.setEnabled(permission.isWritable());
						
			buttonCrossProject.setLayoutData(CMGridData);
			buttonCrossProject.setText("Cross Project");
			buttonCrossProject.addSelectionListener(new SelectionAdapter() {

				/**
				 * {@inheritDoc}
				 * 
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				public void widgetSelected(SelectionEvent e) {

					// Pregunta antes de salvar si es que se han hecho cambios
					boolean pp = PlatformUI.getWorkbench().saveAllEditors(true);

					CrossProjectUI CrossProjectSet = new CrossProjectUI(parent.getShell());					
					
					CrossProjectSet.open();
																					
				}
			});
			
			//End ALC
			
			//begin HEO
			
							
			
			buttonImportFromEpf = new Button(group, 0);
			
			buttonImportFromEpf.setEnabled(permission.isWritable());		
			
			buttonImportFromEpf.setLayoutData(CMGridData);
			buttonImportFromEpf.setText("Import from EPF");
			buttonImportFromEpf.addSelectionListener(new SelectionAdapter() {

				/**
				 * {@inheritDoc}
				 * 
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				public void widgetSelected(SelectionEvent e) {

					// Pregunta antes de salvar si es que se han hecho cambios
					boolean pp = PlatformUI.getWorkbench().saveAllEditors(true);

					ImportFromEpfUI ImportFromEpfSet = new ImportFromEpfUI(parent.getShell());					
					
					ImportFromEpfSet.open();
																					
				}
			});
			
			//End HEO
			//Start ARL HEO
			buttonImportFromFile = new Button(group, 0);
			
			
			buttonImportFromFile.setEnabled(permission.isWritable());
			
			
			buttonImportFromFile.setLayoutData(CMGridData);
			buttonImportFromFile.setText("Import from File");
			buttonImportFromFile.addSelectionListener(new SelectionAdapter() {

				/**
				 * {@inheritDoc}
				 * 
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				public void widgetSelected(SelectionEvent e) {

					// Pregunta antes de salvar si es que se han hecho cambios
					boolean pp = PlatformUI.getWorkbench().saveAllEditors(true);

					ImportFromFileUI ImportFromFileSet = new ImportFromFileUI(parent.getShell());					
					
					ImportFromFileSet.open();
																					
				}
			});
			//End ARL HEO
			//Start EPF2ArgTransformation
			/*buttonepf2arg = new Button(group, 0);
			
			buttonepf2arg.setEnabled(permission.isWritable());
			
			buttonepf2arg.setLayoutData(CMGridData);
			buttonepf2arg.setText("EPF Composer -> Argumentation");
			buttonepf2arg.addSelectionListener(new SelectionAdapter() {

				/**
				 * {@inheritDoc}
				 * 
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				/*public void widgetSelected(SelectionEvent e) {

					// Pregunta antes de salvar si es que se han hecho cambios
					boolean pp = PlatformUI.getWorkbench().saveAllEditors(true);

					EPF2ARGUI EpfSet = new EPF2ARGUI(parent.getShell());
					
					EpfSet.open();
																					
				}
			});*/
			//End EPF2ArgTransformation
			
		}
	
	// End of user code


}
