/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.core.commands.operations.IOperationHistory;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.impl.TransactionalEditingDomainImpl;
import org.eclipse.emf.workspace.WorkspaceEditingDomainFactory;
import org.eclipse.emf.workspace.impl.WorkspaceCommandStackImpl;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.ui.IWorkbenchPart;


/**
 * An editing domain factory registered on the extension point to create our shared editing domain for EXTLibrary model editors.
 * 
 */
public class EditingDomainFactory extends WorkspaceEditingDomainFactory implements TransactionalEditingDomain.Factory {

	private static final Log logger = LogFactory.getLog(EditingDomainFactory.class);

	@Override
	public TransactionalEditingDomain createEditingDomain(IOperationHistory history) {
		WorkspaceCommandStackImpl stack = new WorkspaceCommandStackImpl(history);

		TransactionalEditingDomain domain = new TransactionalEditingDomainImpl(AdapterFactoryHandler.eINSTANCE.getAdapterFactory(), stack);

		// // register a default workspace synchronizer for the created editing domain
		// WorkspaceSynchronizer workspaceSynchronizer = new WorkspaceSynchronizer(domain);

		mapResourceSet(domain);

		return domain;
	}

	public static TransactionalEditingDomain createEditingDomain(IWorkbenchPart workbenchPart, IOperationHistory history) {
		return new EditingDomainFactory().createEditingDomain(history);
	}

	public static TransactionalEditingDomain createEditingDomain(IWizard wizard, IOperationHistory history) {
		return new EditingDomainFactory().createEditingDomain(history);
	}

	public static TransactionalEditingDomain createEditingDomain(IWorkbenchPart workbenchPart) {
		return new EditingDomainFactory().createEditingDomain();
	}

	public static TransactionalEditingDomain createEditingDomain(IWizard wizard) {
		return new EditingDomainFactory().createEditingDomain();
	}

	public static TransactionalEditingDomain createDefaultEditingDomain() {
		return new EditingDomainFactory().createEditingDomain();
	}

	public static Log getLogger() {
		return logger;
	}

}
