/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.impl;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.providers.EMFListContentProvider;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;

import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;

// End of user code

/**
 * 
 * 
 */
public class RefCriticalityApplicabilityPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, RefCriticalityApplicabilityPropertiesEditionPart {

	protected EObjectFlatComboViewer applicLevel;
	protected EMFComboViewer applicLevelCombo;
	protected EObjectFlatComboViewer criticLevel;
	protected EMFComboViewer criticLevelCombo;
	protected Text comment;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public RefCriticalityApplicabilityPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence refCriticalityApplicabilityStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = refCriticalityApplicabilityStep.addStep(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.class);
		// Start IRR
		//propertiesStep.addStep(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel);
		propertiesStep.addStep(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo);
		//propertiesStep.addStep(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel);
		// En d IRR
		propertiesStep.addStep(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo);
		propertiesStep.addStep(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment);
		
		
		composer = new PartComposer(refCriticalityApplicabilityStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel) {
					return createApplicLevelFlatComboViewer(parent);
				}
				if (key == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo) {
					return createApplicLevelComboEMFComboViewer(parent);
				}
				if (key == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel) {
					return createCriticLevelFlatComboViewer(parent);
				}
				if (key == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo) {
					return createCriticLevelComboEMFComboViewer(parent);
				}
				if (key == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment) {
					return createCommentText(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(RefframeworkMessages.RefCriticalityApplicabilityPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createApplicLevelFlatComboViewer(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel, RefframeworkMessages.RefCriticalityApplicabilityPropertiesEditionPart_ApplicLevelLabel);
		applicLevel = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel, RefframeworkViewsRepository.SWT_KIND));
		applicLevel.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		applicLevel.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefCriticalityApplicabilityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getApplicLevel()));
			}

		});
		GridData applicLevelData = new GridData(GridData.FILL_HORIZONTAL);
		applicLevel.setLayoutData(applicLevelData);
		applicLevel.setID(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createApplicLevelFlatComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createApplicLevelComboEMFComboViewer(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo, RefframeworkMessages.RefCriticalityApplicabilityPropertiesEditionPart_ApplicLevelComboLabel);
		applicLevelCombo = new EMFComboViewer(parent);
		GridData applicLevelComboData = new GridData(GridData.FILL_HORIZONTAL);
		applicLevelCombo.getCombo().setLayoutData(applicLevelComboData);
		applicLevelCombo.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		applicLevelCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefCriticalityApplicabilityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getApplicLevelCombo()));
			}

		});
		applicLevelCombo.setContentProvider(new EMFListContentProvider());
		EditingUtils.setID(applicLevelCombo.getCombo(), RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo);
		EditingUtils.setEEFtype(applicLevelCombo.getCombo(), "eef::Combo"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createApplicLevelComboEMFComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createCriticLevelFlatComboViewer(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel, RefframeworkMessages.RefCriticalityApplicabilityPropertiesEditionPart_CriticLevelLabel);
		criticLevel = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel, RefframeworkViewsRepository.SWT_KIND));
		criticLevel.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		criticLevel.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefCriticalityApplicabilityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getCriticLevel()));
			}

		});
		GridData criticLevelData = new GridData(GridData.FILL_HORIZONTAL);
		criticLevel.setLayoutData(criticLevelData);
		criticLevel.setID(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createCriticLevelFlatComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createCriticLevelComboEMFComboViewer(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo, RefframeworkMessages.RefCriticalityApplicabilityPropertiesEditionPart_CriticLevelComboLabel);
		criticLevelCombo = new EMFComboViewer(parent);
		GridData criticLevelComboData = new GridData(GridData.FILL_HORIZONTAL);
		criticLevelCombo.getCombo().setLayoutData(criticLevelComboData);
		criticLevelCombo.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		criticLevelCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefCriticalityApplicabilityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getCriticLevelCombo()));
			}

		});
		criticLevelCombo.setContentProvider(new EMFListContentProvider());
		EditingUtils.setID(criticLevelCombo.getCombo(), RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo);
		EditingUtils.setEEFtype(criticLevelCombo.getCombo(), "eef::Combo"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createCriticLevelComboEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createCommentText(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment, RefframeworkMessages.RefCriticalityApplicabilityPropertiesEditionPart_CommentLabel);
		comment = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData commentData = new GridData(GridData.FILL_HORIZONTAL);
		comment.setLayoutData(commentData);
		comment.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefCriticalityApplicabilityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, comment.getText()));
			}

		});
		comment.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefCriticalityApplicabilityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, comment.getText()));
				}
			}

		});
		EditingUtils.setID(comment, RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment);
		EditingUtils.setEEFtype(comment, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createCommentText

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#getApplicLevel()
	 * 
	 */
	public EObject getApplicLevel() {
		if (applicLevel.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) applicLevel.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#initApplicLevel(EObjectFlatComboSettings)
	 */
	public void initApplicLevel(EObjectFlatComboSettings settings) {
		applicLevel.setInput(settings);
		if (current != null) {
			applicLevel.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel);
		if (eefElementEditorReadOnlyState && applicLevel.isEnabled()) {
			applicLevel.setEnabled(false);
			applicLevel.setToolTipText(RefframeworkMessages.RefCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicLevel.isEnabled()) {
			applicLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#setApplicLevel(EObject newValue)
	 * 
	 */
	public void setApplicLevel(EObject newValue) {
		if (newValue != null) {
			applicLevel.setSelection(new StructuredSelection(newValue));
		} else {
			applicLevel.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel);
		if (eefElementEditorReadOnlyState && applicLevel.isEnabled()) {
			applicLevel.setEnabled(false);
			applicLevel.setToolTipText(RefframeworkMessages.RefCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicLevel.isEnabled()) {
			applicLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#setApplicLevelButtonMode(ButtonsModeEnum newValue)
	 */
	public void setApplicLevelButtonMode(ButtonsModeEnum newValue) {
		applicLevel.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#addFilterApplicLevel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicLevel(ViewerFilter filter) {
		applicLevel.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#addBusinessFilterApplicLevel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToApplicLevel(ViewerFilter filter) {
		applicLevel.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#getApplicLevelCombo()
	 * 
	 */
	public Object getApplicLevelCombo() {
		if (applicLevelCombo.getSelection() instanceof StructuredSelection) {
			return ((StructuredSelection) applicLevelCombo.getSelection()).getFirstElement();
		}
		return "";
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#initApplicLevelCombo(Object input, Object currentValue)
	 */
	public void initApplicLevelCombo(Object input, Object currentValue) {
		applicLevelCombo.setInput(input);
		if (currentValue != null) {
			applicLevelCombo.setSelection(new StructuredSelection(currentValue));
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#setApplicLevelCombo(Object newValue)
	 * 
	 */
	public void setApplicLevelCombo(Object newValue) {
		if (newValue != null) {
			applicLevelCombo.modelUpdating(new StructuredSelection(newValue));
		} else {
			applicLevelCombo.modelUpdating(new StructuredSelection("")); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo);
		if (eefElementEditorReadOnlyState && applicLevelCombo.isEnabled()) {
			applicLevelCombo.setEnabled(false);
			applicLevelCombo.setToolTipText(RefframeworkMessages.RefCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicLevelCombo.isEnabled()) {
			applicLevelCombo.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#addFilterApplicLevelCombo(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicLevelCombo(ViewerFilter filter) {
		applicLevelCombo.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#getCriticLevel()
	 * 
	 */
	public EObject getCriticLevel() {
		if (criticLevel.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) criticLevel.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#initCriticLevel(EObjectFlatComboSettings)
	 */
	public void initCriticLevel(EObjectFlatComboSettings settings) {
		criticLevel.setInput(settings);
		if (current != null) {
			criticLevel.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel);
		if (eefElementEditorReadOnlyState && criticLevel.isEnabled()) {
			criticLevel.setEnabled(false);
			criticLevel.setToolTipText(RefframeworkMessages.RefCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !criticLevel.isEnabled()) {
			criticLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#setCriticLevel(EObject newValue)
	 * 
	 */
	public void setCriticLevel(EObject newValue) {
		if (newValue != null) {
			criticLevel.setSelection(new StructuredSelection(newValue));
		} else {
			criticLevel.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel);
		if (eefElementEditorReadOnlyState && criticLevel.isEnabled()) {
			criticLevel.setEnabled(false);
			criticLevel.setToolTipText(RefframeworkMessages.RefCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !criticLevel.isEnabled()) {
			criticLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#setCriticLevelButtonMode(ButtonsModeEnum newValue)
	 */
	public void setCriticLevelButtonMode(ButtonsModeEnum newValue) {
		criticLevel.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#addFilterCriticLevel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToCriticLevel(ViewerFilter filter) {
		criticLevel.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#addBusinessFilterCriticLevel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToCriticLevel(ViewerFilter filter) {
		criticLevel.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#getCriticLevelCombo()
	 * 
	 */
	public Object getCriticLevelCombo() {
		if (criticLevelCombo.getSelection() instanceof StructuredSelection) {
			return ((StructuredSelection) criticLevelCombo.getSelection()).getFirstElement();
		}
		return "";
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#initCriticLevelCombo(Object input, Object currentValue)
	 */
	public void initCriticLevelCombo(Object input, Object currentValue) {
		criticLevelCombo.setInput(input);
		if (currentValue != null) {
			criticLevelCombo.setSelection(new StructuredSelection(currentValue));
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#setCriticLevelCombo(Object newValue)
	 * 
	 */
	public void setCriticLevelCombo(Object newValue) {
		if (newValue != null) {
			criticLevelCombo.modelUpdating(new StructuredSelection(newValue));
		} else {
			criticLevelCombo.modelUpdating(new StructuredSelection("")); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo);
		if (eefElementEditorReadOnlyState && criticLevelCombo.isEnabled()) {
			criticLevelCombo.setEnabled(false);
			criticLevelCombo.setToolTipText(RefframeworkMessages.RefCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !criticLevelCombo.isEnabled()) {
			criticLevelCombo.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#addFilterCriticLevelCombo(ViewerFilter filter)
	 * 
	 */
	public void addFilterToCriticLevelCombo(ViewerFilter filter) {
		criticLevelCombo.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#getComment()
	 * 
	 */
	public String getComment() {
		return comment.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart#setComment(String newValue)
	 * 
	 */
	public void setComment(String newValue) {
		if (newValue != null) {
			comment.setText(newValue);
		} else {
			comment.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment);
		if (eefElementEditorReadOnlyState && comment.isEnabled()) {
			comment.setEnabled(false);
			comment.setToolTipText(RefframeworkMessages.RefCriticalityApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !comment.isEnabled()) {
			comment.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.RefCriticalityApplicability_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
