/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

import org.docx4j.wml.P;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAsset;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetEvaluation;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetResource;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetails;
import org.eclipse.opencert.webapp.reports.containers.ObjectObject;
import org.eclipse.opencert.webapp.reports.containers.visitors.IComplianceDetailsVisitor;
import org.eclipse.opencert.webapp.reports.export.docx.DocxBuilder;
import org.eclipse.opencert.webapp.reports.export.docx.DocxBuilder.FontColor;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;

public class ComplianceDetailsStatusDocxVisitor
	implements IComplianceDetailsVisitor<Integer>
{
	private final DocxBuilder _docxBuilder;

	public ComplianceDetailsStatusDocxVisitor(DocxBuilder docxBuilder) 
	{
		_docxBuilder = docxBuilder;
	}

	@Override
	public Integer visitComplianceDetails(ComplianceDetails rootCD) 
	{
		final String justificationTxt = rootCD.getComplianceMapJustification();
		final String complianceType = rootCD.getComplianceMapType();
		
		P p = _docxBuilder.startParagraph();
		_docxBuilder.appendNL(p);
		 final ObjectObject<DocxBuilder.FontColor, String> justificationColorAndText = createJustificationColorAndText(complianceType);
		_docxBuilder.appendText(p, "Compliance Level: ", DocxBuilder.FontStyle.ITALIC);
		_docxBuilder.appendText(p, justificationColorAndText.o2, DocxBuilder.FontStyle.BOLD, justificationColorAndText.o1);
		_docxBuilder.appendNL(p);
		
		_docxBuilder.appendText(p, "Justification: ", DocxBuilder.FontStyle.ITALIC);
		_docxBuilder.appendText(p, justificationTxt, DocxBuilder.FontStyle.BOLD, justificationColorAndText.o1);
		_docxBuilder.finishParagraph(p);
		
		return 0;
	}

    @Override
	public Integer visitComplianceAsset(ComplianceAsset complianceAsset, ComplianceDetails rootCD, Integer parentNode) 
	{
	    final P p = _docxBuilder.startParagraph();
		
		_docxBuilder.appendText(p, "Evidence Asset Name: ", DocxBuilder.FontStyle.ITALIC);
		_docxBuilder.appendText(p, complianceAsset.getName(), DocxBuilder.FontStyle.BOLD);
		_docxBuilder.finishParagraph(p);
		
		return 0;
	}

	@Override
	public Integer visitComplianceResource(ComplianceAssetResource complianceResource, ComplianceDetails rootCD, Integer parentNode) 
	{
	    final P p = _docxBuilder.startParagraph();
		_docxBuilder.appendText(p, "Resource Name: ", DocxBuilder.FontStyle.ITALIC);
		_docxBuilder.appendText(p, complianceResource.getName(), DocxBuilder.FontStyle.BOLD);
		_docxBuilder.appendNL(p);
		
		_docxBuilder.appendText(p, "Resource File: ", DocxBuilder.FontStyle.ITALIC);
		_docxBuilder.appendText(p, complianceResource.getRepoUrl() + "/" + complianceResource.getLocation(), DocxBuilder.FontStyle.BOLD);
		_docxBuilder.finishParagraph(p);
		
		return 0;
	}
	
	@Override
	public Integer visitComplianceEvaluation(ComplianceAssetEvaluation complianceEvaluation, ComplianceDetails rootCD, Integer parentNode)
	{
		return null;
	}

	private ObjectObject<FontColor, String> createJustificationColorAndText(String complianceType)
    {
	    ObjectObject<DocxBuilder.FontColor, String> result;
	    
	    if ((MapKind.FULL.getName()).equals(complianceType)) 
	    {
            result = new ObjectObject<DocxBuilder.FontColor, String>(DocxBuilder.FontColor.GREEN, "Fully Compliant");
        } else if ((MapKind.PARTIAL.getName()).equals(complianceType)) 
        {
            result = new ObjectObject<DocxBuilder.FontColor, String>(DocxBuilder.FontColor.BLUE, "Partially Compliant");
        } else if ((MapKind.NO_MAP.getName()).equals(complianceType)) 
        {
            result = new ObjectObject<DocxBuilder.FontColor, String>(DocxBuilder.FontColor.RED, "Not Compliant");
        } else {
            throw new IllegalArgumentException(complianceType);
        }
	    return result;
    }



}
