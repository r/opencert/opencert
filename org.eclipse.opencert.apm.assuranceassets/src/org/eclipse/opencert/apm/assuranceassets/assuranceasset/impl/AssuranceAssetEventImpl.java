/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl;

import java.util.Date;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.general.general.NamedElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assurance Asset Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEventImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEventImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEventImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEventImpl#getResultingEvaluation <em>Resulting Evaluation</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEventImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEventImpl#getTime <em>Time</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssuranceAssetEventImpl extends AssuranceAssetImpl implements AssuranceAssetEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssuranceAssetEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return (String)eGet(GeneralPackage.Literals.NAMED_ELEMENT__ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		eSet(GeneralPackage.Literals.NAMED_ELEMENT__ID, newId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eGet(GeneralPackage.Literals.NAMED_ELEMENT__NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eSet(GeneralPackage.Literals.NAMED_ELEMENT__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return (String)eGet(GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		eSet(GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, newDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssuranceAssetEvaluation getResultingEvaluation() {
		return (AssuranceAssetEvaluation)eGet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVENT__RESULTING_EVALUATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResultingEvaluation(AssuranceAssetEvaluation newResultingEvaluation) {
		eSet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVENT__RESULTING_EVALUATION, newResultingEvaluation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventKind getType() {
		return (EventKind)eGet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVENT__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(EventKind newType) {
		eSet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVENT__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getTime() {
		return (Date)eGet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVENT__TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTime(Date newTime) {
		eSet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVENT__TIME, newTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case AssuranceassetPackage.ASSURANCE_ASSET_EVENT__ID: return GeneralPackage.NAMED_ELEMENT__ID;
				case AssuranceassetPackage.ASSURANCE_ASSET_EVENT__NAME: return GeneralPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (derivedFeatureID) {
				case AssuranceassetPackage.ASSURANCE_ASSET_EVENT__DESCRIPTION: return GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.NAMED_ELEMENT__ID: return AssuranceassetPackage.ASSURANCE_ASSET_EVENT__ID;
				case GeneralPackage.NAMED_ELEMENT__NAME: return AssuranceassetPackage.ASSURANCE_ASSET_EVENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION: return AssuranceassetPackage.ASSURANCE_ASSET_EVENT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //AssuranceAssetEventImpl
