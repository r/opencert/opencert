/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.evm.evidspec.evidence.search;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.common.id.CDOIDUtil;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;

import com.google.gson.Gson;

public class OslcKmQueryCDOHandler {

	// Default Remote Uri configuration:
	public static final String TrcAmassWebServiceUrl = "http://authoring.reusecompany.com:9999/OslcKmService.svc/IndexRequirement";
	public static final String TrcAmassWebServiceUrl_Retrieval = "http://authoring.reusecompany.com:9999/OslcKmService.svc/RetrieveSimilarRequirements";

	public static final String StringEmpty = "";
	private static final String HttpPostProtocolName = "POST";
	private static final String ContentTypeRequestProperty = "Content-Type";
	private static final String JsonContentTypeRequestPropertyValue = "application/json";
	private static final String ContentLengthRequestProperty = "Content-Length";
	private static final String AsciiFormat = "ASCII";

	private static final String ReqTextParam = "text";

	// Json parts:
	private static final String JsonStartWebRequest = "{\"";
	private static final String JsonEndWebRequest = "\"}";
	private static final String JsonAttributeValueSeparatorWebRequest = "\":\"";

	// Answer headers / footers:
	private static final String XMLResponseHeader = "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">";
	private static final String XMLResponseFooter = "</string>";
	// Debug messages:
	private static final String HttpWebRequestErrorMessage = "Failed : HTTP error code : ";

	public static List<CDOObject> RunQuery(String query, CDOSession cdoSession) {
		String queryResult = "";
		try {
			// Service invoke
			queryResult = getSimilarRequirementsInOslcKmRepository(query);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Results for query '" + query + "': " + queryResult);

		CDOView viewCDO = CDOConnectionUtil.instance.openView(cdoSession);
		List<CDOObject> cdoObjects = new ArrayList<CDOObject>();
		List<String> idsFromJson = null;
		if (queryResult != null && queryResult != "") {
			idsFromJson = GetObjectsFromJson(queryResult);
			if (idsFromJson != null && idsFromJson.size() > 0) {

				CDOObject cdoObject = null;			
				String cdoIdAsString = null;
				for (int i = 0; i < idsFromJson.size(); i++) {
					cdoIdAsString = idsFromJson.get(i);
					if (cdoIdAsString != null) {
						cdoObject = resolveCDOObject(cdoIdAsString, viewCDO);
						if (cdoObject != null && !cdoObjects.contains(cdoObject)) {
							cdoObjects.add(cdoObject);
						}
					}
				}
			}
		}
		return cdoObjects;
	}

	private static CDOObject resolveCDOObject(String objectId, Object context) {
		// the only context we can handle at the moment is a CDOView
		if (context instanceof CDOView) {
			try {
				CDOID id = CDOIDUtil.read(objectId);
				CDOView view = (CDOView) context;
				CDOObject object = view.getObject(id, true);
				return object;
			} catch (Exception e) {
				// ignore
				e.printStackTrace();
			}
		}

		return null;
	}

	public static String getSimilarRequirementsInOslcKmRepository(String text) throws java.net.ConnectException {
		String result = "";
		HttpURLConnection conn;
		conn = null;
		DataInputStream doi;
		doi = null;
		InputStream inputStream;
		inputStream = null;
		Scanner s;
		s = null;
		try {
			if (text != null && !text.equals(StringEmpty)) {

				// Building POST parameters
				StringBuilder postData = new StringBuilder();
				postData.append(JsonStartWebRequest);
				postData.append(ReqTextParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(text);
				postData.append(JsonEndWebRequest);

				byte[] postDataBytes = postData.toString().getBytes(AsciiFormat);

				URL url = new URL(TrcAmassWebServiceUrl_Retrieval);

				conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod(HttpPostProtocolName);
				conn.setRequestProperty(ContentTypeRequestProperty, JsonContentTypeRequestPropertyValue);
				conn.setRequestProperty(ContentLengthRequestProperty, String.valueOf(postDataBytes.length));
				conn.setDoOutput(true);
				conn.getOutputStream().write(postDataBytes);

				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					System.out.println(HttpWebRequestErrorMessage + conn.getResponseCode());

				} else {

					inputStream = conn.getInputStream();
					doi = new DataInputStream(inputStream);
					result = doi.readLine();

					if (result != null && !result.equals(StringEmpty)) {
						result = result.replace(XMLResponseHeader, StringEmpty);
						result = result.replace(XMLResponseFooter, StringEmpty);
					}

					// s = new Scanner(inputStream);
					// s.useDelimiter(Delimiter);
					// result = s.hasNextBoolean() ? s.nextBoolean() : false;
				}
			}
		} catch (java.net.ConnectException e) {
			String errorMessage;
			errorMessage = e.getMessage() + " @ " + TrcAmassWebServiceUrl;
			System.out.println(errorMessage);
			throw e;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (s != null) {
				s.close();
				s = null;
			}
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception ex1) {

				}
				inputStream = null;
			}
			if (conn != null) {
				conn.disconnect();
				conn = null;
			}
		}
		return result;
	}

	public static List<String> GetObjectsFromJson(String json) {
		final Gson gson;
		gson = new Gson();
		List<String> result = null;
		Reader inputString = null;
		BufferedReader inFromUser = null;
		try {
			inputString = new StringReader(json);
			inFromUser = new BufferedReader(inputString);
			result = gson.fromJson(inFromUser, List.class);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			throw ex;
		} finally {
			if (inputString != null) {
				try {
					inputString.close();
					inputString = null;
				} catch (Exception ex) {
				}
			}
			if (inFromUser != null) {
				try {
					inFromUser.close();
					inFromUser = null;
				} catch (Exception ex) {
				}
			}
		}
		return result;
	}
}
