/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;


import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.ibm.icu.text.SimpleDateFormat;
import com.vaadin.annotations.Theme;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.view.metrics.JFreeChartWrapper;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartColor;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;





@Theme("opencerttheme")
public class AssuranceChartsMetrics 
	extends CustomComponent

{


	private static final long serialVersionUID = 2013140386664732042L;
	private static final String WEEKLY_EVIDENCE_ASSURANCE_CHART_TITLE = "Weekly Assurance Assets Metrics - Evidence"; 
	private static final String HISTOGRAM_EVIDENCE_ASSURANCE_CHART_TITLE = "Histogram Assurance Assets Metrics - Evidence"; 
	private static final String WEEKLY_PROCESS_ASSURANCE_CHART_TITLE = "Weekly Assurance Assets Metrics - Process"; 
	private static final String HISTOGRAM_PROCESS_ASSURANCE_CHART_TITLE = "Histogram Assurance Assets Metrics - Process"; 
    private static final String NO_DATA_AVAILABLE = "No Data Available";
    
    private byte[] weeklyEvidence;
    private byte[] histogramEvidence;
    private byte[] weeklyProcess;
    private byte[] histogramProcess;
    
    private List<Date> artefactListDate;
    private HashMap<Date,HashMap<String,Integer>> artefactValues;
    
    private List<Date> activityListDate;
    private HashMap<Date,HashMap<String,Integer>> activityValues;
 
    class DateComparator implements Comparator<Date> {
        @Override
        public int compare(Date a, Date b) { 
            return a.before(b) ? -1 : a.after(b) ? 1 : 0;
        }
    }
    
    
                      
    public AssuranceChartsMetrics(AssuranceProjectWrapper project)
    {      
         
        VerticalLayout mainPanel = new VerticalLayout();
		
		mainPanel.setStyleName("chartsSpacing");
		mainPanel.setSpacing(true);
		
	
	
		
		List<AssuranceAssetEvent> artefactEvidenceEvents = getArtefactsEvents(project);
	    artefactListDate = new ArrayList<Date>();
	    artefactValues = new HashMap<Date,HashMap<String,Integer>>(artefactEvidenceEvents.size());
	    countArtefacts(artefactEvidenceEvents);
	    
	    
	    List<AssuranceAssetEvent> activityProcessEvents = getActivitiesEvents(project);
	    activityListDate = new ArrayList<Date>();
	    activityValues = new HashMap<Date,HashMap<String,Integer>>(activityProcessEvents.size());
	    countActivities(activityProcessEvents);

		
		mainPanel.addComponent(weeklyArtefactAssuranceMetrics());
		mainPanel.addComponent(histogramArtefactAssuranceMetrics());
		mainPanel.addComponent(weeklyActivityAssuranceMetrics());		
		mainPanel.addComponent(histogramActivityAssuranceMetrics());
		
		//add mainPanel component
		setCompositionRoot(mainPanel);
		setSizeFull();
        
 
    }




    private List<AssuranceAssetEvent> getActivitiesEvents(AssuranceProjectWrapper newProject) {
    	
    	List<AssuranceAssetEvent> assuranceAssetEvents = new ArrayList<AssuranceAssetEvent>();
    	
    	AssuranceProjectManager projectManager = 
				(AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
    	AssuranceProject project = projectManager.getProject(newProject.getId());
    	
    	if(project != null){
	    	EList<AssetsPackage> assetsPackages = project.getAssetsPackage();
			for (AssetsPackage assetsPackage : assetsPackages) {
				if (assetsPackage.isIsActive()) {				
					EList<ProcessModel> processModels = assetsPackage.getProcessModel();
					for (ProcessModel processModel : processModels) {
						/**/
						EObject eProcessModel= (EObject) processModel; 
			    		 for (Iterator<EObject> iterator = eProcessModel.eAllContents(); iterator.hasNext();) {
			        		 EObject aEObject = iterator.next();
			        		 if (aEObject instanceof Activity){
			        			 Activity activity = (Activity) aEObject;
			        			 List<AssuranceAssetEvent> checkDate = activity.getLifecycleEvent();
									for(AssuranceAssetEvent event : checkDate){
										if(event.getTime()!=null) assuranceAssetEvents.add(event);
									}
			        		 }
			    		 }
						/**/
						
					}
				}
			}
    	}
    	
    	return assuranceAssetEvents;

	}




	private List<AssuranceAssetEvent> getArtefactsEvents(AssuranceProjectWrapper newProject) {
    	
    	List<AssuranceAssetEvent> assuranceAssetEvents = new ArrayList<AssuranceAssetEvent>();
    	
    	AssuranceProjectManager projectManager = 
				(AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
    	AssuranceProject project = projectManager.getProject(newProject.getId());
    	
    	if(project != null){
    	
	    	EList<AssetsPackage> assetsPackages = project.getAssetsPackage();
			for (AssetsPackage assetsPackage : assetsPackages) {
				if (assetsPackage.isIsActive()) {
					EList<ArtefactModel> artefactModels = assetsPackage.getArtefactsModel();
					for (ArtefactModel artefactModel : artefactModels) {
						/**/
						
			    		EObject eArtefactModel= (EObject) artefactModel; 
			    		 for (Iterator<EObject> iterator = eArtefactModel.eAllContents(); iterator.hasNext();) {
			        		 EObject aEObject = iterator.next();
			        		 if (aEObject instanceof Artefact){
			        			 Artefact artefact = (Artefact) aEObject;
			        			 List<AssuranceAssetEvent> checkDate = artefact.getLifecycleEvent();
									for(AssuranceAssetEvent event : checkDate){
										if(event.getTime()!=null) assuranceAssetEvents.add(event);
									}
			        		 }
			    		 }
						/**/
						
					}
				}
			}
    	}
    	
    	return assuranceAssetEvents;
	}




	private Component histogramActivityAssuranceMetrics() {
    	
    	final JFreeChart chart = ChartFactory.createBarChart(
        		HISTOGRAM_PROCESS_ASSURANCE_CHART_TITLE,      // chart title
                "Days",               // domain axis label
                "Number of Activity Events", // range axis label
                populateWithActivityDailyData(),                  // data
                PlotOrientation.VERTICAL, // orientation
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
            );

    	// get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED);  
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
      
        final CategoryAxis axis = plot.getDomainAxis();
        axis.setCategoryLabelPositions(
            CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 8.0)
        );
        
        final CategoryItemRenderer renderer = plot.getRenderer();
        renderer.setBaseItemLabelsVisible(true);     
        renderer.setSeriesPaint(0, ChartColor.LIGHT_BLUE);
       
        
        final BarRenderer barRenderer = (BarRenderer) plot.getRenderer();
        barRenderer.setDrawBarOutline(false);
        barRenderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        barRenderer.setBaseItemLabelsVisible(true);
        barRenderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
        barRenderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));
        
        
        chart.removeLegend();
        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
		
        
        
        try {
			histogramProcess = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
        
		Component baseAssetTypeWrapper = new JFreeChartWrapper(chart);
		baseAssetTypeWrapper.setHeight("80%");
		baseAssetTypeWrapper.setWidth("80%");
		baseAssetTypeWrapper.setStyleName("leftChartsSpacing");
		return baseAssetTypeWrapper;
	}




	private CategoryDataset populateWithActivityDailyData() {
		
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		
		if(!activityListDate.isEmpty()){
			for(Date d : activityListDate){
				
				HashMap<String,Integer> info = activityValues.get(d);
				int event = 0;
				event += info.containsKey("Creation") ? info.get("Creation") : 0;
				event += info.containsKey("Modification") ? info.get("Modification") : 0;
				event += info.containsKey("Evaluation") ? info.get("Evaluation") : 0;
				event += info.containsKey("Approval") ? info.get("Approval") : 0;
				event += info.containsKey("Revocation") ? info.get("Revocation") : 0;
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
				String date = sdf.format(d);
				
				dataset.addValue(event, "Event", date);
			}
		}
	
        return dataset;
	}




	private Component weeklyActivityAssuranceMetrics() {
    	final JFreeChart chart = ChartFactory.createBarChart(
        		WEEKLY_PROCESS_ASSURANCE_CHART_TITLE,      // chart title
                "Weeks",               // domain axis label
                "Number of Activity Events", // range axis label
                populateWithActivityWeeklyData(),                  // data
                PlotOrientation.VERTICAL, // orientation
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
            );

    	// get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED);  
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        final CategoryAxis axis = plot.getDomainAxis();
        axis.setCategoryLabelPositions(
            CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 8.0)
        );
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

      
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
        renderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));
        


        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
		
        
        try {
			weeklyProcess = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
		Component baseAssetTypeWrapper = new JFreeChartWrapper(chart);
		baseAssetTypeWrapper.setHeight("80%");
		baseAssetTypeWrapper.setWidth("80%");
		baseAssetTypeWrapper.setStyleName("leftChartsSpacing");
		return baseAssetTypeWrapper;
	}




	private CategoryDataset populateWithActivityWeeklyData() {
		
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		int weekNumber = 0;
		boolean lastWeekAdded=false;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
		long lastDate = activityListDate.isEmpty() ? 0 : activityListDate.get(0).getTime();
		int creation = 0;
		int modification = 0;
		int evaluation = 0;
		int approval = 0;
		int revocation = 0;
		String from ="";
		String to = "";
		if(!activityListDate.isEmpty()){
			for(Date d : activityListDate){
				
				HashMap<String,Integer> info = activityValues.get(d);
				creation += info.containsKey("Creation") ? info.get("Creation") : 0;
				modification += info.containsKey("Modification") ? info.get("Modification") : 0;
				evaluation += info.containsKey("Evaluation") ? info.get("Evaluation") : 0;
				approval += info.containsKey("Approval") ? info.get("Approval") : 0;
				revocation += info.containsKey("Revocation") ? info.get("Revocation") : 0;
				lastWeekAdded=false;
				
				from = sdf.format(new Date(lastDate));
				to = sdf.format(d);
				
				if(moreThanAWeek(lastDate, d.getTime())) {

					dataset.addValue(creation, "Creation",  from + " - " + to);
					
					dataset.addValue(creation, "Creation",  from + " - " + to);
					dataset.addValue(modification, "Modification",  from + " - " + to);
					dataset.addValue(evaluation, "Evaluation",  from + " - " + to);
					dataset.addValue(approval, "Approval",  from + " - " + to);
					dataset.addValue(revocation, "Revocation",  from + " - " + to);
					weekNumber++;
					lastDate=d.getTime();	
					creation=0;modification=0;evaluation=0;approval=0;revocation=0;
					lastWeekAdded=true;
				}
				
				
			}
		}
		
		if (!lastWeekAdded){
			dataset.addValue(creation, "Creation",  from + " - " + to);
			dataset.addValue(modification, "Modification",  from + " - " + to);
			dataset.addValue(evaluation, "Evaluation",  from + " - " + to);
			dataset.addValue(approval, "Approval",  from + " - " + to);
			dataset.addValue(revocation, "Revocation",  from + " - " + to);
		}
		
		if (weekNumber==0&&creation==0&&modification==0&&evaluation==0&&approval==0&&revocation==0) return null;
		else return dataset;
	}




	private void countActivities(List<AssuranceAssetEvent> activityProcessEvents) {
    	
    	if(!activityProcessEvents.isEmpty()){
        	for(AssuranceAssetEvent event : activityProcessEvents){
        		String kind = event.getType().getLiteral();
        		Date fullDate = event.getTime(); 
        		Date date = truncateDate(fullDate);
        		
        		
        		if(!activityValues.containsKey(date)){
        			HashMap<String,Integer> info = new HashMap<String,Integer>(5);
        			info.put(kind, 1);
        			activityValues.put(date,info);
        			activityListDate.add(date);
				}
				else {
					HashMap<String,Integer> info = activityValues.get(date);
					if(!info.containsKey(kind)){
						info.put(kind, 1);
						
					}
					else info.put(kind, (info.get(kind)+1));
					
					activityValues.put(date,info);	
				}
        	}
        }
    	
    	activityListDate.sort(new DateComparator());
		
	}




	private void countArtefacts(List<AssuranceAssetEvent> artefactEvidenceEvents) {

    	
        if(!artefactEvidenceEvents.isEmpty()){
        	for(AssuranceAssetEvent event : artefactEvidenceEvents){
        		String kind = event.getType().getLiteral();
        		Date fullDate = event.getTime(); 
        		Date date = truncateDate(fullDate);
        		
        		
        		if(!artefactValues.containsKey(date)){
        			HashMap<String,Integer> info = new HashMap<String,Integer>(5);
        			info.put(kind, 1);
        			artefactValues.put(date,info);
        			artefactListDate.add(date);
				}
				else {
					HashMap<String,Integer> info = artefactValues.get(date);
					if(!info.containsKey(kind)){
						info.put(kind, 1);
						
					}
					else info.put(kind, (info.get(kind)+1));
					
					artefactValues.put(date,info);	
				}
        	}
        }
    	
    	artefactListDate.sort(new DateComparator());
	}

    
	private Date truncateDate(Date fullDate) {
		if(fullDate==null) fullDate=new Date();	
		SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
		String date = sdf.format(fullDate); 
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			return new Date();
		}
	}




	private CategoryDataset populateWithArtefactWeeklyData() {
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		int weekNumber = 0;
		boolean lastWeekAdded=false;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
		long lastDate = artefactListDate.isEmpty() ? 0 : artefactListDate.get(0).getTime();
		int creation = 0;
		int modification = 0;
		int evaluation = 0;
		int approval = 0;
		int revocation = 0;
		String from ="";
		String to = "";
		if(!artefactListDate.isEmpty()){
			for(Date d : artefactListDate){
				
				HashMap<String,Integer> info = artefactValues.get(d);
				creation += info.containsKey("Creation") ? info.get("Creation") : 0;
				modification += info.containsKey("Modification") ? info.get("Modification") : 0;
				evaluation += info.containsKey("Evaluation") ? info.get("Evaluation") : 0;
				approval += info.containsKey("Approval") ? info.get("Approval") : 0;
				revocation += info.containsKey("Revocation") ? info.get("Revocation") : 0;
				lastWeekAdded=false;
				
				from = sdf.format(new Date(lastDate));
				to = sdf.format(d);
				
				if(moreThanAWeek(lastDate, d.getTime())) {

					dataset.addValue(creation, "Creation",  from + " - " + to);
					
					dataset.addValue(creation, "Creation",  from + " - " + to);
					dataset.addValue(modification, "Modification",  from + " - " + to);
					dataset.addValue(evaluation, "Evaluation",  from + " - " + to);
					dataset.addValue(approval, "Approval",  from + " - " + to);
					dataset.addValue(revocation, "Revocation",  from + " - " + to);
					weekNumber++;
					lastDate=d.getTime();	
					creation=0;modification=0;evaluation=0;approval=0;revocation=0;
					lastWeekAdded=true;
				}
				
				
			}
		}
		
		if (!lastWeekAdded){
			dataset.addValue(creation, "Creation",  from + " - " + to);
			dataset.addValue(modification, "Modification",  from + " - " + to);
			dataset.addValue(evaluation, "Evaluation",  from + " - " + to);
			dataset.addValue(approval, "Approval",  from + " - " + to);
			dataset.addValue(revocation, "Revocation",  from + " - " + to);
		}
		
		if (weekNumber==0&&creation==0&&modification==0&&evaluation==0&&approval==0&&revocation==0) return null;
		else return dataset;
	}
    
    
	private boolean moreThanAWeek(long lastDate, long actualDate) {
		long different = actualDate - lastDate;
		
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
		long daysInMilli = hoursInMilli * 24;
		
		long elapsedDays = different / daysInMilli;
		
		return (elapsedDays>=7);
		
	}




	private Component weeklyArtefactAssuranceMetrics() {
			
	
       
        final JFreeChart chart = ChartFactory.createBarChart(
        		WEEKLY_EVIDENCE_ASSURANCE_CHART_TITLE,      // chart title
                "Weeks",               // domain axis label
                "Number of Artefact Events", // range axis label
                populateWithArtefactWeeklyData(),                  // data
                PlotOrientation.VERTICAL, // orientation
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
            );

        // get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED);  
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        final CategoryAxis axis = plot.getDomainAxis();
        axis.setCategoryLabelPositions(
            CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 8.0)
        );
      
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
        renderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));
        
     


        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
		
       
        try {
			weeklyEvidence = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		Component baseAssetTypeWrapper = new JFreeChartWrapper(chart);
		baseAssetTypeWrapper.setHeight("80%");
		baseAssetTypeWrapper.setWidth("80%");
		baseAssetTypeWrapper.setStyleName("leftChartsSpacing");
		return baseAssetTypeWrapper;
	}


	


	private CategoryDataset populateWithArtefactDailyData() {
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		
		if(!artefactListDate.isEmpty()){
			for(Date d : artefactListDate){
				
				HashMap<String,Integer> info = artefactValues.get(d);
				int event = 0;
				event += info.containsKey("Creation") ? info.get("Creation") : 0;
				event += info.containsKey("Modification") ? info.get("Modification") : 0;
				event += info.containsKey("Evaluation") ? info.get("Evaluation") : 0;
				event += info.containsKey("Approval") ? info.get("Approval") : 0;
				event += info.containsKey("Revocation") ? info.get("Revocation") : 0;
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
				String date = sdf.format(d);
				
				dataset.addValue(event, "Event", date);
				
			}
		}
	
        return dataset;
	}



	private Component histogramArtefactAssuranceMetrics() {
		
	 
       
        final JFreeChart chart = ChartFactory.createBarChart(
        		HISTOGRAM_EVIDENCE_ASSURANCE_CHART_TITLE,      // chart title
                "Days",               // domain axis label
                "Number of Artefact Events", // range axis label
                populateWithArtefactDailyData(),                  // data
                PlotOrientation.VERTICAL, // orientation
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
            );

        // get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED);  
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
      
        final CategoryAxis axis = plot.getDomainAxis();
        axis.setCategoryLabelPositions(
            CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 8.0)
        );
        
        final CategoryItemRenderer renderer = plot.getRenderer();
        renderer.setBaseItemLabelsVisible(true);
        renderer.setSeriesPaint(0, ChartColor.LIGHT_BLUE);
       
        
        plot.setForegroundAlpha(0.8f);
        plot.setBackgroundAlpha(0.2f);      
        //baseAssetTypePlot.setSectionPaint("Base Activity", Color.green);


      
        final BarRenderer barRenderer = (BarRenderer) plot.getRenderer();
        barRenderer.setDrawBarOutline(false);
        barRenderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        barRenderer.setBaseItemLabelsVisible(true);
        barRenderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
        barRenderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));

        
        chart.removeLegend();
        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
        
        try {
			histogramEvidence = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Component baseAssetTypeWrapper = new JFreeChartWrapper(chart);
		baseAssetTypeWrapper.setHeight("80%");
		baseAssetTypeWrapper.setWidth("80%");
		baseAssetTypeWrapper.setStyleName("leftChartsSpacing");
		return baseAssetTypeWrapper;
	}




	public String getWeeklyEvidenceAssuranceChartTitle() {
		return WEEKLY_EVIDENCE_ASSURANCE_CHART_TITLE;
	}




	public String getHistogramEvidenceAssuranceChartTitle() {
		return HISTOGRAM_EVIDENCE_ASSURANCE_CHART_TITLE;
	}




	public String getWeeklyProcessAssuranceChartTitle() {
		return WEEKLY_PROCESS_ASSURANCE_CHART_TITLE;
	}




	public String getHistogramProcessAssuranceChartTitle() {
		return HISTOGRAM_PROCESS_ASSURANCE_CHART_TITLE;
	}




	public byte[] getWeeklyEvidence() {
		return weeklyEvidence;
	}




	public byte[] getHistogramEvidence() {
		return histogramEvidence;
	}




	public byte[] getWeeklyProcess() {
		return weeklyProcess;
	}




	public byte[] getHistogramProcess() {
		return histogramProcess;
	}




	
	
    
    
	
}

