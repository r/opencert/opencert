/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Technique</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseTechnique#getCriticApplic <em>Critic Applic</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseTechnique#getAim <em>Aim</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseTechnique()
 * @model
 * @generated
 */
public interface BaseTechnique extends BaseAssurableElement, DescribableElement, BaselineElement {
	/**
	 * Returns the value of the '<em><b>Critic Applic</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Critic Applic</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Critic Applic</em>' reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseTechnique_CriticApplic()
	 * @model
	 * @generated
	 */
	EList<BaseCriticalityApplicability> getCriticApplic();

	/**
	 * Returns the value of the '<em><b>Aim</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aim</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aim</em>' attribute.
	 * @see #setAim(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseTechnique_Aim()
	 * @model
	 * @generated
	 */
	String getAim();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseTechnique#getAim <em>Aim</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aim</em>' attribute.
	 * @see #getAim()
	 * @generated
	 */
	void setAim(String value);

} // BaseTechnique
