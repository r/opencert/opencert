/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.exttools.connectors;

import java.io.IOException;

import org.eclipse.opencert.webapp.reports.exttools.processors.util.ErrorFailPassProperty;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;



public class ParasoftDTPUTConnector
        extends AbstractParasoftDTPFilterConnector
{
    @Override
    public String getName()
    {
        return "Parasoft DTP UT Connector";
    }

    @Override
    public String getDescription()
    {
        return "Connects to Parasoft DTP server and retrieves a number of Unit Test violations "
                + "reported for the specific DTP project filter. "
                + "Note: it uses DTP REST API /dynamicAnalysisTestCases";
    }
    
    @Override
    public String getUrlSuffix()
    {
        final String result = "/grs/api/v1/dynamicAnalysisTestCases/count?filterId="
                + getSettingsValue(FILTER_KEY);
        return result;
    }

    @Override
    public int processHttpResponse(String httpResponse) throws IOException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        ErrorFailPassProperty cc = objectMapper.readValue(httpResponse, ErrorFailPassProperty.class);
        
        return cc.getFail();
    }
}
