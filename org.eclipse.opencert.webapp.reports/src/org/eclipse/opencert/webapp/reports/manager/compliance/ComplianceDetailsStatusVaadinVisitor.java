/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.eclipse.opencert.webapp.reports.containers.BaselineElementDetails;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAsset;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetEvaluation;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetResource;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetails;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetailsStatusItem;
import org.eclipse.opencert.webapp.reports.containers.visitors.IComplianceDetailsVisitor;
import org.eclipse.opencert.webapp.reports.impactanalysis.ImpactAnalyserExecutor;
import org.eclipse.opencert.webapp.reports.listeners.ComplianceStatusListener;
import org.eclipse.opencert.webapp.reports.listeners.EvidenceFileUploaderListener;
import org.eclipse.opencert.webapp.reports.listeners.UnassignOrAssignArtefactListener;
import org.eclipse.opencert.webapp.reports.manager.AssuranceAssetEventManager;
import org.eclipse.opencert.webapp.reports.manager.BaseAssurableElementManager;
import org.eclipse.opencert.webapp.reports.util.BaselineElementType;
import org.eclipse.opencert.webapp.reports.util.ComplianceAssetType;
import org.eclipse.opencert.webapp.reports.util.ICommonCssStyles;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.util.UploadFileMode;
import org.eclipse.opencert.webapp.reports.view.AbstractComplianceReport;
import org.eclipse.opencert.webapp.reports.view.UploadFileWindow;
import org.eclipse.opencert.webapp.reports.view.common.ConfirmationWindow;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.property.OpencertPropertiesReader;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;


public class ComplianceDetailsStatusVaadinVisitor
	implements IComplianceDetailsVisitor<ComplianceDetailsStatusItem>
{
	private final HierarchicalContainer _hcResult;
	private final String[] _columnIDs;

	private static final String TREE_LEVEL_JUST = "levelJust";
	private static final String TREE_LEVEL_ASSET = "levelAsset";
	private static final String TREE_LEVEL_RESOURCE = "levelResource";
	
	private static final String UNASSIGN_LINK_STYLE = "unassignLink";
	
	private static final String COMPLIANCE_TYPE_FULLY_DESC = "Fully";
	private static final String COMPLIANCE_TYPE_PARTIALLY_DESC = "Partially";
	private static final String COMPLIANCE_TYPE_NO_MAP_DESC = "No map";
	private static final String BASE_ARTEFACT_UNASSIGN_LINK_DESC = "Unassign this Justification, Artefact and Resource from Baseline Element.";
	private static final String BASE_ACTIVITY_UNASSIGN_LINK_DESC = "Unassign this Justification and Activity from Baseline Element.";
	private static final String BASE_REQUIREMENT_UNASSIGN_LINK_DESC = "Unassign this Justification and Artefact(Resource)/Activity from Baseline Element.";
    
    private AbstractComplianceReport complianceEstimationReport;
    private EvidenceFileUploaderListener evidenceFileUploaderListener;
    private UnassignOrAssignArtefactListener unassignOrAssignArtefactListener;
    private ComplianceStatusListener complianceStatusListener;
    private final BaselineElementDetails _selectedBaselineElementDetails;
	
	public ComplianceDetailsStatusVaadinVisitor(BaselineElementDetails selectedBaselineElementDetails, 
			HierarchicalContainer complianceDetailsContainer, String[] columnIDs)
	{
		_hcResult = complianceDetailsContainer;
		if (columnIDs == null || columnIDs.length != 3) {
			throw new IllegalArgumentException();
		}
		_columnIDs = columnIDs;
		_selectedBaselineElementDetails = selectedBaselineElementDetails;
	}

	@Override
	public ComplianceDetailsStatusItem visitComplianceDetails(final ComplianceDetails complianceDetails) 
	{
		String complianceJustificationName = buildComplianceJustificationName(complianceDetails);
		HorizontalLayout justificationNameLayout = createHorizontalLayout(createLabel(complianceJustificationName, "justification"), null);
		Label justificationTypeLabel = buildComplianceJustificationTypeLabel(complianceDetails.getComplianceMapType());
		Button unassignButton = createUnassignButton(complianceDetails);
		
		ComplianceDetailsStatusItem item = new ComplianceDetailsStatusItem(complianceDetails.getComplianceMapCdoId(), justificationNameLayout,
				justificationTypeLabel, unassignButton, TREE_LEVEL_JUST, complianceDetails);
		addItemToContainer(_hcResult, item, null);
		
		unassignButton.addClickListener((e) ->
        {
            complianceStatusListener.selectComplianceDetailsStatusItem(item);
        });
		
		addLayoutClickListener(justificationNameLayout, item);
		
		return item;
	}

	@Override
	public ComplianceDetailsStatusItem visitComplianceAsset(ComplianceAsset complianceAsset, ComplianceDetails rootCD, ComplianceDetailsStatusItem parentNode) 
	{
		String complianceAssetName = buildComplianceAssetName(complianceAsset);
		HorizontalLayout assetNameLayout = createHorizontalLayout(createLabel(complianceAssetName, "asset"), null);
		
		ComplianceDetailsStatusItem assetItem = new ComplianceDetailsStatusItem(complianceAsset.getCdoId(), assetNameLayout, 
				createIAComplianceLabel(complianceAsset), createIAComplianceButton(complianceAsset, rootCD), TREE_LEVEL_ASSET, rootCD);
		addItemToContainer(_hcResult, assetItem, parentNode);
		
		addLayoutClickListener(assetNameLayout, assetItem);
		
		return assetItem;
	}

    private Button createIAComplianceButton(ComplianceAsset complianceAsset, ComplianceDetails complianceDetails)
    {
        final IAEvaluation iaEvaluation = complianceAsset.getIaEvaluation();
        if (iaEvaluation == null || !iaEvaluation.needsAttention() || IAEvaluationStatus.REVOKED.equals(iaEvaluation.getEvaluationStatus())) {
            return null;
        }
        
        Button actionButton = new Button(iaEvaluation.getEvaluationStatus().getQuickActionLabel());
        actionButton.setDescription("Report as " + iaEvaluation.getEvaluationStatus().getQuickActionLabel());
        actionButton.setImmediate(true);
        actionButton.setStyleName(ICommonCssStyles.IA_ACTION_BUTTON_STYLE);
        
        final Long eventCdoId = CDOStorageUtil.getCDOId(iaEvaluation.getAssuranceAssetEvent());
        
        actionButton.addClickListener((e) -> {
            AssuranceAssetEventManager assuranceAssetEventManager  = (AssuranceAssetEventManager)SpringContextHelper.getBeanFromWebappContext(AssuranceAssetEventManager.SPRING_NAME);
            assuranceAssetEventManager.setDateToNow(eventCdoId);
            Notification.show("\""+ iaEvaluation.getEvaluationStatus().getQuickActionLabel() + "\" action performed", Notification.Type.HUMANIZED_MESSAGE);
            
            restoreLeftPanelFocus(complianceDetails);
            
        });
        
        return actionButton;
    }

    private void restoreLeftPanelFocus(ComplianceDetails complianceDetails)
    {
        complianceEstimationReport.artefactAssignedOrUnassigned(complianceDetails.getBaseAssetId());
        complianceStatusListener.updateBaselineDetailsStatus();
    }

    private Label createIAComplianceLabel(ComplianceAsset complianceAsset)
    {
    	IAEvaluation iaEvaluation = complianceAsset.getIaEvaluation();
    	if (iaEvaluation == null || !iaEvaluation.needsAttention()) {
    		return new Label("");
    	}
        
    	Label iaComplianceLabel = createLabel(iaEvaluation.getEvaluationStatus().getUserFriendlyLabel(), "noCompliant");
    	iaComplianceLabel.setDescription(iaEvaluation.generateIAStatusDescription(
    			"<br/><font color=\"red\">" + iaEvaluation.getEvaluationStatus().getRequiredUserAction() + "</font>"));
    
    	return iaComplianceLabel;
    }

	@Override
	public ComplianceDetailsStatusItem visitComplianceResource(ComplianceAssetResource complianceResource, ComplianceDetails rootCD, ComplianceDetailsStatusItem parentNode) 
	{
		String complianceResourceName = buildComplianceResourceName(complianceResource);
		
		Button modifyButton = createModifyResourceFileButton(complianceResource, rootCD);
		HorizontalLayout resourceLayout = createHorizontalLayout(createLabel(complianceResourceName, "resource"), modifyButton, "complianceResource");
		
		ComplianceDetailsStatusItem resourceItem = new ComplianceDetailsStatusItem(complianceResource.getCdoId(), resourceLayout, 
				null, null, TREE_LEVEL_RESOURCE, rootCD);
		addItemToContainer(_hcResult, resourceItem, parentNode);
		
		addLayoutClickListener(resourceLayout, resourceItem);
		modifyButton.addClickListener((e) ->
        {
            complianceStatusListener.selectComplianceDetailsStatusItem(resourceItem);
        });
		
		return resourceItem;
	}
	
	@Override
	public ComplianceDetailsStatusItem visitComplianceEvaluation(ComplianceAssetEvaluation complianceEvaluation, ComplianceDetails rootCD, ComplianceDetailsStatusItem parentNode) {
		return null;
	}

	public HierarchicalContainer getResult() 
	{
		return _hcResult;
	}
	
	private String buildComplianceJustificationName(ComplianceDetails complianceJustification) 
	{
		String complianceExplanation = complianceJustification.getComplianceMapJustification();
		String complianceExplanationItemId = "Compliance Justification: " +
				"<i>" + complianceExplanation + "</i>";
		
		return complianceExplanationItemId;
	}
	
	private Label buildComplianceJustificationTypeLabel(String complianceType) {
		Label type = null;
		if ((MapKind.FULL.getName()).equals(complianceType)) {
			type = createLabel(COMPLIANCE_TYPE_FULLY_DESC, "fullyCompliant");
			type.setDescription("Fully Compliant");
		} else if ((MapKind.PARTIAL.getName()).equals(complianceType)) {
			type = createLabel(COMPLIANCE_TYPE_PARTIALLY_DESC, "partiallyCompliant");
			type.setDescription("Partially Compliant");
		} else if ((MapKind.NO_MAP.getName()).equals(complianceType)) {
			type = createLabel(COMPLIANCE_TYPE_NO_MAP_DESC, "noCompliant");
			type.setDescription("Not Compliant");
		}
		return type;
	}
	
	private String buildComplianceAssetName(ComplianceAsset complianceAsset) 
	{
		String complianceAssetName = "";
		if (complianceAsset.getName() != null) {
			complianceAssetName = complianceAsset.getName();
		}
		String complianceAssetItemId = complianceAsset.getType() + ": <i>" + 
					complianceAssetName + "</i>";

		return complianceAssetItemId;
	}
	
	private String buildComplianceResourceName(ComplianceAssetResource complianceResource) 
	{
		String complianceResourceFile = "";
		if (complianceResource.getName() != null) {
			complianceResourceFile = complianceResource.getLocation();
		}
		String targetUrl = complianceResource.getRepoUrl() + "/" + complianceResource.getLocation();
		String complianceEvaluationItemId = "Resource file: <i>" + complianceResourceFile + "</i>&nbsp;&nbsp;&nbsp;" + "<a href=\"" + targetUrl 
				+ "\" target=\"_blank\" title=\"Download the resource file\">[Download]</a>";

		return complianceEvaluationItemId;
	}
	
	private void addItemToContainer(HierarchicalContainer hierarchicalContainer, ComplianceDetailsStatusItem childItem, ComplianceDetailsStatusItem parentItem) 
	{
		Item item = hierarchicalContainer.addItem(childItem);
		mergePropertiesWithData(item, childItem);
		String itemLevel = childItem.getLevel();
		
		if (!TREE_LEVEL_JUST.equals(itemLevel)) {
			hierarchicalContainer.setParent(childItem, parentItem);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void mergePropertiesWithData(Item item, ComplianceDetailsStatusItem complianceDetailsStatusItem) 
	{
		item.getItemProperty(_columnIDs[0]).setValue(complianceDetailsStatusItem.getName());
		item.getItemProperty(_columnIDs[1]).setValue(complianceDetailsStatusItem.getType());
		item.getItemProperty(_columnIDs[2]).setValue(complianceDetailsStatusItem.getUnassign());
	}
	
	private Label createLabel(String labelCaption, String styleName) {
		Label label = new Label("", ContentMode.HTML);
		label.setStyleName(styleName);
		label.setValue(labelCaption);
		
		return label;
	}
	
	private HorizontalLayout createHorizontalLayout(Label name, Button button) {
		return createHorizontalLayout(name, button, null);
	}
	
	private HorizontalLayout createHorizontalLayout(Label name, Button button, String style) {
		HorizontalLayout hLayout = new HorizontalLayout();
		//hLayout.setWidth("100%");
		
		hLayout.setImmediate(true);
		hLayout.addComponent(name);
		if (button != null) {
			hLayout.addComponent(button);
			//hLayout.setExpandRatio(name, 3);
			//hLayout.setExpandRatio(button, 1);
		}
		
		if (style != null && (!"".equals(style))) {
			hLayout.setStyleName(style);
		}
			
		return hLayout;
	}
	
	private void addLayoutClickListener(HorizontalLayout horizontalLayout, ComplianceDetailsStatusItem item) {
		horizontalLayout.addLayoutClickListener(new LayoutClickListener() {
			private static final long serialVersionUID = 5031081955716195872L;

			@Override
			public void layoutClick(LayoutClickEvent event) {
				complianceStatusListener.selectComplianceDetailsStatusItem(item);
			}
		});
	}

	private Button createUnassignButton(final ComplianceDetails complianceDetails) {
		//Sub-Window
		ConfirmationWindow unassignComplianceMapWindow = createConfirmationWindow(complianceDetails);
		
        Button unassignLink = new Button(new ThemeResource("images/unassign.png"));
		unassignLink.setStyleName(UNASSIGN_LINK_STYLE);
		if (BaselineElementType.BASE_ARTEFACT.equals(complianceDetails.getBaseAssetType())) {
			unassignLink.setDescription(BASE_ARTEFACT_UNASSIGN_LINK_DESC);
		} else if (BaselineElementType.BASE_ACTIVITY.equals(complianceDetails.getBaseAssetType())) {
			unassignLink.setDescription(BASE_ACTIVITY_UNASSIGN_LINK_DESC);
		} else if (BaselineElementType.BASE_REQUIREMENT.equals(complianceDetails.getBaseAssetType())) {
			unassignLink.setDescription(BASE_REQUIREMENT_UNASSIGN_LINK_DESC);
		}
		
		unassignLink.addClickListener((e) -> 
		{
			if (!(UI.getCurrent().getWindows().contains(unassignComplianceMapWindow))) {
				UI.getCurrent().addWindow(unassignComplianceMapWindow);  
            } 
        });
		return unassignLink;
	}
	
	private Button createModifyResourceFileButton(ComplianceAssetResource complianceResource, ComplianceDetails complianceDetails) {
		UploadFileWindow uploadFileWindow = new UploadFileWindow(UploadFileMode.MODIFY_FILE, complianceResource, evidenceFileUploaderListener, 
				complianceDetails.getBaseAssetName(), complianceEstimationReport.getTitle());
		uploadFileWindow.addUnassignOrAssignArtefactListener(complianceEstimationReport);
		uploadFileWindow.addUnassignOrAssignArtefactListener(unassignOrAssignArtefactListener);
		uploadFileWindow.setBaseAssetComplianceStatus(_selectedBaselineElementDetails.getBaseAssetComplianceStatus());
		
		Button modifyFileLink = new Button("[Modify]");
		modifyFileLink.setStyleName("modifyFile");
		modifyFileLink.setDescription("Modify the resource file.");
		modifyFileLink.addClickListener((e) -> 
		{
			if (!(UI.getCurrent().getWindows().contains(uploadFileWindow))) {
				uploadFileWindow.getModificationDate().setValue(new Date());
				UI.getCurrent().addWindow(uploadFileWindow); 
			}
        });
		return modifyFileLink;
	}
	
	private ConfirmationWindow createConfirmationWindow(ComplianceDetails complianceDetails) {
		String windowCaption = "Unassign Compliance";
		String complianceJustification = complianceDetails.getComplianceMapJustification() != null ? complianceDetails.getComplianceMapJustification() : "";
		String confirmQuestion = "Are you sure you want to remove the following Compliance Justification: " + 
				"<b><i>\"" + complianceJustification + "\"</b></i>?";
		String bottomNote = "";
		
		if (BaselineElementType.BASE_ARTEFACT.equals(complianceDetails.getBaseAssetType()) ||
				BaselineElementType.BASE_REQUIREMENT.equals(complianceDetails.getBaseAssetType())) {
			bottomNote = "Note: the artefact resource file will not be removed from SVN.";
		}
		
		ConfirmationWindow unassignComplianceMapWindow = new ConfirmationWindow(windowCaption, confirmQuestion, bottomNote);
		unassignComplianceMapWindow.addYesAction((e) -> 
        {
            List<Long> artefactsCdoIds = new ArrayList<Long>();
            Collection<ComplianceAsset> complianceAssets = complianceDetails.getComplianceAssets();
            for (ComplianceAsset complianceAsset : complianceAssets) {
                if (ComplianceAssetType.ARTEFACT.equals(complianceAsset.getType())) {
                    artefactsCdoIds.add(complianceAsset.getCdoId());
                }
            }
            
            if (OpencertPropertiesReader.getInstance().isImpactAnalysisTrigerringFromWebEnabled())
            {
                ImpactAnalyserExecutor impactAnalyserExecutor = new ImpactAnalyserExecutor();
                List<UnassignOrAssignArtefactListener> unassignOrAssignArtefactListeners = new ArrayList<UnassignOrAssignArtefactListener>();
                unassignOrAssignArtefactListeners.add(complianceEstimationReport);
                unassignOrAssignArtefactListeners.add(unassignOrAssignArtefactListener);
                impactAnalyserExecutor.performImpactAnalysis(artefactsCdoIds, EventKind.MODIFICATION, unassignOrAssignArtefactListeners,
                		_selectedBaselineElementDetails.getBaseAssetComplianceStatus().getItemId());
            }
            
            removeAssetComplianceMapFromBaseAssurableElement(complianceDetails.getComplianceMapCdoId());
			
			Notification.show("The Compliance Justification has been removed sucessfully.", Notification.Type.HUMANIZED_MESSAGE);
			
			unassignComplianceMapWindow.close();	
			
            restoreLeftPanelFocus(complianceDetails);
            
            OpencertLogger.info(
                    String.format("Removed compliance justification: \"%s\" from compliance base asset: \"%s\"",  complianceDetails.getComplianceMapJustification(), complianceDetails.getBaseAssetName()));
        });
		
		return unassignComplianceMapWindow;
	}

	private void removeAssetComplianceMapFromBaseAssurableElement(Long baseComplianceMapId) {
		
	    //UNASSIGN
	    
	    BaseAssurableElementManager baseAssurableElementManager = (BaseAssurableElementManager)SpringContextHelper.getBeanFromWebappContext(BaseAssurableElementManager.SPRING_NAME);
		baseAssurableElementManager.removeBaseComplianceMap(baseComplianceMapId);
	}
        
    public void setComplianceEstimationReport(AbstractComplianceReport complianceEstimationReport)
    {
        this.complianceEstimationReport = complianceEstimationReport;
    }
    
    public void setEvidenceFileUploaderListener(EvidenceFileUploaderListener evidenceFileUploaderListener) 
    {
    	this.evidenceFileUploaderListener = evidenceFileUploaderListener;
    }
    
    public void setUnassignOrAssignArtefactListener(UnassignOrAssignArtefactListener unassignOrAssignArtefactListener) {
		this.unassignOrAssignArtefactListener = unassignOrAssignArtefactListener;
	}

    public void setComplianceStatusListener(ComplianceStatusListener complianceStatusListener) 
    {
    	this.complianceStatusListener = complianceStatusListener;
    }
}
