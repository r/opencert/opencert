/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
//import org.eclipse.ui.internal.layout.LayoutUtil;

public class Choice extends Figure {
	
	
	//says if the element is related with any entity abstraction extension
	private  int shape;
	//sets the visibility of the element
	private boolean visibility;
	
	public Choice(){
		this.setShape(0);
		this.setVisibility(false);
		this.setMinimumSize(new Dimension(80,60));
	}
	@Override
	public void paint(Graphics graphics) {
		//Rectangle r = getPoints().getBounds();
		Rectangle r = this.getBounds();
		Color bg = graphics.getBackgroundColor();
		Color fg = graphics.getForegroundColor();
		int lw = graphics.getLineWidth();
		graphics.setLineWidth(1);
		int radius = (r.width + r.height) / 32;
		Rectangle circle = new Rectangle(r.getCenter().x - radius,
				r.getCenter().y - 3*radius, 2 * radius, 2 * radius);
		if (shape == 1) {
			graphics.setBackgroundColor(ColorConstants.white);
			graphics.setForegroundColor(ColorConstants.black);
			graphics.drawOval(circle);
			graphics.fillOval(circle);
		} else if (shape == 2) {
			graphics.setBackgroundColor(ColorConstants.black);
			graphics.setForegroundColor(ColorConstants.black);
			graphics.drawOval(circle);
			graphics.fillOval(circle);
		}

		graphics.setBackgroundColor(ColorConstants.black);
		graphics.setForegroundColor(ColorConstants.black);

		Point p5 = new Point(r.x + r.width / 2, r.y + r.height / 2);
		Point p6 = new Point(r.x + 3 * r.width / 4, r.y + 3 * r.height
				/ 4);
		Point p7 = new Point(r.x + r.width / 2, r.y + r.height);
		Point p8 = new Point(r.x + 1 * r.width / 4, r.y + 3 * r.height
				/ 4);

		PointList pointList = new PointList();
		pointList.addPoint(p5);
		pointList.addPoint(p6);
		pointList.addPoint(p7);
		pointList.addPoint(p8);
		// Fill the shape
		graphics.fillPolygon(pointList);

		// Draw the outline
		graphics.setLineWidth(1);

		// Draw the outline
		//graphics.drawLine(p1, p2);
		//graphics.drawLine(p1, p3);
		//graphics.drawLine(p3, p4);
		//graphics.drawLine(p2, p4);

		// Draw the outline
		graphics.drawLine(p5, p6);
		graphics.drawLine(p6, p7);
		graphics.drawLine(p7, p8);
		graphics.drawLine(p5, p8);
		//graphics.drawLine(p6, p8);

		graphics.setBackgroundColor(bg);
		graphics.setForegroundColor(fg);
		Point p9 = new Point(r.x + r.width / 2, r.y);		
		graphics.drawLine(p5, p9);
		
		graphics.setLineWidth(lw);
		super.paint(graphics);
	}
	
	public  void setShape(int shape) {
		this.shape = shape;
	}
	public  int getShape() {
		return shape;
	}
	public void setVisibility(boolean p) {
		this.visibility = p;
	}
	public boolean getVisibility() {
		return visibility;
	}	
	
	
	}
	