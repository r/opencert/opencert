/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.infra.mappings.mapping;

import org.eclipse.opencert.infra.general.general.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.infra.mappings.mapping.Map#getMapGroup <em>Map Group</em>}</li>
 *   <li>{@link org.eclipse.opencert.infra.mappings.mapping.Map#getMapJustification <em>Map Justification</em>}</li>
 *   <li>{@link org.eclipse.opencert.infra.mappings.mapping.Map#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.infra.mappings.mapping.MappingPackage#getMap()
 * @model
 * @generated
 */
public interface Map extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Map Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Map Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Map Group</em>' reference.
	 * @see #setMapGroup(MapGroup)
	 * @see org.eclipse.opencert.infra.mappings.mapping.MappingPackage#getMap_MapGroup()
	 * @model
	 * @generated
	 */
	MapGroup getMapGroup();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.infra.mappings.mapping.Map#getMapGroup <em>Map Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Map Group</em>' reference.
	 * @see #getMapGroup()
	 * @generated
	 */
	void setMapGroup(MapGroup value);

	/**
	 * Returns the value of the '<em><b>Map Justification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Map Justification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Map Justification</em>' containment reference.
	 * @see #setMapJustification(MapJustification)
	 * @see org.eclipse.opencert.infra.mappings.mapping.MappingPackage#getMap_MapJustification()
	 * @model containment="true"
	 * @generated
	 */
	MapJustification getMapJustification();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.infra.mappings.mapping.Map#getMapJustification <em>Map Justification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Map Justification</em>' containment reference.
	 * @see #getMapJustification()
	 * @generated
	 */
	void setMapJustification(MapJustification value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.infra.mappings.mapping.MapKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MapKind
	 * @see #setType(MapKind)
	 * @see org.eclipse.opencert.infra.mappings.mapping.MappingPackage#getMap_Type()
	 * @model
	 * @generated
	 */
	MapKind getType();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.infra.mappings.mapping.Map#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MapKind
	 * @see #getType()
	 * @generated
	 */
	void setType(MapKind value);

} // Map
