/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface AssetsPackagePropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);


	/**
	 * @return the isActive
	 * 
	 */
	public Boolean getIsActive();

	/**
	 * Defines a new isActive
	 * @param newValue the new isActive to set
	 * 
	 */
	public void setIsActive(Boolean newValue);




	/**
	 * Init the artefactsModel
	 * @param settings settings for the artefactsModel ReferencesTable 
	 */
	public void initArtefactsModel(ReferencesTableSettings settings);

	/**
	 * Update the artefactsModel
	 * @param newValue the artefactsModel to update
	 * 
	 */
	public void updateArtefactsModel();

	/**
	 * Adds the given filter to the artefactsModel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToArtefactsModel(ViewerFilter filter);

	/**
	 * Adds the given filter to the artefactsModel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToArtefactsModel(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the artefactsModel table
	 * 
	 */
	public boolean isContainedInArtefactsModelTable(EObject element);




	/**
	 * Init the argumentationModel
	 * @param settings settings for the argumentationModel ReferencesTable 
	 */
	public void initArgumentationModel(ReferencesTableSettings settings);

	/**
	 * Update the argumentationModel
	 * @param newValue the argumentationModel to update
	 * 
	 */
	public void updateArgumentationModel();

	/**
	 * Adds the given filter to the argumentationModel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToArgumentationModel(ViewerFilter filter);

	/**
	 * Adds the given filter to the argumentationModel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToArgumentationModel(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the argumentationModel table
	 * 
	 */
	public boolean isContainedInArgumentationModelTable(EObject element);




	/**
	 * Init the processModel
	 * @param settings settings for the processModel ReferencesTable 
	 */
	public void initProcessModel(ReferencesTableSettings settings);

	/**
	 * Update the processModel
	 * @param newValue the processModel to update
	 * 
	 */
	public void updateProcessModel();

	/**
	 * Adds the given filter to the processModel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToProcessModel(ViewerFilter filter);

	/**
	 * Adds the given filter to the processModel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToProcessModel(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the processModel table
	 * 
	 */
	public boolean isContainedInProcessModelTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
