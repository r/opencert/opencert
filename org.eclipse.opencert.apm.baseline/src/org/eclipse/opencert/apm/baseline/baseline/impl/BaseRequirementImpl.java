/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel;
import org.eclipse.opencert.apm.baseline.baseline.BaselineElement;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#isIsSelected <em>Is Selected</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getSelectionJustification <em>Selection Justification</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getAssumptions <em>Assumptions</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getRationale <em>Rationale</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getImage <em>Image</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getApplicability <em>Applicability</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl#getSubRequirement <em>Sub Requirement</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BaseRequirementImpl extends BaseAssurableElementImpl implements BaseRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BaselinePackage.Literals.BASE_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsSelected() {
		return (Boolean)eGet(BaselinePackage.Literals.BASELINE_ELEMENT__IS_SELECTED, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsSelected(boolean newIsSelected) {
		eSet(BaselinePackage.Literals.BASELINE_ELEMENT__IS_SELECTED, newIsSelected);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSelectionJustification() {
		return (String)eGet(BaselinePackage.Literals.BASELINE_ELEMENT__SELECTION_JUSTIFICATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectionJustification(String newSelectionJustification) {
		eSet(BaselinePackage.Literals.BASELINE_ELEMENT__SELECTION_JUSTIFICATION, newSelectionJustification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return (String)eGet(GeneralPackage.Literals.NAMED_ELEMENT__ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		eSet(GeneralPackage.Literals.NAMED_ELEMENT__ID, newId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eGet(GeneralPackage.Literals.NAMED_ELEMENT__NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eSet(GeneralPackage.Literals.NAMED_ELEMENT__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return (String)eGet(GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		eSet(GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, newDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReference() {
		return (String)eGet(BaselinePackage.Literals.BASE_REQUIREMENT__REFERENCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference(String newReference) {
		eSet(BaselinePackage.Literals.BASE_REQUIREMENT__REFERENCE, newReference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAssumptions() {
		return (String)eGet(BaselinePackage.Literals.BASE_REQUIREMENT__ASSUMPTIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssumptions(String newAssumptions) {
		eSet(BaselinePackage.Literals.BASE_REQUIREMENT__ASSUMPTIONS, newAssumptions);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRationale() {
		return (String)eGet(BaselinePackage.Literals.BASE_REQUIREMENT__RATIONALE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRationale(String newRationale) {
		eSet(BaselinePackage.Literals.BASE_REQUIREMENT__RATIONALE, newRationale);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImage() {
		return (String)eGet(BaselinePackage.Literals.BASE_REQUIREMENT__IMAGE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImage(String newImage) {
		eSet(BaselinePackage.Literals.BASE_REQUIREMENT__IMAGE, newImage);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAnnotations() {
		return (String)eGet(BaselinePackage.Literals.BASE_REQUIREMENT__ANNOTATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotations(String newAnnotations) {
		eSet(BaselinePackage.Literals.BASE_REQUIREMENT__ANNOTATIONS, newAnnotations);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseRequirementRel> getOwnedRel() {
		return (EList<BaseRequirementRel>)eGet(BaselinePackage.Literals.BASE_REQUIREMENT__OWNED_REL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseApplicability> getApplicability() {
		return (EList<BaseApplicability>)eGet(BaselinePackage.Literals.BASE_REQUIREMENT__APPLICABILITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseRequirement> getSubRequirement() {
		return (EList<BaseRequirement>)eGet(BaselinePackage.Literals.BASE_REQUIREMENT__SUB_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == BaselineElement.class) {
			switch (derivedFeatureID) {
				case BaselinePackage.BASE_REQUIREMENT__IS_SELECTED: return BaselinePackage.BASELINE_ELEMENT__IS_SELECTED;
				case BaselinePackage.BASE_REQUIREMENT__SELECTION_JUSTIFICATION: return BaselinePackage.BASELINE_ELEMENT__SELECTION_JUSTIFICATION;
				default: return -1;
			}
		}
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case BaselinePackage.BASE_REQUIREMENT__ID: return GeneralPackage.NAMED_ELEMENT__ID;
				case BaselinePackage.BASE_REQUIREMENT__NAME: return GeneralPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (derivedFeatureID) {
				case BaselinePackage.BASE_REQUIREMENT__DESCRIPTION: return GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == BaselineElement.class) {
			switch (baseFeatureID) {
				case BaselinePackage.BASELINE_ELEMENT__IS_SELECTED: return BaselinePackage.BASE_REQUIREMENT__IS_SELECTED;
				case BaselinePackage.BASELINE_ELEMENT__SELECTION_JUSTIFICATION: return BaselinePackage.BASE_REQUIREMENT__SELECTION_JUSTIFICATION;
				default: return -1;
			}
		}
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.NAMED_ELEMENT__ID: return BaselinePackage.BASE_REQUIREMENT__ID;
				case GeneralPackage.NAMED_ELEMENT__NAME: return BaselinePackage.BASE_REQUIREMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION: return BaselinePackage.BASE_REQUIREMENT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //BaseRequirementImpl
