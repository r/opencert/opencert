/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String VocabularyCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String VocabularyCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String VocabularyCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String VocabularyCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String VocabularyCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String VocabularyCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String VocabularyCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String VocabularyCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String VocabularyDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String VocabularyDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String VocabularyDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String VocabularyDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String VocabularyDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String VocabularyDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String VocabularyDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String VocabularyDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String VocabularyDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String VocabularyDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String VocabularyDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String VocabularyDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String VocabularyDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String VocabularyNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String VocabularyDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String VocabularyDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String VocabularyDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String VocabularyDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String VocabularyDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String VocabularyElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Objects1Group_title;

	/**
	 * @generated
	 */
	public static String Connections2Group_title;

	/**
	 * @generated
	 */
	public static String Category1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Category1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String SourceofDefinition2CreationTool_title;

	/**
	 * @generated
	 */
	public static String SourceofDefinition2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Term3CreationTool_title;

	/**
	 * @generated
	 */
	public static String Term3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Categorizedterm1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Categorizedterm1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Definedby2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Definedby2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Hasa3CreationTool_title;

	/**
	 * @generated
	 */
	public static String Hasa3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Isa4CreationTool_title;

	/**
	 * @generated
	 */
	public static String Isa4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Refersto5CreationTool_title;

	/**
	 * @generated
	 */
	public static String Refersto5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Subcategory6CreationTool_title;

	/**
	 * @generated
	 */
	public static String Subcategory6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Vocabulary_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Term_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Term_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Category_2002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Category_2002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SourceOfDefinition_2003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CategoryTerms_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CategoryTerms_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CategorySubCategories_4002_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CategorySubCategories_4002_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TermDefinedBy_4003_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TermDefinedBy_4003_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TermIsA_4004_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TermIsA_4004_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TermHasA_4005_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TermHasA_4005_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TermRefersTo_4006_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TermRefersTo_4006_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String VocabularyModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String VocabularyModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
