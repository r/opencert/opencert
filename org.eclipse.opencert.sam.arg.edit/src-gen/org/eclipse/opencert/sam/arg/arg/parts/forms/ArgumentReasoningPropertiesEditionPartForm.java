/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;
import org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart;

import org.eclipse.opencert.sam.arg.arg.providers.ArgMessages;

// End of user code

/**
 * 
 * 
 */
public class ArgumentReasoningPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ArgumentReasoningPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text content;
	protected Text toBeSupported;
	protected Text toBeInstantiated;
	protected ReferencesTable hasStructure;
	protected List<ViewerFilter> hasStructureBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> hasStructureFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public ArgumentReasoningPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArgumentReasoningPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence argumentReasoningStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = argumentReasoningStep.addStep(ArgViewsRepository.ArgumentReasoning.Properties.class);
		propertiesStep.addStep(ArgViewsRepository.ArgumentReasoning.Properties.id);
		propertiesStep.addStep(ArgViewsRepository.ArgumentReasoning.Properties.name);
		propertiesStep.addStep(ArgViewsRepository.ArgumentReasoning.Properties.description);
		propertiesStep.addStep(ArgViewsRepository.ArgumentReasoning.Properties.content);
		propertiesStep.addStep(ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported);
		propertiesStep.addStep(ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated);
		propertiesStep.addStep(ArgViewsRepository.ArgumentReasoning.Properties.hasStructure);
		
		
		composer = new PartComposer(argumentReasoningStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ArgViewsRepository.ArgumentReasoning.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.ArgumentReasoning.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.ArgumentReasoning.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.ArgumentReasoning.Properties.description) {
					return createDescriptionText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.ArgumentReasoning.Properties.content) {
					return createContentText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported) {
					return createToBeSupportedText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated) {
					return createToBeInstantiatedText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.ArgumentReasoning.Properties.hasStructure) {
					return createHasStructureReferencesTable(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(ArgMessages.ArgumentReasoningPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.ArgumentReasoning.Properties.id, ArgMessages.ArgumentReasoningPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentReasoningPropertiesEditionPartForm.this,
							ArgViewsRepository.ArgumentReasoning.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									ArgViewsRepository.ArgumentReasoning.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentReasoningPropertiesEditionPartForm.this, ArgViewsRepository.ArgumentReasoning.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, ArgViewsRepository.ArgumentReasoning.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.ArgumentReasoning.Properties.id, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.ArgumentReasoning.Properties.name, ArgMessages.ArgumentReasoningPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentReasoningPropertiesEditionPartForm.this,
							ArgViewsRepository.ArgumentReasoning.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									ArgViewsRepository.ArgumentReasoning.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentReasoningPropertiesEditionPartForm.this, ArgViewsRepository.ArgumentReasoning.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, ArgViewsRepository.ArgumentReasoning.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.ArgumentReasoning.Properties.name, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.ArgumentReasoning.Properties.description, ArgMessages.ArgumentReasoningPropertiesEditionPart_DescriptionLabel);
		description = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		description.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentReasoningPropertiesEditionPartForm.this,
							ArgViewsRepository.ArgumentReasoning.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									ArgViewsRepository.ArgumentReasoning.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		description.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentReasoningPropertiesEditionPartForm.this, ArgViewsRepository.ArgumentReasoning.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}
		});
		EditingUtils.setID(description, ArgViewsRepository.ArgumentReasoning.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.ArgumentReasoning.Properties.description, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createContentText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.ArgumentReasoning.Properties.content, ArgMessages.ArgumentReasoningPropertiesEditionPart_ContentLabel);
		content = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		content.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData contentData = new GridData(GridData.FILL_HORIZONTAL);
		content.setLayoutData(contentData);
		content.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentReasoningPropertiesEditionPartForm.this,
							ArgViewsRepository.ArgumentReasoning.Properties.content,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, content.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									ArgViewsRepository.ArgumentReasoning.Properties.content,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, content.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		content.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentReasoningPropertiesEditionPartForm.this, ArgViewsRepository.ArgumentReasoning.Properties.content, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, content.getText()));
				}
			}
		});
		EditingUtils.setID(content, ArgViewsRepository.ArgumentReasoning.Properties.content);
		EditingUtils.setEEFtype(content, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.ArgumentReasoning.Properties.content, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createContentText

		// End of user code
		return parent;
	}

	
	protected Composite createToBeSupportedText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported, ArgMessages.ArgumentReasoningPropertiesEditionPart_ToBeSupportedLabel);
		toBeSupported = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		toBeSupported.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData toBeSupportedData = new GridData(GridData.FILL_HORIZONTAL);
		toBeSupported.setLayoutData(toBeSupportedData);
		toBeSupported.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentReasoningPropertiesEditionPartForm.this,
							ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, toBeSupported.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, toBeSupported.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		toBeSupported.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentReasoningPropertiesEditionPartForm.this, ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, toBeSupported.getText()));
				}
			}
		});
		EditingUtils.setID(toBeSupported, ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported);
		EditingUtils.setEEFtype(toBeSupported, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createToBeSupportedText

		// End of user code
		return parent;
	}

	
	protected Composite createToBeInstantiatedText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated, ArgMessages.ArgumentReasoningPropertiesEditionPart_ToBeInstantiatedLabel);
		toBeInstantiated = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		toBeInstantiated.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData toBeInstantiatedData = new GridData(GridData.FILL_HORIZONTAL);
		toBeInstantiated.setLayoutData(toBeInstantiatedData);
		toBeInstantiated.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentReasoningPropertiesEditionPartForm.this,
							ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, toBeInstantiated.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, toBeInstantiated.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentReasoningPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		toBeInstantiated.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentReasoningPropertiesEditionPartForm.this, ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, toBeInstantiated.getText()));
				}
			}
		});
		EditingUtils.setID(toBeInstantiated, ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated);
		EditingUtils.setEEFtype(toBeInstantiated, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createToBeInstantiatedText

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createHasStructureReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.hasStructure = new ReferencesTable(getDescription(ArgViewsRepository.ArgumentReasoning.Properties.hasStructure, ArgMessages.ArgumentReasoningPropertiesEditionPart_HasStructureLabel), new ReferencesTableListener	() {
			public void handleAdd() { addHasStructure(); }
			public void handleEdit(EObject element) { editHasStructure(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveHasStructure(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromHasStructure(element); }
			public void navigateTo(EObject element) { }
		});
		this.hasStructure.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.ArgumentReasoning.Properties.hasStructure, ArgViewsRepository.FORM_KIND));
		this.hasStructure.createControls(parent, widgetFactory);
		this.hasStructure.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentReasoningPropertiesEditionPartForm.this, ArgViewsRepository.ArgumentReasoning.Properties.hasStructure, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData hasStructureData = new GridData(GridData.FILL_HORIZONTAL);
		hasStructureData.horizontalSpan = 3;
		this.hasStructure.setLayoutData(hasStructureData);
		this.hasStructure.disableMove();
		hasStructure.setID(ArgViewsRepository.ArgumentReasoning.Properties.hasStructure);
		hasStructure.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createHasStructureReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addHasStructure() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(hasStructure.getInput(), hasStructureFilters, hasStructureBusinessFilters,
		"hasStructure", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentReasoningPropertiesEditionPartForm.this, ArgViewsRepository.ArgumentReasoning.Properties.hasStructure,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				hasStructure.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveHasStructure(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentReasoningPropertiesEditionPartForm.this, ArgViewsRepository.ArgumentReasoning.Properties.hasStructure, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		hasStructure.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromHasStructure(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentReasoningPropertiesEditionPartForm.this, ArgViewsRepository.ArgumentReasoning.Properties.hasStructure, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		hasStructure.refresh();
	}

	/**
	 * 
	 */
	protected void editHasStructure(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				hasStructure.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.ArgumentReasoning.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(ArgMessages.ArgumentReasoning_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.ArgumentReasoning.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(ArgMessages.ArgumentReasoning_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.ArgumentReasoning.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(ArgMessages.ArgumentReasoning_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#getContent()
	 * 
	 */
	public String getContent() {
		return content.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#setContent(String newValue)
	 * 
	 */
	public void setContent(String newValue) {
		if (newValue != null) {
			content.setText(newValue);
		} else {
			content.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.ArgumentReasoning.Properties.content);
		if (eefElementEditorReadOnlyState && content.isEnabled()) {
			content.setEnabled(false);
			content.setToolTipText(ArgMessages.ArgumentReasoning_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !content.isEnabled()) {
			content.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#getToBeSupported()
	 * 
	 */
	public String getToBeSupported() {
		return toBeSupported.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#setToBeSupported(String newValue)
	 * 
	 */
	public void setToBeSupported(String newValue) {
		if (newValue != null) {
			toBeSupported.setText(newValue);
		} else {
			toBeSupported.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported);
		if (eefElementEditorReadOnlyState && toBeSupported.isEnabled()) {
			toBeSupported.setEnabled(false);
			toBeSupported.setToolTipText(ArgMessages.ArgumentReasoning_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !toBeSupported.isEnabled()) {
			toBeSupported.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#getToBeInstantiated()
	 * 
	 */
	public String getToBeInstantiated() {
		return toBeInstantiated.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#setToBeInstantiated(String newValue)
	 * 
	 */
	public void setToBeInstantiated(String newValue) {
		if (newValue != null) {
			toBeInstantiated.setText(newValue);
		} else {
			toBeInstantiated.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated);
		if (eefElementEditorReadOnlyState && toBeInstantiated.isEnabled()) {
			toBeInstantiated.setEnabled(false);
			toBeInstantiated.setToolTipText(ArgMessages.ArgumentReasoning_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !toBeInstantiated.isEnabled()) {
			toBeInstantiated.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#initHasStructure(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initHasStructure(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		hasStructure.setContentProvider(contentProvider);
		hasStructure.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.ArgumentReasoning.Properties.hasStructure);
		if (eefElementEditorReadOnlyState && hasStructure.getTable().isEnabled()) {
			hasStructure.setEnabled(false);
			hasStructure.setToolTipText(ArgMessages.ArgumentReasoning_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !hasStructure.getTable().isEnabled()) {
			hasStructure.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#updateHasStructure()
	 * 
	 */
	public void updateHasStructure() {
	hasStructure.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#addFilterHasStructure(ViewerFilter filter)
	 * 
	 */
	public void addFilterToHasStructure(ViewerFilter filter) {
		hasStructureFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#addBusinessFilterHasStructure(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToHasStructure(ViewerFilter filter) {
		hasStructureBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart#isContainedInHasStructureTable(EObject element)
	 * 
	 */
	public boolean isContainedInHasStructureTable(EObject element) {
		return ((ReferencesTableSettings)hasStructure.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ArgMessages.ArgumentReasoning_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
