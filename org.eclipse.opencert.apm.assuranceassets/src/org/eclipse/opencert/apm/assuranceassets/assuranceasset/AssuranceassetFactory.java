/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage
 * @generated
 */
public interface AssuranceassetFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AssuranceassetFactory eINSTANCE = org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Assurance Assets Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assurance Assets Model</em>'.
	 * @generated
	 */
	AssuranceAssetsModel createAssuranceAssetsModel();

	/**
	 * Returns a new object of class '<em>Manageable Assurance Asset</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Manageable Assurance Asset</em>'.
	 * @generated
	 */
	ManageableAssuranceAsset createManageableAssuranceAsset();

	/**
	 * Returns a new object of class '<em>Assurance Asset Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assurance Asset Event</em>'.
	 * @generated
	 */
	AssuranceAssetEvent createAssuranceAssetEvent();

	/**
	 * Returns a new object of class '<em>Assurance Asset Evaluation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assurance Asset Evaluation</em>'.
	 * @generated
	 */
	AssuranceAssetEvaluation createAssuranceAssetEvaluation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AssuranceassetPackage getAssuranceassetPackage();

} //AssuranceassetFactory
