/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.externaltools.api.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.eclipse.opencert.externaltools.api.ExternalToolQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ExternalToolQueryDAOImpl
        implements ExternalToolQueryDAO
{
    @Autowired
    @Qualifier("hibernate4AnnotatedSessionFactory")
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void save(ExternalToolQuery externalToolQuery)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(externalToolQuery);
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public List<ExternalToolQuery> list()
    {
        Session session = this.sessionFactory.getCurrentSession();
        List<ExternalToolQuery> externalToolQueryList = session.createQuery("from ExternalToolQuery").list();

        return externalToolQueryList;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public ExternalToolQuery findRecentByBaseElementId(long id)
    {
        Session session = this.sessionFactory.getCurrentSession();

        Query query = session
                .createQuery("from ExternalToolQuery e left join fetch e.externalToolQueryResults left join fetch e.externalToolQuerySettings where e.baseElementId = :id order by e.editDate desc");
        query.setParameter("id", id);

        List<ExternalToolQuery> externalToolQueryList = query.list();

        return externalToolQueryList.size() > 0 ? externalToolQueryList.get(0) : null;
    }

    @Transactional
    @Override
    public void delete(long id)
    {
        Session session = this.sessionFactory.getCurrentSession();

        session.delete(session.get(ExternalToolQuery.class, id));
    }
}
