/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package claimtypes.impl;

import claimtypes.ClaimType;
import claimtypes.ClaimtypesPackage;
import claimtypes.SentenceStructure;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Claim Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link claimtypes.impl.ClaimTypeImpl#getSentenceStructures <em>Sentence Structures</em>}</li>
 *   <li>{@link claimtypes.impl.ClaimTypeImpl#getColor <em>Color</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClaimTypeImpl extends DescribableElementImpl implements ClaimType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClaimTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClaimtypesPackage.Literals.CLAIM_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SentenceStructure> getSentenceStructures() {
		return (EList<SentenceStructure>)eGet(ClaimtypesPackage.Literals.CLAIM_TYPE__SENTENCE_STRUCTURES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getColor() {
		return (String)eGet(ClaimtypesPackage.Literals.CLAIM_TYPE__COLOR, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(String newColor) {
		eSet(ClaimtypesPackage.Literals.CLAIM_TYPE__COLOR, newColor);
	}

} //ClaimTypeImpl
