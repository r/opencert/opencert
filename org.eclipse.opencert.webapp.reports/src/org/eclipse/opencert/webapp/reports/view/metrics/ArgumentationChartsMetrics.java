/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;


import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Container.Hierarchical;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.view.metrics.JFreeChartWrapper;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.AssertedEvidence;
import org.eclipse.opencert.sam.arg.arg.AssertedInference;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.InformationElementType;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;





@Theme("opencerttheme")
public class ArgumentationChartsMetrics 
	extends CustomComponent

{


	private static final long serialVersionUID = -6440221725845075366L;
	private static final String TOTAL_CLAIMS = "Number of All Claims";
	private static final String SAFETY_CLAIMS = "Number of Public Safety Claims";
	private static final String UNINSTANTIATED = "Number of Uninstantiated Safety Claims in a Safety Case";
	private static final String UNDEVELOPED = "Number of Undeveloped Safety Claims";
	private static final String UNINSTANTIATED_AND_UNDEVELOPED = "Number of Uninstantiated and Undeveloped Safety Claims in a Safety Case";
	private static final String NO_CITATION = "Number of Claims with no Information Element Citation Associated";
	private static final String NO_DATA_AVAILABLE = "No Data Available";
	private static final String GRAPHICAL_REPR = "Graphical Representation";
	private static final String ASSUMED_CLAIMS = "Number of Assumed Safety Claims";
	private static final String CONTEXT = "Context Information Elements";
	
	private int allClaims;
	private int safetyClaims;
	private int uninstantiated;
	private int uninAndUndev;
	private int undeveloped;
	private int noCitation;
	private int assumedClaims;
	private int contextElements;

	private byte[] argumentationChart;
    private Hierarchical dataDescription;
    
    

	public String getGraphicalRepr() {
		return GRAPHICAL_REPR;
	}




	public byte[] getArgumentationChart() {
		return argumentationChart;
	}




	public Hierarchical getDataDescription() {
		return dataDescription;
	}




	public ArgumentationChartsMetrics(AssuranceProjectWrapper project)
    {      
         
        VerticalLayout mainPanel = new VerticalLayout();
		
		mainPanel.setStyleName("chartsSpacing");
		mainPanel.setSpacing(true);
		
		safetyClaims=0;
		uninstantiated=0;
		undeveloped=0;
		noCitation=0;
		uninAndUndev=0;
		assumedClaims=0;
		contextElements=0;
		

		getArgumentationData(project);
		mainPanel.addComponent(argumentationProcessMetrics());
		mainPanel.addComponent(graphDataMetric());

		
		//add mainPanel component
		setCompositionRoot(mainPanel);
		setSizeFull();
        
 
    }



	
	private Component graphDataMetric() {
    	final JFreeChart chart = ChartFactory.createBarChart(
    			GRAPHICAL_REPR,      // chart title
                "Types",               // domain axis label
                "Claims", // range axis label
                populateWithData(),                  // data
                PlotOrientation.HORIZONTAL, // orientation
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
            );

    	// get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED);  
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        // disable bar outlines...
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        //number in bars
        renderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
        renderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));


        //With this uncommented all the name is visualized but in a ugly way..
        /*
        final CategoryAxis axis = plot.getDomainAxis();
        axis.setCategoryLabelPositions(
            CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 180.0)
        );
    	*/
      
     


        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
		
        try {
			argumentationChart = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
		Component chartWrapper = new JFreeChartWrapper(chart);
		chartWrapper.setHeight("80%");
		chartWrapper.setWidth("80%");
		chartWrapper.setStyleName("leftChartsSpacing");
		return chartWrapper;
	}
	
	
	private CategoryDataset populateWithData() {
		
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	
		dataset.addValue(allClaims-safetyClaims, "Private Claims", SAFETY_CLAIMS.replaceAll("Number of ", ""));
		dataset.addValue(safetyClaims, "Public Claims", SAFETY_CLAIMS.replaceAll("Number of ", ""));
	
		dataset.addValue(allClaims-assumedClaims, "Unassumed Claims", ASSUMED_CLAIMS.replaceAll("Number of ", ""));
		dataset.addValue(assumedClaims, "Assumed Claims", ASSUMED_CLAIMS.replaceAll("Number of ", ""));
	
		
		dataset.addValue(allClaims-uninstantiated, "Instantiated Safety Claims", UNINSTANTIATED.replaceAll("Number of ", ""));
		dataset.addValue(uninstantiated, "Uninstantiated Safety Claims", UNINSTANTIATED.replaceAll("Number of ", ""));
	
		dataset.setValue( allClaims-undeveloped, "Developed Safety Claims", UNDEVELOPED.replaceAll("Number of ", ""));
		dataset.setValue( undeveloped, "Undeveloped Safety Claims", UNDEVELOPED.replaceAll("Number of ", ""));
		
		dataset.setValue(allClaims-noCitation, "With Information Citation Associated", NO_CITATION.replaceAll("Number of ", ""));
		dataset.setValue(noCitation, "No Information Citation Associated", NO_CITATION.replaceAll("Number of ", ""));
		
		dataset.setValue(contextElements, CONTEXT, CONTEXT);
		
		
		
        return dataset;
	}

    



	private void getArgumentationData(AssuranceProjectWrapper newProject) {
    	
    
    	
    	AssuranceProjectManager projectManager = 
				(AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
    	AssuranceProject project = projectManager.getProject(newProject.getId());
    	
    	if(project != null){
    		
    		EList<AssetsPackage> assetsPackages = project.getAssetsPackage();
    		for (AssetsPackage assetsPackage : assetsPackages) {
    			if (assetsPackage.isIsActive()) {
    				EList<Case> argumentationModels = assetsPackage.getArgumentationModel();
    				for (Case argumentationModel : argumentationModels) {
    					
    					int context=0;
    					EObject eArgumentationModel= (EObject) argumentationModel; 
			    		 for (Iterator<EObject> iterator = eArgumentationModel.eAllContents(); iterator.hasNext();) {
			        		 EObject aEObject = iterator.next();
			        		 if (aEObject instanceof InformationElementCitation){
			        			 InformationElementCitation iec = (InformationElementCitation) aEObject;
			        			 if(iec.getType()!=null){
				        			 InformationElementType type = iec.getType();
				        			 if(type.getName().equals("Context"))context++;
			        			 }
			        		 }
			    		 }
    					contextElements+=context;
    					buildArgumentationData(argumentationModel);
    					
    				}
    			}
    		}
    	}
    	
	}




	private void buildArgumentationData(Case argumentationModel) {
		
		int publicClaimsCount = 0;
		int toBeSupportedClaimsCount = 0;
		int toBeInstantiatedClaimsCount = 0;
		int toBeSupportedAndInstantiated=0;
		int assumed=0;
		List<Long> claimIds = new ArrayList<Long>();
		List<Long> sourceAssertedInferenceIds = new ArrayList<Long>();
		List<Long> sourceAssertedEvidenceIds = new ArrayList<Long>();
		
		EList<ArgumentElement> arguments = argumentationModel.getArgument();
		for (ArgumentElement argument : arguments) {
			if (argument instanceof Claim) {
				claimIds.add(CDOStorageUtil.getCDOId(argument));
				Claim claim = (Claim)argument;
				if (claim.getPublic()) {
					publicClaimsCount++;
				}
				if (claim.getToBeSupported()) {
					toBeSupportedClaimsCount++;
				}
				if (claim.getToBeInstantiated()) {
					toBeInstantiatedClaimsCount++;
				}
				if (claim.getToBeInstantiated()&&claim.getToBeSupported()) {
					toBeInstantiatedClaimsCount++;
				}
				if (claim.getAssumed()){
					assumed++;
				}
			} else if (argument instanceof AssertedInference) {
				buildSourceAssertedInferenceIds(sourceAssertedInferenceIds, argument);
			} else if (argument instanceof AssertedEvidence) {
				buildSourceAssertedEvidenceIds(sourceAssertedEvidenceIds, argument);
			}
		}
		int claimsWithNoEvidenceAssociated = countClaimsWithNoEvidenceAssociated(claimIds, sourceAssertedInferenceIds, sourceAssertedEvidenceIds);
		
		
		
		
		safetyClaims += publicClaimsCount;
		uninstantiated += toBeInstantiatedClaimsCount;
		undeveloped += toBeSupportedClaimsCount;
		noCitation+=claimsWithNoEvidenceAssociated;
		uninAndUndev+=toBeSupportedAndInstantiated;
		allClaims+=claimIds.size();
		assumedClaims+=assumed;
	}

	
	private void buildSourceAssertedInferenceIds(List<Long> sourceAssertedInferenceIds, ArgumentElement argument) {
		AssertedInference assertedInf = (AssertedInference)argument;
		EList<ArgumentationElement> sources = assertedInf.getSource();
		EList<ArgumentationElement> targets = assertedInf.getTarget();
		if (targets != null && targets.size() > 0) {
			for (ArgumentationElement source: sources) {
				long sourceId = CDOStorageUtil.getCDOId(source);
				if (!sourceAssertedInferenceIds.contains(sourceId)) {
					sourceAssertedInferenceIds.add(sourceId);
				}
			}
		}
	}
	
	private void buildSourceAssertedEvidenceIds(List<Long> sourceAssertedEvidenceIds, ArgumentElement argument) {
		AssertedEvidence assertedEvid = (AssertedEvidence)argument;
		EList<ArgumentElement> sources = assertedEvid.getSource();
		EList<ArgumentElement> targets = assertedEvid.getTarget();
		if (targets != null && targets.size() > 0) {
			for (ArgumentElement source: sources) {
				long sourceId = CDOStorageUtil.getCDOId(source);
				if (!sourceAssertedEvidenceIds.contains(sourceId)) {
					sourceAssertedEvidenceIds.add(sourceId);
				}
			}
		}
	}

	private int countClaimsWithNoEvidenceAssociated(List<Long> claimsIds, List<Long> sourceAssertedInferenceIds, List<Long> sourceAssertedEvidenceIds) {
		int claimsWithoutEvidencesCount = 0;
		for (Long claimsId : claimsIds) {
			if (!sourceAssertedInferenceIds.contains(claimsId) && !sourceAssertedEvidenceIds.contains(claimsId)) {
				claimsWithoutEvidencesCount++;
			}
		}
	
		return claimsWithoutEvidencesCount;
	}


	private Component argumentationProcessMetrics() {

		
		TreeTable treetable = new TreeTable("");
		treetable.setWidth("100%");
		treetable.addStyleName("resourceEfficiencyTable");

		// Define two columns for the built-in container
		treetable.addContainerProperty("Type", String.class, "");
		treetable.addContainerProperty("Value",  String.class, "");

		Object total = treetable.addItem(new Object[]{TOTAL_CLAIMS, Integer.toString(allClaims)},null); 
		Object safety = treetable.addItem(new Object[]{SAFETY_CLAIMS, Integer.toString(safetyClaims)},null); 
		Object assum = treetable.addItem(new Object[]{ASSUMED_CLAIMS, Integer.toString(assumedClaims)},null);
		Object unin = treetable.addItem(new Object[]{UNINSTANTIATED, Integer.toString(uninstantiated)},null); 
		Object undev = treetable.addItem(new Object[]{UNDEVELOPED, Integer.toString(undeveloped)},null); 
		Object uninUndev = treetable.addItem(new Object[]{UNINSTANTIATED_AND_UNDEVELOPED, Integer.toString(uninAndUndev)},null); 
		Object noCit = treetable.addItem(new Object[]{NO_CITATION, Integer.toString(noCitation)},null); 
		Object cntx = treetable.addItem(new Object[]{CONTEXT, Integer.toString(contextElements)},null); 
		

		treetable.setChildrenAllowed(total, false);
		treetable.setChildrenAllowed(safety, false);
		treetable.setChildrenAllowed(assum, false);
		treetable.setChildrenAllowed(unin, false);
		treetable.setChildrenAllowed(undev, false);
		treetable.setChildrenAllowed(uninUndev, false);
		treetable.setChildrenAllowed(noCit, false);
		treetable.setChildrenAllowed(cntx, false);
			
		treetable.setPageLength(8);
		
		dataDescription = treetable.getContainerDataSource();
		return treetable;
		
		
	}




	




	 
	



	
}

