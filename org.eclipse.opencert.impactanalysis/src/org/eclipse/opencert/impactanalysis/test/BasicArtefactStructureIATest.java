/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.test;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.impactanalysis.ImpactAnalyser;
import org.eclipse.opencert.impactanalysis.ImpactSource;
import org.eclipse.opencert.impactanalysis.relations.IArtefactRelationImpact;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.executors.TemplatedSimpleInTransactionExecutor;
import org.eclipse.opencert.storage.cdo.property.OpencertPropertiesReader;

public class BasicArtefactStructureIATest
{
    public static void main(String[] args)
    {
        List<ImpactSource> impactSources = new ArrayList<ImpactSource>();
        
        impactSources.add(createImpactSource("Artefact000", EventKind.MODIFICATION));
        //impactSources.add(createImpactSource("Artefact00", EventKind.MODIFICATION));
        //impactSources.add(createImpactSource("ArtefactA", EventKind.MODIFICATION));
//        impactSources.add(createImpactSource("ArtefactBB", EventKind.MODIFICATION));
//        impactSources.add(createImpactSource("ArtefactCC", EventKind.MODIFICATION));
        
        //impactSources.add(createImpactSource("RootArtefact_test_single", EventKind.MODIFICATION));
        
        
        System.out.println("----- IMPACT ANALYSIS VISUALIZATION -----");
        System.out.print("rootImpactingSources: ");
        for (ImpactSource impactSource : impactSources) {
            System.out.print("(");
            System.out.print(impactSource.getImpactingArtefactCdoId());
            System.out.print(", ");
            System.out.print(impactSource.getImpactingEventKind());
            System.out.print("), ");
        }
        System.out.println("-----------------------------------------\n\n");
        
        
        String result = performIASimulation(impactSources);
        
        System.out.println(result);
    }
    
    
    private static String performIASimulation(final List<ImpactSource> impactSources)
    {
        
            
        String result = null;     
        ImpactAnalyser impactAnalyser = new ImpactAnalyser(OpencertPropertiesReader.getInstance());
        try {
            
            //perform impact analysis for given artefact and EventKind on it
            List<IArtefactRelationImpact> artefactsRelationImpacts =
                    impactAnalyser.listArtefactsRelationImpacts(impactSources);
            
            //retrieve basic textual visualization of impact
            result = impactAnalyser.visualizeArtefactsRelationImpacts(artefactsRelationImpacts);
            
            //execute all artefactsRelationImpacts <- in the future we can do it selectively, based on user decisions in dialog box
            impactAnalyser.executeArtefactsRelationImpacts(artefactsRelationImpacts);
        
        } finally {
            impactAnalyser.close();
        }

        return result;
    }


    private static ImpactSource createImpactSource(
            final String rootImpactingArtefact, EventKind rootEventKind)
    {
        Long initialArtefactCDOId = 
            new TemplatedSimpleInTransactionExecutor<Long>()
            {
                @Override
                public Long executeInTransaction(CDOTransaction cdoTransaction)
                {
                    
                    CDOQuery query = cdoTransaction.createQuery("sql", "select * from evidence_artefact where name ilike '" + rootImpactingArtefact + "'"
                            + CDOStorageUtil.getMandatoryCDOQuerySuffix());
                    Artefact artefact = query.getResultValue(Artefact.class);
                    if (artefact == null) {
                        throw new RuntimeException("Root impacting artefact doesn't exist in database");
                    }
                    
                    return CDOStorageUtil.getCDOId(artefact);
                }
            }.executeReadOnlyOperation();
            
        ImpactSource rootImpactSource = new ImpactSource(initialArtefactCDOId, rootEventKind);
        return rootImpactSource;
    }
}
