/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.aida.tracemodel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.capra.core.CapraException;
import org.eclipse.capra.core.adapters.TraceLinkAdapter;
import org.eclipse.capra.core.util.TraceLinkAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelFactory;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.polarsys.chess.contracts.profile.chesscontract.Contract;

/**
 * This adapter provides access to ContractAgreementLink traces.
 * 
 * @author Stefano Puri
 */
public class ContractAgreementLinkAdapter implements TraceLinkAdapter {

	public static final String CONTRACT_AGREEMENT_LINK_TYPE = "Contract-Agreement Trace";

	@Override
	public boolean canAdapt(EClass traceType) {
		if (traceType.equals(OpenCertTraceLinkMetaModelPackage.eINSTANCE.getContractAgreementLink())) {
			return true;
		}
		return false;
	}

	@Override
	public List<EObject> getSources(EObject trace) {
		assert trace instanceof ContractAgreementLink;
		ContractAgreementLink contractTraceLink = (ContractAgreementLink) trace;
		List<EObject> contracts = new ArrayList<EObject>();
		contracts.add(contractTraceLink.getSources());
		return contracts;
	}

	@Override
	public List<EObject> getTargets(EObject trace) {
		assert trace instanceof ContractAgreementLink;
		ContractAgreementLink genericTraceLink = (ContractAgreementLink) trace;
		List<EObject> claims = new ArrayList<EObject>();
		for (Agreement c : genericTraceLink.getTargets()) {
			claims.add(c);
		}
		return claims;
	}

	@Override
	public String getLinkType() {
		return CONTRACT_AGREEMENT_LINK_TYPE;
	}

	@Override
	public boolean canCreateLink(List<EObject> sources, List<EObject> targets) {
		if (sources.isEmpty() || targets.isEmpty())
			return false;
		for (EObject obj : sources) {
			if (!(obj instanceof Contract))
				return false;
		}
		for (EObject obj : targets) {
			if (!(obj instanceof Agreement))
				return false;
		}
		return true;
	}

	public EObject createLink(List<EObject> sources, List<EObject> targets) {
		ContractAgreementLink traceLink = OpenCertTraceLinkMetaModelFactory.eINSTANCE.createContractAgreementLink();
		traceLink.setSources((Contract) sources.get(0));
		for (EObject obj : targets) {
			traceLink.getTargets().add((Agreement) obj);
		}

		return traceLink;
	}

	@Override
	public EObject createLink(List<EObject> sources, List<EObject> targets, List<TraceLinkAttribute> attributes)
			throws CapraException {
		return this.createLink(sources, targets);
	}

	@Override
	public List<TraceLinkAttribute> getAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLabelForEObject(EObject eobject) {
		// TODO Auto-generated method stub
		return null;
	}
}
