/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.ui.feedback;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.URLTransfer;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * Handle drops of URLs from outside or inside globally.
 * 
 * @author mauersberger
 */
public class AnyURLDropHandler extends DropTargetAdapter {

	@Override
	public void drop(DropTargetEvent event) {
		// double check
		if (!URLTransfer.getInstance().isSupportedType(event.currentDataType)) {
			return;
		}

		try {
			String urlString = (String) URLTransfer.getInstance().nativeToJava(event.currentDataType);
			drop(urlString);
		} catch (Exception exception) {
			// TODO: handle exception
			exception.printStackTrace();
			event.widget.getDisplay().beep();
		}
	}

	/**
	 * Handle any given dropped URL.
	 *
	 * @param urlString
	 * @throws Exception
	 */
	public void drop(String urlString) throws Exception {
		URL url = new URL(urlString);
		drop(url);
	}

	/**
	 * Handle any given dropped URL.
	 *
	 * @param url
	 * @throws Exception
	 */
	public void drop(URL url) throws Exception {
		// now check of what type the URI is
		if (isResourceURI(url)) {
			handleResourceURI(url);
			return;
		}

		// TODO Search all projects

		// TODO Search all editors and views

		// TODO Search through all Subversion locations?

		// TODO Search through tasks maybe?
	}

	/*
	 * Handle plain EMF resource URI.
	 */
	private void handleResourceURI(URL url) {
		// TODO Search an open view that has the object
		IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
		for (IWorkbenchWindow window : windows) {
			// this is just to consider the active one FIRST
			IWorkbenchPage activePage = window.getActivePage();
			IWorkbenchPage[] pages = window.getPages();
			for (IWorkbenchPage page : pages) {
				// this is just to consider the active one FIRST
				IWorkbenchPart activePart = page.getActivePart();

				IViewReference[] viewReferences = page.getViewReferences();
				for (IViewReference vr : viewReferences) {
					IViewPart view = vr.getView(false);
					if (view == null) {
						continue;
					}

					// now check the input

				}
			}
		}
	}

	@Override
	public void dragEnter(DropTargetEvent event) {
		event.detail = DND.DROP_LINK;

		for (int i = 0; i < event.dataTypes.length; i++) {
			if (URLTransfer.getInstance().isSupportedType(event.dataTypes[i])) {
				event.currentDataType = event.dataTypes[i];
				break;
			}
		}
	}

	private boolean isResourceURI(URL url) {
		try {
			URI uri = URI.createURI(url.toString());
			return uri.isPlatformResource();
		} catch (Exception exception) {
			return false;
		}
	}
}
