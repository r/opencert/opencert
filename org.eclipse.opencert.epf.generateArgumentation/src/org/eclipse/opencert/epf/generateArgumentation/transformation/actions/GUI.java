/*******************************************************************************
 * Copyright (c) 2018, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *   Faiz Ul Muram
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.epf.generateArgumentation.transformation.actions;

import java.util.HashMap;

import org.eclipse.emf.cdo.dawn.ui.composites.CDOResourceNodeSelectionWidget;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class GUI extends Dialog{
	
	CDOResourceFolder assuranceprojectFolder;
	CDOResourceFolder argFolder;
	
	HashMap<Object, Object> options = new HashMap<Object, Object>();

	private Label argLabel;
	private String resourceName;
	private CDOResourceNodeSelectionWidget chooser;
	private Shell shell;
	
	public GUI(Shell parentShell, CDOView view) {
		super(parentShell);
		this.shell=parentShell;
	}
	public GUI(IShellProvider parentShell) {
		super(parentShell);
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
	    super.createButtonsForButtonBar(parent);

	    Button ok = getButton(IDialogConstants.OK_ID);
	    setButtonLayoutData(ok);

	    chooser.addSelectionChangedListener(new ISelectionChangedListener(){

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				if(event.getSelection() instanceof IStructuredSelection){

					IStructuredSelection selection = (IStructuredSelection) event.getSelection();
					Object element = selection.getFirstElement();
					if(element instanceof CDOResourceNode){
						resourceName = ((CDOResourceNode)element).getName();
					}
				}
			}
			
		});
	    
	    Button cancel = getButton(IDialogConstants.CANCEL_ID);
	    
	    setButtonLayoutData(cancel);
	 }
	
	@Override
	protected Control createDialogArea(final Composite parent) {

		Composite contents = (Composite) super.createDialogArea(parent);
		
		GridData contentsGridData2 = (GridData) contents.getLayoutData();
		contentsGridData2.horizontalAlignment = SWT.FILL;
		contentsGridData2.verticalAlignment = SWT.FILL;
		contentsGridData2.minimumHeight = 300;
		contentsGridData2.minimumWidth = 300;
		
		argLabel = new Label(contents, SWT.NONE);
		argLabel.setText("Please select your assurance project folder:");
		GridData argLabelGridData = new GridData();		
		argLabelGridData.horizontalAlignment = SWT.FILL;
		argLabelGridData.verticalAlignment = SWT.FILL;
		GridLayout layoutLabel = new GridLayout();
		layoutLabel.marginHeight = 20;
		layoutLabel.marginWidth = 0;
		argLabel.setLayoutData(argLabelGridData);
		
		GridLayout layout = new GridLayout();
		layout.marginHeight = 5;
		layout.marginWidth = 0;
			
		chooser = new CDOResourceNodeSelectionWidget(contents,SWT.FILL);
		chooser.setLayoutData(contentsGridData2);
		chooser.setLayout(layout);
		
		contents.setData(chooser);
		
		return contents;
	}

	public String resourceName(){
		return resourceName;
	}
	
	@Override
	protected void cancelPressed(){
		MessageDialog.openError(shell, "Selection error", "An assurance project must be selected in order to perform the transformation"); 
	}
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Assurance project selection");
	}
}
