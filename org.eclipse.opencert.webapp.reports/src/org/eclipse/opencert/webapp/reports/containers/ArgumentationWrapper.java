/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

import com.vaadin.ui.Button;
import com.vaadin.ui.Label;

public class ArgumentationWrapper {
	private Label caseResource;
	private Button claimsCount;
	private Button publicClaimsCount;
	private Button argumentationCount;
	private Button argumentElementCitationCount;
	private Button toBeSupportedClaimsCount;
	private Button toBeInstantiatedClaimsCount;
	private Button claimsWithNoEvidenceAssociatedCount;
	
	public ArgumentationWrapper() {
	}
	
	public ArgumentationWrapper(Label caseResource, Button claimsCount, Button publicClaimsCount, Button argumentationCount, 
			Button argumentElementCitationCount, Button toBeSupportedClaimsCount, Button toBeInstantiatedClaimsCount,
			Button claimsWithNoEvidenceAssociatedCount) {
		this.caseResource = caseResource;
		this.claimsCount = claimsCount;
		this.publicClaimsCount = publicClaimsCount;
		this.argumentationCount = argumentationCount;
		this.argumentElementCitationCount = argumentElementCitationCount;
		this.toBeSupportedClaimsCount = toBeSupportedClaimsCount;
		this.toBeInstantiatedClaimsCount = toBeInstantiatedClaimsCount;
		this.claimsWithNoEvidenceAssociatedCount = claimsWithNoEvidenceAssociatedCount;
	}
	
	public Label getCaseResource() {
		return caseResource;
	}

	public void setCaseResource(Label caseResource) {
		this.caseResource = caseResource;
	}
	
	public Button getClaimsCount() {
		return claimsCount;
	}

	public void setClaimsCount(Button claimsCount) {
		this.claimsCount = claimsCount;
	}
	
	public Button getPublicClaimsCount() {
		return publicClaimsCount;
	}

	public void setPublicClaimsCount(Button publicClaimsCount) {
		this.publicClaimsCount = publicClaimsCount;
	}
	
	public Button getArgumentationCount() {
		return argumentationCount;
	}

	public void setArgumentationCount(Button argumentationCount) {
		this.argumentationCount = argumentationCount;
	}
	
	public Button getArgumentElementCitationCount() {
		return argumentElementCitationCount;
	}

	public void setArgumentElementCitationCount(Button argumentElementCitationCount) {
		this.argumentElementCitationCount = argumentElementCitationCount;
	}
	
	public Button getToBeSupportedClaimsCount() {
		return toBeSupportedClaimsCount;
	}

	public void setToBeSupportedClaimsCount(Button toBeSupportedClaimsCount) {
		this.toBeSupportedClaimsCount = toBeSupportedClaimsCount;
	}
	
	public Button getToBeInstantiatedClaimsCount() {
		return toBeInstantiatedClaimsCount;
	}

	public void setToBeInstantiatedClaimsCount(Button toBeInstantiatedClaimsCount) {
		this.toBeInstantiatedClaimsCount = toBeInstantiatedClaimsCount;
	}
	
	public Button getClaimsWithNoEvidenceAssociatedCount() {
		return claimsWithNoEvidenceAssociatedCount;
	}

	public void setClaimsWithNoEvidenceAssociatedCount(Button claimsWithNoEvidenceAssociatedCount) {
		this.claimsWithNoEvidenceAssociatedCount = claimsWithNoEvidenceAssociatedCount;
	}
}
