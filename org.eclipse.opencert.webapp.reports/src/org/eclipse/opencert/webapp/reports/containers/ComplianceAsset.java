/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

import java.util.Collection;
import java.util.List;

import org.eclipse.opencert.webapp.reports.manager.compliance.IAEvaluation;

public class ComplianceAsset {
	private String   id;
	private String name;
	private String description;
	private String type;
	private Long cdoId;
	private Collection<ComplianceAssetProperty> complianceAssetProperties;
	private Collection<ComplianceAssetEvaluation> complianceAssetEvaluations;
	private Collection<ComplianceAssetResource> complianceAssetResources;
    private IAEvaluation iaEvaluation;
	
	public ComplianceAsset(String id, String name, String description, String type, Long cdoId, List<ComplianceAssetProperty> complianceAssetProperties,
			List<ComplianceAssetEvaluation> complianceAssetEvaluations, List<ComplianceAssetResource> complianceAssetResources, IAEvaluation iaEvaluation) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.type = type;
		this.cdoId = cdoId;
		this.complianceAssetProperties = complianceAssetProperties;
		this.complianceAssetEvaluations = complianceAssetEvaluations;
		this.complianceAssetResources = complianceAssetResources;
		this.iaEvaluation = iaEvaluation;
	}

	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Long getCdoId() {
		return this.cdoId;
	}
	
	public void setCdoId(Long cdoId) {
		this.cdoId = cdoId;
	}
		
	public Collection<ComplianceAssetProperty> getComplianceAssetProperties() {
		return this.complianceAssetProperties;
	}
	
	public void setComplianceAssetProperties(Collection<ComplianceAssetProperty> complianceAssetProperties) {
		this.complianceAssetProperties = complianceAssetProperties;
	}
	
	public Collection<ComplianceAssetEvaluation> getComplianceAssetEvaluations() {
		return this.complianceAssetEvaluations;
	}
	
	public void setComplianceAssetEvaluations(Collection<ComplianceAssetEvaluation> complianceAssetEvaluations) {
		this.complianceAssetEvaluations = complianceAssetEvaluations;
	}
	
	public Collection<ComplianceAssetResource> getComplianceAssetResources() {
		return this.complianceAssetResources;
	}
	
	public void setComplianceAssetResources(Collection<ComplianceAssetResource> complianceAssetResources) {
		this.complianceAssetResources = complianceAssetResources;
	}

    public IAEvaluation getIaEvaluation()
    {
        return iaEvaluation;
    }
	
}
