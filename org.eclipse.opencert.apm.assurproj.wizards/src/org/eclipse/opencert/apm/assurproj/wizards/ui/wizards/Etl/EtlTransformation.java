/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards.ui.wizards.Etl;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.epsilon.common.parse.problem.ParseProblem;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelElementTypeNotFoundException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;
import org.eclipse.epsilon.etl.EtlModule;
import org.eclipse.epsilon.etl.IEtlModule;
import org.eclipse.epsilon.etl.dom.TransformationRule;
import org.eclipse.epsilon.etl.trace.Transformation;
import org.eclipse.opencert.apm.assurproj.wizards.Activator;




public class EtlTransformation {

	private static final String UTF_8 = "UTF-8";
	
	private String etlTransformation;
	private String sourceName;
	private String sourceMetaModelURI;
	//MCP
	private Object result;
	//CRF problem	
	//private Object oldEPackage;
	private Object IoldEPackage;
	private Object OoldEPackage;	
	//CRF problem	
	private String sourceMetaModelFilePath;
	//MCP
	private String sourceModelFilePath;
	private boolean sourceReadOnLoad;
	private boolean sourceStoreOnDisposal;
	private String targetName;
	private String targetMetaModelURI;
	//CRF problem	
	private String targetMetaModelFilePath;
	//CRF problem	
	private String targetModelFilePath;
	private boolean targetReadOnLoad;
	private boolean targetStoreOnDisposal;
	private String pluginID;
	
	public EtlTransformation(String name)
	{
	}
	  
	private EtlModule createModule() {
			return new EtlModule();
	}
	
	private void postProcess() {
			
	}
		
	private void preProcess() {
			
	}
	
	/*
	private Object execute(IEolExecutableModule module) throws EolRuntimeException {
		    return module.execute();
	}
	*/

	public void execute() throws Exception {
		URL resolvedTransformationURL = null;
		URI transformURI = null;
		String file = null;
		try {
			String spluginPath = Platform.getBundle(Activator.PLUGIN_ID).getLocation();
			RedirectSystemErr.out("spluginPath:= ." + spluginPath + ".");
			if(spluginPath.contains("plugins")) // si estoy en modo jar
			{
				String pluginPath = "configuration";
				
				URL transformationEntry = Platform.getBundle(pluginID).getEntry(
						etlTransformation);
				
				resolvedTransformationURL = FileLocator
						.resolve(transformationEntry);
				// MCP
				file = resolvedTransformationURL.getFile();
				file = file.replace(" ", "%20");// reemplazar caracter blanco
				
			    String wrong = file.substring(file.indexOf("plugins"), file.indexOf("/"+etlTransformation));
			    String file2 = file.replace(wrong, pluginPath);
			    file = file2;
			}
			else
			{	
				URL transformationEntry = Platform.getBundle(pluginID).getEntry(
						etlTransformation);
				
				resolvedTransformationURL = FileLocator
						.resolve(transformationEntry);
				// MCP
				file = "file:" + resolvedTransformationURL.getFile();
				file = file.replace(" ", "%20");// reemplazar caracter blanco
			}
			RedirectSystemErr.out("rules file:= ." + file + ".");
			//transformURI = resolvedTransformationURL.toURI();
			transformURI = new URI(file);
			// MCP

		} catch (IOException e) {
			e.printStackTrace();
			RedirectSystemErr.out("execute exception 1: "+e.toString());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			RedirectSystemErr.out("execute exception 2: "+e.toString());
		}

		// MCP
		//IEolExecutableModule etlModule = new EtlModule();
		IEtlModule etlModule = new EtlModule();
		// MCP
		try {
			etlModule.parse(transformURI);
		} catch (Exception e) {
			e.printStackTrace();
			RedirectSystemErr.out("execute exception 3: "+e.toString());
		}

		if (etlModule.getParseProblems().size() > 0) {
			System.err.println("Parse errors occured...");
			for (ParseProblem problem : etlModule.getParseProblems()) {
				System.err.println(problem.toString());
				RedirectSystemErr.out("encontrado "+problem.toString());
			}
		}
		for ( TransformationRule rule: etlModule.getTransformationRules())
		{
			RedirectSystemErr.out("rule = ."+ rule.toString() + ".");
			
		}
		EmfModel sourceModel = createSource();
		EmfModel targetModel = createTarget();
		if(sourceModel == null)
		{
			RedirectSystemErr.out("sourceModel is null");
		}
		if(targetModel == null)
		{
			RedirectSystemErr.out("targetModel is null");
		}
		etlModule.getContext().getModelRepository().addModel(sourceModel);
		etlModule.getContext().getModelRepository().addModel(targetModel);
		try {
			result = etlModule.execute();
		} catch (EolRuntimeException e) {
			e.printStackTrace();
			RedirectSystemErr.out("execute exception 4: "+e.toString());
		}

		RedirectSystemErr.out("execute voy a leer reglas disparadas ");
		for(Transformation elem : etlModule.getContext().getTransformationTrace().getTransformations())
		{
			TransformationRule rule = elem.getRule();
			Object source = elem.getSource();
			Collection targets = elem.getTargets();
			
			System.out.println("Fired rule =" + rule.getName() + ": source =[" + source.toString() + "] & tragets =[" + targets.toString() + "]");
		}
		
		sourceModel.dispose();
		targetModel.dispose();
		etlModule.getContext().getModelRepository().dispose();
	}
  
	public Object getResult()
	{
		return result;
	}

	protected EmfModel createSource() {
		EmfModel emfModel = new EmfModel();
		emfModel.setName(sourceName);
		// MCP
		if(sourceMetaModelURI != null && !sourceMetaModelURI.isEmpty()) 
		{ 
			emfModel.setMetamodelFileBased(false); 
			if(sourceMetaModelURI.contains(","))
			{
				String[] metaModelURIs = sourceMetaModelURI.split(",");
				List<String> uris =new ArrayList<String>(metaModelURIs.length);
				for(int i=0;i<metaModelURIs.length; i++)
				{
					uris.add(metaModelURIs[i].trim());
				};
				RedirectSystemErr.out("uris:= ." + uris.toString() + ".");	
				emfModel.setMetamodelUris(uris);
			}
			else
			{
				RedirectSystemErr.out("sourceMetaModelURI:= ." + sourceMetaModelURI + ".");
				emfModel.setMetamodelUri(sourceMetaModelURI);
			}
		}

		if(sourceMetaModelFilePath != null && !sourceMetaModelFilePath.isEmpty()) 
		{ 
			emfModel.setMetamodelFileBased(true); 
			if(sourceMetaModelFilePath.contains(","))
			{
				String[] metaModelURIs = sourceMetaModelFilePath.split(",");
				List<String> files = new ArrayList<String>(metaModelURIs.length);
				List<org.eclipse.emf.common.util.URI> fileUris =new ArrayList<org.eclipse.emf.common.util.URI>(metaModelURIs.length);
				for(int i=0;i<metaModelURIs.length; i++)
				{
					files.add(metaModelURIs[i].trim());
				};
	
				RedirectSystemErr.out("files:= ." + files.toString() + ".");
				emfModel.setMetamodelFiles(files);
			}
			else
			{
				RedirectSystemErr.out("sourceMetaModelFilePath:= ." + sourceMetaModelFilePath + ".");
				emfModel.setMetamodelFile(sourceMetaModelFilePath);
			}
		}
		RedirectSystemErr.out("sourceModelFilePath:= ." + sourceModelFilePath + ".");
		emfModel.setModelFile(sourceModelFilePath);
		emfModel.setReadOnLoad(sourceReadOnLoad);
		emfModel.setStoredOnDisposal(sourceStoreOnDisposal);
		emfModel.setCachingEnabled(true);
		emfModel.setExpand(true);
		// MCP
		try {
			emfModel.load();
			System.out.println("emfModel.getAllOfType="+emfModel.getAllOfType("BaseFramework").toString());
			RedirectSystemErr.out("emfModel.getAllOfType="+emfModel.getAllOfType("BaseFramework").toString());
			System.out.println("emfModel.getAllOfKind="+emfModel.getAllOfKind("BaseFramework").toString());
			RedirectSystemErr.out("emfModel.getAllOfKind="+emfModel.getAllOfKind("BaseFramework").toString());
		} catch (EolModelLoadingException e) {
			e.printStackTrace();
		} catch (EolModelElementTypeNotFoundException e) {
			e.printStackTrace();
		}
		return emfModel;
	}

	protected EmfModel createTarget() {
		EmfModel emfModel = new EmfModel();
		emfModel.setName(targetName);
		//CRF problem		
		//RedirectSystemErr.out("targetMetaModelURI:= ." + targetMetaModelURI + ".");		
		//emfModel.setMetamodelUri(targetMetaModelURI);
		if(targetMetaModelURI != null && !targetMetaModelURI.isEmpty()) 
		{ 
			emfModel.setMetamodelFileBased(false); 
			if(targetMetaModelURI.contains(","))
			{
				String[] metaModelURIs = targetMetaModelURI.split(",");
				List<String> uris =new ArrayList<String>(metaModelURIs.length);
				for(int i=0;i<metaModelURIs.length; i++)
				{
					uris.add(metaModelURIs[i].trim());
				};
				RedirectSystemErr.out("target uris:= ." + uris.toString() + ".");	
				emfModel.setMetamodelUris(uris);
			}
			else
			{
				RedirectSystemErr.out("targetMetaModelURI:= ." + targetMetaModelURI + ".");
				emfModel.setMetamodelUri(targetMetaModelURI);
			}
		}

		if(targetMetaModelFilePath != null && !targetMetaModelFilePath.isEmpty()) 
		{ 
			emfModel.setMetamodelFileBased(true); 
			if(targetMetaModelFilePath.contains(","))
			{
				String[] metaModelURIs = targetMetaModelFilePath.split(",");
				List<String> files = new ArrayList<String>(metaModelURIs.length);
				List<org.eclipse.emf.common.util.URI> fileUris =new ArrayList<org.eclipse.emf.common.util.URI>(metaModelURIs.length);
				for(int i=0;i<metaModelURIs.length; i++)
				{
					files.add(metaModelURIs[i].trim());
				};
	
				RedirectSystemErr.out("target files:= ." + files.toString() + ".");
				emfModel.setMetamodelFiles(files);
			}
			else
			{
				RedirectSystemErr.out("targetMetaModelFilePath:= ." + targetMetaModelFilePath + ".");
				emfModel.setMetamodelFile(targetMetaModelFilePath);
			}
		}
		//CRF problem		
		RedirectSystemErr.out("targetModelFilePath:= ." + targetModelFilePath + ".");		
		emfModel.setModelFile(targetModelFilePath);
		emfModel.setReadOnLoad(targetReadOnLoad);
		emfModel.setStoredOnDisposal(targetStoreOnDisposal);
		// MCP
		emfModel.setCachingEnabled(true);
		emfModel.setExpand(true);
		// MCP
		try {
			emfModel.load();
			System.out.println("emfModel.getAllOfType="+emfModel.getAllOfType("Case").toString());
			RedirectSystemErr.out("emfModel.getAllOfType="+emfModel.getAllOfType("Case").toString());
			System.out.println("emfModel.getAllOfKind="+emfModel.getAllOfKind("Case").toString());
			RedirectSystemErr.out("emfModel.getAllOfKind="+emfModel.getAllOfKind("Case").toString());
		} catch (EolModelLoadingException e) {
			e.printStackTrace();
		} catch (EolModelElementTypeNotFoundException e) {
			e.printStackTrace();
		}
		return emfModel;
	}
	

	public void createJob(
			EtlTransformParameters parameters) {
		this.pluginID = parameters.getPluginID();
		this.etlTransformation = parameters.getEtlTransform();
		this.sourceName = parameters.getSourceName();
		this.sourceMetaModelURI = parameters.getSourceMetaModelURI();
		//MCP
		this.sourceMetaModelFilePath = parameters.getSourceMetaModelFilePath();
		//CRF problem		
		//this.oldEPackage = parameters.getOldEPackages();
		this.IoldEPackage = parameters.getIOldEPackages();
		this.OoldEPackage = parameters.getOOldEPackages();		
		//CRF problem		
		//MCP
		this.sourceModelFilePath = parameters.getSourceModelFilePath();
		this.sourceReadOnLoad = parameters.isSourceReadOnLoad();
		this.targetName = parameters.getTargetName();
		this.targetMetaModelURI = parameters.getTargetMetaModelURI();
		//CRF problem		
		this.targetMetaModelFilePath = parameters.getTargetMetaModelFilePath();
		//CRF problem		
		this.targetModelFilePath = parameters.getTargetModelFilePath();
		this.targetReadOnLoad = parameters.isTargetReadOnLoad();
		this.targetStoreOnDisposal = parameters.isTargetStoreOnDisposal();
	}
}
