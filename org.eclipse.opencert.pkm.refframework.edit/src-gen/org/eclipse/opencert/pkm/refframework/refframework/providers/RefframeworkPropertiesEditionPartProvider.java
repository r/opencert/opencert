/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.providers;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.api.providers.IPropertiesEditionPartProvider;

import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;

import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.ActivityApplicabilityPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.ActivityEquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.ActivityRequirementPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.ArtefactEquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefActivityPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefActivityRelPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefApplicabilityLevelPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefApplicabilityPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefApplicabilityRelPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefArtefactPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefArtefactRelPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefControlCategoryPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefCriticalityApplicabilityPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefCriticalityLevelPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefEquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefFrameworkPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefIndependencyLevelPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefRecommendationLevelPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefRequirementPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefRequirementRelPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefRolePropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RefTechniquePropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RequirementApplicabilityPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RequirementEquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.RoleEquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.pkm.refframework.refframework.parts.forms.TechniqueEquivalenceMapPropertiesEditionPartForm;

import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.ActivityApplicabilityPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.ActivityEquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.ActivityRequirementPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.ArtefactEquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefActivityPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefActivityRelPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefApplicabilityLevelPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefApplicabilityPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefApplicabilityRelPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefArtefactPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefArtefactRelPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefControlCategoryPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefCriticalityApplicabilityPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefCriticalityLevelPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefEquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefFrameworkPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefIndependencyLevelPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefRecommendationLevelPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefRequirementPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefRequirementRelPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefRolePropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RefTechniquePropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RequirementApplicabilityPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RequirementEquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.RoleEquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.pkm.refframework.refframework.parts.impl.TechniqueEquivalenceMapPropertiesEditionPartImpl;

/**
 * 
 * 
 */
public class RefframeworkPropertiesEditionPartProvider implements IPropertiesEditionPartProvider {

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#provides(java.lang.Object)
	 * 
	 */
	public boolean provides(Object key) {
		return key == RefframeworkViewsRepository.class;
	}

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#getPropertiesEditionPart(java.lang.Object, int, org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(Object key, int kind, IPropertiesEditionComponent component) {
		if (key == RefframeworkViewsRepository.RefFramework_.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefFrameworkPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefFrameworkPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefRequirement.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefRequirementPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefRequirementPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefArtefact.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefArtefactPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefArtefactPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefActivity.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefActivityPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefActivityPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefRequirementRel.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefRequirementRelPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefRequirementRelPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefRole.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefRolePropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefRolePropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefApplicabilityLevel.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefApplicabilityLevelPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefApplicabilityLevelPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefCriticalityLevel.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefCriticalityLevelPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefCriticalityLevelPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefTechnique.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefTechniquePropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefTechniquePropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefArtefactRel.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefArtefactRelPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefArtefactRelPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefCriticalityApplicability.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefCriticalityApplicabilityPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefCriticalityApplicabilityPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefActivityRel.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefActivityRelPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefActivityRelPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefIndependencyLevel.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefIndependencyLevelPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefIndependencyLevelPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefRecommendationLevel.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefRecommendationLevelPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefRecommendationLevelPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefControlCategory.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefControlCategoryPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefControlCategoryPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefApplicability.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefApplicabilityPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefApplicabilityPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefApplicabilityRel.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefApplicabilityRelPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefApplicabilityRelPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RefEquivalenceMap.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RefEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RefEquivalenceMapPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.ActivityRequirement.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new ActivityRequirementPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new ActivityRequirementPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.ActivityApplicability.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new ActivityApplicabilityPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new ActivityApplicabilityPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.ActivityEquivalenceMap.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new ActivityEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new ActivityEquivalenceMapPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RequirementApplicability.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RequirementApplicabilityPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RequirementApplicabilityPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RequirementEquivalenceMap.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RequirementEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RequirementEquivalenceMapPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.ArtefactEquivalenceMap.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new ArtefactEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new ArtefactEquivalenceMapPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.RoleEquivalenceMap.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new RoleEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new RoleEquivalenceMapPropertiesEditionPartForm(component);
		}
		if (key == RefframeworkViewsRepository.TechniqueEquivalenceMap.class) {
			if (kind == RefframeworkViewsRepository.SWT_KIND)
				return new TechniqueEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == RefframeworkViewsRepository.FORM_KIND)
				return new TechniqueEquivalenceMapPropertiesEditionPartForm(component);
		}
		return null;
	}

}
