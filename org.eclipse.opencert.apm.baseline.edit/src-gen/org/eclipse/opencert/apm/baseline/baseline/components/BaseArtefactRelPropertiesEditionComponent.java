/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class BaseArtefactRelPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for source EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings sourceSettings;
	
	/**
	 * Settings for target EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings targetSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaseArtefactRelPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseArtefactRel, String editing_mode) {
		super(editingContext, baseArtefactRel, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = BaselineViewsRepository.class;
		partKey = BaselineViewsRepository.BaseArtefactRel.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaseArtefactRel baseArtefactRel = (BaseArtefactRel)elt;
			final BaseArtefactRelPropertiesEditionPart basePart = (BaseArtefactRelPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseArtefactRel.getId()));
			
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseArtefactRel.getName()));
			
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.description))
				basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, baseArtefactRel.getDescription()));
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource)) {
				basePart.setMaxMultiplicitySource(EEFConverterUtil.convertToString(EcorePackage.Literals.EINT, baseArtefactRel.getMaxMultiplicitySource()));
			}
			
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource)) {
				basePart.setMinMultiplicitySource(EEFConverterUtil.convertToString(EcorePackage.Literals.EINT, baseArtefactRel.getMinMultiplicitySource()));
			}
			
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget)) {
				basePart.setMaxMultiplicityTarget(EEFConverterUtil.convertToString(EcorePackage.Literals.EINT, baseArtefactRel.getMaxMultiplicityTarget()));
			}
			
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget)) {
				basePart.setMinMultiplicityTarget(EEFConverterUtil.convertToString(EcorePackage.Literals.EINT, baseArtefactRel.getMinMultiplicityTarget()));
			}
			
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect)) {
				basePart.initModificationEffect(EEFUtils.choiceOfValues(baseArtefactRel, BaselinePackage.eINSTANCE.getBaseArtefactRel_ModificationEffect()), baseArtefactRel.getModificationEffect());
			}
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect)) {
				basePart.initRevocationEffect(EEFUtils.choiceOfValues(baseArtefactRel, BaselinePackage.eINSTANCE.getBaseArtefactRel_RevocationEffect()), baseArtefactRel.getRevocationEffect());
			}
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.source)) {
				// init part
				sourceSettings = new EObjectFlatComboSettings(baseArtefactRel, BaselinePackage.eINSTANCE.getBaseArtefactRel_Source());
				basePart.initSource(sourceSettings);
				// set the button mode
				basePart.setSourceButtonMode(ButtonsModeEnum.BROWSE);
			}
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.target)) {
				// init part
				targetSettings = new EObjectFlatComboSettings(baseArtefactRel, BaselinePackage.eINSTANCE.getBaseArtefactRel_Target());
				basePart.initTarget(targetSettings);
				// set the button mode
				basePart.setTargetButtonMode(ButtonsModeEnum.BROWSE);
			}
			// init filters
			
			
			
			
			
			
			
			
			
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.source)) {
				basePart.addFilterToSource(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof BaseArtefact);
					}
					
				});
				// Start of user code for additional businessfilters for source
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.target)) {
				basePart.addFilterToTarget(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof BaseArtefact);
					}
					
				});
				// Start of user code for additional businessfilters for target
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}














	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource) {
			return BaselinePackage.eINSTANCE.getBaseArtefactRel_MaxMultiplicitySource();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource) {
			return BaselinePackage.eINSTANCE.getBaseArtefactRel_MinMultiplicitySource();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget) {
			return BaselinePackage.eINSTANCE.getBaseArtefactRel_MaxMultiplicityTarget();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget) {
			return BaselinePackage.eINSTANCE.getBaseArtefactRel_MinMultiplicityTarget();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect) {
			return BaselinePackage.eINSTANCE.getBaseArtefactRel_ModificationEffect();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect) {
			return BaselinePackage.eINSTANCE.getBaseArtefactRel_RevocationEffect();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.source) {
			return BaselinePackage.eINSTANCE.getBaseArtefactRel_Source();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefactRel.Properties.target) {
			return BaselinePackage.eINSTANCE.getBaseArtefactRel_Target();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaseArtefactRel baseArtefactRel = (BaseArtefactRel)semanticObject;
		if (BaselineViewsRepository.BaseArtefactRel.Properties.id == event.getAffectedEditor()) {
			baseArtefactRel.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseArtefactRel.Properties.name == event.getAffectedEditor()) {
			baseArtefactRel.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseArtefactRel.Properties.description == event.getAffectedEditor()) {
			baseArtefactRel.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource == event.getAffectedEditor()) {
			baseArtefactRel.setMaxMultiplicitySource((EEFConverterUtil.createIntFromString(EcorePackage.Literals.EINT, (String)event.getNewValue())));
		}
		if (BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource == event.getAffectedEditor()) {
			baseArtefactRel.setMinMultiplicitySource((EEFConverterUtil.createIntFromString(EcorePackage.Literals.EINT, (String)event.getNewValue())));
		}
		if (BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget == event.getAffectedEditor()) {
			baseArtefactRel.setMaxMultiplicityTarget((EEFConverterUtil.createIntFromString(EcorePackage.Literals.EINT, (String)event.getNewValue())));
		}
		if (BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget == event.getAffectedEditor()) {
			baseArtefactRel.setMinMultiplicityTarget((EEFConverterUtil.createIntFromString(EcorePackage.Literals.EINT, (String)event.getNewValue())));
		}
		if (BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect == event.getAffectedEditor()) {
			baseArtefactRel.setModificationEffect((ChangeEffectKind)event.getNewValue());
		}
		if (BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect == event.getAffectedEditor()) {
			baseArtefactRel.setRevocationEffect((ChangeEffectKind)event.getNewValue());
		}
		if (BaselineViewsRepository.BaseArtefactRel.Properties.source == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				sourceSettings.setToReference((BaseArtefact)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				BaseArtefact eObject = BaselineFactory.eINSTANCE.createBaseArtefact();
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, eObject, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(eObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy != null) {
						policy.execute();
					}
				}
				sourceSettings.setToReference(eObject);
			}
		}
		if (BaselineViewsRepository.BaseArtefactRel.Properties.target == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				targetSettings.setToReference((BaseArtefact)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				BaseArtefact eObject = BaselineFactory.eINSTANCE.createBaseArtefact();
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, eObject, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(eObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy != null) {
						policy.execute();
					}
				}
				targetSettings.setToReference(eObject);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaseArtefactRelPropertiesEditionPart basePart = (BaseArtefactRelPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.description)){
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseArtefactRel_MaxMultiplicitySource().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource)) {
				if (msg.getNewValue() != null) {
					basePart.setMaxMultiplicitySource(EcoreUtil.convertToString(EcorePackage.Literals.EINT, msg.getNewValue()));
				} else {
					basePart.setMaxMultiplicitySource("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseArtefactRel_MinMultiplicitySource().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource)) {
				if (msg.getNewValue() != null) {
					basePart.setMinMultiplicitySource(EcoreUtil.convertToString(EcorePackage.Literals.EINT, msg.getNewValue()));
				} else {
					basePart.setMinMultiplicitySource("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseArtefactRel_MaxMultiplicityTarget().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget)) {
				if (msg.getNewValue() != null) {
					basePart.setMaxMultiplicityTarget(EcoreUtil.convertToString(EcorePackage.Literals.EINT, msg.getNewValue()));
				} else {
					basePart.setMaxMultiplicityTarget("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseArtefactRel_MinMultiplicityTarget().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget)) {
				if (msg.getNewValue() != null) {
					basePart.setMinMultiplicityTarget(EcoreUtil.convertToString(EcorePackage.Literals.EINT, msg.getNewValue()));
				} else {
					basePart.setMinMultiplicityTarget("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseArtefactRel_ModificationEffect().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect))
				basePart.setModificationEffect((ChangeEffectKind)msg.getNewValue());
			
			if (BaselinePackage.eINSTANCE.getBaseArtefactRel_RevocationEffect().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect))
				basePart.setRevocationEffect((ChangeEffectKind)msg.getNewValue());
			
			if (BaselinePackage.eINSTANCE.getBaseArtefactRel_Source().equals(msg.getFeature()) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.source))
				basePart.setSource((EObject)msg.getNewValue());
			if (BaselinePackage.eINSTANCE.getBaseArtefactRel_Target().equals(msg.getFeature()) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefactRel.Properties.target))
				basePart.setTarget((EObject)msg.getNewValue());
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			BaselinePackage.eINSTANCE.getBaseArtefactRel_MaxMultiplicitySource(),
			BaselinePackage.eINSTANCE.getBaseArtefactRel_MinMultiplicitySource(),
			BaselinePackage.eINSTANCE.getBaseArtefactRel_MaxMultiplicityTarget(),
			BaselinePackage.eINSTANCE.getBaseArtefactRel_MinMultiplicityTarget(),
			BaselinePackage.eINSTANCE.getBaseArtefactRel_ModificationEffect(),
			BaselinePackage.eINSTANCE.getBaseArtefactRel_RevocationEffect(),
			BaselinePackage.eINSTANCE.getBaseArtefactRel_Source(),
			BaselinePackage.eINSTANCE.getBaseArtefactRel_Target()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#isRequired(java.lang.Object, int)
	 * 
	 */
	public boolean isRequired(Object key, int kind) {
		return key == BaselineViewsRepository.BaseArtefactRel.Properties.source || key == BaselineViewsRepository.BaseArtefactRel.Properties.target;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (BaselineViewsRepository.BaseArtefactRel.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefactRel.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefactRel.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseArtefactRel_MaxMultiplicitySource().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseArtefactRel_MaxMultiplicitySource().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseArtefactRel_MinMultiplicitySource().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseArtefactRel_MinMultiplicitySource().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseArtefactRel_MaxMultiplicityTarget().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseArtefactRel_MaxMultiplicityTarget().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseArtefactRel_MinMultiplicityTarget().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseArtefactRel_MinMultiplicityTarget().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseArtefactRel_ModificationEffect().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseArtefactRel_ModificationEffect().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseArtefactRel_RevocationEffect().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseArtefactRel_RevocationEffect().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
