/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.pkm.refframework.refframework.diagram.part;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * This class is used to get the selection index and run the AssociateDiagram Action
 * 
 * @author M� Carmen Palacios
 */
/**
 * @generated NOT
 */
public class AssociateDiagramHandler extends AbstractHandler {
	public static final String ID = "org.eclipse.opencert.pkm.refframework.refframework.diagram.part.activate";
	public static final String PARM_EDITOR = "org.eclipse.opencert.pkm.refframework.refframework.diagram.part.activate.index";

	/**
	 * @generated NOT
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		Object index = event.getObjectParameterForExecution(PARM_EDITOR);
		if (!(index instanceof Integer)) {
			throw new ExecutionException("Invalid index: " + index);
		}
		int i = ((Integer) index).intValue();
		try {
			String fileName = AssociateDiagram.getFile(i);
			ISelection selection = AssociateDiagram.geSelection();
			System.out.println("ActivateEditorHandler i=" + i);
			System.out.println("ActivateEditorHandler fileName=" + fileName);
			AssociateDiagramAction as = new AssociateDiagramAction(fileName, selection, AssociateDiagramAction.ACTION_ASSOCIATE);
			as.init();
			as.run();
		} catch (Exception e) {
			throw new ExecutionException("ActivateEditorHandler Failed", e);
		}
		return null;
	}

}

 

