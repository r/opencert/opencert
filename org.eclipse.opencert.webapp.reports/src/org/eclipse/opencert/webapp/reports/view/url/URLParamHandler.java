/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.url;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.client.utils.URIBuilder;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.util.OPException;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;

import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

public class URLParamHandler
{
    public static final String GUI_MODE_URL_PARAM = "GUIMode";
    public static final String REPORT_ID_URL_PARAM = "reportID";
    public static final String PROJECT_NAME_URL_PARAM = "projectName";
    
    public final static String URL_CODING = "UTF-8";
    
    public static URLParamsContainer processURLParams(VaadinRequest request)
    {
        try 
        {
            GUIMode guiMode = detectParamEnumValue(request, GUI_MODE_URL_PARAM, GUIMode.class, GUIMode.values());
            if (guiMode == null) {
                guiMode = GUIMode.NORMAL;
            }
            ReportID reportID = detectParamEnumValue(request, REPORT_ID_URL_PARAM, ReportID.class, ReportID.values());
            String projectName = detectProjectValue(request, PROJECT_NAME_URL_PARAM); 
            
            return new URLParamsContainer(reportID, guiMode, projectName, 
                    UI.getCurrent().getPage().getLocation().getHost(), 
                    UI.getCurrent().getPage().getLocation().getPort(), 
                    UI.getCurrent().getPage().getLocation().getPath());
        } 
        catch (URLOPException ex) 
        {
            return new URLParamsContainer(false, ex.getMessage(),
                    UI.getCurrent().getPage().getLocation().getHost(), 
                    UI.getCurrent().getPage().getLocation().getPort(), 
                    UI.getCurrent().getPage().getLocation().getPath());
        }
    }
    
    /**
     * Generates full http URL to the OEPNCOSS web page for the specific urlParams.
     * Note: guiMode parameter forces the generated mode 
     */
    public static String generateWebURL(URLParamsContainer urlParams, GUIMode guiMode)
    {
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme("http").setHost(urlParams.getHost()).setPort(urlParams.getPort());
        uriBuilder.setPath(urlParams.getPath());
        
        if (guiMode != null) {
            uriBuilder.addParameter(GUI_MODE_URL_PARAM, guiMode.name());
        }
        if (urlParams.getReportID() != null) {
            uriBuilder.addParameter(REPORT_ID_URL_PARAM, urlParams.getReportID().name());
        }
        if (urlParams.getDetectedProjectName() != null) {
            uriBuilder.addParameter(PROJECT_NAME_URL_PARAM, urlParams.getDetectedProjectName());
        }
        
        return uriBuilder.toString();
    }

    
    private static String detectProjectValue(VaadinRequest request, String projectNameUrlParam)
        throws URLOPException
    {        
        final String projectNameValue = decodeValueFromRequest(request, projectNameUrlParam);
        
        return projectNameValue;
    }

    private static <T extends Enum<T>> T detectParamEnumValue(VaadinRequest request, String param, Class<T> clazz, T[] values)
        throws URLOPException
    {
        String value = decodeValueFromRequest(request, param);
        
        if (value == null) {
            return null;
        }
        
        try {
            T resultEnumValue = Enum.valueOf(clazz, value);
            
            return resultEnumValue;
            
        } catch (RuntimeException ex) 
        {
            OpencertLogger.error(ex);
            throw new URLOPException(param, value, Arrays.asList(values));
        }
    }

    private static String decodeValueFromRequest(VaadinRequest request, String param)
    {        
        String value = request.getParameter(param);
     
        // No need for explicit decoding - web browser already does the decoding.
        
        return value;
//        try 
//        {
//            return (value != null ? URLDecoder.decode(value, URL_CODING) : null);            
//        } 
//        catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//            return null;
//        }
    }

    private static String encodeValueForRequest(String value)
    {
        try 
        {
            return (value != null ? URLEncoder.encode(value, URL_CODING) : null);            
        } 
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static GUIMode detectGUIMode(VaadinRequest request)
        throws URLOPException
    {
        String guiModeName = request.getParameter(GUI_MODE_URL_PARAM);
        
        if (guiModeName == null) {
            return GUIMode.NORMAL;
        }
        
        try {
            GUIMode guiMode = GUIMode.valueOf(guiModeName);
            
            return guiMode;
            
        } catch (RuntimeException ex) {
            OpencertLogger.error(ex);
            throw new URLOPException(GUI_MODE_URL_PARAM, guiModeName, Arrays.asList(GUIMode.values()));
        }
    }
    
    private final static class URLOPException extends OPException
    {
        private String message;

        public URLOPException(String urlParamName, String urlParamValue, List<?> values)
        {
            super("");
            String msg0 = String.format("Value: %s is illegal for the parameter: %s. "
                    + "\nPossible values are: ", urlParamValue, urlParamName);
            String msg = msg0 + appendValues(urlParamName, values);
            msg += "\n";
            msg += generateGeneralParamsHelp();        
            
            this.message = msg;
        }

        private String appendValues(String urlParam, List<?> values)
        {
            String msg = "\nParameter: " + urlParam;
            msg += "\nValues: ";
            for (Object value : values) 
            {
                msg += encodeValueForRequest(value.toString());
                msg += " ";
            }
            msg += "\n";
            return msg;
        }
        
        private String generateGeneralParamsHelp()
        {
            String msg = "\nAcceptable URL parameters: ";
            msg += appendValues(GUI_MODE_URL_PARAM, Arrays.asList(GUIMode.values()));
            msg += appendValues(REPORT_ID_URL_PARAM, Arrays.asList(ReportID.values()));
            
            AssuranceProjectManager projectManager = (AssuranceProjectManager) SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
            List<AssuranceProjectWrapper> projects = projectManager.getProjects();            
            List<String> projectNames = projects.stream().map(e -> e.getName()).collect(Collectors.toList());
            msg += appendValues(PROJECT_NAME_URL_PARAM, projectNames);
            
            return msg;
        }

        @Override
        public String getMessage()
        {
            return this.message;
        }
    }
}