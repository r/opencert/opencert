/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.aida.tracemodel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.capra.core.CapraException;
import org.eclipse.capra.core.adapters.TraceLinkAdapter;
import org.eclipse.capra.core.util.TraceLinkAttribute;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelFactory;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;
import org.polarsys.chess.contracts.profile.chesscontract.Contract;
import org.polarsys.chess.contracts.profile.chesscontract.util.ContractEntityUtil;
import org.polarsys.chess.contracts.profile.chesscontract.util.EntityUtil;

/**
 * This adapter provides access to GenericTraceLink traces.
 * 
 * @author Stefano Puri
 */
public class ContractClaimLinkAdapter implements TraceLinkAdapter {

	public static final String CONTRACT_CLAIM_LINK_TYPE = "Contract-Claim Trace";

	@Override
	public boolean canAdapt(EClass traceType) {
		if (traceType.equals(OpenCertTraceLinkMetaModelPackage.eINSTANCE.getContractClaimLink())) {
			return true;
		}
		return false;
	}

	@Override
	public List<EObject> getSources(EObject trace) {
		assert trace instanceof ContractClaimLink;
		ContractClaimLink contractTraceLink = (ContractClaimLink) trace;
		List<EObject> contracts = new ArrayList<EObject>();
		// for (Contract c : contractTraceLink.getSources()){
		contracts.add(contractTraceLink.getSources());
		// }
		return contracts;
	}

	@Override
	public List<EObject> getTargets(EObject trace) {
		assert trace instanceof ContractClaimLink;
		ContractClaimLink genericTraceLink = (ContractClaimLink) trace;
		List<EObject> claims = new ArrayList<EObject>();
		for (Claim c : genericTraceLink.getTargets()) {
			claims.add(c);
		}
		return claims;
	}

	@Override
	public String getLinkType() {
		return CONTRACT_CLAIM_LINK_TYPE;
	}

	@Override
	public boolean canCreateLink(List<EObject> sources, List<EObject> targets) {
		if (sources.isEmpty() || targets.isEmpty())
			return false;
		for (EObject obj : sources) {
			if (!(obj instanceof Contract) && (!(obj instanceof Element) || !ContractEntityUtil.getInstance().isContract((Element) obj)))
				return false;
		}
		for (EObject obj : targets) {
			if (!(obj instanceof Claim))
				return false;
		}
		return true;
	}

	public EObject createLink(List<EObject> sources, List<EObject> targets) {
		ContractClaimLink traceLink = OpenCertTraceLinkMetaModelFactory.eINSTANCE.createContractClaimLink();
		
		if( sources.get(0) instanceof Contract)
			traceLink.setSources((Contract) sources.get(0));
		else{
			if(ContractEntityUtil.getInstance().isContract((Element) sources.get(0))){
			Stereotype contr = ((Element) sources.get(0)).getApplicableStereotype(ContractEntityUtil.getInstance().CONTRACT);
			traceLink.setSources((Contract)((Element) sources.get(0)).getStereotypeApplication(contr));
			}
		}
		// for (EObject obj : sources){
		// traceLink.getSources().add((Contract) obj);
		// }
		for (EObject obj : targets) {
			traceLink.getTargets().add((Claim) obj);
		}

		return traceLink;
	}

	@Override
	public EObject createLink(List<EObject> sources, List<EObject> targets, List<TraceLinkAttribute> attributes)
			throws CapraException {
		return this.createLink(sources, targets);
	}

	@Override
	public List<TraceLinkAttribute> getAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLabelForEObject(EObject eobject) {
		
		URI uri = null;
		if (eobject != null && eobject.eResource() !=null)
			uri = eobject.eResource().getURI();

		if (eobject instanceof Contract){
			Contract contr = (Contract) eobject;
			return (uri!=null ? contr.getBase_Class().getName() + " ("+uri+")" : contr.getBase_Class().getName());
			
		}
		if (eobject instanceof Claim){
			Claim claim = (Claim) eobject;
			return (uri != null ? ((Claim)eobject).getName() + " ("+uri+")" : ((Claim)eobject).getName());
		}
		return null;
	}
}
