/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages.adapters;

import org.eclipse.amalgam.explorer.activity.ui.api.editor.ActivityExplorerEditor;
import org.eclipse.jface.window.Window;
import org.eclipse.opencert.infra.dashboard.dialogs.DashboardMappingSetDialog;
import org.eclipse.opencert.infra.dashboard.helpers.EclipseHelper;
import org.eclipse.opencert.pkm.refframework.refframework.utils.MappingSet;
import org.eclipse.ui.forms.events.HyperlinkAdapter;

/**
 * A {@link HyperlinkAdapter} to open the {@link MappingSet} dialog.
 */
public class OpenMappingSetDialogHyperLinkAdapter extends AbstractOpenDialogHyperLinkAdapter {

	/**
	 * Default constructor.
	 */
	public OpenMappingSetDialogHyperLinkAdapter() {
		super();
	}

	/**
	 * Open a dialog when the link is pressed.
	 */
	@Override
	protected void openDialog() {

		DashboardMappingSetDialog vMappingSet = new DashboardMappingSetDialog(EclipseHelper.getActiveShell());

		// Open the dialog
		boolean vIsOKButtonClicked = vMappingSet.open() == Window.OK;

		// Refresh the page to update the diagram viewers
		if (vIsOKButtonClicked && EclipseHelper.getActiveEditor() instanceof ActivityExplorerEditor) {
			ActivityExplorerEditor vActExplorerEditor = (ActivityExplorerEditor) EclipseHelper.getActiveEditor();
			vActExplorerEditor.setActivePage(STANDARD_COMPLIANCE_DEFINITION_PAGE_ID);
		}
	}
}
