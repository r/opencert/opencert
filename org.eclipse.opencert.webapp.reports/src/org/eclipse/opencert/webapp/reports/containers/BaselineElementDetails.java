/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;


public class BaselineElementDetails
{
    private long cdoId;
    private String baselineElementName;
    private String baselineElementType;
    private BaseAssetComplianceStatus baseAssetComplianceStatus;
    
    public BaselineElementDetails(long cdoId, String baselineElementName, String baselineElementType, BaseAssetComplianceStatus baseAssetComplianceStatus)
    {
        super();
        this.cdoId = cdoId;
        this.baselineElementName = baselineElementName;
        this.baselineElementType = baselineElementType;
        this.baseAssetComplianceStatus = baseAssetComplianceStatus;
    }
    
    public long getCdoId()
    {
        return cdoId;
    }
    
    public void setCdoId(long cdoId)
    {
        this.cdoId = cdoId;
    }
    
    public String getBaselineElementName()
    {
        return baselineElementName;
    }
    
    public void setBaselineElementName(String baselineElementName)
    {
        this.baselineElementName = baselineElementName;
    }
    
    public String getBaselineElementType()
    {
        return baselineElementType;
    }
    
    public void setBaselineElementType(String baselineElementType)
    {
        this.baselineElementType = baselineElementType;
    }
    
    public BaseAssetComplianceStatus getBaseAssetComplianceStatus()
    {
        return baseAssetComplianceStatus;
    }
    
    public void setBaseAssetComplianceStatus(BaseAssetComplianceStatus baseAssetComplianceStatus)
    {
        this.baseAssetComplianceStatus = baseAssetComplianceStatus;
    }
    
    @Override
    public String toString()
    {
        return "BaselineElementDetails [cdoId=" + cdoId
                + ", baselineElementName=" + baselineElementName
                + ", baselineElementType=" + baselineElementType
                + ", baseAssetComplianceStatus=" + baseAssetComplianceStatus
                + "]";
    }
       
}
