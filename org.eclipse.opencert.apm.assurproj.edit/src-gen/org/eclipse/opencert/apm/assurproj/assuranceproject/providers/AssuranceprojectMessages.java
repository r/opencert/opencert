/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.providers;

import org.eclipse.osgi.util.NLS;

/**
 * 
 * 
 */
public class AssuranceprojectMessages extends NLS {
	
	private static final String BUNDLE_NAME = "org.eclipse.opencert.apm.assurproj.assuranceproject.providers.assuranceprojectMessages"; //$NON-NLS-1$

	
	public static String AssuranceProjectPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PermissionConfigPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AssetsPackagePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String BaselineConfigPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AssuranceAssetsPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PermissionConfigurationsPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ProjectBaselinesPropertiesEditionPart_PropertiesGroupLabel;


	
	public static String AssuranceProject_ReadOnly;

	
	public static String AssuranceProject_Part_Title;

	
	public static String PermissionConfig_ReadOnly;

	
	public static String PermissionConfig_Part_Title;

	
	public static String AssetsPackage_ReadOnly;

	
	public static String AssetsPackage_Part_Title;

	
	public static String BaselineConfig_ReadOnly;

	
	public static String BaselineConfig_Part_Title;

	
	public static String AssuranceAssets_ReadOnly;

	
	public static String AssuranceAssets_Part_Title;

	
	public static String PermissionConfigurations_ReadOnly;

	
	public static String PermissionConfigurations_Part_Title;

	
	public static String ProjectBaselines_ReadOnly;

	
	public static String ProjectBaselines_Part_Title;


	
	public static String AssuranceProjectPropertiesEditionPart_IdLabel;

	
	public static String AssuranceProjectPropertiesEditionPart_NameLabel;

	
	public static String AssuranceProjectPropertiesEditionPart_DescriptionLabel;

	
	public static String AssuranceProjectPropertiesEditionPart_CreatedByLabel;

	
	public static String AssuranceProjectPropertiesEditionPart_ResponsibleLabel;

	
	public static String AssuranceProjectPropertiesEditionPart_DateLabel;

	
	public static String AssuranceProjectPropertiesEditionPart_VersionLabel;

	
	public static String AssuranceProjectPropertiesEditionPart_SubProjectLabel;

	
	public static String PermissionConfigPropertiesEditionPart_IdLabel;

	
	public static String PermissionConfigPropertiesEditionPart_NameLabel;

	
	public static String PermissionConfigPropertiesEditionPart_DescriptionLabel;

	
	public static String PermissionConfigPropertiesEditionPart_IsActiveLabel;

	
	public static String AssetsPackagePropertiesEditionPart_IdLabel;

	
	public static String AssetsPackagePropertiesEditionPart_NameLabel;

	
	public static String AssetsPackagePropertiesEditionPart_DescriptionLabel;

	
	public static String AssetsPackagePropertiesEditionPart_IsActiveLabel;

	
	public static String AssetsPackagePropertiesEditionPart_ArtefactsModelLabel;

	
	public static String AssetsPackagePropertiesEditionPart_ArgumentationModelLabel;

	
	public static String AssetsPackagePropertiesEditionPart_ProcessModelLabel;

	
	public static String BaselineConfigPropertiesEditionPart_IdLabel;

	
	public static String BaselineConfigPropertiesEditionPart_NameLabel;

	
	public static String BaselineConfigPropertiesEditionPart_DescriptionLabel;

	
	public static String BaselineConfigPropertiesEditionPart_ComplianceMapGroupLabel;

	
	public static String BaselineConfigPropertiesEditionPart_IsActiveLabel;

	
	public static String BaselineConfigPropertiesEditionPart_RefFrameworkLabel;

	
	public static String AssuranceAssetsPropertiesEditionPart_AssetsPackageLabel;

	
	public static String AssuranceAssetsPropertiesEditionPart_AssetsPackageTableLabel;

	
	public static String PermissionConfigurationsPropertiesEditionPart_PermissionConfLabel;

	
	public static String PermissionConfigurationsPropertiesEditionPart_PermissionConfTableLabel;

	
	public static String ProjectBaselinesPropertiesEditionPart_BaselineConfigLabel;

	
	public static String ProjectBaselinesPropertiesEditionPart_BaselineConfigTableLabel;


	
	public static String PropertiesEditionPart_DocumentationLabel;

	
	public static String PropertiesEditionPart_IntegerValueMessage;

	
	public static String PropertiesEditionPart_FloatValueMessage;

	
	public static String PropertiesEditionPart_ShortValueMessage;

	
	public static String PropertiesEditionPart_LongValueMessage;

	
	public static String PropertiesEditionPart_ByteValueMessage;

	
	public static String PropertiesEditionPart_BigIntegerValueMessage;

	
	public static String PropertiesEditionPart_BigDecimalValueMessage;

	
	public static String PropertiesEditionPart_DoubleValueMessage;

	
	public static String PropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PropertiesEditionPart_RequiredFeatureMessage;

	
	public static String PropertiesEditionPart_AddTableViewerLabel;

	
	public static String PropertiesEditionPart_EditTableViewerLabel;

	
	public static String PropertiesEditionPart_RemoveTableViewerLabel;

	
	public static String PropertiesEditionPart_AddListViewerLabel;

	
	public static String PropertiesEditionPart_RemoveListViewerLabel;

	// Start of user code for additionnal NLS Constants
	
	// End of user code

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, AssuranceprojectMessages.class);
	}

	
	private AssuranceprojectMessages() {
		//protect instanciation
	}
}
