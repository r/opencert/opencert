/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

public class ComplianceAssetEvaluation {
	
	private String id;
	private String name;
	private String criterion;
	private String criterionDescription;
	private String result;
	private String rationale;
	private String evaluationEvent;
	

	public ComplianceAssetEvaluation(String id, String name, String criterion, String criterionDescription, 
			String result, String rationale, String evaluationEvent) {
		this.id = id;
		this.name = name;
		this.criterion = criterion;
		this.criterionDescription = criterionDescription;
		this.result = result;
		this.rationale = rationale;
		this.evaluationEvent = evaluationEvent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCriterion() {
		return this.criterion;
	}
	
	public void setCriterion(String criterion) {
		this.criterion = criterion;
	}

	public String getCriterionDescription() {
		return criterionDescription;
	}

	public void setCriterionDescription(String criterionDescription) {
		this.criterionDescription = criterionDescription;
	}

	
	public String getResult() {
		return this.result;
	}
	
	public void setResult(String result) {
		this.result = result;
	}

	public String getRationale() {
		return rationale;
	}

	public void setRationale(String rationale) {
		this.rationale = rationale;
	}
	
	public String getEvaluationEvent() {
		return evaluationEvent;
	}

	public void setEvaluationEvent(String evaluationEvent) {
		this.evaluationEvent = evaluationEvent;
	}

}
