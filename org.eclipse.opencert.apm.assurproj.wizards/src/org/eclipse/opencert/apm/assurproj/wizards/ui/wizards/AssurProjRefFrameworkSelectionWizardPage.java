/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards.ui.wizards;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.opencert.apm.assurproj.utils.widget.CheckboxTreeViewerExt;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

//import es.esi.gemde.certification.qreference.reference.QualificationRequirement;
//import es.esi.gemde.certification.utils.widget.CheckboxTreeViewerExt;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;



import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.apm.assurproj.wizards.Activator;
import org.eclipse.opencert.pkm.refframework.refframework.*;

public class AssurProjRefFrameworkSelectionWizardPage extends WizardPage {
	
	private static final String OPENCERT_REFFRAMEWORK_DIR_KEY = "OPENCERT_REFFRAMEWORK_DIR_KEY";
	private static final String REFFRAMEWORK = ".refframework";
	private static final String TAILORED_REFFRAMEWORK = "tailoredRefFramework";
	private static final String NAME_FOR_BASELINE = "Name for Baseline:";
	private static final String BASELINE_SELECTION = "Baseline Selection";
	public static final String REFERENCE_FRAMEWORK_SELECTION = "Reference Framework Selection";
	private static final String CHOOSE_ASSOCIATED_REFERENCE_FRAMEWORK = "Choose associated Reference Framework";
	private static final String OPENCERT_REFERENCE_FRAMEWORK_SELECTION = "OPENCERT Reference Framework Selection";
	private static final String CRITICALITY_SELECTION = "Select the Criticality level:";
	private static final String APPLICABILITY_SELECTION = "Select the Applicability level:";
	public static final int CHANGE_EVENT = 7500;
	protected List refList;
	protected ArrayList<String> refListDir;
	protected String qref;
	protected IFile modelFile;
	protected ComposedAdapterFactory adapterFactory;
	protected ComposedAdapterFactory critAdapterFactory;
	protected ComposedAdapterFactory appliAdapterFactory;
	protected AdapterFactoryEditingDomain editingDomain;
	protected ResourceSet resourceSet;
	//protected Tree tree;
	protected CheckboxTreeViewerExt modelViewer;
	protected CheckboxTreeViewerExt criticalityViewer;
	protected CheckboxTreeViewerExt aplicabilityViewer;
	private Object [] results;
    private Text baselineName;

    private CDOView viewCDO=null;


	public AssurProjRefFrameworkSelectionWizardPage(String pageName) {
		super(pageName);
		// TODO Auto-generated constructor stub
		this.setTitle(OPENCERT_REFERENCE_FRAMEWORK_SELECTION);
		this.setMessage(CHOOSE_ASSOCIATED_REFERENCE_FRAMEWORK);
	}

	public AssurProjRefFrameworkSelectionWizardPage(String pageName, String title,
			ImageDescriptor titleImage) {
		super(pageName, title, titleImage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createControl(Composite parent) {
		setControl(createControls(parent));
	}
	/**
	 * createControls: builds the composites and controls to show the references and 
	 * the requirements of the selected reference
	 * @param parent the composite where the controls are going to be located
	 * @return the composite parent (the one that is the input parameer
	 */
	
	protected Composite createControls(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE | SWT.FILL);
		GridLayout layout1 = new GridLayout(3,true);
		composite.setLayout(layout1);
		
		Group group = new Group(composite, SWT.FILL);
		group.setLayout(new GridLayout(1, false));

		group.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		group.setText(REFERENCE_FRAMEWORK_SELECTION); //$NON-NLS-1$	
		
		Composite modelComposite = new Composite(group, SWT.FILL);
		modelComposite.setLayout(new GridLayout(1, false));
		refList = new List(modelComposite, SWT.FILL | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);
		GridData refListGridData = new GridData(SWT.FILL,SWT.FILL,true,true);
		refListGridData.heightHint = 300;
		refList.setLayoutData(refListGridData);
		refListDir = new ArrayList<String>();
		findQRef();
		qref = "";
		refList.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent e) {
			  		qref = refListDir.get(refList.getSelectionIndex()).toString();
			  		CreateCriticalModelFromFile(criticalityViewer, qref);
			  		CreateModelFromFile(modelViewer, qref);
			  		CreateApplicabilityList(aplicabilityViewer, qref);
			  		results = modelViewer.getCheckedElements();
			  		checkComplete();
		      }
		});
		Group groupReq = new Group(composite, SWT.NULL |SWT.FILL);
		groupReq.setLayout(new GridLayout(2, false));
		groupReq.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,2,3));
		groupReq.setText(BASELINE_SELECTION); //$NON-NLS-1$
		
		Label criticalityLabel = new Label(groupReq, SWT.NONE);
		Label aplicabilityLabel = new Label(groupReq, SWT.NONE);
		
		criticalityLabel.setText(CRITICALITY_SELECTION); 
	    criticalityViewer = new CheckboxTreeViewerExt(groupReq, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData critGridData = new GridData(SWT.FILL,SWT.FILL,true,true,1,1);
		critGridData.heightHint = 100;
	    criticalityViewer.getTree().setLayoutData(critGridData);
	    criticalityViewer.expandAll();

		ComposedAdapterFactory critAdapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		criticalityViewer.setContentProvider(new AdapterFactoryContentProvider(critAdapterFactory));
		criticalityViewer.setLabelProvider(new AdapterFactoryLabelProvider(critAdapterFactory));
		
		ICheckStateListener checkCritStateListener = new ICheckStateListener(){
			public void checkStateChanged (CheckStateChangedEvent event){
				Object changedElement = event.getElement();
				boolean status = criticalityViewer.getChecked(changedElement);
				boolean isExpanded = criticalityViewer.getExpandedState(changedElement);

				criticalityViewer.expandToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				criticalityViewer.setSubtreeChecked(changedElement, status);
				if (!isExpanded){
					criticalityViewer.collapseToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				}
				if (status){
					criticalityViewer.setParentsChecked(changedElement,true);
				}
//				else{
//					if (modelViewer.allBrothersUnChecked(changedElement)){
//						modelViewer.setParentsChecked(changedElement,false);
//					}
//				}
				updateModelViewer(criticalityViewer,aplicabilityViewer, modelViewer, qref);
				results = modelViewer.getCheckedElements();
				checkComplete();
			}

		};
		criticalityViewer.addCheckStateListener(checkCritStateListener);
		
//		Label aplicabilityLabel = new Label(groupReq, SWT.NONE);
		aplicabilityLabel.setText(APPLICABILITY_SELECTION); 
		aplicabilityViewer = new CheckboxTreeViewerExt(groupReq, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData appGridData = new GridData(SWT.FILL,SWT.FILL,true,false,1,1);
		appGridData.heightHint = 100;
		aplicabilityViewer.getTree().setLayoutData(appGridData);
		aplicabilityViewer.expandAll();

		ComposedAdapterFactory appliAdapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		aplicabilityViewer.setContentProvider(new AdapterFactoryContentProvider(appliAdapterFactory));
		aplicabilityViewer.setLabelProvider(new AdapterFactoryLabelProvider(appliAdapterFactory));
		
		ICheckStateListener checkAppStateListener = new ICheckStateListener(){
			public void checkStateChanged (CheckStateChangedEvent event){
				Object changedElement = event.getElement();
				boolean status = aplicabilityViewer.getChecked(changedElement);
				boolean isExpanded = aplicabilityViewer.getExpandedState(changedElement);

				aplicabilityViewer.expandToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				aplicabilityViewer.setSubtreeChecked(changedElement, status);
				if (!isExpanded){
					aplicabilityViewer.collapseToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				}
				if (status){
					aplicabilityViewer.setParentsChecked(changedElement,true);
				}
				updateModelViewer(criticalityViewer,aplicabilityViewer, modelViewer, qref);
				results = modelViewer.getCheckedElements();
				checkComplete();
			}

		};
		aplicabilityViewer.addCheckStateListener(checkAppStateListener);
		
//		modelViewer = new CheckboxTreeViewer(groupReq, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		modelViewer = new CheckboxTreeViewerExt(groupReq, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData modelGridData = new GridData(SWT.FILL,SWT.FILL,true,true,2,1);
		modelGridData.heightHint = 300;
		modelViewer.getTree().setLayoutData(modelGridData);
		modelViewer.expandAll();
		
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		modelViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		modelViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		ICheckStateListener checkStateListener = new ICheckStateListener(){
			public void checkStateChanged (CheckStateChangedEvent event){
				Object changedElement = event.getElement();
				boolean status = modelViewer.getChecked(changedElement);
				boolean isExpanded = modelViewer.getExpandedState(changedElement);
				modelViewer.expandToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				modelViewer.setSubtreeChecked(changedElement, status);
				if (!isExpanded){
					modelViewer.collapseToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				}
				if (status){
					modelViewer.setParentsChecked(changedElement,true);
				}
//				else{
//					if (modelViewer.allBrothersUnChecked(changedElement)){
//						modelViewer.setParentsChecked(changedElement,false);
//					}
//				}
				results = modelViewer.getCheckedElements();

				checkComplete();
			}
		};
		modelViewer.addCheckStateListener(checkStateListener);

		Composite nameComposite = new Composite(composite, SWT.NONE | SWT.FILL);
		GridLayout nameGridLayout = new GridLayout(2,false);
		nameComposite.setLayout(nameGridLayout);
		
		Label label1 = new Label(nameComposite, SWT.NONE);
	    label1.setText(NAME_FOR_BASELINE);
	    baselineName = new Text(nameComposite, SWT.BORDER | SWT.SINGLE | SWT.FILL);
	    GridData textGridData =  new GridData(SWT.FILL,SWT.FILL,true,false,1,1);
	    textGridData.widthHint= 300;
        baselineName.setLayoutData(textGridData);
	    baselineName.setText(TAILORED_REFFRAMEWORK);
	    baselineName.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				results = modelViewer.getCheckedElements();
				checkComplete();
			}
		});

	    this.setPageComplete(false);
		return composite;
	}

	public String getBaselineName() {
	    return baselineName.getText();

	  }

    public void checkComplete(){
    	if(!this.getBaselineName().equals("") && modelViewer.getCheckedElements().length>0){
    		this.setPageComplete(true);
    	}
    	else {
    		this.setPageComplete(false);
    	}
    	    
    }
    
    
	public boolean performOk(final IProject project, IProgressMonitor monitor) {
		monitor.worked(1);
		
		return true;
	}
	/**
	 * findQRef: obtains from preferences the directory where the standards must be. It will store in refList and refListDir all
	 * founded standards (name and path respectively)
	 * @return the number of standards
	 */
	public int findQRef (){
		int nStandards = 0;
		// Gets the directory from preferences where the application can get the standards
		
		//String strQRefDir = PlatformUI.getPreferenceStore().getString(OPENCERT_REFFRAMEWORK_DIR_KEY);
		
        //File QRefDir = new File(strQRefDir);
        // Looks into the directory to obtain all the files with .refframework extension
        //if (!QRefDir.isDirectory()) return 0;
        nStandards = addFilesOfDir();
		return nStandards;
	}
	/**
	 * addFilesOfDir
	 * @param dir directory of the file system where to look into (recursively)
	 * @return number of standards obtained
	 */
	public int addFilesOfDir (){
		int sumStd=0;
		
		
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		
						
		viewCDO = CDOConnectionUtil.instance.openView(sessionCDO);				
		CDOResourceNode[] listR=  viewCDO.getElements();			
		for (int i=0; i<listR.length; i++){
			if (hasReadPermission(listR[i])) {
				if(listR[i] instanceof CDOResourceFolder){
					checkFolderContents((CDOResourceFolder)listR[i],sumStd);
				}
				else if (listR[i].getName().endsWith(REFFRAMEWORK)){
						refList.add(listR[i].getPath());
						refListDir.add(listR[i].getPath());
	        			sumStd++;
				}		
			}
		}
		
		return sumStd;
	}
	
	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	private void checkFolderContents(CDOResourceFolder cdoResourceFolder, int sumStd) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN=  cdoResourceFolder.getNodes();
			for (int i=0; i<listN.size(); i++){
				if(listN.get(i) instanceof CDOResourceFolder){
					checkFolderContents((CDOResourceFolder)listN.get(i),sumStd);
				}
				else if (listN.get(i).getName().endsWith(REFFRAMEWORK)){
					refList.add(listN.get(i).getPath());
					refListDir.add(listN.get(i).getPath());
	    			sumStd++;
				}
			}
		}
		
	}

	public String getQRef() {
		return qref;
	}
	/**
	 * CreateModelFromFile: load the resource of the model
	 * @param tviewer widget in which the model will be visualized
	 * @param model string with the path and name of the model in the file system
	 */
	public void CreateModelFromFile(CheckboxTreeViewerExt tviewer, String model){
		if (model != null) {
			// Load the model from the file
			//ResourceSet rs = new ResourceSetImpl();
			//URI modelURI = URI.createFileURI(model);
			
		   Resource resource = null;
			try {
				resource = viewCDO.getResource(model);
				tviewer.setInput(null);
				tviewer.setInput(resource);
			    final ViewerFilter modelFilter = new ViewerFilter() {
			        public boolean select(
			          Viewer viewer,
			          Object parentElement,
			          Object element) {
			        	if (element instanceof RefFramework)
			        		return true;
			        	if (element instanceof RefActivity)
			        		return true;
			        	else if (element instanceof RefArtefact)
			        		return true;
			        	else if (element instanceof RefRole)
			        		return true;
			        	else if (element instanceof RefRequirement)
			        		return true;
			        	else if (element instanceof RefTechnique)
			        		return true;
		        		else
		        			return false;
			        }
			    };
			    tviewer.addFilter(modelFilter);
//			    tviewer.expandToLevel(2);
			    tviewer.expandAll();
			    tviewer.setAllChecked(true);
			   // viewCDO.getSession().close();
			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
				tviewer.setInput(null);
			}
		}
	}
	/**
	 * CreateCriticalModelFromFile: load the resource of the model
	 * @param tviewer widget in which the model will be visualized
	 * @param model string with the path and name of the model in the file system
	 */
	public void CreateCriticalModelFromFile(CheckboxTreeViewerExt tviewer, String model){
		if (model != null) {	
		   Resource resource = null;
			try {
				resource = viewCDO.getResource(model);
				tviewer.setInput(null);
				tviewer.setInput(resource);
				
			    final ViewerFilter modelFilter = new ViewerFilter() {
			        public boolean select(
			          Viewer viewer,
			          Object parentElement,
			          Object element) {
			        	if (element instanceof RefFramework)
			        		return true;
			        	if (element instanceof RefCriticalityLevel)
			        		return true;
		        		else
		        			return false;
			        }
			    };
			    tviewer.addFilter(modelFilter);
			    tviewer.expandAll();
			    tviewer.setAllChecked(true);
			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
				tviewer.setInput(null);
			}
		}
	}
	private void CreateApplicabilityList( CheckboxTreeViewerExt viewer, String model) {
		if (model != null) {	
			   Resource resource = null;
			   try {
					resource = viewCDO.getResource(model);
					viewer.setInput(null);
					viewer.setInput(resource);
					
				    final ViewerFilter modelFilter = new ViewerFilter() {
				        public boolean select(
				          Viewer viewer,
				          Object parentElement,
				          Object element) {
				        	if (element instanceof RefFramework)
				        		return true;
				        	if (element instanceof RefApplicabilityLevel)
				        		return true;
			        		else
			        			return false;
				        }
				    };
				    viewer.addFilter(modelFilter);
				    viewer.expandAll();
				    viewer.setAllChecked(true);
				   // viewCDO.getSession().close();
			   }catch (Exception ex) {
					MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
					viewer.setInput(null);
				}
		}
		
	}
	private void updateModelViewer(CheckboxTreeViewerExt criticalityViewer, CheckboxTreeViewerExt aplicabilityViewer, CheckboxTreeViewerExt tviewer, String qref) {	
		Resource model=viewCDO.getResource(qref);
		EObject reference = model.getContents().get(0);
		if(reference instanceof RefFramework){
				EList<RefActivity> activities = ((RefFramework) reference).getOwnedActivities();
				// EList<RefRequirement> requirements = ((RefFramework) reference).getOwnedRequirement();
				UpdateActCriticality(criticalityViewer,aplicabilityViewer, tviewer,activities);
			}
	}

	public void UpdateActCriticality(CheckboxTreeViewerExt criticalityViewer, CheckboxTreeViewerExt aplicabilityViewer, CheckboxTreeViewerExt tviewer, EList<RefActivity> activities){
			
		Iterator<RefActivity> actIterator = activities.iterator();
		while (actIterator.hasNext()){
			RefActivity act=actIterator.next();
			EList<RefActivity> subActivities = act.getSubActivity();
			
			EList<RefRequirement> requirements = act.getOwnedRequirement();
			EList<RefApplicability> applicabilities = act.getApplicability();
			UpdateCriticality(criticalityViewer,aplicabilityViewer, tviewer,applicabilities,act,null);
			UpdateReqCriticality(criticalityViewer,aplicabilityViewer, tviewer,requirements,act);
			
			if(subActivities.size()>0){
				UpdateActCriticality(criticalityViewer,aplicabilityViewer, tviewer,subActivities);
			}
		}
	}
	
	public void UpdateReqCriticality(CheckboxTreeViewerExt criticalityViewer, 
			CheckboxTreeViewerExt aplicabilityViewer,
			CheckboxTreeViewerExt tviewer,
			EList<RefRequirement> requirements,
			RefActivity act){
		Iterator<RefRequirement> reqIterator = requirements.iterator();
		while(reqIterator.hasNext()){
			RefRequirement req = reqIterator.next();
			EList<RefRequirement> subReq = req.getSubRequirement();
			
			EList<RefApplicability> reqApplicabilities = req.getApplicability();
			UpdateCriticality(criticalityViewer, aplicabilityViewer, tviewer,reqApplicabilities,act,req);
			
			if(subReq.size()>0){
				UpdateReqCriticality(criticalityViewer,aplicabilityViewer, tviewer,subReq,act);
			}
		}
		
	
	}
	public void UpdateCriticality(CheckboxTreeViewerExt criticalityViewer, CheckboxTreeViewerExt aplicabilityViewer,CheckboxTreeViewerExt tviewer,EList<RefApplicability> applicabilities, RefActivity act, RefRequirement req){
		
		//Is there is not applicabilities  related to the element,it will be checked if it's parent refActivity is checked
		if(applicabilities.size()==0){		
			if(req!=null){
				tviewer.setChecked(req, tviewer.getChecked(act));
			}
		}
		
		Iterator<RefApplicability> applIterator = applicabilities.iterator();		
		while(applIterator.hasNext()){
			RefApplicability appli = applIterator.next();
			RefAssurableElement target = appli.getApplicTarget();
			if (target!=null) {			
				tviewer.setChecked(target, false);
				//tviewer.setChildrenStatus(target,false);
				EList<RefCriticalityApplicability> critAppli = appli.getApplicCritic();
				Iterator<RefCriticalityApplicability> critAppliIterator = critAppli.iterator();
				while(critAppliIterator.hasNext()){
					RefCriticalityApplicability critApp = critAppliIterator.next();
					RefCriticalityLevel criticalityLevel = critApp.getCriticLevel();
					RefApplicabilityLevel applicabilityLevel = critApp.getApplicLevel();
					boolean statusCrit = criticalityViewer.getChecked(criticalityLevel);
					boolean statusApp =  aplicabilityViewer.getChecked(applicabilityLevel);
					if(statusCrit && statusApp ){
						tviewer.setChecked(target, true);
						//tviewer.setChildrenStatus(target,true);
					}
					
					//ALC
					/*if(target instanceof RefActivity){
						EList<RefCriticalityApplicability> critAppli = appli.getApplicCritic();
						Iterator<RefCriticalityApplicability> critAppliIterator = critAppli.iterator();
						EList<RefApplicability> applicabilities2 =((RefActivity) target).getApplicability();
					}
					if(target instanceof RefRequirement){*
						
					}*/
					
				}
					
			}	
			else if (act!=null){
				EList<RefCriticalityApplicability> critAppli = appli.getApplicCritic();
				Iterator<RefCriticalityApplicability> critAppliIterator = critAppli.iterator();
				while(critAppliIterator.hasNext()){
					RefCriticalityApplicability critApp = critAppliIterator.next();
					RefCriticalityLevel criticalityLevel = critApp.getCriticLevel();
					RefApplicabilityLevel applicabilityLevel = critApp.getApplicLevel();
					boolean statusCrit = criticalityViewer.getChecked(criticalityLevel);
					boolean statusApp =  aplicabilityViewer.getChecked(applicabilityLevel);
					if(statusCrit && statusApp ){						
						tviewer.setChecked(act, true);						
						//tviewer.setChildrenStatus(act,true);
						if(req!=null){
							tviewer.setChecked(req, true);						
							//tviewer.setChildrenStatus(req,true);
						}
						
					}
					else{
						tviewer.setChecked(act, false);
						//tviewer.setChildrenStatus(act,false);
						
						if(req!=null){
							tviewer.setChecked(req, false);						
						//	tviewer.setChildrenStatus(req,false);
						}
					}
				}
			}
		}

	}

	
	
	public Object[] getSelectedRefReq(){
		return results;
	}
	
	public Object[] getSelectedCritLevels(){
		return criticalityViewer.getCheckedElements();
	}
	
	public Object[] getSelectedApliLevels(){
		return aplicabilityViewer.getCheckedElements();
	}
}
