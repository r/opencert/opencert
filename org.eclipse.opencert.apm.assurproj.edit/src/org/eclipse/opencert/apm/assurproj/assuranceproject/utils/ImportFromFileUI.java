/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.assuranceproject.utils;

import java.util.HashMap;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.etl.CDOTransformFileHandler;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;


public class ImportFromFileUI extends Dialog {
	
	private static final String EVIDENCE_Extension = ".evidence";
	private static final String PROCESS_Extension = ".process";
	private static final String ARGUMENTATION_Extension = ".arg";
	protected String SOURCE_Extension = ".arg";
	private static final String EVIDENCE_Folder = "EVIDENCE";
	private static final String PROCESSES_Folder = "PROCESSES";
	private static final String ARGUMENTATION_Folder = "ARGUMENTATION";
	protected String SOURCE_Folder="";
	
	private static final String IMPORT = "Import";
	private static final String CLOSE = "Close";

	protected AssuranceprojectPackage dprojectPackage = AssuranceprojectPackage.eINSTANCE;
	protected AssuranceprojectFactory dprojectFactory = dprojectPackage.getAssuranceprojectFactory();
	
	CDOResourceFolder assuranceprojectFolder;
	CDOResourceFolder evidenceFolder;
	CDOResourceFolder procFolder;
	CDOResourceFolder argFolder;
	CDOResourceFolder targetFolder;

	private IEditorPart editor;
	private URI resourceURI;
	private CDOSession sessionCDO = null;
	private CDOTransaction transaction;
	HashMap<Object, Object> options = new HashMap<Object, Object>();
	private static final String UTF_8 = "UTF-8";
			
	private Label sourceLabel;
	private Text sourceText;
	private Button sourceButton;
	private String sourcePath;
	private String sourceDirectory;

	private Label targetLabel;
	private Text targetNameText;
	private Label importedTargetModelPath;

	
	public ImportFromFileUI(Shell parentShell) {
		
		super(parentShell);

		options.put(XMLResource.OPTION_ENCODING, UTF_8);
		editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage().getActiveEditor();
		resourceURI = EditUIUtil.getURI(editor.getEditorInput());
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		transaction = sessionCDO.openTransaction();
	}

	public ImportFromFileUI(IShellProvider parentShell) {
		super(parentShell);
	}
	
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
	    super.createButtonsForButtonBar(parent);

	    Button ok = getButton(IDialogConstants.OK_ID);
	    ok.setText(IMPORT);
	    setButtonLayoutData(ok);

	    Button cancel = getButton(IDialogConstants.CANCEL_ID);
	    cancel.setText(CLOSE);
	    setButtonLayoutData(cancel);
	 }
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		final Composite contents = (Composite) super.createDialogArea(parent);

		GridData contentsGridData = (GridData) contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;
					
		createSourceDialogArea(contents, adapterFactory);
		createTargetDialogArea(contents, adapterFactory);
		
		return contents;
	}

	protected void createSourceDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {
		
		final Composite sourceComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, false, true);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			sourceComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 5;			
			
			sourceComposite.setLayout(layout);
		}
		
		Group groupSource = new Group(sourceComposite, SWT.NULL);
		groupSource.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		groupSource.setText("Source Files");
		GridLayout layoutNew = new GridLayout();
		layoutNew.marginHeight = 10;
		layoutNew.marginWidth = 10;
		layoutNew.numColumns = 3;
		groupSource.setLayout(layoutNew);
		
		Label modelTypeLabel= new Label (groupSource, SWT.NONE);
		modelTypeLabel.setText("Model type:");
		final Combo modelType = new Combo(groupSource, SWT.READ_ONLY);
		modelType.add("Argumentation Model");
		modelType.add("Evidence Model");
		modelType.add("Process Model");
		modelType.setLayoutData(new GridData(SWT.FILL,SWT.NONE,true,true,2,1));
		modelType.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				if (modelType.getSelectionIndex() != -1 ) {
					switch (modelType.getSelectionIndex()){
					case 0:
						SOURCE_Extension=ARGUMENTATION_Extension;
						SOURCE_Folder= ARGUMENTATION_Folder;
						break;
					case 1:
						SOURCE_Extension=EVIDENCE_Extension;
						SOURCE_Folder= EVIDENCE_Folder;
						break;
					case 2:
						SOURCE_Extension=PROCESS_Extension;
						SOURCE_Folder= PROCESSES_Folder;
						break;
					}
					sourceButton.setEnabled(true);
					
				}
			}
		});
		
		// Label Source
		sourceLabel = new Label(groupSource, SWT.NONE);
		sourceLabel.setText("Source File:");
		GridData sourceLabelGridData = new GridData();		
		sourceLabelGridData.horizontalAlignment = SWT.FILL;
		sourceLabelGridData.verticalAlignment = SWT.FILL;
		sourceLabel.setLayoutData(sourceLabelGridData);
		// Text Source
		sourceText = new Text(groupSource, SWT.BORDER);
		GridData sourceTextGridData = new GridData();
		sourceTextGridData.grabExcessHorizontalSpace = true;
		sourceTextGridData.minimumWidth = 500;
		sourceTextGridData.horizontalAlignment = SWT.FILL;
		sourceTextGridData.verticalAlignment = SWT.FILL;
		sourceText.setLayoutData(sourceTextGridData);
		sourceText.setEditable(true);
		// Button Source
		sourceButton = new Button(groupSource, SWT.NONE);
		sourceButton.setText("Browse...");
		sourceButton.setEnabled(false);
		sourceButton.addSelectionListener(new SelectionAdapter() {
	        public void widgetSelected(SelectionEvent event) {
	          FileDialog dlg = new FileDialog(sourceButton.getShell(), SWT.None);

	          // Set the initial filter path according
	          // to anything they've selected or typed in
	          dlg.setFilterPath(sourceDirectory);

	          // Change the title bar text
	          dlg.setText("Select a File");
	         // String[] sourceFilterExt = {"*"+ARGUMENTATION_Extension}; 
	          
	          String[] sourceFilterExt = {"*"+SOURCE_Extension}; 
	          dlg.setFilterExtensions(sourceFilterExt);

	          // Calling open() will open and run the dialog.
	          // It will return the selected directory, or
	          // null if user cancels
	          sourcePath = dlg.open();
	          sourceDirectory = dlg.getFilterPath();
	          String sourceFileName = dlg.getFileName();
	          if (sourcePath != null) {
	            // Set the text box to the new selection
	        	sourceText.setText(sourcePath);
	        	setDefaultTargetModelNames(sourceFileName);
	          }
	        }
	      });
	}
		
	protected void setDefaultTargetModelNames (final String fileName){
		targetNameText.setText(fileName);		
	}
	
	protected void createTargetDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {

		
		final Composite targetComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, false, true);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			targetComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 5;			
			
			targetComposite.setLayout(layout);
		}
		
		Group groupTarget = new Group(targetComposite, SWT.NULL);
		groupTarget.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		groupTarget.setText("Imported Models");
		GridLayout layoutNew = new GridLayout();
		layoutNew.marginHeight = 10;
		layoutNew.marginWidth = 10;
		layoutNew.numColumns = 3;
		groupTarget.setLayout(layoutNew);
		
		Label modelLabel = new Label(groupTarget, SWT.NONE);
		modelLabel.setText("");
		GridData modelLabelGridData = new GridData();		
		modelLabelGridData.horizontalAlignment = SWT.FILL;
		modelLabelGridData.verticalAlignment = SWT.FILL;
		modelLabel.setLayoutData(modelLabelGridData);
		
		Label nameLabel = new Label(groupTarget, SWT.NONE);
		nameLabel.setText("Import using Name:");
		GridData nameLabelGridData = new GridData();		
		nameLabelGridData.horizontalAlignment = SWT.FILL;
		nameLabelGridData.verticalAlignment = SWT.FILL;
		nameLabel.setLayoutData(nameLabelGridData);
		
		Label importedLabel = new Label(groupTarget, SWT.NONE);
		String projectPath = resourceURI.path().substring(0, resourceURI.path().lastIndexOf("/"));
		projectPath=projectPath.substring(0, projectPath.lastIndexOf("/"));
		importedLabel.setText("Result @" + projectPath);
		GridData importedLabelGridData = new GridData();		
		importedLabelGridData.horizontalAlignment = SWT.FILL;
		importedLabelGridData.verticalAlignment = SWT.FILL;
		importedLabel.setLayoutData(importedLabelGridData);

		targetLabel = new Label(groupTarget, SWT.NONE);
		targetLabel.setText("Destination model:");
		GridData targetLabelGridData = new GridData();		
		targetLabelGridData.horizontalAlignment = SWT.FILL;
		targetLabelGridData.verticalAlignment = SWT.FILL;
		targetLabel.setLayoutData(targetLabelGridData);

		// Target Name Text 
		targetNameText = new Text(groupTarget, SWT.BORDER);
		GridData targetNameTextGridData = new GridData();
		targetNameTextGridData.grabExcessHorizontalSpace = true;
		targetNameTextGridData.minimumWidth = 200;
		targetNameText.setLayoutData(targetNameTextGridData);
		targetNameText.setEditable(true);

		// Path of imported destination model
		importedTargetModelPath = new Label(groupTarget, SWT.BORDER);
		GridData importedTargetModelPathGridData = new GridData();		
		importedTargetModelPathGridData.grabExcessHorizontalSpace = true;
		importedTargetModelPathGridData.minimumWidth = 350;
		importedTargetModelPathGridData.horizontalAlignment = SWT.FILL;
		importedTargetModelPathGridData.verticalAlignment = SWT.FILL;
		importedTargetModelPath.setLayoutData(importedTargetModelPathGridData);
	}

	protected void setImportBottom(){
		if((sourceText.getText().length()==0)){

		}		
	}
	  		
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Import from File");
	}
	
	@Override
	protected void okPressed() {
		boolean allOK=true;
		if((sourceText.getText().length()==0)){
			MessageDialog.openError(getShell(), "Source Files",
					"You must select the source files.");
			return;
		}

		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		dialog.open();
		IProgressMonitor monitor = dialog.getProgressMonitor();
		monitor.beginTask("Import File Information", 5);
		
		monitor.worked(1);

		try {
			runImportFromFile(monitor, resourceURI);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	
		monitor.worked(5);		
		monitor.done();
		dialog.close();

		if(allOK){
			MessageDialog.openInformation(getShell(), "Import success", "Selected files imported");
			getButton(IDialogConstants.OK_ID).setEnabled(false); 
		}
	}

					
	@Override
	public boolean close() {		
		//Refresh the Repository explorer view
		DawnExplorer repoView =null;
		IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if ("org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer".equals(viewReferences[i].getId())) {
				repoView = (DawnExplorer)viewReferences[i].getView(false);
				repoView.refreshViewer(false);
				break;
			}
		}
		if (!transaction.isClosed()) transaction.close();
		return super.close();
	}		
	

	private void runImportFromFile(			
			final IProgressMonitor monitor, final URI projectModel) throws ExecutionException {

		CDOResource assuranceprojectResource=null;

		String path = projectModel.path();
		path=path.substring(0, path.lastIndexOf("/"));
		assuranceprojectFolder= transaction.getResourceFolder(path);
		
		String targetFolderPath = assuranceprojectFolder.getPath().substring(0, assuranceprojectFolder.getPath().lastIndexOf("/"));
		targetFolderPath=targetFolderPath + "/" + SOURCE_Folder;
		targetFolder= transaction.getResourceFolder(targetFolderPath);

		URI targetModelURI=null;
		CDOResource targetModelResource=null;

//		targetModelResource  = transaction.getOrCreateResource(targetFolder.getPath() + "/" + targetNameText.getText() + SOURCE_Extension);
		targetModelResource  = transaction.getOrCreateResource(targetFolder.getPath() + "/" + targetNameText.getText());
		targetModelURI = targetModelResource.getURI();
		try {

			synchronized (transaction)
			{
//				CDOTransformFileHandler transfo = new CDOTransformFileHandler(assuranceprojectFolder, sourceText.getText(), targetModelResource,transaction);
				CDOTransformFileHandler transfo = new CDOTransformFileHandler(assuranceprojectFolder, sourceText.getText(), targetModelResource,transaction, SOURCE_Extension);
				transfo.execute();
				transaction.commit();
				String targetSubPath=targetModelURI.path().substring(targetModelURI.path().indexOf("/")+1);
				targetSubPath=targetSubPath.substring(targetSubPath.indexOf("/"));
				importedTargetModelPath.setText(targetSubPath);
				monitor.worked(4);
			}
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			String msgError="Issues at importing the model:";
			importedTargetModelPath.setText(msgError + e.getMessage());		
			monitor.done();
		}

		try {
			
			assuranceprojectResource  = transaction.getResource(projectModel.path());
			EList<EObject> contents = assuranceprojectResource.getContents();

			EObject rootObject =contents.get(0);
			AssuranceProject assurproj = (AssuranceProject)rootObject;

			AssetsPackage assPackageconfig = null;
			for (AssetsPackage assetPackage: assurproj.getAssetsPackage()) {
				if(assetPackage.isIsActive()){
					assPackageconfig=assetPackage;
				}
			}		

			CDOResource targetResource = transaction.getResource(targetModelURI.path());
			EList<EObject> targetContents = targetResource.getContents();
			EObject targetRootObject =targetContents.get(0);
			if(SOURCE_Extension.equals(ARGUMENTATION_Extension)){
				Case argModel = (Case)targetRootObject;
				assPackageconfig.getArgumentationModel().add(argModel);
			}
			if(SOURCE_Extension.equals(EVIDENCE_Extension)){
				ArtefactModel eviArtefactModel = (ArtefactModel)targetRootObject;
				assPackageconfig.getArtefactsModel().add(eviArtefactModel);
			}
			if(SOURCE_Extension.equals(PROCESS_Extension)){
				ProcessModel procProcessModel = (ProcessModel)targetRootObject;
				assPackageconfig.getProcessModel().add(procProcessModel);
			}

			// Save the contents of the resource to the file system.
			assuranceprojectResource.save(options);
			transaction.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MessageDialog.openWarning(getShell(), "Models not Linked", "The generated models cannot be linked to the Assurance Project");
			transaction.rollback();
			monitor.done();
		}	
	}
	
}

