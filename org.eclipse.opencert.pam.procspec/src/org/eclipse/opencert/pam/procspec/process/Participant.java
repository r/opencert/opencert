/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pam.procspec.process;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Participant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Participant#getOwnedArtefact <em>Owned Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Participant#getTriggeredAssetEvent <em>Triggered Asset Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getParticipant()
 * @model
 * @generated
 */
public interface Participant extends AssuranceAsset, DescribableElement {
	/**
	 * Returns the value of the '<em><b>Owned Artefact</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.Artefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Artefact</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Artefact</em>' reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getParticipant_OwnedArtefact()
	 * @model
	 * @generated
	 */
	EList<Artefact> getOwnedArtefact();

	/**
	 * Returns the value of the '<em><b>Triggered Asset Event</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Triggered Asset Event</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triggered Asset Event</em>' reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getParticipant_TriggeredAssetEvent()
	 * @model
	 * @generated
	 */
	EList<AssuranceAssetEvent> getTriggeredAssetEvent();

} // Participant
