/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

public class HtmlProcessor
{
    private static final String WWW_PREFIX = "www.";
    private static final String HTTPS_PREFIX = "https://";
    private static final String HTTP_PREFIX = "http://";
    private static final String _A_HREF_PATTERN = "<a href=\"%s\" title=\"%s\">%s</a>"; //$NON-NLS-1$
    
    private HtmlProcessor()
    {
    }

    public static String processHtmlLinks(String description)
    {
        if (description == null) {
            return null;
        }
        String result = description;
        
        Map<String, String> replacementLinks = generateReplacementLinks(description);
        Iterator<Entry<String, String>> iterator = replacementLinks.entrySet().iterator();
        while (iterator.hasNext())
        {
            Entry<String, String> next = iterator.next();
            result = result.replaceAll(next.getKey(), next.getValue());
        }
        
        return result;
    }

    private static Map<String, String> generateReplacementLinks(String description)
    {
        Map<String, String> result = new HashMap<String, String>();
        
        List<HtmlLink> toBeReplacedLinks = detectHtmlLinkPhrases(description);
        for (HtmlLink link : toBeReplacedLinks)
        {
            final String replaced = String.format(_A_HREF_PATTERN, link.htmlLink, "Open the link", link.visualText);
            result.put(link.visualText, replaced);
        }
        return result;
    }

    private static List<HtmlLink> detectHtmlLinkPhrases(String description)
    {
        List<HtmlLink> result = new LinkedList<>();
        
        String[] phrasesSplittedByWhitespace = StringUtils.split(description);
        
        for (String phraseNoSpace : phrasesSplittedByWhitespace)
        {
            final boolean isLink = startsWithLinkPhrase(phraseNoSpace);
            if (isLink) {
                String htmlLink = ensureNonRelativeLink(phraseNoSpace);
                HtmlLink res = new HtmlLink(phraseNoSpace, htmlLink);
                result.add(res);    
            }
        }
        return result;
    }

    private static boolean startsWithLinkPhrase(String phraseNoSpace)
    {
        phraseNoSpace = phraseNoSpace.toLowerCase();

        return (phraseNoSpace.startsWith(HTTP_PREFIX) ||
                phraseNoSpace.startsWith(HTTPS_PREFIX) ||
                phraseNoSpace.startsWith(WWW_PREFIX)
                );
    }
    
    private static String ensureNonRelativeLink(String phrase)
    {
        if (phrase.toLowerCase().startsWith(HTTP_PREFIX) || phrase.toLowerCase().startsWith(HTTPS_PREFIX))
        {
            return phrase;
        }
        return HTTP_PREFIX + phrase;
    }

    private static class HtmlLink
    {
        final String visualText;
        final String htmlLink;
        
        public HtmlLink(String visualText, String htmlLink)
        {
            this.visualText = visualText;
            this.htmlLink = htmlLink;
        }
    }
}



