/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.opencert.sam.arg.arg.Agreement;

import org.polarsys.chess.contracts.profile.chesscontract.Contract;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contract Agreement Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink#getSources <em>Sources</em>}</li>
 *   <li>{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink#getTargets <em>Targets</em>}</li>
 * </ul>
 *
 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage#getContractAgreementLink()
 * @model
 * @generated
 */
public interface ContractAgreementLink extends EObject {
	/**
	 * Returns the value of the '<em><b>Sources</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sources</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sources</em>' reference.
	 * @see #setSources(Contract)
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage#getContractAgreementLink_Sources()
	 * @model required="true"
	 * @generated
	 */
	Contract getSources();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink#getSources <em>Sources</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sources</em>' reference.
	 * @see #getSources()
	 * @generated
	 */
	void setSources(Contract value);

	/**
	 * Returns the value of the '<em><b>Targets</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.Agreement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Targets</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Targets</em>' reference list.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage#getContractAgreementLink_Targets()
	 * @model required="true"
	 * @generated
	 */
	EList<Agreement> getTargets();

} // ContractAgreementLink
