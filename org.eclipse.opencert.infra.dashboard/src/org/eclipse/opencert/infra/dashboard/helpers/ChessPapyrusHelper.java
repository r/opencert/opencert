/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.helpers;

import java.util.Objects;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.commands.Activator;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.uml2.uml.Model;

/**
 * Helper methods to work with Papyrus CHESS Uml model elements.
 */
public final class ChessPapyrusHelper {

	/** Singleton instance of this class. */
	public static final ChessPapyrusHelper INSTANCE = new ChessPapyrusHelper();

	/** Text for warning there is no active Chess editor. */
	private final String NO_ACTIVE_CHESS_EDITOR_WARNING_TEXT = "Active Papyrus CHESS editor not found"; //$NON-NLS-1$

	/** Message for warning there is no active Chess editor. */
	private final String NO_ACTIVE_CHESS_EDITOR_WARNING_MESSAGE = "At least one Papyrus CHESS editor must be active to create a new diagram. Please open a Papyrus CHESS model."; //$NON-NLS-1$

	/** Name of the model requirement view. */
	public static final String MODEL_REQUIREMENT_VIEW_NAME = "modelRequirementView"; //$NON-NLS-1$

	/** Name of the model system view. */
	public static final String MODEL_SYSTEM_VIEW_NAME = "modelSystemView"; //$NON-NLS-1$

	/**
	 * Default constructor, private to avoid instantiation.
	 */
	private ChessPapyrusHelper() {
		super();
	}

	/**
	 * @return The root UML model element, which is the first node in the resource content
	 */
	public Model getUmlModelFromResource(final Resource pResource) {
		Model vModel = null;

		// Get the current Model from the resource content
		if (!pResource.getContents().isEmpty()) {
			// Get the root object of the resource
			EObject vObject = pResource.getContents().get(0);

			if (vObject instanceof Model) {
				vModel = (Model) vObject;
			}
		}

		return vModel;
	}

	/**
	 * @return The root UML model element, which is the first node in the resource content, or {@code null} if not found
	 */
	public Model getUmlModelFromModelSet(final ModelSet pModelSet) {
		Objects.requireNonNull(pModelSet);

		Model vModel = null;

		// Retrieve the Uml URI from the model set
		URI vUmlURI = pModelSet.getURIWithoutExtension().appendFileExtension("uml"); //$NON-NLS-1$
		Resource vResource = pModelSet.getResource(vUmlURI, false);

		// Get the current model from the resource content
		if (Objects.nonNull(vResource) && !vResource.getContents().isEmpty()) {
			// Get the root object of the resource
			EObject vObject = vResource.getContents().get(0);

			if (vObject instanceof Model) {
				vModel = (Model) vObject;
			}
		}

		return vModel;
	}

	/**
	 * Get the root UML model from the diagram editor.
	 *
	 * @return The found UML {@link Model}, otherwise {@code null}
	 */
	public Model getUmlModelFromDiagramEditor(final IMultiDiagramEditor vPapyrusDiagramEditor) {
		Model vRootModel = null;

		// Try to goto the right package and create the new diagram there
		if (Objects.nonNull(vPapyrusDiagramEditor)) {
			ServicesRegistry vRegistry = vPapyrusDiagramEditor.getServicesRegistry();

			if (Objects.nonNull(vRegistry)) {
				ModelSet vModelSet = null;
				try {
					vModelSet = vRegistry.getService(ModelSet.class);
				} catch (ServiceException pException) {
					Activator.log.error(pException);
				}

				if (Objects.nonNull(vModelSet)) {
					// Get the root model
					vRootModel = ChessPapyrusHelper.INSTANCE.getUmlModelFromModelSet(vModelSet);
				}
			}
		}

		return vRootModel;
	}

	/**
	 * @return The root UML model element, which is the first node in the resource content
	 */
	public org.eclipse.uml2.uml.Package getPackageFromModelByName(final Model pModel, final String pPackageName) {
		Objects.requireNonNull(pModel);
		return pModel.getNestedPackage(pPackageName);
	}

	/**
	 * Open the message box to warn user that no active Papyrus editor is opened.
	 */
	public void openNoActivePapyrusEditorMessageBox() {
		MessageBox vMessageBox = new MessageBox(EclipseHelper.getActiveShell(), SWT.OK | SWT.ICON_WARNING);

		vMessageBox.setText(NO_ACTIVE_CHESS_EDITOR_WARNING_TEXT);
		vMessageBox.setMessage(NO_ACTIVE_CHESS_EDITOR_WARNING_MESSAGE);
		vMessageBox.open();
	}
}
