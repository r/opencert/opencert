/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.pkm.refframework.refframework.diagram.part;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.wizards.DawnCreateNewDiagramResourceWizardPage;
import org.eclipse.emf.cdo.dawn.ui.wizards.DawnCreateNewResourceWizardPage;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

public class DawnRefframeworkCreationWizard extends RefframeworkCreationWizard {
	private CDOView view;

	private DawnCreateNewDiagramResourceWizardPage dawnDiagramModelFilePage;

	private DawnCreateNewResourceWizardPage dawnDomainModelFilePage;

	public DawnRefframeworkCreationWizard() {
		super();
	}

	@Override
	public boolean performFinish() {
		IRunnableWithProgress op = new WorkspaceModifyOperation(null) {
			@Override
			protected void execute(IProgressMonitor monitor)
					throws CoreException, InterruptedException {
				URI diagramResourceURI = dawnDiagramModelFilePage.getURI();
				URI domainModelResourceURI = dawnDomainModelFilePage.getURI();

				diagram = DawnRefframeworkDiagramEditorUtil.createDiagram(
						diagramResourceURI, domainModelResourceURI, monitor);

				if (isOpenNewlyCreatedDiagramEditor() && diagram != null) {
					try {
						DawnRefframeworkDiagramEditorUtil.openDiagram(diagram);
					} catch (PartInitException e) {
						ErrorDialog
								.openError(
										getContainer().getShell(),
										Messages.RefframeworkCreationWizardOpenEditorError,
										null, e.getStatus());
					}
				}
			}
		};
		try {
			getContainer().run(false, true, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			if (e.getTargetException() instanceof CoreException) {
				ErrorDialog.openError(getContainer().getShell(),
						Messages.RefframeworkCreationWizardCreationError, null,
						((CoreException) e.getTargetException()).getStatus());
			} else {
				RefframeworkDiagramEditorPlugin.getInstance().logError(
						"Error creating diagram", e.getTargetException()); //$NON-NLS-1$
			}
			return false;
		}
		return diagram != null;
	}

	@Override
	public void addPages() {
		// Create new pages only if resource wizard pages do not exist
		if (getPageCount() > 0) {
			return;
		}

		// The CDO view need to be initialised before creating wizard pages
		initCDOView();

		dawnDiagramModelFilePage = new DawnCreateNewDiagramResourceWizardPage(
				"refframework_diagram", false, view);
		dawnDiagramModelFilePage
				.setTitle(Messages.RefframeworkCreationWizard_DiagramModelFilePageTitle);
		dawnDiagramModelFilePage
				.setDescription(Messages.RefframeworkCreationWizard_DiagramModelFilePageDescription);
		dawnDiagramModelFilePage.setCreateAutomaticResourceName(true);
		addPage(dawnDiagramModelFilePage);

		dawnDomainModelFilePage = new DawnCreateNewResourceWizardPage(
				"refframework", false, view) {
			@Override
			public void setVisible(boolean visible) {
				if (visible) {
					URI uri = dawnDiagramModelFilePage.getURI();
					String fileName = uri.lastSegment();
					fileName = fileName.substring(0, fileName.length()
							- ".refframework_diagram".length()); //$NON-NLS-1$
					fileName += ".refframework";
					dawnDomainModelFilePage.setResourceNamePrefix(fileName);
					dawnDomainModelFilePage
							.setResourcePath(dawnDiagramModelFilePage
									.getResourcePath());
				}
				super.setVisible(visible);
			}
		};
		dawnDomainModelFilePage
				.setTitle(Messages.RefframeworkCreationWizard_DomainModelFilePageTitle);
		dawnDomainModelFilePage
				.setDescription(Messages.RefframeworkCreationWizard_DomainModelFilePageDescription);

		dawnDomainModelFilePage
				.setResourceValidationType(DawnCreateNewResourceWizardPage.VALIDATION_WARN);
		addPage(dawnDomainModelFilePage);
	}

	/**
	 * Initialise the CDO view.
	 */
	protected void initCDOView() {
		CDOConnectionUtil.instance.init(PreferenceConstants.getRepositoryName(), PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession session = CDOConnectionUtil.instance.getCurrentSession();
		view = CDOConnectionUtil.instance.openView(session);
	}

	@Override
	public void dispose() {
		view.close();
	}
}
