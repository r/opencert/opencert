/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API, implementation and maintenance and upgrades
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.policies;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DiagramDragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.opencert.sam.arg.ui.util.UtilConstants;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Location;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.diagram.DnD.ExpandBarDropEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.DnD.PatternDropEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.LoadViewLocations;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.RefreshMultiDiagramCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.RepairCDOViewLocationsCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.RepairDawnViewLocationsCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.RepairViewLocationsCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

/**
 * @generated NOT
 */
/**
 * The Class CaseSemiCanonicalOpenEditPolicy.
 * 
 * @author  M� Carmen Palacios
 * @author  Alejandra Ruiz
 */
public class CaseOpencertDragDropEditPolicy extends DiagramDragDropEditPolicy  {

	private TransactionalEditingDomain editingDomain;	
	private Object model;

	
	public CaseOpencertDragDropEditPolicy(TransactionalEditingDomain ed, Object mo) {
		super();
		editingDomain = ed;
		model = mo;
	}
	
	
	public Command getDropObjectsCommand(
			DropObjectsRequest dropRequest) {
		String sourceFile="";
		LoadViewLocations l=null;
		List<EObject> nodelist = new ArrayList();		
		List<EObject> choicelist = new ArrayList();
		List<EObject> list = new ArrayList();		
		Command res=null;
		EObject rootObject=null;
		for (Iterator<?> it = dropRequest.getObjects()
				.iterator(); it.hasNext();) {
			Object nextObject = it.next();
			// Changes ARL
			//We have a Diagram object saved in CDO
			if (nextObject instanceof Diagram){
				CDOResource e=(CDOResource)((Diagram) nextObject).eResource();
				sourceFile= e.getPath();
				l=new LoadViewLocations(e);
				PatternDropEditPolicy eb = new PatternDropEditPolicy(getHost());
				res=eb.getDropObjectsCommand(dropRequest);
				choicelist = eb.getChoiceNodesInDrop();
				nodelist = eb.getNodesInDrop();
				break;
			}
			//End ARL
			//si se desea instanciar un fichero pattern
			if(nextObject instanceof String) 
			{
				sourceFile = (String)nextObject;
				ExpandBarDropEditPolicy eb = new ExpandBarDropEditPolicy(getHost());
				res = eb.getDropObjectsCommand(dropRequest);
				choicelist = eb.getChoiceNodesInDrop();
				nodelist = eb.getNodesInDrop();
				break;				
			}
			// si copiamos un modelo....
			else if (nextObject instanceof EObject) 
			{
				rootObject = (EObject)nextObject;
				list.add(rootObject);
			}
		}
	    if(!list.isEmpty())
	    {
			res = new ICommandProxy(
					new RefreshMultiDiagramCommand(list, editingDomain, (View)model, getHost()));

	    }
	    if (l!=null){
	    	Point offset = new Point(dropRequest.getLocation());
	    	res = res.chain(new ICommandProxy(
					new RepairCDOViewLocationsCommand(l,offset, nodelist, editingDomain, (View)model, getHost())));
	    }
	    else
	    {
	    	/*
	    	res = res.chain(new ICommandProxy(
					new RepairChoiceOPCommand(choicelist, editingDomain, (View)model, getHost())));
			*/
			Point offset = new Point(dropRequest.getLocation());

	    	res = res.chain(new ICommandProxy(
					new RepairViewLocationsCommand(sourceFile, offset, nodelist, editingDomain, (View)model, getHost())));

	    	/* Dawn cdo. Ya no es necesario se hace todo en RepairViewLocationsCommand
	    	res = res.chain(new ICommandProxy(
					new RepairChoiceOPCommand(sourceFile, offset, choicelist, editingDomain, (View)model, getHost())));
			*/
	    }
		return res;
	}
}
