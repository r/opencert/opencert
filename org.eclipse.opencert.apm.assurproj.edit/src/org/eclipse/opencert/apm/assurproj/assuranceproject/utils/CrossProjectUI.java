/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.assuranceproject.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;














import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.CDOObjectReference;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.common.util.CDOException;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.ShapeImpl;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.ModelElement;



public class CrossProjectUI extends Dialog {

	// MCP CrossProject	
	private static final String EXTENSION_DIAGRAM = "_diagram";
	private Map<String, EObject> eIDToObjectMap = new HashMap<String, EObject>(); // only for arg model elements
	// MCP CrossProject	

	protected ArrayList<String> sResult = null;
	

	private IEditorPart editor;
	private URI resourceURI;
	private CDOSession sessionCDO = null;

	private CDOResource resourceAPFrom = null; 		// Resource Source Assurance project
	private AssuranceProject assuranceProjectFrom;	//model Source Assurance project
	private CDOResource resourceAPTarget = null; 
	private AssuranceProject assuranceProjectTarget;//model Target Assurance project
	private CDOResource artefactResource = null;
			
	private CDOView viewCDO = null;
	
	private CheckboxTreeViewerExt checktreeBaselineSource;
	private ResourceSet baselineSourceResourceSet;
	private ResourceSet evidenceSourceResourceSet;
	private CheckboxTreeViewerExt checktreeArgSource;
	private ResourceSet argSourceResourceSet;
	private CDOResource argResource = null;
	
	private CheckboxTreeViewerExt checktreeProcessSource;
	private ResourceSet processSourceResourceSet;
	private CDOResource processResource = null;
	
	private Text projectsourceText;
	private Label sourceLabel;
	private CheckboxTreeViewerExt checktreeEvidenceSource;
	
			
	private Label targetLabel;
	private Text projecttargetText;


	private Button checkBaseline;


	private Button checkArtefacts;


	private Button checkArguments;


	private Button checkProcess;


	private CDOTransaction transaction;
	
	private  Map<EObject, EObject> map = new HashMap<EObject, EObject>();
	
	HashMap<Object, Object> options = new HashMap<Object, Object>();


	private AssetsPackage targetAssetsPackage;
	
	protected AssuranceprojectPackage projectPackage = AssuranceprojectPackage.eINSTANCE;
	protected AssuranceprojectFactory projectFactory = projectPackage.getAssuranceprojectFactory();
	
	public CrossProjectUI(Shell parentShell) {
		
		super(parentShell);

		options.put(XMLResource.OPTION_ENCODING, "UTF-8");
		
		editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage().getActiveEditor();
		resourceURI = EditUIUtil.getURI(editor.getEditorInput());

		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		
		CDOView [] views = sessionCDO.getViews();
		if (views.length!=0) {
			viewCDO = views[0];				
		}
		
		transaction = sessionCDO.openTransaction();
		

		//setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);	
		
		
	}

	public CrossProjectUI(IShellProvider parentShell) {
		super(parentShell);
	}
	
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
	    super.createButtonsForButtonBar(parent);

	    Button ok = getButton(IDialogConstants.OK_ID);
	    ok.setText("Reuse");
	    ok.setEnabled(false);
	    setButtonLayoutData(ok);

	    Button cancel = getButton(IDialogConstants.CANCEL_ID);
	    cancel.setText("Close");
	    setButtonLayoutData(cancel);
	 }
	
	@Override
	protected Control createDialogArea(final Composite parent) {

		
	
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		final Composite contents = (Composite) super.createDialogArea(parent);

		GridData contentsGridData = (GridData) contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;

		// Default filter
	//	sElementFilter = "Artefact";
					
		createSourceDialogArea(contents, adapterFactory);

		//parent.getShell().setSize(800, 600);
		
		return contents;
	}

	protected void createSourceDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {

		
		final Composite sourceComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, false, true);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			sourceComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 10;			
			
			sourceComposite.setLayout(layout);
		}

		

		final Composite compositeSourceProject = new Composite(sourceComposite, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, false, true);
			dataNew.horizontalAlignment = SWT.END;
			compositeSourceProject.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			compositeSourceProject.setLayout(layoutNew);
		}
		
		// Label
		targetLabel = new Label(compositeSourceProject, SWT.NONE);
		targetLabel.setText("Target Project:");
		GridData targetLabelGridData = new GridData();		
		targetLabelGridData.horizontalAlignment = SWT.FILL;
		targetLabelGridData.verticalAlignment = SWT.FILL;
		targetLabel.setLayoutData(targetLabelGridData);
		// Text
		projecttargetText = new Text(compositeSourceProject, SWT.NONE);
		
		GridData nameTextGridData = new GridData();
		nameTextGridData.grabExcessHorizontalSpace = true;
		nameTextGridData.minimumWidth = 50;
		nameTextGridData.horizontalAlignment = SWT.FILL;
		nameTextGridData.verticalAlignment = SWT.FILL;
		projecttargetText.setLayoutData(nameTextGridData);
		//projecttargetText.setText(resourceURI.toString());
		
				
		String path = "";
		for (int i = 0; i< resourceURI.segmentCount(); i++ ) {
			path = path + resourceURI.segment(i) + "/";
		}
		
		path = path.substring(0,path.length()-1);
		projecttargetText.setText(path);
		projecttargetText.setEditable(false);
				
		
		// Label
		sourceLabel = new Label(compositeSourceProject, SWT.NONE);
		sourceLabel.setText("Source Project:");
		GridData sourceLabelGridData = new GridData();
		sourceLabelGridData.horizontalAlignment = SWT.FILL;
		sourceLabelGridData.verticalAlignment = SWT.FILL;
		sourceLabel.setLayoutData(sourceLabelGridData);
		// Text
		projectsourceText = new Text(compositeSourceProject, SWT.NONE);		
		projectsourceText.setLayoutData(nameTextGridData);
		projectsourceText.setEditable(false);
		//Button
		Button selectSourceProject = new Button(compositeSourceProject, SWT.NONE);
		selectSourceProject.setText("Search...");
		GridData NewGroupMapGridData = new GridData();
		NewGroupMapGridData.verticalAlignment = SWT.BEGINNING;
		NewGroupMapGridData.horizontalAlignment = SWT.FILL;
		selectSourceProject.setLayoutData(NewGroupMapGridData);

		selectSourceProject.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				
				ListAssuranceProject listAssuranceProject = new ListAssuranceProject(compositeSourceProject.getShell(), sessionCDO);
				if (listAssuranceProject.open() == Window.OK) {
					sResult = new ArrayList<String>();
					sResult.add(listAssuranceProject.getResult().get(0));
					sResult.add(listAssuranceProject.getResult().get(1));
					
					projectsourceText.setText(listAssuranceProject.getResult().get(1));
					
					try {	
						resourceAPFrom = viewCDO.getResource(sResult.get(1));						
						resourceAPTarget= transaction.getResource(projecttargetText.getText());
						
						assuranceProjectFrom = (AssuranceProject) resourceAPFrom.getContents().get(0);
						assuranceProjectTarget= (AssuranceProject) resourceAPTarget.getContents().get(0);
						
						//Load Source Baselines
						EList<BaselineConfig> lstBaselineConfig = assuranceProjectFrom.getBaselineConfig();
						Iterator<BaselineConfig> baselineConfig = lstBaselineConfig.iterator();
						
						baselineSourceResourceSet = new ResourceSetImpl();
						
						boolean bFind = false;
						while (baselineConfig.hasNext() && !bFind) {
							BaselineConfig bconfig = baselineConfig.next();
							if (bconfig.isIsActive()) {
								bFind = true;
								
								EList<BaseFramework> lstBaseFramework = bconfig.getRefFramework();										
								Iterator<BaseFramework> itbaseFramework = lstBaseFramework.iterator();
																
								while (itbaseFramework.hasNext()) {
																		
									BaseFramework baseFramework = itbaseFramework.next();									
									try {
										baselineSourceResourceSet.getResources().add(baseFramework.eResource());
									} catch (NullPointerException e1) {
										// TODO Auto-generated catch block
										//e1.printStackTrace();
									}
								}
								//treeBaselineSource.setInput(baselineSourceResourceSet);
								
								CreateModelFromResourceSet(checktreeBaselineSource,baselineSourceResourceSet);
								
								//To search the nodes to be moved.
								checktreeBaselineSource.expandAll();
																						
								//To expand the moved nodes.
								//treeBaselineSource.expandAll();
								
								
							}
						}
						
						//Load the evidence models of the active assetsPackage of the selected assurance project 
						EList<AssetsPackage> lstAssetsPackage = assuranceProjectFrom.getAssetsPackage();
						Iterator<AssetsPackage> assetsPackage = lstAssetsPackage.iterator();
						
						evidenceSourceResourceSet = new ResourceSetImpl();
						AssetsPackage aPackage=null;
						
						 bFind = false;
						while (assetsPackage.hasNext() && !bFind) {
							aPackage = assetsPackage.next();
							if (aPackage.isIsActive()) {
								bFind= true;
								EList<ArtefactModel> lstArtefactModel = aPackage.getArtefactsModel();
								Iterator<ArtefactModel> artefactModel = lstArtefactModel.iterator();
								while (artefactModel.hasNext()) {
									ArtefactModel artefact = artefactModel.next();
									artefactResource = CDOUtil.getCDOObject(artefact).cdoResource();
									try {
										evidenceSourceResourceSet.getResources().add(artefactResource);
									} catch (NullPointerException e1) {
										// TODO Auto-generated catch block
										//e1.printStackTrace();
									}
								}							
								CreateModelFromResourceSet(checktreeEvidenceSource,evidenceSourceResourceSet);
								checktreeEvidenceSource.expandAll();
							}
						}	
						
						
						//Load the argumentation models of the active assetsPackage of the selected assurance project 												
						argSourceResourceSet = new ResourceSetImpl();
																		
						EList<Case> lstArgModel = aPackage.getArgumentationModel();
						Iterator<Case> argModel = lstArgModel.iterator();
						while (argModel.hasNext()) {
							Case argRoot = argModel.next();
							argResource = CDOUtil.getCDOObject(argRoot).cdoResource();
							try {
								argSourceResourceSet.getResources().add(argResource);
							} catch (NullPointerException e1) {
								// TODO Auto-generated catch block
								//e1.printStackTrace();
							}
						}							
						CreateModelFromResourceSet(checktreeArgSource,argSourceResourceSet);
						checktreeArgSource.expandAll();						
						
						
						//Load the process models of the active assetsPackage of the selected assurance project 												
						processSourceResourceSet = new ResourceSetImpl();
																						
						EList<ProcessModel> lstProcessModel = aPackage.getProcessModel();
						Iterator<ProcessModel> processModel = lstProcessModel.iterator();
						while (processModel.hasNext()) {
							ProcessModel processRoot = processModel.next();
							processResource = CDOUtil.getCDOObject(processRoot).cdoResource();
							try {
								processSourceResourceSet.getResources().add(processResource);
							} catch (NullPointerException e1) {
								// TODO Auto-generated catch block
								//e1.printStackTrace();
							}
						}							
						CreateModelFromResourceSet(checktreeProcessSource,processSourceResourceSet);
						checktreeProcessSource.expandAll();							
						
						getButton(IDialogConstants.OK_ID).setEnabled(true);
						
						checkBaseline.setSelection(true);
						
						defaultSelection();
						
					} catch (CDOException cdoe) {
						cdoe.printStackTrace();
					}	
				}
			}
		});
		
		
		Label dummy = new Label(compositeSourceProject, SWT.NONE);
		dummy.setText("");
		
		
		checkBaseline= new Button(compositeSourceProject, SWT.CHECK);
		checkBaseline.setText("Copy Baseline models");
		//checkBaseline.setSelection(false);
		
		checkBaseline.addSelectionListener(new SelectionListener(){
			public void widgetSelected(SelectionEvent e){
				Widget widget=e.widget;
				if (widget instanceof Button) {
					Button button=(Button)widget;
					if(button.getSelection()){	
						
						defaultSelection();
					}
					else{						
						for(TreeItem root:checktreeBaselineSource.getTree().getItems()){
							root.setChecked(false);
						}
						checktreeBaselineSource.getTree().setEnabled(false);
						
						checkArtefacts.setSelection(true);		
						checkArtefacts.setEnabled(true);
						for(TreeItem root:checktreeEvidenceSource.getTree().getItems()){
							root.setChecked(true);
						}
						checktreeEvidenceSource.getTree().setEnabled(true);
												
						checkArguments.setSelection(false);		
						checkArguments.setEnabled(false);
						for(TreeItem root:checktreeArgSource.getTree().getItems()){
							root.setChecked(false);
						}
						checktreeArgSource.getTree().setEnabled(false);
												
						checkProcess.setSelection(false);		
						checkProcess.setEnabled(false);
						for(TreeItem root:checktreeProcessSource.getTree().getItems()){
							root.setChecked(false);
						}
						checktreeProcessSource.getTree().setEnabled(false);
					}
				}
			}
			public void widgetDefaultSelected(SelectionEvent e){
			}
		});
		
		checkBaseline.setEnabled(true);
	    
		
		//Load the baseline
		GridData modelViewerGridData = new GridData();
		modelViewerGridData.verticalAlignment = SWT.FILL;
		modelViewerGridData.horizontalAlignment = SWT.FILL;
		modelViewerGridData.widthHint = 400;
		//modelViewerGridData.heightHint = 400;
		modelViewerGridData.grabExcessHorizontalSpace = true;
		modelViewerGridData.grabExcessVerticalSpace = true;
		
		checktreeBaselineSource = new CheckboxTreeViewerExt(compositeSourceProject,
				SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		
		checktreeBaselineSource.getTree().setLayoutData(modelViewerGridData);
		checktreeBaselineSource.setContentProvider(new AdapterFactoryContentProvider(
				adapterFactory));
		checktreeBaselineSource.setLabelProvider(new AdapterFactoryLabelProvider(
				adapterFactory));
		checktreeBaselineSource.expandAll();
				
		checktreeBaselineSource.getTree().setEnabled(false);
		
		// TreeViewer for Artefact models
		
		checkArtefacts= new Button(compositeSourceProject, SWT.CHECK);
		checkArtefacts.setText("Copy Evidence Models");
		checkArtefacts.setSelection(false);		
		checkArtefacts.setEnabled(true);
		
		
		checktreeEvidenceSource = new CheckboxTreeViewerExt(compositeSourceProject, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
				
		checktreeEvidenceSource.getTree().setLayoutData(modelViewerGridData);
		checktreeEvidenceSource.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		checktreeEvidenceSource.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		checktreeEvidenceSource.getTree().setEnabled(false);
		// TreeViewer for Argumentation models
		
		checkArguments= new Button(compositeSourceProject, SWT.CHECK);
		checkArguments.setText("Copy Argumentation Models");
		checkArguments.setSelection(false);		
		checkArguments.setEnabled(false);
		
		checktreeArgSource = new CheckboxTreeViewerExt(compositeSourceProject, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
				
		checktreeArgSource.getTree().setLayoutData(modelViewerGridData);
		checktreeArgSource.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		checktreeArgSource.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
				
		checktreeArgSource.getTree().setEnabled(false);
		
		// TreeViewer for Process models
		
		checkProcess= new Button(compositeSourceProject, SWT.CHECK);
		checkProcess.setText("Copy Process Models");
		checkProcess.setSelection(false);		
		checkProcess.setEnabled(false);
		
		checktreeProcessSource = new CheckboxTreeViewerExt(compositeSourceProject, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
				
		checktreeProcessSource.getTree().setLayoutData(modelViewerGridData);
		checktreeProcessSource.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		checktreeProcessSource.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
						
		
		checktreeProcessSource.getTree().setEnabled(false);
	}
		
					
	
	
	
	public void CreateModelFromResourceSet(TreeViewer tree ,ResourceSet resourceSet) {

		try {
			tree.setInput(resourceSet);
			
			final ViewerFilter modelFilter = new ViewerFilter() {
				public boolean select(Viewer viewer, Object parentElement,
						Object element) {
					
					//if (sElementFilter.contentEquals("Artefact")) {
						if (element instanceof CDOResource) 
							return true;						
						else
							return false;
					//} else
					//	return false;
				}
			};
			tree.addFilter(modelFilter);
		} catch (Exception ex) {
			MessageDialog
					.openError(getShell(), "Not valid model file",
							"The provided model file couldn't be parsed as an EMF resource");
			tree.setInput(null);
		}
	}
			
	  		
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Cross Project");
	}
	
	@Override
	protected void okPressed() {
		boolean allOK=true;
		if(projectsourceText.getText().length()==0){
			MessageDialog.openError(getShell(), "Selected Source Project",
					"You must select the source project of the reuse.");
			return;
		}

				
		//Load the evidence models of the active assetsPackage of the selected assurance project 
		EList<AssetsPackage> lstAssetsPackage = assuranceProjectTarget.getAssetsPackage();
		Iterator<AssetsPackage> assetsPackage = lstAssetsPackage.iterator();
								
		
		while (assetsPackage.hasNext()) {
			targetAssetsPackage = assetsPackage.next();
			if (targetAssetsPackage.isIsActive()) {
				break;				
			}
		}
		
		if(checkBaseline.getSelection()){			
			
			MessageDialog.openWarning(getShell(), "Changes to target Assurance Project",
					"The model will be copied and related to a new Asset Package and Baseline Configuration that will be set up as the active ones.");
			
			AssetsPackage newAP= projectFactory.createAssetsPackage();								
			newAP.setId("" + assuranceProjectTarget.getAssetsPackage().size());
			newAP.setName("AP_" + assuranceProjectTarget.getAssetsPackage().size());
			newAP.setDescription("Created automatically for Cross Project Reuse");			
			newAP.setIsActive(true);
			assuranceProjectTarget.getAssetsPackage().add(newAP);
			targetAssetsPackage.setIsActive(false);
			targetAssetsPackage=newAP;
			
			BaselineConfig newBC = projectFactory.createBaselineConfig();
			newBC.setId("" + assuranceProjectTarget.getBaselineConfig().size());
			newBC.setName("AP_" + assuranceProjectTarget.getBaselineConfig().size());
			newBC.setDescription("Created automatically for Cross Project Reuse");			
			
			
			//Load the evidence models of the active assetsPackage of the selected assurance project 
			EList<BaselineConfig> lstBC = assuranceProjectTarget.getBaselineConfig();
			Iterator<BaselineConfig> bcI = lstBC.iterator();
									
			//Inactive the active Baseline Config
			while (bcI.hasNext()) {
				BaselineConfig oneBC = bcI.next();
				if (oneBC.isIsActive()) {
					oneBC.setIsActive(false);				
				}
			}
						
			
			newBC.setIsActive(true);
			assuranceProjectTarget.getBaselineConfig().add(newBC);
			
			if (allOK) allOK=copyEvidences();
			if (allOK) allOK=copyArgumentations();
			if (allOK) allOK=copyProcesses();
			if (allOK) allOK=copyMaps();
			//Baselines is the last because has mappings to the other models
			if (allOK) allOK=copyBaselines(newBC);
		}
		else{

			if(checktreeEvidenceSource.getTree().getItemCount()==0){
				MessageDialog.openError(getShell(), "Wrong Source Project",
						"The selected source project doesn't have evidence model.");
				return;
			}

			allOK=copyEvidences();
		}

		if(allOK){
			MessageDialog.openInformation(getShell(), "Reuse success", "Selected models reused");
		}


	}

	private boolean copyMaps() {		
		String sourcePath= projectsourceText.getText();
		String [] pathParts= sourcePath.split("/");
		String projectFolder=pathParts[0]+ "/" + pathParts[1] +"/" +  pathParts[2] ;							
		
		CDOResourceFolder folder = transaction.getResourceFolder(projectFolder);
		EList <CDOResourceNode> contents=null;
		if(folder instanceof CDOResourceFolder){
			contents= ((CDOResourceFolder)folder).getNodes();			
		}
		
		for(int x=0;x<contents.size();x++){
			CDOResourceNode oneNode  = contents.get(x);
			if(oneNode instanceof CDOResource){
				if(oneNode.getName().endsWith("mapping")){
					CDOResource sourceResource  = (CDOResource)oneNode;
					String projectPath= projecttargetText.getText();
					pathParts= projectPath.split("/");
					String targetPath=pathParts[0]+ "/" + pathParts[1] +"/" + oneNode.getName();
					targetPath = createNotExistingName(targetPath);
					CDOResource targetResource  = transaction.createResource(targetPath);
					
					initCopy(sourceResource, targetResource);

					try {				
						targetResource.save(options);						
					} catch (Exception e) {
						MessageDialog.openError(getShell(), "Mapping reuse failed", "Error copying model " + targetPath);

						// TODO Auto-generated catch block					
						e.printStackTrace();
						transaction.rollback();
						return false;
					}
				}
			}
			
		}
		return true;
	}
	
	private boolean copyBaselines(BaselineConfig bconfigActive) {
		Object [] baselines= checktreeBaselineSource.getCheckedElements();
		String projectPath= projecttargetText.getText();
		String [] pathParts= projectPath.split("/");
									
		
		for(int x=0;x<baselines.length;x++){

			CDOResource sourceResource  = (CDOResource)baselines[x];

			String targetPath=pathParts[0]+ "/" + pathParts[1] +"/" + sourceResource.getName();

			targetPath = createNotExistingName(targetPath);
			
			CDOResource targetResource  = transaction.createResource(targetPath);
			//diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");
			//To copy the refframework as baseline
			//MCPCDOResource targetResource  = assuranceprojectFolder.addResource(baselineName + ".baseline");

			initCopy(sourceResource, targetResource);

			try {				
				targetResource.save(options);
				
				bconfigActive.getRefFramework().add((BaseFramework) targetResource.getContents().get(0));
				
				resourceAPTarget.save(options);				

			} catch (IOException e) {
				MessageDialog.openError(getShell(), "Baseline reuse failed", "Error copying model " + targetPath);

				// TODO Auto-generated catch block					
				e.printStackTrace();
				transaction.rollback();
				return false;
			}
		}
		return true;
	}
	
	private boolean copyEvidences() {
		Object [] evidences= checktreeEvidenceSource.getCheckedElements();
		String projectPath= projecttargetText.getText();
		String [] pathParts= projectPath.split("/");

		for(int x=0;x<evidences.length;x++){

			CDOResource sourceResource  = (CDOResource)evidences[x];

			String targetPath=pathParts[0]+ "/" + "EVIDENCE/" + sourceResource.getName();
			
			targetPath = createNotExistingName(targetPath);
			
			CDOResource targetResource  = transaction.createResource(targetPath);
			
			//diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");
			//To copy the refframework as baseline
			//MCPCDOResource targetResource  = assuranceprojectFolder.addResource(baselineName + ".baseline");

			initCopy(sourceResource, targetResource);

			try {		
				
				targetResource.save(options);
				targetAssetsPackage.getArtefactsModel().add((ArtefactModel) targetResource.getContents().get(0));
				resourceAPTarget.save(options);
			} catch (IOException e) {
				MessageDialog.openError(getShell(), "Evidence reuse failed", "Error copying model " + targetPath);

				// TODO Auto-generated catch block					
				e.printStackTrace();
				transaction.rollback();
				return false;
			}
		}
		return true;
	}
			
						
	private String createNotExistingName(String targetPath) {
		
		if(viewCDO.hasResource(targetPath)){
			String [] tokens= targetPath.split("/");
			
			String [] nameTokes = tokens[2].split("\\.");			
			//Change only the name
			String modelName = nameTokes[0];
			String modelType = nameTokes[1];
			char lastchar = modelName.charAt(modelName.length()-1);
			char prevlastchar = modelName.charAt(modelName.length()-2);
			
			if(Character.isDigit(prevlastchar) && Character.isDigit(lastchar)){
				int number= Integer.parseInt("" + prevlastchar + lastchar);
				number++;
				modelName=modelName.substring(0, modelName.length()-2) + number;
			}
			else if(Character.isDigit(lastchar)){
				int number= Integer.parseInt("" + lastchar);
				number++;
				modelName=modelName.substring(0, modelName.length()-1) + number;
			}
			else{
				modelName=modelName + "1";
			}
			
			targetPath=tokens[0]+ "/" + tokens[1] + "/" + modelName + "." + modelType;
			
			//Check again the new name
			targetPath=createNotExistingName(targetPath);
		}
		return targetPath;
	}

	// MCP CrossProject
	private List<CDOResource> getDiagramsFromModel(CDOResource modelCDO) {
		List<CDOResource> res = new ArrayList<CDOResource>();
		
		/* No funciona porque los diagramas no est�n cargados en memoria o es que s�lo funciona con modelos
		EObject aEObject= modelCDO.getContents().get(0);
		List<CDOObjectReference> rc = transaction.queryXRefs(CDOUtil.getCDOObject(aEObject), new EReference[] {});
		for(CDOObjectReference oneRef:rc){
			if (oneRef.getSourceObject().eResource() != modelCDO) res.add((CDOResource)(oneRef.getSourceObject().eResource()));
			if (oneRef.getTargetObject().eResource() != modelCDO) res.add((CDOResource)(oneRef.getTargetObject().eResource()));
		}
		List<EObject> list = aEObject.eCrossReferences();
		for(EObject elem : list)
		{
			res.add((CDOResource)elem.eResource());
		}
		*/
		String temp = modelCDO.getPath();
		String sourceFolderPath = temp.substring(0, temp.lastIndexOf("/"));
		CDOResourceFolder argFolder=transaction.getResourceFolder(sourceFolderPath);
		EList<CDOResourceNode> listN = argFolder.getNodes();
		for ( CDOResourceNode node : listN) {
			if (node instanceof CDOResourceFolder) {
				System.out.println("getDiagramsFromModel: found subfolder =."+ node.toString());
				List<CDOResource> res2 = getDiagramsFromModel((CDOResource) node);
				res.addAll(res2);
			} else if (node instanceof CDOResource) {					
				CDOResource elem = (CDOResource)node;
				if(elem.getPath().contains(EXTENSION_DIAGRAM)){
					EObject model = (EObject) ((DiagramImpl)elem.getContents().get(0)).basicGetElement();
					//if (elem.getContents().get(0).eResource() == modelCDO)
					if (model.eResource().getURI() == modelCDO.getURI())
					{
						res.add((CDOResource)elem.getContents().get(0).eResource());
					}
				}
			}
			//listN.remove(0); ME DA PROBLEMAS PORQUE ESTOY BORRANDO!!!
		}
		
		return res;
	}
	// MCP CrossProject
	
	private boolean copyArgumentations() {
		Object [] argumentations= checktreeArgSource.getCheckedElements();
		String projectPath= projecttargetText.getText();
		String [] pathParts= projectPath.split("/");

		// MCP CrossProject
		IProgressMonitor monitor = null;
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		if(argumentations.length > 0)
		{
			dialog.open();
			monitor = dialog.getProgressMonitor();
			String CREATE_ARG_DIAGRAM = "Create Cross Arg diagram";
			monitor.beginTask(CREATE_ARG_DIAGRAM, 20);
		}
		// MCP CrossProject			

		for(int x=0;x<argumentations.length;x++){

			CDOResource sourceResource  = (CDOResource)argumentations[x];

			String targetPath=pathParts[0]+ "/" + "ARGUMENTATION/" + sourceResource.getName();
			String targetFolderPath=pathParts[0]+ "/" + "ARGUMENTATION";
			targetPath = createNotExistingName(targetPath);
			
			CDOResource targetResource  = transaction.createResource(targetPath);
			//diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");
			//To copy the refframework as baseline
			//MCPCDOResource targetResource  = assuranceprojectFolder.addResource(baselineName + ".baseline");



			initCopy(sourceResource, targetResource);

			try {				
				targetResource.save(options);
				
				// MCP CrossProject
				// crear los diagramas del modelo
				//String sourceDiagram= sourceResource.getPath() + "_diagram";
				//CDOResource sourceDiagramResource  = transaction.getOrCreateResource(sourceDiagram);
				List<CDOResource> listDiagrams = getDiagramsFromModel(sourceResource);
				for(CDOResource sourceDiagramResource: listDiagrams)
				{
					// lista de objetos en diagrama
					//List<EObject> listRootObjects = sourceDiagramResource.getContents();
					List<EObject> listRootObjectsSource = ((Diagram)sourceDiagramResource.getContents().get(0)).getPersistedChildren();
					List<EObject> listRootObjects = new ArrayList<EObject>();
					for(EObject element : listRootObjectsSource)
					{
						EObject objsource = ((ShapeImpl)element).getElement();
						//EObject obj = map.get(objsource);
				        CDOID cdoID = CDOUtil.getCDOObject(objsource).cdoID();
				        Long  cdoIdLong = new Long(cdoID.toURIFragment());
						EObject obj = eIDToObjectMap.get(cdoID.toString());
						listRootObjects.add(obj);
					}
					// crear el recurso diagram vacio
					CDOResourceFolder argFolder=transaction.getResourceFolder(targetFolderPath);				
					//CDOResourceFolder argFolder=projectFolder.addResourceFolder("ARGUMENTATION");
			        //CDOResource targetDiagramResource  = transaction.getOrCreateResource(targetPath + EXTENSION_DIAGRAM);
					String targetDiagram = targetPath.substring(0, targetPath.lastIndexOf("/") + 1) + sourceDiagramResource.getName();
					targetDiagram = createNotExistingName(targetDiagram);
					CDOResource targetDiagramResource  = transaction.getOrCreateResource(targetDiagram);
					DawnArgDiagramUtil myDiagram = new DawnArgDiagramUtil(sourceDiagramResource, listRootObjects, targetResource.getURI(), targetDiagramResource.getURI(), argFolder, transaction);
					myDiagram.generateDiagram(monitor);
				}

				//MCP: se lanza LocalCommitConflictException!!!; los anteriores commit son sin parametro pero sigue el problema
				Set<CDOObject> conf = transaction.getConflicts();
				for (Iterator<CDOObject>it = conf.iterator(); it.hasNext();) {
					CDOObject nextObject = it.next();
					System.out.println("copyArgumentations: confict "+nextObject.toString());
				}
				transaction.commit();//???MCP

			
				// MCP CrossProject
				
				targetAssetsPackage.getArgumentationModel().add((Case) targetResource.getContents().get(0));
				resourceAPTarget.save(options);
				
				// MCP CrossProject
				//???transaction.close();
				// MCP CrossProject			
				
			} catch (Exception e) {
				MessageDialog.openError(getShell(), "Argumentation reuse failed", "Error copying model " + targetPath);

				// TODO Auto-generated catch block					
				e.printStackTrace();
				transaction.rollback();
				// MCP CrossProject
				if(argumentations.length > 0)
				{
					dialog.close();
				}
				// MCP CrossProject		
				return false;
			}
		}// cycle for
		
		// MCP CrossProject
		if(argumentations.length > 0)
		{
			dialog.close();
		}
		// MCP CrossProject		
		
		return true;
	}	 
			
	
	private boolean copyProcesses() {
		Object [] processes= checktreeProcessSource.getCheckedElements();
		String projectPath= projecttargetText.getText();
		String [] pathParts= projectPath.split("/");

		for(int x=0;x<processes.length;x++){

			CDOResource sourceResource  = (CDOResource)processes[x];

			String targetPath=pathParts[0]+ "/" + "PROCESSES/" + sourceResource.getName();
			targetPath = createNotExistingName(targetPath);
			
			CDOResource targetResource  = transaction.createResource(targetPath);
			//diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");
			//To copy the refframework as baseline
			//MCPCDOResource targetResource  = assuranceprojectFolder.addResource(baselineName + ".baseline");

			initCopy(sourceResource, targetResource);			    

			try {				
				targetResource.save(options);
				
				targetAssetsPackage.getProcessModel().add((ProcessModel) targetResource.getContents().get(0));
				resourceAPTarget.save(options);

			} catch (IOException e) {
				MessageDialog.openError(getShell(), "Process reuse failed", "Error copying model " + targetPath);

				// TODO Auto-generated catch block					
				e.printStackTrace();
				transaction.rollback();
				return false;
			}
		}
		return true;
	}

	private void initCopy(CDOResource sourceResource, CDOResource targetResource) {
		// Create empty roots
		for (EObject aEObject : sourceResource.getContents()) {
			targetResource.getContents().add(getCorrespondingEObject(aEObject));
		}

		// Copy EObjects, containment in the resource is done automatically since we have
		// already attached the corresponding root EObject to the target resource
		for (Iterator<EObject> iterator = sourceResource.getAllContents(); iterator.hasNext();) {
			EObject aEObject = iterator.next();
			
			//Avoid copy assurance assent events
			if(aEObject instanceof AssuranceAssetEvent){
				continue;
			}
			EObject bEObject = getCorrespondingEObject(aEObject);

			copyAEObjectToBEObject(aEObject, bEObject);

			// MCP CrossProject			
			if(aEObject instanceof ModelElement)
			{
		        CDOID cdoID = CDOUtil.getCDOObject(aEObject).cdoID();
		        Long  cdoIdLong = new Long(cdoID.toURIFragment());
				eIDToObjectMap.put(cdoID.toString(), bEObject);
			}
			// MCP CrossProject			
		}
	}	
	
	@Override
	public boolean close() {		
		//Refresh the Repository explorer view
		DawnExplorer repoView =null;
		IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if ("org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer".equals(viewReferences[i].getId())) {
				repoView = (DawnExplorer)viewReferences[i].getView(false);
				repoView.refreshViewer(false);
				break;
			}
		}
		if (!transaction.isClosed()) transaction.close();
		return super.close();
	}		
	
	private  EObject getCorrespondingEObject(EObject aEObject) {
		
		EObject bEObject = map.get(aEObject);			
		if (bEObject == null) {			
			if(aEObject instanceof MapGroup){
				for ( EObject key : map.keySet() ) {
				   if(key instanceof MapGroup){
					   if(CDOUtil.getCDOObject(key).cdoID() == CDOUtil.getCDOObject(aEObject).cdoID()){
						   return map.get(key);
					   }
				   }
				}
			}
			
			EClass aEClass = aEObject.eClass();			
			//if(aEClass.getName().startsWith("Ref")){
			
				//String bClassName = aEClass.getName().replaceFirst("Ref", "Base");
				// Second search the corresponding EClass in BPackage
				//EClass bEClass = (EClass) BaselinePackage.eINSTANCE.getEClassifier(bClassName);
				// Create a new empty instance and register to avoid dups
				bEObject = EcoreUtil.create(aEClass);
				map.put(aEObject, bEObject);
			//}
			 /*if (aEClass.getName().equals("MapJustification")){
				bEObject = EcoreUtil.create(aEClass);
				map.put(aEObject, bEObject);
			}
			else if (aEClass.getName().equals("MapGroup")){
				bEObject = EcoreUtil.create(aEClass);
				map.put(aEObject, bEObject);
			}
			else{*/
				//bEObject=aEObject;
				//map.put(aEObject, aEObject);
			//}
		}
		return bEObject;
	}
	
	private  void copyAEObjectToBEObject(EObject aEObject, EObject bEObject) {
		
		
		for (EStructuralFeature aFeature : aEObject.eClass().getEAllStructuralFeatures()) {
			if (aEObject.eIsSet(aFeature)) {
				// Get the corresponding feature in the target EClass.
				// Get simply the feature with the same name
				
				EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature(aFeature.getName());								
				
				if(bFeature != null){
					if (aFeature instanceof EAttribute) {
						bEObject.eSet(bFeature, aEObject.eGet(aFeature));
					} else { // EReference
																													
						if(aEObject.eClass().getName().equals("BaseEquivalenceMap") && aFeature.getName().equals("target") ){
							bEObject.eSet(bFeature,aEObject.eGet(aFeature));
						}
						else if(aFeature.getName().equals("refFramework") || aFeature.getName().equals("refAssurableElement")){
							bEObject.eSet(bFeature,aEObject.eGet(aFeature));
						}
						/*else if(aFeature.getName().equals("refFramework")){
							bEObject.eSet(bFeature,aEObject.eGet(aFeature));
						}	*/					
						/*else if(aEObject.eClass().getName().equals("MapGroup")){
							EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							bEObject.eSet(bFeature, copy);							
						}
						
						else if(aFeature.getName().equals("refFramework") || aFeature.getName().equals("refAssurableElement")){
							bEObject.eSet(bFeature,aEObject.eGet(aFeature));
						}
						/*else if (aFeature.getName().equals("mapJustification")){
							EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
							
							bEObject.eSet(bFeature, copy);
						}*/
						/*else if (aFeature.getName().equals("ownedTechnique")){
							EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
							
							bEObject.eSet(bFeature, copy);
						}*/						
						else if (aFeature.getName().equals("mapGroup")){
							if(aEObject.eClass().getName().equals("BaseComplianceMap")){
								EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
								bEObject.eSet(bFeature, copy);
							}
							else{
							//EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
							
							//bEObject.eSet(bFeature, copy);
							
								bEObject.eSet(bFeature,aEObject.eGet(aFeature));
							}
						}
						else{	
							if(!aFeature.getName().equals("lifecycleEvent")){
								if(aEObject.eGet(aFeature) instanceof EList){
									@SuppressWarnings("unchecked")
									EList<EObject> aList = (EList<EObject>) aEObject.eGet(aFeature);
									EList<EObject> bList = new BasicEList<EObject>(); 
									for (int i = 0; i < aList.size(); i++) {
										bList.add(getCorrespondingEObject(aList.get(i)));
									}
									bEObject.eSet(bFeature, bList);
								}
								else{
									
									//System.out.println("Feature " + aFeature.getName() + " of " + aEObject.eClass().getName() + " not copied to Baseline");
									EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
									//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
									
									bEObject.eSet(bFeature, copy);
								}
							}
						}
					}
				}
			}
		}
		
		//Add the linkf to the refframeork model concepts.
		
		/*if(aEObject instanceof RefFramework){
			 EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("refFramework");
			 bEObject.eSet(bFeature,aEObject);
		}
		else if(bEObject instanceof BaseAssurableElement){
			EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("refAssurableElement");
			 bEObject.eSet(bFeature,aEObject);
		}*/
		
	}

	private void defaultSelection() {
		for(TreeItem root:checktreeBaselineSource.getTree().getItems()){
			root.setChecked(true);
		}
		checktreeBaselineSource.getTree().setEnabled(false);
								
		checkArtefacts.setSelection(true);		
		checkArtefacts.setEnabled(false);
		
		for(TreeItem root:checktreeEvidenceSource.getTree().getItems()){
			root.setChecked(true);
		}
		checktreeEvidenceSource.getTree().setEnabled(false);
								
		checkArguments.setSelection(true);		
		checkArguments.setEnabled(false);
		for(TreeItem root:checktreeArgSource.getTree().getItems()){
			root.setChecked(true);
		}
		checktreeArgSource.getTree().setEnabled(false);
								
		checkProcess.setSelection(true);		
		checkProcess.setEnabled(false);
		for(TreeItem root:checktreeProcessSource.getTree().getItems()){
			root.setChecked(true);
		}
		checktreeProcessSource.getTree().setEnabled(false);
	}
}

