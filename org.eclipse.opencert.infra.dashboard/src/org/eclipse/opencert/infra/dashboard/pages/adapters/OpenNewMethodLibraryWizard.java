/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages.adapters;

import org.eclipse.epf.authoring.ui.wizards.NewLibraryWizard;
import org.eclipse.opencert.infra.dashboard.extensionregistry.OpenCertNewWizardIDs;
import org.eclipse.opencert.infra.dashboard.extensionregistry.OpenCertWizardsRegistry;
import org.eclipse.opencert.infra.dashboard.helpers.EclipseHelper;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.forms.events.HyperlinkAdapter;

/**
 * A {@link HyperlinkAdapter} to open the new method library wizard.
 */
public class OpenNewMethodLibraryWizard extends AbstractOpenWizardHyperLinkAdapter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected INewWizard getWizard() {
		return OpenCertWizardsRegistry.getInstance().getMethodLibraryNewWizard();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isCorrectWizardInstance(INewWizard pWizard) {
		return pWizard instanceof NewLibraryWizard;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void initWizard(INewWizard pWizard) {
		NewLibraryWizard vNewLibWizard = (NewLibraryWizard) pWizard;

		// Initialise the wizard
		vNewLibWizard.init(EclipseHelper.getWorkbenchWindow().getWorkbench(),
				getCurrentSelectionFromProjectExplorer(OpenCertNewWizardIDs.METHOD_LIBRARY_NEW_WIZARD_ID));
	}
}
