/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;

import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRolSelectionPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class BaseRoleBaseRolSelectionPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASEROLSELECTION_PART = "BaseRolSelection"; //$NON-NLS-1$

	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaseRoleBaseRolSelectionPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseRole, String editing_mode) {
		super(editingContext, baseRole, editing_mode);
		parts = new String[] { BASEROLSELECTION_PART };
		repositoryKey = BaselineViewsRepository.class;
		partKey = BaselineViewsRepository.BaseRolSelection.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaseRole baseRole = (BaseRole)elt;
			final BaseRolSelectionPropertiesEditionPart baseRolSelectionPart = (BaseRolSelectionPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(BaselineViewsRepository.BaseRolSelection.Properties.isSelected)) {
				baseRolSelectionPart.setIsSelected(baseRole.isIsSelected());
			}
			if (isAccessible(BaselineViewsRepository.BaseRolSelection.Properties.selectionJustification))
				baseRolSelectionPart.setSelectionJustification(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseRole.getSelectionJustification()));
			
			// init filters
			
			
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}





	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == BaselineViewsRepository.BaseRolSelection.Properties.isSelected) {
			return BaselinePackage.eINSTANCE.getBaselineElement_IsSelected();
		}
		if (editorKey == BaselineViewsRepository.BaseRolSelection.Properties.selectionJustification) {
			return BaselinePackage.eINSTANCE.getBaselineElement_SelectionJustification();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaseRole baseRole = (BaseRole)semanticObject;
		if (BaselineViewsRepository.BaseRolSelection.Properties.isSelected == event.getAffectedEditor()) {
			baseRole.setIsSelected((Boolean)event.getNewValue());
		}
		if (BaselineViewsRepository.BaseRolSelection.Properties.selectionJustification == event.getAffectedEditor()) {
			baseRole.setSelectionJustification((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaseRolSelectionPropertiesEditionPart baseRolSelectionPart = (BaseRolSelectionPropertiesEditionPart)editingPart;
			if (BaselinePackage.eINSTANCE.getBaselineElement_IsSelected().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && baseRolSelectionPart != null && isAccessible(BaselineViewsRepository.BaseRolSelection.Properties.isSelected))
				baseRolSelectionPart.setIsSelected((Boolean)msg.getNewValue());
			
			if (BaselinePackage.eINSTANCE.getBaselineElement_SelectionJustification().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && baseRolSelectionPart != null && isAccessible(BaselineViewsRepository.BaseRolSelection.Properties.selectionJustification)) {
				if (msg.getNewValue() != null) {
					baseRolSelectionPart.setSelectionJustification(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					baseRolSelectionPart.setSelectionJustification("");
				}
			}
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			BaselinePackage.eINSTANCE.getBaselineElement_IsSelected(),
			BaselinePackage.eINSTANCE.getBaselineElement_SelectionJustification()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (BaselineViewsRepository.BaseRolSelection.Properties.isSelected == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaselineElement_IsSelected().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaselineElement_IsSelected().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseRolSelection.Properties.selectionJustification == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaselineElement_SelectionJustification().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaselineElement_SelectionJustification().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
