/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.labelparser;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem.ItemHeaderType;
import org.eclipse.opencert.userguidance.labelparser.tokens.TextToken;
import org.eclipse.opencert.userguidance.labelparser.tokens.Token;

public abstract class Highlighter {
	private final static Color idColor = new Color(null, 185, 50, 96);
	private final static Color pathColor = new Color(null, 87, 173, 143);
	private final static Color uriColor = new Color(null, 70, 70, 250);
	private final static Color varColor = new Color(null, 130, 110, 30);
	private final static Color vocColor = new Color(null, 100, 100, 100);

	private final String inputText;
	private final Font normalFont;
	private final Font boldFont;

	private String outputText;
	private List<StyleRange> styles;
	private List<Token> tokens;

	public Highlighter(String input, Font normalFont, Font boldFont) {
		this.inputText = input;
		this.normalFont = normalFont;
		this.boldFont = boldFont;
	}

	public void calculateHighlighting() {
		tokens = LabelParserUtil.parse(inputText);
		styles = new ArrayList<StyleRange>(tokens.size());
		StringBuilder outputTextBuilder = new StringBuilder();

		// Indicates how much shorter the printed string is in comparison with
		// the parsed string
		int truncationOffset = 0;

		if (tokens != null) {
			for (Token token : tokens) {
				if (token instanceof HighlightItem) {
					HighlightItem item = (HighlightItem) token;

					// Token representation
					String tokenRepresentation = getRepresentation(item);

					outputTextBuilder.append(tokenRepresentation);

					// Style
					StyleRange styleRange = new StyleRange();
					styleRange.start = item.getStartIndex() - truncationOffset;
					styleRange.length = tokenRepresentation.length();
					styleRange.foreground = getHighlightColor(item
							.getHeaderType());
					styleRange.font = getHighlightFont(item.getHeaderType());
					styleRange.underline = isUnderlined(item.getHeaderType());
					styles.add(styleRange);

					truncationOffset += item.getRawString().length()
							- tokenRepresentation.length();
				} else if (token instanceof TextToken) {
					TextToken textToken = (TextToken) token;
					String tokenRepresentation = textToken.getRepresentation();
					outputTextBuilder.append(tokenRepresentation);

					// Style
//					StyleRange styleRange = new StyleRange();
//					styleRange.start = textToken.getStartIndex()
//							- truncationOffset;
//					styleRange.length = tokenRepresentation.length();
//					styleRange.font = normalFont;
//					styles.add(styleRange);
				}
			}
		}

		outputText = outputTextBuilder.toString();
	}

	public String getOutputText() {
		return outputText;
	}

	public List<StyleRange> getStyles() {
		return styles;
	}

	public StyleRange[] getStylesArray() {
		StyleRange[] stylesArray = styles
				.toArray(new StyleRange[styles.size()]);
		return stylesArray;
	}

	public List<Token> getTokens() {
		return tokens;
	}

	protected Color getHighlightColor(ItemHeaderType type) {
		Color result = null;
		switch (type) {
		case id:
			result = idColor;
			break;
		case path:
			result = pathColor;
			break;
		case uri:
			result = uriColor;
			break;
		case var:
			result = varColor;
			break;
		case voc:
			result = vocColor;
			break;
		default:
			result = ColorConstants.black;
		}
		return result;
	}

	protected Font getHighlightFont(ItemHeaderType type) {
		Font result = null;
		switch (type) {
		case id:
			result = boldFont;
			break;
		case path:
			result = boldFont;
			break;
		case uri:
			result = boldFont;
			break;
		case var:
			result = boldFont;
			break;
		case voc:
			result = boldFont;
			break;
		default:
			result = normalFont;
		}
		return result;
	}

	protected boolean isUnderlined(ItemHeaderType type) {
		boolean result;
		switch (type) {
		case id:
			result = false;
			break;
		case path:
			result = true;
			break;
		case uri:
			result = true;
			break;
		case var:
			result = false;
			break;
		case voc:
			result = false;
			break;
		default:
			result = false;
		}
		return result;
	}

	protected abstract String getRepresentation(HighlightItem item);
}
