/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages.adapters;

import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;

/**
 * A {@link HyperlinkAdapter} to create a new CHESS element.
 */
public abstract class AbstractCreateNewChessElementHyperLinkAdapter extends HyperlinkAdapter {

	/** ID of the model explorer view. */
	protected static final String MODEL_EXPLORER_VIEW_ID = "org.eclipse.papyrus.views.modelexplorer.modelexplorer"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	public AbstractCreateNewChessElementHyperLinkAdapter() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void linkActivated(final HyperlinkEvent pEvent) {
		createChessElement();
	}

	/**
	 * Create a CHESS element when the link is pressed.
	 */
	protected abstract void createChessElement();
}
