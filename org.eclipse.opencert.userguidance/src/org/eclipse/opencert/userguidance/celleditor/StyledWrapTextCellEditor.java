/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.celleditor;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ExtendedModifyEvent;
import org.eclipse.swt.custom.ExtendedModifyListener;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.opencert.userguidance.StyledText2;
import org.eclipse.opencert.userguidance.autocomplete.AutoCompleteField2;
import org.eclipse.opencert.userguidance.autocomplete.StyledTextContentAdapter;

// Autocomplete and styled text cell editor
public class StyledWrapTextCellEditor extends StyledTextCellEditorEx {
	private AutoCompleteField2 field;
	private EObject eobj;
	private int featureID;

	public StyledWrapTextCellEditor(Composite parent, int style, EObject eobj,
			int featureID) {
		super(parent, style, eobj, featureID);
		this.eobj = eobj;
		this.featureID = featureID;

		create(parent);
	}

	// Fix the key handling for escape, enter and ctrl + enter.
	@Override
	protected void keyReleaseOccured(KeyEvent keyEvent) {
		if (keyEvent.character == '\u001b') { // Escape character
			fireCancelEditor();
			deactivate();
		} else if (keyEvent.character == '\r'
				&& (keyEvent.stateMask & SWT.CTRL) != 0) {
			StyledText styledText = (StyledText) getControl();
			int caretOffset = styledText.getCaretOffset();
			String text = styledText.getText();
			styledText.setText(text + "\n");
			styledText.setCaretOffset(caretOffset + 1);
		} else if (keyEvent.character == '\r') { // Return key
			if (!field.isPopupOpen()) {
				fireApplyEditorValue();
				deactivate();
			}
		} else {
			super.keyReleaseOccured(keyEvent);
		}
	}

	@Override
	protected Control createControl(Composite parent) {
		final StyledText2 styledText = (StyledText2) super
				.createControl(parent);

		styledText.addExtendedModifyListener(new ExtendedModifyListener() {
			@Override
			public void modifyText(ExtendedModifyEvent event) {
				CellEditorHighlighter highlighter = new CellEditorHighlighter(
						styledText.getText(), styledText.getFont(), styledText
								.getFont());
				highlighter.calculateHighlighting();
				List<StyleRange> ranges = highlighter.getStyles();
				styledText.setStyleRanges(ranges);
			}
		});

		// Autocomplete
		field = new AutoCompleteField2(styledText,
				new StyledTextContentAdapter(), eobj, featureID);

		return styledText;
	}
}
