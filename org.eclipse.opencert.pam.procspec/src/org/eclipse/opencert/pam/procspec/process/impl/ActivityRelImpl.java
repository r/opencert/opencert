/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pam.procspec.process.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;
import org.eclipse.opencert.infra.general.general.ActivityRelKind;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.ActivityRel;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity Rel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityRelImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityRelImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityRelImpl#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActivityRelImpl extends CDOObjectImpl implements ActivityRel {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final ActivityRelKind TYPE_EDEFAULT = ActivityRelKind.DECOMPOSITION;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityRelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProcessPackage.Literals.ACTIVITY_REL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity getTarget() {
		return (Activity)eDynamicGet(ProcessPackage.ACTIVITY_REL__TARGET, ProcessPackage.Literals.ACTIVITY_REL__TARGET, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity basicGetTarget() {
		return (Activity)eDynamicGet(ProcessPackage.ACTIVITY_REL__TARGET, ProcessPackage.Literals.ACTIVITY_REL__TARGET, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(Activity newTarget) {
		eDynamicSet(ProcessPackage.ACTIVITY_REL__TARGET, ProcessPackage.Literals.ACTIVITY_REL__TARGET, newTarget);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity getSource() {
		return (Activity)eDynamicGet(ProcessPackage.ACTIVITY_REL__SOURCE, ProcessPackage.Literals.ACTIVITY_REL__SOURCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity basicGetSource() {
		return (Activity)eDynamicGet(ProcessPackage.ACTIVITY_REL__SOURCE, ProcessPackage.Literals.ACTIVITY_REL__SOURCE, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(Activity newSource) {
		eDynamicSet(ProcessPackage.ACTIVITY_REL__SOURCE, ProcessPackage.Literals.ACTIVITY_REL__SOURCE, newSource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityRelKind getType() {
		return (ActivityRelKind)eDynamicGet(ProcessPackage.ACTIVITY_REL__TYPE, ProcessPackage.Literals.ACTIVITY_REL__TYPE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(ActivityRelKind newType) {
		eDynamicSet(ProcessPackage.ACTIVITY_REL__TYPE, ProcessPackage.Literals.ACTIVITY_REL__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ProcessPackage.ACTIVITY_REL__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case ProcessPackage.ACTIVITY_REL__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case ProcessPackage.ACTIVITY_REL__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ProcessPackage.ACTIVITY_REL__TARGET:
				setTarget((Activity)newValue);
				return;
			case ProcessPackage.ACTIVITY_REL__SOURCE:
				setSource((Activity)newValue);
				return;
			case ProcessPackage.ACTIVITY_REL__TYPE:
				setType((ActivityRelKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ProcessPackage.ACTIVITY_REL__TARGET:
				setTarget((Activity)null);
				return;
			case ProcessPackage.ACTIVITY_REL__SOURCE:
				setSource((Activity)null);
				return;
			case ProcessPackage.ACTIVITY_REL__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ProcessPackage.ACTIVITY_REL__TARGET:
				return basicGetTarget() != null;
			case ProcessPackage.ACTIVITY_REL__SOURCE:
				return basicGetSource() != null;
			case ProcessPackage.ACTIVITY_REL__TYPE:
				return getType() != TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //ActivityRelImpl
