/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.properties.property.Property;
import org.eclipse.opencert.infra.properties.property.PropertyPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class BaseArtefactBasePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for constrainingRequirement ReferencesTable
	 */
	private ReferencesTableSettings constrainingRequirementSettings;
	
	/**
	 * Settings for applicableTechnique ReferencesTable
	 */
	private ReferencesTableSettings applicableTechniqueSettings;
	
	/**
	 * Settings for ownedRel ReferencesTable
	 */
	protected ReferencesTableSettings ownedRelSettings;
	
	/**
	 * Settings for property ReferencesTable
	 */
	private ReferencesTableSettings propertySettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaseArtefactBasePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseArtefact, String editing_mode) {
		super(editingContext, baseArtefact, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = BaselineViewsRepository.class;
		partKey = BaselineViewsRepository.BaseArtefact.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaseArtefact baseArtefact = (BaseArtefact)elt;
			final BaseArtefactPropertiesEditionPart basePart = (BaseArtefactPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseArtefact.getId()));
			
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseArtefact.getName()));
			
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.description))
				basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, baseArtefact.getDescription()));
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.reference))
				basePart.setReference(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseArtefact.getReference()));
			
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.constrainingRequirement)) {
				constrainingRequirementSettings = new ReferencesTableSettings(baseArtefact, BaselinePackage.eINSTANCE.getBaseArtefact_ConstrainingRequirement());
				basePart.initConstrainingRequirement(constrainingRequirementSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.applicableTechnique)) {
				applicableTechniqueSettings = new ReferencesTableSettings(baseArtefact, BaselinePackage.eINSTANCE.getBaseArtefact_ApplicableTechnique());
				basePart.initApplicableTechnique(applicableTechniqueSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.ownedRel)) {
				ownedRelSettings = new ReferencesTableSettings(baseArtefact, BaselinePackage.eINSTANCE.getBaseArtefact_OwnedRel());
				basePart.initOwnedRel(ownedRelSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.property)) {
				propertySettings = new ReferencesTableSettings(baseArtefact, BaselinePackage.eINSTANCE.getBaseArtefact_Property());
				basePart.initProperty(propertySettings);
			}
			// init filters
			
			
			
			
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.constrainingRequirement)) {
				basePart.addFilterToConstrainingRequirement(new EObjectFilter(BaselinePackage.Literals.BASE_REQUIREMENT));
				// Start of user code for additional businessfilters for constrainingRequirement
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.applicableTechnique)) {
				basePart.addFilterToApplicableTechnique(new EObjectFilter(BaselinePackage.Literals.BASE_TECHNIQUE));
				// Start of user code for additional businessfilters for applicableTechnique
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.ownedRel)) {
				basePart.addFilterToOwnedRel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseArtefactRel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRel
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseArtefact.Properties.property)) {
				basePart.addFilterToProperty(new EObjectFilter(PropertyPackage.Literals.PROPERTY));
				// Start of user code for additional businessfilters for property
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}











	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == BaselineViewsRepository.BaseArtefact.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefact.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefact.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefact.Properties.reference) {
			return BaselinePackage.eINSTANCE.getBaseArtefact_Reference();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefact.Properties.constrainingRequirement) {
			return BaselinePackage.eINSTANCE.getBaseArtefact_ConstrainingRequirement();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefact.Properties.applicableTechnique) {
			return BaselinePackage.eINSTANCE.getBaseArtefact_ApplicableTechnique();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefact.Properties.ownedRel) {
			return BaselinePackage.eINSTANCE.getBaseArtefact_OwnedRel();
		}
		if (editorKey == BaselineViewsRepository.BaseArtefact.Properties.property) {
			return BaselinePackage.eINSTANCE.getBaseArtefact_Property();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaseArtefact baseArtefact = (BaseArtefact)semanticObject;
		if (BaselineViewsRepository.BaseArtefact.Properties.id == event.getAffectedEditor()) {
			baseArtefact.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseArtefact.Properties.name == event.getAffectedEditor()) {
			baseArtefact.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseArtefact.Properties.description == event.getAffectedEditor()) {
			baseArtefact.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseArtefact.Properties.reference == event.getAffectedEditor()) {
			baseArtefact.setReference((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseArtefact.Properties.constrainingRequirement == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof BaseRequirement) {
					constrainingRequirementSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				constrainingRequirementSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				constrainingRequirementSettings.move(event.getNewIndex(), (BaseRequirement) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseArtefact.Properties.applicableTechnique == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof BaseTechnique) {
					applicableTechniqueSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				applicableTechniqueSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				applicableTechniqueSettings.move(event.getNewIndex(), (BaseTechnique) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseArtefact.Properties.ownedRel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRelSettings.move(event.getNewIndex(), (BaseArtefactRel) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseArtefact.Properties.property == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Property) {
					propertySettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				propertySettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				propertySettings.move(event.getNewIndex(), (Property) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaseArtefactPropertiesEditionPart basePart = (BaseArtefactPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefact.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefact.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefact.Properties.description)){
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseArtefact_Reference().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseArtefact.Properties.reference)) {
				if (msg.getNewValue() != null) {
					basePart.setReference(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setReference("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseArtefact_ConstrainingRequirement().equals(msg.getFeature())  && isAccessible(BaselineViewsRepository.BaseArtefact.Properties.constrainingRequirement))
				basePart.updateConstrainingRequirement();
			if (BaselinePackage.eINSTANCE.getBaseArtefact_ApplicableTechnique().equals(msg.getFeature())  && isAccessible(BaselineViewsRepository.BaseArtefact.Properties.applicableTechnique))
				basePart.updateApplicableTechnique();
			if (BaselinePackage.eINSTANCE.getBaseArtefact_OwnedRel().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseArtefact.Properties.ownedRel))
				basePart.updateOwnedRel();
			if (BaselinePackage.eINSTANCE.getBaseArtefact_Property().equals(msg.getFeature())  && isAccessible(BaselineViewsRepository.BaseArtefact.Properties.property))
				basePart.updateProperty();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			BaselinePackage.eINSTANCE.getBaseArtefact_Reference(),
			BaselinePackage.eINSTANCE.getBaseArtefact_ConstrainingRequirement(),
			BaselinePackage.eINSTANCE.getBaseArtefact_ApplicableTechnique(),
			BaselinePackage.eINSTANCE.getBaseArtefact_OwnedRel(),
			BaselinePackage.eINSTANCE.getBaseArtefact_Property()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (BaselineViewsRepository.BaseArtefact.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefact.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefact.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseArtefact.Properties.reference == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseArtefact_Reference().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseArtefact_Reference().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
