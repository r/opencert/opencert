/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.policies;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.channels.FileChannel;
import java.util.Iterator;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.DawnEditorInput;
import org.eclipse.emf.cdo.dawn.ui.helper.EditorDescriptionHelper;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.OpenEditPolicy;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.HintedDiagramLinkStyle;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.Style;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.ShapeImpl;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.CaseEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditor;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorPlugin;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorUtil;
import org.eclipse.opencert.sam.arg.arg.diagram.part.Messages;
import org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl;
import org.eclipse.opencert.sam.arg.ui.util.UtilConstants;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.part.FileEditorInput;



/**
 * @generated NOT
 */
/**
 * @author M� Carmen Palacios
 * @author Alejandra Ruiz
 */
public class OpenFileDiagramEditPolicy extends OpenEditPolicy {
	
	CDOTransaction transaction = null;


	/**
	 * @generated not
	 */
	protected Command getOpenCommand(Request request) {
		EditPart targetEditPart = getTargetEditPart(request);
		if (false == targetEditPart.getModel() instanceof View) {
			return null;
		}
		View view = (View) targetEditPart.getModel();
		if(view.getElement() instanceof Argumentation){
			Argumentation element = (Argumentation) view.getElement();
			String loca=element.getLocation();
			if (loca!=null){
			if (loca.startsWith("/")){
				//we are in cdo				
				CDOConnectionUtil.instance.init(
						PreferenceConstants.getRepositoryName(),
						PreferenceConstants.getProtocol(),
						PreferenceConstants.getServerName());
				CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
				CDOView viewCDO = CDOConnectionUtil.instance.openView(sessionCDO);	
				transaction=sessionCDO.openTransaction();
				URI uri=null;
				CDOResourceNode[] listR=  viewCDO.getElements();	
				Diagram d=null;
				for (int i=0; i<listR.length; i++){
					String aux=listR[i].getPath();
					Object o=listR[i];
					if(o instanceof CDOResourceFolder){
						CDOResourceNode r=checkFolderContents((CDOResourceFolder)o,loca);
						if (r!=null){
							uri=r.getURI();
							EList<EObject> nodeContent = r.eContents();
							Iterator<EObject> it=nodeContent.iterator();
							while(it.hasNext()){
								Object obj2=it.next();
								if(obj2 instanceof Diagram){
									d=(Diagram) obj2;
								}
							}				
						}
					}
					else{	
					if(aux.contains(loca)){
						uri=listR[i].getURI();
						EList<EObject> nodeContent = listR[i].eContents();
						Iterator<EObject> it=nodeContent.iterator();
						while(it.hasNext()){
							Object obj2=it.next();
							if(obj2 instanceof Diagram){
								d=(Diagram) obj2;
							}
						}
					}
					}
				}
				if (d == null) {
					return null;
				}
		        String editorID =  EditorDescriptionHelper.getEditorIdForDawnEditor(d.getName());
		        
		          if (editorID != null && !editorID.equals(""))
		          {
		            try
		            {
		              DawnEditorInput editorInput = new DawnEditorInput(uri);
		              IWorkbenchPage page = PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getActivePage();		              
		              page.openEditor(editorInput, editorID);
		            }
		            catch (PartInitException e)
		            {
		              e.printStackTrace();
		            }
		          }
	
			}else{
				//we are using files
				String finalDiagramLocation="";
				String modulesDir = Platform.getPreferencesService().
						  getString("org.eclipse.opencert.sam.preferences", UtilConstants.Module_PATH, "Modules Dir wrong", null);
				if (loca.startsWith("\\")){
					finalDiagramLocation=modulesDir.concat(loca);
					loca=finalDiagramLocation;
				}

				Style link = view.getStyle(NotationPackage.eINSTANCE
						.getHintedDiagramLinkStyle());
				if (false == link instanceof HintedDiagramLinkStyle) {
					return null;
				}
				return new ICommandProxy(new OpenFileDiagramCommand(
						(HintedDiagramLinkStyle) link, loca));
			}
			}// location is empty	
			//Are we in CDO or using files?
			URI u=view.eResource().getURI();
			if (u.toString().startsWith("cdo"))//We are in cdo
			{
				loca="cdo";
				return null;
			}else{
				loca=null;
			}
			Style link = view.getStyle(NotationPackage.eINSTANCE
					.getHintedDiagramLinkStyle());
			if (false == link instanceof HintedDiagramLinkStyle) {
				return null;
			}
			return new ICommandProxy(new OpenFileDiagramCommand(
					(HintedDiagramLinkStyle) link, loca));			
		}
		return null;
		
	}
	
	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	private CDOResourceNode checkFolderContents(CDOResourceFolder cdoResourceFolder, String searchDir) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN=  cdoResourceFolder.getNodes();
			for (int i=0; i<listN.size(); i++){
				String aux=listN.get(i).getPath();
				if(aux.contains(searchDir)){
					//we find it
					CDOResourceNode node=listN.get(i);
					return node;
				}
				else{
					if(listN.get(i) instanceof CDOResourceFolder){
						checkFolderContents((CDOResourceFolder)listN.get(i), searchDir);
					}
				}
			}
		}
		return null;
	}

	/**
	 * @generated NOT
	 */
	private static class OpenFileDiagramCommand extends
			AbstractTransactionalCommand {

		/**
		 * @generated
		 */
		private final HintedDiagramLinkStyle diagramFacet;
		private final String diagramLocation;
		
		/**
		 * @param loca 
		 * @generated
		 */
		OpenFileDiagramCommand(HintedDiagramLinkStyle linkStyle, String loca) {
			// editing domain is taken for original diagram, 
			// if we open diagram from another file, we should use another editing domain
			super(TransactionUtil.getEditingDomain(linkStyle),
					Messages.CommandName_OpenDiagram, null);
			diagramFacet = linkStyle;
			diagramLocation = loca;
		}

		// FIXME canExecute if  !(readOnly && getDiagramToOpen == null), i.e. open works on ro diagrams only when there's associated diagram already

		/**
		 * @generated NOT
		 */
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
				IAdaptable info) throws ExecutionException {
			try {
				Diagram diagram =null;
				URI uri=null;
				if (diagramLocation==null){
					diagram = initializeNewDiagram();
					uri = EcoreUtil.getURI(diagram);
				}
				if(diagramLocation=="cdo"){
					diagram = initializeNewRepositoryDiagram();
					uri = EcoreUtil.getURI(diagram);
					String editorID =  EditorDescriptionHelper.getEditorIdForDawnEditor(diagram.getName());
			        
			          if (editorID != null && !editorID.equals(""))
			          {
			            try
			            {
			              DawnEditorInput editorInput = new DawnEditorInput(uri);
			              IWorkbenchPage page = PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getActivePage();		              
			              page.openEditor(editorInput, editorID);
			            }
			            catch (PartInitException e)
			            {
			              e.printStackTrace();
			            }
			            return CommandResult.newOKCommandResult();
			          }
				}
				else{
					uri=URI.createURI(diagramLocation);
				}
//				String editorName = "org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorID";
				
				
				IWorkspace workspace= ResourcesPlugin.getWorkspace(); 
				IPath location= Path.fromOSString(uri.toString()); 
				IFile file2= workspace.getRoot().getFileForLocation(location); 
			    IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			    IWorkbenchPage page = window.getActivePage();
			    try {
			    	IEditorDescriptor[] aux = PlatformUI.getWorkbench().getEditorRegistry().getEditors(file2.getFullPath().toString());
			    	page.openEditor (new FileEditorInput(file2),
						aux[1].getId());
				  }
				  catch (PartInitException exception) {
						  exception.printStackTrace();
				  }
				return CommandResult.newOKCommandResult();
			} catch (Exception ex) {
				throw new ExecutionException("Can't open diagram", ex);
			}
		}

		/**
		 * @generated
		 */
		protected Diagram getDiagramToOpen() {
			
			return diagramFacet.getDiagramLink();
		}

		/**
		 * @generated NOT
		 */
		 private boolean copyToFile(String contentSourceFile, File destFile) throws IOException {
			 BufferedWriter writer = new BufferedWriter(new FileWriter(destFile));
		     writer.write(contentSourceFile);


			 return true;
		  }

		
		/**
		 * @generated NOT
		 */
		 private boolean copyFile(File sourceFile, File destFile) throws IOException {
			  FileChannel source = null;
			  FileChannel destination = null;
			  try {
			   source = new FileInputStream(sourceFile).getChannel();
			   destination = new FileOutputStream(destFile).getChannel();
			   destination.transferFrom(source, 0, source.size());
			  }
			  finally {
			   if(source != null) {
			    source.close();
			   } else if(destination != null) {
			    destination.close();
			   }
			 }
			 return true;
		  }
		
		
		/**
		 * @generated NOT
		 */
		protected Diagram initializeNewDiagram() throws ExecutionException {
			//Start MCP
			/* No funciona porque ViewService.createDiagrama porque debe ser especializacion de la clase diagram.
			 * Debo utilizar  RefframeworkDiagramEditorUtil.createDiagram
			Diagram d = ViewService.createDiagrma(getDiagramDomainElement(),
					getDiagramKind(), getPreferencesHint());
			*/
//			return null;
			String finalDiagramLocation="";
			String modulesDir = Platform.getPreferencesService().
					  getString("org.eclipse.opencert.sam.preferences", UtilConstants.Module_PATH, "Modules Dir wrong", null);
			if(modulesDir.compareTo("Modules Dir wrong") == 0)
			{
				throw new ExecutionException("Can't create diagram due to wrong Modules Dir");	
			}
			URI modulesDirURI = URI.createPlatformResourceURI(modulesDir, false);
			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(modulesDirURI.lastSegment()); ; //retrieve the ModulesDir IProject
			// create an empty diagram and model
			ShapeImpl sh = (ShapeImpl) diagramFacet.eContainer();
			ArgumentationImpl argImpl = (ArgumentationImpl)sh.basicGetElement();
			if(argImpl == null || argImpl.getId() == null) // si diagram relacionado a una View cuyo modelo ha sido borrado
			{
				throw new ExecutionException("Can't create diagram null.arg_diagram");				
			}
			String name = argImpl.getId();		
//			String location= modulesDir.concat(argImpl.getLocation());
			String location= argImpl.getLocation();
    		File locationFile = null; 
    		if(location != null) locationFile = new File(location);
    		Diagram d = null;
    		if(location == null || location.isEmpty() || locationFile == null || !locationFile.exists())
    		{
    			
				String diagramName = name + CaseEditPart.FILE_DIAGRAM_ID;
				String modelName =  name + CaseEditPart.FILE_MODEL_ID;
				URI projectPluginDiagramURI = URI.createPlatformResourceURI(ArgDiagramEditorPlugin.ID, false);
				IProject projectPluginDiagram = ResourcesPlugin.getWorkspace().getRoot().getProject(projectPluginDiagramURI.lastSegment());
				String emptyDiagram = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
									+ "<notation:Diagram xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:arg=\"arg\" xmlns:notation=\"http://www.eclipse.org/gmf/runtime/1.0.2/notation\" xmi:id=\"_n3jlgPBwEeKfGOouoiya7g\" type=\"Arg\" name=\"empty.arg_diagram\" measurementUnit=\"Pixel\"> "
									+ "<styles xmi:type=\"notation:DiagramStyle\" xmi:id=\"_n3jlgfBwEeKfGOouoiya7g\"/>"
									+ "<element xmi:type=\"arg:Case\" href=\"empty.arg#/\"/> "
									+ "</notation:Diagram>";
				String emptyModel = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
							        + "<arg:Case xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:arg=\"arg\"/>";
	    		File file2 = new File(modulesDir+"\\"+ diagramName);
	    		File file2M = new File(modulesDir+"\\"+ modelName);

	    		try {
	    			//diagram 
	    			copyToFile (emptyDiagram, file2);
	    			// model
	    			copyToFile(emptyModel, file2M);
	    		}
	    		 catch (IOException exception) {
	    			  exception.printStackTrace();
	    		}
	    		
	    		String diagramFName = project.getFile(diagramName).getFullPath().toString();
	    		finalDiagramLocation = diagramFName;
	    		String modelFName =  project.getFile(modelName).getFullPath().toString();
	    				
				URI diagramURI = URI.createPlatformResourceURI(diagramFName, false);
				URI modelURI = URI.createPlatformResourceURI(modelFName, false);
				Resource diagramResource = ArgDiagramEditorUtil.createDiagram(diagramURI, modelURI, new NullProgressMonitor());//Falla aqui.
				
				d = (Diagram) diagramResource.getContents().get(0);
    		}
    		else
    		{
    			String diagramFName = location;
    			finalDiagramLocation = diagramFName;
				//URI diagramURI = URI.createPlatformResourceURI(diagramFName, false);
				//Resource diagramResource = ArgDiagramEditorUtil.openDiagram(diagramURI);
				URI diagramURI = URI.createFileURI(diagramFName);
				ResourceSet resourceSet = getEditingDomain().getResourceSet();
				System.out.println("Loaded " + diagramURI);
				Resource diagramResource = resourceSet.getResource(diagramURI, true);
				d = (Diagram) diagramResource.getContents().get(0);
    		}
    		
    		// initialice location property
    		String locationprop = (String)argImpl.eGet(ArgPackage.Literals.ARGUMENTATION__LOCATION);
    		if(locationprop == null || locationprop.isEmpty() || locationprop == "")
    		{
    			String nombre = finalDiagramLocation.substring(finalDiagramLocation.lastIndexOf("/"), finalDiagramLocation.length());
    			String finalDiagramLocation2 = modulesDir + nombre; 
    			argImpl.eSet(ArgPackage.Literals.ARGUMENTATION__LOCATION, finalDiagramLocation2);
    		}
			// End MCP
			
			if (d == null) {
				throw new ExecutionException("Can't create diagram of '"
						+ getDiagramKind() + "' kind");
			}
			diagramFacet.setDiagramLink(d);
			assert diagramFacet.eResource() != null;
			//???diagramFacet.eResource().getContents().add(d);
			EObject container = diagramFacet.eContainer();
			while (container instanceof View) {
				((View) container).persist();
				container = container.eContainer();
			}
			try {
				new WorkspaceModifyOperation() {
					protected void execute(IProgressMonitor monitor)
							throws CoreException, InvocationTargetException,
							InterruptedException {
						try {
							for (Iterator it = diagramFacet.eResource()
									.getResourceSet().getResources().iterator(); it
									.hasNext();) {
								Resource nextResource = (Resource) it.next();
								if (nextResource.isLoaded()
										&& !getEditingDomain().isReadOnly(
												nextResource)) {
									nextResource.save(ArgDiagramEditorUtil
											.getSaveOptions());
								}
							}
						} catch (IOException ex) {
							throw new InvocationTargetException(ex,
									"Save operation failed");
						}
					}
				}.run(null);
			} catch (InvocationTargetException e) {
				throw new ExecutionException("Can't create diagram of '"
						+ getDiagramKind() + "' kind", e);
			} catch (InterruptedException e) {
				throw new ExecutionException("Can't create diagram of '"
						+ getDiagramKind() + "' kind", e);
			}
			
			return d;
		}

		protected Diagram initializeNewRepositoryDiagram() throws ExecutionException {
			String finalDiagramLocation="";
			String modulesDir = Platform.getPreferencesService().
					  getString("org.eclipse.opencert.sam.preferences", "cdoModulePathPreference", "Modules Dir wrong", null);
			
			if(modulesDir.compareTo("Modules Dir wrong") == 0)
			{
				throw new ExecutionException("Can't create diagram due to wrong Modules Dir");	
			}
			URI modulesDirURI = URI.createURI(modulesDir);
			//IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(modulesDirURI.lastSegment()); ; //retrieve the ModulesDir IProject
			// create an empty diagram and model
			ShapeImpl sh = (ShapeImpl) diagramFacet.eContainer();
			ArgumentationImpl argImpl = (ArgumentationImpl)sh.basicGetElement();
			if(argImpl == null || argImpl.getId() == null) // si diagram relacionado a una View cuyo modelo ha sido borrado
			{
				throw new ExecutionException("Can't create diagram null.arg_diagram");				
			}
			String name = argImpl.getId();		
			String location= modulesDir;
//    		File locationFile = null; 
//    		if(location != null) locationFile = new File(location);
    		Diagram d = null;
//    		if(location == null || location.isEmpty() || locationFile == null || !locationFile.exists())
//    		{
    		CDOConnectionUtil.instance.init(
					PreferenceConstants.getRepositoryName(),
					PreferenceConstants.getProtocol(),
					PreferenceConstants.getServerName());
			CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
			CDOView viewCDO = CDOConnectionUtil.instance.openView(sessionCDO);	
			CDOTransaction transaction=sessionCDO.openTransaction();	
				String diagramName = name + CaseEditPart.FILE_DIAGRAM_ID;
				String modelName =  name + CaseEditPart.FILE_MODEL_ID;
				URI projectPluginDiagramURI = URI.createPlatformResourceURI(ArgDiagramEditorPlugin.ID, false);

				String emptyDiagram = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
									+ "<notation:Diagram xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:arg=\"arg\" xmlns:notation=\"http://www.eclipse.org/gmf/runtime/1.0.2/notation\" xmi:id=\"_n3jlgPBwEeKfGOouoiya7g\" type=\"Arg\" name=\"empty.arg_diagram\" measurementUnit=\"Pixel\"> "
									+ "<styles xmi:type=\"notation:DiagramStyle\" xmi:id=\"_n3jlgfBwEeKfGOouoiya7g\"/>"
									+ "<element xmi:type=\"arg:Case\" href=\"empty.arg#/\"/> "
									+ "</notation:Diagram>";
				String emptyModel = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
							        + "<arg:Case xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:arg=\"arg\"/>";

	    		
	    		String diagramFName = modulesDir + '/'+ diagramName;
	    		finalDiagramLocation = diagramFName;
	    		String modelFName =   modulesDir + '/'+ modelName;
	    		
				URI diagramURI = URI.createURI(diagramFName);
				URI modelURI = URI.createURI(modelFName);
				Resource diagramResource = ArgDiagramEditorUtil.createDiagram(diagramURI, modelURI, new NullProgressMonitor());
				CDOResource argResource  = transaction.getOrCreateResource(modelFName);
				
				d = (Diagram) diagramResource.getContents().get(0);
    		
//    		}else
//    		{
//    			String diagramFName = location;
//    			finalDiagramLocation = diagramFName;
//				//URI diagramURI = URI.createPlatformResourceURI(diagramFName, false);
//				//Resource diagramResource = ArgDiagramEditorUtil.openDiagram(diagramURI);
//				URI diagramURI = URI.createFileURI(diagramFName);
//				ResourceSet resourceSet = getEditingDomain().getResourceSet();
//				System.out.println("Loaded " + diagramURI);
//				Resource diagramResource = resourceSet.getResource(diagramURI, true);
//				d = (Diagram) diagramResource.getContents().get(0);
//    		}
    		
    		// initialice location property
    		String locationprop = (String)argImpl.eGet(ArgPackage.Literals.ARGUMENTATION__LOCATION);
    		if(locationprop == null || locationprop.isEmpty() || locationprop == "")
    		{
    			String nombre = finalDiagramLocation.substring(finalDiagramLocation.lastIndexOf("/"), finalDiagramLocation.length());
    			String finalDiagramLocation2 = modulesDir + nombre; 
    			argImpl.eSet(ArgPackage.Literals.ARGUMENTATION__LOCATION, finalDiagramLocation2);
    		}
			// End MCP
			
			if (d == null) {
				throw new ExecutionException("Can't create diagram of '"
						+ getDiagramKind() + "' kind");
			}
			diagramFacet.setDiagramLink(d);
			assert diagramFacet.eResource() != null;
			//???diagramFacet.eResource().getContents().add(d);
			EObject container = diagramFacet.eContainer();
			while (container instanceof View) {
				((View) container).persist();
				container = container.eContainer();
			}
			try {
				new WorkspaceModifyOperation() {
					protected void execute(IProgressMonitor monitor)
							throws CoreException, InvocationTargetException,
							InterruptedException {
						try {
							for (Iterator it = diagramFacet.eResource()
									.getResourceSet().getResources().iterator(); it
									.hasNext();) {
								Resource nextResource = (Resource) it.next();
								if (nextResource.isLoaded()
										&& !getEditingDomain().isReadOnly(
												nextResource)) {
									nextResource.save(ArgDiagramEditorUtil
											.getSaveOptions());
								}
							}
						} catch (IOException ex) {
							throw new InvocationTargetException(ex,
									"Save operation failed");
						}
					}
				}.run(null);
			} catch (InvocationTargetException e) {
				throw new ExecutionException("Can't create diagram of '"
						+ getDiagramKind() + "' kind", e);
			} catch (InterruptedException e) {
				throw new ExecutionException("Can't create diagram of '"
						+ getDiagramKind() + "' kind", e);
			}
			return d;
		}
		/**
		 * @generated
		 */
		protected EObject getDiagramDomainElement() {
			// use same element as associated with EP
			return ((View) diagramFacet.eContainer()).getElement();
		}

		/**
		 * @generated
		 */
		protected PreferencesHint getPreferencesHint() {
			// XXX prefhint from target diagram's editor?
			return ArgDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT;
		}

		/**
		 * @generated
		 */
		protected String getDiagramKind() {
			return CaseEditPart.MODEL_ID;
		}

		/**
		 * @generated
		 */
		protected String getEditorID() {
			return ArgDiagramEditor.ID;
		}
		
	}

}
