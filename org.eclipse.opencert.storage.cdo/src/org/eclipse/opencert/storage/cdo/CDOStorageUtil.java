/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo;

import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.ecore.EObject;

public class CDOStorageUtil
{
    public static Long getCDOId(EObject target)
    {
        if (target == null) {
            throw new NullPointerException("Cannot get CDOId for null object parameter");
        }
        
        CDOID cdoId = CDOUtil.getCDOObject(target).cdoID();
        Long  cdoIdLong = new Long(cdoId.toURIFragment());
        
        return cdoIdLong;
    }
    
    
    public static String getMandatoryCDOQuerySuffix()
    {
        return getMandatoryCDOQuerySuffix(true);
    }
    
    public static String getMandatoryCDOQuerySuffix(boolean prependAnd)
    {
        String result = "";
        if (prependAnd) {
        	result += " and ";
        }
        result += " cdo_revised = 0 and cdo_version >= 0";
        
        return result;
    }
}
