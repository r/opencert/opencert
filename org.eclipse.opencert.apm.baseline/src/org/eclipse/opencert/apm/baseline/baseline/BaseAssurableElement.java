/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.common.util.EList;

import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Assurable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement#getEquivalenceMap <em>Equivalence Map</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement#getComplianceMap <em>Compliance Map</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement#getRefAssurableElement <em>Ref Assurable Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseAssurableElement()
 * @model abstract="true"
 * @extends CDOObject
 * @generated
 */
public interface BaseAssurableElement extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Equivalence Map</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equivalence Map</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equivalence Map</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseAssurableElement_EquivalenceMap()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseEquivalenceMap> getEquivalenceMap();

	/**
	 * Returns the value of the '<em><b>Compliance Map</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compliance Map</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compliance Map</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseAssurableElement_ComplianceMap()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseComplianceMap> getComplianceMap();

	/**
	 * Returns the value of the '<em><b>Ref Assurable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref Assurable Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Assurable Element</em>' reference.
	 * @see #setRefAssurableElement(RefAssurableElement)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseAssurableElement_RefAssurableElement()
	 * @model
	 * @generated
	 */
	RefAssurableElement getRefAssurableElement();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement#getRefAssurableElement <em>Ref Assurable Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref Assurable Element</em>' reference.
	 * @see #getRefAssurableElement()
	 * @generated
	 */
	void setRefAssurableElement(RefAssurableElement value);

} // BaseAssurableElement
