/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='org.eclipse.opencert.chess.tracemodel'"
 * @generated
 */
public interface OpenCertTraceLinkMetaModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "OpenCertTraceLinkMetaModel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "org.eclipse.opencert.aida.tracemodel.OpenCertTraceLinkMetaModel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "OpenCertTraceLinkMetaModel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OpenCertTraceLinkMetaModelPackage eINSTANCE = org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractClaimLinkImpl <em>Contract Claim Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractClaimLinkImpl
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getContractClaimLink()
	 * @generated
	 */
	int CONTRACT_CLAIM_LINK = 0;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_CLAIM_LINK__SOURCES = 0;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_CLAIM_LINK__TARGETS = 1;

	/**
	 * The number of structural features of the '<em>Contract Claim Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_CLAIM_LINK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Contract Claim Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_CLAIM_LINK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractArtefactLinkImpl <em>Contract Artefact Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractArtefactLinkImpl
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getContractArtefactLink()
	 * @generated
	 */
	int CONTRACT_ARTEFACT_LINK = 1;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_ARTEFACT_LINK__SOURCES = 0;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_ARTEFACT_LINK__TARGETS = 1;

	/**
	 * The number of structural features of the '<em>Contract Artefact Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_ARTEFACT_LINK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Contract Artefact Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_ARTEFACT_LINK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.FormalPropertyClaimLinkImpl <em>Formal Property Claim Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.FormalPropertyClaimLinkImpl
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getFormalPropertyClaimLink()
	 * @generated
	 */
	int FORMAL_PROPERTY_CLAIM_LINK = 2;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAL_PROPERTY_CLAIM_LINK__SOURCES = 0;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAL_PROPERTY_CLAIM_LINK__TARGETS = 1;

	/**
	 * The number of structural features of the '<em>Formal Property Claim Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAL_PROPERTY_CLAIM_LINK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Formal Property Claim Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAL_PROPERTY_CLAIM_LINK_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractAgreementLinkImpl <em>Contract Agreement Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractAgreementLinkImpl
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getContractAgreementLink()
	 * @generated
	 */
	int CONTRACT_AGREEMENT_LINK = 3;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_AGREEMENT_LINK__SOURCES = 0;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_AGREEMENT_LINK__TARGETS = 1;

	/**
	 * The number of structural features of the '<em>Contract Agreement Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_AGREEMENT_LINK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Contract Agreement Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_AGREEMENT_LINK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.AnalysisContextArtefactLinkImpl <em>Analysis Context Artefact Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.AnalysisContextArtefactLinkImpl
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getAnalysisContextArtefactLink()
	 * @generated
	 */
	int ANALYSIS_CONTEXT_ARTEFACT_LINK = 4;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_CONTEXT_ARTEFACT_LINK__SOURCES = 0;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_CONTEXT_ARTEFACT_LINK__TARGETS = 1;

	/**
	 * The number of structural features of the '<em>Analysis Context Artefact Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_CONTEXT_ARTEFACT_LINK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Analysis Context Artefact Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_CONTEXT_ARTEFACT_LINK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ComponentArgumentationElementLinkImpl <em>Component Argumentation Element Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ComponentArgumentationElementLinkImpl
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getComponentArgumentationElementLink()
	 * @generated
	 */
	int COMPONENT_ARGUMENTATION_ELEMENT_LINK = 5;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_ARGUMENTATION_ELEMENT_LINK__SOURCES = 0;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_ARGUMENTATION_ELEMENT_LINK__TARGETS = 1;

	/**
	 * The number of structural features of the '<em>Component Argumentation Element Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_ARGUMENTATION_ELEMENT_LINK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Component Argumentation Element Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_ARGUMENTATION_ELEMENT_LINK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.RequirementClaimLinkImpl <em>Requirement Claim Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.RequirementClaimLinkImpl
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getRequirementClaimLink()
	 * @generated
	 */
	int REQUIREMENT_CLAIM_LINK = 6;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_CLAIM_LINK__SOURCES = 0;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_CLAIM_LINK__TARGETS = 1;

	/**
	 * The number of structural features of the '<em>Requirement Claim Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_CLAIM_LINK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Requirement Claim Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_CLAIM_LINK_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink <em>Contract Claim Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contract Claim Link</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink
	 * @generated
	 */
	EClass getContractClaimLink();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sources</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink#getSources()
	 * @see #getContractClaimLink()
	 * @generated
	 */
	EReference getContractClaimLink_Sources();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Targets</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink#getTargets()
	 * @see #getContractClaimLink()
	 * @generated
	 */
	EReference getContractClaimLink_Targets();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractArtefactLink <em>Contract Artefact Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contract Artefact Link</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractArtefactLink
	 * @generated
	 */
	EClass getContractArtefactLink();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractArtefactLink#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sources</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractArtefactLink#getSources()
	 * @see #getContractArtefactLink()
	 * @generated
	 */
	EReference getContractArtefactLink_Sources();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractArtefactLink#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Targets</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractArtefactLink#getTargets()
	 * @see #getContractArtefactLink()
	 * @generated
	 */
	EReference getContractArtefactLink_Targets();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink <em>Formal Property Claim Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formal Property Claim Link</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink
	 * @generated
	 */
	EClass getFormalPropertyClaimLink();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sources</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink#getSources()
	 * @see #getFormalPropertyClaimLink()
	 * @generated
	 */
	EReference getFormalPropertyClaimLink_Sources();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Targets</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink#getTargets()
	 * @see #getFormalPropertyClaimLink()
	 * @generated
	 */
	EReference getFormalPropertyClaimLink_Targets();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink <em>Contract Agreement Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contract Agreement Link</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink
	 * @generated
	 */
	EClass getContractAgreementLink();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sources</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink#getSources()
	 * @see #getContractAgreementLink()
	 * @generated
	 */
	EReference getContractAgreementLink_Sources();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Targets</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink#getTargets()
	 * @see #getContractAgreementLink()
	 * @generated
	 */
	EReference getContractAgreementLink_Targets();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.AnalysisContextArtefactLink <em>Analysis Context Artefact Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Analysis Context Artefact Link</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.AnalysisContextArtefactLink
	 * @generated
	 */
	EClass getAnalysisContextArtefactLink();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.AnalysisContextArtefactLink#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sources</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.AnalysisContextArtefactLink#getSources()
	 * @see #getAnalysisContextArtefactLink()
	 * @generated
	 */
	EReference getAnalysisContextArtefactLink_Sources();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.AnalysisContextArtefactLink#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Targets</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.AnalysisContextArtefactLink#getTargets()
	 * @see #getAnalysisContextArtefactLink()
	 * @generated
	 */
	EReference getAnalysisContextArtefactLink_Targets();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink <em>Component Argumentation Element Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Argumentation Element Link</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink
	 * @generated
	 */
	EClass getComponentArgumentationElementLink();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sources</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink#getSources()
	 * @see #getComponentArgumentationElementLink()
	 * @generated
	 */
	EReference getComponentArgumentationElementLink_Sources();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Targets</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink#getTargets()
	 * @see #getComponentArgumentationElementLink()
	 * @generated
	 */
	EReference getComponentArgumentationElementLink_Targets();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.RequirementClaimLink <em>Requirement Claim Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requirement Claim Link</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.RequirementClaimLink
	 * @generated
	 */
	EClass getRequirementClaimLink();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.RequirementClaimLink#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sources</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.RequirementClaimLink#getSources()
	 * @see #getRequirementClaimLink()
	 * @generated
	 */
	EReference getRequirementClaimLink_Sources();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.RequirementClaimLink#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Targets</em>'.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.RequirementClaimLink#getTargets()
	 * @see #getRequirementClaimLink()
	 * @generated
	 */
	EReference getRequirementClaimLink_Targets();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OpenCertTraceLinkMetaModelFactory getOpenCertTraceLinkMetaModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractClaimLinkImpl <em>Contract Claim Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractClaimLinkImpl
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getContractClaimLink()
		 * @generated
		 */
		EClass CONTRACT_CLAIM_LINK = eINSTANCE.getContractClaimLink();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTRACT_CLAIM_LINK__SOURCES = eINSTANCE.getContractClaimLink_Sources();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTRACT_CLAIM_LINK__TARGETS = eINSTANCE.getContractClaimLink_Targets();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractArtefactLinkImpl <em>Contract Artefact Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractArtefactLinkImpl
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getContractArtefactLink()
		 * @generated
		 */
		EClass CONTRACT_ARTEFACT_LINK = eINSTANCE.getContractArtefactLink();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTRACT_ARTEFACT_LINK__SOURCES = eINSTANCE.getContractArtefactLink_Sources();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTRACT_ARTEFACT_LINK__TARGETS = eINSTANCE.getContractArtefactLink_Targets();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.FormalPropertyClaimLinkImpl <em>Formal Property Claim Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.FormalPropertyClaimLinkImpl
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getFormalPropertyClaimLink()
		 * @generated
		 */
		EClass FORMAL_PROPERTY_CLAIM_LINK = eINSTANCE.getFormalPropertyClaimLink();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMAL_PROPERTY_CLAIM_LINK__SOURCES = eINSTANCE.getFormalPropertyClaimLink_Sources();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMAL_PROPERTY_CLAIM_LINK__TARGETS = eINSTANCE.getFormalPropertyClaimLink_Targets();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractAgreementLinkImpl <em>Contract Agreement Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractAgreementLinkImpl
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getContractAgreementLink()
		 * @generated
		 */
		EClass CONTRACT_AGREEMENT_LINK = eINSTANCE.getContractAgreementLink();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTRACT_AGREEMENT_LINK__SOURCES = eINSTANCE.getContractAgreementLink_Sources();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTRACT_AGREEMENT_LINK__TARGETS = eINSTANCE.getContractAgreementLink_Targets();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.AnalysisContextArtefactLinkImpl <em>Analysis Context Artefact Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.AnalysisContextArtefactLinkImpl
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getAnalysisContextArtefactLink()
		 * @generated
		 */
		EClass ANALYSIS_CONTEXT_ARTEFACT_LINK = eINSTANCE.getAnalysisContextArtefactLink();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANALYSIS_CONTEXT_ARTEFACT_LINK__SOURCES = eINSTANCE.getAnalysisContextArtefactLink_Sources();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANALYSIS_CONTEXT_ARTEFACT_LINK__TARGETS = eINSTANCE.getAnalysisContextArtefactLink_Targets();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ComponentArgumentationElementLinkImpl <em>Component Argumentation Element Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ComponentArgumentationElementLinkImpl
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getComponentArgumentationElementLink()
		 * @generated
		 */
		EClass COMPONENT_ARGUMENTATION_ELEMENT_LINK = eINSTANCE.getComponentArgumentationElementLink();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_ARGUMENTATION_ELEMENT_LINK__SOURCES = eINSTANCE.getComponentArgumentationElementLink_Sources();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_ARGUMENTATION_ELEMENT_LINK__TARGETS = eINSTANCE.getComponentArgumentationElementLink_Targets();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.RequirementClaimLinkImpl <em>Requirement Claim Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.RequirementClaimLinkImpl
		 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelPackageImpl#getRequirementClaimLink()
		 * @generated
		 */
		EClass REQUIREMENT_CLAIM_LINK = eINSTANCE.getRequirementClaimLink();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_CLAIM_LINK__SOURCES = eINSTANCE.getRequirementClaimLink_Sources();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_CLAIM_LINK__TARGETS = eINSTANCE.getRequirementClaimLink_Targets();

	}

} //OpenCertTraceLinkMetaModelPackage
