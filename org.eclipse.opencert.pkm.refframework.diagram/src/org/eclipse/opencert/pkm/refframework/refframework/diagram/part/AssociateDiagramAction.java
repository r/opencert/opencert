/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.pkm.refframework.refframework.diagram.part;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;


import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.Disposable;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
//import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.commands.UnexecutableCommand;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.ui.action.AbstractActionHandler;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.ui.actions.ActionIds;
import org.eclipse.gmf.runtime.diagram.ui.actions.DiagramAction;
import org.eclipse.gmf.runtime.diagram.ui.commands.CommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.l10n.DiagramUIMessages;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.EditCommandRequestWrapper;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.resources.GMFResourceFactory;
import org.eclipse.gmf.runtime.emf.type.core.EditHelperContext;
import org.eclipse.gmf.runtime.emf.type.core.IClientContext;
import org.eclipse.gmf.runtime.emf.type.core.requests.AbstractEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.HintedDiagramLinkStyle;
import org.eclipse.gmf.runtime.notation.NotationFactory;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.Style;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.NodeImpl;
import org.eclipse.gmf.runtime.notation.impl.ShapeImpl;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.opencert.infra.general.general.impl.NamedElementImpl;
import org.eclipse.swt.dnd.DND;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyDelegatingOperation;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivity2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefFrameworkEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkDiagramEditor;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkDiagramEditorPlugin;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkDiagramEditorUtil;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.Messages;



import org.eclipse.emf.common.util.URI;


/**
 * This class (of Action type) is used to associate a diagram file to a Node element
 * 
 * @author M� Carmen Palacios
 */
/**
 * @generated NOT
 */
public class AssociateDiagramAction extends Action 
{
	private static int cont = 0;
	public static final String ACTION_ASSOCIATE = "AS";
	public static final String ACTION_DISSOCIATE = "DS";	
	
	private static final String ASSOCIATE_REQUEST = "Associate";
	private static final String DISSOCIATE_REQUEST = "Dissociate";
    private String actionId; 
    private HashMap actionMap = new HashMap();
    {
    	actionMap.put(ACTION_ASSOCIATE, ASSOCIATE_REQUEST); 
    	actionMap.put(ACTION_DISSOCIATE, DISSOCIATE_REQUEST);
    }
    private String diagramfullFileName="";
	private EditPart editPart;
	private ISelection selection;
	
	/**
	 * @generated NOT
	 */
	public AssociateDiagramAction(String diagramFile, ISelection selection, String actionId) {
		//super(part);
		// AHORA PASO EL FICHERO COMO UN RESOURCE!!!
		//diagramfullFileName = diagramFile;
		String kk = "platform:/resource/";
		int lastInd = diagramFile.length();  
		diagramfullFileName = diagramFile.substring(kk.length(), lastInd);
		this.actionId = actionId;
		this.selection = selection;
		editPart = null;
	}


	
	
	/**
	 * @generated NOT
	 */
	public void init() {
		//super.init();
	
		if(!diagramfullFileName.contains(RefFrameworkEditPart.FILE_DIAGRAM_ID)) return;
		int beginInd = diagramfullFileName.lastIndexOf("\\"); 
		if (beginInd == -1) beginInd = diagramfullFileName.lastIndexOf("/"); 
		if (actionId.equals(ACTION_ASSOCIATE)) 
		{ 
			cont++;
			setId("associateDiagram" + cont);
			setText("Associate diagram " + diagramfullFileName.substring(beginInd+1));
			setToolTipText("Associate diagram......");

		} 
		else if (actionId.equals(ACTION_DISSOCIATE)) {
			cont++;
			setId("dissociateDiagram" + cont);
			setText("Dissociate diagram " + diagramfullFileName.substring(beginInd+1));
			setToolTipText("Dissociate diagram......");
		} 
		
	}

	/**
	 * @generated NOT
	 */
	protected String getCommandLabel() {
		return "MCP Action"; //????
	}

	/**
	 * @generated NOT
	 */
	/*
	// OJO: No se me invoka
	public void selectionChanged(IAction action, ISelection selection)
	{
		IStructuredSelection s = (IStructuredSelection)selection;
		if(!s.isEmpty() && s.getFirstElement() instanceof RefActivityEditPart)
		{
			editPart = (RefActivityEditPart)s.getFirstElement();
		}
	}
	*/

	/**
	 * @generated NOT
	 */
	public void run()
	{
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
	    IWorkbenchPage page = window.getActivePage();
	    ISelection selection = page.getSelection();
		
		IStructuredSelection s = (IStructuredSelection)selection;
		s = (IStructuredSelection)this.selection;
		if(s!= null && !s.isEmpty() && (s.getFirstElement() instanceof RefActivityEditPart || s.getFirstElement() instanceof RefActivity2EditPart || s.getFirstElement() instanceof RefArtefactEditPart  || s.getFirstElement() instanceof RefRoleEditPart ))
		//if(s!= null && !s.isEmpty() && s.getFirstElement() instanceof NamedElementImpl)
		{
			editPart = (EditPart)s.getFirstElement();
		}

		if(editPart != null)
		{
			View view = (View) editPart.getModel();
			EObject element = view.getElement();
			//final String diagramName = "kk";
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(element);
			AssociateDiagramCommand cmd = new AssociateDiagramCommand(domain, "test associate MCP", null);
			cmd.init(view, diagramfullFileName);
			try {
			    cmd.execute(new NullProgressMonitor(),  null);
			} catch (ExecutionException ex) {
				ex.printStackTrace();
			}
		}
	}

	
	/**
	 * @generated NOT
	 */
	protected HintedDiagramLinkStyle getHintedDiagramLink(View view, String diagramName) {
		HintedDiagramLinkStyle diagramFacet = null;
		boolean found = false;
		for( Object element : view.getStyles())
		{
			if (true == element instanceof HintedDiagramLinkStyle)
			{
				HintedDiagramLinkStyle elementHD = (HintedDiagramLinkStyle)element;
				if(elementHD.getHint() != null && elementHD.getHint().compareTo("Sub"+diagramName) == 0)
				{
					found = true;
					diagramFacet = elementHD;
				}
			}
		}
		if(!found)
		{
			String diagramFacetName = "Sub"+diagramName;
			diagramFacet = NotationFactory.eINSTANCE.createHintedDiagramLinkStyle();
			diagramFacet.setHint(diagramFacetName); 
			//OJO FALTA!!!diagramFacet.eContainer() = 
			view.getStyles().add(diagramFacet);
		}
		
		return diagramFacet;
		}
	
	/**
	 * @generated NOT
	 */
	protected HintedDiagramLinkStyle addHintedDiagramLink(View view, String diagramName) {
		HintedDiagramLinkStyle diagramFacet = null;
		boolean found = false;
		for( Object element : view.getStyles())
		{
			if (true == element instanceof HintedDiagramLinkStyle)
			{
				HintedDiagramLinkStyle elementHD = (HintedDiagramLinkStyle)element;
				if(elementHD.getHint() != null && elementHD.getHint().compareTo("Sub"+diagramName) == 0)
				{
					found = true;
					diagramFacet = elementHD;
				}
			}
		}
		if(!found)
		{
			String diagramFacetName = "Sub"+diagramName;
			diagramFacet = NotationFactory.eINSTANCE.createHintedDiagramLinkStyle();
			diagramFacet.setHint(diagramFacetName); 
			view.getStyles().add(diagramFacet);
		}
		
		return diagramFacet;
		}
	
	/**
	 * @generated NOT
	 */
	public class AssociateDiagramCommand extends AbstractTransactionalCommand{
		TransactionalEditingDomain myEditingDomain;
		View view;
		String diagramfullFileName;
		
		public AssociateDiagramCommand (TransactionalEditingDomain domain, String label, List affectedFiles)
		{
			super(domain, label, affectedFiles);
			myEditingDomain = domain;
		}
		
		public void init(View view, String diagramfullFileName)
		{
			this.view = view;
			this.diagramfullFileName = diagramfullFileName;
		}

		
		/**
		 * @generated NOT
		 */
		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
				IAdaptable info) throws ExecutionException {
			try {
				/*
				Diagram diagram = ((HintedDiagramLinkStyle) view.getStyle(NotationPackage.eINSTANCE
						.getHintedDiagramLinkStyle())).getDiagramLink();
				*/
				HintedDiagramLinkStyle hinted = getHintedDiagramLink(view, diagramfullFileName);
				Diagram diagram = hinted.getDiagramLink();
				if(diagram == null)
				{
					/*
					diagram = intializeNewDiagram((HintedDiagramLinkStyle) view.getStyle(NotationPackage.eINSTANCE
							.getHintedDiagramLinkStyle()));
					*/
					diagram = intializeNewDiagram(hinted);
				}
				/*
				URI uri = EcoreUtil.getURI(diagram);
				String editorName = uri.toFileString();				
				if(editorName == null && (diagram.getName() == null || diagram.getName().compareTo("") == 0)) // si diagram relacionado a una View cuyo modelo ha sido borrado
				{
					//+ diagram.getName());					
					throw new ExecutionException("Can't open diagram '");
				}
				IEditorInput editorInput = new URIEditorInput(uri, editorName);
				IWorkbenchPage page = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage();
				page.openEditor(editorInput, getEditorID());
				*/
				return CommandResult.newOKCommandResult();

			} catch (Exception ex) {
				throw new ExecutionException("Can't associate the diagram", ex);
			}
		}
		
		/**
		 * @generated NOT
		 */
		protected String getDiagramKind() {
			return RefFrameworkEditPart.MODEL_ID;
		}

		/**
		 * @generated NOT
		 */
		protected String getEditorID() {
			return RefframeworkDiagramEditor.ID;
		}
	
		/**
		 * @generated NOT
		 */
		public boolean copyToFile(String contentSourceFile, File destFile) throws IOException {
			 BufferedWriter writer = new BufferedWriter(new FileWriter(destFile));
		     writer.write(contentSourceFile);

			 return true;
		}
		
		/**
		 * @generated NOT
		 */
		protected Diagram intializeNewDiagram(final HintedDiagramLinkStyle diagramFacet) throws ExecutionException {
			/* No funciona porque ViewService.createDiagrama porque debe ser especializacion de la clase diagram.
			 * Debo utilizar  RefframeworkDiagramEditorUtil.createDiagram
			Diagram d = ViewService.createDiagrma(getDiagramDomainElement(),
					getDiagramKind(), getPreferencesHint());
			*/
			NodeImpl sh = (NodeImpl) diagramFacet.eContainer();
			NamedElementImpl neImpl = (NamedElementImpl)sh.basicGetElement();
			//NamedElementImpl neImpl = (NamedElementImpl)view.getElement();
			//if(neImpl == null || neImpl.getId() == null) // si diagram relacionado a una View cuyo modelo ha sido borrado
			if(neImpl == null || neImpl.getName() == null)
			{
				throw new ExecutionException("Can't associate a diagram to the element");				
			}
	
			String diagramFName = diagramfullFileName;
			URI diagramURI = URI.createPlatformResourceURI(diagramFName, false);
			ResourceSet resourceSet = myEditingDomain.getResourceSet();			
			/* en el editingDomain solo tengo los diagramas que son hintedlinks 
			for (Resource nextResource : myEditingDomain.getResourceSet()
					.getResources()) {
				URI nextURI = nextResource.getURI();
				System.out.println("resource = " + nextURI.toString());
			}
			Resource diagramResource = resourceSet.getResource(diagramURI, true);
			*/
			//Funciona pero no esta cargado el recurso: Resource diagramResource = myEditingDomain.getResourceSet().createResource(diagramURI);
			Resource diagramResource ;			
			diagramResource = myEditingDomain.getResourceSet().getResource(diagramURI.trimFragment(), false);
			if (diagramResource == null) {
				diagramResource = myEditingDomain.getResourceSet().createResource(diagramURI.trimFragment());
			}
			if (!diagramResource.isLoaded()) {
				try {
					Map options = new HashMap(GMFResourceFactory.getDefaultLoadOptions());
					// @see 171060 
					// options.put(org.eclipse.emf.ecore.xmi.XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, Boolean.TRUE);
					diagramResource.load(options);
				} catch (IOException e) {
					diagramResource.unload();
					//throw e;
					throw new ExecutionException("Can't lod diagram'");
				}
			}
			// abre un diagrama ya creado!!!: Resource diagramResource = RefframeworkDiagramEditorUtil.openDiagram(diagramURI);			
			Diagram d = (Diagram) diagramResource.getContents().get(0);			
			// MCP End
			if (d == null) {
				throw new ExecutionException("Can't create diagram of '"
						+ getDiagramKind() + "' kind");
			}
			d.setName(diagramfullFileName);
			diagramFacet.setDiagramLink(d);
			assert diagramFacet.eResource() != null;
			//???diagramFacet.eResource().getContents().add(d);
			EObject container = diagramFacet.eContainer();
			while (container instanceof View) {
				((View) container).persist();
				container = container.eContainer();
			}
			try {
				new WorkspaceModifyOperation() {
					protected void execute(IProgressMonitor monitor)
							throws CoreException, InvocationTargetException,
							InterruptedException {
						try {
							for (Iterator it = diagramFacet.eResource()
									.getResourceSet().getResources().iterator(); it
									.hasNext();) {
								Resource nextResource = (Resource) it.next();
								if (nextResource.isLoaded()
										&& !getEditingDomain().isReadOnly(
												nextResource)) {
									nextResource
											.save(RefframeworkDiagramEditorUtil
													.getSaveOptions());
								}
							}
						} catch (IOException ex) {
							throw new InvocationTargetException(ex,
									"Save operation failed");
						}
					}
				}.run(null);
			} catch (InvocationTargetException e) {
				throw new ExecutionException("Can't create diagram of '"
						+ getDiagramKind() + "' kind", e);
			} catch (InterruptedException e) {
				throw new ExecutionException("Can't create diagram of '"
						+ getDiagramKind() + "' kind", e);
			}
			return d;
		}

		}; // new AbstractTransactionalCommand
	
}

