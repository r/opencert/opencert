/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.part;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EPackageEditPart;
import org.eclipse.emf.ecoretools.diagram.part.EcoreDiagramEditorPlugin;
import org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractModelNavigator;
import org.eclipse.emf.ecoretools.diagram.ui.outline.IOutlineMenuConstants;
import org.eclipse.emf.ecoretools.diagram.ui.outline.NavigatorLabelProvider;
import org.eclipse.emf.ecoretools.diagram.ui.outline.actions.CreateDiagramAction;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.emf.ecoretools.diagram.ui.outline.internal.Activator;
/**
* A navigator that gives a model-oriented view in the outline.
*/
public class ArgModelNavigator extends AbstractModelNavigator {
			// Start MCP	
			@Override
			protected void initProviders() {
				// TODO Auto-generated method stub
            	AdapterFactoryContentProvider adapterContentProvider = new NavigatorAdapterFactoryContentProvider(getAdapterFactory());
        		adapterContentProvider.inputChanged(getTreeViewer(), null, null);
        		getTreeViewer().setContentProvider(new MyNavigatorContentProvider(getDiagramResource(), adapterContentProvider));

        		ILabelProvider fullLabelProvider = new DecoratingLabelProvider(new NavigatorLabelProvider(new AdapterFactoryLabelProvider(getAdapterFactory())), Activator.getDefault().getWorkbench()
        				.getDecoratorManager().getLabelDecorator());
        		getTreeViewer().setLabelProvider(fullLabelProvider);
																	
			}
			// End MCP			

            /**
             * Constructor
             * 
             * @param parent
             *            the parent Composite
             * @param diagEditor
             *            the diagram editor
             * @param pageSite
             *            the IPageSite
             */
            public ArgModelNavigator(Composite parent, IDiagramWorkbenchPart diagEditor, IPageSite pageSite) {
                    super(parent, diagEditor, pageSite);
            }
    
            /**
             * @see org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractModelNavigator#getAdapterFactory()
             */
            @Override
            protected AdapterFactory getAdapterFactory() {
                    return EcoreDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory();
            }
    
            /**
             * @see org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractModelNavigator#createDiagramsMenu(org.eclipse.jface.action.IMenuManager,
             *      org.eclipse.emf.ecore.EObject)
             */
            @Override
            protected void createDiagramsMenu(IMenuManager manager, EObject selectedObject) {
                    super.createDiagramsMenu(manager, selectedObject);
    
                    if (selectedObject instanceof EPackage) {
                            manager.appendToGroup(IOutlineMenuConstants.NEW_GROUP, new CreateDiagramAction(selectedObject, getDiagramResource(), EPackageEditPart.MODEL_ID,
                                            EcoreDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT));
                    }
            }
    
    }
