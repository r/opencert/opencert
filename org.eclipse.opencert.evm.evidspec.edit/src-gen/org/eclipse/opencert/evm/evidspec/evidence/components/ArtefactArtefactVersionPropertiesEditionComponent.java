/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.components;

// Start of user code for imports
import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.evm.evidspec.evidence.Resource;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class ArtefactArtefactVersionPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String ARTEFACTVERSION_PART = "ArtefactVersion"; //$NON-NLS-1$

	
	/**
	 * Settings for resource ReferencesTable
	 */
	protected ReferencesTableSettings resourceSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ArtefactArtefactVersionPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject artefact, String editing_mode) {
		super(editingContext, artefact, editing_mode);
		parts = new String[] { ARTEFACTVERSION_PART };
		repositoryKey = EvidenceViewsRepository.class;
		partKey = EvidenceViewsRepository.ArtefactVersion.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Artefact artefact = (Artefact)elt;
			final ArtefactVersionPropertiesEditionPart artefactVersionPart = (ArtefactVersionPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.versionID))
				artefactVersionPart.setVersionID(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, artefact.getVersionID()));
			
			if (isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.date))
				artefactVersionPart.setDate(EEFConverterUtil.convertToString(EcorePackage.Literals.EDATE, artefact.getDate()));
			
			if (isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.changes))
				artefactVersionPart.setChanges(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, artefact.getChanges()));
			
			if (isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion)) {
				artefactVersionPart.setIsLastVersion(artefact.isIsLastVersion());
			}
			if (isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate)) {
				artefactVersionPart.setIsTemplate(artefact.isIsTemplate());
			}
			if (isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable)) {
				artefactVersionPart.setIsConfigurable(artefact.isIsConfigurable());
			}
			if (isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.resource)) {
				resourceSettings = new ReferencesTableSettings(artefact, EvidencePackage.eINSTANCE.getArtefact_Resource());
				artefactVersionPart.initResource(resourceSettings);
			}
			// init filters
			
			
			
			
			
			
			if (isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.resource)) {
				artefactVersionPart.addFilterToResource(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof Resource); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for resource
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}










	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == EvidenceViewsRepository.ArtefactVersion.Properties.versionID) {
			return EvidencePackage.eINSTANCE.getArtefact_VersionID();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactVersion.Properties.date) {
			return EvidencePackage.eINSTANCE.getArtefact_Date();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactVersion.Properties.changes) {
			return EvidencePackage.eINSTANCE.getArtefact_Changes();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion) {
			return EvidencePackage.eINSTANCE.getArtefact_IsLastVersion();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate) {
			return EvidencePackage.eINSTANCE.getArtefact_IsTemplate();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable) {
			return EvidencePackage.eINSTANCE.getArtefact_IsConfigurable();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactVersion.Properties.resource) {
			return EvidencePackage.eINSTANCE.getArtefact_Resource();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Artefact artefact = (Artefact)semanticObject;
		if (EvidenceViewsRepository.ArtefactVersion.Properties.versionID == event.getAffectedEditor()) {
			artefact.setVersionID((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (EvidenceViewsRepository.ArtefactVersion.Properties.date == event.getAffectedEditor()) {
			artefact.setDate((java.util.Date)EEFConverterUtil.createFromString(EcorePackage.Literals.EDATE, (String)event.getNewValue()));
		}
		if (EvidenceViewsRepository.ArtefactVersion.Properties.changes == event.getAffectedEditor()) {
			artefact.setChanges((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion == event.getAffectedEditor()) {
			//Start IRR
			if ((Boolean)event.getNewValue()) {
				ArtefactDefinition artdefinition =(ArtefactDefinition)artefact.eContainer();
				EList<Artefact> allVersion = artdefinition.getArtefact();
				Iterator<Artefact> iter = allVersion.iterator();
				
				while (iter.hasNext()) {
					Artefact oneArtefact= (Artefact) iter.next();
					oneArtefact.setIsLastVersion(false);
				}
			}
			//End IRR
			
			artefact.setIsLastVersion((Boolean)event.getNewValue());
		}
		if (EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate == event.getAffectedEditor()) {
			artefact.setIsTemplate((Boolean)event.getNewValue());
		}
		if (EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable == event.getAffectedEditor()) {
			artefact.setIsConfigurable((Boolean)event.getNewValue());
		}
		if (EvidenceViewsRepository.ArtefactVersion.Properties.resource == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, resourceSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				resourceSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				resourceSettings.move(event.getNewIndex(), (Resource) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ArtefactVersionPropertiesEditionPart artefactVersionPart = (ArtefactVersionPropertiesEditionPart)editingPart;
			if (EvidencePackage.eINSTANCE.getArtefact_VersionID().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && artefactVersionPart != null && isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.versionID)) {
				if (msg.getNewValue() != null) {
					artefactVersionPart.setVersionID(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					artefactVersionPart.setVersionID("");
				}
			}
			if (EvidencePackage.eINSTANCE.getArtefact_Date().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && artefactVersionPart != null && isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.date)) {
				if (msg.getNewValue() != null) {
					artefactVersionPart.setDate(EcoreUtil.convertToString(EcorePackage.Literals.EDATE, msg.getNewValue()));
				} else {
					artefactVersionPart.setDate("");
				}
			}
			if (EvidencePackage.eINSTANCE.getArtefact_Changes().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && artefactVersionPart != null && isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.changes)) {
				if (msg.getNewValue() != null) {
					artefactVersionPart.setChanges(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					artefactVersionPart.setChanges("");
				}
			}
			if (EvidencePackage.eINSTANCE.getArtefact_IsLastVersion().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && artefactVersionPart != null && isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion))
				artefactVersionPart.setIsLastVersion((Boolean)msg.getNewValue());
			
			if (EvidencePackage.eINSTANCE.getArtefact_IsTemplate().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && artefactVersionPart != null && isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate))
				artefactVersionPart.setIsTemplate((Boolean)msg.getNewValue());
			
			if (EvidencePackage.eINSTANCE.getArtefact_IsConfigurable().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && artefactVersionPart != null && isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable))
				artefactVersionPart.setIsConfigurable((Boolean)msg.getNewValue());
			
			if (EvidencePackage.eINSTANCE.getArtefact_Resource().equals(msg.getFeature()) && isAccessible(EvidenceViewsRepository.ArtefactVersion.Properties.resource))
				artefactVersionPart.updateResource();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			EvidencePackage.eINSTANCE.getArtefact_VersionID(),
			EvidencePackage.eINSTANCE.getArtefact_Date(),
			EvidencePackage.eINSTANCE.getArtefact_Changes(),
			EvidencePackage.eINSTANCE.getArtefact_IsLastVersion(),
			EvidencePackage.eINSTANCE.getArtefact_IsTemplate(),
			EvidencePackage.eINSTANCE.getArtefact_IsConfigurable(),
			EvidencePackage.eINSTANCE.getArtefact_Resource()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#isRequired(java.lang.Object, int)
	 * 
	 */
	public boolean isRequired(Object key, int kind) {
		return key == EvidenceViewsRepository.ArtefactVersion.Properties.versionID;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (EvidenceViewsRepository.ArtefactVersion.Properties.versionID == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefact_VersionID().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefact_VersionID().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactVersion.Properties.date == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefact_Date().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefact_Date().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactVersion.Properties.changes == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefact_Changes().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefact_Changes().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefact_IsLastVersion().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefact_IsLastVersion().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefact_IsTemplate().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefact_IsTemplate().getEAttributeType(), newValue);
				}
				if (EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(EvidencePackage.eINSTANCE.getArtefact_IsConfigurable().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(EvidencePackage.eINSTANCE.getArtefact_IsConfigurable().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
