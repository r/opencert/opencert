/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Assurable Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseAssurableElementImpl#getEquivalenceMap <em>Equivalence Map</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseAssurableElementImpl#getComplianceMap <em>Compliance Map</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseAssurableElementImpl#getRefAssurableElement <em>Ref Assurable Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class BaseAssurableElementImpl extends CDOObjectImpl implements BaseAssurableElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseAssurableElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BaselinePackage.Literals.BASE_ASSURABLE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseEquivalenceMap> getEquivalenceMap() {
		return (EList<BaseEquivalenceMap>)eGet(BaselinePackage.Literals.BASE_ASSURABLE_ELEMENT__EQUIVALENCE_MAP, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseComplianceMap> getComplianceMap() {
		return (EList<BaseComplianceMap>)eGet(BaselinePackage.Literals.BASE_ASSURABLE_ELEMENT__COMPLIANCE_MAP, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefAssurableElement getRefAssurableElement() {
		return (RefAssurableElement)eGet(BaselinePackage.Literals.BASE_ASSURABLE_ELEMENT__REF_ASSURABLE_ELEMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefAssurableElement(RefAssurableElement newRefAssurableElement) {
		eSet(BaselinePackage.Literals.BASE_ASSURABLE_ELEMENT__REF_ASSURABLE_ELEMENT, newRefAssurableElement);
	}

} //BaseAssurableElementImpl
