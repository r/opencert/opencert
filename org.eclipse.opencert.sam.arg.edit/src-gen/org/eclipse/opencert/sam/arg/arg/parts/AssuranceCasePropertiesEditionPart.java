/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface AssuranceCasePropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);




	/**
	 * Init the hasArgument
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initHasArgument(ReferencesTableSettings settings);

	/**
	 * Update the hasArgument
	 * @param newValue the hasArgument to update
	 * 
	 */
	public void updateHasArgument();

	/**
	 * Adds the given filter to the hasArgument edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToHasArgument(ViewerFilter filter);

	/**
	 * Adds the given filter to the hasArgument edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToHasArgument(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the hasArgument table
	 * 
	 */
	public boolean isContainedInHasArgumentTable(EObject element);




	/**
	 * Init the hasEvidence
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initHasEvidence(ReferencesTableSettings settings);

	/**
	 * Update the hasEvidence
	 * @param newValue the hasEvidence to update
	 * 
	 */
	public void updateHasEvidence();

	/**
	 * Adds the given filter to the hasEvidence edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToHasEvidence(ViewerFilter filter);

	/**
	 * Adds the given filter to the hasEvidence edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToHasEvidence(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the hasEvidence table
	 * 
	 */
	public boolean isContainedInHasEvidenceTable(EObject element);




	/**
	 * Init the composedOf
	 * @param settings settings for the composedOf ReferencesTable 
	 */
	public void initComposedOf(ReferencesTableSettings settings);

	/**
	 * Update the composedOf
	 * @param newValue the composedOf to update
	 * 
	 */
	public void updateComposedOf();

	/**
	 * Adds the given filter to the composedOf edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToComposedOf(ViewerFilter filter);

	/**
	 * Adds the given filter to the composedOf edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToComposedOf(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the composedOf table
	 * 
	 */
	public boolean isContainedInComposedOfTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
