/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;

// End of user code

/**
 * 
 * 
 */
public class ArtefactEventsPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ArtefactEventsPropertiesEditionPart {

	protected ReferencesTable lifecycleEvent;
	protected List<ViewerFilter> lifecycleEventBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> lifecycleEventFilters = new ArrayList<ViewerFilter>();
	protected TableViewer lifecycleEventTable;
	protected List<ViewerFilter> lifecycleEventTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> lifecycleEventTableFilters = new ArrayList<ViewerFilter>();
	protected Button addLifecycleEventTable;
	protected Button removeLifecycleEventTable;
	protected Button editLifecycleEventTable;



	/**
	 * For {@link ISection} use only.
	 */
	public ArtefactEventsPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArtefactEventsPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence artefactEventsStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = artefactEventsStep.addStep(EvidenceViewsRepository.ArtefactEvents.Properties.class);
		
		// Start IRR
		// propertiesStep.addStep(EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent);
		// Fin IRR
		
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable);
		
		
		composer = new PartComposer(artefactEventsStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.ArtefactEvents.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent) {
					return createLifecycleEventTableComposition(widgetFactory, parent);
				}
				if (key == EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable) {
					return createLifecycleEventTableTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(EvidenceMessages.ArtefactEventsPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createLifecycleEventTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.lifecycleEvent = new ReferencesTable(getDescription(EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent, EvidenceMessages.ArtefactEventsPropertiesEditionPart_LifecycleEventLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEventsPropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				lifecycleEvent.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEventsPropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				lifecycleEvent.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEventsPropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				lifecycleEvent.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEventsPropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				lifecycleEvent.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.lifecycleEventFilters) {
			this.lifecycleEvent.addFilter(filter);
		}
		this.lifecycleEvent.setHelpText(propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent, EvidenceViewsRepository.FORM_KIND));
		this.lifecycleEvent.createControls(parent, widgetFactory);
		this.lifecycleEvent.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEventsPropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData lifecycleEventData = new GridData(GridData.FILL_HORIZONTAL);
		lifecycleEventData.horizontalSpan = 3;
		this.lifecycleEvent.setLayoutData(lifecycleEventData);
		this.lifecycleEvent.setLowerBound(0);
		this.lifecycleEvent.setUpperBound(-1);
		lifecycleEvent.setID(EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent);
		lifecycleEvent.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createLifecycleEventTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createLifecycleEventTableTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableLifecycleEventTable = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableLifecycleEventTable.setHeaderVisible(true);
		GridData gdLifecycleEventTable = new GridData();
		gdLifecycleEventTable.grabExcessHorizontalSpace = true;
		gdLifecycleEventTable.horizontalAlignment = GridData.FILL;
		gdLifecycleEventTable.grabExcessVerticalSpace = true;
		gdLifecycleEventTable.verticalAlignment = GridData.FILL;
		tableLifecycleEventTable.setLayoutData(gdLifecycleEventTable);
		tableLifecycleEventTable.setLinesVisible(true);

		// Start of user code for columns definition for LifecycleEventTable
		// Start IRR

				TableColumn name = new TableColumn(tableLifecycleEventTable, SWT.NONE);
				name.setWidth(80);
				name.setText("Name"); //$NON-NLS-1$

				TableColumn name1 = new TableColumn(tableLifecycleEventTable, SWT.NONE);
				name1.setWidth(80);
				name1.setText("Description"); //$NON-NLS-1$

				// End IRR
		// End of user code

		lifecycleEventTable = new TableViewer(tableLifecycleEventTable);
		lifecycleEventTable.setContentProvider(new ArrayContentProvider());
		lifecycleEventTable.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for LifecycleEventTable
						public String getColumnText(Object object, int columnIndex) {
							
							// Start IRR
							
							/*AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
							if (object instanceof EObject) {
								switch (columnIndex) {
								case 0:
									return labelProvider.getText(object);
								}
							}*/
							
							if (object instanceof EObject) {
								AssuranceAssetEvent assuranceAssetEventObject = (AssuranceAssetEvent)object;
								switch (columnIndex) {
								case 0:							
									return assuranceAssetEventObject.getName();
								case 1:							
									return assuranceAssetEventObject.getDescription();
								
								}
							}
							
							return ""; //$NON-NLS-1$
						}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		lifecycleEventTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (lifecycleEventTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) lifecycleEventTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEventsPropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						lifecycleEventTable.refresh();
					}
				}
			}

		});
		GridData lifecycleEventTableData = new GridData(GridData.FILL_HORIZONTAL);
		lifecycleEventTableData.minimumHeight = 120;
		lifecycleEventTableData.heightHint = 120;
		lifecycleEventTable.getTable().setLayoutData(lifecycleEventTableData);
		for (ViewerFilter filter : this.lifecycleEventTableFilters) {
			lifecycleEventTable.addFilter(filter);
		}
		EditingUtils.setID(lifecycleEventTable.getTable(), EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable);
		EditingUtils.setEEFtype(lifecycleEventTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createLifecycleEventTablePanel(widgetFactory, tableContainer);
		// Start of user code for createLifecycleEventTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createLifecycleEventTablePanel(FormToolkit widgetFactory, Composite container) {
		Composite lifecycleEventTablePanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout lifecycleEventTablePanelLayout = new GridLayout();
		lifecycleEventTablePanelLayout.numColumns = 1;
		lifecycleEventTablePanel.setLayout(lifecycleEventTablePanelLayout);
		addLifecycleEventTable = widgetFactory.createButton(lifecycleEventTablePanel, EvidenceMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addLifecycleEventTableData = new GridData(GridData.FILL_HORIZONTAL);
		addLifecycleEventTable.setLayoutData(addLifecycleEventTableData);
		addLifecycleEventTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEventsPropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				lifecycleEventTable.refresh();
			}
		});
		EditingUtils.setID(addLifecycleEventTable, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable);
		EditingUtils.setEEFtype(addLifecycleEventTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeLifecycleEventTable = widgetFactory.createButton(lifecycleEventTablePanel, EvidenceMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeLifecycleEventTableData = new GridData(GridData.FILL_HORIZONTAL);
		removeLifecycleEventTable.setLayoutData(removeLifecycleEventTableData);
		removeLifecycleEventTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (lifecycleEventTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) lifecycleEventTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEventsPropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						lifecycleEventTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeLifecycleEventTable, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable);
		EditingUtils.setEEFtype(removeLifecycleEventTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editLifecycleEventTable = widgetFactory.createButton(lifecycleEventTablePanel, EvidenceMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editLifecycleEventTableData = new GridData(GridData.FILL_HORIZONTAL);
		editLifecycleEventTable.setLayoutData(editLifecycleEventTableData);
		editLifecycleEventTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (lifecycleEventTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) lifecycleEventTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEventsPropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						lifecycleEventTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editLifecycleEventTable, EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable);
		EditingUtils.setEEFtype(editLifecycleEventTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createLifecycleEventTablePanel

		// End of user code
		return lifecycleEventTablePanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart#initLifecycleEvent(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initLifecycleEvent(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		lifecycleEvent.setContentProvider(contentProvider);
		lifecycleEvent.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEvent);
		if (eefElementEditorReadOnlyState && lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(false);
			lifecycleEvent.setToolTipText(EvidenceMessages.ArtefactEvents_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart#updateLifecycleEvent()
	 * 
	 */
	public void updateLifecycleEvent() {
	lifecycleEvent.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart#addFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventFilters.add(filter);
		if (this.lifecycleEvent != null) {
			this.lifecycleEvent.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart#addBusinessFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart#isContainedInLifecycleEventTable(EObject element)
	 * 
	 */
	public boolean isContainedInLifecycleEventTable(EObject element) {
		return ((ReferencesTableSettings)lifecycleEvent.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart#initLifecycleEventTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initLifecycleEventTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		lifecycleEventTable.setContentProvider(contentProvider);
		lifecycleEventTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactEvents.Properties.lifecycleEventTable);
		if (eefElementEditorReadOnlyState && lifecycleEventTable.getTable().isEnabled()) {
			lifecycleEventTable.getTable().setEnabled(false);
			lifecycleEventTable.getTable().setToolTipText(EvidenceMessages.ArtefactEvents_ReadOnly);
			addLifecycleEventTable.setEnabled(false);
			addLifecycleEventTable.setToolTipText(EvidenceMessages.ArtefactEvents_ReadOnly);
			removeLifecycleEventTable.setEnabled(false);
			removeLifecycleEventTable.setToolTipText(EvidenceMessages.ArtefactEvents_ReadOnly);
			editLifecycleEventTable.setEnabled(false);
			editLifecycleEventTable.setToolTipText(EvidenceMessages.ArtefactEvents_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !lifecycleEventTable.getTable().isEnabled()) {
			lifecycleEventTable.getTable().setEnabled(true);
			addLifecycleEventTable.setEnabled(true);
			removeLifecycleEventTable.setEnabled(true);
			editLifecycleEventTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart#updateLifecycleEventTable()
	 * 
	 */
	public void updateLifecycleEventTable() {
	lifecycleEventTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart#addFilterLifecycleEventTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToLifecycleEventTable(ViewerFilter filter) {
		lifecycleEventTableFilters.add(filter);
		if (this.lifecycleEventTable != null) {
			this.lifecycleEventTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart#addBusinessFilterLifecycleEventTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEventTable(ViewerFilter filter) {
		lifecycleEventTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart#isContainedInLifecycleEventTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInLifecycleEventTableTable(EObject element) {
		return ((ReferencesTableSettings)lifecycleEventTable.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.ArtefactEvents_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
