/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.providers;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.providers.impl.PropertiesEditingProviderImpl;

import org.eclipse.jface.viewers.IFilter;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;

import org.eclipse.opencert.evm.evidspec.evidence.components.ArtefactArtefactEvaluationPropertiesEditionComponent;
import org.eclipse.opencert.evm.evidspec.evidence.components.ArtefactArtefactEventsPropertiesEditionComponent;
import org.eclipse.opencert.evm.evidspec.evidence.components.ArtefactArtefactPropertyValuePropertiesEditionComponent;
import org.eclipse.opencert.evm.evidspec.evidence.components.ArtefactArtefactVersionPropertiesEditionComponent;
import org.eclipse.opencert.evm.evidspec.evidence.components.ArtefactBasePropertiesEditionComponent;
import org.eclipse.opencert.evm.evidspec.evidence.components.ArtefactPropertiesEditionComponent;

/**
 * 
 * 
 */
public class ArtefactPropertiesEditionProvider extends PropertiesEditingProviderImpl {

	/**
	 * Constructor without provider for super types.
	 */
	public ArtefactPropertiesEditionProvider() {
		super();
	}

	/**
	 * Constructor with providers for super types.
	 * @param superProviders providers to use for super types.
	 */
	public ArtefactPropertiesEditionProvider(List<PropertiesEditingProvider> superProviders) {
		super(superProviders);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext) {
		return (editingContext.getEObject() instanceof Artefact) 
					&& (EvidencePackage.Literals.ARTEFACT == editingContext.getEObject().eClass());
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext, String part) {
		return (editingContext.getEObject() instanceof Artefact) && (ArtefactBasePropertiesEditionComponent.BASE_PART.equals(part) || ArtefactArtefactVersionPropertiesEditionComponent.ARTEFACTVERSION_PART.equals(part) || ArtefactArtefactPropertyValuePropertiesEditionComponent.ARTEFACTPROPERTYVALUE_PART.equals(part) || ArtefactArtefactEvaluationPropertiesEditionComponent.ARTEFACTEVALUATION_PART.equals(part) || ArtefactArtefactEventsPropertiesEditionComponent.ARTEFACTEVENTS_PART.equals(part));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof Artefact) && (refinement == ArtefactBasePropertiesEditionComponent.class || refinement == ArtefactArtefactVersionPropertiesEditionComponent.class || refinement == ArtefactArtefactPropertyValuePropertiesEditionComponent.class || refinement == ArtefactArtefactEvaluationPropertiesEditionComponent.class || refinement == ArtefactArtefactEventsPropertiesEditionComponent.class);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, String part, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof Artefact) && ((ArtefactBasePropertiesEditionComponent.BASE_PART.equals(part) && refinement == ArtefactBasePropertiesEditionComponent.class) || (ArtefactArtefactVersionPropertiesEditionComponent.ARTEFACTVERSION_PART.equals(part) && refinement == ArtefactArtefactVersionPropertiesEditionComponent.class) || (ArtefactArtefactPropertyValuePropertiesEditionComponent.ARTEFACTPROPERTYVALUE_PART.equals(part) && refinement == ArtefactArtefactPropertyValuePropertiesEditionComponent.class) || (ArtefactArtefactEvaluationPropertiesEditionComponent.ARTEFACTEVALUATION_PART.equals(part) && refinement == ArtefactArtefactEvaluationPropertiesEditionComponent.class) || (ArtefactArtefactEventsPropertiesEditionComponent.ARTEFACTEVENTS_PART.equals(part) && refinement == ArtefactArtefactEventsPropertiesEditionComponent.class));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode) {
		if (editingContext.getEObject() instanceof Artefact) {
			return new ArtefactPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part) {
		if (editingContext.getEObject() instanceof Artefact) {
			if (ArtefactBasePropertiesEditionComponent.BASE_PART.equals(part))
				return new ArtefactBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ArtefactArtefactVersionPropertiesEditionComponent.ARTEFACTVERSION_PART.equals(part))
				return new ArtefactArtefactVersionPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ArtefactArtefactPropertyValuePropertiesEditionComponent.ARTEFACTPROPERTYVALUE_PART.equals(part))
				return new ArtefactArtefactPropertyValuePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ArtefactArtefactEvaluationPropertiesEditionComponent.ARTEFACTEVALUATION_PART.equals(part))
				return new ArtefactArtefactEvaluationPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ArtefactArtefactEventsPropertiesEditionComponent.ARTEFACTEVENTS_PART.equals(part))
				return new ArtefactArtefactEventsPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String, java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part, java.lang.Class refinement) {
		if (editingContext.getEObject() instanceof Artefact) {
			if (ArtefactBasePropertiesEditionComponent.BASE_PART.equals(part)
				&& refinement == ArtefactBasePropertiesEditionComponent.class)
				return new ArtefactBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ArtefactArtefactVersionPropertiesEditionComponent.ARTEFACTVERSION_PART.equals(part)
				&& refinement == ArtefactArtefactVersionPropertiesEditionComponent.class)
				return new ArtefactArtefactVersionPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ArtefactArtefactPropertyValuePropertiesEditionComponent.ARTEFACTPROPERTYVALUE_PART.equals(part)
				&& refinement == ArtefactArtefactPropertyValuePropertiesEditionComponent.class)
				return new ArtefactArtefactPropertyValuePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ArtefactArtefactEvaluationPropertiesEditionComponent.ARTEFACTEVALUATION_PART.equals(part)
				&& refinement == ArtefactArtefactEvaluationPropertiesEditionComponent.class)
				return new ArtefactArtefactEvaluationPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (ArtefactArtefactEventsPropertiesEditionComponent.ARTEFACTEVENTS_PART.equals(part)
				&& refinement == ArtefactArtefactEventsPropertiesEditionComponent.class)
				return new ArtefactArtefactEventsPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part, refinement);
	}

	/**
	 * Provides the filter used by the plugin.xml to assign part forms.
	 */
	public static class EditionFilter implements IFilter {
		
		/**
		 * {@inheritDoc}
		 * 
		 * @see org.eclipse.jface.viewers.IFilter#select(java.lang.Object)
		 */
		public boolean select(Object toTest) {
			EObject eObj = EEFUtils.resolveSemanticObject(toTest);
			return eObj != null && EvidencePackage.Literals.ARTEFACT == eObj.eClass();
		}
		
	}

}
