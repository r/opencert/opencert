/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.export.docx.ComplianceEstimationReportDocxGenerator;
import org.eclipse.opencert.webapp.reports.listeners.ComplianceDetailsStatusListener;
import org.eclipse.opencert.webapp.reports.listeners.UnassignOrAssignArtefactListener;
import org.eclipse.opencert.webapp.reports.view.common.AbstractBaselineFrameworkReport;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.export.ExportToDocxPanel;
import org.eclipse.opencert.webapp.reports.view.url.URLParamsContainer;

import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.VerticalSplitPanel;


public abstract class AbstractComplianceReport
        extends AbstractBaselineFrameworkReport
        implements UnassignOrAssignArtefactListener
{
    private ComplianceDetailsStatusPanel _complianceDetailsStatusPanel;

    private final ExportToDocxPanel _exportTopDocxPanel;

    private boolean isShowExternalToolsPanels;
    private URLParamsContainer urlParamsContainer;

    private GUIMode guiMode;

    protected AbstractComplianceReport(boolean showExternalToolsPanels, GUIMode guiMode, URLParamsContainer urlParamsContainer)
    {
        isShowExternalToolsPanels = showExternalToolsPanels;
        this.guiMode = guiMode;
        this.urlParamsContainer = urlParamsContainer;
        
        final String basePath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
        final ComplianceEstimationReportDocxGenerator generator = new ComplianceEstimationReportDocxGenerator(basePath);
        _exportTopDocxPanel = new ExportToDocxPanel(generator);
        addBaselineChangeListener(_exportTopDocxPanel);
    }

    @Override
    public void projectChanged(AssuranceProjectWrapper newProject)
    {
        super.projectChanged(newProject);
        
        if (newProject == null) {
            return;
        }

        _complianceDetailsStatusPanel.setNewProject(newProject);
        _exportTopDocxPanel.setNewProject(newProject);
    }

    @Override
    public Component getMainComponent(GUIMode gm)
    {
        BaselineItemDetailsPanel baselineItemDetailsPanel = new BaselineItemDetailsPanel(isShowExternalToolsPanels);
        baselineItemDetailsPanel.addExternalToolResultOrQueryAddedListener((cdoItemId) -> super.refreshBaselineTableBasedOnFrameworkCombo());
        
        ProjectBaselineComplianceStatusTable projectBaselineComplianceStatusTable = new ProjectBaselineComplianceStatusTable(isShowExternalToolsPanels, gm, urlParamsContainer);
        addBaselineChangeListener(projectBaselineComplianceStatusTable);
        
        baselineItemDetailsPanel.addExternalToolResultOrQueryAddedListener(projectBaselineComplianceStatusTable);

        projectBaselineComplianceStatusTable.addProjectBaselineComplianceTableListener(baselineItemDetailsPanel);

        _complianceDetailsStatusPanel = new ComplianceDetailsStatusPanel(this);
        _complianceDetailsStatusPanel.getDragAndDropComplianceEvidencePanel().addUnassignOrAssignArtefactListener(projectBaselineComplianceStatusTable);
        
        projectBaselineComplianceStatusTable.addProjectBaselineComplianceTableListener(_complianceDetailsStatusPanel);

        VerticalSplitPanel leftSplitPanel = new VerticalSplitPanel();
        leftSplitPanel.setHeight(100, Unit.PERCENTAGE);
        leftSplitPanel.setFirstComponent(projectBaselineComplianceStatusTable);
        leftSplitPanel.getFirstComponent().setStyleName("splitPanelContentAutoScroll");

        leftSplitPanel.setSecondComponent(baselineItemDetailsPanel);

        VerticalSplitPanel rightSplitPanel = new VerticalSplitPanel();
        rightSplitPanel.setStyleName("complianceEstimationReportRightPanel");
        rightSplitPanel.setMinSplitPosition(67, Unit.PIXELS);
        rightSplitPanel.setWidth(100, Unit.PERCENTAGE);
        rightSplitPanel.setHeight(100, Unit.PERCENTAGE);

        VerticalLayout complianceDetailsAndDragDropPanelLayout = new VerticalLayout();
        complianceDetailsAndDragDropPanelLayout.setStyleName("complianceDetailsAndDragDropPanelLayout");
        complianceDetailsAndDragDropPanelLayout.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.Unit.PIXELS);
        complianceDetailsAndDragDropPanelLayout.addComponent(_complianceDetailsStatusPanel);

        Panel complianceItemDetailsPanel = new ComplianceItemDetailsPanel();
        _complianceDetailsStatusPanel.setComplianceDetailsStatusListener((ComplianceDetailsStatusListener) complianceItemDetailsPanel);
        _complianceDetailsStatusPanel.setUnassignOrAssignArtefactListener(projectBaselineComplianceStatusTable);

        rightSplitPanel.setFirstComponent(complianceDetailsAndDragDropPanelLayout);
        rightSplitPanel.setSecondComponent(complianceItemDetailsPanel);

        HorizontalSplitPanel mainSplitPanel = new HorizontalSplitPanel();
        mainSplitPanel.setHeight("100%");
        mainSplitPanel.setFirstComponent(leftSplitPanel);
        mainSplitPanel.setSecondComponent(rightSplitPanel);

        if (guiMode == GUIMode.NORMAL) {
            leftSplitPanel.setSplitPosition(75);
            rightSplitPanel.setSplitPosition(75);
            mainSplitPanel.setSplitPosition(45);
        } else if (guiMode == GUIMode.COMPACT) {
            leftSplitPanel.setSplitPosition(100);
            rightSplitPanel.setSplitPosition(100);
            mainSplitPanel.setSplitPosition(100);
        }
        
        return mainSplitPanel;
    }

    @Override
    public Component getExportToDocxComponent()
    {
        HorizontalLayout panel = new HorizontalLayout();
        panel.addComponent(_exportTopDocxPanel.getButton());
        
        return panel;
    }

    @Override
    public void artefactAssignedOrUnassigned(long cdoItemId)
    {
        super.refreshBaselineTableBasedOnFrameworkCombo();

    }
}