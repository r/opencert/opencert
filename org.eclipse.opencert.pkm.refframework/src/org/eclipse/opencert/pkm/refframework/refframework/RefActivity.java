/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ref Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getObjective <em>Objective</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getScope <em>Scope</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getRequiredArtefact <em>Required Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getProducedArtefact <em>Produced Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getSubActivity <em>Sub Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getPrecedingActivity <em>Preceding Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getOwnedRequirement <em>Owned Requirement</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getRole <em>Role</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getApplicableTechnique <em>Applicable Technique</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getApplicability <em>Applicability</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity()
 * @model annotation="gmf.node label='name' border.color='0,0,0' border.width='2'"
 * @generated
 */
public interface RefActivity extends DescribableElement, RefAssurableElement {
	/**
	 * Returns the value of the '<em><b>Objective</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Objective</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Objective</em>' attribute.
	 * @see #setObjective(String)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_Objective()
	 * @model
	 * @generated
	 */
	String getObjective();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getObjective <em>Objective</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Objective</em>' attribute.
	 * @see #getObjective()
	 * @generated
	 */
	void setObjective(String value);

	/**
	 * Returns the value of the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope</em>' attribute.
	 * @see #setScope(String)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_Scope()
	 * @model
	 * @generated
	 */
	String getScope();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getScope <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scope</em>' attribute.
	 * @see #getScope()
	 * @generated
	 */
	void setScope(String value);

	/**
	 * Returns the value of the '<em><b>Required Artefact</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Artefact</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Artefact</em>' reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_RequiredArtefact()
	 * @model annotation="gmf.link color='255,0,0' source.decoration='closedarrow' style='dash' tool.small.bundle='org.eclipse.opencert.pkm.refframework' tool.small.path='icons/Require.gif'"
	 * @generated
	 */
	EList<RefArtefact> getRequiredArtefact();

	/**
	 * Returns the value of the '<em><b>Produced Artefact</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Produced Artefact</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Produced Artefact</em>' reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_ProducedArtefact()
	 * @model annotation="gmf.link color='0,255,0' target.decoration='filledclosedarrow' style='solid' tool.small.bundle='org.eclipse.opencert.pkm.refframework' tool.small.path='icons/Produce.gif'"
	 * @generated
	 */
	EList<RefArtefact> getProducedArtefact();

	/**
	 * Returns the value of the '<em><b>Sub Activity</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Activity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Activity</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_SubActivity()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefActivity> getSubActivity();

	/**
	 * Returns the value of the '<em><b>Preceding Activity</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preceding Activity</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preceding Activity</em>' reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_PrecedingActivity()
	 * @model annotation="gmf.link source.decoration='arrow' tool.small.bundle='org.eclipse.opencert.pkm.refframework' tool.small.path='icons/Precedence.gif'"
	 * @generated
	 */
	EList<RefActivity> getPrecedingActivity();

	/**
	 * Returns the value of the '<em><b>Owned Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Requirement</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_OwnedRequirement()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefRequirement> getOwnedRequirement();

	/**
	 * Returns the value of the '<em><b>Role</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefRole}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_Role()
	 * @model annotation="gmf.link color='0,0,255' source.decoration='closedarrow' style='dash' tool.small.bundle='org.eclipse.opencert.pkm.refframework' tool.small.path='icons/Executing.gif'"
	 * @generated
	 */
	EList<RefRole> getRole();

	/**
	 * Returns the value of the '<em><b>Applicable Technique</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applicable Technique</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applicable Technique</em>' reference.
	 * @see #setApplicableTechnique(RefTechnique)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_ApplicableTechnique()
	 * @model
	 * @generated
	 */
	RefTechnique getApplicableTechnique();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getApplicableTechnique <em>Applicable Technique</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Applicable Technique</em>' reference.
	 * @see #getApplicableTechnique()
	 * @generated
	 */
	void setApplicableTechnique(RefTechnique value);

	/**
	 * Returns the value of the '<em><b>Owned Rel</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Rel</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Rel</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_OwnedRel()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefActivityRel> getOwnedRel();

	/**
	 * Returns the value of the '<em><b>Applicability</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applicability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applicability</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivity_Applicability()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefApplicability> getApplicability();

} // RefActivity
