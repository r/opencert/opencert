/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset;
import org.eclipse.opencert.infra.general.general.impl.NamedElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assurance Asset Evaluation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEvaluationImpl#getEvaluation <em>Evaluation</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEvaluationImpl#getLifecycleEvent <em>Lifecycle Event</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEvaluationImpl#getCriterion <em>Criterion</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEvaluationImpl#getCriterionDescription <em>Criterion Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEvaluationImpl#getEvaluationResult <em>Evaluation Result</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEvaluationImpl#getRationale <em>Rationale</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssuranceAssetEvaluationImpl extends NamedElementImpl implements AssuranceAssetEvaluation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssuranceAssetEvaluationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVALUATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssuranceAssetEvaluation> getEvaluation() {
		return (EList<AssuranceAssetEvaluation>)eGet(AssuranceassetPackage.Literals.MANAGEABLE_ASSURANCE_ASSET__EVALUATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssuranceAssetEvent> getLifecycleEvent() {
		return (EList<AssuranceAssetEvent>)eGet(AssuranceassetPackage.Literals.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCriterion() {
		return (String)eGet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVALUATION__CRITERION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCriterion(String newCriterion) {
		eSet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVALUATION__CRITERION, newCriterion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCriterionDescription() {
		return (String)eGet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVALUATION__CRITERION_DESCRIPTION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCriterionDescription(String newCriterionDescription) {
		eSet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVALUATION__CRITERION_DESCRIPTION, newCriterionDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEvaluationResult() {
		return (String)eGet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVALUATION__EVALUATION_RESULT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvaluationResult(String newEvaluationResult) {
		eSet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVALUATION__EVALUATION_RESULT, newEvaluationResult);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRationale() {
		return (String)eGet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVALUATION__RATIONALE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRationale(String newRationale) {
		eSet(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVALUATION__RATIONALE, newRationale);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AssuranceAsset.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ManageableAssuranceAsset.class) {
			switch (derivedFeatureID) {
				case AssuranceassetPackage.ASSURANCE_ASSET_EVALUATION__EVALUATION: return AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__EVALUATION;
				case AssuranceassetPackage.ASSURANCE_ASSET_EVALUATION__LIFECYCLE_EVENT: return AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AssuranceAsset.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ManageableAssuranceAsset.class) {
			switch (baseFeatureID) {
				case AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__EVALUATION: return AssuranceassetPackage.ASSURANCE_ASSET_EVALUATION__EVALUATION;
				case AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT: return AssuranceassetPackage.ASSURANCE_ASSET_EVALUATION__LIFECYCLE_EVENT;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //AssuranceAssetEvaluationImpl
