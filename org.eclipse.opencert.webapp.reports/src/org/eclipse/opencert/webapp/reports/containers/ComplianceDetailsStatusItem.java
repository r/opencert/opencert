/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

public class ComplianceDetailsStatusItem {
	private long itemId;
	private HorizontalLayout name;
	private Label type;
	private Button unassign;
	private String level;
	private ComplianceDetails complianceDetails;

	public ComplianceDetailsStatusItem(long itemId, HorizontalLayout name, Label type, Button unassign, String level, ComplianceDetails complianceDetails) {
		this.itemId = itemId;
		this.name = name;
		this.type = type;
		this.unassign = unassign;
		this.level = level;
		this.complianceDetails = complianceDetails;
	}
    
	public long getItemId()
    {
        return itemId;
    }

    public void setItemId(long itemId)
    {
        this.itemId = itemId;
    }
	
	public HorizontalLayout getName() {
		return name;
	}
	
	public void setName(HorizontalLayout name) {
		this.name = name;
	}
	
	public Label getType() {
		return type;
	}
	
	public void setType(Label type) {
		this.type = type;
	}
	
	public Button getUnassign() {
		return unassign;
	}
	
	public void setUnassign(Button unassign) {
		this.unassign = unassign;
	}
	
	public String getLevel() {
		return level;
	}
	
	public void setLevel(String level) {
		this.level = level;
	}
	
	public ComplianceDetails getComplianceDetails() {
		return complianceDetails;
	}
	
	public void setComplianceDetails(ComplianceDetails complianceDetails) {
		this.complianceDetails = complianceDetails;
	}
}
