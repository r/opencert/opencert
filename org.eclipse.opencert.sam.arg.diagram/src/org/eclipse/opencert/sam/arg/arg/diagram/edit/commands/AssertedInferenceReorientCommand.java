/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.AssertedInference;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.ArgBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class AssertedInferenceReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public AssertedInferenceReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof AssertedInference) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof ArgumentationElement && newEnd instanceof ArgumentationElement)) {
			return false;
		}
		if (getLink().getTarget().size() != 1) {
			return false;
		}
		ArgumentationElement target = (ArgumentationElement) getLink()
				.getTarget().get(0);
		if (!(getLink().eContainer() instanceof Case)) {
			return false;
		}
		Case container = (Case) getLink().eContainer();
		return ArgBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistAssertedInference_4001(container, getLink(),
						getNewSource(), target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof ArgumentationElement && newEnd instanceof ArgumentationElement)) {
			return false;
		}
		if (getLink().getSource().size() != 1) {
			return false;
		}
		ArgumentationElement source = (ArgumentationElement) getLink()
				.getSource().get(0);
		if (!(getLink().eContainer() instanceof Case)) {
			return false;
		}
		Case container = (Case) getLink().eContainer();
		return ArgBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistAssertedInference_4001(container, getLink(), source,
						getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().getSource().remove(getOldSource());
		getLink().getSource().add(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().getTarget().remove(getOldTarget());
		getLink().getTarget().add(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected AssertedInference getLink() {
		return (AssertedInference) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected ArgumentationElement getOldSource() {
		return (ArgumentationElement) oldEnd;
	}

	/**
	 * @generated
	 */
	protected ArgumentationElement getNewSource() {
		return (ArgumentationElement) newEnd;
	}

	/**
	 * @generated
	 */
	protected ArgumentationElement getOldTarget() {
		return (ArgumentationElement) oldEnd;
	}

	/**
	 * @generated
	 */
	protected ArgumentationElement getNewTarget() {
		return (ArgumentationElement) newEnd;
	}
}
