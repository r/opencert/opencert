/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import java.util.function.Predicate;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.ecore.EAnnotation;

/**
 * Filters objects that should not be indexed.
 * 
 * @author mauersberger
 */
public class ElasticIndexFilter implements Predicate<Object> {

	@Override
	public boolean test(Object t) {
		// do not index map entries
		if (t instanceof BasicEMap.Entry) {
			return false;
		}
		// do not index annotations
		if (t instanceof EAnnotation) {
			return false;
		}

		return true;
	}

}
