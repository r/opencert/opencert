/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ref Artefact Rel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl#getMaxMultiplicitySource <em>Max Multiplicity Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl#getMinMultiplicitySource <em>Min Multiplicity Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl#getMaxMultiplicityTarget <em>Max Multiplicity Target</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl#getMinMultiplicityTarget <em>Min Multiplicity Target</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl#getModificationEffect <em>Modification Effect</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl#getRevocationEffect <em>Revocation Effect</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RefArtefactRelImpl extends DescribableElementImpl implements RefArtefactRel {
	/**
	 * The default value of the '{@link #getMaxMultiplicitySource() <em>Max Multiplicity Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxMultiplicitySource()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_MULTIPLICITY_SOURCE_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getMinMultiplicitySource() <em>Min Multiplicity Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinMultiplicitySource()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_MULTIPLICITY_SOURCE_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getMaxMultiplicityTarget() <em>Max Multiplicity Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxMultiplicityTarget()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_MULTIPLICITY_TARGET_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getMinMultiplicityTarget() <em>Min Multiplicity Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinMultiplicityTarget()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_MULTIPLICITY_TARGET_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getModificationEffect() <em>Modification Effect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModificationEffect()
	 * @generated
	 * @ordered
	 */
	protected static final ChangeEffectKind MODIFICATION_EFFECT_EDEFAULT = ChangeEffectKind.NONE;

	/**
	 * The default value of the '{@link #getRevocationEffect() <em>Revocation Effect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRevocationEffect()
	 * @generated
	 * @ordered
	 */
	protected static final ChangeEffectKind REVOCATION_EFFECT_EDEFAULT = ChangeEffectKind.NONE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefArtefactRelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RefframeworkPackage.Literals.REF_ARTEFACT_REL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxMultiplicitySource() {
		return (Integer)eDynamicGet(RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE, RefframeworkPackage.Literals.REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxMultiplicitySource(int newMaxMultiplicitySource) {
		eDynamicSet(RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE, RefframeworkPackage.Literals.REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE, newMaxMultiplicitySource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinMultiplicitySource() {
		return (Integer)eDynamicGet(RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE, RefframeworkPackage.Literals.REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinMultiplicitySource(int newMinMultiplicitySource) {
		eDynamicSet(RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE, RefframeworkPackage.Literals.REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE, newMinMultiplicitySource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxMultiplicityTarget() {
		return (Integer)eDynamicGet(RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET, RefframeworkPackage.Literals.REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxMultiplicityTarget(int newMaxMultiplicityTarget) {
		eDynamicSet(RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET, RefframeworkPackage.Literals.REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET, newMaxMultiplicityTarget);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinMultiplicityTarget() {
		return (Integer)eDynamicGet(RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET, RefframeworkPackage.Literals.REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinMultiplicityTarget(int newMinMultiplicityTarget) {
		eDynamicSet(RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET, RefframeworkPackage.Literals.REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET, newMinMultiplicityTarget);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEffectKind getModificationEffect() {
		return (ChangeEffectKind)eDynamicGet(RefframeworkPackage.REF_ARTEFACT_REL__MODIFICATION_EFFECT, RefframeworkPackage.Literals.REF_ARTEFACT_REL__MODIFICATION_EFFECT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModificationEffect(ChangeEffectKind newModificationEffect) {
		eDynamicSet(RefframeworkPackage.REF_ARTEFACT_REL__MODIFICATION_EFFECT, RefframeworkPackage.Literals.REF_ARTEFACT_REL__MODIFICATION_EFFECT, newModificationEffect);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEffectKind getRevocationEffect() {
		return (ChangeEffectKind)eDynamicGet(RefframeworkPackage.REF_ARTEFACT_REL__REVOCATION_EFFECT, RefframeworkPackage.Literals.REF_ARTEFACT_REL__REVOCATION_EFFECT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRevocationEffect(ChangeEffectKind newRevocationEffect) {
		eDynamicSet(RefframeworkPackage.REF_ARTEFACT_REL__REVOCATION_EFFECT, RefframeworkPackage.Literals.REF_ARTEFACT_REL__REVOCATION_EFFECT, newRevocationEffect);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefArtefact getSource() {
		return (RefArtefact)eDynamicGet(RefframeworkPackage.REF_ARTEFACT_REL__SOURCE, RefframeworkPackage.Literals.REF_ARTEFACT_REL__SOURCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefArtefact basicGetSource() {
		return (RefArtefact)eDynamicGet(RefframeworkPackage.REF_ARTEFACT_REL__SOURCE, RefframeworkPackage.Literals.REF_ARTEFACT_REL__SOURCE, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(RefArtefact newSource) {
		eDynamicSet(RefframeworkPackage.REF_ARTEFACT_REL__SOURCE, RefframeworkPackage.Literals.REF_ARTEFACT_REL__SOURCE, newSource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefArtefact getTarget() {
		return (RefArtefact)eDynamicGet(RefframeworkPackage.REF_ARTEFACT_REL__TARGET, RefframeworkPackage.Literals.REF_ARTEFACT_REL__TARGET, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefArtefact basicGetTarget() {
		return (RefArtefact)eDynamicGet(RefframeworkPackage.REF_ARTEFACT_REL__TARGET, RefframeworkPackage.Literals.REF_ARTEFACT_REL__TARGET, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(RefArtefact newTarget) {
		eDynamicSet(RefframeworkPackage.REF_ARTEFACT_REL__TARGET, RefframeworkPackage.Literals.REF_ARTEFACT_REL__TARGET, newTarget);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE:
				return getMaxMultiplicitySource();
			case RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE:
				return getMinMultiplicitySource();
			case RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET:
				return getMaxMultiplicityTarget();
			case RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET:
				return getMinMultiplicityTarget();
			case RefframeworkPackage.REF_ARTEFACT_REL__MODIFICATION_EFFECT:
				return getModificationEffect();
			case RefframeworkPackage.REF_ARTEFACT_REL__REVOCATION_EFFECT:
				return getRevocationEffect();
			case RefframeworkPackage.REF_ARTEFACT_REL__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case RefframeworkPackage.REF_ARTEFACT_REL__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE:
				setMaxMultiplicitySource((Integer)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE:
				setMinMultiplicitySource((Integer)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET:
				setMaxMultiplicityTarget((Integer)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET:
				setMinMultiplicityTarget((Integer)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__MODIFICATION_EFFECT:
				setModificationEffect((ChangeEffectKind)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__REVOCATION_EFFECT:
				setRevocationEffect((ChangeEffectKind)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__SOURCE:
				setSource((RefArtefact)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__TARGET:
				setTarget((RefArtefact)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE:
				setMaxMultiplicitySource(MAX_MULTIPLICITY_SOURCE_EDEFAULT);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE:
				setMinMultiplicitySource(MIN_MULTIPLICITY_SOURCE_EDEFAULT);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET:
				setMaxMultiplicityTarget(MAX_MULTIPLICITY_TARGET_EDEFAULT);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET:
				setMinMultiplicityTarget(MIN_MULTIPLICITY_TARGET_EDEFAULT);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__MODIFICATION_EFFECT:
				setModificationEffect(MODIFICATION_EFFECT_EDEFAULT);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__REVOCATION_EFFECT:
				setRevocationEffect(REVOCATION_EFFECT_EDEFAULT);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__SOURCE:
				setSource((RefArtefact)null);
				return;
			case RefframeworkPackage.REF_ARTEFACT_REL__TARGET:
				setTarget((RefArtefact)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE:
				return getMaxMultiplicitySource() != MAX_MULTIPLICITY_SOURCE_EDEFAULT;
			case RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE:
				return getMinMultiplicitySource() != MIN_MULTIPLICITY_SOURCE_EDEFAULT;
			case RefframeworkPackage.REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET:
				return getMaxMultiplicityTarget() != MAX_MULTIPLICITY_TARGET_EDEFAULT;
			case RefframeworkPackage.REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET:
				return getMinMultiplicityTarget() != MIN_MULTIPLICITY_TARGET_EDEFAULT;
			case RefframeworkPackage.REF_ARTEFACT_REL__MODIFICATION_EFFECT:
				return getModificationEffect() != MODIFICATION_EFFECT_EDEFAULT;
			case RefframeworkPackage.REF_ARTEFACT_REL__REVOCATION_EFFECT:
				return getRevocationEffect() != REVOCATION_EFFECT_EDEFAULT;
			case RefframeworkPackage.REF_ARTEFACT_REL__SOURCE:
				return basicGetSource() != null;
			case RefframeworkPackage.REF_ARTEFACT_REL__TARGET:
				return basicGetTarget() != null;
		}
		return super.eIsSet(featureID);
	}

} //RefArtefactRelImpl
