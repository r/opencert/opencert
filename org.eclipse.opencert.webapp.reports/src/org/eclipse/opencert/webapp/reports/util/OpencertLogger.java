/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

import org.apache.log4j.Logger;
import org.eclipse.opencert.webapp.reports.security.SecurityUtil;

public class OpencertLogger 
{
    private final static String MESSAGE_FORMAT = "\tUser: [%s]\t Message: [%s]";
    private final static Logger LOGGER = Logger.getLogger(OpencertLogger.class.getName());
    
    public static void info(String message)
    {
        message = buildMessage(message);
        LOGGER.info(message);
    }

    public static void warn(String message)
    {
        message = buildMessage(message);
        LOGGER.warn(message);
    }

    public static void error(String message)
    {
        message = buildMessage(message);
        LOGGER.error(message);
    }

    public static void error(Exception ex)
    {
        final String message = buildMessage("");
        LOGGER.error(message, ex);
    }

    public static void error(String message, Exception ex)
    {
        message = buildMessage(message);
        LOGGER.error(message, ex);
    }

	private static String buildMessage(String message)
	{
		String loggedUser = SecurityUtil.getLoginName();
		final String result = String.format(MESSAGE_FORMAT, loggedUser, message);
		return result;
	}

}
