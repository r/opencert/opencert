/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.views.properties.tabbed.ISection;
import org.eclipse.opencert.evm.evidspec.evidence.Value;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;

// End of user code

/**
 * 
 * 
 */
public class ArtefactPropertyValuePropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ArtefactPropertyValuePropertiesEditionPart {

	protected ReferencesTable propertyValue;
	protected List<ViewerFilter> propertyValueBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> propertyValueFilters = new ArrayList<ViewerFilter>();
	protected TableViewer propertyValueTable;
	protected List<ViewerFilter> propertyValueTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> propertyValueTableFilters = new ArrayList<ViewerFilter>();
	protected Button addPropertyValueTable;
	protected Button removePropertyValueTable;
	protected Button editPropertyValueTable;



	/**
	 * For {@link ISection} use only.
	 */
	public ArtefactPropertyValuePropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArtefactPropertyValuePropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence artefactPropertyValueStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = artefactPropertyValueStep.addStep(EvidenceViewsRepository.ArtefactPropertyValue.Properties.class);
		// Start IRR
		// propertiesStep.addStep(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue);
		// End IRR
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable);
		
		
		composer = new PartComposer(artefactPropertyValueStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.ArtefactPropertyValue.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue) {
					return createPropertyValueTableComposition(widgetFactory, parent);
				}
				if (key == EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable) {
					return createPropertyValueTableTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(EvidenceMessages.ArtefactPropertyValuePropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createPropertyValueTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.propertyValue = new ReferencesTable(getDescription(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue, EvidenceMessages.ArtefactPropertyValuePropertiesEditionPart_PropertyValueLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertyValuePropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				propertyValue.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertyValuePropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				propertyValue.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertyValuePropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				propertyValue.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertyValuePropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				propertyValue.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.propertyValueFilters) {
			this.propertyValue.addFilter(filter);
		}
		this.propertyValue.setHelpText(propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue, EvidenceViewsRepository.FORM_KIND));
		this.propertyValue.createControls(parent, widgetFactory);
		this.propertyValue.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertyValuePropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData propertyValueData = new GridData(GridData.FILL_HORIZONTAL);
		propertyValueData.horizontalSpan = 3;
		this.propertyValue.setLayoutData(propertyValueData);
		this.propertyValue.setLowerBound(0);
		this.propertyValue.setUpperBound(-1);
		propertyValue.setID(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue);
		propertyValue.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createPropertyValueTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createPropertyValueTableTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tablePropertyValueTable = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tablePropertyValueTable.setHeaderVisible(true);
		GridData gdPropertyValueTable = new GridData();
		gdPropertyValueTable.grabExcessHorizontalSpace = true;
		gdPropertyValueTable.horizontalAlignment = GridData.FILL;
		gdPropertyValueTable.grabExcessVerticalSpace = true;
		gdPropertyValueTable.verticalAlignment = GridData.FILL;
		tablePropertyValueTable.setLayoutData(gdPropertyValueTable);
		tablePropertyValueTable.setLinesVisible(true);

		// Start of user code for columns definition for PropertyValueTable
		//Start IRR
				/*TableColumn name = new TableColumn(tablePropertyValueTable, SWT.NONE);
				name.setWidth(80);
				name.setText("Label"); //$NON-NLS-1$
				 */		
				TableColumn name = new TableColumn(tablePropertyValueTable, SWT.NONE);
				name.setWidth(200);
				name.setText("Property"); //$NON-NLS-1$

				TableColumn name1 = new TableColumn(tablePropertyValueTable, SWT.NONE);
				name1.setWidth(80);
				name1.setText("Value"); //$NON-NLS-1$
				//End IRR
		// End of user code

		propertyValueTable = new TableViewer(tablePropertyValueTable);
		propertyValueTable.setContentProvider(new ArrayContentProvider());
		propertyValueTable.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for PropertyValueTable
						public String getColumnText(Object object, int columnIndex) {
							// Start IRR
							
							/*AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
							if (object instanceof EObject) {
								switch (columnIndex) {
								case 0:
									return labelProvider.getText(object);
								}
							}*/
							
							if (object instanceof EObject) {
								Value valueObject = (Value)object;
								switch (columnIndex) {
								case 0:							
									return valueObject.getName();
								case 1:							
									return valueObject.getValue();
								}
							}
							
							// End IRR
							return ""; //$NON-NLS-1$
						}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		propertyValueTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (propertyValueTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) propertyValueTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertyValuePropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						propertyValueTable.refresh();
					}
				}
			}

		});
		GridData propertyValueTableData = new GridData(GridData.FILL_HORIZONTAL);
		propertyValueTableData.minimumHeight = 120;
		propertyValueTableData.heightHint = 120;
		propertyValueTable.getTable().setLayoutData(propertyValueTableData);
		for (ViewerFilter filter : this.propertyValueTableFilters) {
			propertyValueTable.addFilter(filter);
		}
		EditingUtils.setID(propertyValueTable.getTable(), EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable);
		EditingUtils.setEEFtype(propertyValueTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createPropertyValueTablePanel(widgetFactory, tableContainer);
		// Start of user code for createPropertyValueTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createPropertyValueTablePanel(FormToolkit widgetFactory, Composite container) {
		Composite propertyValueTablePanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout propertyValueTablePanelLayout = new GridLayout();
		propertyValueTablePanelLayout.numColumns = 1;
		propertyValueTablePanel.setLayout(propertyValueTablePanelLayout);
		addPropertyValueTable = widgetFactory.createButton(propertyValueTablePanel, EvidenceMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addPropertyValueTableData = new GridData(GridData.FILL_HORIZONTAL);
		addPropertyValueTable.setLayoutData(addPropertyValueTableData);
		addPropertyValueTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertyValuePropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				propertyValueTable.refresh();
			}
		});
		EditingUtils.setID(addPropertyValueTable, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable);
		EditingUtils.setEEFtype(addPropertyValueTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removePropertyValueTable = widgetFactory.createButton(propertyValueTablePanel, EvidenceMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removePropertyValueTableData = new GridData(GridData.FILL_HORIZONTAL);
		removePropertyValueTable.setLayoutData(removePropertyValueTableData);
		removePropertyValueTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (propertyValueTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) propertyValueTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertyValuePropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						propertyValueTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removePropertyValueTable, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable);
		EditingUtils.setEEFtype(removePropertyValueTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editPropertyValueTable = widgetFactory.createButton(propertyValueTablePanel, EvidenceMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editPropertyValueTableData = new GridData(GridData.FILL_HORIZONTAL);
		editPropertyValueTable.setLayoutData(editPropertyValueTableData);
		editPropertyValueTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (propertyValueTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) propertyValueTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertyValuePropertiesEditionPartForm.this, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						propertyValueTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editPropertyValueTable, EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable);
		EditingUtils.setEEFtype(editPropertyValueTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createPropertyValueTablePanel

		// End of user code
		return propertyValueTablePanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart#initPropertyValue(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initPropertyValue(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		propertyValue.setContentProvider(contentProvider);
		propertyValue.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue);
		if (eefElementEditorReadOnlyState && propertyValue.isEnabled()) {
			propertyValue.setEnabled(false);
			propertyValue.setToolTipText(EvidenceMessages.ArtefactPropertyValue_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !propertyValue.isEnabled()) {
			propertyValue.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart#updatePropertyValue()
	 * 
	 */
	public void updatePropertyValue() {
	propertyValue.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart#addFilterPropertyValue(ViewerFilter filter)
	 * 
	 */
	public void addFilterToPropertyValue(ViewerFilter filter) {
		propertyValueFilters.add(filter);
		if (this.propertyValue != null) {
			this.propertyValue.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart#addBusinessFilterPropertyValue(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToPropertyValue(ViewerFilter filter) {
		propertyValueBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart#isContainedInPropertyValueTable(EObject element)
	 * 
	 */
	public boolean isContainedInPropertyValueTable(EObject element) {
		return ((ReferencesTableSettings)propertyValue.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart#initPropertyValueTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initPropertyValueTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		propertyValueTable.setContentProvider(contentProvider);
		propertyValueTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable);
		if (eefElementEditorReadOnlyState && propertyValueTable.getTable().isEnabled()) {
			propertyValueTable.getTable().setEnabled(false);
			propertyValueTable.getTable().setToolTipText(EvidenceMessages.ArtefactPropertyValue_ReadOnly);
			addPropertyValueTable.setEnabled(false);
			addPropertyValueTable.setToolTipText(EvidenceMessages.ArtefactPropertyValue_ReadOnly);
			removePropertyValueTable.setEnabled(false);
			removePropertyValueTable.setToolTipText(EvidenceMessages.ArtefactPropertyValue_ReadOnly);
			editPropertyValueTable.setEnabled(false);
			editPropertyValueTable.setToolTipText(EvidenceMessages.ArtefactPropertyValue_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !propertyValueTable.getTable().isEnabled()) {
			propertyValueTable.getTable().setEnabled(true);
			addPropertyValueTable.setEnabled(true);
			removePropertyValueTable.setEnabled(true);
			editPropertyValueTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart#updatePropertyValueTable()
	 * 
	 */
	public void updatePropertyValueTable() {
	propertyValueTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart#addFilterPropertyValueTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToPropertyValueTable(ViewerFilter filter) {
		propertyValueTableFilters.add(filter);
		if (this.propertyValueTable != null) {
			this.propertyValueTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart#addBusinessFilterPropertyValueTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToPropertyValueTable(ViewerFilter filter) {
		propertyValueTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart#isContainedInPropertyValueTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInPropertyValueTableTable(EObject element) {
		return ((ReferencesTableSettings)propertyValueTable.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.ArtefactPropertyValue_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
