/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

public class ComplianceNumber
{
    private int fullyCompliant = 0;
    
    private int partiallyCompliant = 0;
    
    public ComplianceNumber() {
        
    }

    public ComplianceNumber(int fullyCompliant, int partiallyCompliant)
    {
        super();
        this.fullyCompliant = fullyCompliant;
        this.partiallyCompliant = partiallyCompliant;
    }

    public int getFullyCompliant()
    {
        return fullyCompliant;
    }

    public void setFullyCompliant(int fullyCompliant)
    {
        this.fullyCompliant = fullyCompliant;
    }

    public int getPartiallyCompliant()
    {
        return partiallyCompliant;
    }

    public void setPartiallyCompliant(int partiallyCompliant)
    {
        this.partiallyCompliant = partiallyCompliant;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + fullyCompliant;
        result = prime * result + partiallyCompliant;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ComplianceNumber))
            return false;
        ComplianceNumber other = (ComplianceNumber) obj;
        if (fullyCompliant != other.fullyCompliant)
            return false;
        if (partiallyCompliant != other.partiallyCompliant)
            return false;
        return true;
    }

    
    
}
