/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.views.properties.tabbed.ISection;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;
import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;

// End of user code

/**
 * 
 * 
 */
public class ActivityEquivalenceMapPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ActivityEquivalenceMapPropertiesEditionPart {

	protected ReferencesTable equivalence;
	protected List<ViewerFilter> equivalenceBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> equivalenceFilters = new ArrayList<ViewerFilter>();
	protected TableViewer activityEquivalenceMap;
	protected List<ViewerFilter> activityEquivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> activityEquivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addActivityEquivalenceMap;
	protected Button removeActivityEquivalenceMap;
	protected Button editActivityEquivalenceMap;



	/**
	 * For {@link ISection} use only.
	 */
	public ActivityEquivalenceMapPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ActivityEquivalenceMapPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence activityEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = activityEquivalenceMapStep.addStep(RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence);
		// End IRR
		propertiesStep.addStep(RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_);
		
		
		composer = new PartComposer(activityEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence) {
					return createEquivalenceTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_) {
					return createActivityEquivalenceMapTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(RefframeworkMessages.ActivityEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEquivalenceTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.equivalence = new ReferencesTable(getDescription(RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence, RefframeworkMessages.ActivityEquivalenceMapPropertiesEditionPart_EquivalenceLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				equivalence.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				equivalence.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				equivalence.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				equivalence.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.equivalenceFilters) {
			this.equivalence.addFilter(filter);
		}
		this.equivalence.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence, RefframeworkViewsRepository.FORM_KIND));
		this.equivalence.createControls(parent, widgetFactory);
		this.equivalence.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData equivalenceData = new GridData(GridData.FILL_HORIZONTAL);
		equivalenceData.horizontalSpan = 3;
		this.equivalence.setLayoutData(equivalenceData);
		this.equivalence.setLowerBound(0);
		this.equivalence.setUpperBound(-1);
		equivalence.setID(RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence);
		equivalence.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEquivalenceTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createActivityEquivalenceMapTableComposition(FormToolkit widgetFactory, final Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableActivityEquivalenceMap = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableActivityEquivalenceMap.setHeaderVisible(true);
		GridData gdActivityEquivalenceMap = new GridData();
		gdActivityEquivalenceMap.grabExcessHorizontalSpace = true;
		gdActivityEquivalenceMap.horizontalAlignment = GridData.FILL;
		gdActivityEquivalenceMap.grabExcessVerticalSpace = true;
		gdActivityEquivalenceMap.verticalAlignment = GridData.FILL;
		tableActivityEquivalenceMap.setLayoutData(gdActivityEquivalenceMap);
		tableActivityEquivalenceMap.setLinesVisible(true);

		// Start of user code for columns definition for ActivityEquivalenceMap

		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableActivityEquivalenceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableActivityEquivalenceMap,
				SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableActivityEquivalenceMap,
				SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableActivityEquivalenceMap,
				SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Activities"); //$NON-NLS-1$
		// End IRR
		// End of user code
		
		activityEquivalenceMap = new TableViewer(tableActivityEquivalenceMap);
		activityEquivalenceMap.setContentProvider(new ArrayContentProvider());
		activityEquivalenceMap.setLabelProvider(new ITableLabelProvider() {
			// Start of user code for label provider definition for
			// ActivityEquivalenceMap
			public String getColumnText(Object object, int columnIndex) {
				// Start IRR
				/*
				 * AdapterFactoryLabelProvider labelProvider = new
				 * AdapterFactoryLabelProvider(adapterFactory); if (object
				 * instanceof EObject) { switch (columnIndex) { case 0: return
				 * labelProvider.getText(object); } }
				 */
				if (object instanceof EObject) {
					RefEquivalenceMap refEquivalenceMap = (RefEquivalenceMap) object;
					
					
					switch (columnIndex) {
					case 0:
						return refEquivalenceMap.getId();
					case 1:
						if (refEquivalenceMap.getMapGroup() == null)
							return "";
						else
							return refEquivalenceMap.getMapGroup().getName();
					case 2:
						if (refEquivalenceMap.getTarget() == null)
							return "";
						else {
							EList<RefAssurableElement> LstElement = refEquivalenceMap
									.getTarget();
							Iterator<RefAssurableElement> iter = LstElement
									.iterator();
							String sElement = "[";
							
							// Start IRR 20141205 
							
							EObject object1 = null;
							while (iter.hasNext()) {
								object1 = iter.next();
								if (object1 instanceof RefActivity) {
									RefActivity refElement = (RefActivity) object1;
									if (!(refEquivalenceMap.cdoResource().toString().equals(refElement.cdoResource().toString()))) {
										sElement = sElement + refElement.getName() + ",";
									}	
								}
							}
							
							// End IRR 20141205
							
							// delete ,
							sElement = sElement.substring(0,
									sElement.length() - 1);
							
							if (!sElement.equals("")) 
								sElement = sElement + "]";
							return sElement;
						}

					}
				}
				// End IRR

				return ""; //$NON-NLS-1$
			}

			public Image getColumnImage(Object element, int columnIndex) {
				return null;
			}

			// End of user code

			public void addListener(ILabelProviderListener listener) {
				
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
				
				
				
			}

		});
		activityEquivalenceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (activityEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) activityEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						activityEquivalenceMap.refresh();
					}
				}
			}

		});
		GridData activityEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		activityEquivalenceMapData.minimumHeight = 120;
		activityEquivalenceMapData.heightHint = 120;
		activityEquivalenceMap.getTable().setLayoutData(activityEquivalenceMapData);
		for (ViewerFilter filter : this.activityEquivalenceMapFilters) {
			activityEquivalenceMap.addFilter(filter);
		}
		EditingUtils.setID(activityEquivalenceMap.getTable(), RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_);
		EditingUtils.setEEFtype(activityEquivalenceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createActivityEquivalenceMapPanel(widgetFactory, tableContainer);
		// Start of user code for createActivityEquivalenceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createActivityEquivalenceMapPanel(FormToolkit widgetFactory, Composite container) {
		Composite activityEquivalenceMapPanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout activityEquivalenceMapPanelLayout = new GridLayout();
		activityEquivalenceMapPanelLayout.numColumns = 1;
		activityEquivalenceMapPanel.setLayout(activityEquivalenceMapPanelLayout);
		addActivityEquivalenceMap = widgetFactory.createButton(activityEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addActivityEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addActivityEquivalenceMap.setLayoutData(addActivityEquivalenceMapData);
		addActivityEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				activityEquivalenceMap.refresh();
			}
		});
		EditingUtils.setID(addActivityEquivalenceMap, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_);
		EditingUtils.setEEFtype(addActivityEquivalenceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeActivityEquivalenceMap = widgetFactory.createButton(activityEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeActivityEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeActivityEquivalenceMap.setLayoutData(removeActivityEquivalenceMapData);
		removeActivityEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (activityEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) activityEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						activityEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeActivityEquivalenceMap, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_);
		EditingUtils.setEEFtype(removeActivityEquivalenceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editActivityEquivalenceMap = widgetFactory.createButton(activityEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editActivityEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editActivityEquivalenceMap.setLayoutData(editActivityEquivalenceMapData);
		editActivityEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (activityEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) activityEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						activityEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editActivityEquivalenceMap, RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_);
		EditingUtils.setEEFtype(editActivityEquivalenceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createActivityEquivalenceMapPanel

		// End of user code
		return activityEquivalenceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart#initEquivalence(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEquivalence(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		equivalence.setContentProvider(contentProvider);
		equivalence.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.equivalence);
		if (eefElementEditorReadOnlyState && equivalence.isEnabled()) {
			equivalence.setEnabled(false);
			equivalence.setToolTipText(RefframeworkMessages.ActivityEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !equivalence.isEnabled()) {
			equivalence.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart#updateEquivalence()
	 * 
	 */
	public void updateEquivalence() {
	equivalence.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart#addFilterEquivalence(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEquivalence(ViewerFilter filter) {
		equivalenceFilters.add(filter);
		if (this.equivalence != null) {
			this.equivalence.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart#addBusinessFilterEquivalence(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEquivalence(ViewerFilter filter) {
		equivalenceBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart#isContainedInEquivalenceTable(EObject element)
	 * 
	 */
	public boolean isContainedInEquivalenceTable(EObject element) {
		return ((ReferencesTableSettings)equivalence.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart#initActivityEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initActivityEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		activityEquivalenceMap.setContentProvider(contentProvider);
		activityEquivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.ActivityEquivalenceMap.Properties.activityEquivalenceMap_);
		if (eefElementEditorReadOnlyState && activityEquivalenceMap.getTable().isEnabled()) {
			activityEquivalenceMap.getTable().setEnabled(false);
			activityEquivalenceMap.getTable().setToolTipText(RefframeworkMessages.ActivityEquivalenceMap_ReadOnly);
			addActivityEquivalenceMap.setEnabled(false);
			addActivityEquivalenceMap.setToolTipText(RefframeworkMessages.ActivityEquivalenceMap_ReadOnly);
			removeActivityEquivalenceMap.setEnabled(false);
			removeActivityEquivalenceMap.setToolTipText(RefframeworkMessages.ActivityEquivalenceMap_ReadOnly);
			editActivityEquivalenceMap.setEnabled(false);
			editActivityEquivalenceMap.setToolTipText(RefframeworkMessages.ActivityEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !activityEquivalenceMap.getTable().isEnabled()) {
			activityEquivalenceMap.getTable().setEnabled(true);
			addActivityEquivalenceMap.setEnabled(true);
			removeActivityEquivalenceMap.setEnabled(true);
			editActivityEquivalenceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart#updateActivityEquivalenceMap()
	 * 
	 */
	public void updateActivityEquivalenceMap() {
	activityEquivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart#addFilterActivityEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToActivityEquivalenceMap(ViewerFilter filter) {
		activityEquivalenceMapFilters.add(filter);
		if (this.activityEquivalenceMap != null) {
			this.activityEquivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart#addBusinessFilterActivityEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToActivityEquivalenceMap(ViewerFilter filter) {
		activityEquivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart#isContainedInActivityEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInActivityEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)activityEquivalenceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.ActivityEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
