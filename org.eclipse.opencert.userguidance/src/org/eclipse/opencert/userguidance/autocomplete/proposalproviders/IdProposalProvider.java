/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.autocomplete.proposalproviders;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.swt.graphics.Image;
import org.eclipse.opencert.userguidance.autocomplete.ContentProposal2;
import org.eclipse.opencert.userguidance.util.ImageUtil;

public class IdProposalProvider extends AbstractProposalProvider {

	private final static String path = "/icons/proposalicons/id.png";

	private static Image icon = null;

	public IdProposalProvider(EObject eobj, int featureID) {
		super(eobj, featureID);

	}

	@Override
	protected List<ContentProposal2> createProposals(String stringToComplete,
			EObject eobj, int featureID) {
		List<ContentProposal2> result = new ArrayList<ContentProposal2>();
		EObject modelRoot = EcoreUtil.getRootContainer(eobj);
		TreeIterator<EObject> iterator = modelRoot.eAllContents();
		while (iterator.hasNext()) {
			EObject item = iterator.next();
			String id = getIdProperty(item);

			if (id == null || id.length() == 0) {
				continue;
			}
			if (id.contains(" ")) {
				id = "\"" + id + "\"";
			}
			String idString = "id:" + id;
			if (idString.toLowerCase().startsWith(
					stringToComplete.toLowerCase())) {
				ContentProposal2 proposal = new ContentProposal2(idString, null,
						getImage());
				result.add(proposal);
			}
		}
		return result;
	}

	private String getIdProperty(EObject item) {
		String result = null;
		for (EAttribute attribute : item.eClass().getEAllAttributes()) {
			if (attribute.getName().equalsIgnoreCase("id")) {
				result = (String) item.eGet((EStructuralFeature) attribute);
				break;
			}
		}

		return result;
	}

	@Override
	protected Image createImage() {
		if (icon == null) {
			icon = ImageUtil.createImage(this, path);
		}

		return icon;
	}
}
