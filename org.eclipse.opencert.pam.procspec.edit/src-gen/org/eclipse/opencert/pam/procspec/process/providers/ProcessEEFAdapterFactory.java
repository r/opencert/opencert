/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.providers;

import org.eclipse.emf.common.notify.Adapter;

import org.eclipse.opencert.pam.procspec.process.util.ProcessAdapterFactory;

/**
 * 
 * 
 */
public class ProcessEEFAdapterFactory extends ProcessAdapterFactory {

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pam.procspec.process.util.ProcessAdapterFactory#createProcessModelAdapter()
	 * 
	 */
	public Adapter createProcessModelAdapter() {
		return new ProcessModelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pam.procspec.process.util.ProcessAdapterFactory#createActivityAdapter()
	 * 
	 */
	public Adapter createActivityAdapter() {
		return new ActivityPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pam.procspec.process.util.ProcessAdapterFactory#createParticipantAdapter()
	 * 
	 */
	public Adapter createParticipantAdapter() {
		return new ParticipantPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pam.procspec.process.util.ProcessAdapterFactory#createTechniqueAdapter()
	 * 
	 */
	public Adapter createTechniqueAdapter() {
		return new TechniquePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pam.procspec.process.util.ProcessAdapterFactory#createPersonAdapter()
	 * 
	 */
	public Adapter createPersonAdapter() {
		return new PersonPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pam.procspec.process.util.ProcessAdapterFactory#createToolAdapter()
	 * 
	 */
	public Adapter createToolAdapter() {
		return new ToolPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pam.procspec.process.util.ProcessAdapterFactory#createOrganizationAdapter()
	 * 
	 */
	public Adapter createOrganizationAdapter() {
		return new OrganizationPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pam.procspec.process.util.ProcessAdapterFactory#createActivityRelAdapter()
	 * 
	 */
	public Adapter createActivityRelAdapter() {
		return new ActivityRelPropertiesEditionProvider();
	}

}
