/*******************************************************************************
 * Copyright (c) 2017 Honeywell, Inc.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Vit Koksa  <Vit.Koksa@honeywell.com>
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.vavmanager;

import java.io.DataOutputStream;
//import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
//import org.apache.http.entity.mime.MultipartEntityBuilder;

public class Sender {
	//Request message;
	
	Sender(){
		
	}
	
	public HttpURLConnection sendMessage(Request msg){
		URL url;
		HttpURLConnection conn;
		try {
			// open HTTP URL connection
			url = new URL(msg.URL);
			conn = (HttpURLConnection) url.openConnection();
			
			// prepare message content
			byte[] postData;
			String rdf;
			if (msg.content != null && !msg.content.isEmpty()) {
				rdf = msg.content;
				//rdf = "--6877c94b-e1f5-4881-b9f0-f2715d3a837d\n" + rdf + "\n--6877c94b-e1f5-4881-b9f0-f2715d3a837d--";
			} else {
				if (msg.type != null && !msg.type.equalsIgnoreCase("monitor")) {
					throw new RuntimeException("Message content is empty.");
				} else {
					rdf = "";
				}
			}
			postData = rdf.getBytes(StandardCharsets.UTF_8);
			int postDataLength = postData.length;
			
			conn.setDoOutput( true );
			conn.setInstanceFollowRedirects( false );
			conn.setRequestMethod(msg.method);
			conn.setRequestProperty("Content", "System.Net.Http.MultipartFormDataContent");
			if (msg.type != null) {
				conn.setRequestProperty("type", msg.type);
				if (msg.type.equalsIgnoreCase("monitor")) {
					conn.setRequestProperty("id", msg.id);
				}
			}
			//conn.setRequestProperty("type", "query");
			//conn.setRequestProperty("cmd", "divine --version");
			conn.setRequestProperty( "Content-Type", "multipart/form-data; boundary=\"6675fade-a79f-4756-b292-f2ca3f5e7530\""); // "Content-Type", "text/x-c"
			//conn.setRequestProperty( "Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); // "Content-Type", "text/x-c"
			conn.setRequestProperty( "charset", "utf-8");
			conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
			//conn.setRequestProperty("Content-Disposition", "form-data; name=\"file\"; filename=\"" + Path.GetFileName(fileName) + "\"");
			//conn.setRequestProperty("Content-Disposition", "form-data; name=\"file\"; filename=\"plan.xml\""
			conn.setUseCaches( false );
			try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
			   wr.write( postData );
			   wr.flush();
			}
			//String connection = conn.getContent().toString();	// toString();
			conn.connect();  //.. will be fired automatically e.g. by conn.getInputStream();
			return conn;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return null;
		
	}

	/* Temporary workaround, before Verification Server will be able to work with normal messages without LTL files. */
	public HttpURLConnection uploadString(String ltl, String urlStr){
		URL url;
		HttpURLConnection conn;
		
		try {
			// open HTTP URL connection
			url = new URL(urlStr);
			//url = new URL("http://138.90.228.243:6080");	// Temporary Rak
			//url = new URL("http://138.90.228.189:6080");	// Temporary Rak
			//url = new URL("http://158.138.138.152:6080");	// Rak
			//url = new URL("http://158.138.139.17:6080");	// Verify
			conn = (HttpURLConnection) url.openConnection();
			
			// prepare message content
			byte[] postData;
			//String ltl = new String(Files.readAllBytes(Paths.get(fileName)));
			ltl = "--6877c94b-e1f5-4881-b9f0-f2715d3a837d\n" + ltl + "\n--6877c94b-e1f5-4881-b9f0-f2715d3a837d--";
			postData = ltl.getBytes(StandardCharsets.UTF_8);
			//postData = msg.content.getBytes(StandardCharsets.UTF_8);
			//int postDataLength = postData.length;
						
			conn.setDoOutput( true );
			conn.setInstanceFollowRedirects( false );
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content", "System.Net.Http.MultipartFormDataContent");
			conn.setRequestProperty("type", "upload");
			//conn.setRequestProperty("cmd", "divine --version");
			conn.setRequestProperty( "Content-Type", "multipart/form-data; charset=utf-8; boundary=\"6877c94b-e1f5-4881-b9f0-f2715d3a837d\""); // "Content-Type", "text/x-c"
			//conn.setRequestProperty( "Content-Type", "text/x-c"); // "Content-Type", "text/x-c"
			//conn.setRequestProperty("Content-Disposition", "form-data; name=\"file\"; filename=\"" + fileName + "\"");
			//conn.setRequestProperty("Content-Disposition", "form-data; name=\"file\"; filename=\"" + "C:\\ZTEMP\\smazat\\aaa.ltl" + "\"");
			//conn.setRequestProperty("Content-Disposition", "form-data; name=\"file\"; filename=\"" + (new File(fileName).getName()) + "\"");
			//conn.setRequestProperty( "charset", "utf-8");
			//conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
			conn.setUseCaches( false );
			try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
			   wr.write( postData );
			   wr.flush();
			}
			conn.connect();  //.. will be fired automatically e.g. by conn.getInputStream();
			return conn;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/* Temporary workaround, before Verification Server will be able to work with normal messages without LTL files. */
	public HttpURLConnection uploadFile(String fileName){
		URL url;
		HttpURLConnection conn;
		
		try {
			// open HTTP URL connection
			url = new URL("http://147.251.51.238:8080");	// pleiada pleiada01.fi.muni.cz
			//url = new URL("http://138.90.228.243:6080");	// Rak
			//url = new URL("http://138.90.228.189:6080");	// Temporary Rak
			//url = new URL("http://158.138.138.152:6080");	// Rak
			//url = new URL("http://158.138.139.17:6080");	// Verify
			conn = (HttpURLConnection) url.openConnection();
			
			// prepare message content
			byte[] postData;
			Path path = Paths.get(fileName);
			String ltl = Base64.getEncoder().encodeToString(Files.readAllBytes(path));	//new String(Files.readAllBytes(path));
			ltl = "--6877c94b-e1f5-4881-b9f0-f2715d3a837d\n" + ltl + "\n--6877c94b-e1f5-4881-b9f0-f2715d3a837d--";
			postData = ltl.getBytes(StandardCharsets.UTF_8);
			//postData = msg.content.getBytes(StandardCharsets.UTF_8);
			//int postDataLength = postData.length;
						
			conn.setDoOutput( true );
			conn.setInstanceFollowRedirects( false );
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content", "System.Net.Http.MultipartFormDataContent");
			conn.setRequestProperty("type", "upload");
			//conn.setRequestProperty("cmd", "divine --version");
			conn.setRequestProperty( "Content-Type", "multipart/form-data; charset=utf-8; boundary=\"6877c94b-e1f5-4881-b9f0-f2715d3a837d\""); // "Content-Type", "text/x-c"
			//conn.setRequestProperty( "Content-Type", "text/x-c"); // "Content-Type", "text/x-c"
			conn.setRequestProperty("Content-Disposition", "form-data; name=\"file\"; filename=\"" + fileName + "\"");
			//conn.setRequestProperty("Content-Disposition", "form-data; name=\"file\"; filename=\"" + "C:\\ZTEMP\\smazat\\aaa.ltl" + "\"");
			//conn.setRequestProperty("Content-Disposition", "form-data; name=\"file\"; filename=\"" + (new File(fileName).getName()) + "\"");
			//conn.setRequestProperty( "charset", "utf-8");
			//conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
			conn.setUseCaches( false );
			try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
			   wr.write( postData );
			   wr.flush();
			}
			conn.connect();  //.. will be fired automatically e.g. by conn.getInputStream();
			return conn;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
}
