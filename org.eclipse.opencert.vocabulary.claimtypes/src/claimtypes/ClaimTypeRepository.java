/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package claimtypes;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Claim Type Repository</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link claimtypes.ClaimTypeRepository#getClaimTypes <em>Claim Types</em>}</li>
 * </ul>
 * </p>
 *
 * @see claimtypes.ClaimtypesPackage#getClaimTypeRepository()
 * @model
 * @generated
 */
public interface ClaimTypeRepository extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Claim Types</b></em>' containment reference list.
	 * The list contents are of type {@link claimtypes.ClaimType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Claim Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Claim Types</em>' containment reference list.
	 * @see claimtypes.ClaimtypesPackage#getClaimTypeRepository_ClaimTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ClaimType> getClaimTypes();

} // ClaimTypeRepository
