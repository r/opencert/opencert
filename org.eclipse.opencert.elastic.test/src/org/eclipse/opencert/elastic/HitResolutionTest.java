/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.elastic.cdo.CDOObjectResolver;
import org.eclipse.opencert.elastic.search.DummyObjectResolver;
import org.eclipse.opencert.elastic.search.EMFObjectResolver;
import org.eclipse.opencert.elastic.search.ElasticFinder;
import org.eclipse.opencert.elastic.search.ElasticFinderImpl;
import org.eclipse.opencert.elastic.search.Hit;
import org.eclipse.opencert.elastic.search.HitResolution;
import org.eclipse.opencert.elastic.search.ObjectResolver;
import org.junit.Assert;
import org.junit.Test;

public class HitResolutionTest {

	@Test
	public void testHitAndResolve() throws Exception {
		testHitAndResolve(ElasticFinderImpl.onDummy(TestData.umlClasses()), new DummyObjectResolver());
		testHitAndResolve(ElasticFinderImpl.onDefaultClient().within("uml"), new EMFObjectResolver());
	}
	
	private void testHitAndResolve(ElasticFinder finder, ObjectResolver<? extends Object> resolver) throws Exception {
		Map<String, Object> filters = new HashMap<>();
		filters.put("abstract", Boolean.FALSE);
		filters.put("interface", Boolean.FALSE);
		Set<Hit> hits = finder.search(".*Component.*", filters);
		Assert.assertNotNull("set expected", hits);
		Assert.assertEquals("2 hits expected", 2, hits.size());

		HitResolution<Object> res = HitResolution.on(TestData.uml()).using(resolver);
		Set<Object> resolved = res.resolve(hits, 2);
		Assert.assertNotNull("set expected", resolved);
		Assert.assertEquals("2 hits expected", 2, resolved.size());
	}
	
	@Test
	public void testCDOAPIFits() throws Exception {
		// just to make sure the CDO extension fits the other API
		HitResolution<EObject> resolution = HitResolution.on(null);
		resolution.using(new CDOObjectResolver());
	}
	
}
