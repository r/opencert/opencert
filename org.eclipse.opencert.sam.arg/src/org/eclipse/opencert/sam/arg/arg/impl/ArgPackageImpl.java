/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.ArgumentReasoning;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension;
import org.eclipse.opencert.sam.arg.arg.AssertedChallenge;
import org.eclipse.opencert.sam.arg.arg.AssertedContext;
import org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence;
import org.eclipse.opencert.sam.arg.arg.AssertedEvidence;
import org.eclipse.opencert.sam.arg.arg.AssertedInference;
import org.eclipse.opencert.sam.arg.arg.AssertedRelationship;
import org.eclipse.opencert.sam.arg.arg.Assertion;
import org.eclipse.opencert.sam.arg.arg.AssuranceCase;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.Choice;
import org.eclipse.opencert.sam.arg.arg.CitationElementType;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.InformationElementType;
import org.eclipse.opencert.sam.arg.arg.ModelElement;
import org.eclipse.opencert.sam.arg.arg.ReasoningElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ArgPackageImpl extends EPackageImpl implements ArgPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assuranceCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass argumentationElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass argumentationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass argumentElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass informationElementCitationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reasoningElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass argumentElementCitationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass argumentReasoningEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass claimEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertedRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass choiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertedInferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertedEvidenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertedContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertedChallengeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertedCounterEvidenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agreementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assertedByMultiplicityExtensionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum informationElementTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum citationElementTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ArgPackageImpl() {
		super(eNS_URI, ArgFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ArgPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ArgPackage init() {
		if (isInited) return (ArgPackage)EPackage.Registry.INSTANCE.getEPackage(ArgPackage.eNS_URI);

		// Obtain or create and register package
		ArgPackageImpl theArgPackage = (ArgPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ArgPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ArgPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EvidencePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theArgPackage.createPackageContents();

		// Initialize created meta-data
		theArgPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theArgPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ArgPackage.eNS_URI, theArgPackage);
		return theArgPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCase() {
		return caseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCase_Argument() {
		return (EReference)caseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCase_Argumentation() {
		return (EReference)caseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCase_Agreement() {
		return (EReference)caseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCase_Cited() {
		return (EReference)caseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCase_Information() {
		return (EReference)caseEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelElement() {
		return modelElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelElement_Id() {
		return (EAttribute)modelElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelElement_Name() {
		return (EAttribute)modelElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssuranceCase() {
		return assuranceCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssuranceCase_HasArgument() {
		return (EReference)assuranceCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssuranceCase_HasEvidence() {
		return (EReference)assuranceCaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssuranceCase_ComposedOf() {
		return (EReference)assuranceCaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArgumentationElement() {
		return argumentationElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArgumentationElement_Description() {
		return (EAttribute)argumentationElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArgumentationElement_Content() {
		return (EAttribute)argumentationElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArgumentation() {
		return argumentationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArgumentation_Location() {
		return (EAttribute)argumentationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArgumentation_Argumentation() {
		return (EReference)argumentationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArgumentation_ConsistOf() {
		return (EReference)argumentationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArgumentElement() {
		return argumentElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInformationElementCitation() {
		return informationElementCitationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInformationElementCitation_Type() {
		return (EAttribute)informationElementCitationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInformationElementCitation_ToBeInstantiated() {
		return (EAttribute)informationElementCitationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInformationElementCitation_Url() {
		return (EAttribute)informationElementCitationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInformationElementCitation_Artefact() {
		return (EReference)informationElementCitationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReasoningElement() {
		return reasoningElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArgumentElementCitation() {
		return argumentElementCitationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArgumentElementCitation_CitedType() {
		return (EAttribute)argumentElementCitationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArgumentElementCitation_ArgumentationReference() {
		return (EAttribute)argumentElementCitationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertion() {
		return assertionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArgumentReasoning() {
		return argumentReasoningEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArgumentReasoning_ToBeSupported() {
		return (EAttribute)argumentReasoningEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArgumentReasoning_ToBeInstantiated() {
		return (EAttribute)argumentReasoningEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArgumentReasoning_HasStructure() {
		return (EReference)argumentReasoningEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClaim() {
		return claimEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClaim_Public() {
		return (EAttribute)claimEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClaim_Assumed() {
		return (EAttribute)claimEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClaim_ToBeSupported() {
		return (EAttribute)claimEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClaim_ToBeInstantiated() {
		return (EAttribute)claimEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClaim_Choice() {
		return (EReference)claimEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertedRelationship() {
		return assertedRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChoice() {
		return choiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChoice_SourceMultiextension() {
		return (EAttribute)choiceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChoice_SourceCardinality() {
		return (EAttribute)choiceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChoice_Optionality() {
		return (EAttribute)choiceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertedInference() {
		return assertedInferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertedInference_Multiextension() {
		return (EAttribute)assertedInferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertedInference_Cardinality() {
		return (EAttribute)assertedInferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertedInference_Source() {
		return (EReference)assertedInferenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertedInference_Target() {
		return (EReference)assertedInferenceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertedEvidence() {
		return assertedEvidenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertedEvidence_Multiextension() {
		return (EAttribute)assertedEvidenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertedEvidence_Cardinality() {
		return (EAttribute)assertedEvidenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertedEvidence_Source() {
		return (EReference)assertedEvidenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertedEvidence_Target() {
		return (EReference)assertedEvidenceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertedContext() {
		return assertedContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertedContext_Multiextension() {
		return (EAttribute)assertedContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertedContext_Cardinality() {
		return (EAttribute)assertedContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertedContext_Source() {
		return (EReference)assertedContextEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertedContext_Target() {
		return (EReference)assertedContextEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertedChallenge() {
		return assertedChallengeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertedChallenge_Source() {
		return (EReference)assertedChallengeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertedChallenge_Target() {
		return (EReference)assertedChallengeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertedCounterEvidence() {
		return assertedCounterEvidenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertedCounterEvidence_Source() {
		return (EReference)assertedCounterEvidenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertedCounterEvidence_Target() {
		return (EReference)assertedCounterEvidenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgreement() {
		return agreementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgreement_Between() {
		return (EReference)agreementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAssertedByMultiplicityExtension() {
		return assertedByMultiplicityExtensionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getInformationElementType() {
		return informationElementTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCitationElementType() {
		return citationElementTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgFactory getArgFactory() {
		return (ArgFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		caseEClass = createEClass(CASE);
		createEReference(caseEClass, CASE__ARGUMENT);
		createEReference(caseEClass, CASE__ARGUMENTATION);
		createEReference(caseEClass, CASE__AGREEMENT);
		createEReference(caseEClass, CASE__CITED);
		createEReference(caseEClass, CASE__INFORMATION);

		modelElementEClass = createEClass(MODEL_ELEMENT);
		createEAttribute(modelElementEClass, MODEL_ELEMENT__ID);
		createEAttribute(modelElementEClass, MODEL_ELEMENT__NAME);

		assuranceCaseEClass = createEClass(ASSURANCE_CASE);
		createEReference(assuranceCaseEClass, ASSURANCE_CASE__HAS_ARGUMENT);
		createEReference(assuranceCaseEClass, ASSURANCE_CASE__HAS_EVIDENCE);
		createEReference(assuranceCaseEClass, ASSURANCE_CASE__COMPOSED_OF);

		argumentationElementEClass = createEClass(ARGUMENTATION_ELEMENT);
		createEAttribute(argumentationElementEClass, ARGUMENTATION_ELEMENT__DESCRIPTION);
		createEAttribute(argumentationElementEClass, ARGUMENTATION_ELEMENT__CONTENT);

		argumentationEClass = createEClass(ARGUMENTATION);
		createEAttribute(argumentationEClass, ARGUMENTATION__LOCATION);
		createEReference(argumentationEClass, ARGUMENTATION__ARGUMENTATION);
		createEReference(argumentationEClass, ARGUMENTATION__CONSIST_OF);

		argumentElementEClass = createEClass(ARGUMENT_ELEMENT);

		informationElementCitationEClass = createEClass(INFORMATION_ELEMENT_CITATION);
		createEAttribute(informationElementCitationEClass, INFORMATION_ELEMENT_CITATION__TYPE);
		createEAttribute(informationElementCitationEClass, INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED);
		createEAttribute(informationElementCitationEClass, INFORMATION_ELEMENT_CITATION__URL);
		createEReference(informationElementCitationEClass, INFORMATION_ELEMENT_CITATION__ARTEFACT);

		reasoningElementEClass = createEClass(REASONING_ELEMENT);

		argumentElementCitationEClass = createEClass(ARGUMENT_ELEMENT_CITATION);
		createEAttribute(argumentElementCitationEClass, ARGUMENT_ELEMENT_CITATION__CITED_TYPE);
		createEAttribute(argumentElementCitationEClass, ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE);

		assertionEClass = createEClass(ASSERTION);

		argumentReasoningEClass = createEClass(ARGUMENT_REASONING);
		createEAttribute(argumentReasoningEClass, ARGUMENT_REASONING__TO_BE_SUPPORTED);
		createEAttribute(argumentReasoningEClass, ARGUMENT_REASONING__TO_BE_INSTANTIATED);
		createEReference(argumentReasoningEClass, ARGUMENT_REASONING__HAS_STRUCTURE);

		claimEClass = createEClass(CLAIM);
		createEAttribute(claimEClass, CLAIM__PUBLIC);
		createEAttribute(claimEClass, CLAIM__ASSUMED);
		createEAttribute(claimEClass, CLAIM__TO_BE_SUPPORTED);
		createEAttribute(claimEClass, CLAIM__TO_BE_INSTANTIATED);
		createEReference(claimEClass, CLAIM__CHOICE);

		assertedRelationshipEClass = createEClass(ASSERTED_RELATIONSHIP);

		choiceEClass = createEClass(CHOICE);
		createEAttribute(choiceEClass, CHOICE__SOURCE_MULTIEXTENSION);
		createEAttribute(choiceEClass, CHOICE__SOURCE_CARDINALITY);
		createEAttribute(choiceEClass, CHOICE__OPTIONALITY);

		assertedInferenceEClass = createEClass(ASSERTED_INFERENCE);
		createEAttribute(assertedInferenceEClass, ASSERTED_INFERENCE__MULTIEXTENSION);
		createEAttribute(assertedInferenceEClass, ASSERTED_INFERENCE__CARDINALITY);
		createEReference(assertedInferenceEClass, ASSERTED_INFERENCE__SOURCE);
		createEReference(assertedInferenceEClass, ASSERTED_INFERENCE__TARGET);

		assertedEvidenceEClass = createEClass(ASSERTED_EVIDENCE);
		createEAttribute(assertedEvidenceEClass, ASSERTED_EVIDENCE__MULTIEXTENSION);
		createEAttribute(assertedEvidenceEClass, ASSERTED_EVIDENCE__CARDINALITY);
		createEReference(assertedEvidenceEClass, ASSERTED_EVIDENCE__SOURCE);
		createEReference(assertedEvidenceEClass, ASSERTED_EVIDENCE__TARGET);

		assertedContextEClass = createEClass(ASSERTED_CONTEXT);
		createEAttribute(assertedContextEClass, ASSERTED_CONTEXT__MULTIEXTENSION);
		createEAttribute(assertedContextEClass, ASSERTED_CONTEXT__CARDINALITY);
		createEReference(assertedContextEClass, ASSERTED_CONTEXT__SOURCE);
		createEReference(assertedContextEClass, ASSERTED_CONTEXT__TARGET);

		assertedChallengeEClass = createEClass(ASSERTED_CHALLENGE);
		createEReference(assertedChallengeEClass, ASSERTED_CHALLENGE__SOURCE);
		createEReference(assertedChallengeEClass, ASSERTED_CHALLENGE__TARGET);

		assertedCounterEvidenceEClass = createEClass(ASSERTED_COUNTER_EVIDENCE);
		createEReference(assertedCounterEvidenceEClass, ASSERTED_COUNTER_EVIDENCE__SOURCE);
		createEReference(assertedCounterEvidenceEClass, ASSERTED_COUNTER_EVIDENCE__TARGET);

		agreementEClass = createEClass(AGREEMENT);
		createEReference(agreementEClass, AGREEMENT__BETWEEN);

		// Create enums
		assertedByMultiplicityExtensionEEnum = createEEnum(ASSERTED_BY_MULTIPLICITY_EXTENSION);
		informationElementTypeEEnum = createEEnum(INFORMATION_ELEMENT_TYPE);
		citationElementTypeEEnum = createEEnum(CITATION_ELEMENT_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EvidencePackage theEvidencePackage = (EvidencePackage)EPackage.Registry.INSTANCE.getEPackage(EvidencePackage.eNS_URI);
		AssuranceassetPackage theAssuranceassetPackage = (AssuranceassetPackage)EPackage.Registry.INSTANCE.getEPackage(AssuranceassetPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		assuranceCaseEClass.getESuperTypes().add(this.getModelElement());
		argumentationElementEClass.getESuperTypes().add(this.getModelElement());
		argumentationEClass.getESuperTypes().add(this.getArgumentationElement());
		argumentationEClass.getESuperTypes().add(theAssuranceassetPackage.getManageableAssuranceAsset());
		argumentElementEClass.getESuperTypes().add(this.getArgumentationElement());
		informationElementCitationEClass.getESuperTypes().add(this.getArgumentElement());
		reasoningElementEClass.getESuperTypes().add(this.getArgumentElement());
		argumentElementCitationEClass.getESuperTypes().add(this.getArgumentElement());
		assertionEClass.getESuperTypes().add(this.getReasoningElement());
		argumentReasoningEClass.getESuperTypes().add(this.getReasoningElement());
		claimEClass.getESuperTypes().add(this.getAssertion());
		claimEClass.getESuperTypes().add(theAssuranceassetPackage.getManageableAssuranceAsset());
		assertedRelationshipEClass.getESuperTypes().add(this.getAssertion());
		choiceEClass.getESuperTypes().add(this.getArgumentElement());
		assertedInferenceEClass.getESuperTypes().add(this.getAssertedRelationship());
		assertedEvidenceEClass.getESuperTypes().add(this.getAssertedRelationship());
		assertedContextEClass.getESuperTypes().add(this.getAssertedRelationship());
		assertedChallengeEClass.getESuperTypes().add(this.getAssertedRelationship());
		assertedCounterEvidenceEClass.getESuperTypes().add(this.getAssertedRelationship());
		agreementEClass.getESuperTypes().add(this.getArgumentation());

		// Initialize classes, features, and operations; add parameters
		initEClass(caseEClass, Case.class, "Case", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCase_Argument(), this.getArgumentElement(), null, "argument", null, 0, -1, Case.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCase_Argumentation(), this.getArgumentation(), null, "argumentation", null, 0, -1, Case.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCase_Agreement(), this.getAgreement(), null, "agreement", null, 0, -1, Case.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCase_Cited(), this.getArgumentElementCitation(), null, "cited", null, 0, -1, Case.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCase_Information(), this.getInformationElementCitation(), null, "information", null, 0, -1, Case.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(modelElementEClass, ModelElement.class, "ModelElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModelElement_Id(), ecorePackage.getEString(), "id", null, 0, 1, ModelElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModelElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, ModelElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assuranceCaseEClass, AssuranceCase.class, "AssuranceCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssuranceCase_HasArgument(), this.getArgumentation(), null, "hasArgument", null, 0, -1, AssuranceCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssuranceCase_HasEvidence(), theEvidencePackage.getArtefact(), null, "hasEvidence", null, 0, -1, AssuranceCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssuranceCase_ComposedOf(), this.getAssuranceCase(), null, "composedOf", null, 0, -1, AssuranceCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(argumentationElementEClass, ArgumentationElement.class, "ArgumentationElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getArgumentationElement_Description(), ecorePackage.getEString(), "description", null, 0, 1, ArgumentationElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArgumentationElement_Content(), ecorePackage.getEString(), "content", null, 0, 1, ArgumentationElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(argumentationEClass, Argumentation.class, "Argumentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getArgumentation_Location(), ecorePackage.getEString(), "location", null, 0, 1, Argumentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArgumentation_Argumentation(), this.getArgumentation(), null, "argumentation", null, 0, -1, Argumentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArgumentation_ConsistOf(), this.getArgumentElement(), null, "consistOf", null, 0, -1, Argumentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(argumentElementEClass, ArgumentElement.class, "ArgumentElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(informationElementCitationEClass, InformationElementCitation.class, "InformationElementCitation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInformationElementCitation_Type(), this.getInformationElementType(), "type", null, 0, 1, InformationElementCitation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInformationElementCitation_ToBeInstantiated(), ecorePackage.getEBooleanObject(), "toBeInstantiated", "false", 1, 1, InformationElementCitation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInformationElementCitation_Url(), ecorePackage.getEString(), "url", null, 0, 1, InformationElementCitation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInformationElementCitation_Artefact(), theEvidencePackage.getArtefact(), null, "artefact", null, 0, -1, InformationElementCitation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(reasoningElementEClass, ReasoningElement.class, "ReasoningElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(argumentElementCitationEClass, ArgumentElementCitation.class, "ArgumentElementCitation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getArgumentElementCitation_CitedType(), this.getCitationElementType(), "citedType", null, 0, 1, ArgumentElementCitation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArgumentElementCitation_ArgumentationReference(), ecorePackage.getEString(), "argumentationReference", null, 0, 1, ArgumentElementCitation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertionEClass, Assertion.class, "Assertion", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(argumentReasoningEClass, ArgumentReasoning.class, "ArgumentReasoning", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getArgumentReasoning_ToBeSupported(), ecorePackage.getEBooleanObject(), "toBeSupported", "false", 1, 1, ArgumentReasoning.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArgumentReasoning_ToBeInstantiated(), ecorePackage.getEBooleanObject(), "toBeInstantiated", "false", 1, 1, ArgumentReasoning.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArgumentReasoning_HasStructure(), this.getArgumentation(), null, "hasStructure", null, 0, -1, ArgumentReasoning.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(claimEClass, Claim.class, "Claim", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClaim_Public(), ecorePackage.getEBooleanObject(), "public", "false", 1, 1, Claim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClaim_Assumed(), ecorePackage.getEBooleanObject(), "assumed", "false", 1, 1, Claim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClaim_ToBeSupported(), ecorePackage.getEBooleanObject(), "toBeSupported", "false", 1, 1, Claim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClaim_ToBeInstantiated(), ecorePackage.getEBooleanObject(), "toBeInstantiated", "false", 1, 1, Claim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClaim_Choice(), this.getChoice(), null, "choice", null, 0, 1, Claim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertedRelationshipEClass, AssertedRelationship.class, "AssertedRelationship", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(choiceEClass, Choice.class, "Choice", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getChoice_SourceMultiextension(), this.getAssertedByMultiplicityExtension(), "sourceMultiextension", null, 0, 1, Choice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChoice_SourceCardinality(), ecorePackage.getEString(), "sourceCardinality", "", 0, 1, Choice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChoice_Optionality(), ecorePackage.getEString(), "optionality", null, 0, 1, Choice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertedInferenceEClass, AssertedInference.class, "AssertedInference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssertedInference_Multiextension(), this.getAssertedByMultiplicityExtension(), "multiextension", null, 0, 1, AssertedInference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssertedInference_Cardinality(), ecorePackage.getEString(), "cardinality", "", 0, 1, AssertedInference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssertedInference_Source(), this.getArgumentationElement(), null, "source", null, 0, -1, AssertedInference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssertedInference_Target(), this.getArgumentationElement(), null, "target", null, 0, -1, AssertedInference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertedEvidenceEClass, AssertedEvidence.class, "AssertedEvidence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssertedEvidence_Multiextension(), this.getAssertedByMultiplicityExtension(), "multiextension", null, 0, 1, AssertedEvidence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssertedEvidence_Cardinality(), ecorePackage.getEString(), "cardinality", "", 0, 1, AssertedEvidence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssertedEvidence_Source(), this.getArgumentElement(), null, "source", null, 0, -1, AssertedEvidence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssertedEvidence_Target(), this.getArgumentElement(), null, "target", null, 0, -1, AssertedEvidence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertedContextEClass, AssertedContext.class, "AssertedContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssertedContext_Multiextension(), this.getAssertedByMultiplicityExtension(), "multiextension", null, 0, 1, AssertedContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssertedContext_Cardinality(), ecorePackage.getEString(), "cardinality", "", 0, 1, AssertedContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssertedContext_Source(), this.getArgumentationElement(), null, "source", null, 0, -1, AssertedContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssertedContext_Target(), this.getArgumentationElement(), null, "target", null, 0, -1, AssertedContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertedChallengeEClass, AssertedChallenge.class, "AssertedChallenge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssertedChallenge_Source(), this.getClaim(), null, "source", null, 0, -1, AssertedChallenge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssertedChallenge_Target(), this.getAssertion(), null, "target", null, 0, -1, AssertedChallenge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertedCounterEvidenceEClass, AssertedCounterEvidence.class, "AssertedCounterEvidence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssertedCounterEvidence_Source(), this.getInformationElementCitation(), null, "source", null, 0, -1, AssertedCounterEvidence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssertedCounterEvidence_Target(), this.getAssertion(), null, "target", null, 0, -1, AssertedCounterEvidence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(agreementEClass, Agreement.class, "Agreement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAgreement_Between(), this.getArgumentation(), null, "between", null, 0, -1, Agreement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(assertedByMultiplicityExtensionEEnum, AssertedByMultiplicityExtension.class, "AssertedByMultiplicityExtension");
		addEEnumLiteral(assertedByMultiplicityExtensionEEnum, AssertedByMultiplicityExtension.NORMAL);
		addEEnumLiteral(assertedByMultiplicityExtensionEEnum, AssertedByMultiplicityExtension.OPTIONAL);
		addEEnumLiteral(assertedByMultiplicityExtensionEEnum, AssertedByMultiplicityExtension.MULTI);

		initEEnum(informationElementTypeEEnum, InformationElementType.class, "InformationElementType");
		addEEnumLiteral(informationElementTypeEEnum, InformationElementType.JUSTIFICATION);
		addEEnumLiteral(informationElementTypeEEnum, InformationElementType.CONTEXT);
		addEEnumLiteral(informationElementTypeEEnum, InformationElementType.SOLUTION);

		initEEnum(citationElementTypeEEnum, CitationElementType.class, "CitationElementType");
		addEEnumLiteral(citationElementTypeEEnum, CitationElementType.CLAIM);
		addEEnumLiteral(citationElementTypeEEnum, CitationElementType.CONTEXT);
		addEEnumLiteral(citationElementTypeEEnum, CitationElementType.SOLUTION);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// gmf.diagram
		createGmfAnnotations();
		// gmf.label
		createGmf_1Annotations();
		// gmf.node
		createGmf_2Annotations();
		// gmf.affixed
		createGmf_3Annotations();
		// gmf.link
		createGmf_4Annotations();
	}

	/**
	 * Initializes the annotations for <b>gmf.diagram</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmfAnnotations() {
		String source = "gmf.diagram";		
		addAnnotation
		  (caseEClass, 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });															
	}

	/**
	 * Initializes the annotations for <b>gmf.label</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_1Annotations() {
		String source = "gmf.label";			
		addAnnotation
		  (getArgumentationElement_Description(), 
		   source, 
		   new String[] {
		   });					
		addAnnotation
		  (getArgumentElementCitation_ArgumentationReference(), 
		   source, 
		   new String[] {
		   });										
	}

	/**
	 * Initializes the annotations for <b>gmf.node</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_2Annotations() {
		String source = "gmf.node";				
		addAnnotation
		  (argumentationEClass, 
		   source, 
		   new String[] {
			 "label", "id",
			 "label.icon", "false",
			 "figure", "gsnfigures.GSNArgumentModule",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.small.path", "GSN_tooling_icons/ArgumentModule.gif",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.path", "GSN_tooling_icons/ArgumentModule.gif",
			 "size", "120,80"
		   });		
		addAnnotation
		  (informationElementCitationEClass, 
		   source, 
		   new String[] {
			 "label", "id",
			 "label.icon", "false",
			 "figure", "gsnfigures.GSNSolution",
			 "tool.small.path", "GSN_tooling_icons/Solution.gif",
			 "tool.large.path", "GSN_tooling_icons/Solution.gif",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg",
			 "size", "100,100"
		   });		
		addAnnotation
		  (argumentElementCitationEClass, 
		   source, 
		   new String[] {
			 "label", "id",
			 "label.icon", "false",
			 "figure", "gsnfigures.GSNArgumentModule",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.small.path", "GSN_tooling_icons/ArgumentModule.gif",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.path", "GSN_tooling_icons/ArgumentModule.gif",
			 "size", "120,80"
		   });			
		addAnnotation
		  (argumentReasoningEClass, 
		   source, 
		   new String[] {
			 "label", "id",
			 "label.icon", "false",
			 "figure", "gsnfigures.GSNStrategy",
			 "tool.small.path", "GSN_tooling_icons/Strategy.gif",
			 "tool.large.path", "GSN_tooling_icons/Strategy.gif",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg",
			 "size", "120,80"
		   });		
		addAnnotation
		  (claimEClass, 
		   source, 
		   new String[] {
			 "label", "id",
			 "label.icon", "false",
			 "figure", "gsnfigures.GSNGoal",
			 "tool.small.path", "GSN_tooling_icons/Goal.gif",
			 "tool.large.path", "GSN_tooling_icons/Goal.gif",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg",
			 "size", "120,80"
		   });			
		addAnnotation
		  (choiceEClass, 
		   source, 
		   new String[] {
			 "label", "optionality",
			 "label.placement", "external",
			 "label.icon", "false",
			 "figure", "org.eclipse.opencert.sam.arg.ui.figures.Choice",
			 "tool.small.path", "GSN_tooling_icons/Optionality.gif",
			 "tool.large.path", "GSN_tooling_icons/Optionality.gif",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg",
			 "size", "40,40"
		   });							
		addAnnotation
		  (agreementEClass, 
		   source, 
		   new String[] {
			 "label", "id",
			 "label.icon", "false",
			 "figure", "gsnfigures.GSNContract",
			 "tool.small.path", "GSN_tooling_icons/Contract.gif",
			 "tool.large.path", "GSN_tooling_icons/Contract.gif",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg",
			 "size", "120,80"
		   });
	}

	/**
	 * Initializes the annotations for <b>gmf.affixed</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_3Annotations() {
		String source = "gmf.affixed";										
		addAnnotation
		  (getClaim_Choice(), 
		   source, 
		   new String[] {
		   });							
	}

	/**
	 * Initializes the annotations for <b>gmf.link</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_4Annotations() {
		String source = "gmf.link";												
		addAnnotation
		  (assertedInferenceEClass, 
		   source, 
		   new String[] {
			 "color", "0,0,0",
			 "source", "source",
			 "target", "target",
			 "target.constraint", "self.oclIsTypeOf(Claim) or self.oclIsTypeOf(Argumentation) or self.oclIsTypeOf(ArgumentReasoning) or self.oclIsTypeOf(Agreement)",
			 "source.constraint", "self.oclIsTypeOf(Choice) or self.oclIsTypeOf(Claim) or self.oclIsTypeOf(ArgumentElementCitation) or self.oclIsTypeOf(Argumentation) or self.oclIsTypeOf(ArgumentReasoning)",
			 "style", "solid",
			 "width", "1",
			 "target.decoration", "filledclosedarrow",
			 "tool.small.path", "GSN_tooling_icons/solvedBy.gif",
			 "tool.large.path", "GSN_tooling_icons/solvedBy.gif",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg",
			 "label", "cardinality"
		   });		
		addAnnotation
		  (assertedEvidenceEClass, 
		   source, 
		   new String[] {
			 "color", "0,0,0",
			 "source", "source",
			 "source.constraint", "self.oclIsTypeOf(Claim)",
			 "target", "target",
			 "target.constraint", "self.oclIsTypeOf(InformationElementCitation)",
			 "style", "solid",
			 "width", "1",
			 "target.decoration", "filledclosedarrow",
			 "tool.small.path", "GSN_tooling_icons/solvedBy.gif",
			 "tool.large.path", "GSN_tooling_icons/solvedBy.gif",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg",
			 "label", "cardinality"
		   });		
		addAnnotation
		  (assertedContextEClass, 
		   source, 
		   new String[] {
			 "color", "0,0,0",
			 "source", "source",
			 "source.constraint", "self.oclIsTypeOf(ArgumentReasoning) or self.oclIsTypeOf(Claim) or self.oclIsTypeOf(Argumentation)",
			 "target", "target",
			 "target.constraint", "self.oclIsTypeOf(ArgumentElementCitation) or self.oclIsTypeOf(InformationElementCitation)",
			 "style", "solid",
			 "width", "1",
			 "target.decoration", "closedarrow",
			 "tool.small.path", "GSN_tooling_icons/inTheContextOf.gif",
			 "tool.large.path", "GSN_tooling_icons/inTheContextOf.gif",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg",
			 "label", "cardinality"
		   });		
		addAnnotation
		  (assertedChallengeEClass, 
		   source, 
		   new String[] {
			 "source", "source",
			 "target", "target",
			 "color", "255,0,0",
			 "style", "solid",
			 "width", "1",
			 "source.constraint", "self.oclIsTypeOf(Claim)",
			 "target.constraint", "self.oclIsTypeOf(Claim) or self.oclIsTypeOf(AssertedInference) or self.oclIsTypeOf(AssertedEvidence) or self.oclIsTypeOf(AssertedContext) or self.oclIsTypeOf(AssertedChallenge) or self.oclIsTypeOf(AssertedCounterEvidence)",
			 "target.decoration", "filledclosedarrow",
			 "tool.small.path", "SACEM_tooling_icons/Challenge.gif",
			 "tool.large.path", "SACEM_tooling_icons/Challenge.gif",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg"
		   });		
		addAnnotation
		  (assertedCounterEvidenceEClass, 
		   source, 
		   new String[] {
			 "source", "source",
			 "target", "target",
			 "color", "255,0,0",
			 "style", "solid",
			 "width", "1",
			 "source.constraint", "self.oclIsTypeOf(InformationElementCitation)",
			 "target.constraint", "self.oclIsTypeOf(Claim) or self.oclIsTypeOf(AssertedInference) or self.oclIsTypeOf(AssertedEvidence) or self.oclIsTypeOf(AssertedContext) or self.oclIsTypeOf(AssertedChallenge) or self.oclIsTypeOf(AssertedCounterEvidence)",
			 "target.decoration", "filledclosedarrow",
			 "tool.small.path", "SACEM_tooling_icons/CounterEvidence.gif",
			 "tool.large.path", "SACEM_tooling_icons/CounterEvidence.gif",
			 "tool.small.bundle", "org.eclipse.opencert.sam.arg",
			 "tool.large.bundle", "org.eclipse.opencert.sam.arg"
		   });	
	}

} //ArgPackageImpl
