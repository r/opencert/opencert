/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.opencert.sam.arg.arg.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ArgFactoryImpl extends EFactoryImpl implements ArgFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ArgFactory init() {
		try {
			ArgFactory theArgFactory = (ArgFactory)EPackage.Registry.INSTANCE.getEFactory(ArgPackage.eNS_URI);
			if (theArgFactory != null) {
				return theArgFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ArgFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ArgPackage.CASE: return (EObject)createCase();
			case ArgPackage.ASSURANCE_CASE: return (EObject)createAssuranceCase();
			case ArgPackage.ARGUMENTATION: return (EObject)createArgumentation();
			case ArgPackage.INFORMATION_ELEMENT_CITATION: return (EObject)createInformationElementCitation();
			case ArgPackage.ARGUMENT_ELEMENT_CITATION: return (EObject)createArgumentElementCitation();
			case ArgPackage.ARGUMENT_REASONING: return (EObject)createArgumentReasoning();
			case ArgPackage.CLAIM: return (EObject)createClaim();
			case ArgPackage.CHOICE: return (EObject)createChoice();
			case ArgPackage.ASSERTED_INFERENCE: return (EObject)createAssertedInference();
			case ArgPackage.ASSERTED_EVIDENCE: return (EObject)createAssertedEvidence();
			case ArgPackage.ASSERTED_CONTEXT: return (EObject)createAssertedContext();
			case ArgPackage.ASSERTED_CHALLENGE: return (EObject)createAssertedChallenge();
			case ArgPackage.ASSERTED_COUNTER_EVIDENCE: return (EObject)createAssertedCounterEvidence();
			case ArgPackage.AGREEMENT: return (EObject)createAgreement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ArgPackage.ASSERTED_BY_MULTIPLICITY_EXTENSION:
				return createAssertedByMultiplicityExtensionFromString(eDataType, initialValue);
			case ArgPackage.INFORMATION_ELEMENT_TYPE:
				return createInformationElementTypeFromString(eDataType, initialValue);
			case ArgPackage.CITATION_ELEMENT_TYPE:
				return createCitationElementTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ArgPackage.ASSERTED_BY_MULTIPLICITY_EXTENSION:
				return convertAssertedByMultiplicityExtensionToString(eDataType, instanceValue);
			case ArgPackage.INFORMATION_ELEMENT_TYPE:
				return convertInformationElementTypeToString(eDataType, instanceValue);
			case ArgPackage.CITATION_ELEMENT_TYPE:
				return convertCitationElementTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Case createCase() {
		CaseImpl case_ = new CaseImpl();
		return case_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssuranceCase createAssuranceCase() {
		AssuranceCaseImpl assuranceCase = new AssuranceCaseImpl();
		return assuranceCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Argumentation createArgumentation() {
		ArgumentationImpl argumentation = new ArgumentationImpl();
		return argumentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InformationElementCitation createInformationElementCitation() {
		InformationElementCitationImpl informationElementCitation = new InformationElementCitationImpl();
		return informationElementCitation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgumentElementCitation createArgumentElementCitation() {
		ArgumentElementCitationImpl argumentElementCitation = new ArgumentElementCitationImpl();
		return argumentElementCitation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgumentReasoning createArgumentReasoning() {
		ArgumentReasoningImpl argumentReasoning = new ArgumentReasoningImpl();
		return argumentReasoning;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Claim createClaim() {
		ClaimImpl claim = new ClaimImpl();
		return claim;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Choice createChoice() {
		ChoiceImpl choice = new ChoiceImpl();
		return choice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertedInference createAssertedInference() {
		AssertedInferenceImpl assertedInference = new AssertedInferenceImpl();
		return assertedInference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertedEvidence createAssertedEvidence() {
		AssertedEvidenceImpl assertedEvidence = new AssertedEvidenceImpl();
		return assertedEvidence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertedContext createAssertedContext() {
		AssertedContextImpl assertedContext = new AssertedContextImpl();
		return assertedContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertedChallenge createAssertedChallenge() {
		AssertedChallengeImpl assertedChallenge = new AssertedChallengeImpl();
		return assertedChallenge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertedCounterEvidence createAssertedCounterEvidence() {
		AssertedCounterEvidenceImpl assertedCounterEvidence = new AssertedCounterEvidenceImpl();
		return assertedCounterEvidence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agreement createAgreement() {
		AgreementImpl agreement = new AgreementImpl();
		return agreement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertedByMultiplicityExtension createAssertedByMultiplicityExtensionFromString(EDataType eDataType, String initialValue) {
		AssertedByMultiplicityExtension result = AssertedByMultiplicityExtension.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssertedByMultiplicityExtensionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InformationElementType createInformationElementTypeFromString(EDataType eDataType, String initialValue) {
		InformationElementType result = InformationElementType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInformationElementTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CitationElementType createCitationElementTypeFromString(EDataType eDataType, String initialValue) {
		CitationElementType result = CitationElementType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCitationElementTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgPackage getArgPackage() {
		return (ArgPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ArgPackage getPackage() {
		return ArgPackage.eINSTANCE;
	}

} //ArgFactoryImpl
