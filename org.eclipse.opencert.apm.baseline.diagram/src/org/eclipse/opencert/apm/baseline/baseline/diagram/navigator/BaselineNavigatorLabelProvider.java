/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.CommonParserHint;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivity2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityName2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityNameEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityPrecedingActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityProducedArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRequiredArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactNameEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseFrameworkEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleNameEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramEditorPlugin;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineParserProvider;

/**
 * @generated
 */
public class BaselineNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		BaselineDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		BaselineDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof BaselineNavigatorItem
				&& !isOwnView(((BaselineNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof BaselineNavigatorGroup) {
			BaselineNavigatorGroup group = (BaselineNavigatorGroup) element;
			return BaselineDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof BaselineNavigatorItem) {
			BaselineNavigatorItem navigatorItem = (BaselineNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getImage(view);
			}
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (BaselineVisualIDRegistry.getVisualID(view)) {
		case BaseFrameworkEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://baseline/1.0?BaseFramework", BaselineElementTypes.BaseFramework_1000); //$NON-NLS-1$
		case BaseActivityEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://baseline/1.0?BaseActivity", BaselineElementTypes.BaseActivity_2001); //$NON-NLS-1$
		case BaseArtefactEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://baseline/1.0?BaseArtefact", BaselineElementTypes.BaseArtefact_2002); //$NON-NLS-1$
		case BaseRoleEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://baseline/1.0?BaseRole", BaselineElementTypes.BaseRole_2003); //$NON-NLS-1$
		case BaseActivity2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://baseline/1.0?BaseActivity", BaselineElementTypes.BaseActivity_3001); //$NON-NLS-1$
		case BaseActivityRequiredArtefactEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://baseline/1.0?BaseActivity?requiredArtefact", BaselineElementTypes.BaseActivityRequiredArtefact_4001); //$NON-NLS-1$
		case BaseActivityProducedArtefactEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://baseline/1.0?BaseActivity?producedArtefact", BaselineElementTypes.BaseActivityProducedArtefact_4002); //$NON-NLS-1$
		case BaseActivityPrecedingActivityEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://baseline/1.0?BaseActivity?precedingActivity", BaselineElementTypes.BaseActivityPrecedingActivity_4003); //$NON-NLS-1$
		case BaseActivityRoleEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://baseline/1.0?BaseActivity?role", BaselineElementTypes.BaseActivityRole_4004); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = BaselineDiagramEditorPlugin.getInstance()
				.getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& BaselineElementTypes.isKnownElementType(elementType)) {
			image = BaselineElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof BaselineNavigatorGroup) {
			BaselineNavigatorGroup group = (BaselineNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof BaselineNavigatorItem) {
			BaselineNavigatorItem navigatorItem = (BaselineNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getText(view);
			}
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (BaselineVisualIDRegistry.getVisualID(view)) {
		case BaseFrameworkEditPart.VISUAL_ID:
			return getBaseFramework_1000Text(view);
		case BaseActivityEditPart.VISUAL_ID:
			return getBaseActivity_2001Text(view);
		case BaseArtefactEditPart.VISUAL_ID:
			return getBaseArtefact_2002Text(view);
		case BaseRoleEditPart.VISUAL_ID:
			return getBaseRole_2003Text(view);
		case BaseActivity2EditPart.VISUAL_ID:
			return getBaseActivity_3001Text(view);
		case BaseActivityRequiredArtefactEditPart.VISUAL_ID:
			return getBaseActivityRequiredArtefact_4001Text(view);
		case BaseActivityProducedArtefactEditPart.VISUAL_ID:
			return getBaseActivityProducedArtefact_4002Text(view);
		case BaseActivityPrecedingActivityEditPart.VISUAL_ID:
			return getBaseActivityPrecedingActivity_4003Text(view);
		case BaseActivityRoleEditPart.VISUAL_ID:
			return getBaseActivityRole_4004Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getBaseFramework_1000Text(View view) {
		BaseFramework domainModelElement = (BaseFramework) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getBaseActivity_2001Text(View view) {
		IParser parser = BaselineParserProvider.getParser(
				BaselineElementTypes.BaseActivity_2001,
				view.getElement() != null ? view.getElement() : view,
				BaselineVisualIDRegistry
						.getType(BaseActivityNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getBaseArtefact_2002Text(View view) {
		IParser parser = BaselineParserProvider.getParser(
				BaselineElementTypes.BaseArtefact_2002,
				view.getElement() != null ? view.getElement() : view,
				BaselineVisualIDRegistry
						.getType(BaseArtefactNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getBaseRole_2003Text(View view) {
		IParser parser = BaselineParserProvider.getParser(
				BaselineElementTypes.BaseRole_2003,
				view.getElement() != null ? view.getElement() : view,
				BaselineVisualIDRegistry
						.getType(BaseRoleNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getBaseActivity_3001Text(View view) {
		IParser parser = BaselineParserProvider.getParser(
				BaselineElementTypes.BaseActivity_3001,
				view.getElement() != null ? view.getElement() : view,
				BaselineVisualIDRegistry
						.getType(BaseActivityName2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getBaseActivityRequiredArtefact_4001Text(View view) {
		IParser parser = BaselineParserProvider.getParser(
				BaselineElementTypes.BaseActivityRequiredArtefact_4001,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getBaseActivityProducedArtefact_4002Text(View view) {
		IParser parser = BaselineParserProvider.getParser(
				BaselineElementTypes.BaseActivityProducedArtefact_4002,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getBaseActivityPrecedingActivity_4003Text(View view) {
		IParser parser = BaselineParserProvider.getParser(
				BaselineElementTypes.BaseActivityPrecedingActivity_4003,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getBaseActivityRole_4004Text(View view) {
		IParser parser = BaselineParserProvider.getParser(
				BaselineElementTypes.BaseActivityRole_4004,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return BaseFrameworkEditPart.MODEL_ID.equals(BaselineVisualIDRegistry
				.getModelID(view));
	}

}
