/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.exttools.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.opencert.externaltools.api.AbstractExternalToolConnector;
import org.eclipse.opencert.externaltools.api.ExternalToolQuery;

public class NotNeededConnector
        extends AbstractExternalToolConnector
{
    @Override
    public String getName()
    {
        return "Not needed";
    }

    @Override
    public String getDescription()
    {
        return "The baseline element has been marked as it does not require any external tool connector.";
    }

    @Override
    public List<String> getSettingKeys()
    {
        return new ArrayList<String>();
    }

    @Override
    public Map<String, String> getSettingDefaultValues()
    {
        return new HashMap<String, String>();
    }

    @Override
    public Integer[] getDefaultResultRanges()
    {
        return new Integer[] { 0, 0, 0, 0 };
    }

    @Override
    public String[] getDefaultResultMsgs()
    {
        return new String[] { "", "", "" };
    }

    @Override
    public int connectAndProcessResponse(ExternalToolQuery externalToolQuery) throws IOException
    {
        return -1;
    }

}
