/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage
 * @generated
 */
public class OpenCertTraceLinkMetaModelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OpenCertTraceLinkMetaModelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpenCertTraceLinkMetaModelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = OpenCertTraceLinkMetaModelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpenCertTraceLinkMetaModelSwitch<Adapter> modelSwitch =
		new OpenCertTraceLinkMetaModelSwitch<Adapter>() {
			@Override
			public Adapter caseContractClaimLink(ContractClaimLink object) {
				return createContractClaimLinkAdapter();
			}
			@Override
			public Adapter caseContractArtefactLink(ContractArtefactLink object) {
				return createContractArtefactLinkAdapter();
			}
			@Override
			public Adapter caseFormalPropertyClaimLink(FormalPropertyClaimLink object) {
				return createFormalPropertyClaimLinkAdapter();
			}
			@Override
			public Adapter caseContractAgreementLink(ContractAgreementLink object) {
				return createContractAgreementLinkAdapter();
			}
			@Override
			public Adapter caseAnalysisContextArtefactLink(AnalysisContextArtefactLink object) {
				return createAnalysisContextArtefactLinkAdapter();
			}
			@Override
			public Adapter caseComponentArgumentationElementLink(ComponentArgumentationElementLink object) {
				return createComponentArgumentationElementLinkAdapter();
			}
			@Override
			public Adapter caseRequirementClaimLink(RequirementClaimLink object) {
				return createRequirementClaimLinkAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink <em>Contract Claim Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink
	 * @generated
	 */
	public Adapter createContractClaimLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractArtefactLink <em>Contract Artefact Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractArtefactLink
	 * @generated
	 */
	public Adapter createContractArtefactLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink <em>Formal Property Claim Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink
	 * @generated
	 */
	public Adapter createFormalPropertyClaimLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink <em>Contract Agreement Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractAgreementLink
	 * @generated
	 */
	public Adapter createContractAgreementLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.AnalysisContextArtefactLink <em>Analysis Context Artefact Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.AnalysisContextArtefactLink
	 * @generated
	 */
	public Adapter createAnalysisContextArtefactLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink <em>Component Argumentation Element Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink
	 * @generated
	 */
	public Adapter createComponentArgumentationElementLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.RequirementClaimLink <em>Requirement Claim Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.RequirementClaimLink
	 * @generated
	 */
	public Adapter createRequirementClaimLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //OpenCertTraceLinkMetaModelAdapterFactory
