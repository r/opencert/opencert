/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.relations;

import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

public class ArtefactsRelationImpactFactory
{
    public static IArtefactRelationImpact createArtefactsRelationImpact(int recursionDepth, ArtefactRel originalArtefactRel,
            EventKind impactingEventKind)
    {
        ChangeEffectKind impactedChangeEffectKind = selectArtefactRelChangeEffectByEventKind(originalArtefactRel.getModificationEffect(), 
                originalArtefactRel.getRevocationEffect(), impactingEventKind);
        
        if (impactedChangeEffectKind == null) {
            return null;
        }
        
        return new ArtefactsRelationImpact(recursionDepth, originalArtefactRel, impactedChangeEffectKind, impactingEventKind);
    }
    
    
    public static IArtefactRelationImpact createArtefactsRelationImpact(int recursionDepth, BaseArtefactRel originalBaseArtefactRel,
            EventKind impactingEventKind)
    {
        ChangeEffectKind impactedChangeEffectKind = selectArtefactRelChangeEffectByEventKind(originalBaseArtefactRel.getModificationEffect(), 
                originalBaseArtefactRel.getRevocationEffect(), impactingEventKind);
        
        if (impactedChangeEffectKind == null) {
            return null;
        }
        
        return new BaseArtefactsRelationImpact(recursionDepth, originalBaseArtefactRel, impactedChangeEffectKind, impactingEventKind);
    }
    
    
    public static IArtefactRelationImpact createEvidenceMappedToBaseArtefactImpact(int recursionDepth, Artefact impactingArtefact,
            BaseArtefact impactedMappedBaseArtefact, EventKind impactingEventKind, IArtefactRelationImpact sourceArtefactRelationImpact)
    {
        ChangeEffectKind impactedChangeEffectKind = null;
        
        //in this case this means that this is modify/revoke on an artefact mapped to this baseartefact, so we should react as in case of
        //first artefact - react only on modify and revoke (user calling "validate" on an artefact he is altering himself has no sense)
        if (sourceArtefactRelationImpact == null) {
            impactedChangeEffectKind = selectArtefactRelChangeEffectByEventKind(
                    ChangeEffectKind.MODIFY, 
                    ChangeEffectKind.REVOKE,
                    impactingEventKind);
        } else {
            //in this case we want to mimic the ArtefactRel that propagated to mapped Artefact. We threat mapped BaseArtefact just like
            //this Artefact, so impact should be exactly the same
            
            if (ArtefactRelationType.DIRECT_EVIDENCE_RELATION.equals(sourceArtefactRelationImpact.getArtefactRelationType())) {
                ArtefactsRelationImpact artefactRelationImpactToMimic = (ArtefactsRelationImpact)sourceArtefactRelationImpact;
                
                impactedChangeEffectKind = selectArtefactRelChangeEffectByEventKind(
                        artefactRelationImpactToMimic.getImpactingArtefactRelModificationEffect(),
                        artefactRelationImpactToMimic.getImpactingArtefactRelRevocationEffect(),
                        impactingEventKind);
            }
            
            if (ArtefactRelationType.BASELINE_TO_EVIDENCE_VIA_MAP_RELATION.equals(sourceArtefactRelationImpact.getArtefactRelationType())) {
                
                BaseArtefactMappedToArtefactRelationImpact baseArtefactMappedToArtefactRelationImpact = 
                        (BaseArtefactMappedToArtefactRelationImpact)sourceArtefactRelationImpact;
                
                //this block is to prevent the turn-around back from Artefact to BaseArtefact that we have just came.
                Long impactedMappedArtefactCDOId = CDOStorageUtil.getCDOId(impactedMappedBaseArtefact);
                Long originallyImpactingBaseArtefactCDOId = baseArtefactMappedToArtefactRelationImpact.getImpactingArtefactCDOId();
                if (impactedMappedArtefactCDOId != null && impactedMappedArtefactCDOId.equals(originallyImpactingBaseArtefactCDOId)) {
                    return null;
                }
                
                
                impactedChangeEffectKind = selectArtefactRelChangeEffectByEventKind(
                        baseArtefactMappedToArtefactRelationImpact.getImpactingBaseArtefactRelModificationEffect(),
                        baseArtefactMappedToArtefactRelationImpact.getImpactingBaseArtefactRelRevocationEffect(),
                        impactingEventKind);
                
            }
        }
        
        
        if (impactedChangeEffectKind == null) {
            return null;
        }
        
        ArtefactToMappedBaseArtefactRelationImpact artefactToMappedBaseArtefactRelationImpact = new ArtefactToMappedBaseArtefactRelationImpact(
                recursionDepth, impactingArtefact, impactedMappedBaseArtefact, impactedChangeEffectKind, impactingEventKind);
       
        return artefactToMappedBaseArtefactRelationImpact;
    }
    
    public static IArtefactRelationImpact createBaseArtefactMappedToEvidenceImpact(
            int recursionDepth, BaseArtefact baseArtefact,
            Artefact mappedArtefact, EventKind impactingEventKind, IArtefactRelationImpact sourceArtefactRelationImpact)
    {
        if (sourceArtefactRelationImpact == null || 
                !ArtefactRelationType.DIRECT_BASELINE_RELATION.equals(sourceArtefactRelationImpact.getArtefactRelationType())) {
            throw new IllegalStateException("Impact analysis from Baseline to Evidence can performed only in a chain in which previous relation was of type"
                    + ArtefactRelationType.DIRECT_BASELINE_RELATION + ". Offending BaseArtefact: " + baseArtefact);
        }
        
        BaseArtefactsRelationImpact baseArtefactsRelationImpact = (BaseArtefactsRelationImpact)sourceArtefactRelationImpact;
        
        ChangeEffectKind impactingBaseArtefactRelModificationEffect = baseArtefactsRelationImpact.getImpactingBaseArtefactRelModificationEffect();
        ChangeEffectKind impactingBaseArtefactRelRevocationEffect = baseArtefactsRelationImpact.getImpactingBaseArtefactRelRevocationEffect();
        
        ChangeEffectKind impactedChangeEffectKind = selectArtefactRelChangeEffectByEventKind(
                impactingBaseArtefactRelModificationEffect,
                impactingBaseArtefactRelRevocationEffect,
                impactingEventKind);
        
        if (impactedChangeEffectKind == null) {
            return null;
        }
        
        return new BaseArtefactMappedToArtefactRelationImpact(recursionDepth, impactingBaseArtefactRelModificationEffect, impactingBaseArtefactRelRevocationEffect, 
                baseArtefact, mappedArtefact, impactedChangeEffectKind, impactingEventKind);
    }
    
    
    private static ChangeEffectKind selectArtefactRelChangeEffectByEventKind(
            ChangeEffectKind modificationChangeEffectKind,
            ChangeEffectKind revocationChangeEffectKind,
            EventKind impactingEventKind)
    {
        if (EventKind.MODIFICATION.equals(impactingEventKind)) {
            return modificationChangeEffectKind;                        
        }
        
        if (EventKind.REVOCATION.equals(impactingEventKind)) {
            return revocationChangeEffectKind;
        }
        
        return null;
    }
}
