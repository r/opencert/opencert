/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.pam.procspec.process.Activity;

// End of user code

/**
 * 
 * 
 */
public class BaseArtefactComplianceMapPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseArtefactComplianceMapPropertiesEditionPart {

	protected ReferencesTable complianceMap;
	protected List<ViewerFilter> complianceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> complianceMapFilters = new ArrayList<ViewerFilter>();
	protected TableViewer artefactComplianceMap;
	protected List<ViewerFilter> artefactComplianceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> artefactComplianceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addArtefactComplianceMap;
	protected Button removeArtefactComplianceMap;
	protected Button editArtefactComplianceMap;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseArtefactComplianceMapPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseArtefactComplianceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseArtefactComplianceMapStep.addStep(BaselineViewsRepository.BaseArtefactComplianceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap);
		
		
		composer = new PartComposer(baseArtefactComplianceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseArtefactComplianceMap.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap) {
					return createComplianceMapAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap) {
					return createArtefactComplianceMapTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseArtefactComplianceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createComplianceMapAdvancedTableComposition(Composite parent) {
		this.complianceMap = new ReferencesTable(getDescription(BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap, BaselineMessages.BaseArtefactComplianceMapPropertiesEditionPart_ComplianceMapLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				complianceMap.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				complianceMap.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				complianceMap.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				complianceMap.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.complianceMapFilters) {
			this.complianceMap.addFilter(filter);
		}
		this.complianceMap.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap, BaselineViewsRepository.SWT_KIND));
		this.complianceMap.createControls(parent);
		this.complianceMap.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData complianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		complianceMapData.horizontalSpan = 3;
		this.complianceMap.setLayoutData(complianceMapData);
		this.complianceMap.setLowerBound(0);
		this.complianceMap.setUpperBound(-1);
		complianceMap.setID(BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap);
		complianceMap.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createComplianceMapAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactComplianceMapTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableArtefactComplianceMap = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableArtefactComplianceMap.setHeaderVisible(true);
		GridData gdArtefactComplianceMap = new GridData();
		gdArtefactComplianceMap.grabExcessHorizontalSpace = true;
		gdArtefactComplianceMap.horizontalAlignment = GridData.FILL;
		gdArtefactComplianceMap.grabExcessVerticalSpace = true;
		gdArtefactComplianceMap.verticalAlignment = GridData.FILL;
		tableArtefactComplianceMap.setLayoutData(gdArtefactComplianceMap);
		tableArtefactComplianceMap.setLinesVisible(true);

		// Start of user code for columns definition for ComplianceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableArtefactComplianceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableArtefactComplianceMap, SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableArtefactComplianceMap,
				SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableArtefactComplianceMap,
				SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Artefacts"); //$NON-NLS-1$
		// End IRR
		// End of user code

		artefactComplianceMap = new TableViewer(tableArtefactComplianceMap);
		artefactComplianceMap.setContentProvider(new ArrayContentProvider());
		artefactComplianceMap.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for ArtefactComplianceMap
			public String getColumnText(Object object, int columnIndex) {
				// Start IRR
				/*
				 * AdapterFactoryLabelProvider labelProvider = new
				 * AdapterFactoryLabelProvider(adapterFactory); if (object
				 * instanceof EObject) { switch (columnIndex) { case 0: return
				 * labelProvider.getText(object); case 1: return
				 * labelProvider.getText(object); } }
				 */
				if (object instanceof EObject) {
					BaseComplianceMap baselineComplianceMapObject = (BaseComplianceMap) object;
					switch (columnIndex) {
					case 0:
						return baselineComplianceMapObject.getId();
					case 1:
						if (baselineComplianceMapObject.getMapGroup() == null)
							return "";
						else
							return baselineComplianceMapObject.getMapGroup()
									.getName();
					case 2:
						if (baselineComplianceMapObject.getTarget() == null)
							return "";
						else {
							EList<AssuranceAsset> LstAssurance = baselineComplianceMapObject
									.getTarget();
							Iterator<AssuranceAsset> iter = LstAssurance
									.iterator();
							String sArtefact = "[";
							while (iter.hasNext()) {
								AssuranceAsset refManAss = (AssuranceAsset) iter
										.next();
								if (refManAss instanceof Artefact) {
									Artefact art = (Artefact) refManAss;
									sArtefact = sArtefact + art.getName() + ",";
								} else if (refManAss instanceof Activity) {
									Artefact art = (Artefact) refManAss;
									sArtefact = sArtefact + art.getName() + ",";
								}
							}
							// delete ,
							sArtefact = sArtefact.substring(0,
									sArtefact.length() - 1);
							sArtefact = sArtefact + "]";
							return sArtefact;
						}

					}
				}
				// End IRR

				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		artefactComplianceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (artefactComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						artefactComplianceMap.refresh();
					}
				}
			}

		});
		GridData artefactComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		artefactComplianceMapData.minimumHeight = 120;
		artefactComplianceMapData.heightHint = 120;
		artefactComplianceMap.getTable().setLayoutData(artefactComplianceMapData);
		for (ViewerFilter filter : this.artefactComplianceMapFilters) {
			artefactComplianceMap.addFilter(filter);
		}
		EditingUtils.setID(artefactComplianceMap.getTable(), BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap);
		EditingUtils.setEEFtype(artefactComplianceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createArtefactComplianceMapPanel(tableContainer);
		// Start of user code for createArtefactComplianceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactComplianceMapPanel(Composite container) {
		Composite artefactComplianceMapPanel = new Composite(container, SWT.NONE);
		GridLayout artefactComplianceMapPanelLayout = new GridLayout();
		artefactComplianceMapPanelLayout.numColumns = 1;
		artefactComplianceMapPanel.setLayout(artefactComplianceMapPanelLayout);
		addArtefactComplianceMap = new Button(artefactComplianceMapPanel, SWT.NONE);
		addArtefactComplianceMap.setText(BaselineMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addArtefactComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addArtefactComplianceMap.setLayoutData(addArtefactComplianceMapData);
		addArtefactComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				artefactComplianceMap.refresh();
			}
		});
		EditingUtils.setID(addArtefactComplianceMap, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap);
		EditingUtils.setEEFtype(addArtefactComplianceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeArtefactComplianceMap = new Button(artefactComplianceMapPanel, SWT.NONE);
		removeArtefactComplianceMap.setText(BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeArtefactComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeArtefactComplianceMap.setLayoutData(removeArtefactComplianceMapData);
		removeArtefactComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (artefactComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						artefactComplianceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeArtefactComplianceMap, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap);
		EditingUtils.setEEFtype(removeArtefactComplianceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editArtefactComplianceMap = new Button(artefactComplianceMapPanel, SWT.NONE);
		editArtefactComplianceMap.setText(BaselineMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editArtefactComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editArtefactComplianceMap.setLayoutData(editArtefactComplianceMapData);
		editArtefactComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (artefactComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						artefactComplianceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editArtefactComplianceMap, BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap);
		EditingUtils.setEEFtype(editArtefactComplianceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createArtefactComplianceMapPanel

		// End of user code
		return artefactComplianceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart#initComplianceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initComplianceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		complianceMap.setContentProvider(contentProvider);
		complianceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactComplianceMap.Properties.complianceMap);
		if (eefElementEditorReadOnlyState && complianceMap.isEnabled()) {
			complianceMap.setEnabled(false);
			complianceMap.setToolTipText(BaselineMessages.BaseArtefactComplianceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !complianceMap.isEnabled()) {
			complianceMap.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart#updateComplianceMap()
	 * 
	 */
	public void updateComplianceMap() {
	complianceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart#addFilterComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToComplianceMap(ViewerFilter filter) {
		complianceMapFilters.add(filter);
		if (this.complianceMap != null) {
			this.complianceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart#addBusinessFilterComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToComplianceMap(ViewerFilter filter) {
		complianceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart#isContainedInComplianceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInComplianceMapTable(EObject element) {
		return ((ReferencesTableSettings)complianceMap.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart#initArtefactComplianceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initArtefactComplianceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		artefactComplianceMap.setContentProvider(contentProvider);
		artefactComplianceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactComplianceMap.Properties.artefactComplianceMap);
		if (eefElementEditorReadOnlyState && artefactComplianceMap.getTable().isEnabled()) {
			artefactComplianceMap.getTable().setEnabled(false);
			artefactComplianceMap.getTable().setToolTipText(BaselineMessages.BaseArtefactComplianceMap_ReadOnly);
			addArtefactComplianceMap.setEnabled(false);
			addArtefactComplianceMap.setToolTipText(BaselineMessages.BaseArtefactComplianceMap_ReadOnly);
			removeArtefactComplianceMap.setEnabled(false);
			removeArtefactComplianceMap.setToolTipText(BaselineMessages.BaseArtefactComplianceMap_ReadOnly);
			editArtefactComplianceMap.setEnabled(false);
			editArtefactComplianceMap.setToolTipText(BaselineMessages.BaseArtefactComplianceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !artefactComplianceMap.getTable().isEnabled()) {
			artefactComplianceMap.getTable().setEnabled(true);
			addArtefactComplianceMap.setEnabled(true);
			removeArtefactComplianceMap.setEnabled(true);
			editArtefactComplianceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart#updateArtefactComplianceMap()
	 * 
	 */
	public void updateArtefactComplianceMap() {
	artefactComplianceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart#addFilterArtefactComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArtefactComplianceMap(ViewerFilter filter) {
		artefactComplianceMapFilters.add(filter);
		if (this.artefactComplianceMap != null) {
			this.artefactComplianceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart#addBusinessFilterArtefactComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArtefactComplianceMap(ViewerFilter filter) {
		artefactComplianceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart#isContainedInArtefactComplianceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInArtefactComplianceMapTable(EObject element) {
		return ((ReferencesTableSettings)artefactComplianceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseArtefactComplianceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
