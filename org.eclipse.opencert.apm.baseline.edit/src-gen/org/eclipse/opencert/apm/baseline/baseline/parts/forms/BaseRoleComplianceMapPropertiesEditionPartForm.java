/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.forms;

// Start of user code for imports


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;
import org.eclipse.opencert.pam.procspec.process.Participant;

// End of user code

/**
 * 
 * 
 */
public class BaseRoleComplianceMapPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, BaseRoleComplianceMapPropertiesEditionPart {

	protected ReferencesTable complianceMap;
	protected List<ViewerFilter> complianceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> complianceMapFilters = new ArrayList<ViewerFilter>();
	protected TableViewer roleComplianceMap;
	protected List<ViewerFilter> roleComplianceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> roleComplianceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addRoleComplianceMap;
	protected Button removeRoleComplianceMap;
	protected Button editRoleComplianceMap;



	/**
	 * For {@link ISection} use only.
	 */
	public BaseRoleComplianceMapPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseRoleComplianceMapPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence baseRoleComplianceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseRoleComplianceMapStep.addStep(BaselineViewsRepository.BaseRoleComplianceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap);
		
		
		composer = new PartComposer(baseRoleComplianceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseRoleComplianceMap.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap) {
					return createComplianceMapTableComposition(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap) {
					return createRoleComplianceMapTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(BaselineMessages.BaseRoleComplianceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createComplianceMapTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.complianceMap = new ReferencesTable(getDescription(BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap, BaselineMessages.BaseRoleComplianceMapPropertiesEditionPart_ComplianceMapLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				complianceMap.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				complianceMap.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				complianceMap.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				complianceMap.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.complianceMapFilters) {
			this.complianceMap.addFilter(filter);
		}
		this.complianceMap.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap, BaselineViewsRepository.FORM_KIND));
		this.complianceMap.createControls(parent, widgetFactory);
		this.complianceMap.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData complianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		complianceMapData.horizontalSpan = 3;
		this.complianceMap.setLayoutData(complianceMapData);
		this.complianceMap.setLowerBound(0);
		this.complianceMap.setUpperBound(-1);
		complianceMap.setID(BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap);
		complianceMap.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createComplianceMapTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRoleComplianceMapTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableRoleComplianceMap = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableRoleComplianceMap.setHeaderVisible(true);
		GridData gdRoleComplianceMap = new GridData();
		gdRoleComplianceMap.grabExcessHorizontalSpace = true;
		gdRoleComplianceMap.horizontalAlignment = GridData.FILL;
		gdRoleComplianceMap.grabExcessVerticalSpace = true;
		gdRoleComplianceMap.verticalAlignment = GridData.FILL;
		tableRoleComplianceMap.setLayoutData(gdRoleComplianceMap);
		tableRoleComplianceMap.setLinesVisible(true);

		// Start of user code for columns definition for RoleComplianceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableRoleComplianceMap, SWT.NONE);
		 * name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableRoleComplianceMap, SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableRoleComplianceMap, SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableRoleComplianceMap, SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Participants"); //$NON-NLS-1$
		// End IRR
		// End of user code

		roleComplianceMap = new TableViewer(tableRoleComplianceMap);
		roleComplianceMap.setContentProvider(new ArrayContentProvider());
		roleComplianceMap.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for RoleComplianceMap
			public String getColumnText(Object object, int columnIndex) {
				//Start IRR
				/*AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
				if (object instanceof EObject) {
					switch (columnIndex) {
					case 0:
						return labelProvider.getText(object);
					case 1:
						return labelProvider.getText(object);
					}
				}*/
				if (object instanceof EObject) {
					BaseComplianceMap baselineComplianceMapObject = (BaseComplianceMap)object;
					switch (columnIndex) {
					case 0:							
						return baselineComplianceMapObject.getId();
					case 1:							
						if (baselineComplianceMapObject.getMapGroup() == null)
							return "";
						else
							return baselineComplianceMapObject.getMapGroup().getName();
					case 2:							
						if (baselineComplianceMapObject.getTarget() == null)
							return "";
						else{
							EList<AssuranceAsset> LstAssurance = baselineComplianceMapObject.getTarget();
							Iterator<AssuranceAsset> iter = LstAssurance.iterator();
							String sArtefact = "[";
							while (iter.hasNext()) {
								AssuranceAsset refManAss = (AssuranceAsset) iter.next();
								
								if (refManAss instanceof Participant) {
									Participant art = (Participant) refManAss;
									sArtefact = sArtefact + art.getName() + ",";  
								}
								
							}
							//delete ,
							sArtefact = sArtefact.substring(0, sArtefact.length()-1);
							sArtefact = sArtefact + "]";
							return sArtefact;
						}
							
					}
				}
				//End IRR
				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		roleComplianceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (roleComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) roleComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						roleComplianceMap.refresh();
					}
				}
			}

		});
		GridData roleComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		roleComplianceMapData.minimumHeight = 120;
		roleComplianceMapData.heightHint = 120;
		roleComplianceMap.getTable().setLayoutData(roleComplianceMapData);
		for (ViewerFilter filter : this.roleComplianceMapFilters) {
			roleComplianceMap.addFilter(filter);
		}
		EditingUtils.setID(roleComplianceMap.getTable(), BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap);
		EditingUtils.setEEFtype(roleComplianceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createRoleComplianceMapPanel(widgetFactory, tableContainer);
		// Start of user code for createRoleComplianceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRoleComplianceMapPanel(FormToolkit widgetFactory, Composite container) {
		Composite roleComplianceMapPanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout roleComplianceMapPanelLayout = new GridLayout();
		roleComplianceMapPanelLayout.numColumns = 1;
		roleComplianceMapPanel.setLayout(roleComplianceMapPanelLayout);
		addRoleComplianceMap = widgetFactory.createButton(roleComplianceMapPanel, BaselineMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addRoleComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addRoleComplianceMap.setLayoutData(addRoleComplianceMapData);
		addRoleComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				roleComplianceMap.refresh();
			}
		});
		EditingUtils.setID(addRoleComplianceMap, BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap);
		EditingUtils.setEEFtype(addRoleComplianceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeRoleComplianceMap = widgetFactory.createButton(roleComplianceMapPanel, BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeRoleComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeRoleComplianceMap.setLayoutData(removeRoleComplianceMapData);
		removeRoleComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (roleComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) roleComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						roleComplianceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeRoleComplianceMap, BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap);
		EditingUtils.setEEFtype(removeRoleComplianceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editRoleComplianceMap = widgetFactory.createButton(roleComplianceMapPanel, BaselineMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editRoleComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editRoleComplianceMap.setLayoutData(editRoleComplianceMapData);
		editRoleComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (roleComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) roleComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRoleComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						roleComplianceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editRoleComplianceMap, BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap);
		EditingUtils.setEEFtype(editRoleComplianceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createRoleComplianceMapPanel

		// End of user code
		return roleComplianceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart#initComplianceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initComplianceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		complianceMap.setContentProvider(contentProvider);
		complianceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRoleComplianceMap.Properties.complianceMap);
		if (eefElementEditorReadOnlyState && complianceMap.isEnabled()) {
			complianceMap.setEnabled(false);
			complianceMap.setToolTipText(BaselineMessages.BaseRoleComplianceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !complianceMap.isEnabled()) {
			complianceMap.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart#updateComplianceMap()
	 * 
	 */
	public void updateComplianceMap() {
	complianceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart#addFilterComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToComplianceMap(ViewerFilter filter) {
		complianceMapFilters.add(filter);
		if (this.complianceMap != null) {
			this.complianceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart#addBusinessFilterComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToComplianceMap(ViewerFilter filter) {
		complianceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart#isContainedInComplianceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInComplianceMapTable(EObject element) {
		return ((ReferencesTableSettings)complianceMap.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart#initRoleComplianceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initRoleComplianceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		roleComplianceMap.setContentProvider(contentProvider);
		roleComplianceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRoleComplianceMap.Properties.roleComplianceMap);
		if (eefElementEditorReadOnlyState && roleComplianceMap.getTable().isEnabled()) {
			roleComplianceMap.getTable().setEnabled(false);
			roleComplianceMap.getTable().setToolTipText(BaselineMessages.BaseRoleComplianceMap_ReadOnly);
			addRoleComplianceMap.setEnabled(false);
			addRoleComplianceMap.setToolTipText(BaselineMessages.BaseRoleComplianceMap_ReadOnly);
			removeRoleComplianceMap.setEnabled(false);
			removeRoleComplianceMap.setToolTipText(BaselineMessages.BaseRoleComplianceMap_ReadOnly);
			editRoleComplianceMap.setEnabled(false);
			editRoleComplianceMap.setToolTipText(BaselineMessages.BaseRoleComplianceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !roleComplianceMap.getTable().isEnabled()) {
			roleComplianceMap.getTable().setEnabled(true);
			addRoleComplianceMap.setEnabled(true);
			removeRoleComplianceMap.setEnabled(true);
			editRoleComplianceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart#updateRoleComplianceMap()
	 * 
	 */
	public void updateRoleComplianceMap() {
	roleComplianceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart#addFilterRoleComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToRoleComplianceMap(ViewerFilter filter) {
		roleComplianceMapFilters.add(filter);
		if (this.roleComplianceMap != null) {
			this.roleComplianceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart#addBusinessFilterRoleComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToRoleComplianceMap(ViewerFilter filter) {
		roleComplianceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart#isContainedInRoleComplianceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInRoleComplianceMapTable(EObject element) {
		return ((ReferencesTableSettings)roleComplianceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseRoleComplianceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
