/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.test;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.infra.mappings.mapping.MapJustification;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.infra.mappings.mapping.MappingFactory;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.StandaloneCDOAccessor;
import org.eclipse.opencert.storage.cdo.executors.SimpleInTransactionExecutor;
import org.eclipse.opencert.storage.cdo.test.DataCreationRoutines;

public class BaseFrameworkImpactStructureCreator
{
    
    public static void main(String[] args)
    {
        createUseCaseTest();
    }

    
    /*
     * 
     * ArtefactA --MODIFY, REVOKE--> ArtefactB --REVOKE, REVOKE--> ArtefactC (...)
     * 
     * 
     * (...) ArtefactC --MODIFY, VALIDATE--> ArtefactD --MODIFY, VALIDATE--> ArtefactE 
     * 
     */
    private static void createBasicImpactStructure()
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                BaseArtefact notImpactingArtefact = DataCreationRoutines.createBaseArtefact("NotImpactingArtefact", 1);
                
                BaseArtefact baseArtefactA = DataCreationRoutines.createBaseArtefact("BaseArtefactA", 1);
                BaseArtefact baseArtefactB = DataCreationRoutines.createBaseArtefact("BaseArtefactB", 2);
                BaseArtefact baseArtefactC = DataCreationRoutines.createBaseArtefact("BaseArtefactC", 3);
                BaseArtefact baseArtefactD = DataCreationRoutines.createBaseArtefact("BaseArtefactD", 4);
                BaseArtefact baseArtefactE = DataCreationRoutines.createBaseArtefact("BaseArtefactE", 5);
                
                Artefact artefactA = DataCreationRoutines.createArtefact("ArtefactA", 1);
                Artefact artefactE = DataCreationRoutines.createArtefact("ArtefactE", 5);
                
                BaseComplianceMap complianceMapA = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                MapJustification mapJustificationA = MappingFactory.eINSTANCE.createMapJustification();
                mapJustificationA.setExplanation("OK");
                resource.getContents().add(mapJustificationA);
                complianceMapA.setMapJustification(mapJustificationA);
                complianceMapA.setType(MapKind.FULL);
                
                BaseComplianceMap complianceMapE = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                MapJustification mapJustificationB = MappingFactory.eINSTANCE.createMapJustification();
                mapJustificationB.setExplanation("OK");
                resource.getContents().add(mapJustificationB);
                complianceMapE.setMapJustification(mapJustificationB);
                complianceMapE.setType(MapKind.FULL);
                
                complianceMapA.getTarget().add(artefactA);
                complianceMapE.getTarget().add(artefactE);
                
                
                baseArtefactA.getComplianceMap().add(complianceMapA);
                baseArtefactE.getComplianceMap().add(complianceMapE);
                
                /*
                 * The current semantics is "source artefact depends on target artefact", so
                 * the changes in the target may impact the source.
                 * 
                 */
                
                BaseArtefactRel baseArtefactRelA = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelA", baseArtefactB, baseArtefactA, ChangeEffectKind.REVOKE, ChangeEffectKind.REVOKE);
                BaseArtefactRel baseArtefactRelB = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelB", baseArtefactC, baseArtefactB, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                BaseArtefactRel baseArtefactRelC = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelC", baseArtefactD, baseArtefactC, ChangeEffectKind.REVOKE, ChangeEffectKind.REVOKE);
                BaseArtefactRel baseArtefactRelD = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelD", baseArtefactE, baseArtefactD, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                
                
                resource.getContents().add(notImpactingArtefact);
                resource.getContents().add(baseArtefactA);
                resource.getContents().add(baseArtefactB);
                resource.getContents().add(baseArtefactC);
                resource.getContents().add(baseArtefactD);
                resource.getContents().add(baseArtefactE);
                
                resource.getContents().add(artefactA);
                resource.getContents().add(artefactE);
                
                resource.getContents().add(baseArtefactRelA);
                resource.getContents().add(baseArtefactRelB);
                resource.getContents().add(baseArtefactRelC);
                resource.getContents().add(baseArtefactRelD);
                
            }
        }.executeReadWriteOperation();
    }
    
    
    
    
    private static void createAdvancedImpactStructure()
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                BaseArtefact notImpactingArtefact = DataCreationRoutines.createBaseArtefact("NotImpactingArtefact", 1);
                
                BaseArtefact baseArtefactA = DataCreationRoutines.createBaseArtefact("BaseArtefactAA", 1);
                BaseArtefact baseArtefactB = DataCreationRoutines.createBaseArtefact("BaseArtefactBB", 2);
                BaseArtefact baseArtefactC = DataCreationRoutines.createBaseArtefact("BaseArtefactCC", 3);
                BaseArtefact baseArtefactD = DataCreationRoutines.createBaseArtefact("BaseArtefactDD", 4);
                BaseArtefact baseArtefactE = DataCreationRoutines.createBaseArtefact("BaseArtefactEE", 5);
                
                Artefact artefact00 = DataCreationRoutines.createArtefact("Artefact00", 1);
                Artefact artefactA = DataCreationRoutines.createArtefact("ArtefactAA", 1);
                Artefact artefactE = DataCreationRoutines.createArtefact("ArtefactEE", 5);
                Artefact artefactZ = DataCreationRoutines.createArtefact("ArtefactZZ", 5);
                
                Artefact artefactB = DataCreationRoutines.createArtefact("ArtefactBB", 1);
                Artefact artefactC = DataCreationRoutines.createArtefact("ArtefactCC", 5);
                Artefact artefactD = DataCreationRoutines.createArtefact("ArtefactDD", 5);
                
                ArtefactRel artefactRel00 = DataCreationRoutines.createArtefactRel(
                        "ArtefactRel00", artefactA, artefact00, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                ArtefactRel artefactRelZZ = DataCreationRoutines.createArtefactRel(
                        "ArtefactRelZZ", artefactZ, artefactE, ChangeEffectKind.VALIDATE, ChangeEffectKind.VALIDATE);
                
                BaseComplianceMap complianceMapA = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapA.setType(MapKind.FULL);
                BaseComplianceMap complianceMapE = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapE.setType(MapKind.FULL);
                BaseComplianceMap complianceMapB = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapB.setType(MapKind.FULL);
                BaseComplianceMap complianceMapC = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapC.setType(MapKind.FULL);
                BaseComplianceMap complianceMapD = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapD.setType(MapKind.FULL);
                
                
                complianceMapA.getTarget().add(artefactA);
                complianceMapE.getTarget().add(artefactE);
                
                complianceMapB.getTarget().add(artefactB);
                complianceMapC.getTarget().add(artefactC);
                complianceMapD.getTarget().add(artefactD);
                
                
                baseArtefactA.getComplianceMap().add(complianceMapA);
                baseArtefactE.getComplianceMap().add(complianceMapE);
                
                baseArtefactB.getComplianceMap().add(complianceMapB);
                baseArtefactC.getComplianceMap().add(complianceMapC);
                baseArtefactD.getComplianceMap().add(complianceMapD);
                
                /*
                 * The current semantics is "source artefact depends on target artefact", so
                 * the changes in the target may impact the source.
                 * 
                 */
                
                BaseArtefactRel baseArtefactRelA = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelAA", baseArtefactB, baseArtefactA, ChangeEffectKind.REVOKE, ChangeEffectKind.REVOKE);
                BaseArtefactRel baseArtefactRelB = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelBB", baseArtefactC, baseArtefactB, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                BaseArtefactRel baseArtefactRelC = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelCC", baseArtefactD, baseArtefactC, ChangeEffectKind.REVOKE, ChangeEffectKind.REVOKE);
                BaseArtefactRel baseArtefactRelD = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelDD", baseArtefactE, baseArtefactD, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                
                
                resource.getContents().add(artefactRel00);
                resource.getContents().add(artefactRelZZ);
                
                resource.getContents().add(notImpactingArtefact);
                resource.getContents().add(baseArtefactA);
                resource.getContents().add(baseArtefactB);
                resource.getContents().add(baseArtefactC);
                resource.getContents().add(baseArtefactD);
                resource.getContents().add(baseArtefactE);
                
                resource.getContents().add(artefact00);
                resource.getContents().add(artefactA);
                resource.getContents().add(artefactE);
                resource.getContents().add(artefactZ);
                
                resource.getContents().add(artefactB);
                resource.getContents().add(artefactC);
                resource.getContents().add(artefactD);
                
                resource.getContents().add(baseArtefactRelA);
                resource.getContents().add(baseArtefactRelB);
                resource.getContents().add(baseArtefactRelC);
                resource.getContents().add(baseArtefactRelD);
                
            }
        }.executeReadWriteOperation();
    }
    
    
    private static void createAdvancedImpactStructureWIthMultipleMappings()
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                BaseArtefact notImpactingArtefact = DataCreationRoutines.createBaseArtefact("NotImpactingArtefact", 1);
                
                BaseArtefact baseArtefactA = DataCreationRoutines.createBaseArtefact("BaseArtefactAAAA", 1);
                BaseArtefact baseArtefactB = DataCreationRoutines.createBaseArtefact("BaseArtefactBBBB", 2);
                BaseArtefact baseArtefactC = DataCreationRoutines.createBaseArtefact("BaseArtefactCCCC", 3);
                BaseArtefact baseArtefactD = DataCreationRoutines.createBaseArtefact("BaseArtefactDDDD", 4);
                BaseArtefact baseArtefactE = DataCreationRoutines.createBaseArtefact("BaseArtefactEEEE", 5);
                
                BaseArtefact baseArtefactB2 = DataCreationRoutines.createBaseArtefact("BaseArtefactBBBB2", 2);
                BaseArtefact baseArtefactC2 = DataCreationRoutines.createBaseArtefact("BaseArtefactCCCC2", 3);
                BaseArtefact baseArtefactD2 = DataCreationRoutines.createBaseArtefact("BaseArtefactDDDD2", 4);
                BaseArtefact baseArtefactZ2 = DataCreationRoutines.createBaseArtefact("BaseArtefactZZZZ2", 5);
                
                BaseArtefact baseArtefactB3 = DataCreationRoutines.createBaseArtefact("BaseArtefactBBBB3", 2);
                BaseArtefact baseArtefactC3 = DataCreationRoutines.createBaseArtefact("BaseArtefactCCCC3", 3);
                BaseArtefact baseArtefactD3 = DataCreationRoutines.createBaseArtefact("BaseArtefactDDDD3", 4);
                BaseArtefact baseArtefactZ3 = DataCreationRoutines.createBaseArtefact("BaseArtefactZZZZ3", 5);
                
                
                Artefact artefact00 = DataCreationRoutines.createArtefact("Artefact000", 1);
                Artefact artefactA = DataCreationRoutines.createArtefact("ArtefactAAAA", 1);
                Artefact artefactE = DataCreationRoutines.createArtefact("ArtefactEEEE", 5);
                Artefact artefactZ = DataCreationRoutines.createArtefact("ArtefactZZZZ", 5);
                
                Artefact artefactB = DataCreationRoutines.createArtefact("ArtefactBBBB", 1);
                Artefact artefactC = DataCreationRoutines.createArtefact("ArtefactCCCC", 5);
                Artefact artefactD = DataCreationRoutines.createArtefact("ArtefactDDDD", 5);
                
                Artefact artefactB3 = DataCreationRoutines.createArtefact("ArtefactBBBB3", 1);
                Artefact artefactC3 = DataCreationRoutines.createArtefact("ArtefactCCCC3", 5);
                Artefact artefactD3 = DataCreationRoutines.createArtefact("ArtefactDDDD3", 5);
                Artefact artefactZ3 = DataCreationRoutines.createArtefact("ArtefactZZZZ3", 5);

                
                
                ArtefactRel artefactRel00 = DataCreationRoutines.createArtefactRel(
                        "ArtefactRel0000", artefactA, artefact00, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                ArtefactRel artefactRelZZ = DataCreationRoutines.createArtefactRel(
                        "ArtefactRelZZZZ", artefactZ, artefactE, ChangeEffectKind.VALIDATE, ChangeEffectKind.VALIDATE);
                
                BaseComplianceMap complianceMapA = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapA.setType(MapKind.FULL);
                BaseComplianceMap complianceMapE = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapE.setType(MapKind.FULL);
                
                BaseComplianceMap complianceMapB = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapB.setType(MapKind.FULL);
                BaseComplianceMap complianceMapC = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapC.setType(MapKind.FULL);
                BaseComplianceMap complianceMapD = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapD.setType(MapKind.FULL);
                
                
                BaseComplianceMap complianceMapB2 = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapB.setType(MapKind.FULL);
                BaseComplianceMap complianceMapC2 = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapC.setType(MapKind.FULL);
                BaseComplianceMap complianceMapD2 = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapD.setType(MapKind.FULL);
                BaseComplianceMap complianceMapZ2 = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapE.setType(MapKind.FULL);
                
                BaseComplianceMap complianceMapB3 = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapB.setType(MapKind.FULL);
                BaseComplianceMap complianceMapC3 = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapC.setType(MapKind.FULL);
                BaseComplianceMap complianceMapD3 = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapD.setType(MapKind.FULL);
                BaseComplianceMap complianceMapZ3 = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapE.setType(MapKind.FULL);
                
                
                complianceMapA.getTarget().add(artefactA);
                complianceMapE.getTarget().add(artefactE);
                
                complianceMapB.getTarget().add(artefactB);
                complianceMapC.getTarget().add(artefactC);
                complianceMapD.getTarget().add(artefactD);
                
                complianceMapB2.getTarget().add(artefactB);
                complianceMapC2.getTarget().add(artefactC);
                complianceMapD2.getTarget().add(artefactD);
                complianceMapZ2.getTarget().add(artefactZ);
                
                complianceMapB3.getTarget().add(artefactB3);
                complianceMapC3.getTarget().add(artefactC3);
                complianceMapD3.getTarget().add(artefactD3);
                complianceMapZ3.getTarget().add(artefactZ3);
                
                
                baseArtefactA.getComplianceMap().add(complianceMapA);
                baseArtefactE.getComplianceMap().add(complianceMapE);
                
                baseArtefactB.getComplianceMap().add(complianceMapB);
                baseArtefactC.getComplianceMap().add(complianceMapC);
                baseArtefactD.getComplianceMap().add(complianceMapD);
                
                baseArtefactB2.getComplianceMap().add(complianceMapB2);
                baseArtefactC2.getComplianceMap().add(complianceMapC2);
                baseArtefactD2.getComplianceMap().add(complianceMapD2);
                baseArtefactZ2.getComplianceMap().add(complianceMapZ2);
                
                baseArtefactB3.getComplianceMap().add(complianceMapB3);
                baseArtefactC3.getComplianceMap().add(complianceMapC3);
                baseArtefactD3.getComplianceMap().add(complianceMapD3);
                baseArtefactZ3.getComplianceMap().add(complianceMapZ3);
                
                /*
                 * The current semantics is "source artefact depends on target artefact", so
                 * the changes in the target may impact the source.
                 * 
                 */
                
                BaseArtefactRel baseArtefactRelA = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelAA", baseArtefactB, baseArtefactA, ChangeEffectKind.REVOKE, ChangeEffectKind.REVOKE);
                BaseArtefactRel baseArtefactRelB = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelBB", baseArtefactC, baseArtefactB, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                BaseArtefactRel baseArtefactRelC = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelCC", baseArtefactD, baseArtefactC, ChangeEffectKind.REVOKE, ChangeEffectKind.REVOKE);
                BaseArtefactRel baseArtefactRelD = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelDD", baseArtefactE, baseArtefactD, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                
                BaseArtefactRel baseArtefactRelB2 = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelBB2", baseArtefactB3, baseArtefactB2, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
                BaseArtefactRel baseArtefactRelC2 = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelCC2", baseArtefactC3, baseArtefactC2, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
                BaseArtefactRel baseArtefactRelD2 = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelDD2", baseArtefactD3, baseArtefactD2, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
                BaseArtefactRel baseArtefactRelZ2 = DataCreationRoutines.createBaseArtefactRel(
                        "BaseArtefactRelZZ2", baseArtefactZ3, baseArtefactZ2, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
                
                
                resource.getContents().add(artefactRel00);
                resource.getContents().add(artefactRelZZ);
                
                resource.getContents().add(notImpactingArtefact);
                resource.getContents().add(baseArtefactA);
                resource.getContents().add(baseArtefactB);
                resource.getContents().add(baseArtefactC);
                resource.getContents().add(baseArtefactD);
                resource.getContents().add(baseArtefactE);
                
                resource.getContents().add(baseArtefactB2);
                resource.getContents().add(baseArtefactC2);
                resource.getContents().add(baseArtefactD2);
                resource.getContents().add(baseArtefactZ2);
                
                resource.getContents().add(baseArtefactB3);
                resource.getContents().add(baseArtefactC3);
                resource.getContents().add(baseArtefactD3);
                resource.getContents().add(baseArtefactZ3);
                
                
                resource.getContents().add(artefact00);
                resource.getContents().add(artefactA);
                resource.getContents().add(artefactE);
                resource.getContents().add(artefactZ);
                
                resource.getContents().add(artefactB);
                resource.getContents().add(artefactC);
                resource.getContents().add(artefactD);
                
                resource.getContents().add(artefactB3);
                resource.getContents().add(artefactC3);
                resource.getContents().add(artefactD3);
                resource.getContents().add(artefactZ3);
                
                
                resource.getContents().add(baseArtefactRelA);
                resource.getContents().add(baseArtefactRelB);
                resource.getContents().add(baseArtefactRelC);
                resource.getContents().add(baseArtefactRelD);
                
                resource.getContents().add(baseArtefactRelB2);
                resource.getContents().add(baseArtefactRelC2);
                resource.getContents().add(baseArtefactRelD2);
                resource.getContents().add(baseArtefactRelZ2);
            }
        }.executeReadWriteOperation();
    }
    
    
    private static void createUseCaseTest()
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
        
                BaseComplianceMap complianceMapA = BaselineFactory.eINSTANCE.createBaseComplianceMap();
                complianceMapA.setType(MapKind.FULL);
                complianceMapA.setName("complianceMapA");
                MapJustification createMapJustification = MappingFactory.eINSTANCE.createMapJustification();
                createMapJustification.setExplanation("expl");
                complianceMapA.setMapJustification(createMapJustification);
                
                BaseArtefact baseArtefact = getBaseArtefactByCdoId(3942L, cdoTransaction);
                Artefact artefact = getArtefactByCdoId(6491L, cdoTransaction);
                
                complianceMapA.getTarget().add(artefact);
                baseArtefact.getComplianceMap().add(complianceMapA);
            
            }
        }.executeReadWriteOperation();
        
    }
    
    
    private static Artefact getArtefactByCdoId(final Long cdoId, CDOTransaction cdoTransaction)
    {
        CDOQuery query = cdoTransaction.createQuery("sql", "select * from evidence_artefact where cdo_id = " + cdoId
                + CDOStorageUtil.getMandatoryCDOQuerySuffix());
        Artefact artefact = query.getResultValue(Artefact.class);
        if (artefact == null) {
            throw new RuntimeException("Root impacting artefact doesn't exist in database");
        }
        
        return artefact;
    }
    
    
    private static BaseArtefact getBaseArtefactByCdoId(final Long cdoId, CDOTransaction cdoTransaction)
    {
        CDOQuery query = cdoTransaction.createQuery("sql", "select * from baseline_baseartefact where cdo_id = " + cdoId
                + CDOStorageUtil.getMandatoryCDOQuerySuffix());
        BaseArtefact artefact = query.getResultValue(BaseArtefact.class);
        if (artefact == null) {
            throw new RuntimeException("Root impacting artefact doesn't exist in database");
        }
        
        return artefact;
    }
    
    
}
