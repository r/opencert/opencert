/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.administration;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.opencert.webapp.reports.manager.BaseArtefactManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.IComplianceConsts;
import org.eclipse.opencert.webapp.reports.util.HtmlProcessor;
import org.eclipse.opencert.webapp.reports.util.IdNameDesc;
import org.eclipse.opencert.webapp.reports.util.ProjectAdminPageMode;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.view.common.IBaselineFrameworkChangeListener;
import org.eclipse.opencert.webapp.reports.view.common.INotificationPresenter;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;

import com.google.gwt.dev.util.collect.HashSet;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ProjectBaselineArtefactTable
        extends VerticalLayout
        implements IBaselineFrameworkChangeListener
{
    private ComplianceManager<BaselineElementDataRow> complianceManager;
    private RelationCellsController relationCellsProcessor;
    private Table table;
    private INotificationPresenter notificationPresetter;
    private long baselineFrameworkID = -1;
    private List<Long> deletedRowCDOIDs;
    private Set<Long> addedRowIDs;
    private TextField baselineFrameworkField = new TextField("Baseline Framework Name");
    
    private BaselineElementDescriptionDataRowProcessor dataRowProcessor;
    
    private ProjectAdminPageMode projectMode;
    
    private Long editedRowCDOId = null;
       
    public ProjectBaselineArtefactTable()
    {
        deletedRowCDOIDs = new LinkedList<Long>();
        addedRowIDs = new HashSet<Long>();
        
        dataRowProcessor = new BaselineElementDescriptionDataRowProcessor(this);
        complianceManager = new ComplianceManager<BaselineElementDataRow>(dataRowProcessor);
        table = initTable();
        HorizontalLayout horizontalLayout = initAddRowButton();
        baselineFrameworkField.addStyleName("baselineFrameworkField");
        baselineFrameworkField.addBlurListener(e ->
        {
        	updateNotificationPresenterValue();
        });
        addComponent(baselineFrameworkField);
        addComponent(horizontalLayout);
        addComponent(table);
    }
    
    private HorizontalLayout initAddRowButton()
    {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Button button = new Button("Add new Baseline Artefact");
        button.setDescription("Add new Baseline Artefact to this project definition.");
        button.addStyleName("commonButton");
        
        button.addClickListener(e -> 
        {
            addRow();
        });
        horizontalLayout.addComponent(button);
        
        Label spacer = new Label();
        horizontalLayout.addComponent(spacer);
        horizontalLayout.setExpandRatio(spacer, 1.0f);
        
        Label noteLabel = new Label();
        noteLabel.setValue("Note: This page facilitates project editing basic actions (like modification of project baseline artefacts). For more advanced project editing functionality use OPENCERT Client Editor.");
        noteLabel.setStyleName("ProjectAdminBaselineTableNote");
        horizontalLayout.addComponent(noteLabel);
        
        return horizontalLayout;
    }

    void saveEditedRow(final BaselineElementDataRow dataRow)
    {
        dataRow.getEditedNameField().commit();
        dataRow.getEditedDescriptionField().commit();

        setRowEditableInGUI(false, dataRow);
        updateNotificationPresenterValue();
    }
    
    private void updateNotificationPresenterValue() 
    {
    	if (projectMode == ProjectAdminPageMode.CREATE_PROJECT) {
    		notificationPresetter.showNotification(ProjectAdministrationPage.NEW_PROJECT_DATA_CREATED_MSG);
    	} else {
    		notificationPresetter.showNotification(ProjectAdministrationPage.PROJECT_DATA_CHANGED_MSG);
    	}
    }
    
    public void setProjectMode(ProjectAdminPageMode mode) {
    	projectMode = mode;
    }
    
    public void addRow()
    {
        // prepare new data
        BaseArtefact bArtefact = BaselineFactory.eINSTANCE.createBaseArtefact();
        bArtefact.setName("");
        bArtefact.setDescription("");
        final long rowUniqueTemporaryID = RowTempIDGenerator.generateRowUniqueTemporaryId();
        BaselineElementDataRow newRow = dataRowProcessor.createBaseAssetDataRow(rowUniqueTemporaryID, bArtefact, null, ComplianceManager.getBaseArtefactImage());        
        
        HierarchicalContainer hc = (HierarchicalContainer)table.getContainerDataSource();
        Item item = hc.addItem(newRow);
        hc.setChildrenAllowed(newRow, false);
        dataRowProcessor.setItemPropsFromDataRow(item, newRow);
        
        addedRowIDs.add(rowUniqueTemporaryID);
        
        // refresh GUI
        newRow.getEditRowPanel().setEditMode();
        newRow.getEditRowPanel().setVolatile2(true);
        editRow(newRow);
    }
    
    public void cloneRows()
    {
    	HierarchicalContainer newHC = complianceManager.initEmptyTableDataContainer();
    	
    	Container oldDataSource = complianceManager.readBaseAssetDataFromDb(baselineFrameworkID);
    	Collection<?> oldDataRows = oldDataSource.getItemIds();
    	for (Object oldItem : oldDataRows)
        {
    		BaselineElementDataRow oldRow = (BaselineElementDataRow)oldItem;
    		BaseArtefact newBaseArtefact = BaselineFactory.eINSTANCE.createBaseArtefact();
    	    newBaseArtefact.setName(oldRow.getName());
    	    newBaseArtefact.setDescription(oldRow.getDescription());
    	    
    	    final long newRowTempID = RowTempIDGenerator.generateRowUniqueTemporaryId();
    	    BaselineElementDataRow newRow = dataRowProcessor.createBaseAssetDataRow(newRowTempID, newBaseArtefact, null, ComplianceManager.getBaseArtefactImage());
    	    
            Long oldCDOID = oldRow.getCdoID();
            newRow.setOldCDOID(oldCDOID);
            final BaseArtefactManager baseArtefactManager = (BaseArtefactManager)SpringContextHelper.getBeanFromWebappContext(BaseArtefactManager.SPRING_NAME);
            final List<BaseArtefact> oldAffectedBaseArtefacts = baseArtefactManager.getAfftectedBaseArtefacts(oldCDOID, ProjectAdministrationPage.THE_EDITOR_SUPPORTED_IA_EFFECT);
            if (oldAffectedBaseArtefacts != null && oldAffectedBaseArtefacts.size() > 0)
            {
                final BaseArtefactRelationsField baseArtefactRelationsField = new BaseArtefactRelationsField(oldCDOID, oldAffectedBaseArtefacts);
                baseArtefactRelationsField.setNewRowRelationPreservedMode();
                newRow.setRelationsField(baseArtefactRelationsField);
            }

    	    Item newRowItem = newHC.addItem(newRow);
    	    newHC.setChildrenAllowed(newRow, false);    	    
    	    dataRowProcessor.setItemPropsFromDataRow(newRowItem, newRow);
    	    
    	    addedRowIDs.add(newRowTempID);
        }
    	
    	table.setContainerDataSource(newHC);
    }
    
    public void deleteRow(BaselineElementDataRow rowToDelete, boolean deleteFromDb)
    {
        final Long cdoIDToDelete = rowToDelete.getCdoID();
        Collection<?> allRows = table.getItemIds();
        for (Object item : allRows)
        {
            BaselineElementDataRow r = (BaselineElementDataRow)item;
            if (cdoIDToDelete == r.getCdoID())
            {
                table.removeItem(r);
                if (deleteFromDb) {
                    deletedRowCDOIDs.add(cdoIDToDelete);
                }
                break;
            }
        }

        setRowEditableInGUI(false, rowToDelete);
        updateNotificationPresenterValue();
    }
    
    void cancelEditedRow(final BaselineElementDataRow dataRow)
    {
        dataRow.getEditedDescriptionField().discard();
        dataRow.getEditedNameField().discard();
        dataRow.resetEditedRelations();
        
        setRowEditableInGUI(false, dataRow);
    }
    
    void editRow(final BaselineElementDataRow dataRow)
    {
        setRowEditableInGUI(true, dataRow);
    }
    
    @Override
    public void setBaselineFramework(long baselineID)
    {
        baselineFrameworkID  = baselineID;
        editedRowCDOId = null;
        
        Container newDataSource = complianceManager.readBaseAssetDataFromDb(baselineID);
        table.setContainerDataSource(newDataSource);
        
        // sets the *order* of the columns
        table.setVisibleColumns(IComplianceConsts.CDO_PROPERTY_ID, IComplianceConsts.NAME_PROPERTY_ID, IComplianceConsts.DESCRIPTION_PROPERTY_ID, 
                IComplianceConsts.RELATIONS_PROPERTY_ID, IComplianceConsts.BUTTON_PANEL_PROPERTY_ID);
    }
    
    public void setBaselineFrameworkName(String baselineFrameworkValue) 
    {
    	baselineFrameworkField.setValue(baselineFrameworkValue);
    }
    
    public String getBaselineFrameworkName() 
    {
    	return baselineFrameworkField.getValue();
    }

    private void setRowEditableInGUI(boolean setEditable, final BaselineElementDataRow rowToEdit)
    {
        cancelCurrentlyEditedRow(rowToEdit);
        
        if (setEditable) 
        {
            editedRowCDOId = rowToEdit.getCdoID();
            table.select(rowToEdit);
            relationCellsProcessor.relationsCellEdit(setEditable, rowToEdit);
            
            table.removeGeneratedColumn(IComplianceConsts.DESCRIPTION_PROPERTY_ID);
        } 
        else 
        {
            editedRowCDOId = null;
            table.unselect(rowToEdit);
            relationCellsProcessor.relationsCellUnedit();
            
            addGeneratedColumnForDescription(table);
        }
        
        // important so that TableFieldFactory works
        table.setEditable(setEditable);        
        // Refreshes the table GUI
        // calls TableFieldFactory - for Name and Description fields
        table.refreshRowCache();
    }

    private void cancelCurrentlyEditedRow(final BaselineElementDataRow rowToEdit)
    {
        if (editedRowCDOId != null && !editedRowCDOId.equals(rowToEdit.getCdoID()))
        {
            // there is already some row being edited - switch that row off
            BaselineElementDataRow dataRowToCancel = getBaselineElementDataRow(editedRowCDOId);
            if (dataRowToCancel == null) {
                throw new IllegalStateException();
            }
            dataRowToCancel.getEditRowPanel().clickCancelEditedRow(dataRowToCancel);
        }
    }

    private BaselineElementDataRow getBaselineElementDataRow(Long cdoID)
    {
        Collection<?> dataRows = table.getContainerDataSource().getItemIds();
        for (Object item : dataRows)
        {
            BaselineElementDataRow row = (BaselineElementDataRow)item;
            if (cdoID.equals(row.getCdoID())) {
                return row;
            }
        }
        return null;
    }



    private Table initTable()
    {
        Table resultTable = new Table("Baseline Framework Artefacts");
        resultTable.addStyleName("complianceTable");
        resultTable.addStyleName("baselineElementTable");
        
        resultTable.setColumnHeader(IComplianceConsts.CDO_PROPERTY_ID, "CDO Id");
        resultTable.setColumnHeader(IComplianceConsts.NAME_PROPERTY_ID, "Baseline Artefact Name");
        resultTable.setColumnHeader(IComplianceConsts.DESCRIPTION_PROPERTY_ID, "Baseline Artefact Description");
        resultTable.setColumnHeader(IComplianceConsts.RELATIONS_PROPERTY_ID, "Dependent Baseline Artefacts<BR/><i>\'Modification Effect\' for Impact Analysis</i>");
        resultTable.setColumnHeader(IComplianceConsts.BUTTON_PANEL_PROPERTY_ID, "Edit");
        
        resultTable.setColumnExpandRatio(IComplianceConsts.CDO_PROPERTY_ID, 10);
        resultTable.setColumnExpandRatio(IComplianceConsts.NAME_PROPERTY_ID, 30);
        resultTable.setColumnExpandRatio(IComplianceConsts.DESCRIPTION_PROPERTY_ID, 30);
        resultTable.setColumnExpandRatio(IComplianceConsts.RELATIONS_PROPERTY_ID, 20);
        resultTable.setColumnExpandRatio(IComplianceConsts.BUTTON_PANEL_PROPERTY_ID, 10);
        
        resultTable.setColumnWidth(IComplianceConsts.BUTTON_PANEL_PROPERTY_ID, 150);
        resultTable.setColumnAlignment(IComplianceConsts.BUTTON_PANEL_PROPERTY_ID, Align.CENTER);
        
        resultTable.addStyleName("EditBaselineElementTable");    
        resultTable.setPageLength(0);
        resultTable.setBuffered(false);
        resultTable.setSelectable(false);
        resultTable.setNullSelectionAllowed(false);
        resultTable.setImmediate(true);
        resultTable.setSizeFull();
        
        resultTable.setCellStyleGenerator(new Table.CellStyleGenerator() 
        {
            @Override
            public String getStyle(Table source, Object itemId, Object propertyId)
            {
                BaselineElementDataRow row = (BaselineElementDataRow)itemId;
                if (IComplianceConsts.NAME_PROPERTY_ID.equals(propertyId) ||
                        IComplianceConsts.DESCRIPTION_PROPERTY_ID.equals(propertyId))
                {
                    if (ProjectBaselineArtefactTable.RowTempIDGenerator.isTempID(row.getCdoID())) 
                    {
                        return "addedRowNotSaved";
                    }
                }
                return null;
            }
        });
        
        resultTable.addItemClickListener(event ->
        {
            if (!event.isDoubleClick()) {
                return;
            }
            BaselineElementDataRow dataRow = (BaselineElementDataRow)event.getItemId();
            final EditRowPanel editRowPanel = dataRow.getEditRowPanel();
            editRowPanel.clickEditButton();
        });
                                    
        resultTable.setTableFieldFactory(new DefaultFieldFactory()
        {
            // Renders the specific cell of the table
            @Override
            public Field<?> createField(Container container, Object tableRow, Object propertyId, Component uiContext)
            {
                final BaselineElementDataRow renderedRow = (BaselineElementDataRow)tableRow;
                if (renderedRow.getCdoID() == null) {
                    throw new IllegalStateException();
                }
                
                if (renderedRow.getCdoID().equals(editedRowCDOId)) 
                {
                    // for the edited row:
                    if (IComplianceConsts.NAME_PROPERTY_ID.equals(propertyId)) {
                        TextField nameEditingField = renderedRow.getEditedNameField();
                        nameEditingField.focus();
                        return nameEditingField;
                    }
                    if (IComplianceConsts.DESCRIPTION_PROPERTY_ID.equals(propertyId)) {
                        return renderedRow.getEditedDescriptionField();
                    }
                } 
                return null;
            }
        });

        addGeneratedColumnForDescription(resultTable);

        relationCellsProcessor = new RelationCellsController(resultTable);
        
        return resultTable;
    }
    
    private void addGeneratedColumnForDescription(Table theTable)
    {
        final ColumnGenerator columnGenerator = theTable.getColumnGenerator(IComplianceConsts.DESCRIPTION_PROPERTY_ID);
        
        if (columnGenerator != null) {
            return;
        }
        
        theTable.addGeneratedColumn(IComplianceConsts.DESCRIPTION_PROPERTY_ID, new ColumnGenerator() 
        {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId)
            {
                final String desc = (String)source.getContainerProperty(itemId, IComplianceConsts.DESCRIPTION_PROPERTY_ID).getValue();
                Label label = new Label(HtmlProcessor.processHtmlLinks(desc));
                label.setContentMode(ContentMode.HTML);
                return label;
            }
        });
    }

    public void setNotificationPresenter(INotificationPresenter np)
    {
        notificationPresetter = np;
    }

    public long getBaselineCDOId()
    {
        return baselineFrameworkID;
    }

    public BaselineArtafectChangePack getCurrentElementChange()
    {
        BaselineArtafectChangePack changePack = new BaselineArtafectChangePack();

        Collection<?> dataRows = table.getItemIds();
        for (Object item : dataRows)
        {
            BaselineElementDataRow row = (BaselineElementDataRow)item;
            if (addedRowIDs.contains(row.getCdoID()))
            {
                final IdNameDesc idNameDesc = new IdNameDesc(row.getCdoID(), row.getEditedName(), row.getEditedDescription(), row.getRelationsField().getRelationIDs(), row.getOldCDOID());
                changePack.addAddedBaseArtefacts(idNameDesc);
                continue;
            }
            if (row.isChangedInGUI())
            {
                final IdNameDesc idNameDesc = new IdNameDesc(row.getCdoID(), row.getEditedName(), row.getEditedDescription(), row.getEditedRelations(), row.getOldCDOID());
                changePack.addChangedBaseArtefact(idNameDesc);
                continue;
            }
        }
        
        deletedRowCDOIDs.forEach(cdoID -> changePack.addRemovedBaseArtefacts(cdoID));
        
        return changePack;
    }
    
    public static class BaselineArtafectChangePack
    {
        private Collection<IdNameDesc> changedBaseArtefacts = new LinkedList<IdNameDesc>();
        private Collection<IdNameDesc> addedArtefacts = new LinkedList<IdNameDesc>();
        private Collection<Long> removedArtefactIDs = new LinkedList<Long>();

        public Collection<IdNameDesc> getChangedBaseArtefacts()
        {
            return Collections.unmodifiableCollection(changedBaseArtefacts);
        }
        
        public void addChangedBaseArtefact(IdNameDesc idNameDesc)
        {
            changedBaseArtefacts.add(idNameDesc);
        }
        
        public Collection<IdNameDesc> getAddedBaseArtefacts()
        {
            return Collections.unmodifiableCollection(addedArtefacts);
        }
        
        public void addAddedBaseArtefacts(IdNameDesc idNameDesc)
        {
            addedArtefacts.add(idNameDesc);
        }
        
        public Collection<Long> getRemovedBaseArtefacts()
        {
            return Collections.unmodifiableCollection(removedArtefactIDs);
        }
        
        public void addRemovedBaseArtefacts(long id)
        {
            removedArtefactIDs.add(id);
        }
    }

    public static class RowTempIDGenerator
    {
        private static final long INIT_TEMP_ID = -100;
        private static long thisTableRowUniqueTemporaryID = INIT_TEMP_ID;
        
        public static long generateRowUniqueTemporaryId()
        {
            thisTableRowUniqueTemporaryID--;
            
            return thisTableRowUniqueTemporaryID;
        }
        
        public static boolean isTempID(long id)
        {
            return (id <= INIT_TEMP_ID);            
        }
    }
}
