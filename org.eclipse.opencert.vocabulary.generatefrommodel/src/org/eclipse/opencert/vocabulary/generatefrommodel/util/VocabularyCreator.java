/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.generatefrommodel.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.vocabulary.Category;
import org.eclipse.opencert.vocabulary.SourceOfDefinition;
import org.eclipse.opencert.vocabulary.Term;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.opencert.vocabulary.VocabularyFactory;

/**
 * Creates a vocabulary from the CCL model elements in a project. A term is
 * created for every model element which has a description and a name. Terms are
 * put into a category which is derived from the type of the term.
 */
public class VocabularyCreator {

	private List<Category> packageCategories = new ArrayList<Category>();
	private Set<EObject> processedElements = new HashSet<EObject>();

	public void createVocabulary(IProject iProject, Resource targetResource) {
		Vocabulary vocabulary = VocabularyFactory.eINSTANCE.createVocabulary();
		List<EPackage> ePackages = getCclEPackages();
		Map<EClass, Category> termCategories = createTermCategories(ePackages);
		List<EObject> modelElements = FileUtil.getModels(iProject);
		createTerms(vocabulary, modelElements, termCategories);
		addCategories(vocabulary, packageCategories, termCategories);
		addSourceOfDefinition(vocabulary, iProject);

		targetResource.getContents().add(vocabulary);
	}

	private void addSourceOfDefinition(Vocabulary vocabulary, IProject iProject) {
		if (!vocabulary.getTerms().isEmpty()) {
			SourceOfDefinition source = VocabularyFactory.eINSTANCE
					.createSourceOfDefinition();
			source.setUri(iProject.getLocationURI().toASCIIString());
			source.setName("Vocabulary data extracted from the CCL model");

			for (Term term : vocabulary.getTerms()) {
				term.setDefinedBy(source);
			}

			vocabulary.getSourcesOfDefinition().add(source);
		}

	}

	private void addCategories(Vocabulary vocabulary,
			List<Category> packageCategories,
			Map<EClass, Category> termCategories) {
		List<Category> forDeletion = new ArrayList<Category>();
		for (Category termCategory : termCategories.values()) {
			if (termCategory.getTerms().isEmpty()) {
				// forDeletion.add(termCategory);
			} else {
				vocabulary.getCategories().add(termCategory);
			}
		}

		// The package categories must have properly set sub-categories.
		for (Category packageCategory : packageCategories) {
			for (Category subCategory : packageCategory.getSubCategories()) {
				if (vocabulary.getCategories().contains(subCategory)) {
					vocabulary.getCategories().add(packageCategory);
					break;
				}
			}

			if (packageCategory.getSubCategories().isEmpty()) {
				forDeletion.add(packageCategory);
			}
		}

		for (Category category : forDeletion) {
			EcoreUtil.delete(category);
		}

	}

	private List<EPackage> getCclEPackages() {
		List<EPackage> ePackages = new ArrayList<EPackage>();
		ePackages.add(AssuranceassetPackage.eINSTANCE);
		ePackages.add(AssuranceprojectPackage.eINSTANCE);
		ePackages.add(BaselinePackage.eINSTANCE);
		ePackages.add(EvidencePackage.eINSTANCE);
		ePackages.add(MappingPackage.eINSTANCE);
		ePackages.add(ProcessPackage.eINSTANCE);
		ePackages.add(RefframeworkPackage.eINSTANCE);
		ePackages.add(ArgPackage.eINSTANCE);
		return ePackages;
	}

	private Map<EClass, Category> createTermCategories(List<EPackage> ePackages) {
		Map<EClass, Category> result = new HashMap<EClass, Category>();
		for (EPackage ePackage : ePackages) {
			Category packageCategory = VocabularyFactory.eINSTANCE
					.createCategory();
			packageCategory.setName(ePackage.getName());
			packageCategory
					.setDescription("Contains the model elements of the "
							+ ePackage.getName() + " package.");

			boolean hasSubCategories = false;
			for (EObject obj : ePackage.eContents()) {
				if (obj instanceof EClass) {
					EClass eClass = (EClass) obj;

					if (eClass.getName().equals("Claim")) {
						System.out.println();
					}

					if (!eClass.isAbstract() && !eClass.isInterface()) {
						Category termCategory = VocabularyFactory.eINSTANCE
								.createCategory();
						termCategory.setName(eClass.getName());
						termCategory
								.setDescription("Contains model elements of the type "
										+ eClass.getName());
						packageCategory.getSubCategories().add(termCategory);
						result.put(eClass, termCategory);
						hasSubCategories = true;
					}
				}
			}

			if (hasSubCategories) {
				packageCategories.add(packageCategory);
			}
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	private void createTerms(Vocabulary vocabulary,
			List<EObject> modelElements, Map<EClass, Category> categories) {
		for (EObject modelElement : modelElements) {

			// Prevent cycles
			if (processedElements.contains(modelElement)) {
				continue;
			} else {
				processedElements.add(modelElement);
			}

			EClass eClass = modelElement.eClass();

			if (modelElement instanceof Claim) {
				System.out.println();
			}

			Category category = categories.get(eClass);

			// Skip if there is no valid category for the type of the model
			// element
			if (category != null) {
				String name = getStringPropertyValue(modelElement, "name");
				if (name == null) {
					name = getStringPropertyValue(modelElement, "id");
				}
				String description = getStringPropertyValue(modelElement,
						"description");
				if (name != null) {
					Term term = VocabularyFactory.eINSTANCE.createTerm();
					term.setName(name);
					if (description != null) {
						term.getDefinitions().add(description);
					}
					category.getTerms().add(term);
					vocabulary.getTerms().add(term);
				}

				// Recursive part
				for (EReference eReference : eClass.getEReferences()) {
					EClassifier eClassifier = eReference.getEType();
					if (eClassifier instanceof EClass) {
						Object referenceContent = modelElement.eGet(eReference);
						if (referenceContent instanceof EList) {
							createTerms(vocabulary,
									(List<EObject>) referenceContent,
									categories);
						} else if (referenceContent instanceof EObject) {
							createTerms(
									vocabulary,
									Collections
											.singletonList((EObject) referenceContent),
									categories);
						}
					}
				}
			}
		}
	}

	private String getStringPropertyValue(EObject eObject, String propertyName) {
		String result = null;
		Object propertyValue = null;

		EAttribute eAttribute = getAttributeByName(eObject.eClass(),
				propertyName);

		if (eAttribute != null) {
			propertyValue = eObject.eGet(eAttribute);
		}

		if (eAttribute != null && propertyValue instanceof String) {
			result = (String) propertyValue;
		}

		return result;
	}

	private EAttribute getAttributeByName(EClass eClass, String name) {
		EAttribute result = null;
		for (EAttribute eAttribute : eClass.getEAllAttributes()) {
			if (eAttribute.getName().equalsIgnoreCase(name)) {
				result = eAttribute;
				break;
			}
		}

		return result;
	}
}
