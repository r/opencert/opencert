/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedChallengeCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedChallengeReorientCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedContextCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedContextReorientCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedCounterEvidenceCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedCounterEvidenceReorientCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedEvidenceCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedEvidenceReorientCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedInferenceCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedInferenceReorientCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedChallengeEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedContextEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedCounterEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedInferenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;

/**
 * @generated
 */
public class AssertedContextItemSemanticEditPolicy extends
		ArgBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public AssertedContextItemSemanticEditPolicy() {
		super(ArgElementTypes.AssertedContext_4003);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		return getGEFWrapper(new DestroyElementCommand(req));
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req)
				: getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super
				.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (ArgElementTypes.AssertedInference_4001 == req.getElementType()) {
			return getGEFWrapper(new AssertedInferenceCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedEvidence_4002 == req.getElementType()) {
			return getGEFWrapper(new AssertedEvidenceCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedContext_4003 == req.getElementType()) {
			return getGEFWrapper(new AssertedContextCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedChallenge_4004 == req.getElementType()) {
			return null;
		}
		if (ArgElementTypes.AssertedCounterEvidence_4005 == req
				.getElementType()) {
			return null;
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (ArgElementTypes.AssertedInference_4001 == req.getElementType()) {
			return getGEFWrapper(new AssertedInferenceCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedEvidence_4002 == req.getElementType()) {
			return getGEFWrapper(new AssertedEvidenceCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedContext_4003 == req.getElementType()) {
			return getGEFWrapper(new AssertedContextCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedChallenge_4004 == req.getElementType()) {
			return getGEFWrapper(new AssertedChallengeCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedCounterEvidence_4005 == req
				.getElementType()) {
			return getGEFWrapper(new AssertedCounterEvidenceCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * Returns command to reorient EClass based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientRelationshipCommand(
			ReorientRelationshipRequest req) {
		switch (getVisualID(req)) {
		case AssertedInferenceEditPart.VISUAL_ID:
			return getGEFWrapper(new AssertedInferenceReorientCommand(req));
		case AssertedEvidenceEditPart.VISUAL_ID:
			return getGEFWrapper(new AssertedEvidenceReorientCommand(req));
		case AssertedContextEditPart.VISUAL_ID:
			return getGEFWrapper(new AssertedContextReorientCommand(req));
		case AssertedChallengeEditPart.VISUAL_ID:
			return getGEFWrapper(new AssertedChallengeReorientCommand(req));
		case AssertedCounterEvidenceEditPart.VISUAL_ID:
			return getGEFWrapper(new AssertedCounterEvidenceReorientCommand(req));
		}
		return super.getReorientRelationshipCommand(req);
	}

}
