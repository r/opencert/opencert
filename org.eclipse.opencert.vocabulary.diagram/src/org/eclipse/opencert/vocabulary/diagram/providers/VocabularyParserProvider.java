/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.vocabulary.VocabularyPackage;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryNameEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionNameEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionUriEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermDefinitionsEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermNameEditPart;
import org.eclipse.opencert.vocabulary.diagram.parsers.MessageFormatParser;
import org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry;

/**
 * @generated
 */
public class VocabularyParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser termName_5001Parser;

	/**
	 * @generated
	 */
	private IParser getTermName_5001Parser() {
		if (termName_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { VocabularyPackage.eINSTANCE
					.getVocabularyElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			termName_5001Parser = parser;
		}
		return termName_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser termDefinitions_5002Parser;

	/**
	 * @generated
	 */
	private IParser getTermDefinitions_5002Parser() {
		if (termDefinitions_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { VocabularyPackage.eINSTANCE
					.getTerm_Definitions() };
			MessageFormatParser parser = new MessageFormatParser(features);
			termDefinitions_5002Parser = parser;
		}
		return termDefinitions_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser categoryName_5003Parser;

	/**
	 * @generated
	 */
	private IParser getCategoryName_5003Parser() {
		if (categoryName_5003Parser == null) {
			EAttribute[] features = new EAttribute[] { VocabularyPackage.eINSTANCE
					.getVocabularyElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			categoryName_5003Parser = parser;
		}
		return categoryName_5003Parser;
	}

	/**
	 * @generated
	 */
	private IParser sourceOfDefinitionName_5004Parser;

	/**
	 * @generated
	 */
	private IParser getSourceOfDefinitionName_5004Parser() {
		if (sourceOfDefinitionName_5004Parser == null) {
			EAttribute[] features = new EAttribute[] { VocabularyPackage.eINSTANCE
					.getVocabularyElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			sourceOfDefinitionName_5004Parser = parser;
		}
		return sourceOfDefinitionName_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser sourceOfDefinitionUri_5005Parser;

	/**
	 * @generated
	 */
	private IParser getSourceOfDefinitionUri_5005Parser() {
		if (sourceOfDefinitionUri_5005Parser == null) {
			EAttribute[] features = new EAttribute[] { VocabularyPackage.eINSTANCE
					.getSourceOfDefinition_Uri() };
			MessageFormatParser parser = new MessageFormatParser(features);
			sourceOfDefinitionUri_5005Parser = parser;
		}
		return sourceOfDefinitionUri_5005Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case TermNameEditPart.VISUAL_ID:
			return getTermName_5001Parser();
		case TermDefinitionsEditPart.VISUAL_ID:
			return getTermDefinitions_5002Parser();
		case CategoryNameEditPart.VISUAL_ID:
			return getCategoryName_5003Parser();
		case SourceOfDefinitionNameEditPart.VISUAL_ID:
			return getSourceOfDefinitionName_5004Parser();
		case SourceOfDefinitionUriEditPart.VISUAL_ID:
			return getSourceOfDefinitionUri_5005Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(VocabularyVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(VocabularyVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (VocabularyElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
