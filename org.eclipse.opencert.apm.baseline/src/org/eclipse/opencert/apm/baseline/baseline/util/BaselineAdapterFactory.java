/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.apm.baseline.baseline.*;
import org.eclipse.opencert.infra.mappings.mapping.ComplianceMap;
import org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap;
import org.eclipse.opencert.infra.mappings.mapping.Map;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage
 * @generated
 */
public class BaselineAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BaselinePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaselineAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = BaselinePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaselineSwitch<Adapter> modelSwitch =
		new BaselineSwitch<Adapter>() {
			@Override
			public Adapter caseBaseFramework(BaseFramework object) {
				return createBaseFrameworkAdapter();
			}
			@Override
			public Adapter caseBaseRequirement(BaseRequirement object) {
				return createBaseRequirementAdapter();
			}
			@Override
			public Adapter caseBaseArtefact(BaseArtefact object) {
				return createBaseArtefactAdapter();
			}
			@Override
			public Adapter caseBaseActivity(BaseActivity object) {
				return createBaseActivityAdapter();
			}
			@Override
			public Adapter caseBaseRequirementRel(BaseRequirementRel object) {
				return createBaseRequirementRelAdapter();
			}
			@Override
			public Adapter caseBaseRole(BaseRole object) {
				return createBaseRoleAdapter();
			}
			@Override
			public Adapter caseBaseApplicabilityLevel(BaseApplicabilityLevel object) {
				return createBaseApplicabilityLevelAdapter();
			}
			@Override
			public Adapter caseBaseCriticalityLevel(BaseCriticalityLevel object) {
				return createBaseCriticalityLevelAdapter();
			}
			@Override
			public Adapter caseBaseTechnique(BaseTechnique object) {
				return createBaseTechniqueAdapter();
			}
			@Override
			public Adapter caseBaseArtefactRel(BaseArtefactRel object) {
				return createBaseArtefactRelAdapter();
			}
			@Override
			public Adapter caseBaseCriticalityApplicability(BaseCriticalityApplicability object) {
				return createBaseCriticalityApplicabilityAdapter();
			}
			@Override
			public Adapter caseBaseActivityRel(BaseActivityRel object) {
				return createBaseActivityRelAdapter();
			}
			@Override
			public Adapter caseBaseAssurableElement(BaseAssurableElement object) {
				return createBaseAssurableElementAdapter();
			}
			@Override
			public Adapter caseBaseIndependencyLevel(BaseIndependencyLevel object) {
				return createBaseIndependencyLevelAdapter();
			}
			@Override
			public Adapter caseBaseRecommendationLevel(BaseRecommendationLevel object) {
				return createBaseRecommendationLevelAdapter();
			}
			@Override
			public Adapter caseBaseControlCategory(BaseControlCategory object) {
				return createBaseControlCategoryAdapter();
			}
			@Override
			public Adapter caseBaseApplicability(BaseApplicability object) {
				return createBaseApplicabilityAdapter();
			}
			@Override
			public Adapter caseBaseApplicabilityRel(BaseApplicabilityRel object) {
				return createBaseApplicabilityRelAdapter();
			}
			@Override
			public Adapter caseBaselineElement(BaselineElement object) {
				return createBaselineElementAdapter();
			}
			@Override
			public Adapter caseBaseEquivalenceMap(BaseEquivalenceMap object) {
				return createBaseEquivalenceMapAdapter();
			}
			@Override
			public Adapter caseBaseComplianceMap(BaseComplianceMap object) {
				return createBaseComplianceMapAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseDescribableElement(DescribableElement object) {
				return createDescribableElementAdapter();
			}
			@Override
			public Adapter caseMap(Map object) {
				return createMapAdapter();
			}
			@Override
			public Adapter caseEquivalenceMap(EquivalenceMap object) {
				return createEquivalenceMapAdapter();
			}
			@Override
			public Adapter caseComplianceMap(ComplianceMap object) {
				return createComplianceMapAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework <em>Base Framework</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework
	 * @generated
	 */
	public Adapter createBaseFrameworkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement <em>Base Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirement
	 * @generated
	 */
	public Adapter createBaseRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact <em>Base Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefact
	 * @generated
	 */
	public Adapter createBaseArtefactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity <em>Base Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity
	 * @generated
	 */
	public Adapter createBaseActivityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel <em>Base Requirement Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel
	 * @generated
	 */
	public Adapter createBaseRequirementRelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRole <em>Base Role</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRole
	 * @generated
	 */
	public Adapter createBaseRoleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel <em>Base Applicability Level</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel
	 * @generated
	 */
	public Adapter createBaseApplicabilityLevelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel <em>Base Criticality Level</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel
	 * @generated
	 */
	public Adapter createBaseCriticalityLevelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseTechnique <em>Base Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseTechnique
	 * @generated
	 */
	public Adapter createBaseTechniqueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel <em>Base Artefact Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel
	 * @generated
	 */
	public Adapter createBaseArtefactRelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability <em>Base Criticality Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability
	 * @generated
	 */
	public Adapter createBaseCriticalityApplicabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel <em>Base Activity Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel
	 * @generated
	 */
	public Adapter createBaseActivityRelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement <em>Base Assurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement
	 * @generated
	 */
	public Adapter createBaseAssurableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseIndependencyLevel <em>Base Independency Level</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseIndependencyLevel
	 * @generated
	 */
	public Adapter createBaseIndependencyLevelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRecommendationLevel <em>Base Recommendation Level</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRecommendationLevel
	 * @generated
	 */
	public Adapter createBaseRecommendationLevelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseControlCategory <em>Base Control Category</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseControlCategory
	 * @generated
	 */
	public Adapter createBaseControlCategoryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability <em>Base Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicability
	 * @generated
	 */
	public Adapter createBaseApplicabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel <em>Base Applicability Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel
	 * @generated
	 */
	public Adapter createBaseApplicabilityRelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaselineElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselineElement
	 * @generated
	 */
	public Adapter createBaselineElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap <em>Base Equivalence Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap
	 * @generated
	 */
	public Adapter createBaseEquivalenceMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap <em>Base Compliance Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap
	 * @generated
	 */
	public Adapter createBaseComplianceMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.infra.general.general.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.infra.general.general.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.infra.general.general.DescribableElement <em>Describable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.infra.general.general.DescribableElement
	 * @generated
	 */
	public Adapter createDescribableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.infra.mappings.mapping.Map <em>Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.infra.mappings.mapping.Map
	 * @generated
	 */
	public Adapter createMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap <em>Equivalence Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap
	 * @generated
	 */
	public Adapter createEquivalenceMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.infra.mappings.mapping.ComplianceMap <em>Compliance Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.infra.mappings.mapping.ComplianceMap
	 * @generated
	 */
	public Adapter createComplianceMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //BaselineAdapterFactory
