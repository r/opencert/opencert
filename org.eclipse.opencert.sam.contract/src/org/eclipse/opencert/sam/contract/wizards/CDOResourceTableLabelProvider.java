/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.wizards;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.emf.cdo.eresource.CDOResource;

import java.util.ArrayList;

public class CDOResourceTableLabelProvider extends LabelProvider implements ITableLabelProvider
{

	public Image getColumnImage(Object element, int index) {
		return null;
		}
	
	public String getColumnText(Object element, int index) {
		ContractModelElement cme = (ContractModelElement) element;
		CDOResource cdoResource = (CDOResource) cme.getCdoResource();
		EObject modelElement = (EObject) cme.getModelElement();
		switch (index) {
			case 0 :
				return (String) modelElement.eGet(modelElement.eClass().getEStructuralFeature("id"));
			case 1 :
				return (String) modelElement.eGet(modelElement.eClass().getEStructuralFeature("description"));
			case 2 :
				return (String) cdoResource.getPath();
			default :
				return "unknown " + index;
		}
	}
}
