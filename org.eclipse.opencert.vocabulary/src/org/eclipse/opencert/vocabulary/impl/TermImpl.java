/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.vocabulary.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.opencert.vocabulary.SourceOfDefinition;
import org.eclipse.opencert.vocabulary.Term;
import org.eclipse.opencert.vocabulary.VocabularyPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.TermImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.TermImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.TermImpl#getDefinitions <em>Definitions</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.TermImpl#getNotes <em>Notes</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.TermImpl#getExamples <em>Examples</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.TermImpl#getSynonyms <em>Synonyms</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.TermImpl#getDefinedBy <em>Defined By</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.TermImpl#getIsA <em>Is A</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.TermImpl#getHasA <em>Has A</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.TermImpl#getRefersTo <em>Refers To</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TermImpl extends EObjectImpl implements Term
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected static final String DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected String description = DESCRIPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getDefinitions() <em>Definitions</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinitions()
   * @generated
   * @ordered
   */
  protected EList<String> definitions;

  /**
   * The cached value of the '{@link #getNotes() <em>Notes</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNotes()
   * @generated
   * @ordered
   */
  protected EList<String> notes;

  /**
   * The cached value of the '{@link #getExamples() <em>Examples</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExamples()
   * @generated
   * @ordered
   */
  protected EList<String> examples;

  /**
   * The cached value of the '{@link #getSynonyms() <em>Synonyms</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSynonyms()
   * @generated
   * @ordered
   */
  protected EList<String> synonyms;

  /**
   * The cached value of the '{@link #getDefinedBy() <em>Defined By</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinedBy()
   * @generated
   * @ordered
   */
  protected SourceOfDefinition definedBy;

  /**
   * The cached value of the '{@link #getIsA() <em>Is A</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIsA()
   * @generated
   * @ordered
   */
  protected EList<Term> isA;

  /**
   * The cached value of the '{@link #getHasA() <em>Has A</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHasA()
   * @generated
   * @ordered
   */
  protected EList<Term> hasA;

  /**
   * The cached value of the '{@link #getRefersTo() <em>Refers To</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRefersTo()
   * @generated
   * @ordered
   */
  protected EList<Term> refersTo;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TermImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return VocabularyPackage.Literals.TERM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, VocabularyPackage.TERM__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDescription(String newDescription)
  {
    String oldDescription = description;
    description = newDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, VocabularyPackage.TERM__DESCRIPTION, oldDescription, description));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getDefinitions()
  {
    if (definitions == null)
    {
      definitions = new EDataTypeUniqueEList<String>(String.class, this, VocabularyPackage.TERM__DEFINITIONS);
    }
    return definitions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getNotes()
  {
    if (notes == null)
    {
      notes = new EDataTypeUniqueEList<String>(String.class, this, VocabularyPackage.TERM__NOTES);
    }
    return notes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getExamples()
  {
    if (examples == null)
    {
      examples = new EDataTypeUniqueEList<String>(String.class, this, VocabularyPackage.TERM__EXAMPLES);
    }
    return examples;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getSynonyms()
  {
    if (synonyms == null)
    {
      synonyms = new EDataTypeUniqueEList<String>(String.class, this, VocabularyPackage.TERM__SYNONYMS);
    }
    return synonyms;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SourceOfDefinition getDefinedBy()
  {
    if (definedBy != null && definedBy.eIsProxy())
    {
      InternalEObject oldDefinedBy = (InternalEObject)definedBy;
      definedBy = (SourceOfDefinition)eResolveProxy(oldDefinedBy);
      if (definedBy != oldDefinedBy)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, VocabularyPackage.TERM__DEFINED_BY, oldDefinedBy, definedBy));
      }
    }
    return definedBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SourceOfDefinition basicGetDefinedBy()
  {
    return definedBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefinedBy(SourceOfDefinition newDefinedBy)
  {
    SourceOfDefinition oldDefinedBy = definedBy;
    definedBy = newDefinedBy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, VocabularyPackage.TERM__DEFINED_BY, oldDefinedBy, definedBy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Term> getIsA()
  {
    if (isA == null)
    {
      isA = new EObjectResolvingEList<Term>(Term.class, this, VocabularyPackage.TERM__IS_A);
    }
    return isA;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Term> getHasA()
  {
    if (hasA == null)
    {
      hasA = new EObjectResolvingEList<Term>(Term.class, this, VocabularyPackage.TERM__HAS_A);
    }
    return hasA;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Term> getRefersTo()
  {
    if (refersTo == null)
    {
      refersTo = new EObjectResolvingEList<Term>(Term.class, this, VocabularyPackage.TERM__REFERS_TO);
    }
    return refersTo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case VocabularyPackage.TERM__NAME:
        return getName();
      case VocabularyPackage.TERM__DESCRIPTION:
        return getDescription();
      case VocabularyPackage.TERM__DEFINITIONS:
        return getDefinitions();
      case VocabularyPackage.TERM__NOTES:
        return getNotes();
      case VocabularyPackage.TERM__EXAMPLES:
        return getExamples();
      case VocabularyPackage.TERM__SYNONYMS:
        return getSynonyms();
      case VocabularyPackage.TERM__DEFINED_BY:
        if (resolve) return getDefinedBy();
        return basicGetDefinedBy();
      case VocabularyPackage.TERM__IS_A:
        return getIsA();
      case VocabularyPackage.TERM__HAS_A:
        return getHasA();
      case VocabularyPackage.TERM__REFERS_TO:
        return getRefersTo();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case VocabularyPackage.TERM__NAME:
        setName((String)newValue);
        return;
      case VocabularyPackage.TERM__DESCRIPTION:
        setDescription((String)newValue);
        return;
      case VocabularyPackage.TERM__DEFINITIONS:
        getDefinitions().clear();
        getDefinitions().addAll((Collection<? extends String>)newValue);
        return;
      case VocabularyPackage.TERM__NOTES:
        getNotes().clear();
        getNotes().addAll((Collection<? extends String>)newValue);
        return;
      case VocabularyPackage.TERM__EXAMPLES:
        getExamples().clear();
        getExamples().addAll((Collection<? extends String>)newValue);
        return;
      case VocabularyPackage.TERM__SYNONYMS:
        getSynonyms().clear();
        getSynonyms().addAll((Collection<? extends String>)newValue);
        return;
      case VocabularyPackage.TERM__DEFINED_BY:
        setDefinedBy((SourceOfDefinition)newValue);
        return;
      case VocabularyPackage.TERM__IS_A:
        getIsA().clear();
        getIsA().addAll((Collection<? extends Term>)newValue);
        return;
      case VocabularyPackage.TERM__HAS_A:
        getHasA().clear();
        getHasA().addAll((Collection<? extends Term>)newValue);
        return;
      case VocabularyPackage.TERM__REFERS_TO:
        getRefersTo().clear();
        getRefersTo().addAll((Collection<? extends Term>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case VocabularyPackage.TERM__NAME:
        setName(NAME_EDEFAULT);
        return;
      case VocabularyPackage.TERM__DESCRIPTION:
        setDescription(DESCRIPTION_EDEFAULT);
        return;
      case VocabularyPackage.TERM__DEFINITIONS:
        getDefinitions().clear();
        return;
      case VocabularyPackage.TERM__NOTES:
        getNotes().clear();
        return;
      case VocabularyPackage.TERM__EXAMPLES:
        getExamples().clear();
        return;
      case VocabularyPackage.TERM__SYNONYMS:
        getSynonyms().clear();
        return;
      case VocabularyPackage.TERM__DEFINED_BY:
        setDefinedBy((SourceOfDefinition)null);
        return;
      case VocabularyPackage.TERM__IS_A:
        getIsA().clear();
        return;
      case VocabularyPackage.TERM__HAS_A:
        getHasA().clear();
        return;
      case VocabularyPackage.TERM__REFERS_TO:
        getRefersTo().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case VocabularyPackage.TERM__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case VocabularyPackage.TERM__DESCRIPTION:
        return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
      case VocabularyPackage.TERM__DEFINITIONS:
        return definitions != null && !definitions.isEmpty();
      case VocabularyPackage.TERM__NOTES:
        return notes != null && !notes.isEmpty();
      case VocabularyPackage.TERM__EXAMPLES:
        return examples != null && !examples.isEmpty();
      case VocabularyPackage.TERM__SYNONYMS:
        return synonyms != null && !synonyms.isEmpty();
      case VocabularyPackage.TERM__DEFINED_BY:
        return definedBy != null;
      case VocabularyPackage.TERM__IS_A:
        return isA != null && !isA.isEmpty();
      case VocabularyPackage.TERM__HAS_A:
        return hasA != null && !hasA.isEmpty();
      case VocabularyPackage.TERM__REFERS_TO:
        return refersTo != null && !refersTo.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", description: ");
    result.append(description);
    result.append(", definitions: ");
    result.append(definitions);
    result.append(", notes: ");
    result.append(notes);
    result.append(", examples: ");
    result.append(examples);
    result.append(", synonyms: ");
    result.append(synonyms);
    result.append(')');
    return result.toString();
  }

} //TermImpl
