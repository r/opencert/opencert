/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface ArtefactEquivalenceMapPropertiesEditionPart {



	/**
	 * Init the equivalence
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initEquivalence(ReferencesTableSettings settings);

	/**
	 * Update the equivalence
	 * @param newValue the equivalence to update
	 * 
	 */
	public void updateEquivalence();

	/**
	 * Adds the given filter to the equivalence edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToEquivalence(ViewerFilter filter);

	/**
	 * Adds the given filter to the equivalence edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToEquivalence(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the equivalence table
	 * 
	 */
	public boolean isContainedInEquivalenceTable(EObject element);




	/**
	 * Init the artefactEquivalenceMap
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initArtefactEquivalenceMap(ReferencesTableSettings settings);

	/**
	 * Update the artefactEquivalenceMap
	 * @param newValue the artefactEquivalenceMap to update
	 * 
	 */
	public void updateArtefactEquivalenceMap();

	/**
	 * Adds the given filter to the artefactEquivalenceMap edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToArtefactEquivalenceMap(ViewerFilter filter);

	/**
	 * Adds the given filter to the artefactEquivalenceMap edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToArtefactEquivalenceMap(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the artefactEquivalenceMap table
	 * 
	 */
	public boolean isContainedInArtefactEquivalenceMapTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
