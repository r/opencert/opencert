/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.pkm.refframework.refframework.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkFactory;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

public class ComplianceMapNew extends Dialog {

	/**
	 * Settings for mapGroupModel ReferencesTable
	 */
	protected ReferencesTableSettings equivalenceMapModelSettings;
	protected RefEquivalenceMap equivalenceMap;
	protected MapGroup mapGrouplocal;
	protected EList <RefEquivalenceMap> lstequivalenceMap;
	protected RefAssurableElement assurableElement;
	
	protected ArrayList<String> refListMap;
	protected ArrayList<String> refListDirMap;
	
	protected Text idText;
	protected Text nameText;
	
	protected boolean bcheck = true;
	
	
	Map<Object, Object> options = new HashMap<Object, Object>();
		

	//public ComplianceMapNew(Shell parentShell, EObject object, MapGroup mapGroup,RefFramework baseFramework) {
	public ComplianceMapNew(Shell parentShell, EObject object, MapGroup mapGroup) {
		super(parentShell);

		try {
			
			//equivalenceMapModelSettings = new ReferencesTableSettings(baseFramework,RefframeworkPackage.eINSTANCE.getRefEquivalenceMap_Target());
			assurableElement = (RefAssurableElement)object;
			lstequivalenceMap = assurableElement.getEquivalence();
			mapGrouplocal = mapGroup;
			
			// TODO Auto-generated constructor stub
			setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX );

		}
		catch (Exception exception) {

			MessageDialog.openError(getShell(), "Not valid model file", exception.toString());
		}
	}

	public ComplianceMapNew(IShellProvider parentShell) {
		super(parentShell);
		// TODO Auto-generated constructor stub
	}


	@Override
	protected Control createDialogArea(final Composite parent) {

		refListDirMap = new ArrayList<String>();
		refListMap = new ArrayList<String>();

		final Composite contents = (Composite)super.createDialogArea(parent);

		GridLayout contentsGridLayout = (GridLayout)contents.getLayout();
		contentsGridLayout.numColumns = 2;

		GridData contentsGridData = (GridData)contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;

		// Label
		Label idLabel = new Label(contents, SWT.NONE);
		idLabel.setText("ID");
		GridData idLabelGridData = new GridData();
		idLabelGridData.horizontalAlignment = SWT.FILL;
		idLabelGridData.verticalAlignment = SWT.FILL;
		idLabel.setLayoutData(idLabelGridData);

		// Text
		idText = new Text(contents, SWT.NONE);
		GridData idTextGridData = new GridData();
		idTextGridData.horizontalAlignment = SWT.FILL;
		idTextGridData.verticalAlignment = SWT.FILL;
		idText.setLayoutData(idTextGridData);
		
		
		// Label
		Label nameLabel = new Label(contents, SWT.NONE);
		nameLabel.setText("Name");
		GridData nameLabelGridData = new GridData();
		nameLabelGridData.grabExcessHorizontalSpace = true;
		nameLabelGridData.minimumWidth = 20;
		nameLabelGridData.horizontalAlignment = SWT.FILL;
		nameLabelGridData.verticalAlignment = SWT.FILL;
		nameLabel.setLayoutData(nameLabelGridData);

		// Text
		nameText = new Text(contents, SWT.NONE);
		GridData nameTextGridData = new GridData();
		nameTextGridData.grabExcessHorizontalSpace = true;
		nameTextGridData.minimumWidth = 20;
		nameTextGridData.horizontalAlignment = SWT.FILL;
		nameTextGridData.verticalAlignment = SWT.FILL;
		nameText.setLayoutData(nameTextGridData);
		
		return contents;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Selection dialog");
	}

	@Override
	protected void okPressed()
	{
		equivalenceMap = RefframeworkFactory.eINSTANCE.createRefEquivalenceMap();
		equivalenceMap.setId(idText.getText());
		equivalenceMap.setName(nameText.getText());
		equivalenceMap.setMapGroup(mapGrouplocal);
				
		
		//Before to add, verify that the new group is unique (id and name)
		Iterator <RefEquivalenceMap> iter  = lstequivalenceMap.iterator();
		while (iter.hasNext()) {
			RefEquivalenceMap element = iter.next();
			
			if (CDOUtil.getCDOObject(element).cdoID().equals(CDOUtil.getCDOObject(equivalenceMap).cdoID())) {
				bcheck = false;
			}	
		}
		
		if (bcheck){
			ReferencesTableSettings objectEquivalenceMapSettings = new ReferencesTableSettings(assurableElement, RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence());
			
			if (objectEquivalenceMapSettings.contains(equivalenceMap)) {

			} else {
				objectEquivalenceMapSettings.addToReference(equivalenceMap);	
			}
			
			try {				
				objectEquivalenceMapSettings.getSource().eResource().save(options);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			
			
			/*complianceMapModelSettings.addToReference(complianceMap);
			try {
				complianceMapModelSettings.getSource().eResource().save(options);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}	
		super.okPressed();
	}

	@Override
	public boolean close()
	{
		return super.close();
	}
	public Boolean getResult()
	{
		return bcheck;
	}
	
	

}
