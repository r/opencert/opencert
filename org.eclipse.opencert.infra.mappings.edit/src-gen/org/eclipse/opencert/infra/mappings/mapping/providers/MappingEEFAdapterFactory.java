/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.mappings.mapping.providers;

import org.eclipse.emf.common.notify.Adapter;

import org.eclipse.opencert.infra.mappings.mapping.util.MappingAdapterFactory;

/**
 * 
 * 
 */
public class MappingEEFAdapterFactory extends MappingAdapterFactory {

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.infra.mappings.mapping.util.MappingAdapterFactory#createMapModelAdapter()
	 * 
	 */
	public Adapter createMapModelAdapter() {
		return new MapModelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.infra.mappings.mapping.util.MappingAdapterFactory#createMapGroupAdapter()
	 * 
	 */
	public Adapter createMapGroupAdapter() {
		return new MapGroupPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.infra.mappings.mapping.util.MappingAdapterFactory#createMapAdapter()
	 * 
	 */
	public Adapter createMapAdapter() {
		return new MapPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.infra.mappings.mapping.util.MappingAdapterFactory#createMapJustificationAdapter()
	 * 
	 */
	public Adapter createMapJustificationAdapter() {
		return new MapJustificationPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.infra.mappings.mapping.util.MappingAdapterFactory#createComplianceMapAdapter()
	 * 
	 */
	public Adapter createComplianceMapAdapter() {
		return new ComplianceMapPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.infra.mappings.mapping.util.MappingAdapterFactory#createEquivalenceMapAdapter()
	 * 
	 */
	public Adapter createEquivalenceMapAdapter() {
		return new EquivalenceMapPropertiesEditionProvider();
	}

}
