/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.forms;

// Start of user code for imports
import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;
import org.eclipse.opencert.evm.evidspec.evidence.util.ImageUtils;
import org.eclipse.opencert.evm.evidspec.evidence.util.SVNTreeDialog;
import org.eclipse.opencert.infra.svnkit.MainClass;
import org.osgi.framework.Bundle;

// End of user code

/**
 * 
 * 
 */
public class ResourcePropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ResourcePropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text location;
	protected Text format;

	// StartALC
	protected Label repoURL;
	MainClass validateInstance =null;
	// End ALC

	// Start IRR
	protected Button editLocation;
	public ImageUtils imageUtil;

	protected Button committButton;
	protected Button asignButton;
	protected Button deleteButton;
	protected Button openButton;

	protected Table tableHistory;

	protected Program programExplorer;

	protected String sURL;
	protected String sUser;
	protected String sPassword;
	protected String sLocalPath;

	protected boolean useLocalRepository;

	// End IRR

	/**
	 * For {@link ISection} use only.
	 */
	public ResourcePropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ResourcePropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		
		// Start IRR **/
		imageUtil = new ImageUtils();
		// End IRR **/
		
		CompositionSequence resourceStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = resourceStep.addStep(EvidenceViewsRepository.Resource.Properties.class);
		propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.id);
		propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.name);
		propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.description);
		//Start IRR
		propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.format);
		//End IRR
		propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.location);
		
		//propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.format);
		
		
		composer = new PartComposer(resourceStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.Resource.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == EvidenceViewsRepository.Resource.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == EvidenceViewsRepository.Resource.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == EvidenceViewsRepository.Resource.Properties.description) {
					return createDescriptionText(widgetFactory, parent);
				}
				if (key == EvidenceViewsRepository.Resource.Properties.location) {
					return createLocationText(widgetFactory, parent);
				}
				if (key == EvidenceViewsRepository.Resource.Properties.format) {
					return createFormatText(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(EvidenceMessages.ResourcePropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		//Start IRR
		//propertiesGroupLayout.numColumns = 3;
		propertiesGroupLayout.numColumns = 1; 
		//End IRR
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		
		// Start IRR
		
		Composite composite = new Composite (parent , SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			composite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 4;
			layout.makeColumnsEqualWidth = true;
			composite.setLayout(layout);
		}
		
		
		createDescription(composite, EvidenceViewsRepository.Resource.Properties.id, EvidenceMessages.ResourcePropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(composite, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(composite);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		idData.horizontalSpan = 3; //2;
		id.setLayoutData(idData);
		
		// createDescription(parent, EvidenceViewsRepository.Resource.Properties.id, EvidenceMessages.ResourcePropertiesEditionPart_IdLabel);
		// id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		// id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		// widgetFactory.paintBordersFor(parent);
		// GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		// id.setLayoutData(idData);
		
		// End IRR
		
		
		
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ResourcePropertiesEditionPartForm.this,
							EvidenceViewsRepository.Resource.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ResourcePropertiesEditionPartForm.this,
									EvidenceViewsRepository.Resource.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ResourcePropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, EvidenceViewsRepository.Resource.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		
		// Start IRR
		// Comento la siguiente l�nea porque me genera una columna vacia en el layout que no estoy usando
		// FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Resource.Properties.id, EvidenceViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// End IRR
		
		//Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		
		// Start IRR
		
		Composite composite = new Composite (parent , SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			composite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 4;
			layout.makeColumnsEqualWidth = true;
			composite.setLayout(layout);
		}
		
		createDescription(composite, EvidenceViewsRepository.Resource.Properties.name, EvidenceMessages.ResourcePropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(composite, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(composite);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		nameData.horizontalSpan = 3; //2;
		name.setLayoutData(nameData);
		
		
		// createDescription(parent, EvidenceViewsRepository.Resource.Properties.name, EvidenceMessages.ResourcePropertiesEditionPart_NameLabel);
		// name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		// name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		// widgetFactory.paintBordersFor(parent);
		// GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		// name.setLayoutData(nameData);
		
		// End IRR
		
		
		
		
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ResourcePropertiesEditionPartForm.this,
							EvidenceViewsRepository.Resource.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ResourcePropertiesEditionPartForm.this,
									EvidenceViewsRepository.Resource.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ResourcePropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, EvidenceViewsRepository.Resource.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		// Start IRR
		// FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Resource.Properties.name, EvidenceViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// End IRR
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(FormToolkit widgetFactory, Composite parent) {
		
		// Start IRR
		
		Composite composite = new Composite (parent , SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			composite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 4;
			layout.makeColumnsEqualWidth = true;
			composite.setLayout(layout);
		}
		
		
		createDescription(composite, EvidenceViewsRepository.Resource.Properties.description, EvidenceMessages.ResourcePropertiesEditionPart_DescriptionLabel);
		description = widgetFactory.createText(composite, ""); //$NON-NLS-1$
		description.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(composite);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionData.horizontalSpan = 3;//2;
		description.setLayoutData(descriptionData);
		
		
		//createDescription(parent, EvidenceViewsRepository.Resource.Properties.description, EvidenceMessages.ResourcePropertiesEditionPart_DescriptionLabel);
		//description = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		//description.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		//widgetFactory.paintBordersFor(parent);
		//GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		//description.setLayoutData(descriptionData);
		// End IRR
		
		
		description.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ResourcePropertiesEditionPartForm.this,
							EvidenceViewsRepository.Resource.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ResourcePropertiesEditionPartForm.this,
									EvidenceViewsRepository.Resource.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ResourcePropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		description.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}
		});
		EditingUtils.setID(description, EvidenceViewsRepository.Resource.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		
		// Start IRR
		// FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Resource.Properties.description, EvidenceViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// End IRR
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	
	
	private void openFileBrowser() {
		// File standard dialog
	    FileDialog fileDialog = new FileDialog(Display.getDefault().getActiveShell());
	    // Set the text
	    fileDialog.setText("Select Artefact File");
	    // Set filter path
	    if(useLocalRepository){
	    	fileDialog.setFilterPath(getLocalPath());
	    }
	    // Set filter on .txt files
	    //fileDialog.setFilterExtensions(new String[] { "*.txt" });
	    // Put in a readable name for the filter
	    //fileDialog.setFilterNames(new String[] { "Textfiles(*.txt)" });
	    // Open Dialog and save result of selection
	    String selected = fileDialog.open();
	    if (selected != null) 
	    	location.setText(selected);
	    
	    if (propertiesEditionComponent != null)
			propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
		setHasChanged(true);
	}
	
	
	protected Composite createLocationText(FormToolkit widgetFactory, Composite parent) {
		
		// Start IRR
		
		Composite composite = new Composite (parent , SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			composite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 4;
			layout.makeColumnsEqualWidth = true;
			composite.setLayout(layout);
		}
		
		
		
		
		
		
		Composite locationcomposite = new Composite (parent , SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			locationcomposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 4;
			locationcomposite.setLayout(layout);
		}
		
		
		//Start ALC
				repoURL = widgetFactory.createLabel(composite, sURL);		
				Font font = new Font(null,"Arial", 11, SWT.BOLD);
				repoURL.setFont(font);
				
				GridData labelData = new GridData(GridData.FILL_HORIZONTAL);
				labelData.horizontalSpan = 3;
				repoURL.setLayoutData(labelData);
				//End ALC
		
		
		//createDescription(parent, EvidenceViewsRepository.Resource.Properties.location, EvidenceMessages.ResourcePropertiesEditionPart_LocationLabel);
		//location = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		//location.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		//widgetFactory.paintBordersFor(parent);
		//GridData locationData = new GridData(GridData.FILL_HORIZONTAL);
		//location.setLayoutData(locationData);
		
		createDescription(locationcomposite, EvidenceViewsRepository.Resource.Properties.location, EvidenceMessages.ResourcePropertiesEditionPart_LocationLabel);
		location = widgetFactory.createText(locationcomposite, ""); //$NON-NLS-1$
		location.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(locationcomposite);
		GridData locationData = new GridData(GridData.FILL_HORIZONTAL);
		locationData.horizontalSpan = 2;
		location.setLayoutData(locationData);
		// End IRR		
		
		
		location.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ResourcePropertiesEditionPartForm.this,
							EvidenceViewsRepository.Resource.Properties.location,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ResourcePropertiesEditionPartForm.this,
									EvidenceViewsRepository.Resource.Properties.location,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, location.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ResourcePropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		location.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
				}
			}
		});
		
		
		// Start ALC
		location.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				// TODO Auto-generated method stub
				// getRepositoryHandler();
				readHistory();
			}
		});
		// End ALC

		// Start IRR
		editLocation = new Button(locationcomposite, SWT.NONE);
		editLocation.setText("...");
		GridData editLocationData = new GridData();
		editLocation.setLayoutData(editLocationData);

		editLocation.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */

			public void widgetSelected(SelectionEvent e) {

				openFileBrowser();

			}

		});

		// Create buttond for the repository
		createSVNReportButtons(widgetFactory, parent);

		// Create table for repository history
		createSVNReportTableHistory(widgetFactory, parent);
		// End IRR										
		
		
		
		EditingUtils.setID(location, EvidenceViewsRepository.Resource.Properties.location);
		EditingUtils.setEEFtype(location, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Resource.Properties.location, EvidenceViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createLocationText

		// End of user code
		return parent;
	}

	
	protected Composite createFormatText(FormToolkit widgetFactory, Composite parent) {
		
		// Start IRR
		
		Composite composite = new Composite (parent , SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			composite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 4;
			layout.makeColumnsEqualWidth = true;
			composite.setLayout(layout);
		}
		
		
		createDescription(composite, EvidenceViewsRepository.Resource.Properties.format, EvidenceMessages.ResourcePropertiesEditionPart_FormatLabel);
		format = widgetFactory.createText(composite, ""); //$NON-NLS-1$
		format.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(composite);
		GridData formatData = new GridData(GridData.FILL_HORIZONTAL);
		formatData.horizontalSpan = 3; //2;
		format.setLayoutData(formatData);
		
		
		//createDescription(parent, EvidenceViewsRepository.Resource.Properties.format, EvidenceMessages.ResourcePropertiesEditionPart_FormatLabel);
		//format = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		//format.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		//widgetFactory.paintBordersFor(parent);
		//GridData formatData = new GridData(GridData.FILL_HORIZONTAL);
		//format.setLayoutData(formatData);
		
		// End IRR
		
		
		format.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ResourcePropertiesEditionPartForm.this,
							EvidenceViewsRepository.Resource.Properties.format,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, format.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ResourcePropertiesEditionPartForm.this,
									EvidenceViewsRepository.Resource.Properties.format,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, format.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ResourcePropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		format.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.format, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, format.getText()));
				}
			}
		});
		EditingUtils.setID(format, EvidenceViewsRepository.Resource.Properties.format);
		EditingUtils.setEEFtype(format, "eef::Text"); //$NON-NLS-1$
		//FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Resource.Properties.format, EvidenceViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createFormatText

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		Shell thisS= this.container.getShell();
		MessageDialog.openInformation(thisS, "Confirm", "Resource Changed");
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Resource.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(EvidenceMessages.Resource_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Resource.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(EvidenceMessages.Resource_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Resource.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(EvidenceMessages.Resource_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#getLocation()
	 * 
	 */
	public String getLocation() {
		return location.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#setLocation(String newValue)
	 * 
	 */
	public void setLocation(String newValue) {
		if (newValue != null) {
			location.setText(newValue);
		} else {
			location.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Resource.Properties.location);
		if (eefElementEditorReadOnlyState && location.isEnabled()) {
			location.setEnabled(false);
			location.setToolTipText(EvidenceMessages.Resource_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !location.isEnabled()) {
			location.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#getFormat()
	 * 
	 */
	public String getFormat() {
		return format.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#setFormat(String newValue)
	 * 
	 */
	public void setFormat(String newValue) {
		if (newValue != null) {
			format.setText(newValue);
		} else {
			format.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Resource.Properties.format);
		if (eefElementEditorReadOnlyState && format.isEnabled()) {
			format.setEnabled(false);
			format.setToolTipText(EvidenceMessages.Resource_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !format.isEnabled()) {
			format.setEnabled(true);
		}	
		
	}


	
	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.Resource_Part_Title;
	}

	// Start IRR
	
	@Override
	public void setUrl(String newvalue) {
		if (newvalue != null) {
			sURL = newvalue;
		} else {
			sURL = "";
		}
	}

	@Override
	public String getUrl() {
		 return sURL;
	}

	@Override
    public void setUser(String newvalue) {
          if (newvalue != null) {
                 sUser= newvalue;
          } else {
                 sUser = "";
          }
    }

    @Override
    public String getUser() {
          return sUser;
    }

    @Override
    public void setPassword(String newvalue) {
          if (newvalue != null) {
                 sPassword= newvalue;
          } else {
                 sPassword = "";
          }
    }

    @Override
    public String getPassword() {
          return sPassword;
    }

    @Override
    public void setLocalPath(String newvalue) {
          if (newvalue != null) {
                 sLocalPath= newvalue;
          } else {
                 sLocalPath = "";
          }      
    }

    @Override
    public String getLocalPath() {
                 return sLocalPath;
    }


    @Override
    public void setUseLocalRepository(boolean newvalue) {         
    	useLocalRepository= newvalue;            
    }

    @Override
    public boolean getUseLocalRepository() {
           return useLocalRepository;
    }

	@Override
	public void connectRepo() {
		// Begin ALC
		try {
			if (useLocalRepository) {
				repoURL.setText(getLocalPath());
			} else {
				repoURL.setText(getUrl());
			}
			// Avoid reconecctions
			if (validateInstance == null && !useLocalRepository) {
				Bundle svnBundle = Platform
						.getBundle("org.eclipse.opencert.infra.svnkit");
				Class validateClass = svnBundle
						.loadClass("org.eclipse.opencert.infra.svnkit.MainClass");
				validateInstance = (MainClass) validateClass.newInstance();
				validateInstance.connectRepository(getUrl(), getUser(),
						getPassword());
				readHistory();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// End ALC

	}

	// End IRR
	
	// Start of user code additional methods
	
protected Composite createSVNReportButtons(FormToolkit widgetFactory, Composite parent) {
		
		Composite buttoncomposite = new Composite (parent , SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			buttoncomposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 4;
			//layout.makeColumnsEqualWidth = true;
			buttoncomposite.setLayout(layout);
		}
		
		
		
		
		committButton = widgetFactory.createButton(buttoncomposite, "Commit", 0);
		committButton.setSize(30, 10);
		asignButton = widgetFactory.createButton(buttoncomposite, "Asign", 0);
		asignButton.setSize(30, 10);
		deleteButton = widgetFactory.createButton(buttoncomposite, "Delete", 0);
		deleteButton.setSize(30, 10);
		openButton = widgetFactory.createButton(buttoncomposite, "Open", 0);
		openButton.setSize(30, 10);
		
		GridData committButtonData = new GridData(GridData.FILL_HORIZONTAL);
		committButtonData.horizontalSpan = 1;
		committButton.setLayoutData(committButtonData);
		asignButton.setLayoutData(committButtonData);
		deleteButton.setLayoutData(committButtonData);
		openButton.setLayoutData(committButtonData);
		
		committButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
			
				//Begin ALC
			/*	EObject thisEvidence = PropertiesContextService.getInstance()
						.lastElement();*/
				
				if(location.getText()!=null && !location.getText().equals("") && !useLocalRepository){
				
					//String resultMessage="";
					//Check the file is in local file sytem
					File local = new File(location.getText());
					if(local.exists()){
						SVNTreeDialog dialog = new SVNTreeDialog(
								Display.getDefault().getActiveShell(),
								new AdapterFactoryLabelProvider(adapterFactory),
								validateInstance,
								true,
								location.getText(), "Select the destination folder", imageUtil);
						String destinationFolder="";
						if (dialog.open() == Window.OK) {

							//Upload the file to the folder
							destinationFolder = dialog.getResult();
							

							String result= validateInstance.addFileToRepo(location.getText(), destinationFolder, "New artefact " + name.getText());
							if (result.contains("exists")){
								int ax = JOptionPane.showConfirmDialog(null, "Selected file exists in the repository. Do you want to update it?");
								if(ax == JOptionPane.YES_OPTION){
									result=validateInstance.changeRepoFile(location.getText(), destinationFolder, "Update artefact " + name.getText());
									if(!result.contains("error")){
										location.setText(result);
										result="Update Done!";
										if (propertiesEditionComponent != null)
											propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
										setHasChanged(true);
									}								
								}
								else{
									result="Commit Canceled!";
								}
							}
							else if(!result.contains("error")){
								location.setText(result);	
								result="Commit Done!";
								if (propertiesEditionComponent != null)
									propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
								setHasChanged(true);
							}						

							JOptionPane.showMessageDialog(null,
									result,
									"Commit",
									JOptionPane.INFORMATION_MESSAGE);							
						}					
					}
				}
				//End ALC								
			}
			
			
		});
		
		asignButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				//getRepositoryHandler();
				if(useLocalRepository){
					openFileBrowser();									
				}
				else{
					SVNTreeDialog dialog = new SVNTreeDialog(
							Display.getDefault().getActiveShell(),
							new AdapterFactoryLabelProvider(adapterFactory),
							validateInstance,
							false,
							location.getText(), "Select file from repository to assign", imageUtil);
					//String destinationFolder="";
					if (dialog.open() == Window.OK) {

						//Upload the file to the folder
						location.setText(dialog.getResult());					
						if (propertiesEditionComponent != null)
							propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
						setHasChanged(true);
					}				
				}
			}

		});
		
		
		deleteButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if(location.getText()!=null && !location.getText().equals("")){
					if(useLocalRepository){
						int ax = JOptionPane.showConfirmDialog(null, "The file will be deleted from the local hard disk. Are you sure?");
						if(ax == JOptionPane.YES_OPTION){
							File localFile= new File(location.getText());
							localFile.delete();
							location.setText("");
							if (propertiesEditionComponent != null)
								propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
							setHasChanged(true);
						}
					}
					else{

						int ax = JOptionPane.showConfirmDialog(null, "The file will be deleted from the repository. Are you sure?");
						if(ax == JOptionPane.YES_OPTION){
							validateInstance.deleteFile(location.getText());
							location.setText("");	
							if (propertiesEditionComponent != null)
								propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartForm.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
							setHasChanged(true);

						}		



						/*JOptionPane.showMessageDialog(null,
								"delete",
								"delete",
								JOptionPane.INFORMATION_MESSAGE); */
					}
				}			
			}
			

		});
		

		openButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				String selection ="";
				if(location.getText()!=null && !location.getText().equals("")){
					if(useLocalRepository){					
						selection = location.getText();											
					}
					else{					
						selection= validateInstance.getFileFromRepo(location.getText());				
					}
					int dot = selection.lastIndexOf('.');
					if (dot != -1) {
						String extension = selection.substring(dot);
						Program program = Program.findProgram(extension);
						if (program != null) {
							Program.launch(selection);
						} else {
							launchExplorer(selection);
						}
					} else if ("".equals(selection)) {
						// most likely no SVN repository, open in browser
						launchExplorer(location.getText());
					} else {
						launchExplorer(selection);
					}
				}
			}
		});				
		
		return parent;
	}
	
	
	protected Composite createSVNReportTableHistory(FormToolkit widgetFactory, Composite parent) {
		
		Composite Tablecomposite = new Composite (parent , SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			Tablecomposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1;
			Tablecomposite.setLayout(layout);
		}
		
		
		
		tableHistory = widgetFactory.createTable(Tablecomposite,  SWT.BORDER | SWT.MULTI);
				
		tableHistory.setLinesVisible (true);
		tableHistory.setHeaderVisible (true);
		
				
		GridData tableData = new GridData(GridData.FILL_BOTH);
		tableData.horizontalSpan = 1;
		tableHistory.setLayoutData(tableData);
		tableHistory.setItemCount (10);
		
		TableColumn col1 = new TableColumn(tableHistory, SWT.NONE);
		col1.setWidth(60);
		col1.setText("Revision"); //$NON-NLS-1$

		TableColumn col2 = new TableColumn(tableHistory, SWT.NONE);
		col2.setWidth(120);
		col2.setText("Date"); //$NON-NLS-1$
	
		
		TableColumn col3 = new TableColumn(tableHistory, SWT.NONE);
		col3.setWidth(80);
		col3.setText("Author");
		
		TableColumn col4 = new TableColumn(tableHistory, SWT.NONE);
		col4.setWidth(180);
		col4.setText("Comment");
						
		
		return parent;
	}

	private void readHistory() {
		//Begin ALC		
		if(location!= null && !useLocalRepository && validateInstance!=null){
			if(location.getText()!=null && !location.getText().equals("")){
				tableHistory.removeAll();
				ArrayList<String> entriesList=validateInstance.getFileHistory(location.getText());
				for (int i = 0; i < entriesList.size(); i++) {
					String allData = entriesList.get(i);
					TableItem tableItem = new TableItem(tableHistory,i);
					String []parts=allData.split("#");
					for (int j = 0; j < parts.length; j++) {
						tableItem.setText(j,parts[j]);
					}							
				}
			}	
			else{
				tableHistory.removeAll();
				tableHistory.setItemCount (10);
			}
		}
		//End ALC
	}
	
	public void launchExplorer(String param) {
		Program.launch(param);
	}		
	
	// End of user code


}
