/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.junit.Assert;
import org.junit.Test;

import com.google.gson.JsonObject;

public class EObjectToJsonTest {

	@Test
	public void testOutsideResource() {
		JsonObject json = EObjectToJson.INSTANCE.convert(TestData.createComponent());
		Assert.assertNotNull(json);
		System.out.println(json.toString());
	}

	@Test
	public void testInResource() {
		JsonObject json = EObjectToJson.INSTANCE.convert(TestData.createComponent(true));
		Assert.assertNotNull(json);
		System.out.println(json.toString());
	}
	
	@Test
	public void testUML() {
		Iterator<EObject> it = TestData.uml();
		EObject elem = it.next();
		JsonObject json = EObjectToJson.INSTANCE.convert(elem);
		Assert.assertNotNull(json);
		System.out.println(json.toString());
	}
}
