/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.infra.mappings.mapping;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.infra.mappings.mapping.MapModel#getMapGroupModel <em>Map Group Model</em>}</li>
 *   <li>{@link org.eclipse.opencert.infra.mappings.mapping.MapModel#getMapModel <em>Map Model</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.infra.mappings.mapping.MappingPackage#getMapModel()
 * @model
 * @generated
 */
public interface MapModel extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Map Group Model</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.infra.mappings.mapping.MapGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Map Group Model</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Map Group Model</em>' containment reference list.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MappingPackage#getMapModel_MapGroupModel()
	 * @model containment="true"
	 * @generated
	 */
	EList<MapGroup> getMapGroupModel();

	/**
	 * Returns the value of the '<em><b>Map Model</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.infra.mappings.mapping.Map}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Map Model</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Map Model</em>' containment reference list.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MappingPackage#getMapModel_MapModel()
	 * @model containment="true"
	 * @generated
	 */
	EList<Map> getMapModel();

} // MapModel
