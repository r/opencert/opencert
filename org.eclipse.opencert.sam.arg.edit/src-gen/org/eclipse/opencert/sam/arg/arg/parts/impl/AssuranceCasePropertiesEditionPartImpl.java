/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;
import org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart;

import org.eclipse.opencert.sam.arg.arg.providers.ArgMessages;

// End of user code

/**
 * 
 * 
 */
public class AssuranceCasePropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, AssuranceCasePropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected ReferencesTable hasArgument;
	protected List<ViewerFilter> hasArgumentBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> hasArgumentFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable hasEvidence;
	protected List<ViewerFilter> hasEvidenceBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> hasEvidenceFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable composedOf;
	protected List<ViewerFilter> composedOfBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> composedOfFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public AssuranceCasePropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence assuranceCaseStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = assuranceCaseStep.addStep(ArgViewsRepository.AssuranceCase.Properties.class);
		propertiesStep.addStep(ArgViewsRepository.AssuranceCase.Properties.id);
		propertiesStep.addStep(ArgViewsRepository.AssuranceCase.Properties.name);
		propertiesStep.addStep(ArgViewsRepository.AssuranceCase.Properties.hasArgument);
		propertiesStep.addStep(ArgViewsRepository.AssuranceCase.Properties.hasEvidence);
		propertiesStep.addStep(ArgViewsRepository.AssuranceCase.Properties.composedOf);
		
		
		composer = new PartComposer(assuranceCaseStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ArgViewsRepository.AssuranceCase.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == ArgViewsRepository.AssuranceCase.Properties.id) {
					return createIdText(parent);
				}
				if (key == ArgViewsRepository.AssuranceCase.Properties.name) {
					return createNameText(parent);
				}
				if (key == ArgViewsRepository.AssuranceCase.Properties.hasArgument) {
					return createHasArgumentAdvancedTableComposition(parent);
				}
				if (key == ArgViewsRepository.AssuranceCase.Properties.hasEvidence) {
					return createHasEvidenceAdvancedTableComposition(parent);
				}
				if (key == ArgViewsRepository.AssuranceCase.Properties.composedOf) {
					return createComposedOfAdvancedReferencesTable(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(ArgMessages.AssuranceCasePropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, ArgViewsRepository.AssuranceCase.Properties.id, ArgMessages.AssuranceCasePropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, ArgViewsRepository.AssuranceCase.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.AssuranceCase.Properties.id, ArgViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, ArgViewsRepository.AssuranceCase.Properties.name, ArgMessages.AssuranceCasePropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, ArgViewsRepository.AssuranceCase.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.AssuranceCase.Properties.name, ArgViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createHasArgumentAdvancedTableComposition(Composite parent) {
		this.hasArgument = new ReferencesTable(getDescription(ArgViewsRepository.AssuranceCase.Properties.hasArgument, ArgMessages.AssuranceCasePropertiesEditionPart_HasArgumentLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.hasArgument, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				hasArgument.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.hasArgument, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				hasArgument.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.hasArgument, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				hasArgument.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.hasArgument, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				hasArgument.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.hasArgumentFilters) {
			this.hasArgument.addFilter(filter);
		}
		this.hasArgument.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.AssuranceCase.Properties.hasArgument, ArgViewsRepository.SWT_KIND));
		this.hasArgument.createControls(parent);
		this.hasArgument.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.hasArgument, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData hasArgumentData = new GridData(GridData.FILL_HORIZONTAL);
		hasArgumentData.horizontalSpan = 3;
		this.hasArgument.setLayoutData(hasArgumentData);
		this.hasArgument.setLowerBound(0);
		this.hasArgument.setUpperBound(-1);
		hasArgument.setID(ArgViewsRepository.AssuranceCase.Properties.hasArgument);
		hasArgument.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createHasArgumentAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createHasEvidenceAdvancedTableComposition(Composite parent) {
		this.hasEvidence = new ReferencesTable(getDescription(ArgViewsRepository.AssuranceCase.Properties.hasEvidence, ArgMessages.AssuranceCasePropertiesEditionPart_HasEvidenceLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.hasEvidence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				hasEvidence.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.hasEvidence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				hasEvidence.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.hasEvidence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				hasEvidence.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.hasEvidence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				hasEvidence.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.hasEvidenceFilters) {
			this.hasEvidence.addFilter(filter);
		}
		this.hasEvidence.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.AssuranceCase.Properties.hasEvidence, ArgViewsRepository.SWT_KIND));
		this.hasEvidence.createControls(parent);
		this.hasEvidence.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.hasEvidence, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData hasEvidenceData = new GridData(GridData.FILL_HORIZONTAL);
		hasEvidenceData.horizontalSpan = 3;
		this.hasEvidence.setLayoutData(hasEvidenceData);
		this.hasEvidence.setLowerBound(0);
		this.hasEvidence.setUpperBound(-1);
		hasEvidence.setID(ArgViewsRepository.AssuranceCase.Properties.hasEvidence);
		hasEvidence.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createHasEvidenceAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createComposedOfAdvancedReferencesTable(Composite parent) {
		String label = getDescription(ArgViewsRepository.AssuranceCase.Properties.composedOf, ArgMessages.AssuranceCasePropertiesEditionPart_ComposedOfLabel);		 
		this.composedOf = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addComposedOf(); }
			public void handleEdit(EObject element) { editComposedOf(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveComposedOf(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromComposedOf(element); }
			public void navigateTo(EObject element) { }
		});
		this.composedOf.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.AssuranceCase.Properties.composedOf, ArgViewsRepository.SWT_KIND));
		this.composedOf.createControls(parent);
		this.composedOf.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.composedOf, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData composedOfData = new GridData(GridData.FILL_HORIZONTAL);
		composedOfData.horizontalSpan = 3;
		this.composedOf.setLayoutData(composedOfData);
		this.composedOf.disableMove();
		composedOf.setID(ArgViewsRepository.AssuranceCase.Properties.composedOf);
		composedOf.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addComposedOf() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(composedOf.getInput(), composedOfFilters, composedOfBusinessFilters,
		"composedOf", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.composedOf,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				composedOf.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveComposedOf(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.composedOf, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		composedOf.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromComposedOf(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceCasePropertiesEditionPartImpl.this, ArgViewsRepository.AssuranceCase.Properties.composedOf, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		composedOf.refresh();
	}

	/**
	 * 
	 */
	protected void editComposedOf(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				composedOf.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.AssuranceCase.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(ArgMessages.AssuranceCase_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.AssuranceCase.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(ArgMessages.AssuranceCase_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#initHasArgument(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initHasArgument(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		hasArgument.setContentProvider(contentProvider);
		hasArgument.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.AssuranceCase.Properties.hasArgument);
		if (eefElementEditorReadOnlyState && hasArgument.isEnabled()) {
			hasArgument.setEnabled(false);
			hasArgument.setToolTipText(ArgMessages.AssuranceCase_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !hasArgument.isEnabled()) {
			hasArgument.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#updateHasArgument()
	 * 
	 */
	public void updateHasArgument() {
	hasArgument.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#addFilterHasArgument(ViewerFilter filter)
	 * 
	 */
	public void addFilterToHasArgument(ViewerFilter filter) {
		hasArgumentFilters.add(filter);
		if (this.hasArgument != null) {
			this.hasArgument.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#addBusinessFilterHasArgument(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToHasArgument(ViewerFilter filter) {
		hasArgumentBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#isContainedInHasArgumentTable(EObject element)
	 * 
	 */
	public boolean isContainedInHasArgumentTable(EObject element) {
		return ((ReferencesTableSettings)hasArgument.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#initHasEvidence(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initHasEvidence(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		hasEvidence.setContentProvider(contentProvider);
		hasEvidence.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.AssuranceCase.Properties.hasEvidence);
		if (eefElementEditorReadOnlyState && hasEvidence.isEnabled()) {
			hasEvidence.setEnabled(false);
			hasEvidence.setToolTipText(ArgMessages.AssuranceCase_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !hasEvidence.isEnabled()) {
			hasEvidence.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#updateHasEvidence()
	 * 
	 */
	public void updateHasEvidence() {
	hasEvidence.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#addFilterHasEvidence(ViewerFilter filter)
	 * 
	 */
	public void addFilterToHasEvidence(ViewerFilter filter) {
		hasEvidenceFilters.add(filter);
		if (this.hasEvidence != null) {
			this.hasEvidence.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#addBusinessFilterHasEvidence(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToHasEvidence(ViewerFilter filter) {
		hasEvidenceBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#isContainedInHasEvidenceTable(EObject element)
	 * 
	 */
	public boolean isContainedInHasEvidenceTable(EObject element) {
		return ((ReferencesTableSettings)hasEvidence.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#initComposedOf(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initComposedOf(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		composedOf.setContentProvider(contentProvider);
		composedOf.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.AssuranceCase.Properties.composedOf);
		if (eefElementEditorReadOnlyState && composedOf.getTable().isEnabled()) {
			composedOf.setEnabled(false);
			composedOf.setToolTipText(ArgMessages.AssuranceCase_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !composedOf.getTable().isEnabled()) {
			composedOf.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#updateComposedOf()
	 * 
	 */
	public void updateComposedOf() {
	composedOf.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#addFilterComposedOf(ViewerFilter filter)
	 * 
	 */
	public void addFilterToComposedOf(ViewerFilter filter) {
		composedOfFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#addBusinessFilterComposedOf(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToComposedOf(ViewerFilter filter) {
		composedOfBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart#isContainedInComposedOfTable(EObject element)
	 * 
	 */
	public boolean isContainedInComposedOfTable(EObject element) {
		return ((ReferencesTableSettings)composedOf.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ArgMessages.AssuranceCase_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
