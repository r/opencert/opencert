/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.common.IReport;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.impactanalysis.eventcreators.IChangeDrivenEventsCreator;

import com.vaadin.data.util.DefaultItemSorter;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;

public class ItemsPage implements IReport 
{
	private static final String ITEM_LABEL_STYLE = "tableItems";
	
	private static final String NAME = "name";
	private static final String ID = "id";
	private static final String DESCRIPTION = "description";
	
	private static final String NAME_LABEL = "Process Name";
	private static final String ID_LABEL = "Id";
	private static final String DESCRIPTION_LABEL = "Description";
	
	private static final String EVALUATION_LABEL_DESC = "<b>Evaluations: </b><br>";
	private static final String EVENTS_LABEL_DESC = "<b>Impact Analysis Events: </b><br>";
	
	protected static final String EMPTY_DATA_NAME = "...";
	private static final String EVIDENCE_ITEMS_STYLE = "evidenceItems";
	private static final String COUNT_ITEMS_STYLE = "countItems";
	
	protected TreeTable itemsTable;
	
	@Override
	public void projectChanged(AssuranceProjectWrapper newProject) {
		if (newProject == null) {
			HierarchicalContainer hc = createEmptyTableDataContainer();
			itemsTable.setContainerDataSource(hc);
			return;
		}
		
		AssuranceProjectManager projectManager = 
				(AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
		AssuranceProject project = projectManager.getProject(newProject.getId());
		
		HierarchicalContainer hc = createDataContainer(project);
		itemsTable.setContainerDataSource(hc);
		for (Object itemId : itemsTable.getItemIds()) {
			expandItem(itemId);
		}
	}
	
	@Override
    public boolean isSupportsProjects()
    {
        return true;
    }

	@Override
	public ReportID getReportID() {
		return null;
	}

	@Override
	public String getTitle() {
		return null;
	}

	@Override
	public Component getParametersComponent(GUIMode guiMode) 
	{
		return null;
	}

	@Override
	public Component getMainComponent(GUIMode guiMode) 
	{
		VerticalLayout mainPanel = new VerticalLayout();
		mainPanel.setWidth("100%");
		
		Component component = addAdditionalComponent();
		if (component != null) {
			mainPanel.addComponent(addAdditionalComponent());
		}
		
		createEvidenceItemsTable();
		for (Object itemId : itemsTable.getItemIds()) {
			itemsTable.setCollapsed(itemId, false);
		}
		
		mainPanel.addComponent(itemsTable);

		return mainPanel;
	}
	
	protected Component addAdditionalComponent() {
		return null;
	}
	
	protected void expandItem(Object itemId) {
		itemsTable.setCollapsed(itemId, false);
		Collection<?> childs = itemsTable.getChildren(itemId);
		if (childs != null && !childs.isEmpty()) {
			for (Object item : childs) {
				expandItem(item);
			}
		}
	}
	
	protected HierarchicalContainer createDataContainer(AssuranceProject project) {
		HierarchicalContainer hc = createEmptyTableDataContainer();
		return hc;
	}
	
	protected void createEvidenceItemsTable() {
		itemsTable = new TreeTable(null, createEmptyTableDataContainer());
		itemsTable.setStyleName("complianceTable");
		itemsTable.setWidth("100%");
		
		itemsTable.setPageLength(0);
		
		itemsTable.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		itemsTable.setItemCaptionPropertyId(NAME);

		addColumnHeaders();
		itemsTable.setHeight("100%");
		itemsTable.setImmediate(true);
	}
	
	protected void addColumnHeaders() {
		itemsTable.setColumnHeader(NAME, NAME_LABEL);
		itemsTable.setColumnHeader(ID, ID_LABEL);
		itemsTable.setColumnHeader(DESCRIPTION, DESCRIPTION_LABEL);
	}
	
	protected HierarchicalContainer createEmptyTableDataContainer() 
    {     
        HierarchicalContainer hc = new HierarchicalContainer()
        {
			private static final long serialVersionUID = -4854665965909691819L;

			@Override
            public Collection<?> getSortableContainerPropertyIds()
            {
                return getContainerPropertyIds();
            }
        };
        
        hc.setItemSorter(new DefaultItemSorter(new Comparator<Object>()
        {
            public int compare(Object o1, Object o2)
            {
            	if (o1 instanceof String && o2 instanceof String) {
                    return ((String) o1).compareTo(((String) o2));
            	} else if (o1 instanceof Label && o2 instanceof Label) {
                	String caption1 = ((Label) o1).getValue();
                	String caption2 = ((Label) o2).getValue();
                	if (caption1 == null) {
                		caption1 = "";
                	}
                	if (caption2 == null) {
                		caption2 = "";
                	}
                	return (caption1.compareTo(caption2));
                } else if (o1 instanceof Button && o2 instanceof Button) {
                    Integer caption1 = Integer.parseInt(((Button) o1).getCaption());
                    Integer caption2 = Integer.parseInt(((Button) o2).getCaption());
                    return caption1.compareTo(caption2);
                }
                return 0;
            }
        }));
        
        addContainerProperties(hc);
        
        return hc;
    }

	protected void addContainerProperties(HierarchicalContainer hc) {
        hc.addContainerProperty(NAME, String.class, "");
        hc.addContainerProperty(ID, String.class, "");
        hc.addContainerProperty(DESCRIPTION, String.class, "");
	}
	
	protected Label createLabel(String labelName) {
		final Label itemLabel = new Label(labelName);
    	itemLabel.setSizeUndefined();
    	itemLabel.setStyleName(ITEM_LABEL_STYLE);
    	
    	return itemLabel;
	}
	
	protected Button createButton(int labelName) {
		Button button = new Button(Integer.toString(labelName));
		button.setStyleName(COUNT_ITEMS_STYLE);
		
		return button;
	}
    
	protected Label createLabel(String labelName, String labelDescription) {
    	final Label label = new Label(labelName);
    	label.setSizeUndefined();
    	label.setDescription(labelDescription);
    	label.setStyleName(EVIDENCE_ITEMS_STYLE);
    	
    	return label;
    }
	
	protected String createDataValue(String dataValue) {
		if (dataValue != null) {
			return dataValue;
		} else {
			return "";
		}
	}
	
	protected String createDataName(String dataValue) {
		if (dataValue == null || "".equals(dataValue)) {
			return EMPTY_DATA_NAME;
		} else {
			return dataValue;
		}
	}
	
	protected Label createTimeLabel(Date date) {
		return createLabel(createTimeDescription(date));
	}
	
	protected String createTimeDescription(Date date) {
		String time = "";
		
		if (date != null && !("".equals(date.toString()))) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			try {
				time = df.format(date.getTime());
			} catch (Exception ex) {
				OpencertLogger.error(ex);
			}
		}
		
		return time;
	}

    protected Label createEvaluationLabel(List<AssuranceAssetEvaluation> evaluations) {
    	String evaluationName = "";
		String evaluationDesc = EVALUATION_LABEL_DESC;
		int i=1;
		for (AssuranceAssetEvaluation evaluation : evaluations) {
			evaluationName = createDataName(evaluation.getName());
			evaluationDesc += "<b>" + i + "</b>" + ".<br>";
			i++;
			evaluationDesc += "<i>Name:</i> " + createDataValue(evaluation.getName()) + "<br>";
			evaluationDesc += "<i>Criterion:</i> " + createDataValue(evaluation.getCriterion()) + "<br>";
			evaluationDesc += "<i>Criterion Description:</i> " + createDataValue(evaluation.getCriterionDescription()) + "<br>";
			evaluationDesc += "<i>Evaluation Result:</i> " + createDataValue(evaluation.getEvaluationResult()) + "<br>";
			evaluationDesc += "<i>Evaluation Rationale:</i> " + createDataValue(evaluation.getRationale()) + "<br>";
		}
		
		if (EVALUATION_LABEL_DESC.equals(evaluationDesc)) {
			return createLabel(evaluationName);
		} else {
			return createLabel(evaluationName, evaluationDesc);
		}
    }
    
    protected Label createAssuranceAssetEventsLabel(List<AssuranceAssetEvent> events, boolean onlyImpactAnalysisEvents) {
    	String eventType = "";
    	boolean setEventsName = true;
		String eventDesc = EVENTS_LABEL_DESC;
		int i=1;
		for (AssuranceAssetEvent event : events) {
			String eventName = createDataName(event.getName());
			//if (eventName.contains(IMPACT_ANALYSIS)) - this is temporary solution
			//checking should be changed to a type or Participant - no data in CCL now
			if (!onlyImpactAnalysisEvents || IChangeDrivenEventsCreator.EVENT_CREATED_BY_IMPACT_ANALYSIS.equals(eventName)) {
				if (setEventsName) {
					eventType = event.getType().toString();
					setEventsName = false;
				}
				eventDesc += "<b>" + i + "</b>" + ".<br>";
				i++;
				eventDesc += "<i>Type:</i> " + event.getType().toString() + "<br>";
				eventDesc += "<i>Description:</i> " + createDataValue(event.getDescription()) + "<br>";
				eventDesc += "<i>Time:</i> " + createTimeDescription(event.getTime()) + "<br>";
			}
		}
		
		if (EVENTS_LABEL_DESC.equals(eventDesc)) {
			return createLabel(eventType);
		} else {
			return createLabel(eventType, eventDesc);
		}
    }
}