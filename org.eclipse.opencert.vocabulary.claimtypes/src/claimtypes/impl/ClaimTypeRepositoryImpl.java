/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package claimtypes.impl;

import claimtypes.ClaimType;
import claimtypes.ClaimTypeRepository;
import claimtypes.ClaimtypesPackage;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.infra.general.general.impl.NamedElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Claim Type Repository</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link claimtypes.impl.ClaimTypeRepositoryImpl#getClaimTypes <em>Claim Types</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClaimTypeRepositoryImpl extends NamedElementImpl implements ClaimTypeRepository {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClaimTypeRepositoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClaimtypesPackage.Literals.CLAIM_TYPE_REPOSITORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ClaimType> getClaimTypes() {
		return (EList<ClaimType>)eGet(ClaimtypesPackage.Literals.CLAIM_TYPE_REPOSITORY__CLAIM_TYPES, true);
	}

} //ClaimTypeRepositoryImpl
