/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.properties.property.Property;
import org.eclipse.opencert.infra.properties.property.PropertyPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class RefArtefactBasePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for constrainingRequirement ReferencesTable
	 */
	private ReferencesTableSettings constrainingRequirementSettings;
	
	/**
	 * Settings for applicableTechnique ReferencesTable
	 */
	private ReferencesTableSettings applicableTechniqueSettings;
	
	/**
	 * Settings for ownedRel ReferencesTable
	 */
	protected ReferencesTableSettings ownedRelSettings;
	
	/**
	 * Settings for property ReferencesTable
	 */
	private ReferencesTableSettings propertySettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public RefArtefactBasePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refArtefact, String editing_mode) {
		super(editingContext, refArtefact, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = RefframeworkViewsRepository.class;
		partKey = RefframeworkViewsRepository.RefArtefact.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final RefArtefact refArtefact = (RefArtefact)elt;
			final RefArtefactPropertiesEditionPart basePart = (RefArtefactPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refArtefact.getId()));
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refArtefact.getName()));
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.description))
				basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, refArtefact.getDescription()));
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.reference))
				basePart.setReference(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refArtefact.getReference()));
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement)) {
				constrainingRequirementSettings = new ReferencesTableSettings(refArtefact, RefframeworkPackage.eINSTANCE.getRefArtefact_ConstrainingRequirement());
				basePart.initConstrainingRequirement(constrainingRequirementSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique)) {
				applicableTechniqueSettings = new ReferencesTableSettings(refArtefact, RefframeworkPackage.eINSTANCE.getRefArtefact_ApplicableTechnique());
				basePart.initApplicableTechnique(applicableTechniqueSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.ownedRel)) {
				ownedRelSettings = new ReferencesTableSettings(refArtefact, RefframeworkPackage.eINSTANCE.getRefArtefact_OwnedRel());
				basePart.initOwnedRel(ownedRelSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.property)) {
				propertySettings = new ReferencesTableSettings(refArtefact, RefframeworkPackage.eINSTANCE.getRefArtefact_Property());
				basePart.initProperty(propertySettings);
			}
			// init filters
			
			
			
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement)) {
				basePart.addFilterToConstrainingRequirement(new EObjectFilter(RefframeworkPackage.Literals.REF_REQUIREMENT));
				// Start of user code for additional businessfilters for constrainingRequirement
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique)) {
				basePart.addFilterToApplicableTechnique(new EObjectFilter(RefframeworkPackage.Literals.REF_TECHNIQUE));
				// Start of user code for additional businessfilters for applicableTechnique
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.ownedRel)) {
				basePart.addFilterToOwnedRel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefArtefactRel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRel
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.property)) {
				basePart.addFilterToProperty(new EObjectFilter(PropertyPackage.Literals.PROPERTY));
				// Start of user code for additional businessfilters for property
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}











	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == RefframeworkViewsRepository.RefArtefact.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefact.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefact.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefact.Properties.reference) {
			return RefframeworkPackage.eINSTANCE.getRefArtefact_Reference();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement) {
			return RefframeworkPackage.eINSTANCE.getRefArtefact_ConstrainingRequirement();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique) {
			return RefframeworkPackage.eINSTANCE.getRefArtefact_ApplicableTechnique();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefact.Properties.ownedRel) {
			return RefframeworkPackage.eINSTANCE.getRefArtefact_OwnedRel();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefact.Properties.property) {
			return RefframeworkPackage.eINSTANCE.getRefArtefact_Property();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		RefArtefact refArtefact = (RefArtefact)semanticObject;
		if (RefframeworkViewsRepository.RefArtefact.Properties.id == event.getAffectedEditor()) {
			refArtefact.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefArtefact.Properties.name == event.getAffectedEditor()) {
			refArtefact.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefArtefact.Properties.description == event.getAffectedEditor()) {
			refArtefact.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefArtefact.Properties.reference == event.getAffectedEditor()) {
			refArtefact.setReference((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof RefRequirement) {
					constrainingRequirementSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				constrainingRequirementSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				constrainingRequirementSettings.move(event.getNewIndex(), (RefRequirement) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof RefTechnique) {
					applicableTechniqueSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				applicableTechniqueSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				applicableTechniqueSettings.move(event.getNewIndex(), (RefTechnique) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefArtefact.Properties.ownedRel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRelSettings.move(event.getNewIndex(), (RefArtefactRel) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefArtefact.Properties.property == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Property) {
					propertySettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				propertySettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				propertySettings.move(event.getNewIndex(), (Property) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			RefArtefactPropertiesEditionPart basePart = (RefArtefactPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.description)){
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefArtefact_Reference().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.reference)) {
				if (msg.getNewValue() != null) {
					basePart.setReference(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setReference("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefArtefact_ConstrainingRequirement().equals(msg.getFeature())  && isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement))
				basePart.updateConstrainingRequirement();
			if (RefframeworkPackage.eINSTANCE.getRefArtefact_ApplicableTechnique().equals(msg.getFeature())  && isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique))
				basePart.updateApplicableTechnique();
			if (RefframeworkPackage.eINSTANCE.getRefArtefact_OwnedRel().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.ownedRel))
				basePart.updateOwnedRel();
			if (RefframeworkPackage.eINSTANCE.getRefArtefact_Property().equals(msg.getFeature())  && isAccessible(RefframeworkViewsRepository.RefArtefact.Properties.property))
				basePart.updateProperty();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			RefframeworkPackage.eINSTANCE.getRefArtefact_Reference(),
			RefframeworkPackage.eINSTANCE.getRefArtefact_ConstrainingRequirement(),
			RefframeworkPackage.eINSTANCE.getRefArtefact_ApplicableTechnique(),
			RefframeworkPackage.eINSTANCE.getRefArtefact_OwnedRel(),
			RefframeworkPackage.eINSTANCE.getRefArtefact_Property()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (RefframeworkViewsRepository.RefArtefact.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefact.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefact.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefact.Properties.reference == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefArtefact_Reference().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefArtefact_Reference().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
