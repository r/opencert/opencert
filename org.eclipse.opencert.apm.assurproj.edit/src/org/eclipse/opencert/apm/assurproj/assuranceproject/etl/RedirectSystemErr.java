/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/* This code may be freely used and distributed in any project.  */
/* However, please do not remove this credit if you publish this */
/* code in paper or electronic form, such as on a web site.      */
/*****************************************************************/

package org.eclipse.opencert.apm.assurproj.assuranceproject.etl;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;

public class RedirectSystemErr {
	static PrintStream console = null;
	static FileWriter fw = null;
	static String fwString ;
	static int cont = 0;
	
	public static void init(String fichero) throws IOException
	{
		/*
		System.err.println("MCP RedirectSystemErr - This goes to the console");
		console = System.err;*/
		/*
		File file = new File(fichero);
		FileOutputStream fos = new FileOutputStream(file);
		*/
		/*
		PrintStream ps = new PrintStream(fos);
		System.setErr(ps);
		*/
		fwString = fichero;
		//fw = new FileWriter(file);
	}
	
	public static void end() throws IOException
	{
		/*System.setErr(console);
		System.err.println("MCP RedirectSystemErr - This also goes to the console");*/
		if(fw != null) fw.close();
	}
	public static void err(String texto)  {

		//System.err.println(texto);
		try {
		//fw = new FileWriter("D:\\Proyectos\\err"+ ++cont +".txt",true);
		fw = new FileWriter("D:\\Proyectos\\err.txt",true);
		fw.write(texto + "\n");
		fw.flush();
		fw.close();
		fw = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		

		/*
		try {
			throw new Exception("Exception goes to err.txt too");
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
	}
	
	public static void out(String texto)  {
	    FileOutputStream outputF = null;
		try {
			//outputF = new FileOutputStream("D:\\Proyectos\\out"+ ++cont +".txt");
			//outputF = new FileOutputStream("D:\\Personal\\Carmen\\Proassurance\\trabajo\\runtime-201406\\.metadata\\out.txt", true);
	            	String outputPath = System.getProperty("user.home");
			outputF = new FileOutputStream(outputPath +"\\out.txt", true);
			outputF.write(texto.getBytes()); outputF.write("\n".getBytes()); outputF.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}