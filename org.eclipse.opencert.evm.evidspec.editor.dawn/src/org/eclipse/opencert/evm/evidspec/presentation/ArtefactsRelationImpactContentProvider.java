/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.evm.evidspec.presentation;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.opencert.impactanalysis.relations.IArtefactRelationImpact;

public class ArtefactsRelationImpactContentProvider implements ITreeContentProvider {
	 	  
	   List<IArtefactRelationImpact> listArtefactsRelationImpacts;
	
	   public ArtefactsRelationImpactContentProvider(List<IArtefactRelationImpact> listArtefactsRelationImpacts){		
		   this.listArtefactsRelationImpacts=listArtefactsRelationImpacts; 
	   }
	   
		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public Object[] getElements(Object arg0) {		
			List  <Object>roots = new ArrayList<Object>();
			for (IArtefactRelationImpact artefactsRelationImpact : listArtefactsRelationImpacts) {
				if(artefactsRelationImpact.getRecursionDepth()==0){
					roots.add(artefactsRelationImpact);
				}
			}
			
			return roots.toArray();
		}
		
		@Override
		public Object getParent(Object arg0) {
			if (arg0 instanceof IArtefactRelationImpact) {	    	  
		    	  IArtefactRelationImpact artefactsRelationImpact = (IArtefactRelationImpact)arg0;
		    	  if(artefactsRelationImpact.getRecursionDepth()!=0){
			    	  int index = listArtefactsRelationImpacts.indexOf(arg0);
			    	  int depth= artefactsRelationImpact.getRecursionDepth();			    	  
			    	  if(index > 0){
			    		  //Search the precedent IArtefactRelationImpact with a less depth
			    		  for (int nr = index-1; nr >=0; nr--) {
			    			  IArtefactRelationImpact previousRelationImpact =listArtefactsRelationImpacts.get(nr);
			    			  if(previousRelationImpact.getRecursionDepth() == depth -1){
			    				  return new Object[]{listArtefactsRelationImpacts.get(nr)};
			    			  }
			              }			    		  
			    	  }
		    	  }
		    	  else return null;
			}
			return null;
		}
		
		@Override
		public Object[] getChildren(Object arg0) {
			if (arg0 instanceof IArtefactRelationImpact) {	    	  
				 IArtefactRelationImpact artefactsRelationImpact = (IArtefactRelationImpact)arg0;
				 if(artefactsRelationImpact.getRecursionDepth()!=3){
					  int index = listArtefactsRelationImpacts.indexOf(arg0);
			    	  int depth= artefactsRelationImpact.getRecursionDepth();			    	  
			    	  if(index<listArtefactsRelationImpacts.size()-1){
			    		  ArrayList <IArtefactRelationImpact>inter = new ArrayList<IArtefactRelationImpact>();
			    		  for (int nr = index+1; nr <=listArtefactsRelationImpacts.size()-1; nr++) {
			    			  IArtefactRelationImpact nextRelationImpact =listArtefactsRelationImpacts.get(nr);			    			  
			    			  if(nextRelationImpact.getRecursionDepth() == depth +1){
			    				  inter.add(listArtefactsRelationImpacts.get(nr));
			    			  }
			    			  else if(nextRelationImpact.getRecursionDepth() <= depth){
			    				  return inter.toArray();
			    			  }
			              }	
			    		  return inter.toArray();
			    	  }
			    	  else return null;
				 }
				 else return null;
				 
		    	  /*int index = listArtefactsRelationImpacts.indexOf(arg0);
		    	  if(index<listArtefactsRelationImpacts.size()-1){
		    		  if(listArtefactsRelationImpacts.get(index+1).getRecursionDepth()!=0){
		    			  return new Object[]{listArtefactsRelationImpacts.get(index+1)};
		    		  }
		    		  else return null;
		    	  }*/
			}
			return null;
		}
		
		@Override
		public boolean hasChildren(Object arg0) {
			if (arg0 instanceof IArtefactRelationImpact) {	    	  
				 IArtefactRelationImpact artefactsRelationImpact = (IArtefactRelationImpact)arg0;
				 if(artefactsRelationImpact.getRecursionDepth()!=3){
					  int index = listArtefactsRelationImpacts.indexOf(arg0);
			    	  int depth= artefactsRelationImpact.getRecursionDepth();			    	  
			    	  if(index<listArtefactsRelationImpacts.size()-1){			    		  
		    			  IArtefactRelationImpact nextRelationImpact =listArtefactsRelationImpacts.get(index+1);			    			  
		    			  if(nextRelationImpact.getRecursionDepth() == depth +1){
		    				  return true;
		    			  }
		    			  else return false;			    			  			             
			    	  }
			    	  else return false;
				 }		
				 else return false;
			}
			return false;
		}
	}
