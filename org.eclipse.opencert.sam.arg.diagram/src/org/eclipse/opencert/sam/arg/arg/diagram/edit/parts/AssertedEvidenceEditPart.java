/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.parts;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;
import org.eclipse.opencert.gsn.figures.GSNSolvedBy;
import org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension;
import org.eclipse.opencert.sam.arg.arg.AssertedEvidence;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.AssertedEvidenceItemSemanticEditPolicy;

/**
 * @generated
 */
public class AssertedEvidenceEditPart extends ConnectionNodeEditPart implements
		ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 4002;

	/**
	 * @generated
	 */
	public AssertedEvidenceEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new AssertedEvidenceItemSemanticEditPolicy());
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated NOT
	 */

	protected Connection createConnectionFigure() {
		// Start MCP
		//return new AssertedEvidenceFigure();
		if (!(((View) this.getModel()).getElement() instanceof AssertedEvidence)) {
			System.out.println("createNodeShape(): Error model not allowed");
			return new AssertedEvidenceFigure();
		}
		AssertedEvidence editPart = (AssertedEvidence) ((View) this.getModel())
				.getElement();
		AssertedEvidenceFigure figure = new AssertedEvidenceFigure();
		if (editPart.getMultiextension() == AssertedByMultiplicityExtension.OPTIONAL) {
			figure.setShape(1);
		} else if (editPart.getMultiextension() == AssertedByMultiplicityExtension.MULTI) {
			figure.setShape(2);
		} else {
			figure.setShape(0);
		}
		return figure;
		// End MCP
	}

	/**
	 * @generated
	 */
	public AssertedEvidenceFigure getPrimaryShape() {
		return (AssertedEvidenceFigure) getFigure();
	}

	/**
	 * @generated NOT
	 */
	@Override
	protected void handleNotificationEvent(Notification event) {
		// Start MCP
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {
			if (event.getNotifier() instanceof AssertedEvidence) {

				AssertedEvidence editPart = (AssertedEvidence) ((View) this
						.getModel()).getElement();
				AssertedEvidenceFigure figure = (AssertedEvidenceFigure) this
						.getPrimaryShape();
				if (editPart.getMultiextension() == AssertedByMultiplicityExtension.OPTIONAL) {
					figure.setShape(1);
					editPart.setCardinality("");
				} else if (editPart.getMultiextension() == AssertedByMultiplicityExtension.MULTI) {
					figure.setShape(2);
				} else {
					figure.setShape(0);
					editPart.setCardinality("");
				}

			}
			super.handleNotificationEvent(event);
		}
		// End MCP
	}

	/**
	 * @generated NOT
	 */
	// Start MCP	
	public class AssertedEvidenceFigure extends GSNSolvedBy {
		//public class AssertedEvidenceFigure extends PolylineConnectionEx {
		// End MCP

		/**
		 * @generated
		 */
		public AssertedEvidenceFigure() {
			this.setForegroundColor(THIS_FORE);

			setTargetDecoration(createTargetDecoration());
		}

		/**
		 * @generated
		 */
		private RotatableDecoration createTargetDecoration() {
			PolygonDecoration df = new PolygonDecoration();
			df.setFill(true);
			PointList pl = new PointList();
			pl.addPoint(getMapMode().DPtoLP(0), getMapMode().DPtoLP(0));
			pl.addPoint(getMapMode().DPtoLP(-2), getMapMode().DPtoLP(2));
			pl.addPoint(getMapMode().DPtoLP(-2), getMapMode().DPtoLP(-2));
			pl.addPoint(getMapMode().DPtoLP(0), getMapMode().DPtoLP(0));
			df.setTemplate(pl);
			df.setScale(getMapMode().DPtoLP(7), getMapMode().DPtoLP(3));
			return df;
		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 0, 0, 0);

}
