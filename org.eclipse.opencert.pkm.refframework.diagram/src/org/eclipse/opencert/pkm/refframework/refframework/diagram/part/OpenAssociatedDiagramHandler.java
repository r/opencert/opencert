/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.pkm.refframework.refframework.diagram.part;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * This class is used to get the selection index and run the OpenAssociatedDiagram Action
 * 
 * @author M� Carmen Palacios
 */
/**
 * @generated NOT
 */
public class OpenAssociatedDiagramHandler extends AbstractHandler {
	public static final String ID = "org.eclipse.opencert.pkm.refframework.refframework.diagram.part.open";
	public static final String PARM_EDITOR = "org.eclipse.opencert.pkm.refframework.refframework.diagram.part.open.index";

	/**
	 * @generated NOT
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		Object index = event.getObjectParameterForExecution(PARM_EDITOR);
		if (!(index instanceof Integer)) {
			throw new ExecutionException("Invalid index: " + index);
		}
		int i = ((Integer) index).intValue();
		try {
			String fileName = OpenAssociatedDiagram.getFile(i);
			ISelection selection = OpenAssociatedDiagram.getSelection();
			System.out.println("OpenActivatedEditorHandler i=" + i);
			System.out.println("OpenActivatedEditorHandler fileName=" + fileName);
			OpenAssociatedDiagramAction op = new OpenAssociatedDiagramAction(fileName, selection, OpenAssociatedDiagramAction.ACTION_OPEN);
			op.init();
			op.run();
		} catch (Exception e) {
			throw new ExecutionException("OpenActivatedEditorHandler Failed", e);
		}
		return null;
	}

}

 

