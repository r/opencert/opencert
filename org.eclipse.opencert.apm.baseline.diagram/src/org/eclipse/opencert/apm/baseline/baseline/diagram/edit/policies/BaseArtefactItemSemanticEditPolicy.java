/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands.BaseActivityProducedArtefactCreateCommand;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands.BaseActivityProducedArtefactReorientCommand;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands.BaseActivityRequiredArtefactCreateCommand;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands.BaseActivityRequiredArtefactReorientCommand;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityProducedArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRequiredArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes;

/**
 * @generated
 */
public class BaseArtefactItemSemanticEditPolicy extends
		BaselineBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public BaseArtefactItemSemanticEditPolicy() {
		super(BaselineElementTypes.BaseArtefact_2002);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		View view = (View) getHost().getModel();
		CompositeTransactionalCommand cmd = new CompositeTransactionalCommand(
				getEditingDomain(), null);
		cmd.setTransactionNestingEnabled(false);
		for (Iterator<?> it = view.getTargetEdges().iterator(); it.hasNext();) {
			Edge incomingLink = (Edge) it.next();
			if (BaselineVisualIDRegistry.getVisualID(incomingLink) == BaseActivityRequiredArtefactEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						incomingLink.getSource().getElement(), null,
						incomingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
			if (BaselineVisualIDRegistry.getVisualID(incomingLink) == BaseActivityProducedArtefactEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						incomingLink.getSource().getElement(), null,
						incomingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
		}
		EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
		if (annotation == null) {
			// there are indirectly referenced children, need extra commands: false
			addDestroyShortcutsCommand(cmd, view);
			// delete host element
			cmd.add(new DestroyElementCommand(req));
		} else {
			cmd.add(new DeleteCommand(getEditingDomain(), view));
		}
		return getGEFWrapper(cmd.reduce());
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req)
				: getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super
				.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (BaselineElementTypes.BaseActivityRequiredArtefact_4001 == req
				.getElementType()) {
			return null;
		}
		if (BaselineElementTypes.BaseActivityProducedArtefact_4002 == req
				.getElementType()) {
			return null;
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (BaselineElementTypes.BaseActivityRequiredArtefact_4001 == req
				.getElementType()) {
			return getGEFWrapper(new BaseActivityRequiredArtefactCreateCommand(
					req, req.getSource(), req.getTarget()));
		}
		if (BaselineElementTypes.BaseActivityProducedArtefact_4002 == req
				.getElementType()) {
			return getGEFWrapper(new BaseActivityProducedArtefactCreateCommand(
					req, req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * Returns command to reorient EReference based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientReferenceRelationshipCommand(
			ReorientReferenceRelationshipRequest req) {
		switch (getVisualID(req)) {
		case BaseActivityRequiredArtefactEditPart.VISUAL_ID:
			return getGEFWrapper(new BaseActivityRequiredArtefactReorientCommand(
					req));
		case BaseActivityProducedArtefactEditPart.VISUAL_ID:
			return getGEFWrapper(new BaseActivityProducedArtefactReorientCommand(
					req));
		}
		return super.getReorientReferenceRelationshipCommand(req);
	}

}
