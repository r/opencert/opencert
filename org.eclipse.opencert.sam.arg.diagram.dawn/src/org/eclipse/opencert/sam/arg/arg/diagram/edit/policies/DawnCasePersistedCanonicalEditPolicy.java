/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.policies;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.notation.View;

import java.util.ArrayList;
import java.util.List;

//Start MCP
public class DawnCasePersistedCanonicalEditPolicy extends CasePersistedCanonicalEditPolicy {

	public DawnCasePersistedCanonicalEditPolicy() {
		super();
	}

	@Override
	protected CreateViewRequest getCreateViewRequest(
			List<ViewDescriptor> descriptors) {
		List<View> viewChildren = getViewChildren();

		List<ViewDescriptor> tbr = new ArrayList<CreateViewRequest.ViewDescriptor>();

		/* In order to improve efficiency
		for (ViewDescriptor desc : descriptors) {
			EObject obj = (EObject) ((CanonicalElementAdapter) desc
					.getElementAdapter()).getRealObject();

			boolean found = false;

			for (View view : viewChildren) {
				if (view.getElement().equals(obj)) {
					found = true;
					break;
				}
			}
			if (!found) {
				tbr.add(desc);
			}
		}

		//descriptors.removeAll(tbr);
 		*/

		return new CreateViewRequest(descriptors);
	}
}
//End MCP