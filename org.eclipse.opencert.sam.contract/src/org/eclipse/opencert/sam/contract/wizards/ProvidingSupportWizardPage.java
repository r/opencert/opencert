/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.wizards;

import java.util.ArrayList;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.opencert.sam.arg.arg.InformationElementType;

public class ProvidingSupportWizardPage extends WizardPage {
	private Table tblSelectClaim;
	private CDOView viewCDO;
	private Table tblClaimsSelected;
	private ArrayList selectedClaims = new ArrayList();
	private ArrayList<EObject> selectedClaimsEObjects = new ArrayList<EObject>();
	private ArrayList refList = new ArrayList();
	private ArrayList<String> refListDir = new ArrayList();
	private ComboViewer comboViewerSelectArgModel;
	private Table tblContextAssociations;

	
	protected ProvidingSupportWizardPage(String pageName,  CDOView view) {
		super(pageName);
		viewCDO = view;
		setPageComplete(false);
	}

	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		final GridLayout gridLayout = new GridLayout(1, false);
		setControl(container);
		
		container.setLayout(gridLayout);
	
		Label lblSelectArgModel = new Label(container, SWT.NONE);
		lblSelectArgModel.setText("Select Argumentation Model");
		
		comboViewerSelectArgModel = new ComboViewer(container, SWT.NONE);
		Combo comboSelectArgModel = comboViewerSelectArgModel.getCombo();
		
		comboSelectArgModel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		Label lblSelectClaim = new Label(container, SWT.NONE);
		lblSelectClaim.setText("Select Claim(s) Providing Support By Contract");
		
		final TableViewer tblViewerSelectClaims = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		tblSelectClaim = tblViewerSelectClaims.getTable();
		GridData gd_tblSelectClaim = new GridData(GridData.FILL_HORIZONTAL);
		gd_tblSelectClaim.heightHint = GridData.FILL_VERTICAL / 6;
		tblSelectClaim.setLayoutData(gd_tblSelectClaim);
		tblSelectClaim.setHeaderVisible(true);
		tblSelectClaim.setLinesVisible(true);
		
		String[] columnNames = new String[] {
				"ID", "Description", "CDOResource"};
		int[] columnWidths = new int[] {
				150, 250, 200};
		int[] columnAlignments = new int[] {
			SWT.LEFT, SWT.LEFT, SWT.LEFT};
		for (int i = 0; i < columnNames.length; i++) {
			TableColumn tableColumn =
				new TableColumn(tblSelectClaim, columnAlignments[i]);
			tableColumn.setText(columnNames[i]);
			tableColumn.setWidth(columnWidths[i]);
		}
		tblViewerSelectClaims.setLabelProvider(
				new CDOResourceTableLabelProvider());
		tblViewerSelectClaims.setContentProvider(
				new ArrayContentProvider());
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		
		Label lblContextAssociations = new Label(container, SWT.NONE);
		lblContextAssociations.setText("Inherited Context Associations");
		
		final TableViewer tblViewerContextAssociations = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		tblContextAssociations = tblViewerContextAssociations.getTable();
		String[] columnNamesContextAssociation = new String[] {
				"CA Source ID", "CA Source Description", "CA Target ID", "CA Source Description"};
		int[] columnWidthsContextAssociation = new int[] {
			150, 150, 150, 150};
		int[] columnAlignmentsContextAssociation = new int[] {
			SWT.LEFT, SWT.LEFT, SWT.LEFT, SWT.LEFT};
		for (int i = 0; i < columnNamesContextAssociation.length; i++) {
			TableColumn tableColumnContextAssociation =
				new TableColumn(tblContextAssociations, columnAlignmentsContextAssociation[i]);
			tableColumnContextAssociation.setText(columnNamesContextAssociation[i]);
			tableColumnContextAssociation.setWidth(columnWidthsContextAssociation[i]);
		}
		
		tblViewerContextAssociations.setLabelProvider(
				new ContextAssociationTableLabelProvider());
		tblViewerContextAssociations.setContentProvider(
				new ArrayContentProvider());
		tblContextAssociations.setHeaderVisible(true);
		tblContextAssociations.setLinesVisible(true);
		
		GridData gd_tableContextAssociations = new GridData(GridData.FILL_HORIZONTAL);
		gd_tableContextAssociations.heightHint = GridData.FILL_VERTICAL / 10;
		tblContextAssociations.setLayoutData(gd_tableContextAssociations);
		
		Label lblClaimsSelected = new Label(container, SWT.NONE);
		lblClaimsSelected.setText("Claims Selected");
		
		final TableViewer tblViewerClaimsSelected = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		tblClaimsSelected = tblViewerClaimsSelected.getTable();
	
		String[] columnNames2 = new String[] {
				"ID", "Description", "CDOResource"};
		int[] columnWidths2 = new int[] {
			150, 250, 200};
		int[] columnAlignments2 = new int[] {
			SWT.LEFT, SWT.LEFT, SWT.LEFT};
		for (int i = 0; i < columnNames2.length; i++) {
			TableColumn tableColumn2 =
				new TableColumn(tblClaimsSelected, columnAlignments2[i]);
			tableColumn2.setText(columnNames2[i]);
			tableColumn2.setWidth(columnWidths2[i]);
		}
		
		tblViewerClaimsSelected.setLabelProvider(
				new CDOResourceTableLabelProvider());
		tblViewerClaimsSelected.setContentProvider(
				new ArrayContentProvider());
		tblClaimsSelected.setHeaderVisible(true);
		tblClaimsSelected.setLinesVisible(true);
		GridData gd_table = new GridData(GridData.FILL_HORIZONTAL);
		gd_table.heightHint = GridData.FILL_VERTICAL / 10;
		tblClaimsSelected.setLayoutData(gd_table);
		
		comboViewerSelectArgModel.setContentProvider(new ArrayContentProvider());
		comboViewerSelectArgModel.setLabelProvider(new CDOResourceListLabelProvider());
		
		comboViewerSelectArgModel.addSelectionChangedListener(
				new ISelectionChangedListener()
				{
					public void selectionChanged(SelectionChangedEvent event)
					{
						IStructuredSelection selection = (IStructuredSelection) event.getSelection();
						ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
						dialog.open();
						IProgressMonitor monitor = dialog.getProgressMonitor();
						monitor.beginTask("Searching for Candidate Contract Elements", 20);
						
					

						try {			
							ArrayList results = getCandidateContractElements(selection);
							tblViewerSelectClaims.setInput(results.toArray());
						} catch (Exception e) {
							e.printStackTrace();
						}
					
						monitor.worked(1);
						dialog.close();
						
					}
				});
		
		tblViewerSelectClaims.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent arg0) {
				IStructuredSelection selection = (IStructuredSelection) arg0.getSelection();
				ContractModelElement cme = (ContractModelElement)selection.getFirstElement();
				EObject selEObj = (EObject) cme.getModelElement();
				selectedClaims.add(cme);
				selectedClaimsEObjects.add(selEObj);
				tblViewerClaimsSelected.setInput(selectedClaims.toArray());
				getWizard().getContainer().updateButtons();
				ContractModelBuilder modelBuilder = new ContractModelBuilder();
				ArrayList<ContextAssociation> contextAssociations =modelBuilder.computeInheritedContext(selEObj, cme.getCdoResource(), true, true);
				tblViewerContextAssociations.setInput(contextAssociations.toArray());
			}
		});
		
		tblViewerClaimsSelected.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent arg0) {
				IStructuredSelection selection = (IStructuredSelection) arg0.getSelection();
				selectedClaimsEObjects.remove(((ContractModelElement)selection.getFirstElement()).getModelElement());
				selectedClaims.remove(selection.getFirstElement());
				tblViewerClaimsSelected.setInput(selectedClaims.toArray());
				getWizard().getContainer().updateButtons();
			}
		});
		
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		dialog.open();
		IProgressMonitor monitor = dialog.getProgressMonitor();
		monitor.beginTask("Searching for argumnetation files", 20);

		try {			
			populateArgModels();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		monitor.worked(1);
		dialog.close();
		
	}
	
	public ArrayList getCandidateContractElements(IStructuredSelection selection)
	{
		EObject eobj;
		EList elementsInModule;
		EStructuralFeature isPublic;
		boolean isPublicBool;
		EStructuralFeature toBeSupported;
		boolean toBeSupportedBool;
		ArrayList results = new ArrayList();
		CDOResource resource = (CDOResource) selection.getFirstElement();
		TreeIterator ti = resource.eAllContents();
		EStructuralFeature type;
		boolean isSolution;
	
		while(ti.hasNext())
		{
			
			eobj = (EObject) ti.next();
			if(eobj.eClass().getName() == "Argumentation")
			{
				
				elementsInModule = (EList) eobj.eGet(eobj.eClass().getEStructuralFeature("consistOf"));
				for(int z = 0; z < elementsInModule.size(); z++)
				{
					EObject moduleElement = (EObject) elementsInModule.get(z);
					
					if(moduleElement.eClass().getName() =="Claim")
					{
						toBeSupported = moduleElement.eClass().getEStructuralFeature("toBeSupported");
					  	toBeSupportedBool = ((Boolean)moduleElement.eGet(toBeSupported)).booleanValue();
					  	isPublic = moduleElement.eClass().getEStructuralFeature("public");
					  	isPublicBool = ((Boolean)moduleElement.eGet(isPublic)).booleanValue();
					  	
					  	if(isPublicBool == true && toBeSupportedBool == false)
						{
					  		ContractModelElement cme = new ContractModelElement();
							cme.setEnclosingModule(eobj);
							cme.setCdoResource(resource);
					  		cme.setContractRole(ContractModelElement.PROVIDES_SUPPORT);
					  		cme.setModelElement(moduleElement);
						  	results.add(cme);
						}		 
					}
					
					else if(moduleElement.eClass().getName() =="InformationElementCitation")
					{
						type = moduleElement.eClass().getEStructuralFeature("type");
						isSolution = ((InformationElementType)moduleElement.eGet(type)).getName().equals("Solution");
					 	if(isSolution == true)
						{
					 		ContractModelElement cme = new ContractModelElement();
							cme.setEnclosingModule(eobj);
							cme.setCdoResource(resource);
					  		cme.setContractRole(ContractModelElement.PROVIDES_SUPPORT);
					  		cme.setModelElement(moduleElement);
						  	results.add(cme);
					  		
						}	
						
					}
				}
			}		 
		}
		return results;
	}
	
	public void populateArgModels()
	{
		addFilesOfDir();
		setArgModelListInput(refList.toArray());
	}
	
	public int addFilesOfDir()
	{      
		  int sumStd=0;
	     CDOResourceNode[] listR=  viewCDO.getElements();   
	      for (int i=0; i<listR.length; i++){
	            if(listR[i] instanceof CDOResourceFolder){
	                  checkFolderContents((CDOResourceFolder)listR[i],sumStd);
	            }
	            else if (listR[i].getName().endsWith(".arg")){
	            	refList.add(listR[i]);
	                refListDir.add(listR[i].getPath());
	                sumStd++;
	            }                      
	      }
	      return sumStd;
	}
	
	public void setArgModelListInput(Object[] cdoResource)
	{
		comboViewerSelectArgModel.setInput(cdoResource);
		comboViewerSelectArgModel.setSorter(new ViewerSorter() {
			public int compare(
					Viewer viewer, Object c1, Object c2) {
						return ((CDOResource) c1).getPath().compareToIgnoreCase(((CDOResource) c2).getPath());
					}
			});
	}

	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	private void checkFolderContents(CDOResourceFolder cdoResourceFolder, int sumStd) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN=  cdoResourceFolder.getNodes();
			for (int i=0; i<listN.size(); i++){
				if(listN.get(i) instanceof CDOResourceFolder){
					checkFolderContents((CDOResourceFolder)listN.get(i),sumStd);
		        }
				else if (listN.get(i).getName().endsWith(".arg")){
					refList.add(listN.get(i));
		            refListDir.add(listN.get(i).getPath());
		            sumStd++;
		        }
		   }
		}
	}
	
	public ArrayList<EObject> getSelectedEObjects()
	{
		return selectedClaimsEObjects;
	}
	
	public ArrayList getContractModelElements()
	{
		return selectedClaims;
	}
	
	public boolean canFlipToNextPage()
	{
		if(selectedClaims.size() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

}


