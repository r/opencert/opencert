/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetsModel;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceassetViewsRepository;
import org.eclipse.opencert.infra.general.general.GeneralPackage;


// End of user code

/**
 * 
 * 
 */
public class AssuranceAssetsModelPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for AssuranceAsset ReferencesTable
	 */
	protected ReferencesTableSettings assuranceAssetSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public AssuranceAssetsModelPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject assuranceAssetsModel, String editing_mode) {
		super(editingContext, assuranceAssetsModel, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = AssuranceassetViewsRepository.class;
		partKey = AssuranceassetViewsRepository.AssuranceAssetsModel.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final AssuranceAssetsModel assuranceAssetsModel = (AssuranceAssetsModel)elt;
			final AssuranceAssetsModelPropertiesEditionPart basePart = (AssuranceAssetsModelPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceAssetsModel.getId()));
			
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceAssetsModel.getName()));
			
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceAssetsModel.getDescription()));
			
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_)) {
				assuranceAssetSettings = new ReferencesTableSettings(assuranceAssetsModel, AssuranceassetPackage.eINSTANCE.getAssuranceAssetsModel_AssuranceAsset());
				basePart.initAssuranceAsset(assuranceAssetSettings);
			}
			// init filters
			
			
			
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_)) {
				basePart.addFilterToAssuranceAsset(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof AssuranceAsset); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for AssuranceAsset
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}







	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_) {
			return AssuranceassetPackage.eINSTANCE.getAssuranceAssetsModel_AssuranceAsset();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		AssuranceAssetsModel assuranceAssetsModel = (AssuranceAssetsModel)semanticObject;
		if (AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id == event.getAffectedEditor()) {
			assuranceAssetsModel.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name == event.getAffectedEditor()) {
			assuranceAssetsModel.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description == event.getAffectedEditor()) {
			assuranceAssetsModel.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_ == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, assuranceAssetSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				assuranceAssetSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				assuranceAssetSettings.move(event.getNewIndex(), (AssuranceAsset) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			AssuranceAssetsModelPropertiesEditionPart basePart = (AssuranceAssetsModelPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (AssuranceassetPackage.eINSTANCE.getAssuranceAssetsModel_AssuranceAsset().equals(msg.getFeature()) && isAccessible(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_))
				basePart.updateAssuranceAsset();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			AssuranceassetPackage.eINSTANCE.getAssuranceAssetsModel_AssuranceAsset()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
