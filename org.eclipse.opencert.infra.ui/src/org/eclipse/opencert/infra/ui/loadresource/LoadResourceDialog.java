/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.ui.loadresource;

import org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer;
import org.eclipse.emf.cdo.dawn.ui.wizards.dialogs.CDOResourceNodeSelectionDialog;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.ui.CommonUIPlugin;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;

/**
 * Instances of this class allow a user to specify one or more URIs identifying
 * resources. The dialog includes buttons that allow the repository to be
 * browsed, so that the URI can be automatically filled based on the selected
 * resource.
 */
public class LoadResourceDialog extends
		org.eclipse.emf.edit.ui.action.LoadResourceAction.LoadResourceDialog {

	public LoadResourceDialog(Shell parent) {
		super(parent, null);
	}

	public LoadResourceDialog(Shell parent, EditingDomain domain) {
		super(parent, domain);
	}

	/**
	 * Creates and returns the contents of the upper part of this dialog. This
	 * implementation creates a labeled text field for the URI(s) and buttons
	 * for browsing the file system and workspace. These buttons are configured
	 * (selection listeners are added) by calling
	 * {@link #prepareBrowseFileSystemButton} and
	 * {@link #prepareBrowseWorkspaceButton}, respectively.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_MARGIN);
		gridLayout.marginWidth = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
		gridLayout.verticalSpacing = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_SPACING);
		gridLayout.horizontalSpacing = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
		composite.setLayout(gridLayout);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		applyDialogFont(composite);

		{
			FormLayout formLayout = new FormLayout();
			composite.setLayout(formLayout);

			GridData data = new GridData();
			data.verticalAlignment = GridData.FILL;
			data.grabExcessVerticalSpace = true;
			data.horizontalAlignment = GridData.FILL;
			data.grabExcessHorizontalSpace = true;
			if (!EMFPlugin.IS_RESOURCES_BUNDLE_AVAILABLE) {
				data.widthHint = 330;
			}
			composite.setLayoutData(data);
		}

		// buttonComposite has to be the first child of composite because
		// subclasses are expecting this.
		Composite buttonComposite = new Composite(composite, SWT.NONE);

		Label resourceURILabel = new Label(composite, SWT.LEFT);
		{
			resourceURILabel.setText(CommonUIPlugin.INSTANCE
					.getString(isMulti() ? "_UI_ResourceURIs_label"
							: "_UI_ResourceURI_label"));
			FormData data = new FormData();
			data.left = new FormAttachment(0, CONTROL_OFFSET);
			data.top = new FormAttachment(0, CONTROL_OFFSET);
			resourceURILabel.setLayoutData(data);
		}

		{
			FormData data = new FormData();
			data.top = new FormAttachment(resourceURILabel, CONTROL_OFFSET,
					SWT.CENTER);
			data.left = new FormAttachment(resourceURILabel, CONTROL_OFFSET);
			data.right = new FormAttachment(100, -CONTROL_OFFSET);
			buttonComposite.setLayoutData(data);

			buttonComposite.setLayout(new FormLayout());
		}

		uriField = new Text(composite, SWT.BORDER);
		{
			FormData data = new FormData();
			data.top = new FormAttachment(buttonComposite, CONTROL_OFFSET);
			data.left = new FormAttachment(0, CONTROL_OFFSET);
			data.right = new FormAttachment(100, -CONTROL_OFFSET);
			uriField.setLayoutData(data);
		}

		Button browseFileSystemButton = new Button(buttonComposite, SWT.PUSH);
		// browseFileSystemButton.setText(CommonUIPlugin.INSTANCE.getString("_UI_BrowseFileSystem_label"));
		browseFileSystemButton.setText("Browse Repository");
		prepareBrowseFileSystemButton(browseFileSystemButton);

		if (EMFPlugin.IS_RESOURCES_BUNDLE_AVAILABLE) {
			Button browseWorkspaceButton = new Button(buttonComposite, SWT.PUSH);
			{
				FormData data = new FormData();
				data.right = new FormAttachment(100);
				browseWorkspaceButton.setLayoutData(data);
			}
			{
				FormData data = new FormData();
				data.right = new FormAttachment(browseWorkspaceButton,
						-CONTROL_OFFSET);
				browseFileSystemButton.setLayoutData(data);
			}
			browseWorkspaceButton.setText(CommonUIPlugin.INSTANCE
					.getString("_UI_BrowseWorkspace_label"));
			prepareBrowseWorkspaceButton(browseWorkspaceButton);
		} else {
			FormData data = new FormData();
			data.right = new FormAttachment(100);
			browseFileSystemButton.setLayoutData(data);
		}

		Label separatorLabel = new Label(composite, SWT.SEPARATOR
				| SWT.HORIZONTAL);
		{
			FormData data = new FormData();
			data.top = new FormAttachment(uriField,
					(int) (1.5 * CONTROL_OFFSET));
			data.left = new FormAttachment(0, -CONTROL_OFFSET);
			data.right = new FormAttachment(100, CONTROL_OFFSET);
			separatorLabel.setLayoutData(data);
		}

		composite.setTabList(new Control[] { uriField, buttonComposite });
		return composite;
	}

	/**
	 * Called to prepare the Browse Repository button.
	 */
	protected void prepareBrowseFileSystemButton(Button browseFileSystemButton) {
		browseFileSystemButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {

				//
				DawnExplorer repoView = null;
				IViewReference viewReferences[] = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.getViewReferences();
				for (int i = 0; i < viewReferences.length; i++) {
					if ("org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer"
							.equals(viewReferences[i].getId())) {
						repoView = (DawnExplorer) viewReferences[i]
								.getView(false);
						break;
					}
				}

				// FileDialog fileDialog = new FileDialog(getShell(), style);

				CDOResourceNodeSelectionDialog fileDialog = new CDOResourceNodeSelectionDialog(
						getShell(), repoView.getView(), true) {
					@Override
					protected void initializeBounds() {
						getShell().setSize(500, 700);
					}

				};

				fileDialog.open();
				String fileName = fileDialog.getResults().toString();
				// String filterPath = fileDialog.getFilterPath();
				// if (isMulti())
				// {
				// String[] fileNames = fileDialog.getFileNames();
				// StringBuffer uris = new StringBuffer();

				// for (int i = 0, len = fileNames.length; i < len; i++)
				// {
				// uris.append(URI.createFileURI(filterPath + File.separator +
				// fileNames[i]).toString());
				// uris.append("  ");
				// }
				// uriField.setText((uriField.getText() + "  " +
				// uris.toString()).trim());
				// }
				// else
				// {
				// String fileName = fileDialog.getFileName();
				if (fileName != null && !fileName.equals("cdo://opencert/")) {
					// uriField.setText(URI.createFileURI(filterPath +
					// File.separator + fileName).toString());
					uriField.setText(fileName);
				}
				// }
			}
		});
	}

}