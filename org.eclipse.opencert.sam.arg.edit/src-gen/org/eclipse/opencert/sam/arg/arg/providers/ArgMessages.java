/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.providers;

import org.eclipse.osgi.util.NLS;

/**
 * 
 * 
 */
public class ArgMessages extends NLS {
	
	private static final String BUNDLE_NAME = "org.eclipse.opencert.sam.arg.arg.providers.argMessages"; //$NON-NLS-1$

	
	public static String Case_PropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AssuranceCasePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArgumentationPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String InformationElementCitationPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArgumentElementCitationPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArgumentReasoningPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ClaimPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ChoicePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AssertedInferencePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AssertedEvidencePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AssertedContextPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AssertedChallengePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AssertedCounterEvidencePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AgreementPropertiesEditionPart_PropertiesGroupLabel;


	
	public static String Case__ReadOnly;

	
	public static String Case__Part_Title;

	
	public static String AssuranceCase_ReadOnly;

	
	public static String AssuranceCase_Part_Title;

	
	public static String Argumentation_ReadOnly;

	
	public static String Argumentation_Part_Title;

	
	public static String InformationElementCitation_ReadOnly;

	
	public static String InformationElementCitation_Part_Title;

	
	public static String ArgumentElementCitation_ReadOnly;

	
	public static String ArgumentElementCitation_Part_Title;

	
	public static String ArgumentReasoning_ReadOnly;

	
	public static String ArgumentReasoning_Part_Title;

	
	public static String Claim_ReadOnly;

	
	public static String Claim_Part_Title;

	
	public static String Choice_ReadOnly;

	
	public static String Choice_Part_Title;

	
	public static String AssertedInference_ReadOnly;

	
	public static String AssertedInference_Part_Title;

	
	public static String AssertedEvidence_ReadOnly;

	
	public static String AssertedEvidence_Part_Title;

	
	public static String AssertedContext_ReadOnly;

	
	public static String AssertedContext_Part_Title;

	
	public static String AssertedChallenge_ReadOnly;

	
	public static String AssertedChallenge_Part_Title;

	
	public static String AssertedCounterEvidence_ReadOnly;

	
	public static String AssertedCounterEvidence_Part_Title;

	
	public static String Agreement_ReadOnly;

	
	public static String Agreement_Part_Title;


	
	public static String Case_PropertiesEditionPart_ArgumentLabel;

	
	public static String Case_PropertiesEditionPart_ArgumentationLabel;

	
	public static String Case_PropertiesEditionPart_AgreementLabel;

	
	public static String Case_PropertiesEditionPart_CitedLabel;

	
	public static String Case_PropertiesEditionPart_InformationLabel;

	
	public static String AssuranceCasePropertiesEditionPart_IdLabel;

	
	public static String AssuranceCasePropertiesEditionPart_NameLabel;

	
	public static String AssuranceCasePropertiesEditionPart_HasArgumentLabel;

	
	public static String AssuranceCasePropertiesEditionPart_HasEvidenceLabel;

	
	public static String AssuranceCasePropertiesEditionPart_ComposedOfLabel;

	
	public static String ArgumentationPropertiesEditionPart_IdLabel;

	
	public static String ArgumentationPropertiesEditionPart_NameLabel;

	
	public static String ArgumentationPropertiesEditionPart_DescriptionLabel;

	
	public static String ArgumentationPropertiesEditionPart_ContentLabel;

	
	public static String ArgumentationPropertiesEditionPart_EvaluationLabel;

	
	public static String ArgumentationPropertiesEditionPart_LifecycleEventLabel;

	
	public static String ArgumentationPropertiesEditionPart_LocationLabel;

	
	public static String ArgumentationPropertiesEditionPart_ArgumentationLabel;

	
	public static String ArgumentationPropertiesEditionPart_ConsistOfLabel;

	
	public static String InformationElementCitationPropertiesEditionPart_IdLabel;

	
	public static String InformationElementCitationPropertiesEditionPart_NameLabel;

	
	public static String InformationElementCitationPropertiesEditionPart_DescriptionLabel;

	
	public static String InformationElementCitationPropertiesEditionPart_ContentLabel;

	
	public static String InformationElementCitationPropertiesEditionPart_TypeLabel;

	
	public static String InformationElementCitationPropertiesEditionPart_ToBeInstantiatedLabel;

	
	public static String InformationElementCitationPropertiesEditionPart_UrlLabel;

	
	public static String InformationElementCitationPropertiesEditionPart_ArtefactLabel;

	
	public static String ArgumentElementCitationPropertiesEditionPart_IdLabel;

	
	public static String ArgumentElementCitationPropertiesEditionPart_NameLabel;

	
	public static String ArgumentElementCitationPropertiesEditionPart_DescriptionLabel;

	
	public static String ArgumentElementCitationPropertiesEditionPart_ContentLabel;

	
	public static String ArgumentElementCitationPropertiesEditionPart_CitedTypeLabel;

	
	public static String ArgumentElementCitationPropertiesEditionPart_ArgumentationReferenceLabel;

	
	public static String ArgumentReasoningPropertiesEditionPart_IdLabel;

	
	public static String ArgumentReasoningPropertiesEditionPart_NameLabel;

	
	public static String ArgumentReasoningPropertiesEditionPart_DescriptionLabel;

	
	public static String ArgumentReasoningPropertiesEditionPart_ContentLabel;

	
	public static String ArgumentReasoningPropertiesEditionPart_ToBeSupportedLabel;

	
	public static String ArgumentReasoningPropertiesEditionPart_ToBeInstantiatedLabel;

	
	public static String ArgumentReasoningPropertiesEditionPart_HasStructureLabel;

	
	public static String ClaimPropertiesEditionPart_IdLabel;

	
	public static String ClaimPropertiesEditionPart_NameLabel;

	
	public static String ClaimPropertiesEditionPart_DescriptionLabel;

	
	public static String ClaimPropertiesEditionPart_ContentLabel;

	
	public static String ClaimPropertiesEditionPart_EvaluationLabel;

	
	public static String ClaimPropertiesEditionPart_LifecycleEventLabel;

	
	public static String ClaimPropertiesEditionPart_Public_Label;

	
	public static String ClaimPropertiesEditionPart_AssumedLabel;

	
	public static String ClaimPropertiesEditionPart_ToBeSupportedLabel;

	
	public static String ClaimPropertiesEditionPart_ToBeInstantiatedLabel;

	
	public static String ChoicePropertiesEditionPart_IdLabel;

	
	public static String ChoicePropertiesEditionPart_NameLabel;

	
	public static String ChoicePropertiesEditionPart_DescriptionLabel;

	
	public static String ChoicePropertiesEditionPart_ContentLabel;

	
	public static String ChoicePropertiesEditionPart_SourceMultiextensionLabel;

	
	public static String ChoicePropertiesEditionPart_SourceCardinalityLabel;

	
	public static String ChoicePropertiesEditionPart_OptionalityLabel;

	
	public static String AssertedInferencePropertiesEditionPart_IdLabel;

	
	public static String AssertedInferencePropertiesEditionPart_NameLabel;

	
	public static String AssertedInferencePropertiesEditionPart_DescriptionLabel;

	
	public static String AssertedInferencePropertiesEditionPart_ContentLabel;

	
	public static String AssertedInferencePropertiesEditionPart_MultiextensionLabel;

	
	public static String AssertedInferencePropertiesEditionPart_CardinalityLabel;

	
	public static String AssertedInferencePropertiesEditionPart_SourceLabel;

	
	public static String AssertedInferencePropertiesEditionPart_TargetLabel;

	
	public static String AssertedEvidencePropertiesEditionPart_IdLabel;

	
	public static String AssertedEvidencePropertiesEditionPart_NameLabel;

	
	public static String AssertedEvidencePropertiesEditionPart_DescriptionLabel;

	
	public static String AssertedEvidencePropertiesEditionPart_ContentLabel;

	
	public static String AssertedEvidencePropertiesEditionPart_MultiextensionLabel;

	
	public static String AssertedEvidencePropertiesEditionPart_CardinalityLabel;

	
	public static String AssertedEvidencePropertiesEditionPart_SourceLabel;

	
	public static String AssertedEvidencePropertiesEditionPart_TargetLabel;

	
	public static String AssertedContextPropertiesEditionPart_IdLabel;

	
	public static String AssertedContextPropertiesEditionPart_NameLabel;

	
	public static String AssertedContextPropertiesEditionPart_DescriptionLabel;

	
	public static String AssertedContextPropertiesEditionPart_ContentLabel;

	
	public static String AssertedContextPropertiesEditionPart_MultiextensionLabel;

	
	public static String AssertedContextPropertiesEditionPart_CardinalityLabel;

	
	public static String AssertedContextPropertiesEditionPart_SourceLabel;

	
	public static String AssertedContextPropertiesEditionPart_TargetLabel;

	
	public static String AssertedChallengePropertiesEditionPart_IdLabel;

	
	public static String AssertedChallengePropertiesEditionPart_NameLabel;

	
	public static String AssertedChallengePropertiesEditionPart_DescriptionLabel;

	
	public static String AssertedChallengePropertiesEditionPart_ContentLabel;

	
	public static String AssertedChallengePropertiesEditionPart_SourceLabel;

	
	public static String AssertedChallengePropertiesEditionPart_TargetLabel;

	
	public static String AssertedCounterEvidencePropertiesEditionPart_IdLabel;

	
	public static String AssertedCounterEvidencePropertiesEditionPart_NameLabel;

	
	public static String AssertedCounterEvidencePropertiesEditionPart_DescriptionLabel;

	
	public static String AssertedCounterEvidencePropertiesEditionPart_ContentLabel;

	
	public static String AssertedCounterEvidencePropertiesEditionPart_SourceLabel;

	
	public static String AssertedCounterEvidencePropertiesEditionPart_TargetLabel;

	
	public static String AgreementPropertiesEditionPart_IdLabel;

	
	public static String AgreementPropertiesEditionPart_NameLabel;

	
	public static String AgreementPropertiesEditionPart_DescriptionLabel;

	
	public static String AgreementPropertiesEditionPart_ContentLabel;

	
	public static String AgreementPropertiesEditionPart_EvaluationLabel;

	
	public static String AgreementPropertiesEditionPart_LifecycleEventLabel;

	
	public static String AgreementPropertiesEditionPart_LocationLabel;

	
	public static String AgreementPropertiesEditionPart_ArgumentationLabel;

	
	public static String AgreementPropertiesEditionPart_ConsistOfLabel;

	
	public static String AgreementPropertiesEditionPart_BetweenLabel;


	
	public static String PropertiesEditionPart_DocumentationLabel;

	
	public static String PropertiesEditionPart_IntegerValueMessage;

	
	public static String PropertiesEditionPart_FloatValueMessage;

	
	public static String PropertiesEditionPart_ShortValueMessage;

	
	public static String PropertiesEditionPart_LongValueMessage;

	
	public static String PropertiesEditionPart_ByteValueMessage;

	
	public static String PropertiesEditionPart_BigIntegerValueMessage;

	
	public static String PropertiesEditionPart_BigDecimalValueMessage;

	
	public static String PropertiesEditionPart_DoubleValueMessage;

	
	public static String PropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PropertiesEditionPart_RequiredFeatureMessage;

	
	public static String PropertiesEditionPart_AddTableViewerLabel;

	
	public static String PropertiesEditionPart_EditTableViewerLabel;

	
	public static String PropertiesEditionPart_RemoveTableViewerLabel;

	
	public static String PropertiesEditionPart_AddListViewerLabel;

	
	public static String PropertiesEditionPart_RemoveListViewerLabel;

	// Start of user code for additionnal NLS Constants
	
	// End of user code

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, ArgMessages.class);
	}

	
	private ArgMessages() {
		//protect instanciation
	}
}
