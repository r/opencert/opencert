/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class BaseRequirementBasePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for ownedRel ReferencesTable
	 */
	protected ReferencesTableSettings ownedRelSettings;
	
	/**
	 * Settings for subRequirement ReferencesTable
	 */
	protected ReferencesTableSettings subRequirementSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaseRequirementBasePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseRequirement, String editing_mode) {
		super(editingContext, baseRequirement, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = BaselineViewsRepository.class;
		partKey = BaselineViewsRepository.BaseRequirement.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaseRequirement baseRequirement = (BaseRequirement)elt;
			final BaseRequirementPropertiesEditionPart basePart = (BaseRequirementPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseRequirement.getId()));
			
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseRequirement.getName()));
			
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.description))
				basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, baseRequirement.getDescription()));
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.reference))
				basePart.setReference(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseRequirement.getReference()));
			
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.assumptions))
				basePart.setAssumptions(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseRequirement.getAssumptions()));
			
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.rationale))
				basePart.setRationale(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseRequirement.getRationale()));
			
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.image))
				basePart.setImage(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseRequirement.getImage()));
			
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.annotations))
				basePart.setAnnotations(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseRequirement.getAnnotations()));
			
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.ownedRel)) {
				ownedRelSettings = new ReferencesTableSettings(baseRequirement, BaselinePackage.eINSTANCE.getBaseRequirement_OwnedRel());
				basePart.initOwnedRel(ownedRelSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.subRequirement)) {
				subRequirementSettings = new ReferencesTableSettings(baseRequirement, BaselinePackage.eINSTANCE.getBaseRequirement_SubRequirement());
				basePart.initSubRequirement(subRequirementSettings);
			}
			// init filters
			
			
			
			
			
			
			
			
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.ownedRel)) {
				basePart.addFilterToOwnedRel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseRequirementRel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRel
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseRequirement.Properties.subRequirement)) {
				basePart.addFilterToSubRequirement(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseRequirement); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for subRequirement
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}













	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == BaselineViewsRepository.BaseRequirement.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == BaselineViewsRepository.BaseRequirement.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == BaselineViewsRepository.BaseRequirement.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == BaselineViewsRepository.BaseRequirement.Properties.reference) {
			return BaselinePackage.eINSTANCE.getBaseRequirement_Reference();
		}
		if (editorKey == BaselineViewsRepository.BaseRequirement.Properties.assumptions) {
			return BaselinePackage.eINSTANCE.getBaseRequirement_Assumptions();
		}
		if (editorKey == BaselineViewsRepository.BaseRequirement.Properties.rationale) {
			return BaselinePackage.eINSTANCE.getBaseRequirement_Rationale();
		}
		if (editorKey == BaselineViewsRepository.BaseRequirement.Properties.image) {
			return BaselinePackage.eINSTANCE.getBaseRequirement_Image();
		}
		if (editorKey == BaselineViewsRepository.BaseRequirement.Properties.annotations) {
			return BaselinePackage.eINSTANCE.getBaseRequirement_Annotations();
		}
		if (editorKey == BaselineViewsRepository.BaseRequirement.Properties.ownedRel) {
			return BaselinePackage.eINSTANCE.getBaseRequirement_OwnedRel();
		}
		if (editorKey == BaselineViewsRepository.BaseRequirement.Properties.subRequirement) {
			return BaselinePackage.eINSTANCE.getBaseRequirement_SubRequirement();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaseRequirement baseRequirement = (BaseRequirement)semanticObject;
		if (BaselineViewsRepository.BaseRequirement.Properties.id == event.getAffectedEditor()) {
			baseRequirement.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseRequirement.Properties.name == event.getAffectedEditor()) {
			baseRequirement.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseRequirement.Properties.description == event.getAffectedEditor()) {
			baseRequirement.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseRequirement.Properties.reference == event.getAffectedEditor()) {
			baseRequirement.setReference((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseRequirement.Properties.assumptions == event.getAffectedEditor()) {
			baseRequirement.setAssumptions((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseRequirement.Properties.rationale == event.getAffectedEditor()) {
			baseRequirement.setRationale((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseRequirement.Properties.image == event.getAffectedEditor()) {
			baseRequirement.setImage((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseRequirement.Properties.annotations == event.getAffectedEditor()) {
			baseRequirement.setAnnotations((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseRequirement.Properties.ownedRel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRelSettings.move(event.getNewIndex(), (BaseRequirementRel) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseRequirement.Properties.subRequirement == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, subRequirementSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				subRequirementSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				subRequirementSettings.move(event.getNewIndex(), (BaseRequirement) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaseRequirementPropertiesEditionPart basePart = (BaseRequirementPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseRequirement.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseRequirement.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseRequirement.Properties.description)){
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseRequirement_Reference().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseRequirement.Properties.reference)) {
				if (msg.getNewValue() != null) {
					basePart.setReference(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setReference("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseRequirement_Assumptions().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseRequirement.Properties.assumptions)) {
				if (msg.getNewValue() != null) {
					basePart.setAssumptions(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setAssumptions("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseRequirement_Rationale().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseRequirement.Properties.rationale)) {
				if (msg.getNewValue() != null) {
					basePart.setRationale(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setRationale("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseRequirement_Image().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseRequirement.Properties.image)) {
				if (msg.getNewValue() != null) {
					basePart.setImage(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setImage("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseRequirement_Annotations().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseRequirement.Properties.annotations)) {
				if (msg.getNewValue() != null) {
					basePart.setAnnotations(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setAnnotations("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseRequirement_OwnedRel().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseRequirement.Properties.ownedRel))
				basePart.updateOwnedRel();
			if (BaselinePackage.eINSTANCE.getBaseRequirement_SubRequirement().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseRequirement.Properties.subRequirement))
				basePart.updateSubRequirement();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			BaselinePackage.eINSTANCE.getBaseRequirement_Reference(),
			BaselinePackage.eINSTANCE.getBaseRequirement_Assumptions(),
			BaselinePackage.eINSTANCE.getBaseRequirement_Rationale(),
			BaselinePackage.eINSTANCE.getBaseRequirement_Image(),
			BaselinePackage.eINSTANCE.getBaseRequirement_Annotations(),
			BaselinePackage.eINSTANCE.getBaseRequirement_OwnedRel(),
			BaselinePackage.eINSTANCE.getBaseRequirement_SubRequirement()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (BaselineViewsRepository.BaseRequirement.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseRequirement.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseRequirement.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseRequirement.Properties.reference == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseRequirement_Reference().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseRequirement_Reference().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseRequirement.Properties.assumptions == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseRequirement_Assumptions().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseRequirement_Assumptions().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseRequirement.Properties.rationale == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseRequirement_Rationale().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseRequirement_Rationale().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseRequirement.Properties.image == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseRequirement_Image().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseRequirement_Image().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseRequirement.Properties.annotations == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseRequirement_Annotations().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseRequirement_Annotations().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
