/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;
import org.eclipse.opencert.infra.general.general.ActivityRelKind;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Activity Rel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityRelImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityRelImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityRelImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BaseActivityRelImpl extends CDOObjectImpl implements BaseActivityRel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseActivityRelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BaselinePackage.Literals.BASE_ACTIVITY_REL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityRelKind getType() {
		return (ActivityRelKind)eGet(BaselinePackage.Literals.BASE_ACTIVITY_REL__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(ActivityRelKind newType) {
		eSet(BaselinePackage.Literals.BASE_ACTIVITY_REL__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseActivity getSource() {
		return (BaseActivity)eGet(BaselinePackage.Literals.BASE_ACTIVITY_REL__SOURCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(BaseActivity newSource) {
		eSet(BaselinePackage.Literals.BASE_ACTIVITY_REL__SOURCE, newSource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseActivity getTarget() {
		return (BaseActivity)eGet(BaselinePackage.Literals.BASE_ACTIVITY_REL__TARGET, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(BaseActivity newTarget) {
		eSet(BaselinePackage.Literals.BASE_ACTIVITY_REL__TARGET, newTarget);
	}

} //BaseActivityRelImpl
