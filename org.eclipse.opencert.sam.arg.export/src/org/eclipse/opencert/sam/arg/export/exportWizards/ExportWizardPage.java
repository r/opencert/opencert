/*******************************************************************************
 * Copyright (c) 2017 Fundación Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Alejandra Ruiz - initial development and documentation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.export.exportWizards;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;

//public class ExportWizardPage extends WizardExportResourcesPage {
public class ExportWizardPage extends WizardPage {
	
	public static final String ARGUMENT_MODEL_SELECTION = "Argumentation Model Selection";
	private static final String ARGMODEL = ".arg";	
	
	private FileFieldEditor editor;
	private List argList;
	protected ArrayList<String> argListDir;

	private CDOView viewCDO=null;
	private CDOSession sessionCDO = null;
	private ArrayList<CDOResourceNode> argModelList;
    private Composite container;
    public String newFile;
    public IPath path;
    public CDOResourceNode selectedSource;
	
	
	public ExportWizardPage(String pageName, IStructuredSelection selection) {
		super(pageName);
		setTitle(pageName); //NON-NLS-1
		setDescription("Export an argument model from the database into a file with SACM format"); //NON-NLS-1
	}

	public void createControl(Composite parent) {	
		
        container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 3;
        Label labelsource = new Label(container, SWT.NONE);
        labelsource.setText("Select the argumentation model to export");
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace=true;
        gridData.horizontalSpan=3;
        labelsource.setLayoutData(gridData);
        argList = new List(container, SWT.FILL | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);
        argList.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int sourceindex=argList.getSelectionIndex();			
				selectedSource=argModelList.get(sourceindex);
			}
        });
        
        createCDOSourceGroup();
        GridData gridData2 = new GridData();
        gridData2.grabExcessHorizontalSpace=true;
        gridData2.horizontalSpan=3;
        argList.setLayoutData(gridData2);     
        editor = new FileFieldEditor("fileSelect","Select File Destination: ",container); //NON-NLS-1 //NON-NLS-2
        editor.getTextControl(container).addModifyListener(new ModifyListener(){
			public void modifyText(ModifyEvent e) {
				path = new Path(ExportWizardPage.this.editor.getStringValue());
				newFile=path.addFileExtension("arg").toString();
			    setPageComplete(true);
			}
		});
		
		String[] extensions = new String[] { "*.arg" }; //NON-NLS-1
		editor.setFileExtensions(extensions);
        setControl(container);
        setPageComplete(false);
	}

	public void createCDOSourceGroup(){
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		CDOView [] views = sessionCDO.getViews();
		if (views.length!=0) {
			viewCDO = views[0];				
		}
		argListDir = new ArrayList<String>();
		argModelList=findArgumentModels();
		Iterator<CDOResourceNode> iterator = argModelList.iterator();
		while(iterator.hasNext()){
			CDOResourceNode proj=iterator.next();		
			String name=proj.getPath();
			argList.add(name);
		}
	}
	ArrayList<CDOResourceNode> findArgumentModels(){
		ArrayList<CDOResourceNode> argList=new ArrayList<CDOResourceNode>();
		CDOResourceNode[] listR=  viewCDO.getElements();			
		for (int i = 0; i < listR.length; i++) {
			if (listR[i] instanceof CDOResourceFolder) {
				checkFolderContents((CDOResourceFolder) listR[i],ARGMODEL, argList);
			} else if (listR[i].getName().endsWith(ARGMODEL)) {
				argList.add(listR[i]);
			}
		}
		return argList;
	}
	
	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	private ArrayList<CDOResourceNode> checkFolderContents(CDOResourceFolder cdoResourceFolder, String sCadena, ArrayList<CDOResourceNode>argList) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN = cdoResourceFolder.getNodes();
			for (int i = 0; i < listN.size(); i++) {
				if (listN.get(i) instanceof CDOResourceFolder) {
					checkFolderContents((CDOResourceFolder) listN.get(i), sCadena, argList);
				} else if (listN.get(i).getName().endsWith(sCadena)) {
					argList.add(listN.get(i));
				}
			}
		}
		return argList;
	}
}
