/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.evm.evidspec.evidence.provider;


import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetFactory;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.provider.ManageableAssuranceAssetItemProvider;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.evm.evidspec.evidence.util.ArtefactModificationController;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.infra.general.general.GeneralPackage;


/**
 * This is the item provider adapter for a {@link org.eclipse.opencert.evm.evidspec.evidence.Artefact} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ArtefactItemProvider
	extends ManageableAssuranceAssetItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	
	
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArtefactItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addDescriptionPropertyDescriptor(object);
			addVersionIDPropertyDescriptor(object);
			addDatePropertyDescriptor(object);
			addChangesPropertyDescriptor(object);
			addIsLastVersionPropertyDescriptor(object);
			addPrecedentVersionPropertyDescriptor(object);
			addIsTemplatePropertyDescriptor(object);
			addIsConfigurablePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_NamedElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_NamedElement_id_feature", "_UI_NamedElement_type"),
				 GeneralPackage.Literals.NAMED_ELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_NamedElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_NamedElement_name_feature", "_UI_NamedElement_type"),
				 GeneralPackage.Literals.NAMED_ELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DescribableElement_description_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DescribableElement_description_feature", "_UI_DescribableElement_type"),
				 GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Version ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVersionIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Artefact_versionID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Artefact_versionID_feature", "_UI_Artefact_type"),
				 EvidencePackage.Literals.ARTEFACT__VERSION_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Date feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Artefact_date_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Artefact_date_feature", "_UI_Artefact_type"),
				 EvidencePackage.Literals.ARTEFACT__DATE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Changes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChangesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Artefact_changes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Artefact_changes_feature", "_UI_Artefact_type"),
				 EvidencePackage.Literals.ARTEFACT__CHANGES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Last Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsLastVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Artefact_isLastVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Artefact_isLastVersion_feature", "_UI_Artefact_type"),
				 EvidencePackage.Literals.ARTEFACT__IS_LAST_VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Precedent Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrecedentVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Artefact_precedentVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Artefact_precedentVersion_feature", "_UI_Artefact_type"),
				 EvidencePackage.Literals.ARTEFACT__PRECEDENT_VERSION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Template feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsTemplatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Artefact_isTemplate_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Artefact_isTemplate_feature", "_UI_Artefact_type"),
				 EvidencePackage.Literals.ARTEFACT__IS_TEMPLATE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Configurable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsConfigurablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Artefact_isConfigurable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Artefact_isConfigurable_feature", "_UI_Artefact_type"),
				 EvidencePackage.Literals.ARTEFACT__IS_CONFIGURABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(EvidencePackage.Literals.ARTEFACT__PROPERTY_VALUE);
			childrenFeatures.add(EvidencePackage.Literals.ARTEFACT__ARTEFACT_PART);
			childrenFeatures.add(EvidencePackage.Literals.ARTEFACT__OWNED_REL);
			childrenFeatures.add(EvidencePackage.Literals.ARTEFACT__RESOURCE);
		}
		return childrenFeatures;
	}
	
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Artefact.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Artefact"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Artefact)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Artefact_type") :
			getString("_UI_Artefact_type") + " " + label;
	}

	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	
	public void notifyChanged(Notification notification) {
		///ALC code begin
		ArtefactModificationController amc = ArtefactModificationController.getInstance();	
		//This part for modificaction of the Artefact properties or adding subartefacs	
		if(!notification.isTouch() && (notification.getEventType()== Notification.SET || notification.getEventType()== Notification.UNSET)){							 		
			Object afectado =notification.getNotifier();	
			if(afectado instanceof Artefact){	
								
				Artefact mod= (Artefact)afectado;															
				if(!amc.getModifiedArtefacts().contains(mod)){
					amc.getModifiedArtefacts().add(mod);										
										
					AssuranceassetPackage aaPackage = AssuranceassetPackage.eINSTANCE;
					AssuranceassetFactory aaFactory = aaPackage.getAssuranceassetFactory();					
					AssuranceAssetEvent newEvent =  aaFactory.createAssuranceAssetEvent();
					int id= mod.getLifecycleEvent().size()+ 1;
					newEvent.setId(""+id);
					newEvent.setName("AssuranceAssetEvent " + id);
					newEvent.setDescription("Generated automatically during Artefact modification");
					newEvent.setType(EventKind.MODIFICATION);
					newEvent.setTime(new Date());
					mod.getLifecycleEvent().add(newEvent);										
																					
				}																				
			}
		}
		//To capture a new evaluation of Artefact
		if(notification.getEventType()== Notification.ADD){
			if(notification.getFeature() instanceof EStructuralFeature){
				EStructuralFeature featureModified= (EStructuralFeature)notification.getFeature();
				if(featureModified.getName().equals("artefactPart")){
					Object parent =notification.getNotifier();	
					if(parent instanceof Artefact){	
						
						Object child= notification.getNewValue();
						if(child instanceof Artefact){	
													
							Artefact childArtefact= (Artefact)child;
							
							if(!amc.getModifiedArtefacts().contains(childArtefact)){
								amc.getModifiedArtefacts().add(childArtefact);		
								Artefact parentArtefact= (Artefact)parent;														
								EvidencePackage ePackage = EvidencePackage.eINSTANCE;
								EvidenceFactory eFactory = ePackage.getEvidenceFactory();
								ArtefactRel ar=eFactory.createArtefactRel();
								int idR= parentArtefact.getOwnedRel().size()+ 1;							
								ar.setId(""+idR);							
								ar.setName("ArtefactRel " + idR);
								ar.setDescription("Generated automatically during the creation of the artefactPart");
								ar.setModificationEffect(ChangeEffectKind.MODIFY);
								ar.setRevocationEffect(ChangeEffectKind.MODIFY);
								ar.setSource(parentArtefact);
								ar.setTarget(childArtefact);
							
								parentArtefact.getOwnedRel().add(ar);
								
								AssuranceassetPackage aaPackage = AssuranceassetPackage.eINSTANCE;
								AssuranceassetFactory aaFactory = aaPackage.getAssuranceassetFactory();					
								AssuranceAssetEvent newEvent =  aaFactory.createAssuranceAssetEvent();
								int id= childArtefact.getLifecycleEvent().size()+ 1;
								newEvent.setId(""+id);
								newEvent.setName("AssuranceAssetEvent " + id);
								newEvent.setDescription("Generated automatically during Artefact modification");
								newEvent.setType(EventKind.MODIFICATION);
								newEvent.setTime(new Date());
								childArtefact.getLifecycleEvent().add(newEvent);	
							}							
						}																															
					}
				}
				
				else if(featureModified.getName().equals("evaluation")){
					Object afectado =notification.getNotifier();	
					if(afectado instanceof Artefact){	
						Artefact ev= (Artefact)afectado;
						if(!amc.getEvaluatedArtefacts().contains(ev)){
							amc.getEvaluatedArtefacts().add(ev);									
							AssuranceassetPackage aaPackage = AssuranceassetPackage.eINSTANCE;
							AssuranceassetFactory aaFactory = aaPackage.getAssuranceassetFactory();		
							AssuranceAssetEvent newEvaluationEvent =  aaFactory.createAssuranceAssetEvent();
							int id= ev.getLifecycleEvent().size()+ 1;
							newEvaluationEvent.setId(""+id);
							newEvaluationEvent.setName("AssuranceAssetEvent " + id);
							newEvaluationEvent.setDescription("Generated automatically during Artefact evaluation");
							newEvaluationEvent.setType(EventKind.EVALUATION);
							newEvaluationEvent.setTime(new Date());
							EList<EObject> aList = ((EList<EObject>) ev.eGet(featureModified));
							newEvaluationEvent.setResultingEvaluation((AssuranceAssetEvaluation)aList.get(0));
							ev.getLifecycleEvent().add(newEvaluationEvent);
						}
					}
				}
			}
		}
		///ALC code end
		updateChildren(notification);

		switch (notification.getFeatureID(Artefact.class)) {
			case EvidencePackage.ARTEFACT__ID:
			case EvidencePackage.ARTEFACT__NAME:
			case EvidencePackage.ARTEFACT__DESCRIPTION:
			case EvidencePackage.ARTEFACT__VERSION_ID:
			case EvidencePackage.ARTEFACT__DATE:
			case EvidencePackage.ARTEFACT__CHANGES:
			case EvidencePackage.ARTEFACT__IS_LAST_VERSION:
			case EvidencePackage.ARTEFACT__IS_TEMPLATE:
			case EvidencePackage.ARTEFACT__IS_CONFIGURABLE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case EvidencePackage.ARTEFACT__PROPERTY_VALUE:
			case EvidencePackage.ARTEFACT__ARTEFACT_PART:
			case EvidencePackage.ARTEFACT__OWNED_REL:
			case EvidencePackage.ARTEFACT__RESOURCE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(EvidencePackage.Literals.ARTEFACT__PROPERTY_VALUE,
				 EvidenceFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(EvidencePackage.Literals.ARTEFACT__ARTEFACT_PART,
				 EvidenceFactory.eINSTANCE.createArtefact()));

		newChildDescriptors.add
			(createChildParameter
				(EvidencePackage.Literals.ARTEFACT__OWNED_REL,
				 EvidenceFactory.eINSTANCE.createArtefactRel()));

		newChildDescriptors.add
			(createChildParameter
				(EvidencePackage.Literals.ARTEFACT__RESOURCE,
				 EvidenceFactory.eINSTANCE.createResource()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return EvidenceEditPlugin.INSTANCE;
	}

	
}
