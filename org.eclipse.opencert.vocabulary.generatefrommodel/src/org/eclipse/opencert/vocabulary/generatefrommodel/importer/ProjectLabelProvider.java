/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.generatefrommodel.importer;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.opencert.vocabulary.generatefrommodel.Activator;

public class ProjectLabelProvider extends LabelProvider {
	private Image projectImage;
	private Image projectClosedImage;
	private Image folderImage;
	private Image documentImage;

	public ProjectLabelProvider() {
		if (projectImage == null) {
			projectImage = ImageDescriptor.createFromURL(
					FileLocator.find(Activator.getDefault().getBundle(),
							new Path("icons/project.png"), null)).createImage();
		}
		if (projectClosedImage == null) {
			projectClosedImage = ImageDescriptor.createFromURL(
					FileLocator.find(Activator.getDefault().getBundle(),
							new Path("icons/project-closed.png"), null))
					.createImage();
		}
		if (folderImage == null) {
			folderImage = ImageDescriptor
					.createFromURL(
							FileLocator
									.find(Activator.getDefault().getBundle(),
											new Path(
													"icons/famfamfam_silk_icons_v013/icons/folder.png"),
											null)).createImage();
		}
		if (documentImage == null) {
			documentImage = ImageDescriptor
					.createFromURL(
							FileLocator
									.find(Activator.getDefault().getBundle(),
											new Path(
													"icons/famfamfam_silk_icons_v013/icons/page.png"),
											null)).createImage();
		}
	}

	@Override
	public String getText(Object element) {
		String result = null;
		if (element instanceof IResource) {
			return ((IResource) element).getName();
		}

		return result;
	}

	@Override
	public Image getImage(Object element) {
		Image result = null;
		if (element instanceof IProject) {
			if (((IProject) element).isOpen()) {
				result = projectImage;
			} else {
				result = projectClosedImage;
			}
		} else if (element instanceof IFolder) {
			result = folderImage;
		} else if (element instanceof IFile) {
			result = documentImage;
		}

		return result;
	}

	@Override
	public void dispose() {
		projectImage.dispose();
		projectClosedImage.dispose();
		folderImage.dispose();
		documentImage.dispose();

		super.dispose();
	}
}