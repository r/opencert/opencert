/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.LayoutConstraint;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

// Start MCP
public class LoadViewLocations {
    private CDOResource sourceFile;
	//Map<String, Point> objectLocations = new HashMap<String, Point>();    
	Map<String, LayoutConstraint> objectLocations = new HashMap<String, LayoutConstraint>();
	
    /**
     * @generated NOT
     */
    public LoadViewLocations (
					CDOResource sourceFile) {
            this.sourceFile = sourceFile;
    }

	
	/**
	 * @generated NOT
	 */
	// chapuzilla MCP se podria mejorar guardando el ID del origen!!!
	private String traducir(String claseRef)
	{
		String ret = "";
		if(claseRef.contains(RefframeworkPackage.eINSTANCE.getRefActivity().getName()))
		 ret = BaselinePackage.eINSTANCE.getBaseActivity().getName();
		else if(claseRef.contains(RefframeworkPackage.eINSTANCE.getRefArtefact().getName()))
		 ret = BaselinePackage.eINSTANCE.getBaseArtefact().getName();
		else if(claseRef.contains(RefframeworkPackage.eINSTANCE.getRefRole().getName()))
   		 ret = BaselinePackage.eINSTANCE.getBaseRole().getName();
		        		
		return ret;
	}

	/**
	 * @generated NOT
	 */
	private void LoadChildrenLocations(Object editPart0) {
		if(editPart0 == null) {
			return;
		}
		Node editPart = (Node) editPart0;
		for(Object child : editPart.getChildren()) {
			if(child instanceof Node) {
				LoadChildrenLocations(((Object)child));
			}
		}

		if(editPart instanceof Node)
		{       
			Node node= (Node)editPart;
			
			//String objectId = aa.getID(view);

    		//<layoutConstraint xmi:type="notation:Bounds" xmi:id="_HxWc8zZfEeOqz6ZKAHqxFQ" x="440" y="200" width="96" height="91"/>    					
			LayoutConstraint lc = node.getLayoutConstraint();    					

			// hay varios elementos con el mismo UUID 
			// Guardo no el id de la view sino del modelo
			/* PROBLEMA los nuevos ficheros (diagrama y modelo tienen UUIDs muy diferentes!!!!)
			System.out.println("key = "+ objectId+view.getElement().getClass().getName());    					
			objectLocations.put(objectId+view.getElement().getClass().getName(), pt);
			*/
			NamedElement me = (NamedElement)node.getElement();
			//MCP: OJO los UUIDs han cambiado porque he copiado los modelos y ademas dan problemas las superclases en el caso de AgreementImpl!!!!
			// solo si guardo traza transformacion podre hacerlo correctamente 
			//me.eClass().getName()
			System.out.println("LoadChildrenLocations key = "+ me.getId()+me.getName()+me.getClass().getName());    					
			objectLocations.put(me.getId()+me.getName()+traducir(me.eClass().getName()), lc);
		}
	}
	
	/**
	 * @generated NOT
	 */
	public void LoadObjectLocations()
	{
		CDOResource aa = sourceFile;
		DiagramImpl d = (DiagramImpl)aa.getContents().get(0);
		for(Object elem0 : d.getChildren()) 
		{
			if(elem0 instanceof Node)
			{       
				Node view= (Node)elem0;
				
				//String objectId = aa.getID(view);

        		//<layoutConstraint xmi:type="notation:Bounds" xmi:id="_HxWc8zZfEeOqz6ZKAHqxFQ" x="440" y="200" width="96" height="91"/>    					
				LayoutConstraint lc = view.getLayoutConstraint();    					

				// hay varios elementos con el mismo UUID 
				// Guardo no el id de la view sino del modelo
				/* PROBLEMA los nuevos ficheros (diagrama y modelo tienen UUIDs muy diferentes!!!!)
				System.out.println("key = "+ objectId+view.getElement().getClass().getName());    					
				objectLocations.put(objectId+view.getElement().getClass().getName(), pt);
				*/
				NamedElement me = (NamedElement)view.getElement();
				//MCP: OJO los UUIDs han cambiado porque he copiado los modelos y ademas dan problemas las superclases en el caso de AgreementImpl!!!!
				// solo si guardo traza transformacion podre hacerlo correctamente 
				//me.eClass().getName()
				System.out.println("LoadObjectLocations key = "+ me.getId()+me.getName()+me.getClass().getName());    					
				//MCP???objectLocations.put(me.getId()+me.getName()+traducir(me.eClass().getName()), lc);
				LoadChildrenLocations(elem0);
			}
		}
	}
	
	
	public LayoutConstraint getObjectLocation (String key){
		return objectLocations.get(key);			
	}

}
// End MCP