/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.baseline.baseline.diagram.part;

import org.eclipse.emf.cdo.dawn.editors.IDawnEditor;
import org.eclipse.emf.cdo.dawn.editors.IDawnEditorSupport;
import org.eclipse.emf.cdo.dawn.gmf.editors.impl.DawnGMFEditorSupport;
import org.eclipse.emf.cdo.dawn.ui.DawnEditorInput;
import org.eclipse.emf.cdo.ui.CDOEditorInput;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.net4j.util.transaction.TransactionException;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.document.AbstractDocumentProvider;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;

public class DawnBaselineDiagramEditor extends BaselineDiagramEditor
		implements
			IDawnEditor {
	public static String ID = "org.eclipse.opencert.apm.baseline.baseline.diagram.part.DawnBaselineDiagramEditor";

	private IDawnEditorSupport dawnEditorSupport;

	public DawnBaselineDiagramEditor() {
		super();
		setDocumentProvider(new DawnBaselineDocumentProvider());
		setDawnEditorSupport(new DawnGMFEditorSupport(this));
	}

	/**
	 * @generated NOT
	 */	
	@Override
	public void setInput(IEditorInput input) {
		// Start MCP
		URIEditorInput editorInput = null;
		DawnEditorInput input2 = null;
		if(input instanceof DawnEditorInput)
		{
			input2 = (DawnEditorInput)input;				
		}
		else
		{
			editorInput = (URIEditorInput) input;
			input2 = new DawnEditorInput(editorInput.getURI());
		}
		// End MCP
	
		try {
			doSetInput(input2, true);
		} catch (CoreException x) {
			x.printStackTrace(System.err);
			String title = x.getMessage();
			String msg = x.getMessage();
			Shell shell = getSite().getShell();
			ErrorDialog.openError(shell, title, msg, x.getStatus());
		}

		dawnEditorSupport.setView(((DawnEditorInput) input2).getView());
	}

	@Override
	protected void initializeGraphicalViewer() {
		super.initializeGraphicalViewer();
		dawnEditorSupport.registerListeners();
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		try {
			dawnEditorSupport.setDirty(false);
			updateState(getEditorInput());
			validateState(getEditorInput());
			performSave(false, monitor);
		} catch (TransactionException e) {
			if (e.getMessage().contains("conflict")) {
				MessageDialog.openError(Display.getDefault().getActiveShell(),
						"conflict",
						"Your Resource is in conflict and cannot be committed");
			} else {
				throw e;
			}
		}
	}

	@Override
	public boolean isDirty() {
		return dawnEditorSupport.isDirty();
	}

	public String getContributorID() {
		return ID;
	}

	@Override
	protected void setDocumentProvider(IEditorInput input) {
		if (input instanceof IFileEditorInput
				|| input instanceof URIEditorInput
				|| input instanceof CDOEditorInput) {
			setDocumentProvider(getDocumentProvider());
		} else {
			super.setDocumentProvider(input);
		}
	}

	@Override
	public void dispose() {
		try {
			super.dispose();
		} finally {
			dawnEditorSupport.close();
		}
	}

	public CDOView getView() {
		return dawnEditorSupport.getView();
	}

	public void setDirty() {
		dawnEditorSupport.setDirty(true);
		((AbstractDocumentProvider) getDocumentProvider())
				.changed(getEditorInput());
	}

	public void setDawnEditorSupport(IDawnEditorSupport dawnEditorSupport) {
		this.dawnEditorSupport = dawnEditorSupport;
	}

	public IDawnEditorSupport getDawnEditorSupport() {
		return dawnEditorSupport;
	}
}
