/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;
import org.eclipse.opencert.vocabulary.diagram.edit.commands.CategoryCreateCommand;
import org.eclipse.opencert.vocabulary.diagram.edit.commands.SourceOfDefinitionCreateCommand;
import org.eclipse.opencert.vocabulary.diagram.edit.commands.TermCreateCommand;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyElementTypes;

/**
 * @generated
 */
public class VocabularyItemSemanticEditPolicy extends
		VocabularyBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public VocabularyItemSemanticEditPolicy() {
		super(VocabularyElementTypes.Vocabulary_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (VocabularyElementTypes.Term_2001 == req.getElementType()) {
			return getGEFWrapper(new TermCreateCommand(req));
		}
		if (VocabularyElementTypes.Category_2002 == req.getElementType()) {
			return getGEFWrapper(new CategoryCreateCommand(req));
		}
		if (VocabularyElementTypes.SourceOfDefinition_2003 == req
				.getElementType()) {
			return getGEFWrapper(new SourceOfDefinitionCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
