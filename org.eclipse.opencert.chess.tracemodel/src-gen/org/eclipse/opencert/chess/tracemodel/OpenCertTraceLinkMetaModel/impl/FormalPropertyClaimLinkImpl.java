/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage;

import org.eclipse.opencert.sam.arg.arg.Claim;

import org.polarsys.chess.contracts.profile.chesscontract.FormalProperty;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formal Property Claim Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.FormalPropertyClaimLinkImpl#getSources <em>Sources</em>}</li>
 *   <li>{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.FormalPropertyClaimLinkImpl#getTargets <em>Targets</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FormalPropertyClaimLinkImpl extends MinimalEObjectImpl.Container implements FormalPropertyClaimLink {
	/**
	 * The cached value of the '{@link #getSources() <em>Sources</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSources()
	 * @generated
	 * @ordered
	 */
	protected FormalProperty sources;

	/**
	 * The cached value of the '{@link #getTargets() <em>Targets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargets()
	 * @generated
	 * @ordered
	 */
	protected EList<Claim> targets;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormalPropertyClaimLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OpenCertTraceLinkMetaModelPackage.Literals.FORMAL_PROPERTY_CLAIM_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalProperty getSources() {
		if (sources != null && sources.eIsProxy()) {
			InternalEObject oldSources = (InternalEObject)sources;
			sources = (FormalProperty)eResolveProxy(oldSources);
			if (sources != oldSources) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__SOURCES, oldSources, sources));
			}
		}
		return sources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalProperty basicGetSources() {
		return sources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSources(FormalProperty newSources) {
		FormalProperty oldSources = sources;
		sources = newSources;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__SOURCES, oldSources, sources));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Claim> getTargets() {
		if (targets == null) {
			targets = new EObjectResolvingEList<Claim>(Claim.class, this, OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__TARGETS);
		}
		return targets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__SOURCES:
				if (resolve) return getSources();
				return basicGetSources();
			case OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__TARGETS:
				return getTargets();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__SOURCES:
				setSources((FormalProperty)newValue);
				return;
			case OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__TARGETS:
				getTargets().clear();
				getTargets().addAll((Collection<? extends Claim>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__SOURCES:
				setSources((FormalProperty)null);
				return;
			case OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__TARGETS:
				getTargets().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__SOURCES:
				return sources != null;
			case OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK__TARGETS:
				return targets != null && !targets.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FormalPropertyClaimLinkImpl
