/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.handlers;

import java.util.Objects;

import org.eclipse.amalgam.explorer.activity.ui.api.actions.OpenSessionAction;
import org.eclipse.amalgam.explorer.activity.ui.api.editor.ActivityExplorerEditor;
import org.eclipse.amalgam.explorer.activity.ui.api.editor.input.ActivityExplorerEditorInput;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.opencert.infra.dashboard.DashboardActivator;
import org.eclipse.opencert.infra.dashboard.helpers.ChessProjectVisitor;
import org.eclipse.opencert.infra.dashboard.helpers.OpenCertExtensions;
import org.eclipse.opencert.infra.dashboard.helpers.SelectedFilesHelper;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * Concrete implementation for command part in Command pattern. Open the dashboard for the current active project.
 *
 * TODO:
 * - handle the opened diagram editor for CDO project
 * - handle multiple graphical editors opened from different CHESS models
 */
public final class OpenDashboardHandler extends AbstractHandler {

	/**
	 * Default constructor.
	 */
	public OpenDashboardHandler() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(final ExecutionEvent pEvent) throws ExecutionException {

		// The dashboard could be opened when the current selection:
		// - is in the Project Explorer, for a Chess project or a CDO project
		// - is in an opened editor, then the current Chess project can be retrieved
		// but not yet for the CDO OpenCert project
		// Other cases should be ignored

		// TODO: handle the opened diagram editor for CDO project

		// Get selection, filtering with UML extension to find only CHESS projects
		SelectedFilesHelper vSelectionFileHelper = new SelectedFilesHelper();
		vSelectionFileHelper.setExtensionFilters(new String[] { OpenCertExtensions.UML_EXTENSION });
		vSelectionFileHelper.initFromEvent(pEvent);

		IProject vCurrentProjet = vSelectionFileHelper.getSelectedProject();
		Session vSession = null;

		// If the project could be found
		if (Objects.nonNull(vCurrentProjet)) {
			// Visit the project to find a graphical file and create a new session
			ChessProjectVisitor vVisitor = new ChessProjectVisitor();

			try {
				vCurrentProjet.accept(vVisitor, IResource.DEPTH_ONE, IResource.NONE);
			} catch (final CoreException pException) {
				DashboardActivator.getDefault().getLog().log(pException.getStatus());
			}

			IFile vGraphicalFile = vVisitor.getFirstGraphicalFile();
			if (Objects.nonNull(vGraphicalFile)) {
				URI vGraphicalFileURI = URI.createPlatformResourceURI(vGraphicalFile.getFullPath().toOSString(), true);

				// Create a new session
				vSession = SessionManager.INSTANCE.openSession(vGraphicalFileURI, new NullProgressMonitor(), null);
			}
		} else {
			// Otherwise, the selected element could be a CDOResource (for OpenCert project)
			Object vSelectedObject = getFirstSelectionObject(pEvent);
			if (vSelectedObject instanceof CDOResource) {
				// TODO: handle the CDO resource
			}
		}

		// Finally, if a session is found or created, open the corresponding activity explorer
		if (Objects.nonNull(vSession)) {
			OpenSessionAction.openActivityExplorer(vSession);
		} else {
			// Otherwise, open the dashboard for a null session
			IWorkbenchPage vActivePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			try {
				vActivePage.openEditor(new ActivityExplorerEditorInput(null, null), ActivityExplorerEditor.ID);
			} catch (PartInitException pException) {
				DashboardActivator.getDefault().getLog().log(pException.getStatus());
			}
		}

		return null;
	}

	/**
	 * Get the first selection object from an execution event.
	 *
	 * @param pEvent
	 *            The execution event
	 * @return The first selected object
	 */
	private Object getFirstSelectionObject(final ExecutionEvent pEvent) {

		Object vFirstSelectedObject = null;

		IWorkbenchWindow vWorkenchWindow = HandlerUtil.getActiveWorkbenchWindow(pEvent);

		if (Objects.nonNull(vWorkenchWindow)) {
			ISelection vCurrentSelection = vWorkenchWindow.getSelectionService().getSelection();

			if (vCurrentSelection instanceof ITreeSelection) {
				vFirstSelectedObject = ((ITreeSelection) vCurrentSelection).getFirstElement();
			}
		}

		return vFirstSelectedObject;
	}
}
