/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.components;

// Start of user code for imports
import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.infra.mappings.mapping.MappingFactory;


// End of user code

/**
 * 
 * 
 */
public class BaselineConfigPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for complianceMapGroup EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings complianceMapGroupSettings;
	
	/**
	 * Settings for refFramework ReferencesTable
	 */
	private ReferencesTableSettings refFrameworkSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaselineConfigPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baselineConfig, String editing_mode) {
		super(editingContext, baselineConfig, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = AssuranceprojectViewsRepository.class;
		partKey = AssuranceprojectViewsRepository.BaselineConfig.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaselineConfig baselineConfig = (BaselineConfig)elt;
			final BaselineConfigPropertiesEditionPart basePart = (BaselineConfigPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baselineConfig.getId()));
			
			if (isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baselineConfig.getName()));
			
			if (isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baselineConfig.getDescription()));
			
			if (isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup)) {
				// init part
				complianceMapGroupSettings = new EObjectFlatComboSettings(baselineConfig, AssuranceprojectPackage.eINSTANCE.getBaselineConfig_ComplianceMapGroup());
				basePart.initComplianceMapGroup(complianceMapGroupSettings);
				// set the button mode
				basePart.setComplianceMapGroupButtonMode(ButtonsModeEnum.BROWSE);
			}
			if (isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive)) {
				basePart.setIsActive(baselineConfig.isIsActive());
			}
			if (isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework)) {
				refFrameworkSettings = new ReferencesTableSettings(baselineConfig, AssuranceprojectPackage.eINSTANCE.getBaselineConfig_RefFramework());
				basePart.initRefFramework(refFrameworkSettings);
			}
			// init filters
			
			
			
			if (isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup)) {
				basePart.addFilterToComplianceMapGroup(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof MapGroup); //$NON-NLS-1$ 
					}
					
				});
				// Start of user code for additional businessfilters for complianceMapGroup
				// End of user code
			}
			
			if (isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework)) {
				basePart.addFilterToRefFramework(new EObjectFilter(BaselinePackage.Literals.BASE_FRAMEWORK));
				// Start of user code for additional businessfilters for refFramework
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}









	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == AssuranceprojectViewsRepository.BaselineConfig.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == AssuranceprojectViewsRepository.BaselineConfig.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == AssuranceprojectViewsRepository.BaselineConfig.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup) {
			return AssuranceprojectPackage.eINSTANCE.getBaselineConfig_ComplianceMapGroup();
		}
		if (editorKey == AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive) {
			return AssuranceprojectPackage.eINSTANCE.getBaselineConfig_IsActive();
		}
		if (editorKey == AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework) {
			return AssuranceprojectPackage.eINSTANCE.getBaselineConfig_RefFramework();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaselineConfig baselineConfig = (BaselineConfig)semanticObject;
		if (AssuranceprojectViewsRepository.BaselineConfig.Properties.id == event.getAffectedEditor()) {
			baselineConfig.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.BaselineConfig.Properties.name == event.getAffectedEditor()) {
			baselineConfig.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.BaselineConfig.Properties.description == event.getAffectedEditor()) {
			baselineConfig.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				complianceMapGroupSettings.setToReference((MapGroup)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				MapGroup eObject = MappingFactory.eINSTANCE.createMapGroup();
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, eObject, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(eObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy != null) {
						policy.execute();
					}
				}
				complianceMapGroupSettings.setToReference(eObject);
			}
		}
		if (AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive == event.getAffectedEditor()) {
			
			//Start IRR
			
			if ((Boolean)event.getNewValue()) {
				AssuranceProject assProject =(AssuranceProject)baselineConfig.eContainer();
				EList<BaselineConfig> allBaseline = assProject.getBaselineConfig();
				Iterator<BaselineConfig> iter = allBaseline.iterator();
				
				while (iter.hasNext()) {
					BaselineConfig oneBase = (BaselineConfig) iter.next();
					oneBase.setIsActive(false);
				}
			}
			
			//End IRR
			
			baselineConfig.setIsActive((Boolean)event.getNewValue());
		}
		if (AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof BaseFramework) {
					refFrameworkSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				refFrameworkSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				refFrameworkSettings.move(event.getNewIndex(), (BaseFramework) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaselineConfigPropertiesEditionPart basePart = (BaselineConfigPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (AssuranceprojectPackage.eINSTANCE.getBaselineConfig_ComplianceMapGroup().equals(msg.getFeature()) && basePart != null && isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup))
				basePart.setComplianceMapGroup((EObject)msg.getNewValue());
			if (AssuranceprojectPackage.eINSTANCE.getBaselineConfig_IsActive().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive))
				basePart.setIsActive((Boolean)msg.getNewValue());
			
			if (AssuranceprojectPackage.eINSTANCE.getBaselineConfig_RefFramework().equals(msg.getFeature())  && isAccessible(AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework))
				basePart.updateRefFramework();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			AssuranceprojectPackage.eINSTANCE.getBaselineConfig_ComplianceMapGroup(),
			AssuranceprojectPackage.eINSTANCE.getBaselineConfig_IsActive(),
			AssuranceprojectPackage.eINSTANCE.getBaselineConfig_RefFramework()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (AssuranceprojectViewsRepository.BaselineConfig.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.BaselineConfig.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.BaselineConfig.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(AssuranceprojectPackage.eINSTANCE.getBaselineConfig_IsActive().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(AssuranceprojectPackage.eINSTANCE.getBaselineConfig_IsActive().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
