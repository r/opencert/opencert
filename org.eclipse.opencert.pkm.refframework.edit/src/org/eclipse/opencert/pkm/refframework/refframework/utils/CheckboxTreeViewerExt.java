/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package  org.eclipse.opencert.pkm.refframework.refframework.utils;

import org.eclipse.jface.viewers.CheckboxTreeViewer;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class CheckboxTreeViewerExt extends CheckboxTreeViewer {

	public CheckboxTreeViewerExt(Composite parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	public CheckboxTreeViewerExt(Tree tree) {
		super(tree);
		// TODO Auto-generated constructor stub
	}

	public CheckboxTreeViewerExt(Composite parent, int style) {
		super(parent, style);
		// TODO Auto-generated constructor stub
	}
	public boolean allBrothersChecked (Object element){
		Item thisItem=(Item)this.findItem(element);
		Item parent = this.getParentItem((Item)thisItem);
		TreeItem[] children=((TreeItem) parent).getItems();
		for (int i=0;i<children.length;i++){
			if (!children[i].getChecked())
				return false;
		}
		return true;
	}
	public boolean allBrothersUnChecked (Object element){
		Item thisItem=(Item)this.findItem(element);
		Item parent = this.getParentItem((Item)thisItem);
		TreeItem[] children=((TreeItem) parent).getItems();
		for (int i=0;i<children.length;i++){
			if (children[i].getChecked())
				return false;
		}
		return true;
	}
	public boolean anyBrotherChecked (Object element){
		Item thisItem=(Item)this.findItem(element);
		Item parent = this.getParentItem((Item)thisItem);
		if (parent != null){
			TreeItem[] children=((TreeItem) parent).getItems();
			for (int i=0;i<children.length;i++){
				if (children[i].getData().equals(element))
					continue;
				if (children[i].getChecked())
					return true;
			}
		}
		return false;
	}
	
	public boolean CheckedElement (Object element){
		
		boolean result = false;
		
		Item thisItem=(Item)this.findItem(element);
		Item parent = this.getParentItem((Item)thisItem);
		if (parent != null){
			TreeItem[] children=((TreeItem) parent).getItems();
			for (int i=0;i<children.length;i++){
				if (children[i].getData().equals(element))
				{
					result = true;
					children[i].setChecked(true);
					return result;
				}
				
			}
		}
		return result;
	}
	
	
	public void setParentsChecked (Object element,boolean state){
		Item thisItem=(Item)this.findItem(element);
		Item parent = this.getParentItem((Item)thisItem);
		while (parent!=null) {
			this.setChecked(parent.getData(),state);
			parent = this.getParentItem((Item)parent);
			if ((!state)&&(parent != null)){
				if (this.anyBrotherChecked(parent.getData())){
					break;
				}
			}
		}	
	}
}
