/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.session;

import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.opencert.storage.cdo.StandaloneCDOAccessor;
import org.eclipse.opencert.storage.cdo.property.IOpencertClientConfigurationAdapter;

/**
 * NOTE: this class is NOT thread-safe!
 * 
 */
public class SimpleCDOSessionProvider implements ICDOSessionProvider
{
    private CDOSession cdoSession;
    
    private IOpencertClientConfigurationAdapter clientConfigurationAdapter;

    
    public SimpleCDOSessionProvider(IOpencertClientConfigurationAdapter clientConfigurationAdapter)
    {
        this.clientConfigurationAdapter = clientConfigurationAdapter;
    }
    
  
    @Override
    public CDOSession getActiveSession()
    {
        if (cdoSession == null) {
            StandaloneCDOAccessor standaloneCDOAccessor = StandaloneCDOAccessor.getStandaloneCDOAccessor(clientConfigurationAdapter);
            cdoSession = standaloneCDOAccessor.openSession();
        }
        
        return cdoSession;
    }

    @Override
    public void close()
    {
        if (cdoSession != null) {
            cdoSession.close();
        }
    }

}
