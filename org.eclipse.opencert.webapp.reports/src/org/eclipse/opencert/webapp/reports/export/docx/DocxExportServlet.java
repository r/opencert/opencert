/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.export.docx;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.eclipse.opencert.webapp.reports.util.IOUtil;

@SuppressWarnings("serial")
public class DocxExportServlet
    extends HttpServlet
{
    private static final String _EXPORTED_FILE_NAME = "ComplianceReport.docx";
    public static final String BASELINE_ID_PARAM = "baselineID";
    public static final String PROJECT_ID_PARAM = "projectID";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException
    {
        ByteArrayInputStream resultStream = null; 
        try {
            final long baselineFrameworkID = parseLongParam(request, BASELINE_ID_PARAM);
            final long projectID = parseLongParam(request, PROJECT_ID_PARAM);
            
            String basePath = getServletContext().getRealPath(File.separator);
            final ComplianceEstimationReportDocxGenerator generator = new ComplianceEstimationReportDocxGenerator(basePath);
            resultStream = generator.generateProjectBaselineReport(baselineFrameworkID, projectID);
            
            streamToCurrentResponse(resultStream, response);
            
        } catch (Docx4JException | IOException e) {
            e.printStackTrace();
            
            // TODO !!!!
            //Notification.show(e.getMessage(), Notification.TYPE_ERROR_MESSAGE);
        } finally {
            IOUtil.close(resultStream);
        }
    }
        
    private long parseLongParam(HttpServletRequest request, String paramName)
    {
        String val = request.getParameter(paramName);
        if (val == null) {
            throw new IllegalArgumentException("NUll param: " + paramName);
        }
        final long result = Long.parseLong(val);
        
        return result;
    }

    private void streamToCurrentResponse(ByteArrayInputStream inStream, HttpServletResponse response) 
            throws IOException
    {
        OutputStream os = null;
        os = response.getOutputStream();
        byte[] buf = new byte[1024];
        int len;
        while ((len = inStream.read(buf)) > 0) {
            os.write(buf, 0, len);
        }

        response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        response.setHeader ("Content-Disposition", "attachment; filename=\"" + _EXPORTED_FILE_NAME + "\"");
        response.setHeader("Content-Transfer-Encoding", "binary");
        
        //TODO
    }
}
