/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.ui.loadresource;

import org.eclipse.emf.edit.ui.action.LoadResourceAction;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

public class DawnAwareLoadResourceAction extends LoadResourceAction {
	public DawnAwareLoadResourceAction() {
		// System.out.println("DawnAwareLoadResourceAction ctor");
	}

	@Override
	public void run() {
		IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (EditorTypeUtil.isFileEditor(editorPart)) {
			super.run();
		} else {
			org.eclipse.opencert.infra.ui.loadresource.LoadResourceDialog loadResourceDialog = new org.eclipse.opencert.infra.ui.loadresource.LoadResourceDialog(
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), domain);

			loadResourceDialog.open();
		}

	}
}
