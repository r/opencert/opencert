/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;

// End of user code

/**
 * 
 * 
 */
public class ArtefactDefinitionEvaluationPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ArtefactDefinitionEvaluationPropertiesEditionPart {

	protected ReferencesTable evaluation;
	protected List<ViewerFilter> evaluationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> evaluationFilters = new ArrayList<ViewerFilter>();
	protected TableViewer evaluationTable;
	protected List<ViewerFilter> evaluationTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> evaluationTableFilters = new ArrayList<ViewerFilter>();
	protected Button addEvaluationTable;
	protected Button removeEvaluationTable;
	protected Button editEvaluationTable;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArtefactDefinitionEvaluationPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence artefactDefinitionEvaluationStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = artefactDefinitionEvaluationStep.addStep(EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.class);
		// Start IRR
		// propertiesStep.addStep(EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation);
		// End IRR
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable);
		
		
		composer = new PartComposer(artefactDefinitionEvaluationStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation) {
					return createEvaluationAdvancedTableComposition(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable) {
					return createEvaluationTableTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(EvidenceMessages.ArtefactDefinitionEvaluationPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEvaluationAdvancedTableComposition(Composite parent) {
		this.evaluation = new ReferencesTable(getDescription(EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation, EvidenceMessages.ArtefactDefinitionEvaluationPropertiesEditionPart_EvaluationLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEvaluationPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				evaluation.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEvaluationPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				evaluation.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEvaluationPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				evaluation.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEvaluationPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				evaluation.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.evaluationFilters) {
			this.evaluation.addFilter(filter);
		}
		this.evaluation.setHelpText(propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation, EvidenceViewsRepository.SWT_KIND));
		this.evaluation.createControls(parent);
		this.evaluation.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEvaluationPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData evaluationData = new GridData(GridData.FILL_HORIZONTAL);
		evaluationData.horizontalSpan = 3;
		this.evaluation.setLayoutData(evaluationData);
		this.evaluation.setLowerBound(0);
		this.evaluation.setUpperBound(-1);
		evaluation.setID(EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation);
		evaluation.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEvaluationAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEvaluationTableTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableEvaluationTable = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableEvaluationTable.setHeaderVisible(true);
		GridData gdEvaluationTable = new GridData();
		gdEvaluationTable.grabExcessHorizontalSpace = true;
		gdEvaluationTable.horizontalAlignment = GridData.FILL;
		gdEvaluationTable.grabExcessVerticalSpace = true;
		gdEvaluationTable.verticalAlignment = GridData.FILL;
		tableEvaluationTable.setLayoutData(gdEvaluationTable);
		tableEvaluationTable.setLinesVisible(true);

		// Start of user code for columns definition for EvaluationTable
		// Start IRR

		TableColumn name = new TableColumn(tableEvaluationTable, SWT.NONE);
		name.setWidth(80);
		name.setText("Name"); //$NON-NLS-1$

		TableColumn name1 = new TableColumn(tableEvaluationTable, SWT.NONE);
		name1.setWidth(80);
		name1.setText("Criterion"); //$NON-NLS-1$

		TableColumn name2 = new TableColumn(tableEvaluationTable, SWT.NONE);
		name2.setWidth(160);
		name2.setText("Evaluation Result"); 

		// End IRR
		// End of user code

		evaluationTable = new TableViewer(tableEvaluationTable);
		evaluationTable.setContentProvider(new ArrayContentProvider());
		evaluationTable.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for EvaluationTable
						public String getColumnText(Object object, int columnIndex) {
							// Start IRR
							
							/*AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
							if (object instanceof EObject) {
								switch (columnIndex) {
								case 0:
									return labelProvider.getText(object);
								}
							}*/
							
							if (object instanceof EObject) {
								AssuranceAssetEvaluation assuranceAssetEvaluationObject = (AssuranceAssetEvaluation)object;
								switch (columnIndex) {
								case 0:							
									return assuranceAssetEvaluationObject.getName();
								case 1:							
									return assuranceAssetEvaluationObject.getCriterion();
								case 2:
									return assuranceAssetEvaluationObject.getEvaluationResult();
								}
							}
							
							// End IRR
							return ""; //$NON-NLS-1$
						}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		evaluationTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (evaluationTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) evaluationTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEvaluationPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						evaluationTable.refresh();
					}
				}
			}

		});
		GridData evaluationTableData = new GridData(GridData.FILL_HORIZONTAL);
		evaluationTableData.minimumHeight = 120;
		evaluationTableData.heightHint = 120;
		evaluationTable.getTable().setLayoutData(evaluationTableData);
		for (ViewerFilter filter : this.evaluationTableFilters) {
			evaluationTable.addFilter(filter);
		}
		EditingUtils.setID(evaluationTable.getTable(), EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable);
		EditingUtils.setEEFtype(evaluationTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createEvaluationTablePanel(tableContainer);
		// Start of user code for createEvaluationTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEvaluationTablePanel(Composite container) {
		Composite evaluationTablePanel = new Composite(container, SWT.NONE);
		GridLayout evaluationTablePanelLayout = new GridLayout();
		evaluationTablePanelLayout.numColumns = 1;
		evaluationTablePanel.setLayout(evaluationTablePanelLayout);
		addEvaluationTable = new Button(evaluationTablePanel, SWT.NONE);
		addEvaluationTable.setText(EvidenceMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addEvaluationTableData = new GridData(GridData.FILL_HORIZONTAL);
		addEvaluationTable.setLayoutData(addEvaluationTableData);
		addEvaluationTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEvaluationPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				evaluationTable.refresh();
			}
		});
		EditingUtils.setID(addEvaluationTable, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable);
		EditingUtils.setEEFtype(addEvaluationTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeEvaluationTable = new Button(evaluationTablePanel, SWT.NONE);
		removeEvaluationTable.setText(EvidenceMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeEvaluationTableData = new GridData(GridData.FILL_HORIZONTAL);
		removeEvaluationTable.setLayoutData(removeEvaluationTableData);
		removeEvaluationTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (evaluationTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) evaluationTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEvaluationPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						evaluationTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeEvaluationTable, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable);
		EditingUtils.setEEFtype(removeEvaluationTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editEvaluationTable = new Button(evaluationTablePanel, SWT.NONE);
		editEvaluationTable.setText(EvidenceMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editEvaluationTableData = new GridData(GridData.FILL_HORIZONTAL);
		editEvaluationTable.setLayoutData(editEvaluationTableData);
		editEvaluationTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (evaluationTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) evaluationTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEvaluationPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						evaluationTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editEvaluationTable, EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable);
		EditingUtils.setEEFtype(editEvaluationTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createEvaluationTablePanel

		// End of user code
		return evaluationTablePanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart#initEvaluation(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEvaluation(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		evaluation.setContentProvider(contentProvider);
		evaluation.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluation);
		if (eefElementEditorReadOnlyState && evaluation.isEnabled()) {
			evaluation.setEnabled(false);
			evaluation.setToolTipText(EvidenceMessages.ArtefactDefinitionEvaluation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !evaluation.isEnabled()) {
			evaluation.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart#updateEvaluation()
	 * 
	 */
	public void updateEvaluation() {
	evaluation.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart#addFilterEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEvaluation(ViewerFilter filter) {
		evaluationFilters.add(filter);
		if (this.evaluation != null) {
			this.evaluation.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart#addBusinessFilterEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEvaluation(ViewerFilter filter) {
		evaluationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart#isContainedInEvaluationTable(EObject element)
	 * 
	 */
	public boolean isContainedInEvaluationTable(EObject element) {
		return ((ReferencesTableSettings)evaluation.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart#initEvaluationTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEvaluationTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		evaluationTable.setContentProvider(contentProvider);
		evaluationTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactDefinitionEvaluation.Properties.evaluationTable);
		if (eefElementEditorReadOnlyState && evaluationTable.getTable().isEnabled()) {
			evaluationTable.getTable().setEnabled(false);
			evaluationTable.getTable().setToolTipText(EvidenceMessages.ArtefactDefinitionEvaluation_ReadOnly);
			addEvaluationTable.setEnabled(false);
			addEvaluationTable.setToolTipText(EvidenceMessages.ArtefactDefinitionEvaluation_ReadOnly);
			removeEvaluationTable.setEnabled(false);
			removeEvaluationTable.setToolTipText(EvidenceMessages.ArtefactDefinitionEvaluation_ReadOnly);
			editEvaluationTable.setEnabled(false);
			editEvaluationTable.setToolTipText(EvidenceMessages.ArtefactDefinitionEvaluation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !evaluationTable.getTable().isEnabled()) {
			evaluationTable.getTable().setEnabled(true);
			addEvaluationTable.setEnabled(true);
			removeEvaluationTable.setEnabled(true);
			editEvaluationTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart#updateEvaluationTable()
	 * 
	 */
	public void updateEvaluationTable() {
	evaluationTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart#addFilterEvaluationTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEvaluationTable(ViewerFilter filter) {
		evaluationTableFilters.add(filter);
		if (this.evaluationTable != null) {
			this.evaluationTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart#addBusinessFilterEvaluationTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEvaluationTable(ViewerFilter filter) {
		evaluationTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart#isContainedInEvaluationTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInEvaluationTableTable(EObject element) {
		return ((ReferencesTableSettings)evaluationTable.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.ArtefactDefinitionEvaluation_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
