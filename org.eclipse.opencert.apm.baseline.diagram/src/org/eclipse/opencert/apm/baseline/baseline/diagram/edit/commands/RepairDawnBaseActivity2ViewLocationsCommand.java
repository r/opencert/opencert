/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;
import org.eclipse.emf.internal.cdo.object.CDOLegacyAdapter;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.DeferredLayoutCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.commands.SetViewMutabilityCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.CompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.requests.ArrangeRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.RefreshConnectionsRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.resources.GMFResourceFactory;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.Bounds;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.LayoutConstraint;
import org.eclipse.gmf.runtime.notation.Location;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.gmf.tooling.runtime.update.UpdaterLinkDescriptor;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivity2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseFrameworkEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramUpdater;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineLinkDescriptor;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineNodeDescriptor;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;
import org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * 
 * @author  M� Carmen Palacios
 */
/**
 * @generated NOT
 */
public class RepairDawnBaseActivity2ViewLocationsCommand extends AbstractTransactionalCommand {
	    private LoadViewLocations sourceFileViewLocations;
		private Point offset;
        private View parentView;
        private EditPart host;

        /**
         * @generated NOT
         */
        public RepairDawnBaseActivity2ViewLocationsCommand (
        				LoadViewLocations sourceFileViewLocations,
        				Point offset,
                        TransactionalEditingDomain editingDomain, View parentView,
                        EditPart host) {
                super(editingDomain, "Repair location of views", getWorkspaceFiles(parentView));
                this.sourceFileViewLocations = sourceFileViewLocations;
                this.parentView = parentView;
                this.host = host;
                this.offset = offset;
        }
        
        private List<View> findViews(List<EObject> listNodes)
        {
        	List<View> res = new ArrayList();
        	
        	for(EObject elem : listNodes)
        	{
        		EditPart  editPart = lookForEditPart(elem);
        		View view = ((IGraphicalEditPart)editPart).getNotationView();
        		res.add(view);
        	}
        	return res;
        }
        
    	private EditPart lookForEditPart(EObject semantic) {
    		Collection<EditPart> editPartSet = ((IGraphicalEditPart) host).getViewer().getEditPartRegistry().values();
    		Iterator<EditPart> editPartIterator = editPartSet.iterator();
    		EditPart existedEditPart = null;
    		while(editPartIterator.hasNext() && existedEditPart == null) {
    			EditPart currentEditPart = editPartIterator.next();

    			if(isEditPartTypeAdapted(currentEditPart.getClass(), semantic.eClass()) && semantic.equals(((GraphicalEditPart)currentEditPart).resolveSemanticElement())) {
    				existedEditPart = currentEditPart;
    			}
    		}
    		return existedEditPart;
    	}

    	private boolean isEditPartTypeAdapted(Class<? extends EditPart> editPartClass, EClass eClass) {
    		if(DiagramEditPart.class.isAssignableFrom(editPartClass) || CompartmentEditPart.class.isAssignableFrom(editPartClass)) {
    			// the edit part is disqualified, as a compartment or a diagram can not be dropped
    			return false;
    		} else if(GraphicalEditPart.class.isAssignableFrom(editPartClass)) {
    			// check the edit part type against advised ones
    			return isEditPartTypeSuitableForEClass(editPartClass.asSubclass(GraphicalEditPart.class), eClass);
    		} else {
    			// only a GraphicalEditPart must be selected
    			return false;
    		}
    	}

    	protected boolean isEditPartTypeSuitableForEClass(Class<? extends GraphicalEditPart> editPartClass, EClass eClass) {
    		return true;
    	}
    	
        
        /**
         * @generated NOT
         */
        protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
                        IAdaptable info) throws ExecutionException {
	        	
        		BaseActivity2RepairDawnViewLocationsCanonicalEditPolicy kk = new BaseActivity2RepairDawnViewLocationsCanonicalEditPolicy();
                kk.setHost(host);
                kk.setoffset(offset);
                //kk.LoadObjectLocations(sourceFile);
                kk.refresh();                
                kk.deactivate(); // para que no invoque refreshSemantic() al eliminar del diagrama uno de los objetos
                
                return CommandResult.newOKCommandResult();
        }
        
		/**
		 * @generated NOT
		 */
        public class   BaseActivity2RepairDawnViewLocationsCanonicalEditPolicy extends CanonicalEditPolicy {
        	private Map<String, String> eObjectToIDMap = null;        	
        	private Point offset;
        	private List resultMCP = null;

        	
        	
        	
    		/**
    		 * @generated NOT
    		 */
        	public void setoffset(Point offset)
        	{
        		this.offset = offset;
        	}


        	
        	
    		/**
    		 * @generated NOT
    		 */
        	protected IStatus changeLocations(List<View> nodel, Location offset) {
        		
        		for(View elem : nodel)
        		{
        			changeLocation((Node)elem, offset);
        		}
        		
    			return Status.OK_STATUS;        		
        	}

    		/**
    		 * @generated NOT
    		 */
    		private IStatus changeLocation(Node node, Location offset) { 
    			Location lc = (Location) node.getLayoutConstraint();
    			int x = lc.getX(); int y = lc.getY();
    			lc.setX(offset.getX()+x);
    			lc.setY(offset.getY()+y);
    			return Status.OK_STATUS;
    		}
    		

    		/**
    		 * @generated
    		 */
    		private Set<EStructuralFeature> myFeaturesToSynchronize;

    		/**
    		 * @generated
    		 */
    		protected void refreshOnActivate() {
    			// Need to activate editpart children before invoking the canonical refresh for EditParts to add event listeners
    			List<?> c = getHost().getChildren();
    			for (int i = 0; i < c.size(); i++) {
    				((EditPart) c.get(i)).activate();
    			}
    			super.refreshOnActivate();
    		}

    		/**
    		 * @generated
    		 */
    		protected EStructuralFeature getFeatureToSynchronize() {
    			return BaselinePackage.eINSTANCE.getBaseActivity_SubActivity();
    		}
    		
    		/**
    		 * @generated
    		 */
    		@SuppressWarnings("rawtypes")
    		protected List getSemanticChildrenList() {
    			View viewObject = (View) getHost().getModel();
    			LinkedList<EObject> result = new LinkedList<EObject>();
    			List<BaselineNodeDescriptor> childDescriptors = BaselineDiagramUpdater
    					.getBaseActivityBaseActivitySubActivityCompartment_7002SemanticChildren(viewObject);
    			for (BaselineNodeDescriptor d : childDescriptors) {
    				result.add(d.getModelElement());
    			}
    			return result;
    		}

    		/**
    		 * @generated
    		 */
    		protected boolean isOrphaned(Collection<EObject> semanticChildren,
    				final View view) {
    			return isMyDiagramElement(view)
    					&& !semanticChildren.contains(view.getElement());
    		}

    		/**
    		 * @generated NOT
    		 */
    		private boolean isMyDiagramElement(View view) {
    			return BaseActivity2EditPart.VISUAL_ID == BaselineVisualIDRegistry
    					.getVisualID(view);
    		}

    		/**
    		 * @generated NOT
    		 */
    		// Start MCP
    		private void setIDs() {
    			eObjectToIDMap = new HashMap<String, String>();
    			
    			List<EObject> result = getSemanticChildrenList();
    			
    			for(EObject elem : result)
    			{
    	    		// Si CDO Native
    		        CDOID cdoID = CDOUtil.getCDOObject(elem).cdoID();
    		        Long  cdoIdLong = new Long(cdoID.toURIFragment());
    				//Parece que cambian los objetos por su estado!!!
    				//eObjectToIDMap.put(elem, cdoID.toString());
    		        eObjectToIDMap.put(cdoID.toString(), cdoID.toString());
    		        NamedElement me = (NamedElement)elem;
    				System.out.println("setIDs key = "+ me.getId()+"|"+me.getName()+"|"+me.getClass().getName() + ". (" + cdoID.toString() + ")");    					
    				//eObjectToIDMap.put(elem, me.getId()+me.getName()+me.eClass().getName());
    			}
    		}	
    		// End MCP	
    		
    		/**
    		 * @generated NOT
    		 */
    		// Start MCP	
    		private String getID(String eObjectID) {
    		    if (eObjectToIDMap == null)
    		    {
    		      return null;
    		    }
    		    else
    		    {
    		      return eObjectToIDMap.get(eObjectID);
    		    }
    		  }
    		// End MCP
    		
			// MCP Cross domain
    		/**
    		 * @generated NOT
    		 */
    		protected Command createViewsAndArrangeCommand2(Point locat, Dimension size, List viewDescriptors) {
    			CreateViewRequest createViewRequest =
    				new CreateViewRequest(viewDescriptors);
    			createViewRequest.setLocation(locat);
    			if (size != null && !size.isEmpty())
    			{
    				createViewRequest.setSize(size);
    			}
    			Command createCommand = getHost().getCommand(createViewRequest);

    			if (createCommand != null) {
    				// Cross project
    				//List result = (List)createViewRequest.getNewObject();
    				resultMCP =  (List)createViewRequest.getNewObject();
    				//MCPdropRequest.setResult(result);

    				RefreshConnectionsRequest refreshRequest =
    					new RefreshConnectionsRequest(resultMCP);
    				Command refreshCommand = getHost().getCommand(refreshRequest);

    				ArrangeRequest arrangeRequest =
    					new ArrangeRequest(RequestConstants.REQ_ARRANGE_DEFERRED);
    				arrangeRequest.setViewAdaptersToArrange(resultMCP);
    				Command arrangeCommand = getHost().getCommand(arrangeRequest);

    				CompoundCommand cc = new CompoundCommand(createCommand.getLabel());
    				cc.add(createCommand.chain(refreshCommand));
    				cc.add(arrangeCommand);
    				
    				return cc;
    			}
    			return null;
    		}
			// MCP Cross domain
    		
    		/**
    		 * @generated NOT
    		 */
    		protected void refreshSemantic() {
    			if (resolveSemanticElement() == null) {
    				return;
    			}
    			LinkedList<IAdaptable> createdViews = new LinkedList<IAdaptable>();
    			List<BaselineNodeDescriptor> childDescriptors = BaselineDiagramUpdater
    					.getBaseActivityBaseActivitySubActivityCompartment_7002SemanticChildren((View) getHost()
    							.getModel());
    			// Start MCP
    			setIDs();
    			
    			//End MCP
    			LinkedList<View> orphaned = new LinkedList<View>();
    			// we care to check only views we recognize as ours
    			LinkedList<View> knownViewChildren = new LinkedList<View>();
    			for (View v : getViewChildren()) {
    				if (isMyDiagramElement(v)) {
    					knownViewChildren.add(v);
    				}
    			}
    			// Start MCP
    			/*
    			// alternative to #cleanCanonicalSemanticChildren(getViewChildren(), semanticChildren)
    			//
    			// iteration happens over list of desired semantic elements, trying to find best matching View, while original CEP
    			// iterates views, potentially losing view (size/bounds) information - i.e. if there are few views to reference same EObject, only last one 
    			// to answer isOrphaned == true will be used for the domain element representation, see #cleanCanonicalSemanticChildren()
				for (Iterator<BaselineNodeDescriptor> descriptorsIterator = childDescriptors
						.iterator(); descriptorsIterator.hasNext();) {
					BaselineNodeDescriptor next = descriptorsIterator.next();
					String hint = BaselineVisualIDRegistry.getType(next.getVisualID());
					LinkedList<View> perfectMatch = new LinkedList<View>(); // both semanticElement and hint match that of NodeDescriptor
					for (View childView : getViewChildren()) {
						EObject semanticElement = childView.getElement();
						if (next.getModelElement().equals(semanticElement)) {
							if (hint.equals(childView.getType())) {
								perfectMatch.add(childView);
								// actually, can stop iteration over view children here, but
								// may want to use not the first view but last one as a 'real' match (the way original CEP does
								// with its trick with viewToSemanticMap inside #cleanCanonicalSemanticChildren
							}
						}
					}
					if (perfectMatch.size() > 0) {
						descriptorsIterator.remove(); // precise match found no need to create anything for the NodeDescriptor
						// use only one view (first or last?), keep rest as orphaned for further consideration
						knownViewChildren.remove(perfectMatch.getFirst());
					}
				}
    			*/
    			// End MCP
    			// those left in knownViewChildren are subject to removal - they are our diagram elements we didn't find match to,
    			// or those we have potential matches to, and thus need to be recreated, preserving size/location information.
    			orphaned.addAll(knownViewChildren);
    			//
    			ArrayList<CreateViewRequest.ViewDescriptor> viewDescriptors = new ArrayList<CreateViewRequest.ViewDescriptor>(
    					childDescriptors.size());
    			for (BaselineNodeDescriptor next : childDescriptors) {
    				String hint = BaselineVisualIDRegistry.getType(next.getVisualID());
    				IAdaptable elementAdapter = new CanonicalElementAdapter(
    						next.getModelElement(), hint);
    				CreateViewRequest.ViewDescriptor descriptor = new CreateViewRequest.ViewDescriptor(
    						//cross domain elementAdapter, Node.class, hint, ViewUtil.APPEND, false,
    						elementAdapter, Node.class, hint, ViewUtil.APPEND, true,
    						host().getDiagramPreferencesHint());
    				viewDescriptors.add(descriptor);
    			}

    			boolean changed = deleteViews(orphaned.iterator());
    			// Start MCP
    			// MCP Cross domain
    			/*    			
    			for(ViewDescriptor oneviewDescriptor : viewDescriptors)
    			{
    				List<ViewDescriptor> oneviewDescriptorl = new ArrayList(1); 
    				oneviewDescriptorl.add(oneviewDescriptor);
    				CreateViewRequest request = getCreateViewRequest(oneviewDescriptorl);
    				NamedElement me = (NamedElement)oneviewDescriptor.getElementAdapter().getAdapter(NamedElement.class);
    				System.out.println("refreshSemantic key = "+ me.getId()+me.getName()+me.getClass().getName());
    				Bounds lc = (Bounds) sourceFileViewLocations.getObjectLocation(me.getId()+me.getName()+me.eClass().getName());
    				int x = 0; int y  = 0;
    				if(lc != null)
    				{
    					x = lc.getX(); y  = lc.getY();
    	    			Dimension newSize = new Dimension(lc.getWidth(), lc.getHeight());
    	    			request.setSize(newSize);
    				}
    				Point newlocation = new Point();
    				newlocation.setX(offset.x()+x);
    				newlocation.setY(offset.y()+y);
	    			request.setLocation(newlocation);
	    			Command cmd = getCreateViewCommand(request);
	    			if (cmd != null && cmd.canExecute()) {
	    				SetViewMutabilityCommand.makeMutable(
	    						new EObjectAdapter(host().getNotationView())).execute();
	    				executeCommand(cmd);
	    				@SuppressWarnings("unchecked")
	    				List<IAdaptable> nl = (List<IAdaptable>) request.getNewObject();
	    				createdViews.addAll(nl);
	    			}
    			}
    			*/
    			// cross domain
				SetViewMutabilityCommand.makeMutable(
						new EObjectAdapter(host().getNotationView())).execute();
    			int cont = 0;
    			for(ViewDescriptor oneviewDescriptor : viewDescriptors)
    			{
    				resultMCP = null;
    				
    				List<ViewDescriptor> oneviewDescriptorl = new ArrayList(1); 
    				oneviewDescriptorl.add(oneviewDescriptor);

    				NamedElement me = (NamedElement)oneviewDescriptor.getElementAdapter().getAdapter(NamedElement.class);    				
    				System.out.println("refreshSemantic key = "+ me.getId()+me.getName()+me.getClass().getName());
    				Bounds lc = (Bounds) sourceFileViewLocations.getObjectLocation(me.getId()+me.getName()+me.eClass().getName());
    				System.out.println("refreshSemantic lc = "+ lc.getX() + ", " + lc.getY() + ", " + lc.getWidth() + ", " + lc.getHeight());    				

    				//Point point = new Point(offset.x+cont, offset.y+cont); cont +=50;
    				Point point = new Point(lc.getX(), lc.getY());
    				Dimension size = new Dimension (lc.getWidth(), lc.getHeight());
	    			Command command = createViewsAndArrangeCommand2(point, size, oneviewDescriptorl);
	    			// update diagram.
	    			//command = command.chain(new UpdateDiagramCommand(getGraphicalHost()));
	    			if(command != null && command.canExecute()) {
	    				/* cross domian
	    				SetViewMutabilityCommand.makeMutable(
	    						new EObjectAdapter(host().getNotationView())).execute();
	    				*/
	    				executeCommand(command);
	    				
	    				createdViews.addAll(resultMCP);
	    			}
    			}
    			// MCP PRUEBA Cross domain
    			
    			// End MCP
    			if (changed || createdViews.size() > 0) {
    				postProcessRefreshSemantic(createdViews);
    			}

    			/*MCP
				if (createdViews.size() > 1) {
					// perform a layout of the container
					DeferredLayoutCommand layoutCmd = new DeferredLayoutCommand(host()
							.getEditingDomain(), createdViews, host());
					executeCommand(new ICommandProxy(layoutCmd));
				}
    			*/

    			makeViewsImmutable(createdViews);
    		}
        }
}
