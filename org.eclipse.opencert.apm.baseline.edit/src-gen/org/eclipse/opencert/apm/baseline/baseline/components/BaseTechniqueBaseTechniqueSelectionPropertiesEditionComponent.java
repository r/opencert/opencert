/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;

import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueSelectionPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class BaseTechniqueBaseTechniqueSelectionPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASETECHNIQUESELECTION_PART = "BaseTechniqueSelection"; //$NON-NLS-1$

	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaseTechniqueBaseTechniqueSelectionPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseTechnique, String editing_mode) {
		super(editingContext, baseTechnique, editing_mode);
		parts = new String[] { BASETECHNIQUESELECTION_PART };
		repositoryKey = BaselineViewsRepository.class;
		partKey = BaselineViewsRepository.BaseTechniqueSelection.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaseTechnique baseTechnique = (BaseTechnique)elt;
			final BaseTechniqueSelectionPropertiesEditionPart baseTechniqueSelectionPart = (BaseTechniqueSelectionPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected)) {
				baseTechniqueSelectionPart.setIsSelected(baseTechnique.isIsSelected());
			}
			if (isAccessible(BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification))
				baseTechniqueSelectionPart.setSelectionJustification(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseTechnique.getSelectionJustification()));
			
			// init filters
			
			
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}





	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected) {
			return BaselinePackage.eINSTANCE.getBaselineElement_IsSelected();
		}
		if (editorKey == BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification) {
			return BaselinePackage.eINSTANCE.getBaselineElement_SelectionJustification();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaseTechnique baseTechnique = (BaseTechnique)semanticObject;
		if (BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected == event.getAffectedEditor()) {
			baseTechnique.setIsSelected((Boolean)event.getNewValue());
		}
		if (BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification == event.getAffectedEditor()) {
			baseTechnique.setSelectionJustification((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaseTechniqueSelectionPropertiesEditionPart baseTechniqueSelectionPart = (BaseTechniqueSelectionPropertiesEditionPart)editingPart;
			if (BaselinePackage.eINSTANCE.getBaselineElement_IsSelected().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && baseTechniqueSelectionPart != null && isAccessible(BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected))
				baseTechniqueSelectionPart.setIsSelected((Boolean)msg.getNewValue());
			
			if (BaselinePackage.eINSTANCE.getBaselineElement_SelectionJustification().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && baseTechniqueSelectionPart != null && isAccessible(BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification)) {
				if (msg.getNewValue() != null) {
					baseTechniqueSelectionPart.setSelectionJustification(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					baseTechniqueSelectionPart.setSelectionJustification("");
				}
			}
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			BaselinePackage.eINSTANCE.getBaselineElement_IsSelected(),
			BaselinePackage.eINSTANCE.getBaselineElement_SelectionJustification()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (BaselineViewsRepository.BaseTechniqueSelection.Properties.isSelected == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaselineElement_IsSelected().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaselineElement_IsSelected().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseTechniqueSelection.Properties.selectionJustification == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaselineElement_SelectionJustification().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaselineElement_SelectionJustification().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
