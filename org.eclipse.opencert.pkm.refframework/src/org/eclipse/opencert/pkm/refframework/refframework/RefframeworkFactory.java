/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage
 * @generated
 */
public interface RefframeworkFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RefframeworkFactory eINSTANCE = org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Ref Framework</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Framework</em>'.
	 * @generated
	 */
	RefFramework createRefFramework();

	/**
	 * Returns a new object of class '<em>Ref Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Requirement</em>'.
	 * @generated
	 */
	RefRequirement createRefRequirement();

	/**
	 * Returns a new object of class '<em>Ref Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Artefact</em>'.
	 * @generated
	 */
	RefArtefact createRefArtefact();

	/**
	 * Returns a new object of class '<em>Ref Activity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Activity</em>'.
	 * @generated
	 */
	RefActivity createRefActivity();

	/**
	 * Returns a new object of class '<em>Ref Requirement Rel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Requirement Rel</em>'.
	 * @generated
	 */
	RefRequirementRel createRefRequirementRel();

	/**
	 * Returns a new object of class '<em>Ref Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Role</em>'.
	 * @generated
	 */
	RefRole createRefRole();

	/**
	 * Returns a new object of class '<em>Ref Applicability Level</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Applicability Level</em>'.
	 * @generated
	 */
	RefApplicabilityLevel createRefApplicabilityLevel();

	/**
	 * Returns a new object of class '<em>Ref Criticality Level</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Criticality Level</em>'.
	 * @generated
	 */
	RefCriticalityLevel createRefCriticalityLevel();

	/**
	 * Returns a new object of class '<em>Ref Technique</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Technique</em>'.
	 * @generated
	 */
	RefTechnique createRefTechnique();

	/**
	 * Returns a new object of class '<em>Ref Artefact Rel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Artefact Rel</em>'.
	 * @generated
	 */
	RefArtefactRel createRefArtefactRel();

	/**
	 * Returns a new object of class '<em>Ref Criticality Applicability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Criticality Applicability</em>'.
	 * @generated
	 */
	RefCriticalityApplicability createRefCriticalityApplicability();

	/**
	 * Returns a new object of class '<em>Ref Activity Rel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Activity Rel</em>'.
	 * @generated
	 */
	RefActivityRel createRefActivityRel();

	/**
	 * Returns a new object of class '<em>Ref Independency Level</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Independency Level</em>'.
	 * @generated
	 */
	RefIndependencyLevel createRefIndependencyLevel();

	/**
	 * Returns a new object of class '<em>Ref Recommendation Level</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Recommendation Level</em>'.
	 * @generated
	 */
	RefRecommendationLevel createRefRecommendationLevel();

	/**
	 * Returns a new object of class '<em>Ref Control Category</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Control Category</em>'.
	 * @generated
	 */
	RefControlCategory createRefControlCategory();

	/**
	 * Returns a new object of class '<em>Ref Applicability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Applicability</em>'.
	 * @generated
	 */
	RefApplicability createRefApplicability();

	/**
	 * Returns a new object of class '<em>Ref Applicability Rel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Applicability Rel</em>'.
	 * @generated
	 */
	RefApplicabilityRel createRefApplicabilityRel();

	/**
	 * Returns a new object of class '<em>Ref Equivalence Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Equivalence Map</em>'.
	 * @generated
	 */
	RefEquivalenceMap createRefEquivalenceMap();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	RefframeworkPackage getRefframeworkPackage();

} //RefframeworkFactory
