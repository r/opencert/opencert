/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.opencert.infra.general.general.provider.DescribableElementItemProvider;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;

/**
 * This is the item provider adapter for a {@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BaseFrameworkItemProvider
	extends DescribableElementItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseFrameworkItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addScopePropertyDescriptor(object);
			addRevPropertyDescriptor(object);
			addPurposePropertyDescriptor(object);
			addPublisherPropertyDescriptor(object);
			addIssuedPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Scope feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BaseFramework_scope_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BaseFramework_scope_feature", "_UI_BaseFramework_type"),
				 BaselinePackage.Literals.BASE_FRAMEWORK__SCOPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Rev feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRevPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BaseFramework_rev_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BaseFramework_rev_feature", "_UI_BaseFramework_type"),
				 BaselinePackage.Literals.BASE_FRAMEWORK__REV,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Purpose feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPurposePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BaseFramework_purpose_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BaseFramework_purpose_feature", "_UI_BaseFramework_type"),
				 BaselinePackage.Literals.BASE_FRAMEWORK__PURPOSE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Publisher feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPublisherPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BaseFramework_publisher_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BaseFramework_publisher_feature", "_UI_BaseFramework_type"),
				 BaselinePackage.Literals.BASE_FRAMEWORK__PUBLISHER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Issued feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIssuedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BaseFramework_issued_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BaseFramework_issued_feature", "_UI_BaseFramework_type"),
				 BaselinePackage.Literals.BASE_FRAMEWORK__ISSUED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_ACTIVITIES);
			childrenFeatures.add(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_ARTEFACT);
			childrenFeatures.add(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_REQUIREMENT);
			childrenFeatures.add(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_APPLIC_LEVEL);
			childrenFeatures.add(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_CRITIC_LEVEL);
			childrenFeatures.add(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_ROLE);
			childrenFeatures.add(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_TECHNIQUE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns BaseFramework.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/BaseFramework"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((BaseFramework)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_BaseFramework_type") :
			getString("_UI_BaseFramework_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(BaseFramework.class)) {
			case BaselinePackage.BASE_FRAMEWORK__SCOPE:
			case BaselinePackage.BASE_FRAMEWORK__REV:
			case BaselinePackage.BASE_FRAMEWORK__PURPOSE:
			case BaselinePackage.BASE_FRAMEWORK__PUBLISHER:
			case BaselinePackage.BASE_FRAMEWORK__ISSUED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case BaselinePackage.BASE_FRAMEWORK__OWNED_ACTIVITIES:
			case BaselinePackage.BASE_FRAMEWORK__OWNED_ARTEFACT:
			case BaselinePackage.BASE_FRAMEWORK__OWNED_REQUIREMENT:
			case BaselinePackage.BASE_FRAMEWORK__OWNED_APPLIC_LEVEL:
			case BaselinePackage.BASE_FRAMEWORK__OWNED_CRITIC_LEVEL:
			case BaselinePackage.BASE_FRAMEWORK__OWNED_ROLE:
			case BaselinePackage.BASE_FRAMEWORK__OWNED_TECHNIQUE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_ACTIVITIES,
				 BaselineFactory.eINSTANCE.createBaseActivity()));

		newChildDescriptors.add
			(createChildParameter
				(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_ARTEFACT,
				 BaselineFactory.eINSTANCE.createBaseArtefact()));

		newChildDescriptors.add
			(createChildParameter
				(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_REQUIREMENT,
				 BaselineFactory.eINSTANCE.createBaseRequirement()));

		newChildDescriptors.add
			(createChildParameter
				(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_APPLIC_LEVEL,
				 BaselineFactory.eINSTANCE.createBaseApplicabilityLevel()));

		newChildDescriptors.add
			(createChildParameter
				(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_APPLIC_LEVEL,
				 BaselineFactory.eINSTANCE.createBaseIndependencyLevel()));

		newChildDescriptors.add
			(createChildParameter
				(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_APPLIC_LEVEL,
				 BaselineFactory.eINSTANCE.createBaseRecommendationLevel()));

		newChildDescriptors.add
			(createChildParameter
				(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_APPLIC_LEVEL,
				 BaselineFactory.eINSTANCE.createBaseControlCategory()));

		newChildDescriptors.add
			(createChildParameter
				(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_CRITIC_LEVEL,
				 BaselineFactory.eINSTANCE.createBaseCriticalityLevel()));

		newChildDescriptors.add
			(createChildParameter
				(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_ROLE,
				 BaselineFactory.eINSTANCE.createBaseRole()));

		newChildDescriptors.add
			(createChildParameter
				(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_TECHNIQUE,
				 BaselineFactory.eINSTANCE.createBaseTechnique()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return BaselineEditPlugin.INSTANCE;
	}

}
