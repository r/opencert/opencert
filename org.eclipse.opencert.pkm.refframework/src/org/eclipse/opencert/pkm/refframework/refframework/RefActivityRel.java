/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.opencert.infra.general.general.ActivityRelKind;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ref Activity Rel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivityRel()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface RefActivityRel extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.infra.general.general.ActivityRelKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.opencert.infra.general.general.ActivityRelKind
	 * @see #setType(ActivityRelKind)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivityRel_Type()
	 * @model
	 * @generated
	 */
	ActivityRelKind getType();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.opencert.infra.general.general.ActivityRelKind
	 * @see #getType()
	 * @generated
	 */
	void setType(ActivityRelKind value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(RefActivity)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivityRel_Source()
	 * @model required="true"
	 * @generated
	 */
	RefActivity getSource();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(RefActivity value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(RefActivity)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefActivityRel_Target()
	 * @model required="true"
	 * @generated
	 */
	RefActivity getTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(RefActivity value);

} // RefActivityRel
