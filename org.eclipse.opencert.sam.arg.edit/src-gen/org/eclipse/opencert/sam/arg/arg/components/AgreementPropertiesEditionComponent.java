/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.parts.AgreementPropertiesEditionPart;
import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class AgreementPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for evaluation ReferencesTable
	 */
	protected ReferencesTableSettings evaluationSettings;
	
	/**
	 * Settings for lifecycleEvent ReferencesTable
	 */
	protected ReferencesTableSettings lifecycleEventSettings;
	
	/**
	 * Settings for argumentation ReferencesTable
	 */
	private ReferencesTableSettings argumentationSettings;
	
	/**
	 * Settings for consistOf ReferencesTable
	 */
	private ReferencesTableSettings consistOfSettings;
	
	/**
	 * Settings for between ReferencesTable
	 */
	private ReferencesTableSettings betweenSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public AgreementPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject agreement, String editing_mode) {
		super(editingContext, agreement, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = ArgViewsRepository.class;
		partKey = ArgViewsRepository.Agreement.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Agreement agreement = (Agreement)elt;
			final AgreementPropertiesEditionPart basePart = (AgreementPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ArgViewsRepository.Agreement.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, agreement.getId()));
			
			if (isAccessible(ArgViewsRepository.Agreement.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, agreement.getName()));
			
			if (isAccessible(ArgViewsRepository.Agreement.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, agreement.getDescription()));
			
			if (isAccessible(ArgViewsRepository.Agreement.Properties.content))
				basePart.setContent(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, agreement.getContent()));
			
			if (isAccessible(ArgViewsRepository.Agreement.Properties.evaluation)) {
				evaluationSettings = new ReferencesTableSettings(agreement, AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation());
				basePart.initEvaluation(evaluationSettings);
			}
			if (isAccessible(ArgViewsRepository.Agreement.Properties.lifecycleEvent)) {
				lifecycleEventSettings = new ReferencesTableSettings(agreement, AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent());
				basePart.initLifecycleEvent(lifecycleEventSettings);
			}
			if (isAccessible(ArgViewsRepository.Agreement.Properties.location))
				basePart.setLocation(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, agreement.getLocation()));
			
			if (isAccessible(ArgViewsRepository.Agreement.Properties.argumentation)) {
				argumentationSettings = new ReferencesTableSettings(agreement, ArgPackage.eINSTANCE.getArgumentation_Argumentation());
				basePart.initArgumentation(argumentationSettings);
			}
			if (isAccessible(ArgViewsRepository.Agreement.Properties.consistOf)) {
				consistOfSettings = new ReferencesTableSettings(agreement, ArgPackage.eINSTANCE.getArgumentation_ConsistOf());
				basePart.initConsistOf(consistOfSettings);
			}
			if (isAccessible(ArgViewsRepository.Agreement.Properties.between)) {
				betweenSettings = new ReferencesTableSettings(agreement, ArgPackage.eINSTANCE.getAgreement_Between());
				basePart.initBetween(betweenSettings);
			}
			// init filters
			
			
			
			
			if (isAccessible(ArgViewsRepository.Agreement.Properties.evaluation)) {
				basePart.addFilterToEvaluation(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof AssuranceAssetEvaluation); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for evaluation
				// End of user code
			}
			if (isAccessible(ArgViewsRepository.Agreement.Properties.lifecycleEvent)) {
				basePart.addFilterToLifecycleEvent(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof AssuranceAssetEvent); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for lifecycleEvent
				// End of user code
			}
			
			if (isAccessible(ArgViewsRepository.Agreement.Properties.argumentation)) {
				basePart.addFilterToArgumentation(new EObjectFilter(ArgPackage.Literals.ARGUMENTATION));
				// Start of user code for additional businessfilters for argumentation
				// End of user code
			}
			if (isAccessible(ArgViewsRepository.Agreement.Properties.consistOf)) {
				basePart.addFilterToConsistOf(new EObjectFilter(ArgPackage.Literals.ARGUMENT_ELEMENT));
				// Start of user code for additional businessfilters for consistOf
				// End of user code
			}
			if (isAccessible(ArgViewsRepository.Agreement.Properties.between)) {
				basePart.addFilterToBetween(new EObjectFilter(ArgPackage.Literals.ARGUMENTATION));
				// Start of user code for additional businessfilters for between
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}













	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ArgViewsRepository.Agreement.Properties.id) {
			return ArgPackage.eINSTANCE.getModelElement_Id();
		}
		if (editorKey == ArgViewsRepository.Agreement.Properties.name) {
			return ArgPackage.eINSTANCE.getModelElement_Name();
		}
		if (editorKey == ArgViewsRepository.Agreement.Properties.description) {
			return ArgPackage.eINSTANCE.getArgumentationElement_Description();
		}
		if (editorKey == ArgViewsRepository.Agreement.Properties.content) {
			return ArgPackage.eINSTANCE.getArgumentationElement_Content();
		}
		if (editorKey == ArgViewsRepository.Agreement.Properties.evaluation) {
			return AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation();
		}
		if (editorKey == ArgViewsRepository.Agreement.Properties.lifecycleEvent) {
			return AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent();
		}
		if (editorKey == ArgViewsRepository.Agreement.Properties.location) {
			return ArgPackage.eINSTANCE.getArgumentation_Location();
		}
		if (editorKey == ArgViewsRepository.Agreement.Properties.argumentation) {
			return ArgPackage.eINSTANCE.getArgumentation_Argumentation();
		}
		if (editorKey == ArgViewsRepository.Agreement.Properties.consistOf) {
			return ArgPackage.eINSTANCE.getArgumentation_ConsistOf();
		}
		if (editorKey == ArgViewsRepository.Agreement.Properties.between) {
			return ArgPackage.eINSTANCE.getAgreement_Between();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Agreement agreement = (Agreement)semanticObject;
		if (ArgViewsRepository.Agreement.Properties.id == event.getAffectedEditor()) {
			agreement.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Agreement.Properties.name == event.getAffectedEditor()) {
			agreement.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Agreement.Properties.description == event.getAffectedEditor()) {
			agreement.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Agreement.Properties.content == event.getAffectedEditor()) {
			agreement.setContent((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Agreement.Properties.evaluation == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, evaluationSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				evaluationSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				evaluationSettings.move(event.getNewIndex(), (AssuranceAssetEvaluation) event.getNewValue());
			}
		}
		if (ArgViewsRepository.Agreement.Properties.lifecycleEvent == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, lifecycleEventSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				lifecycleEventSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				lifecycleEventSettings.move(event.getNewIndex(), (AssuranceAssetEvent) event.getNewValue());
			}
		}
		if (ArgViewsRepository.Agreement.Properties.location == event.getAffectedEditor()) {
			agreement.setLocation((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Agreement.Properties.argumentation == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Argumentation) {
					argumentationSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				argumentationSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				argumentationSettings.move(event.getNewIndex(), (Argumentation) event.getNewValue());
			}
		}
		if (ArgViewsRepository.Agreement.Properties.consistOf == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof ArgumentElement) {
					consistOfSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				consistOfSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				consistOfSettings.move(event.getNewIndex(), (ArgumentElement) event.getNewValue());
			}
		}
		if (ArgViewsRepository.Agreement.Properties.between == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Argumentation) {
					betweenSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				betweenSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				betweenSettings.move(event.getNewIndex(), (Argumentation) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			AgreementPropertiesEditionPart basePart = (AgreementPropertiesEditionPart)editingPart;
			if (ArgPackage.eINSTANCE.getModelElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Agreement.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (ArgPackage.eINSTANCE.getModelElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Agreement.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentationElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Agreement.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentationElement_Content().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Agreement.Properties.content)) {
				if (msg.getNewValue() != null) {
					basePart.setContent(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setContent("");
				}
			}
			if (AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.Agreement.Properties.evaluation))
				basePart.updateEvaluation();
			if (AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.Agreement.Properties.lifecycleEvent))
				basePart.updateLifecycleEvent();
			if (ArgPackage.eINSTANCE.getArgumentation_Location().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Agreement.Properties.location)) {
				if (msg.getNewValue() != null) {
					basePart.setLocation(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setLocation("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentation_Argumentation().equals(msg.getFeature())  && isAccessible(ArgViewsRepository.Agreement.Properties.argumentation))
				basePart.updateArgumentation();
			if (ArgPackage.eINSTANCE.getArgumentation_ConsistOf().equals(msg.getFeature())  && isAccessible(ArgViewsRepository.Agreement.Properties.consistOf))
				basePart.updateConsistOf();
			if (ArgPackage.eINSTANCE.getAgreement_Between().equals(msg.getFeature())  && isAccessible(ArgViewsRepository.Agreement.Properties.between))
				basePart.updateBetween();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			ArgPackage.eINSTANCE.getModelElement_Id(),
			ArgPackage.eINSTANCE.getModelElement_Name(),
			ArgPackage.eINSTANCE.getArgumentationElement_Description(),
			ArgPackage.eINSTANCE.getArgumentationElement_Content(),
			AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation(),
			AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent(),
			ArgPackage.eINSTANCE.getArgumentation_Location(),
			ArgPackage.eINSTANCE.getArgumentation_Argumentation(),
			ArgPackage.eINSTANCE.getArgumentation_ConsistOf(),
			ArgPackage.eINSTANCE.getAgreement_Between()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (ArgViewsRepository.Agreement.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Agreement.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Agreement.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentationElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentationElement_Description().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Agreement.Properties.content == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentationElement_Content().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentationElement_Content().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Agreement.Properties.location == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentation_Location().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentation_Location().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
