/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.generatefrommodel.importer;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class ImportWizardPage extends WizardPage {

	private static final String vocabSuffix = ".vocabulary";

	private TreeViewer sourceProjectTreeViewer;
	private Text targetVocabText;

	private boolean targetVocabOkay = false;

	private IProject initalSelection;

	public ImportWizardPage(String pageName, IProject selectedProject) {
		super(pageName);

		initalSelection = selectedProject;

		setTitle(pageName);
		setDescription("Import vocabulary data from the CCL models of a local project");
	}

	public void createControl(Composite parent) {
		Composite dialogArea = new Composite(parent, SWT.NONE);
		GridData dialogData = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.FILL_HORIZONTAL);
		dialogArea.setLayoutData(dialogData);
		GridLayout dialogAreaLayout = new GridLayout();
		dialogAreaLayout.numColumns = 1;
		dialogAreaLayout.makeColumnsEqualWidth = false;
		dialogAreaLayout.marginWidth = 0;
		dialogAreaLayout.marginHeight = 0;
		dialogArea.setLayout(dialogAreaLayout);

		createProjectSelectionArea(dialogArea);
		setControl(dialogArea);
		setPageComplete(false);
	}

	private void createProjectSelectionArea(Composite dialogArea) {
		Composite resourcesArea = new Composite(dialogArea, SWT.NONE);
		GridData resourcesData = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL
				| GridData.FILL_VERTICAL);
		resourcesArea.setLayoutData(resourcesData);
		GridLayout resourcesLayout = new GridLayout();
		resourcesLayout.numColumns = 2;
		resourcesLayout.marginWidth = 0;
		resourcesLayout.marginHeight = 0;
		resourcesArea.setLayout(resourcesLayout);

		sourceProjectTreeViewer = new TreeViewer(resourcesArea, SWT.SINGLE);
		GridData treeData = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL
				| GridData.FILL_VERTICAL);
		treeData.horizontalSpan = 2;
		sourceProjectTreeViewer.getControl().setLayoutData(treeData);
		sourceProjectTreeViewer.setAutoExpandLevel(2);

		ProjectContentProvider cp = new ProjectContentProvider();
		sourceProjectTreeViewer.setContentProvider(cp);
		sourceProjectTreeViewer.setLabelProvider(new ProjectLabelProvider());
		sourceProjectTreeViewer.setComparator(new ViewerComparator());
		sourceProjectTreeViewer.setUseHashlookup(true);
		sourceProjectTreeViewer
				.addDoubleClickListener(new IDoubleClickListener() {
					@Override
					public void doubleClick(DoubleClickEvent event) {
						ISelection selection = event.getSelection();
						expand(selection);
					}

					private void expand(ISelection selection) {
						if (selection instanceof IStructuredSelection) {
							Object item = ((IStructuredSelection) selection)
									.getFirstElement();
							if (item == null) {
								return;
							}
							if (sourceProjectTreeViewer.getExpandedState(item)) {
								sourceProjectTreeViewer
										.collapseToLevel(item, 1);
							} else {
								sourceProjectTreeViewer.expandToLevel(item, 1);
							}
						}
					}
				});
		sourceProjectTreeViewer
				.addSelectionChangedListener(new ISelectionChangedListener() {
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						ISelection selection = event.getSelection();
						if (!selection.isEmpty()) {
							IResource resource = (IResource) ((IStructuredSelection) selection)
									.getFirstElement();
							if (resource instanceof IContainer) {
								String path = resource.getFullPath().toString();
								targetVocabText.setText(path
										+ "/CCL.vocabulary");
								validateTargetVocab(targetVocabText.getText());
							}
						}
					}

				});

		// This has to be done after the viewer has been laid out
		sourceProjectTreeViewer.setInput(ResourcesPlugin.getWorkspace()
				.getRoot().getProjects());
		if (initalSelection != null) {
			sourceProjectTreeViewer.setSelection(new StructuredSelection(
					new Object[] { initalSelection }));
		}

		Label resourceNameLabel = new Label(resourcesArea, SWT.NONE);
		resourceNameLabel.setText("Path for the new Resource:  ");

		targetVocabText = new Text(resourcesArea, SWT.SINGLE | SWT.BORDER);
		GridData resourceNameData = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.FILL_HORIZONTAL);
		targetVocabText.setLayoutData(resourceNameData);
		targetVocabText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				String text = ((Text) e.getSource()).getText();
				validateTargetVocab(text);
			}
		});
	}

	private void validateTargetVocab(String text) {
		if (text.length() < vocabSuffix.length() + 1
				|| !text.toLowerCase().endsWith(vocabSuffix)
				|| !text.contains("/")) {
			setErrorMessage("Specify a valid vocabulary path with the '.vocabulary' extension.");
			targetVocabOkay = false;
			updatePageComplete();
			return;
		}

		String containerPath = text.substring(0, text.lastIndexOf("/"));
		IResource resource = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(containerPath);
		if (resource == null || !(resource instanceof IContainer)) {
			setErrorMessage("The vocabulary must be contained in a project or folder.");
			targetVocabOkay = false;
			updatePageComplete();
			return;
		}

		targetVocabOkay = true;
		setErrorMessage(null);
		updatePageComplete();

	}

	private void updatePageComplete() {
		setPageComplete(targetVocabOkay);
	}

	public IProject getProject() {
		String text = targetVocabText.getText();
		String containerPath = text.substring(0, text.lastIndexOf("/"));
		IProject project = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(containerPath).getProject();
		return project;
	}

	public IContainer getVocabContainer() {
		String text = targetVocabText.getText();
		String containerPath = text.substring(0, text.lastIndexOf("/"));
		IResource resource = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(containerPath);
		return (IContainer) resource;
	}

	public String getVocabFilename() {
		String text = targetVocabText.getText();
		// String filename = text.substring(text.lastIndexOf("/"));
		// return filename;
		return text;
	}
}
