/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;


import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Container.Hierarchical;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.view.metrics.JFreeChartWrapper;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartColor;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPosition;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.CategoryLabelWidthType;
import org.jfree.data.category.IntervalCategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.text.TextBlockAnchor;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;




@Theme("opencerttheme")
public class TimeEfficiencyChartsMetrics 
	extends CustomComponent

{

	
	private static final long serialVersionUID = 4465593628731452213L;
	private static final String GHANT_CHART_TITLE = "Activity Development Chart";  
    private static final String NO_DATA_AVAILABLE = "No Data Available";
    
    private byte[] gantt;
    private TreeTable treetable;
    private Hierarchical dataDescription;
    
     
    
    public Hierarchical getDataDescription() {
		return dataDescription;
	}



	public String getGhantChartTitle() {
		return GHANT_CHART_TITLE;
	}



	public byte[] getGantt() {
		return gantt;
	}




	public TimeEfficiencyChartsMetrics(AssuranceProjectWrapper project)
    {      
         
        VerticalLayout mainPanel = new VerticalLayout();
		
		mainPanel.setStyleName("chartsSpacing");
		mainPanel.setSpacing(true);
		
 	
		mainPanel.addComponent(timeEfficiencyProcessMetrics(getActivitiesEvents(project)));
		mainPanel.addComponent(treetable);
		
		//add mainPanel component
		setCompositionRoot(mainPanel);
		setSizeFull();
        
 
    }




    private List<Activity> getActivitiesEvents(AssuranceProjectWrapper newProject) {
    	
    	List<Activity> activities = new ArrayList<Activity>();
    	
    	
    	AssuranceProjectManager projectManager = 
				(AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
    	AssuranceProject project = projectManager.getProject(newProject.getId());
    	
    	if(project != null){
	    	EList<AssetsPackage> assetsPackages = project.getAssetsPackage();
			for (AssetsPackage assetsPackage : assetsPackages) {
				if (assetsPackage.isIsActive()) {				
					EList<ProcessModel> processModels = assetsPackage.getProcessModel();
					for (ProcessModel processModel : processModels) {
						/**/
						EObject eProcessModel= (EObject) processModel; 
			    		 for (Iterator<EObject> iterator = eProcessModel.eAllContents(); iterator.hasNext();) {
			        		 EObject aEObject = iterator.next();
			        		 if (aEObject instanceof Activity){
			        			 Activity activity = (Activity) aEObject;
			        			 if (activity.getStartTime()!=null) activities.add(activity);
			        		 }
			    		 }
						/**/
										
					}
				}
			}
    	}
    	
    	return activities;

	}




	private Component timeEfficiencyProcessMetrics(List<Activity> list) {
        
		final JFreeChart chart = ChartFactory.createGanttChart(
				GHANT_CHART_TITLE,  // chart title
	            "Activities",              // domain axis label
	            "Date",              // range axis label
	            populateWithData(list),             // data
	            true,                // include legend
	            true,                // tooltips
	            false                // urls
	        );   
		
		
		// get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED); 
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        // left align the category labels...
        final CategoryAxis axis = plot.getDomainAxis();
        final CategoryLabelPositions p = axis.getCategoryLabelPositions();
        
        final CategoryLabelPosition left = new CategoryLabelPosition(
            RectangleAnchor.LEFT, TextBlockAnchor.CENTER_LEFT, 
            TextAnchor.CENTER_LEFT, 0.0,
            CategoryLabelWidthType.RANGE, 0.30f
        );
        axis.setCategoryLabelPositions(CategoryLabelPositions.replaceLeftPosition(p, left));

        final CategoryItemRenderer renderer = plot.getRenderer();
        renderer.setBaseItemLabelsVisible(true);
        renderer.setSeriesPaint(0, ChartColor.LIGHT_BLUE);
       
       
        
        
        chart.removeLegend();
        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
		
        try {
        	gantt = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		Component chartWrapper = new JFreeChartWrapper(chart);
		chartWrapper.setHeight("80%");
		chartWrapper.setWidth("80%");
		chartWrapper.setStyleName("leftChartsSpacing");
		return chartWrapper;
	}



	private IntervalCategoryDataset populateWithData(List<Activity> list) {

		treetable = new TreeTable();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
    	
    	treetable.setWidth("100%");
		treetable.addStyleName("resourceEfficiencyTable");

		// Define two columns for the built-in container
		treetable.addContainerProperty("Activities", String.class, "");
		treetable.addContainerProperty("Description",  String.class, "");
		
		
		final TaskSeries t = new TaskSeries("");
		Date today = new Date();
		if(!list.isEmpty()){
			for(Activity activity : list){
				
				Object act = treetable.addItem(new Object[]{"Activity ID: "+activity.getId() + " Name: " + activity.getName(), activity.getDescription()},null); 
			
				treetable.setCollapsed(act, false);
				
				Object startDate = treetable.addItem(new Object[]{"Start Date:", activity.getStartTime()==null ? "Not Started" : sdf.format(activity.getStartTime())},null);
				Object endDate = treetable.addItem(new Object[]{"End Date:", activity.getEndTime()==null ? "Still Under Development" : sdf.format(activity.getEndTime())},null);
				
				treetable.setParent(startDate, act);
				treetable.setChildrenAllowed(startDate, false);
				treetable.setParent(endDate, act);
				treetable.setChildrenAllowed(endDate, false);
				
				if(activity.getStartTime().before(today)&&(activity.getEndTime()== null || activity.getStartTime().before(activity.getEndTime()))){
					t.add(new Task(activity.getName(), new SimpleTimePeriod(
							activity.getStartTime()==null ? today : activity.getStartTime(), 
							activity.getEndTime()==null ? today : activity.getEndTime())));
					
				}
				
				List<AssuranceAssetEvaluation> evaluation = activity.getEvaluation();
				if(!evaluation.isEmpty()){
					Object evaluations = treetable.addItem(new Object[]{"Evaluations: ", ""},null);
					treetable.setParent(evaluations,act);
					treetable.setCollapsed(evaluations,false);
					for (AssuranceAssetEvaluation aae : evaluation){
						Object ae = treetable.addItem(new Object[]{"Evaluation ID: "+aae.getId() + " Name: " + aae.getName(), ""},null);
						treetable.setParent(ae,evaluations);
						treetable.setCollapsed(ae,false);
						
						Object evR = treetable.addItem(new Object[]{"Evaluation result: ", aae.getEvaluationResult()},null);
						Object cr = treetable.addItem(new Object[]{"Criterion: ", aae.getCriterion()},null);
						Object crD = treetable.addItem(new Object[]{"Criterion Description: ", aae.getCriterionDescription()},null);
						
						treetable.setParent(evR, ae);
						treetable.setChildrenAllowed(evR, false);
						treetable.setParent(cr, ae);
						treetable.setChildrenAllowed(cr, false);
						treetable.setParent(crD, ae);
						treetable.setChildrenAllowed(crD, false);
						
					}
				}
				
			}
		}
		final TaskSeriesCollection collection = new TaskSeriesCollection();
	    collection.add(t);
	    dataDescription=treetable.getContainerDataSource();
	    return collection;
	}



	 
	



	
}

