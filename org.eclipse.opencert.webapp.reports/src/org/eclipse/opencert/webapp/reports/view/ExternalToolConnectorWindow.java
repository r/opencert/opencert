/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.opencert.webapp.reports.exttools.ExternalToolConnectorsRegistry;
import org.eclipse.opencert.webapp.reports.exttools.connectors.NotDefinedConnector;
import org.eclipse.opencert.webapp.reports.exttools.connectors.NotNeededConnector;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.externaltools.api.ExternalToolQuery;
import org.eclipse.opencert.externaltools.api.ExternalToolQuerySetting;
import org.eclipse.opencert.externaltools.api.IExternalToolConnector;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQueryDAO;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQueryDAOImpl;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ExternalToolConnectorWindow
        extends Window
{
    private static final String RESULT_MSG_LABEL = " Msg: ";

    private static final long serialVersionUID = 392685431243145173L;

    private IExternalToolConnector selectedConnector;

    private ExternalToolQuery externalToolQuery;

    private Panel instanceSettingsPanel = new Panel("Instance Settings");

    private Button saveButton;

    private Component connectorInstancePanel;

    private TextField g2;
    private TextField gMsg;

    private CheckBox enableYellowRange;
    private TextField y1;
    private TextField y2;
    private TextField yMsg;

    private TextField r2;
    private TextField rMsg;

    private TextField instanceLabelField;

    public ExternalToolConnectorWindow(long currentBaseElementCdoId)
    {
        super("Add/Edit External Tool Connector");
        setModal(true);
                
        ExternalToolQueryDAO externalToolQueryDAO = (ExternalToolQueryDAO) SpringContextHelper.getBeanFromWebappContext(ExternalToolQueryDAOImpl.SPRING_NAME);
        externalToolQuery = externalToolQueryDAO.findRecentByBaseElementId(currentBaseElementCdoId);

        setStyleName("externalToolConnectorWindow");

        VerticalLayout mainContent = new VerticalLayout();

        mainContent.addComponent(createConnectorTypePanel());

        connectorInstancePanel = createConnectorInstancePanel();

        if (externalToolQuery.getExternalToolConnectorId().equals(NotNeededConnector.class.getName())
                || externalToolQuery.getExternalToolConnectorId().equals(NotDefinedConnector.class.getName())) {
            connectorInstancePanel.setVisible(false);
        } else {
            connectorInstancePanel.setVisible(true);
        }

        mainContent.addComponent(connectorInstancePanel);

        mainContent.addComponent(createSaveAndCancelButtons());

        setContent(mainContent);

        this.addCloseListener(new CloseListener() {
            private static final long serialVersionUID = 6673378500979019062L;

            @Override
            public void windowClose(CloseEvent e)
            {

            }
        });
    }

    private Component createConnectorTypePanel()
    {
        Panel connectorTypePanel = new Panel("Connector Type");
        connectorTypePanel.setStyleName("externalToolConnectorTypePanelStyle windowSection");

        ExternalToolConnectorsRegistry externalToolConnectorsRegistry = new ExternalToolConnectorsRegistry();
        List<IExternalToolConnector> connectors = externalToolConnectorsRegistry.getExternalToolConnectors();

        BeanItemContainer<IExternalToolConnector> connectorsContainer = new BeanItemContainer<IExternalToolConnector>(IExternalToolConnector.class, connectors);

        final NativeSelect connectorClassSelect = new NativeSelect(null, connectorsContainer);
        connectorClassSelect.setStyleName("connectorType");
        connectorClassSelect.setImmediate(true);

        List<IExternalToolConnector> connectorsList = new ArrayList<IExternalToolConnector>();
        connectorsList.addAll(connectors);

        initSelectedConnector(connectorsList);

        connectorClassSelect.setValue(selectedConnector);
        connectorClassSelect.setItemCaptionPropertyId("name");

        final Label connectorDescLabel = new Label();
        connectorDescLabel.setStyleName("connectorTypeDescription");
        connectorDescLabel.setValue("" + ((IExternalToolConnector) connectorClassSelect.getValue()).getDescription());

        connectorClassSelect.setNullSelectionAllowed(false);

        connectorClassSelect.addValueChangeListener(new ValueChangeListener() {

            private static final long serialVersionUID = -6331557979793223438L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                selectedConnector = (IExternalToolConnector) event.getProperty().getValue();

                if (selectedConnector.getExternalToolConnectorId().equals(NotNeededConnector.class.getName())
                        || selectedConnector.getExternalToolConnectorId().equals(NotDefinedConnector.class.getName())) {
                    connectorInstancePanel.setVisible(false);
                } else {
                    connectorInstancePanel.setVisible(true);
                }

                externalToolQuery = selectedConnector.bootstrapExternalToolQueryInstance();

                connectorDescLabel.setValue("" + selectedConnector.getDescription());

                VerticalLayout keyFieldsLayout = new VerticalLayout();
                
                instanceLabelField.setValue(externalToolQuery.getInstanceLabel() != null ? externalToolQuery.getInstanceLabel() : "");
                
                generateInstanceSettings(keyFieldsLayout);

                setInstanceResultProcessingSettingsValuesInGUI(g2, gMsg, enableYellowRange, y1, y2, yMsg, r2, rMsg);

                instanceSettingsPanel.setContent(keyFieldsLayout);
            }
        });

        HorizontalLayout connectorTypeLayout = new HorizontalLayout();
        connectorTypeLayout.addComponent(connectorClassSelect);
        connectorTypeLayout.addComponent(connectorDescLabel);

        connectorTypePanel.setContent(connectorTypeLayout);
        
        return connectorTypePanel;
    }

    private Component createConnectorInstancePanel()
    {
        Panel connectorInstancePanel = new Panel("Connector Instance");
        connectorInstancePanel.setImmediate(true);
        connectorInstancePanel.setStyleName("externalToolConnectorInstancePanelStyle windowSection");

        instanceLabelField = new TextField("Instance Label: ");
        instanceLabelField.setValue(externalToolQuery.getInstanceLabel() != null ? externalToolQuery.getInstanceLabel() : "");
        instanceLabelField.addValueChangeListener((event) ->
        {
            externalToolQuery.setInstanceLabel((String) event.getProperty().getValue());
        }
        );
        instanceLabelField.setStyleName("externalToolTextFieldStyle");

        VerticalLayout instanceAndResultProcessingLayout = new VerticalLayout();
        instanceAndResultProcessingLayout.addComponent(instanceLabelField);
        
        HorizontalLayout settingsAndResultPanel = new HorizontalLayout();
        settingsAndResultPanel.addComponent(createInstanceSettingsPanel());
        settingsAndResultPanel.addComponent(createInstanceResultProcessingPanel());
        instanceAndResultProcessingLayout.addComponent(settingsAndResultPanel);

        connectorInstancePanel.setContent(instanceAndResultProcessingLayout);

        return connectorInstancePanel;
    }

    private Component createInstanceSettingsPanel()
    {
        instanceSettingsPanel.setStyleName("externalInstanceSettingsPanelStyle windowSection");

        VerticalLayout keyFieldsLayout = new VerticalLayout();
        generateInstanceSettings(keyFieldsLayout);

        instanceSettingsPanel.setContent(keyFieldsLayout);
        return instanceSettingsPanel;
    }

    private Component createInstanceResultProcessingPanel()
    {
        Panel instanceResultProcessingPanel = new Panel("Instance Execution Result Processing");
        instanceResultProcessingPanel.setStyleName("externalToolinstanceResultProcessingPanelStyle windowSection");

        Label resultProcessingDescriptionLabel = new Label(
                "A result of connector execution is a number.<br/>"
                + "For the specific result number ranges, please define:"
                    + "<ul>"
                    + "<li>color of the indicator (Green, Yellow, Red)</li>"
                    + "<li>message - the text to be displayed on the indicator.<br/>"
                    + "If you enter {result}, the actual result number "
                    + "will be presented</li>"
                    + "</ul>", ContentMode.HTML);
        resultProcessingDescriptionLabel.setStyleName("resultProcessingDescriptionLabelStyle");

        GridLayout resultColorsLayout = new GridLayout(7, 3);
        resultColorsLayout.setSizeFull();
        resultColorsLayout.setWidth("380px");
        resultColorsLayout.setHeight("80px");
        resultColorsLayout.setSpacing(true);

        Label gLabel = new Label("Green:");
        gLabel.setWidth(null);
        Label g2Label = new Label("Result <");
        g2Label.setWidth(null);
        
        g2 = createResultField(null, "externalToolRPTextFieldStyle");
        g2.addValueChangeListener(new ValueChangeListener() {

            private static final long serialVersionUID = 4715881879930854701L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                try {
                    externalToolQuery.setGreenStatusMaxValue(Integer.valueOf(((String) event.getProperty().getValue())).intValue());
                } catch (NumberFormatException e) {
                    Notification.show("Only Integers allowed!", Type.WARNING_MESSAGE);
                    g2.setValue("0");
                    externalToolQuery.setGreenStatusMaxValue(0);
                }
            }
        });

        Label g0Msg = new Label(RESULT_MSG_LABEL);
        g0Msg.setWidth(null);
        gMsg = createResultField(null, "externalToolMsgStyle");
        gMsg.setImmediate(true);
        gMsg.addValueChangeListener(new ValueChangeListener() {

            private static final long serialVersionUID = 1957990628396228333L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                externalToolQuery.setGreenStatusMsg((String) event.getProperty().getValue());
            }
        });

        resultColorsLayout.addComponent(gLabel, 1, 0);
        resultColorsLayout.addComponent(g2Label, 3, 0);
        
        resultColorsLayout.addComponent(g2, 4, 0);
        resultColorsLayout.addComponent(g0Msg, 5, 0);
        resultColorsLayout.addComponent(gMsg, 6, 0);


        final Label yLabel = new Label("Yellow: ");
        y1 = createResultField(null, "externalToolRPTextFieldStyle");
        y1.addValueChangeListener(new ValueChangeListener() {

            private static final long serialVersionUID = 888631899447244865L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                try {
                    externalToolQuery.setYellowStatusMinValue(Integer.valueOf(((String) event.getProperty().getValue())).intValue());
                } catch (NumberFormatException e) {
                    Notification.show("Only Integers allowed!", Type.WARNING_MESSAGE);
                    y1.setValue("0");
                    externalToolQuery.setYellowStatusMinValue(0);
                }
            }
        });

        final Label yLabel2 = new Label(" < Result < ");
        yLabel2.setWidth(null);
        y2 = createResultField(null, "externalToolRPTextFieldStyle");
        y2.addValueChangeListener(new ValueChangeListener() {

            private static final long serialVersionUID = 5676009029521095443L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                try {
                    externalToolQuery.setYellowStatusMaxValue(Integer.valueOf(((String) event.getProperty().getValue())).intValue());
                } catch (NumberFormatException e) {
                    Notification.show("Only Integers allowed!", Type.WARNING_MESSAGE);
                    y2.setValue("0");
                    externalToolQuery.setYellowStatusMaxValue(0);
                }
            }
        });

        final Label yLabelMsg = new Label(RESULT_MSG_LABEL);
        yMsg = createResultField(null, "externalToolMsgStyle");
        yMsg.addValueChangeListener(new ValueChangeListener() {

            private static final long serialVersionUID = -1217564601332567537L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                externalToolQuery.setYellowStatusMsg((String) event.getProperty().getValue());
            }
        });

        enableYellowRange = new CheckBox("");
        enableYellowRange.addValueChangeListener((e) -> 
        {
            final boolean isChecked = enableYellowRange.getValue();
            externalToolQuery.setYellowEnabled(isChecked);
            enableYellowComponents(isChecked, yLabel, yLabel2, yLabelMsg);
        });
        final boolean isC = externalToolQuery.isYellowEnabled();
        enableYellowRange.setValue(isC);
        enableYellowComponents(isC, yLabel, yLabel2, yLabelMsg);
        
        resultColorsLayout.addComponent(enableYellowRange, 0, 1);
        resultColorsLayout.addComponent(yLabel, 1, 1);
        resultColorsLayout.addComponent(y1, 2, 1);
        resultColorsLayout.addComponent(yLabel2, 3, 1);
        resultColorsLayout.addComponent(y2, 4, 1);
        resultColorsLayout.addComponent(yLabelMsg, 5, 1);
        resultColorsLayout.addComponent(yMsg, 6, 1);
        
        Label rLabel = new Label("Red:");
        Label r2Label = new Label("Result >");
        r2 = createResultField(null, "externalToolRPTextFieldStyle");
        r2.addValueChangeListener(new ValueChangeListener() {

            private static final long serialVersionUID = -7676473529489946340L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                try {
                    externalToolQuery.setRedStatusMinValue(Integer.valueOf(((String) event.getProperty().getValue())).intValue());
                } catch (NumberFormatException e) {
                    Notification.show("Only Integers allowed!", Type.WARNING_MESSAGE);
                    r2.setValue("0");
                    externalToolQuery.setRedStatusMinValue(0);
                }
            }
        });

        Label r0Msg = new Label(RESULT_MSG_LABEL);
        rMsg = createResultField(null, "externalToolMsgStyle");
        rMsg.addValueChangeListener(new ValueChangeListener() {

            private static final long serialVersionUID = -2329987742752882823L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                externalToolQuery.setRedStatusMsg((String) event.getProperty().getValue());
            }
        });

        resultColorsLayout.addComponent(rLabel, 1, 2);
        resultColorsLayout.addComponent(r2Label, 3, 2);
        resultColorsLayout.addComponent(r2, 4, 2);
        resultColorsLayout.addComponent(r0Msg, 5, 2);
        resultColorsLayout.addComponent(rMsg, 6, 2);

        VerticalLayout instanceResultProcessingLayout = new VerticalLayout();
        instanceResultProcessingLayout.addComponent(resultProcessingDescriptionLabel);
        instanceResultProcessingLayout.addComponent(resultColorsLayout);

        instanceResultProcessingPanel.setContent(instanceResultProcessingLayout);

        setInstanceResultProcessingSettingsValuesInGUI(g2, gMsg, enableYellowRange, y1, y2, yMsg, r2, rMsg);

        return instanceResultProcessingPanel;
    }

    private void enableYellowComponents(final boolean isChecked, Label... labels)
    {
        y1.setEnabled(isChecked);
        y2.setEnabled(isChecked);
        yMsg.setEnabled(isChecked);
        
        if (labels == null) {
            return;
        }
        for (Label label : labels) {
            label.setEnabled(isChecked);
        }
    }

    private TextField createResultField(String name, String cssStyle)
    {
        final TextField result = new TextField(name);
        result.setStyleName(cssStyle);
        
        return result;
    }

    private void setInstanceResultProcessingSettingsValuesInGUI(TextField g2, TextField gMsg, CheckBox enableYellow, TextField y1, TextField y2, TextField yMsg,
            TextField r2, TextField rMsg)
    {
        g2.setValue(String.valueOf(externalToolQuery.getGreenStatusMaxValue()));
        gMsg.setValue(String.valueOf(externalToolQuery.getGreenStatusMsg()));

        enableYellow.setValue(externalToolQuery.isYellowEnabled());
        y1.setValue(String.valueOf(externalToolQuery.getYellowStatusMinValue()));
        y2.setValue(String.valueOf(externalToolQuery.getYellowStatusMaxValue()));
        yMsg.setValue(String.valueOf(externalToolQuery.getYellowStatusMsg()));

        r2.setValue(String.valueOf(externalToolQuery.getRedStatusMinValue()));
        rMsg.setValue(String.valueOf(externalToolQuery.getRedStatusMsg()));
    }

    private HorizontalLayout createSaveAndCancelButtons()
    {
        saveButton = new Button("Save");

        Button cancelButton = new Button("Cancel");
        cancelButton.addClickListener((e) -> close());

        HorizontalLayout saveAndCancelButtonsLayout = new HorizontalLayout();
        saveAndCancelButtonsLayout.addComponent(saveButton);
        saveAndCancelButtonsLayout.addComponent(cancelButton);
        saveAndCancelButtonsLayout.addStyleName("saveAndCancelButtonsStyle commonButton");

        return saveAndCancelButtonsLayout;
    }

    private void generateInstanceSettings(Layout keyFieldsLayout)
    {        
        final Map<String, String> settingsMap = externalToolQuery.getSettingsAsMap();
        if (selectedConnector == null) {
            throw new IllegalStateException("Null selectedConnector");
        }
        final List<String> settingKeys = selectedConnector.getSettingKeys();
             
        for (String settingKey : settingKeys)
        {       
            final TextField textField = new TextField(ExternalToolSettingGUIUtil.generateLabelFromKey(settingKey));
            textField.setImmediate(true);
            textField.setStyleName("externalToolTextFieldStyle");
            final String value = settingsMap.get(settingKey);
            if (value == null) {
                OpencertLogger.error("Illegal State! Null value for settings " + settingKey + " for connector: " + selectedConnector + " settingsMap: " + settingsMap);
            }
            textField.setValue(value);

            textField.addValueChangeListener(new ValueChangeListener() {

                private static final long serialVersionUID = -1680454855393996670L;

                @Override
                public void valueChange(ValueChangeEvent event)
                {
                    final String key = ExternalToolSettingGUIUtil.generateKeyFromLabel(textField.getCaption());
                    String value = (String) event.getProperty().getValue();
                    
                    final List<ExternalToolQuerySetting> externalToolQuerySettings = new ArrayList<ExternalToolQuerySetting>(
                            externalToolQuery.getExternalToolQuerySettings());
                    for (ExternalToolQuerySetting eTQuerySetting : externalToolQuerySettings) 
                    {
                        if (eTQuerySetting.getName().equals(key)) {
                            eTQuerySetting.setValue(value);
                            break;
                        }
                    }
                }
            });

            keyFieldsLayout.addComponent(textField);
        }
    }

    private static class ExternalToolSettingGUIUtil
    {
        public static final String COLON = ":";
        public static String generateLabelFromKey(String eTQSettingKey)
        {
            return eTQSettingKey + COLON;
        }
        
        public static String generateKeyFromLabel(String label)
        {
            if (!label.endsWith(COLON)) {
                throw new IllegalArgumentException();
            }
            return label.substring(0, label.length()-1);
        }
    }

    private void initSelectedConnector(List<IExternalToolConnector> connectorsList)
    {
        if (externalToolQuery == null) {
            selectedConnector = connectorsList.get(0);
            externalToolQuery = selectedConnector.bootstrapExternalToolQueryInstance();
        } else {
            for (IExternalToolConnector connector : connectorsList) {
                if (connector.getExternalToolConnectorId().equals(externalToolQuery.getExternalToolConnectorId())) {
                    selectedConnector = connector;
                    break;
                }
            }
        }
    }

    public IExternalToolConnector getSelectedConnector()
    {
        return selectedConnector;
    }

    public Button getSaveButton()
    {
        return saveButton;
    }

    public ExternalToolQuery getExternalToolQuery()
    {
        return externalToolQuery;
    }
}
