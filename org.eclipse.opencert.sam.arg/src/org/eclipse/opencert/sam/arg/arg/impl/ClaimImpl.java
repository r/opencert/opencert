/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Choice;
import org.eclipse.opencert.sam.arg.arg.Claim;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Claim</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl#getEvaluation <em>Evaluation</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl#getLifecycleEvent <em>Lifecycle Event</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl#getPublic <em>Public</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl#getAssumed <em>Assumed</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl#getToBeSupported <em>To Be Supported</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl#getToBeInstantiated <em>To Be Instantiated</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl#getChoice <em>Choice</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClaimImpl extends AssertionImpl implements Claim {
	/**
	 * The default value of the '{@link #getPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublic()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean PUBLIC_EDEFAULT = Boolean.FALSE;

	/**
	 * The default value of the '{@link #getAssumed() <em>Assumed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssumed()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ASSUMED_EDEFAULT = Boolean.FALSE;

	/**
	 * The default value of the '{@link #getToBeSupported() <em>To Be Supported</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToBeSupported()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean TO_BE_SUPPORTED_EDEFAULT = Boolean.FALSE;

	/**
	 * The default value of the '{@link #getToBeInstantiated() <em>To Be Instantiated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToBeInstantiated()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean TO_BE_INSTANTIATED_EDEFAULT = Boolean.FALSE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClaimImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.CLAIM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssuranceAssetEvaluation> getEvaluation() {
		return (EList<AssuranceAssetEvaluation>)eDynamicGet(ArgPackage.CLAIM__EVALUATION, AssuranceassetPackage.Literals.MANAGEABLE_ASSURANCE_ASSET__EVALUATION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssuranceAssetEvent> getLifecycleEvent() {
		return (EList<AssuranceAssetEvent>)eDynamicGet(ArgPackage.CLAIM__LIFECYCLE_EVENT, AssuranceassetPackage.Literals.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getPublic() {
		return (Boolean)eDynamicGet(ArgPackage.CLAIM__PUBLIC, ArgPackage.Literals.CLAIM__PUBLIC, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublic(Boolean newPublic) {
		eDynamicSet(ArgPackage.CLAIM__PUBLIC, ArgPackage.Literals.CLAIM__PUBLIC, newPublic);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAssumed() {
		return (Boolean)eDynamicGet(ArgPackage.CLAIM__ASSUMED, ArgPackage.Literals.CLAIM__ASSUMED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssumed(Boolean newAssumed) {
		eDynamicSet(ArgPackage.CLAIM__ASSUMED, ArgPackage.Literals.CLAIM__ASSUMED, newAssumed);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getToBeSupported() {
		return (Boolean)eDynamicGet(ArgPackage.CLAIM__TO_BE_SUPPORTED, ArgPackage.Literals.CLAIM__TO_BE_SUPPORTED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToBeSupported(Boolean newToBeSupported) {
		eDynamicSet(ArgPackage.CLAIM__TO_BE_SUPPORTED, ArgPackage.Literals.CLAIM__TO_BE_SUPPORTED, newToBeSupported);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getToBeInstantiated() {
		return (Boolean)eDynamicGet(ArgPackage.CLAIM__TO_BE_INSTANTIATED, ArgPackage.Literals.CLAIM__TO_BE_INSTANTIATED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToBeInstantiated(Boolean newToBeInstantiated) {
		eDynamicSet(ArgPackage.CLAIM__TO_BE_INSTANTIATED, ArgPackage.Literals.CLAIM__TO_BE_INSTANTIATED, newToBeInstantiated);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Choice getChoice() {
		return (Choice)eDynamicGet(ArgPackage.CLAIM__CHOICE, ArgPackage.Literals.CLAIM__CHOICE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChoice(Choice newChoice, NotificationChain msgs) {
		msgs = eDynamicInverseAdd((InternalEObject)newChoice, ArgPackage.CLAIM__CHOICE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoice(Choice newChoice) {
		eDynamicSet(ArgPackage.CLAIM__CHOICE, ArgPackage.Literals.CLAIM__CHOICE, newChoice);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArgPackage.CLAIM__EVALUATION:
				return ((InternalEList<?>)getEvaluation()).basicRemove(otherEnd, msgs);
			case ArgPackage.CLAIM__LIFECYCLE_EVENT:
				return ((InternalEList<?>)getLifecycleEvent()).basicRemove(otherEnd, msgs);
			case ArgPackage.CLAIM__CHOICE:
				return basicSetChoice(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.CLAIM__EVALUATION:
				return getEvaluation();
			case ArgPackage.CLAIM__LIFECYCLE_EVENT:
				return getLifecycleEvent();
			case ArgPackage.CLAIM__PUBLIC:
				return getPublic();
			case ArgPackage.CLAIM__ASSUMED:
				return getAssumed();
			case ArgPackage.CLAIM__TO_BE_SUPPORTED:
				return getToBeSupported();
			case ArgPackage.CLAIM__TO_BE_INSTANTIATED:
				return getToBeInstantiated();
			case ArgPackage.CLAIM__CHOICE:
				return getChoice();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.CLAIM__EVALUATION:
				getEvaluation().clear();
				getEvaluation().addAll((Collection<? extends AssuranceAssetEvaluation>)newValue);
				return;
			case ArgPackage.CLAIM__LIFECYCLE_EVENT:
				getLifecycleEvent().clear();
				getLifecycleEvent().addAll((Collection<? extends AssuranceAssetEvent>)newValue);
				return;
			case ArgPackage.CLAIM__PUBLIC:
				setPublic((Boolean)newValue);
				return;
			case ArgPackage.CLAIM__ASSUMED:
				setAssumed((Boolean)newValue);
				return;
			case ArgPackage.CLAIM__TO_BE_SUPPORTED:
				setToBeSupported((Boolean)newValue);
				return;
			case ArgPackage.CLAIM__TO_BE_INSTANTIATED:
				setToBeInstantiated((Boolean)newValue);
				return;
			case ArgPackage.CLAIM__CHOICE:
				setChoice((Choice)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.CLAIM__EVALUATION:
				getEvaluation().clear();
				return;
			case ArgPackage.CLAIM__LIFECYCLE_EVENT:
				getLifecycleEvent().clear();
				return;
			case ArgPackage.CLAIM__PUBLIC:
				setPublic(PUBLIC_EDEFAULT);
				return;
			case ArgPackage.CLAIM__ASSUMED:
				setAssumed(ASSUMED_EDEFAULT);
				return;
			case ArgPackage.CLAIM__TO_BE_SUPPORTED:
				setToBeSupported(TO_BE_SUPPORTED_EDEFAULT);
				return;
			case ArgPackage.CLAIM__TO_BE_INSTANTIATED:
				setToBeInstantiated(TO_BE_INSTANTIATED_EDEFAULT);
				return;
			case ArgPackage.CLAIM__CHOICE:
				setChoice((Choice)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.CLAIM__EVALUATION:
				return !getEvaluation().isEmpty();
			case ArgPackage.CLAIM__LIFECYCLE_EVENT:
				return !getLifecycleEvent().isEmpty();
			case ArgPackage.CLAIM__PUBLIC:
				return PUBLIC_EDEFAULT == null ? getPublic() != null : !PUBLIC_EDEFAULT.equals(getPublic());
			case ArgPackage.CLAIM__ASSUMED:
				return ASSUMED_EDEFAULT == null ? getAssumed() != null : !ASSUMED_EDEFAULT.equals(getAssumed());
			case ArgPackage.CLAIM__TO_BE_SUPPORTED:
				return TO_BE_SUPPORTED_EDEFAULT == null ? getToBeSupported() != null : !TO_BE_SUPPORTED_EDEFAULT.equals(getToBeSupported());
			case ArgPackage.CLAIM__TO_BE_INSTANTIATED:
				return TO_BE_INSTANTIATED_EDEFAULT == null ? getToBeInstantiated() != null : !TO_BE_INSTANTIATED_EDEFAULT.equals(getToBeInstantiated());
			case ArgPackage.CLAIM__CHOICE:
				return getChoice() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AssuranceAsset.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ManageableAssuranceAsset.class) {
			switch (derivedFeatureID) {
				case ArgPackage.CLAIM__EVALUATION: return AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__EVALUATION;
				case ArgPackage.CLAIM__LIFECYCLE_EVENT: return AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AssuranceAsset.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ManageableAssuranceAsset.class) {
			switch (baseFeatureID) {
				case AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__EVALUATION: return ArgPackage.CLAIM__EVALUATION;
				case AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT: return ArgPackage.CLAIM__LIFECYCLE_EVENT;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ClaimImpl
