/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.externaltools.api.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.eclipse.opencert.externaltools.api.ExternalToolQueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ExternalToolQueryResultDAOImpl
        implements ExternalToolQueryResultDAO
{
    @Autowired
    @Qualifier("hibernate4AnnotatedSessionFactory")
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void save(ExternalToolQueryResult externalToolQueryResult)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(externalToolQueryResult);
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public List<ExternalToolQueryResult> list()
    {
        Session session = this.sessionFactory.getCurrentSession();
        List<ExternalToolQueryResult> externalToolQueryResultList = session.createQuery("from ExternalToolQueryResult").list();

        return externalToolQueryResultList;
    }
}
