/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;

// End of user code

/**
 * 
 * 
 */
public class ArtefactDefinitionEventsPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ArtefactDefinitionEventsPropertiesEditionPart {

	protected ReferencesTable lifecycleEvent;
	protected List<ViewerFilter> lifecycleEventBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> lifecycleEventFilters = new ArrayList<ViewerFilter>();
	protected TableViewer lifecycleEventTable;
	protected List<ViewerFilter> lifecycleEventTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> lifecycleEventTableFilters = new ArrayList<ViewerFilter>();
	protected Button addLifecycleEventTable;
	protected Button removeLifecycleEventTable;
	protected Button editLifecycleEventTable;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArtefactDefinitionEventsPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence artefactDefinitionEventsStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = artefactDefinitionEventsStep.addStep(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.class);
		// Start IRR
		// propertiesStep.addStep(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent);
		// Fin IRR
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable);
		
		
		composer = new PartComposer(artefactDefinitionEventsStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent) {
					return createLifecycleEventAdvancedTableComposition(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable) {
					return createLifecycleEventTableTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(EvidenceMessages.ArtefactDefinitionEventsPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createLifecycleEventAdvancedTableComposition(Composite parent) {
		this.lifecycleEvent = new ReferencesTable(getDescription(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent, EvidenceMessages.ArtefactDefinitionEventsPropertiesEditionPart_LifecycleEventLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEventsPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				lifecycleEvent.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEventsPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				lifecycleEvent.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEventsPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				lifecycleEvent.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEventsPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				lifecycleEvent.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.lifecycleEventFilters) {
			this.lifecycleEvent.addFilter(filter);
		}
		this.lifecycleEvent.setHelpText(propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent, EvidenceViewsRepository.SWT_KIND));
		this.lifecycleEvent.createControls(parent);
		this.lifecycleEvent.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEventsPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData lifecycleEventData = new GridData(GridData.FILL_HORIZONTAL);
		lifecycleEventData.horizontalSpan = 3;
		this.lifecycleEvent.setLayoutData(lifecycleEventData);
		this.lifecycleEvent.setLowerBound(0);
		this.lifecycleEvent.setUpperBound(-1);
		lifecycleEvent.setID(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent);
		lifecycleEvent.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createLifecycleEventAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createLifecycleEventTableTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableLifecycleEventTable = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableLifecycleEventTable.setHeaderVisible(true);
		GridData gdLifecycleEventTable = new GridData();
		gdLifecycleEventTable.grabExcessHorizontalSpace = true;
		gdLifecycleEventTable.horizontalAlignment = GridData.FILL;
		gdLifecycleEventTable.grabExcessVerticalSpace = true;
		gdLifecycleEventTable.verticalAlignment = GridData.FILL;
		tableLifecycleEventTable.setLayoutData(gdLifecycleEventTable);
		tableLifecycleEventTable.setLinesVisible(true);

		// Start of user code for columns definition for LifecycleEventTable
		// Start IRR

		TableColumn name = new TableColumn(tableLifecycleEventTable, SWT.NONE);
		name.setWidth(80);
		name.setText("Name"); //$NON-NLS-1$

		TableColumn name1 = new TableColumn(tableLifecycleEventTable, SWT.NONE);
		name1.setWidth(80);
		name1.setText("Description"); //$NON-NLS-1$

		// End IRR
		// End of user code

		lifecycleEventTable = new TableViewer(tableLifecycleEventTable);
		lifecycleEventTable.setContentProvider(new ArrayContentProvider());
		lifecycleEventTable.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for LifecycleEventTable
						public String getColumnText(Object object, int columnIndex) {
							// Start IRR
							/*AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
							if (object instanceof EObject) {
								switch (columnIndex) {
								case 0:
									return labelProvider.getText(object);
								}
							}*/
							
							if (object instanceof EObject) {
								AssuranceAssetEvent assuranceAssetEventObject = (AssuranceAssetEvent)object;
								switch (columnIndex) {
								case 0:							
									return assuranceAssetEventObject.getName();
								case 1:							
									return assuranceAssetEventObject.getDescription();
								
								}
							}
							return ""; //$NON-NLS-1$
						}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		lifecycleEventTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (lifecycleEventTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) lifecycleEventTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEventsPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						lifecycleEventTable.refresh();
					}
				}
			}

		});
		GridData lifecycleEventTableData = new GridData(GridData.FILL_HORIZONTAL);
		lifecycleEventTableData.minimumHeight = 120;
		lifecycleEventTableData.heightHint = 120;
		lifecycleEventTable.getTable().setLayoutData(lifecycleEventTableData);
		for (ViewerFilter filter : this.lifecycleEventTableFilters) {
			lifecycleEventTable.addFilter(filter);
		}
		EditingUtils.setID(lifecycleEventTable.getTable(), EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable);
		EditingUtils.setEEFtype(lifecycleEventTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createLifecycleEventTablePanel(tableContainer);
		// Start of user code for createLifecycleEventTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createLifecycleEventTablePanel(Composite container) {
		Composite lifecycleEventTablePanel = new Composite(container, SWT.NONE);
		GridLayout lifecycleEventTablePanelLayout = new GridLayout();
		lifecycleEventTablePanelLayout.numColumns = 1;
		lifecycleEventTablePanel.setLayout(lifecycleEventTablePanelLayout);
		addLifecycleEventTable = new Button(lifecycleEventTablePanel, SWT.NONE);
		addLifecycleEventTable.setText(EvidenceMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addLifecycleEventTableData = new GridData(GridData.FILL_HORIZONTAL);
		addLifecycleEventTable.setLayoutData(addLifecycleEventTableData);
		addLifecycleEventTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEventsPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				lifecycleEventTable.refresh();
			}
		});
		EditingUtils.setID(addLifecycleEventTable, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable);
		EditingUtils.setEEFtype(addLifecycleEventTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeLifecycleEventTable = new Button(lifecycleEventTablePanel, SWT.NONE);
		removeLifecycleEventTable.setText(EvidenceMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeLifecycleEventTableData = new GridData(GridData.FILL_HORIZONTAL);
		removeLifecycleEventTable.setLayoutData(removeLifecycleEventTableData);
		removeLifecycleEventTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (lifecycleEventTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) lifecycleEventTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEventsPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						lifecycleEventTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeLifecycleEventTable, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable);
		EditingUtils.setEEFtype(removeLifecycleEventTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editLifecycleEventTable = new Button(lifecycleEventTablePanel, SWT.NONE);
		editLifecycleEventTable.setText(EvidenceMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editLifecycleEventTableData = new GridData(GridData.FILL_HORIZONTAL);
		editLifecycleEventTable.setLayoutData(editLifecycleEventTableData);
		editLifecycleEventTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (lifecycleEventTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) lifecycleEventTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionEventsPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						lifecycleEventTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editLifecycleEventTable, EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable);
		EditingUtils.setEEFtype(editLifecycleEventTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createLifecycleEventTablePanel

		// End of user code
		return lifecycleEventTablePanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart#initLifecycleEvent(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initLifecycleEvent(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		lifecycleEvent.setContentProvider(contentProvider);
		lifecycleEvent.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent);
		if (eefElementEditorReadOnlyState && lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(false);
			lifecycleEvent.setToolTipText(EvidenceMessages.ArtefactDefinitionEvents_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart#updateLifecycleEvent()
	 * 
	 */
	public void updateLifecycleEvent() {
	lifecycleEvent.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart#addFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventFilters.add(filter);
		if (this.lifecycleEvent != null) {
			this.lifecycleEvent.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart#addBusinessFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart#isContainedInLifecycleEventTable(EObject element)
	 * 
	 */
	public boolean isContainedInLifecycleEventTable(EObject element) {
		return ((ReferencesTableSettings)lifecycleEvent.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart#initLifecycleEventTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initLifecycleEventTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		lifecycleEventTable.setContentProvider(contentProvider);
		lifecycleEventTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable);
		if (eefElementEditorReadOnlyState && lifecycleEventTable.getTable().isEnabled()) {
			lifecycleEventTable.getTable().setEnabled(false);
			lifecycleEventTable.getTable().setToolTipText(EvidenceMessages.ArtefactDefinitionEvents_ReadOnly);
			addLifecycleEventTable.setEnabled(false);
			addLifecycleEventTable.setToolTipText(EvidenceMessages.ArtefactDefinitionEvents_ReadOnly);
			removeLifecycleEventTable.setEnabled(false);
			removeLifecycleEventTable.setToolTipText(EvidenceMessages.ArtefactDefinitionEvents_ReadOnly);
			editLifecycleEventTable.setEnabled(false);
			editLifecycleEventTable.setToolTipText(EvidenceMessages.ArtefactDefinitionEvents_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !lifecycleEventTable.getTable().isEnabled()) {
			lifecycleEventTable.getTable().setEnabled(true);
			addLifecycleEventTable.setEnabled(true);
			removeLifecycleEventTable.setEnabled(true);
			editLifecycleEventTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart#updateLifecycleEventTable()
	 * 
	 */
	public void updateLifecycleEventTable() {
	lifecycleEventTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart#addFilterLifecycleEventTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToLifecycleEventTable(ViewerFilter filter) {
		lifecycleEventTableFilters.add(filter);
		if (this.lifecycleEventTable != null) {
			this.lifecycleEventTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart#addBusinessFilterLifecycleEventTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEventTable(ViewerFilter filter) {
		lifecycleEventTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart#isContainedInLifecycleEventTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInLifecycleEventTableTable(EObject element) {
		return ((ReferencesTableSettings)lifecycleEventTable.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.ArtefactDefinitionEvents_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
