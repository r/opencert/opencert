/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.autocomplete;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalListener;
import org.eclipse.swt.custom.StyledText;

// Inserts content proposals and corrects the caret offset 
class ContentProposalListener implements IContentProposalListener {
	private StyledText text;
	private List<Character> whiteSpace;

	public ContentProposalListener(StyledText text) {
		this.text = text;

		whiteSpace = new ArrayList<Character>();
		whiteSpace.add(' ');
		whiteSpace.add('\t');
		whiteSpace.add('\n');
	}

	@Override
	public void proposalAccepted(IContentProposal proposal) {
		String content = text.getText();
		int caretOffset = text.getCaretOffset();
		int prevWhiteSpace = getprevWhiteSpace(content, caretOffset);

		StringBuilder sb = new StringBuilder();
		sb.append(content.substring(0, prevWhiteSpace + 1));
		sb.append(proposal.getContent());
		sb.append(content.substring(caretOffset));

		text.setText(sb.toString());
		text.setCaretOffset(prevWhiteSpace + proposal.getContent().length() + 1);
	}

	private int getprevWhiteSpace(String content, int caretOffset) {
		for (int i = caretOffset - 1; i >= 0; i--) {
			char c = content.charAt(i);
			for (char ws : whiteSpace) {
				if (c == ws) {
					return i;
				}
			}
		}

		return -1;
	}
}