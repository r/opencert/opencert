/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.assuranceproject.utils;

import java.util.HashMap;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.etl.CDOTransformHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;


public class ImportFromEpfUI extends Dialog {
	
	private static final String EVIDENCE_Extension = ".evidence";
	private static final String PROCESS_Extension = ".process";
	private static final String EVIDENCE_Folder = "EVIDENCE";
	private static final String PROCESSES_Folder = "PROCESSES";
	
	
	private static final String IMPORT = "Import";
	private static final String CLOSE = "Close";

	protected AssuranceprojectPackage dprojectPackage = AssuranceprojectPackage.eINSTANCE;
	protected AssuranceprojectFactory dprojectFactory = dprojectPackage.getAssuranceprojectFactory();
	
	CDOResourceFolder assuranceprojectFolder;
	CDOResourceFolder evidenceFolder;
	CDOResourceFolder procFolder;

	private IEditorPart editor;
	private URI resourceURI;
	private CDOSession sessionCDO = null;
	private CDOTransaction transaction;
	HashMap<Object, Object> options = new HashMap<Object, Object>();
	private static final String UTF_8 = "UTF-8";
			
	private Label methodLabel;
	private Text methodText;
	private Button methodButton;
	private String methodPath;
	private String methodDirectory;

	private Text configurationText;
	private Label configurationLabel;
	private Button configurationButton;
	private String configurationPath;
	private String configurationDirectory;
	
	private Label eviLabel;
	private Text eviNameText;
	private Label importedEviModelPath;

	private Label procLabel;
	private Text procNameText;
	private Label importedProcModelPath;
	
	public ImportFromEpfUI(Shell parentShell) {
		
		super(parentShell);

		options.put(XMLResource.OPTION_ENCODING, UTF_8);
		editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage().getActiveEditor();
		resourceURI = EditUIUtil.getURI(editor.getEditorInput());
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		transaction = sessionCDO.openTransaction();
	}

	public ImportFromEpfUI(IShellProvider parentShell) {
		super(parentShell);
	}
	
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
	    super.createButtonsForButtonBar(parent);

	    Button ok = getButton(IDialogConstants.OK_ID);
	    ok.setText(IMPORT);
	    setButtonLayoutData(ok);

	    Button cancel = getButton(IDialogConstants.CANCEL_ID);
	    cancel.setText(CLOSE);
	    setButtonLayoutData(cancel);
	 }
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		
	
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		final Composite contents = (Composite) super.createDialogArea(parent);

		GridData contentsGridData = (GridData) contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;
					
		createSourceDialogArea(contents, adapterFactory);
		createTargetDialogArea(contents, adapterFactory);
		
		return contents;
	}

	protected void createSourceDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {
		
		final Composite sourceComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, false, true);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			sourceComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 5;			
			
			sourceComposite.setLayout(layout);
		}
		
		Group groupSource = new Group(sourceComposite, SWT.NULL);
		groupSource.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		groupSource.setText("EPF Source Files");
		GridLayout layoutNew = new GridLayout();
		layoutNew.marginHeight = 10;
		layoutNew.marginWidth = 10;
		layoutNew.numColumns = 3;
		groupSource.setLayout(layoutNew);
		
		// Label Method
		methodLabel = new Label(groupSource, SWT.NONE);
		methodLabel.setText("EPF Method Library:");
		GridData methodLabelGridData = new GridData();		
		methodLabelGridData.horizontalAlignment = SWT.FILL;
		methodLabelGridData.verticalAlignment = SWT.FILL;
		methodLabel.setLayoutData(methodLabelGridData);
		// Text Method
		methodText = new Text(groupSource, SWT.BORDER);
		GridData methodTextGridData = new GridData();
		methodTextGridData.grabExcessHorizontalSpace = true;
		methodTextGridData.minimumWidth = 500;
		methodTextGridData.horizontalAlignment = SWT.FILL;
		methodTextGridData.verticalAlignment = SWT.FILL;
		methodText.setLayoutData(methodTextGridData);
		methodText.setEditable(true);
		// Button Method
		methodButton = new Button(groupSource, SWT.NONE);
		methodButton.setText("Browse...");
		methodButton.addSelectionListener(new SelectionAdapter() {
	        public void widgetSelected(SelectionEvent event) {
	          FileDialog dlg = new FileDialog(methodButton.getShell(), SWT.None);

	          // Set the initial filter path according
	          // to anything they've selected or typed in
	          dlg.setFilterPath(methodDirectory);

	          // Change the title bar text
	          dlg.setText("Select a EPF Method File");
	          String[] methodFilterExt = {"*.xml"}; 
	          dlg.setFilterExtensions(methodFilterExt);

	          // Calling open() will open and run the dialog.
	          // It will return the selected directory, or
	          // null if user cancels
	          methodPath = dlg.open();
	          methodDirectory = dlg.getFilterPath();
	          if (methodPath != null) {
	            // Set the text box to the new selection
	        	methodText.setText(methodPath);
	          }
	        }
	      });
	    
		
		// Label Configuration
		configurationLabel = new Label(groupSource, SWT.NONE);
		configurationLabel.setText("EPF Process Configuration:");
		GridData configurationLabelGridData = new GridData();
		configurationLabelGridData.horizontalAlignment = SWT.FILL;
		configurationLabelGridData.verticalAlignment = SWT.FILL;
		configurationLabel.setLayoutData(configurationLabelGridData);
		// Text Configuration
		GridData configurationTextGridData = new GridData();
		configurationTextGridData.grabExcessHorizontalSpace = true;
		configurationTextGridData.minimumWidth = 500;
		configurationTextGridData.horizontalAlignment = SWT.FILL;
		configurationTextGridData.verticalAlignment = SWT.FILL;
		configurationText = new Text(groupSource, SWT.BORDER);		
		configurationText.setLayoutData(configurationTextGridData);
		configurationText.setEditable(true);
		//Button Configuration
		configurationButton = new Button(groupSource, SWT.NONE);
		configurationButton.setText("Browse...");
		configurationButton.addSelectionListener(new SelectionAdapter() {
	        public void widgetSelected(SelectionEvent event) {
	          FileDialog dlg = new FileDialog(configurationButton.getShell(), SWT.None);

	          // Set the initial filter path according
	          // to anything they've selected or typed in
	          dlg.setFilterPath(methodDirectory);

	          // Change the title bar text
	          dlg.setText("Select a EPF Configuration File");
	          String[] configurationFilterExt = {"*.xml"}; 
	          dlg.setFilterExtensions(configurationFilterExt);
	          
	          // Calling open() will open and run the dialog.
	          // It will return the selected directory, or
	          // null if user cancels
	          configurationPath = dlg.open();
	          configurationDirectory = dlg.getFilterPath();
	          String confFileName = dlg.getFileName();
	          if (configurationPath != null) {
	            // Set the text box to the new selection
	        	configurationText.setText(configurationPath);
	        	setDefaultTargetModelNames(confFileName);
	          }
	        }
	      });
						
	}
		
	protected void setDefaultTargetModelNames (final String fileName){
		String defaultEviName="";
		String defaultProcName="";
		
		defaultEviName=fileName.substring(0, fileName.lastIndexOf('.'))  + "_Evid";
		defaultProcName=fileName.substring(0, fileName.lastIndexOf('.'))  + "_Proc";
		
		eviNameText.setText(defaultEviName);
		procNameText.setText(defaultProcName);
		
	}
	
	protected void createTargetDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {

		
		final Composite targetComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, false, true);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			targetComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 5;			
			
			targetComposite.setLayout(layout);
		}
		
		Group groupTarget = new Group(targetComposite, SWT.NULL);
		groupTarget.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		groupTarget.setText("Imported Models (Evidence and Process models)");
		GridLayout layoutNew = new GridLayout();
		layoutNew.marginHeight = 10;
		layoutNew.marginWidth = 10;
		layoutNew.numColumns = 3;
		groupTarget.setLayout(layoutNew);
		
		Label modelLabel = new Label(groupTarget, SWT.NONE);
		modelLabel.setText("");
		GridData modelLabelGridData = new GridData();		
		modelLabelGridData.horizontalAlignment = SWT.FILL;
		modelLabelGridData.verticalAlignment = SWT.FILL;
		modelLabel.setLayoutData(modelLabelGridData);
		
		Label nameLabel = new Label(groupTarget, SWT.NONE);
		nameLabel.setText("Import using Name:");
		GridData nameLabelGridData = new GridData();		
		nameLabelGridData.horizontalAlignment = SWT.FILL;
		nameLabelGridData.verticalAlignment = SWT.FILL;
		nameLabel.setLayoutData(nameLabelGridData);
		
		Label importedLabel = new Label(groupTarget, SWT.NONE);
		String projectPath = resourceURI.path().substring(0, resourceURI.path().lastIndexOf("/"));
		projectPath=projectPath.substring(0, projectPath.lastIndexOf("/"));
		importedLabel.setText("Result @" + projectPath);
		GridData importedLabelGridData = new GridData();		
		importedLabelGridData.horizontalAlignment = SWT.FILL;
		importedLabelGridData.verticalAlignment = SWT.FILL;
		importedLabel.setLayoutData(importedLabelGridData);

		eviLabel = new Label(groupTarget, SWT.NONE);
		eviLabel.setText("EVIDENCE model:");
		GridData eviLabelGridData = new GridData();		
		eviLabelGridData.horizontalAlignment = SWT.FILL;
		eviLabelGridData.verticalAlignment = SWT.FILL;
		eviLabel.setLayoutData(eviLabelGridData);

		// Evidence Name Text 
		eviNameText = new Text(groupTarget, SWT.BORDER);
		GridData eviNameTextGridData = new GridData();
		eviNameTextGridData.grabExcessHorizontalSpace = true;
		eviNameTextGridData.minimumWidth = 200;
		//eviNameTextGridData.horizontalAlignment = SWT.FILL;
		//eviNameTextGridData.verticalAlignment = SWT.FILL;
		eviNameText.setLayoutData(eviNameTextGridData);
		eviNameText.setEditable(true);

		// Path of imported Evidence model
		importedEviModelPath = new Label(groupTarget, SWT.BORDER);
		GridData importedEviModelPathGridData = new GridData();		
		importedEviModelPathGridData.grabExcessHorizontalSpace = true;
		importedEviModelPathGridData.minimumWidth = 350;
		importedEviModelPathGridData.horizontalAlignment = SWT.FILL;
		importedEviModelPathGridData.verticalAlignment = SWT.FILL;
		importedEviModelPath.setLayoutData(importedEviModelPathGridData);
		
		
		procLabel = new Label(groupTarget, SWT.NONE);
		procLabel.setText("PROCESS model:");
		GridData procLabelGridData = new GridData();		
		procLabelGridData.horizontalAlignment = SWT.FILL;
		procLabelGridData.verticalAlignment = SWT.FILL;
		procLabel.setLayoutData(procLabelGridData);
		
		// Process Name Text 
		procNameText = new Text(groupTarget, SWT.BORDER);
		GridData procNameTextGridData = new GridData();
		procNameTextGridData.grabExcessHorizontalSpace = true;
		procNameTextGridData.minimumWidth = 200;
		//procNameTextGridData.horizontalAlignment = SWT.FILL;
		//procNameTextGridData.verticalAlignment = SWT.FILL;
		procNameText.setLayoutData(procNameTextGridData);
		procNameText.setEditable(true);

		// Path of imported Process model
		importedProcModelPath = new Label(groupTarget, SWT.BORDER);
		GridData importedProcModelPathGridData = new GridData();
		importedProcModelPathGridData.grabExcessHorizontalSpace = true;
		importedProcModelPathGridData.minimumWidth = 350;
		importedProcModelPathGridData.horizontalAlignment = SWT.FILL;
		importedProcModelPathGridData.verticalAlignment = SWT.FILL;
		importedProcModelPath.setLayoutData(importedProcModelPathGridData);
	}

	protected void setImportBottom(){
		if((configurationText.getText().length()==0) || (methodText.getText().length()==0)){

		}		
	}
	  		
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Import from EPF");
	}
	
	@Override
	protected void okPressed() {
		boolean allOK=true;
		if((configurationText.getText().length()==0) || (methodText.getText().length()==0)){
			MessageDialog.openError(getShell(), "EPF Source Files",
					"You must select the EPF source files.");
			return;
		}

		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		dialog.open();
		IProgressMonitor monitor = dialog.getProgressMonitor();
		monitor.beginTask("Import EPF Information", 5);
		
		monitor.worked(1);

		try {
			runImportFromEPF(monitor, resourceURI);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	
		monitor.worked(5);		
		monitor.done();
		dialog.close();

		if(allOK){
			MessageDialog.openInformation(getShell(), "Import success", "Selected EPF files imported");
			getButton(IDialogConstants.OK_ID).setEnabled(false); 
		}
	}

					
	@Override
	public boolean close() {		
		//Refresh the Repository explorer view
		DawnExplorer repoView =null;
		IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if ("org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer".equals(viewReferences[i].getId())) {
				repoView = (DawnExplorer)viewReferences[i].getView(false);
				repoView.refreshViewer(false);
				break;
			}
		}
		if (!transaction.isClosed()) transaction.close();
		return super.close();
	}		
	

	private void runImportFromEPF(			
			final IProgressMonitor monitor, final URI projectModel) throws ExecutionException {

		CDOResource assuranceprojectResource=null;

		String path = projectModel.path();
		path=path.substring(0, path.lastIndexOf("/"));
		assuranceprojectFolder= transaction.getResourceFolder(path);
			
		String eviFolderPath = assuranceprojectFolder.getPath().substring(0, assuranceprojectFolder.getPath().lastIndexOf("/"));
		eviFolderPath=eviFolderPath + "/" + EVIDENCE_Folder;
		evidenceFolder= transaction.getResourceFolder(eviFolderPath);

		String procFolderPath = assuranceprojectFolder.getPath().substring(0, assuranceprojectFolder.getPath().lastIndexOf("/"));
		procFolderPath=procFolderPath + "/" + PROCESSES_Folder;
		procFolder= transaction.getResourceFolder(procFolderPath);

		URI evidenceModelURI=null;
		URI processModelURI=null;
		CDOResource evidenceModelResource=null;
		CDOResource processModelResource=null;
		evidenceModelResource  = transaction.getOrCreateResource(evidenceFolder.getPath() + "/" + eviNameText.getText() + EVIDENCE_Extension);    		
		evidenceModelURI = evidenceModelResource.getURI();
		processModelResource  = transaction.getOrCreateResource(procFolder.getPath() + "/" + procNameText.getText() + PROCESS_Extension);    		
		processModelURI = processModelResource.getURI();
		try {

			synchronized (transaction)
			{
				//CDOTransformHandler transfo = new CDOTransformHandler(assuranceprojectFolder, sourceModelURI, targetModelURI);
				CDOTransformHandler transfo = new CDOTransformHandler(assuranceprojectFolder, methodText.getText(), configurationText.getText(), evidenceModelResource, processModelResource, transaction);
				transfo.execute();
				transaction.commit();
				String evidenceSubPath=evidenceModelURI.path().substring(evidenceModelURI.path().indexOf("/")+1);
				String processSubPath=processModelURI.path().substring(processModelURI.path().indexOf("/")+1);
				evidenceSubPath=evidenceSubPath.substring(evidenceSubPath.indexOf("/"));
				processSubPath=processSubPath.substring(processSubPath.indexOf("/"));
				importedEviModelPath.setText(evidenceSubPath);
				importedProcModelPath.setText(processSubPath);
				monitor.worked(4);
			}
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			String msgError="Issues at importing the model:";
			importedEviModelPath.setText(msgError + e.getMessage());
			importedProcModelPath.setText(msgError + e.getMessage());			
			monitor.done();
		}

		try {
			
			assuranceprojectResource  = transaction.getResource(projectModel.path());
			EList<EObject> contents = assuranceprojectResource.getContents();

			EObject rootObject =contents.get(0);
			AssuranceProject assurproj = (AssuranceProject)rootObject;

			AssetsPackage assPackageconfig = null;
			for (AssetsPackage assetPackage: assurproj.getAssetsPackage()) {
				if(assetPackage.isIsActive()){
					assPackageconfig=assetPackage;
				}
			}		

			CDOResource eviResource = transaction.getResource(evidenceModelURI.path());
			EList<EObject> eviContents = eviResource.getContents();
			EObject eviRootObject =eviContents.get(0);
			ArtefactModel eviArtefactModel = (ArtefactModel)eviRootObject;
			assPackageconfig.getArtefactsModel().add(eviArtefactModel);
			
			CDOResource procResource = transaction.getResource(processModelURI.path());
			EList<EObject> procContents = procResource.getContents();
			EObject procRootObject =procContents.get(0);
			ProcessModel procProcessModel = (ProcessModel)procRootObject;
			assPackageconfig.getProcessModel().add(procProcessModel);

			// Save the contents of the resource to the file system.
			assuranceprojectResource.save(options);
			transaction.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MessageDialog.openWarning(getShell(), "Models not Linked", "The generated models cannot be linked to the Assurance Project");
			transaction.rollback();
			monitor.done();
		}	
	}
	
}

