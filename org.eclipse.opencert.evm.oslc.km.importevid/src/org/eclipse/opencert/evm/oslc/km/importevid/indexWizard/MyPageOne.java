/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.evm.oslc.km.importevid.indexWizard;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.eclipse.core.internal.resources.Resource;
import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.common.id.CDOIDUtil;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.oslc.km.importevid.wizard.ProjectSelector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.reusecompany.oslc.km.vocabs.srl.Artifact;

public class MyPageOne extends WizardPage {
	
    private Text fileSelector;
    private Composite container;

    public MyPageOne() {
        super("OSLC KM - Index Page");
        setTitle("Select the Model file to import");
        setDescription("The Model file will be indexed into the OSLC-KM repository");
        
        /* C�digo de prueba para desarrollo
        // Getting current CDO Session
        CDOSession cdoSession = CDOConnectionUtil.instance.getCurrentSession();
        //cdoSession.getViews(); => Recupera las vistas activas
        
        CDOView [] views = sessionCDO.getViews();
        if (views.length!=0) {
         viewCDO = views[0];    
        }
        
        // Getting CDO View
    	CDOView viewCDO = CDOConnectionUtil.instance.openView(cdoSession);
        
    	// Getting CDO Elements
    	CDOResourceNode[] listR = viewCDO.getElements();
    	int count = listR.length;
    	
    	// Prueba Iteraci�n
    	List<String> nodeNames = new ArrayList<String>();
    	List<String> nodePaths = new ArrayList<String>();
    	String nodeName;
    	String nodePath;
    	for (int i = 0; i < listR.length; i++) {
    		nodeName = listR[i].getName();
    		nodePath = listR[i].getPath();    
    		nodePaths.add(nodePath);
    		nodeNames.add(nodeName);
    		//System.out.println(nodePath);    				
		}	
    	
    	// 11 oct 2018: prueba recuperaci�n
    	String retrievalResult = "";
    	if (false) {
    		
    		// CASO 1 - Recuperaci�n de elementos para una query
    		try {
				retrievalResult = getSimilarRequirementsInOslcKmRepository("SL-C 1");
			} catch (ConnectException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		System.out.println("Retrieval Results: " + retrievalResult);
    		
    	} else {
    		    		    	
    		// CASO 2 - Indexaci�n de un Assurance Project
        	CDOResource myResource = viewCDO.getResource("/CS1-RTU-Security-P1/ASSURANCE_PROJECT/CS1-RTU-Security-P1.assuranceproject");
        	indexElementsOfCDOResourceAsAssuranceProject_FocusOnEvidenceReuse(myResource);
        	        	
    	} 	
    	
        // Opening a transaction
        CDOTransaction transaction = cdoSession.openTransaction();
        ProjectSelector listAssuranceProject = new ProjectSelector(getShell(), cdoSession);
        if (listAssuranceProject.open() == Window.OK) {
        	
        	// Buscar la referencia al proyecto
        	String projectUri = listAssuranceProject.getResult().get(1);
        	CDOResource resource = transaction.getResource(projectUri);
        	if (resource != null) {
        	
        		// Look for Requirement Items inside of the Assurance Project
        		//resource.getAllContents();
        		EList<EObject> contents = resource.getContents();
        		int contentsCount = contents.size();
        		EObject item = null;
        		String itemName =  null;
        		for (int i = 0; i < contentsCount; i++) {
        			item = contents.get(i);
        			itemName = item.toString();
        		}
        		
        		
        		int prueba = 888;
        		
        	} else {
        		
        		// Project not found
        	}        	
        }
        */
    }
    

 // Web request constants
 	//   Web request header properties and values:
    private static final String CRSeparator = "###";
    public static final String StringEmpty = "";
 	private static final String HttpPostProtocolName = "POST";
 	private static final String ContentTypeRequestProperty = "Content-Type";
 	private static final String JsonContentTypeRequestPropertyValue = "application/json";
 	private static final String ContentLengthRequestProperty = "Content-Length";
 	private static final String AsciiFormat = "ASCII";
 	private static final String Utf8Format = "UTF-8";
 	//   Parameters:
 	private static final String ReqPathParam = "path";
 	private static final String ReqNameParam = "name";
 	private static final String ReqIdParam = "cdoId";
 	private static final String ReqTextParam = "text";
 	private static final String CRsParam = "sil";
 	//   Json parts:
 	private static final String JsonStartWebRequest = "{\"";
 	private static final String JsonEndWebRequest = "\"}";
 	private static final String JsonAttributeValueSeparatorWebRequest = "\":\"";
 	private static final String JsonElementSeparatorWebRequest = "\",\"";
 	//   Answer headers / footers:
 	private static final String XMLResponseHeader = "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">";
 	private static final String XMLResponseFooter = "</string>";
 	//   Debug messages:
 	private static final String HttpWebRequestErrorMessage = "Failed : HTTP error code : ";
	public static boolean indexRequirementInOslcKmRepository(String path, String name, String cdoId, String text, List<String> criticalityLevels) throws java.net.ConnectException {
		boolean result = false;
		HttpURLConnection conn;
		conn = null; 
		DataInputStream doi;
		doi = null;
		InputStream inputStream;
		inputStream = null;
		Scanner s ;
		s = null;
		try {
			if (cdoId != null && !cdoId.equals(StringEmpty)) {
				
				String criticalityLevelsAsString = "";
				if (criticalityLevels != null && criticalityLevels.size() > 0) {
					criticalityLevelsAsString = String.join(CRSeparator, criticalityLevels);
				}
				
				// Building POST parameters
				StringBuilder postData = new StringBuilder();
				postData.append(JsonStartWebRequest);
				postData.append(ReqPathParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(path);
				postData.append(JsonElementSeparatorWebRequest);
				postData.append(ReqNameParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(name);
				postData.append(JsonElementSeparatorWebRequest);
				postData.append(ReqIdParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(cdoId);
				postData.append(JsonElementSeparatorWebRequest);
				postData.append(ReqTextParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(text);
				postData.append(JsonElementSeparatorWebRequest);
				postData.append(CRsParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(criticalityLevelsAsString);
				postData.append(JsonEndWebRequest);
				
		        byte[] postDataBytes = postData.toString().getBytes(AsciiFormat);
	
				URL url = new URL(MyWizard.TrcAmassWebServiceUrl);
				
		        conn = (HttpURLConnection)url.openConnection();
		        conn.setRequestMethod(HttpPostProtocolName);
		        conn.setRequestProperty(ContentTypeRequestProperty, JsonContentTypeRequestPropertyValue);
		        conn.setRequestProperty(ContentLengthRequestProperty, String.valueOf(postDataBytes.length));
		        conn.setDoOutput(true);
		        conn.getOutputStream().write(postDataBytes);
	
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					System.out.println(HttpWebRequestErrorMessage + conn.getResponseCode());
					
				} else {
					inputStream = conn.getInputStream();
					doi = new DataInputStream(inputStream);
					result = doi.readBoolean();
					
					//s = new Scanner(inputStream);
					//s.useDelimiter(Delimiter);
					//result = s.hasNextBoolean() ? s.nextBoolean() : false;					
				}
			}
		} catch (java.net.ConnectException e) {
			String errorMessage;
			errorMessage = e.getMessage() + " @ " + MyWizard.TrcAmassWebServiceUrl;
			System.out.println(errorMessage);
			throw e;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (s != null) {
				s.close();
				s = null;
			}
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch(Exception ex1) {
					
				}
				inputStream = null;
			}
			if (conn != null) {
				conn.disconnect();
				conn = null;
			} 
		}
		return result;
	}
	
	public static List<String> GetObjectsFromJson(String json) {
		final Gson gson;
		gson = new Gson();
		List<String> result = null;
		Reader inputString = null;
		BufferedReader inFromUser = null;
		try {
			inputString = new StringReader(json);
			inFromUser = new BufferedReader(inputString);
			result = gson.fromJson(inFromUser, List.class);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			throw ex;
		} finally {
			if (inputString != null) {
				try {
					inputString.close();
					inputString = null;
				} catch (Exception ex) {
				}
			}
			if (inFromUser != null) {
				try {
					inFromUser.close();
					inFromUser = null;
				} catch (Exception ex) {
				}
			}
		}
		return result;
	}
	
	public static String getSimilarRequirementsInOslcKmRepository(String text)  throws java.net.ConnectException  {
		String result = "";
		HttpURLConnection conn;
		conn = null; 
		DataInputStream doi;
		doi = null;
		InputStream inputStream;
		inputStream = null;
		Scanner s ;
		s = null;
		try {
			if (text != null && !text.equals(StringEmpty)) {
				
				// Building POST parameters
				StringBuilder postData = new StringBuilder();
				postData.append(JsonStartWebRequest);
				postData.append(ReqTextParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(text);				
				postData.append(JsonEndWebRequest);
				
		        byte[] postDataBytes = postData.toString().getBytes(AsciiFormat);
	
				URL url = new URL(MyWizard.TrcAmassWebServiceUrl_Retrieval);
				
		        conn = (HttpURLConnection)url.openConnection();
		        conn.setRequestMethod(HttpPostProtocolName);
		        conn.setRequestProperty(ContentTypeRequestProperty, JsonContentTypeRequestPropertyValue);
		        conn.setRequestProperty(ContentLengthRequestProperty, String.valueOf(postDataBytes.length));
		        conn.setDoOutput(true);
		        conn.getOutputStream().write(postDataBytes);
	
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					System.out.println(HttpWebRequestErrorMessage + conn.getResponseCode());
					
				} else {
					
					inputStream = conn.getInputStream();
					doi = new DataInputStream(inputStream);
					result = doi.readLine();
					
					if (result != null && !result.equals(StringEmpty)) {
						result = result.replace(XMLResponseHeader, StringEmpty);
						result = result.replace(XMLResponseFooter, StringEmpty);
					}
					
					//s = new Scanner(inputStream);
					//s.useDelimiter(Delimiter);
					//result = s.hasNextBoolean() ? s.nextBoolean() : false;					
				}
			}
		} catch (java.net.ConnectException e) {
			String errorMessage;
			errorMessage = e.getMessage() + " @ " + MyWizard.TrcAmassWebServiceUrl;
			System.out.println(errorMessage);
			throw e;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (s != null) {
				s.close();
				s = null;
			}
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch(Exception ex1) {
					
				}
				inputStream = null;
			}
			if (conn != null) {
				conn.disconnect();
				conn = null;
			} 
		}
		return result;
	}

    @Override
    public void createControl(Composite parent) {
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        container = new Composite(parent, SWT.NONE);
        container.setLayout(layout);
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("Model file:");

        fileSelector = new Text(container, SWT.BORDER | SWT.SINGLE);
        fileSelector.setText("");
        fileSelector.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (!fileSelector.getText().isEmpty()) {
                    setPageComplete(true);
                }
            }

        });
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        fileSelector.setLayoutData(gd);
        /*
        Label labelCheck = new Label(container, SWT.NONE);
        labelCheck.setText("Select a Papyrus file:");
        FileDialog check = new FileDialog(getShell());
        */
        // required to avoid an error in the system
        setControl(container);
        setPageComplete(false);

    }

    public String getFileFullPath() {
        return fileSelector.getText();
    }
    
    
    // REFACTORING FOR FINAL SOLUTION
    public static void indexElementsOfCDOResourceAsAssuranceProject(CDOResource cdoResource) {
    	
    	//CDOResource myResource = viewCDO.getResource("/CS1-RTU-Security-P1/ASSURANCE_PROJECT/CS1-RTU-Security-P1.assuranceproject");
    	if (cdoResource != null) {
    		
    		// Getting AssuranceProject
    		EList<EObject> contents = cdoResource.getContents();
    		if (contents != null && contents.size() > 0 && contents.get(0) instanceof AssuranceProject) {
    			
    			// From project: get baseline (from the Baseline Config ...Config) => Navigate through requirements
    			
    			// First element (TODO Iterate)
        		AssuranceProject myAssuranceProject = (AssuranceProject) contents.get(0);
        		EList<BaselineConfig> baselineConfigs = myAssuranceProject.getBaselineConfig();
        		if (baselineConfigs != null && baselineConfigs.size() > 0) {
        			
        			// First element (TODO iterate)
        			BaselineConfig baselineConfig = baselineConfigs.get(0);
        			if (baselineConfig != null) {
        				
        				// From the BaselineConfiguration, get the RefFramework
        				EList<BaseFramework> refFrameworks = baselineConfig.getRefFramework();
        				if (refFrameworks != null && refFrameworks.size() > 0) {
        				
        					// First element (TODO iterate)
        					BaseFramework refFramework = refFrameworks.get(0);
        					if (refFramework != null) {
        						
        						// Getting BaseActivities
        						EList<BaseActivity> baseActivities = refFramework.getOwnedActivities();
        						if (baseActivities != null && baseActivities.size() > 0) {
        							
        							// First element (TODO iterate)
        							BaseActivity baseActivity = baseActivities.get(0);
        							
        							// Parche Dev: primer subactivity
        							baseActivity = baseActivities.get(0).getSubActivity().get(0);
        							
        							if (baseActivity != null) {
        								
        								// Getting Requirements
        								EList<BaseRequirement> requirements = baseActivity.getOwnedRequirement();
        								if (requirements != null && requirements.size() > 0) {
        									
        									// Iterating requirements
        									BaseRequirement requirement = null;
        									for (int i = 0; i < requirements.size(); i++) {
        										requirement = requirements.get(i);
        										if (requirement != null) {
            										
            										// Debug (TODO)
            										System.out.println("");
            										System.out.println("" + String.valueOf(i + 1) + "- Requirement Id: " + requirement.getId());
            										System.out.println("	Name: " + requirement.getName());
            										System.out.println("	Description: " + requirement.getDescription());
            										
            										// Getting Requirement Applicability
            										List<String> silElements = new ArrayList<String>();
            										EList<BaseApplicability> applicabilities = requirement.getApplicability();
            										if (applicabilities != null && applicabilities.size() > 0) {
            											
            											// Iterating Requirement Applicabilities
            											BaseApplicability applicability = null;
            											for (int j = 0; j < applicabilities.size(); j++) {
            												applicability = applicabilities.get(j);	
            												if (applicability != null) {
                												
                												// Getting Criticality Applicabilities
                												EList<BaseCriticalityApplicability> applicabilityLevels = applicability.getApplicCritic();
                												if (applicabilityLevels != null && applicabilityLevels.size() > 0) {
                													
                													// Iterating criticality applicabilities
                													BaseCriticalityApplicability applicabilityElement = null;
                													for (int k = 0; k < applicabilityLevels.size(); k++) {
                														applicabilityElement = applicabilityLevels.get(k);
                    													if (applicabilityElement != null) {
                    													
                    														// Getting Criticality Level
                    														BaseCriticalityLevel criticalityLevel = applicabilityElement.getCriticLevel();
                    														if (criticalityLevel != null) {
                    															System.out.println("	CriticalityLevel: " + criticalityLevel.getName());
                    															silElements.add(criticalityLevel.getName());
                    														}
                    													}    	
                													}                																									
                												}
                											}
            											}  
            											
            											// Index OSLC-KM
                										boolean indexed = false;
                										try {
    														indexed = indexRequirementInOslcKmRepository("", requirement.getId(), requirement.getId(), requirement.getDescription(), silElements);
    													} catch (ConnectException e) {
    														// TODO Auto-generated catch block
    														e.printStackTrace();
    													}
                										System.out.println("	Indexed: " + indexed);
                										
            										}
            									}
        									}        									
        								}
        							}
        						}
        					}
        				}
        			}
        		}
    		}    		    
    	}
    }

    private static List<String> GetSilLevelsFromRequirements(EList<BaseRequirement> requirements) {
    	
    	List<String> silElements = new ArrayList<String>();
    	if (requirements != null && requirements.size() > 0) {
			
			// Iterating requirements (to get SIL-Levels)
			BaseRequirement requirement = null;
			for (int i = 0; i < requirements.size(); i++) {
				requirement = requirements.get(i);
				if (requirement != null) {
					
					// Getting Requirement Applicability
					EList<BaseApplicability> applicabilities = requirement.getApplicability();
					if (applicabilities != null && applicabilities.size() > 0) {
						
						// Iterating Requirement Applicabilities
						BaseApplicability applicability = null;
						for (int j = 0; j < applicabilities.size(); j++) {
							applicability = applicabilities.get(j);	
							if (applicability != null) {
								
								// Getting Criticality Applicabilities
								EList<BaseCriticalityApplicability> applicabilityLevels = applicability.getApplicCritic();
								if (applicabilityLevels != null && applicabilityLevels.size() > 0) {
									
									// Iterating criticality applicabilities
									BaseCriticalityApplicability applicabilityElement = null;
									for (int k = 0; k < applicabilityLevels.size(); k++) {
										applicabilityElement = applicabilityLevels.get(k);
										if (applicabilityElement != null) {
										
											// Getting Criticality Level
											BaseCriticalityLevel criticalityLevel = applicabilityElement.getCriticLevel();
											if (criticalityLevel != null) {
												silElements.add(criticalityLevel.getName());
											}
										}    	
									}                																									
								}
							}
						}
					}
				}
			}        									
		}
    	return silElements;
    }
    
    public static void indexElementsOfCDOResourceAsAssuranceProject_FocusOnEvidenceReuse(CDOResource cdoResource) {
    	
    	//CDOResource myResource = viewCDO.getResource("/CS1-RTU-Security-P1/ASSURANCE_PROJECT/CS1-RTU-Security-P1.assuranceproject");
    	if (cdoResource != null) {
    		
    		// Getting AssuranceProject
    		EList<EObject> contents = cdoResource.getContents();
    		if (contents != null && contents.size() > 0 && contents.get(0) instanceof AssuranceProject) {
    			
    			// First element (TODO Iterate)
        		AssuranceProject myAssuranceProject = (AssuranceProject) contents.get(0);
        		EList<BaselineConfig> baselineConfigs = myAssuranceProject.getBaselineConfig();
        		if (baselineConfigs != null && baselineConfigs.size() > 0) {
        			
        			// Final result => Dictionary of <Artefact, List<SilLevels>> 
        			Map<Artefact, List<String>> artefactsMapping = new HashMap<Artefact, List<String>>();
        			
        			// Iterating baselineConfigs
        			BaselineConfig baselineConfig = null;
        			for (int baselineConfigIndex = 0; baselineConfigIndex < baselineConfigs.size(); baselineConfigIndex++) {
        			
        				baselineConfig = baselineConfigs.get(baselineConfigIndex);
            			if (baselineConfig != null) {
            				
            				// From the BaselineConfiguration, get the RefFramework
            				EList<BaseFramework> refFrameworks = baselineConfig.getRefFramework();
            				if (refFrameworks != null && refFrameworks.size() > 0) {
            				
            					// Iterate RefFrameworks
            					BaseFramework refFramework = null;
                    			for (int refFrameworkIndex = 0; refFrameworkIndex < refFrameworks.size(); refFrameworkIndex++) {

                    				refFramework = refFrameworks.get(refFrameworkIndex);
                					if (refFramework != null) {
                						
                						// New in this function => Getting Base Artefacts
                						EList<BaseArtefact> baseArtefacts = refFramework.getOwnedArtefact();
                						if (baseArtefacts != null && baseArtefacts.size() > 0) {
                							
                							// Iterate BaseArtefacts
                							BaseArtefact baseArtefact = null;
                							for (int baseArtefactIndex = 0; baseArtefactIndex < baseArtefacts.size(); baseArtefactIndex++) {
                								
                								baseArtefact = baseArtefacts.get(baseArtefactIndex);
                								if (baseArtefact != null) {
                									
                									// Del BaseArtefact, por un lado saco los SilLevels de sus Constraining Requirements (from the BaseArtefact)
                									List<String> silLevels = null;
    												EList<BaseRequirement> constrainingRequirements = baseArtefact.getConstrainingRequirement();
    												if (constrainingRequirements != null && constrainingRequirements.size() > 0) {
                    									
                    									// Iterating requirements (to get SIL-Levels)
    													silLevels = GetSilLevelsFromRequirements(constrainingRequirements);        													                                									      							
                    								}
                									
                									// Y por otro lado, navego a trav�s de los BaseComplianceMaps para obtener los Artefacts a indexar
                									EList<BaseComplianceMap> complianceMaps = baseArtefact.getComplianceMap();
                									if (complianceMaps != null && complianceMaps.size() > 0) {
                										
                										// Iterate ComplianceMaps
                										BaseComplianceMap complianceMap = null;
                										for (int complianceMapIndex = 0; complianceMapIndex < complianceMaps.size(); complianceMapIndex++) {
                											
                											complianceMap = complianceMaps.get(complianceMapIndex);
                											if (complianceMap != null) {
                												
                												// Getting Target => Es lo que tengo que indexar (Deben ser Artefacts)
                												EList<AssuranceAsset> targets = complianceMap.getTarget();
                												if (targets != null && targets.size() > 0) {
                													
                													Artefact artefact = null;
                													AssuranceAsset target = null;
                													for (int targetIndex = 0; targetIndex < targets.size(); targetIndex++) {
                														target = targets.get(targetIndex);
                														if (target instanceof Artefact) {
                															artefact = (Artefact)target;
                															if (artefact != null) {
                																
                																// Artefact para indexar
                																String test1 = artefact.getName();
                																String test2 = artefact.getDescription();
                																
                																// Append to the result
                																List<String> silsForThisArtefact = null;
                																if (artefactsMapping.containsKey(artefact)) {
                																	silsForThisArtefact = artefactsMapping.get(artefact);
                																} else {
                																	// Not in the dictionary
                																	silsForThisArtefact = new ArrayList<String>();
                																	artefactsMapping.put(artefact, silsForThisArtefact);
                																}
                																
                																// Complementar con los SilLevels
                																if (silsForThisArtefact != null && silLevels != null && silLevels.size() > 0) {
                																	
                																	String localSil = null;
                																	for (int localSilIndex = 0; localSilIndex < silLevels.size(); localSilIndex++) {
                																		localSil = silLevels.get(localSilIndex);
                																		if (localSil != null) {
                																			if (!silsForThisArtefact.contains(localSil)) {
                																				silsForThisArtefact.add(localSil);
                																			}
                																		}                																		
                																	}
                																}
                															}
                														}
                													}
                												}                												                											
                											}
                										}
                									}
                								}
                							}
                						}
                					}                    				
                    			}            					            				
            				}
            			}        				
        			}    
        			
        			// Final summary
        			if (artefactsMapping != null && artefactsMapping.size() > 0) {
        				Artefact art;
        				List<String> silLevels;
        				Iterator it = artefactsMapping.entrySet().iterator();
        				while (it.hasNext()) {
        					Map.Entry pair = (Map.Entry)it.next();
        					art = (Artefact)pair.getKey();
        					silLevels = (List<String>)pair.getValue();
        					System.out.println("Artefact " + art.getName() + ", " + silLevels.size() + " sil levels");
        				}
        			}
        		}
    		}    		    
    	}
    }

    public static Map<Artefact, List<String>> collectSilLevelsByArtefactFromAssuranceProjects(List<AssuranceProject> assuranceProjects) {
    	
    	// Final result => Dictionary of <Artefact, List<SilLevels>> 
		Map<Artefact, List<String>> artefactsMapping = new HashMap<Artefact, List<String>>();    	
    	if (assuranceProjects != null && assuranceProjects.size() > 0) {
    		
    		AssuranceProject assuranceProject;
    		for (int assuranceProjectIndex = 0; assuranceProjectIndex < assuranceProjects.size(); assuranceProjectIndex++) {
    			assuranceProject = assuranceProjects.get(assuranceProjectIndex);    			
    			EList<BaselineConfig> baselineConfigs = assuranceProject.getBaselineConfig();
        		if (baselineConfigs != null && baselineConfigs.size() > 0) {
        			
        			// Iterating baselineConfigs
        			BaselineConfig baselineConfig = null;
        			for (int baselineConfigIndex = 0; baselineConfigIndex < baselineConfigs.size(); baselineConfigIndex++) {
        			
        				baselineConfig = baselineConfigs.get(baselineConfigIndex);
            			if (baselineConfig != null) {
            				
            				// From the BaselineConfiguration, get the RefFramework
            				EList<BaseFramework> refFrameworks = baselineConfig.getRefFramework();
            				if (refFrameworks != null && refFrameworks.size() > 0) {
            				
            					// Iterate RefFrameworks
            					BaseFramework refFramework = null;
                    			for (int refFrameworkIndex = 0; refFrameworkIndex < refFrameworks.size(); refFrameworkIndex++) {

                    				refFramework = refFrameworks.get(refFrameworkIndex);
                					if (refFramework != null) {
                						
                						// New in this function => Getting Base Artefacts
                						EList<BaseArtefact> baseArtefacts = refFramework.getOwnedArtefact();
                						if (baseArtefacts != null && baseArtefacts.size() > 0) {
                							
                							// Iterate BaseArtefacts
                							BaseArtefact baseArtefact = null;
                							for (int baseArtefactIndex = 0; baseArtefactIndex < baseArtefacts.size(); baseArtefactIndex++) {
                								
                								baseArtefact = baseArtefacts.get(baseArtefactIndex);
                								if (baseArtefact != null) {
                									
                									// Del BaseArtefact, por un lado saco los SilLevels de sus Constraining Requirements (from the BaseArtefact)
                									List<String> silLevels = null;
    												EList<BaseRequirement> constrainingRequirements = baseArtefact.getConstrainingRequirement();
    												if (constrainingRequirements != null && constrainingRequirements.size() > 0) {
                    									
                    									// Iterating requirements (to get SIL-Levels)
    													silLevels = GetSilLevelsFromRequirements(constrainingRequirements);        													                                									      							
                    								}
                									
                									// Y por otro lado, navego a trav�s de los BaseComplianceMaps para obtener los Artefacts a indexar
                									EList<BaseComplianceMap> complianceMaps = baseArtefact.getComplianceMap();
                									if (complianceMaps != null && complianceMaps.size() > 0) {
                										
                										// Iterate ComplianceMaps
                										BaseComplianceMap complianceMap = null;
                										for (int complianceMapIndex = 0; complianceMapIndex < complianceMaps.size(); complianceMapIndex++) {
                											
                											complianceMap = complianceMaps.get(complianceMapIndex);
                											if (complianceMap != null) {
                												
                												// Getting Target => Es lo que tengo que indexar (Deben ser Artefacts)
                												EList<AssuranceAsset> targets = complianceMap.getTarget();
                												if (targets != null && targets.size() > 0) {
                													
                													Artefact artefact = null;
                													AssuranceAsset target = null;
                													for (int targetIndex = 0; targetIndex < targets.size(); targetIndex++) {
                														target = targets.get(targetIndex);
                														if (target instanceof Artefact) {
                															artefact = (Artefact)target;
                															if (artefact != null) {
                																
                																// Artefact para indexar
                																String test1 = artefact.getName();
                																String test2 = artefact.getDescription();
                																
                																// Append to the result
                																List<String> silsForThisArtefact = null;
                																if (artefactsMapping.containsKey(artefact)) {
                																	silsForThisArtefact = artefactsMapping.get(artefact);
                																} else {
                																	// Not in the dictionary
                																	silsForThisArtefact = new ArrayList<String>();
                																	artefactsMapping.put(artefact, silsForThisArtefact);
                																}
                																
                																// Complementar con los SilLevels
                																if (silsForThisArtefact != null && silLevels != null && silLevels.size() > 0) {
                																	
                																	String localSil = null;
                																	for (int localSilIndex = 0; localSilIndex < silLevels.size(); localSilIndex++) {
                																		localSil = silLevels.get(localSilIndex);
                																		if (localSil != null) {
                																			if (!silsForThisArtefact.contains(localSil)) {
                																				silsForThisArtefact.add(localSil);
                																			}
                																		}                																		
                																	}
                																}
                															}
                														}
                													}
                												}                												                											
                											}
                										}
                									}
                								}
                							}
                						}
                					}                    				
                    			}            					            				
            				}
            			}        				
        			}            			        		
        		}
    		}   		    
    	}
    	return artefactsMapping;
    }

    public static void indexArtefacts_FocusOnEvidenceReuse(CDOView cdoView, List<Artefact> artefactsToIndex, Map<Artefact, List<String>> artefacts2Sil) {
    	
    	if (artefactsToIndex != null && artefactsToIndex.size() > 0) {
    		
    		String artefactIdentifier;
    		String textToIndex;
    		Artefact artefact;
    		List<String> silElementsForThisArtefact;
    		for (int i = 0; i < artefactsToIndex.size(); i++) {
    			textToIndex = null;
    			silElementsForThisArtefact = null;
    			artefact = artefactsToIndex.get(i);
    			if (artefact != null) {
    				
    				//artefactIdentifier = artefact.cdoID().toString();
    				artefactIdentifier = "L" + artefact.cdoID().toURIFragment(); // Esto guarda s�lo el ID num�rico, sin la L
    				
    				textToIndex = artefact.getName() + ". " + artefact.getDescription();
    				if (artefacts2Sil != null && artefacts2Sil.containsKey(artefact)) {
    					silElementsForThisArtefact = artefacts2Sil.get(artefact);
    				}
    				
    				// Index OSLC-KM
					boolean indexed = false;
					try {				
						System.out.println("Indexing Artefact " + artefact.getName());
						indexed = indexRequirementInOslcKmRepository("", artefact.getName(), artefactIdentifier, textToIndex, silElementsForThisArtefact);
					} catch (ConnectException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("Result: " + indexed);
					
					// Furthermore, we must index the "AssuranceAssetEvent" objects (not clear in fact so it remains commented)
					/*
					EList<AssuranceAssetEvent> events = artefact.getLifecycleEvent();
					if (events != null && events.size() > 0) {
						
						AssuranceAssetEvent event;
						String eventTextToIndex;
						String eventIdentifier;
						for (int k = 0; k < events.size(); k++) {
							event = events.get(i);
							if (event != null) {
								eventIdentifier = "L" + event.cdoID().toURIFragment();
								eventTextToIndex = event.getName() + ". " + event.getDescription();
								
								// Index OSLC-KM
								indexed = false;
								try {				
									System.out.println("   Indexing AssuranceAssetEvent " + event.getName());
									indexed = indexRequirementInOslcKmRepository("", event.getName(), eventIdentifier, eventTextToIndex, null);
								} catch (ConnectException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								System.out.println("   Result: " + indexed);
							}
						}
					}
					*/
					
					// Furthermore, we must index the "Resource" objects
					EList<org.eclipse.opencert.evm.evidspec.evidence.Resource> artefactResources = artefact.getResource();
					if (artefactResources != null && artefactResources.size() > 0) {
						
						String resTextToIndex;
						String resIdentifier;
						org.eclipse.opencert.evm.evidspec.evidence.Resource res;						
						for (int j = 0; j < artefactResources.size(); j++) {
							res = artefactResources.get(j);
							if (res != null) {
								
								resIdentifier = "L" + res.cdoID().toURIFragment(); // Esto guarda s�lo el ID num�rico, sin la L
								resTextToIndex = res.getName() + ". " + res.getDescription();
								
								// Index OSLC-KM with SIL-LEVELS
								indexed = false;
								try {				
									System.out.println("   Indexing Resource " + res.getName());
									indexed = indexRequirementInOslcKmRepository("", res.getName(), resIdentifier, resTextToIndex, silElementsForThisArtefact);
								} catch (ConnectException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								System.out.println("   Result: " + indexed);
							}							
						}
					}
    			}
    		} 		    
    	}
    }


}