/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface ArtefactEventsPropertiesEditionPart {



	/**
	 * Init the lifecycleEvent
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initLifecycleEvent(ReferencesTableSettings settings);

	/**
	 * Update the lifecycleEvent
	 * @param newValue the lifecycleEvent to update
	 * 
	 */
	public void updateLifecycleEvent();

	/**
	 * Adds the given filter to the lifecycleEvent edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToLifecycleEvent(ViewerFilter filter);

	/**
	 * Adds the given filter to the lifecycleEvent edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEvent(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the lifecycleEvent table
	 * 
	 */
	public boolean isContainedInLifecycleEventTable(EObject element);




	/**
	 * Init the lifecycleEventTable
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initLifecycleEventTable(ReferencesTableSettings settings);

	/**
	 * Update the lifecycleEventTable
	 * @param newValue the lifecycleEventTable to update
	 * 
	 */
	public void updateLifecycleEventTable();

	/**
	 * Adds the given filter to the lifecycleEventTable edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToLifecycleEventTable(ViewerFilter filter);

	/**
	 * Adds the given filter to the lifecycleEventTable edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEventTable(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the lifecycleEventTable table
	 * 
	 */
	public boolean isContainedInLifecycleEventTableTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
