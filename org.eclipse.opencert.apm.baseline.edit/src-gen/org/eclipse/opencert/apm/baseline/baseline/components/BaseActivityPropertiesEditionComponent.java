/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityApplicabilityPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityRequirementPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivitySelectionPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

// End of user code

/**
 * 
 * 
 */
public class BaseActivityPropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private BaseActivityPropertiesEditionPart basePart;

	/**
	 * The BaseActivityBasePropertiesEditionComponent sub component
	 * 
	 */
	protected BaseActivityBasePropertiesEditionComponent baseActivityBasePropertiesEditionComponent;

	/**
	 * The BaseActivitySelection part
	 * 
	 */
	private BaseActivitySelectionPropertiesEditionPart baseActivitySelectionPart;

	/**
	 * The BaseActivityBaseActivitySelectionPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseActivityBaseActivitySelectionPropertiesEditionComponent baseActivityBaseActivitySelectionPropertiesEditionComponent;

	/**
	 * The BaseActivityRequirement part
	 * 
	 */
	private BaseActivityRequirementPropertiesEditionPart baseActivityRequirementPart;

	/**
	 * The BaseActivityBaseActivityRequirementPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseActivityBaseActivityRequirementPropertiesEditionComponent baseActivityBaseActivityRequirementPropertiesEditionComponent;

	/**
	 * The BaseActivityApplicability part
	 * 
	 */
	private BaseActivityApplicabilityPropertiesEditionPart baseActivityApplicabilityPart;

	/**
	 * The BaseActivityBaseActivityApplicabilityPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseActivityBaseActivityApplicabilityPropertiesEditionComponent baseActivityBaseActivityApplicabilityPropertiesEditionComponent;

	/**
	 * The BaseActivityEquivalenceMap part
	 * 
	 */
	private BaseActivityEquivalenceMapPropertiesEditionPart baseActivityEquivalenceMapPart;

	/**
	 * The BaseActivityBaseActivityEquivalenceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseActivityBaseActivityEquivalenceMapPropertiesEditionComponent baseActivityBaseActivityEquivalenceMapPropertiesEditionComponent;

	/**
	 * The BaseActivityComplianceMap part
	 * 
	 */
	private BaseActivityComplianceMapPropertiesEditionPart baseActivityComplianceMapPart;

	/**
	 * The BaseActivityBaseActivityComplianceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseActivityBaseActivityComplianceMapPropertiesEditionComponent baseActivityBaseActivityComplianceMapPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param baseActivity the EObject to edit
	 * 
	 */
	public BaseActivityPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseActivity, String editing_mode) {
		super(editingContext, editing_mode);
		if (baseActivity instanceof BaseActivity) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseActivity, PropertiesEditingProvider.class);
			baseActivityBasePropertiesEditionComponent = (BaseActivityBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseActivityBasePropertiesEditionComponent.BASE_PART, BaseActivityBasePropertiesEditionComponent.class);
			addSubComponent(baseActivityBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseActivity, PropertiesEditingProvider.class);
			baseActivityBaseActivitySelectionPropertiesEditionComponent = (BaseActivityBaseActivitySelectionPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseActivityBaseActivitySelectionPropertiesEditionComponent.BASEACTIVITYSELECTION_PART, BaseActivityBaseActivitySelectionPropertiesEditionComponent.class);
			addSubComponent(baseActivityBaseActivitySelectionPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseActivity, PropertiesEditingProvider.class);
			baseActivityBaseActivityRequirementPropertiesEditionComponent = (BaseActivityBaseActivityRequirementPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseActivityBaseActivityRequirementPropertiesEditionComponent.BASEACTIVITYREQUIREMENT_PART, BaseActivityBaseActivityRequirementPropertiesEditionComponent.class);
			addSubComponent(baseActivityBaseActivityRequirementPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseActivity, PropertiesEditingProvider.class);
			baseActivityBaseActivityApplicabilityPropertiesEditionComponent = (BaseActivityBaseActivityApplicabilityPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseActivityBaseActivityApplicabilityPropertiesEditionComponent.BASEACTIVITYAPPLICABILITY_PART, BaseActivityBaseActivityApplicabilityPropertiesEditionComponent.class);
			addSubComponent(baseActivityBaseActivityApplicabilityPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseActivity, PropertiesEditingProvider.class);
			baseActivityBaseActivityEquivalenceMapPropertiesEditionComponent = (BaseActivityBaseActivityEquivalenceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseActivityBaseActivityEquivalenceMapPropertiesEditionComponent.BASEACTIVITYEQUIVALENCEMAP_PART, BaseActivityBaseActivityEquivalenceMapPropertiesEditionComponent.class);
			addSubComponent(baseActivityBaseActivityEquivalenceMapPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseActivity, PropertiesEditingProvider.class);
			baseActivityBaseActivityComplianceMapPropertiesEditionComponent = (BaseActivityBaseActivityComplianceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseActivityBaseActivityComplianceMapPropertiesEditionComponent.BASEACTIVITYCOMPLIANCEMAP_PART, BaseActivityBaseActivityComplianceMapPropertiesEditionComponent.class);
			addSubComponent(baseActivityBaseActivityComplianceMapPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (BaseActivityBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (BaseActivityPropertiesEditionPart)baseActivityBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (BaseActivityBaseActivitySelectionPropertiesEditionComponent.BASEACTIVITYSELECTION_PART.equals(key)) {
			baseActivitySelectionPart = (BaseActivitySelectionPropertiesEditionPart)baseActivityBaseActivitySelectionPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseActivitySelectionPart;
		}
		if (BaseActivityBaseActivityRequirementPropertiesEditionComponent.BASEACTIVITYREQUIREMENT_PART.equals(key)) {
			baseActivityRequirementPart = (BaseActivityRequirementPropertiesEditionPart)baseActivityBaseActivityRequirementPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseActivityRequirementPart;
		}
		if (BaseActivityBaseActivityApplicabilityPropertiesEditionComponent.BASEACTIVITYAPPLICABILITY_PART.equals(key)) {
			baseActivityApplicabilityPart = (BaseActivityApplicabilityPropertiesEditionPart)baseActivityBaseActivityApplicabilityPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseActivityApplicabilityPart;
		}
		if (BaseActivityBaseActivityEquivalenceMapPropertiesEditionComponent.BASEACTIVITYEQUIVALENCEMAP_PART.equals(key)) {
			baseActivityEquivalenceMapPart = (BaseActivityEquivalenceMapPropertiesEditionPart)baseActivityBaseActivityEquivalenceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseActivityEquivalenceMapPart;
		}
		if (BaseActivityBaseActivityComplianceMapPropertiesEditionComponent.BASEACTIVITYCOMPLIANCEMAP_PART.equals(key)) {
			baseActivityComplianceMapPart = (BaseActivityComplianceMapPropertiesEditionPart)baseActivityBaseActivityComplianceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseActivityComplianceMapPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (BaselineViewsRepository.BaseActivity.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (BaseActivityPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseActivitySelection.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseActivitySelectionPart = (BaseActivitySelectionPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseActivityRequirement.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseActivityRequirementPart = (BaseActivityRequirementPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseActivityApplicability.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseActivityApplicabilityPart = (BaseActivityApplicabilityPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseActivityEquivalenceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseActivityEquivalenceMapPart = (BaseActivityEquivalenceMapPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseActivityComplianceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseActivityComplianceMapPart = (BaseActivityComplianceMapPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == BaselineViewsRepository.BaseActivity.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseActivitySelection.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseActivityRequirement.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseActivityApplicability.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseActivityEquivalenceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseActivityComplianceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
