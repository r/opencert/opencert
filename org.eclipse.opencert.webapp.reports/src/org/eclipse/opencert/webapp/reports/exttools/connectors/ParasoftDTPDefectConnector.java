/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.exttools.connectors;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.opencert.webapp.reports.exttools.processors.util.CountProperty;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ParasoftDTPDefectConnector
        extends AbstractParasoftDTPConnector
{

    private static final String PROJECT_KEY = "Project";

    @Override
    public String getName()
    {
        return "Parasoft DTP Defects Connector";
    }

    @Override
    public String getDescription()
    {
        return "Connects to Parasoft DTP server and retrieves a number of defects reported for the specific DTP project. Note: it uses DTP REST API /defects";
    }

    @Override
    public List<String> getSettingKeys()
    {
        List<String> result = super.getSettingKeys();
        result.add(PROJECT_KEY);
        
        return result;
    }
    
    @Override
    public Map<String, String> getSettingDefaultValues()
    {
        Map <String, String> result = super.getSettingDefaultValues();
        result.put(PROJECT_KEY, "MW");
        
        return result;
    }
    
    
    @Override
    public String getUrlSuffix()
    {
        return "/grs/api/v1/defects?project=" + 
                getSettingsValue(PROJECT_KEY) +  
            "&limit=1000";    
    }

    @Override
    public int processHttpResponse(String httpResponse)
    {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CountProperty defectCount = null;

        try 
        {
            defectCount = mapper.readValue(httpResponse, CountProperty.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return defectCount != null ? defectCount.getCount() : -1;
    }
}
