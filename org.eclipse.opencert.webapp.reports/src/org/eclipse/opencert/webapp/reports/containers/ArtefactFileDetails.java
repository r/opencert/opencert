/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

import java.util.Date;

import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;

public class ArtefactFileDetails
{
    private String fileName = "";
    private String projectName = "";
    private String justification = "";
    private String baseElementName = "";
    private String artefactName = "";
    private String resourceName = "";
    private Date modificationDate;
    
    private AssuranceProject assuranceProject;
    private SvnProperties svnProperties;
    private BaseAssetComplianceStatus baseAssetComplianceStatus;
    private Object complianceValue;
    
    public ArtefactFileDetails() 
    {
        
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getProjectName()
    {
        return projectName;
    }

    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public SvnProperties getSvnProperties()
    {
        return svnProperties;
    }

    public void setSvnProperties(SvnProperties svnProperties)
    {
        this.svnProperties = svnProperties;
    }

    public BaseAssetComplianceStatus getBaseAssetComplianceStatus()
    {
        return baseAssetComplianceStatus;
    }

    public void setBaseAssetComplianceStatus(BaseAssetComplianceStatus baseAssetComplianceStatus)
    {
        this.baseAssetComplianceStatus = baseAssetComplianceStatus;
    }

    public String getJustification()
    {
        return justification;
    }

    public void setJustification(String justification)
    {
        this.justification = justification;
    }
    
    public void setModificationDate(Date modificationDate)
    {
    	this.modificationDate = modificationDate;
    }
    
    public Date getModificationDate()
    {
    	return modificationDate;
    }

    public Object getComplianceValue()
    {
        return complianceValue;
    }

    public void setComplianceValue(Object complianceValue)
    {
        this.complianceValue = complianceValue;
    }
    
    public String getBaseElementName()
    {
        return baseElementName;
    }

    public void setBaseElementName(String baseElementName)
    {
        this.baseElementName = baseElementName;
    }
    
    public String getArtefactName()
    {
        return artefactName;
    }

    public void setArtefactName(String artefactName)
    {
        this.artefactName = artefactName;
    }

    public String getResourceName()
    {
        return resourceName;
    }

    public void setResourceName(String resourceName)
    {
        this.resourceName = resourceName;
    }

    public AssuranceProject getAssuranceProject()
    {
        return assuranceProject;
    }

    public void setAssuranceProject(AssuranceProject assuranceProject)
    {
        this.assuranceProject = assuranceProject;
    }

    @Override
    public String toString()
    {
        return "ArtefactFileDetails [fileName=" + fileName + ", projectName="
                + projectName + ", justification=" + justification
                + ", baseElementName=" + baseElementName + ", artefactName="
                + artefactName + ", resourceName=" + resourceName
                + ", assuranceProject=" + assuranceProject + ", svnProperties="
                + svnProperties + ", baseAssetComplianceStatus="
                + baseAssetComplianceStatus + ", complianceValue="
                + complianceValue + "]";
    }    
}
