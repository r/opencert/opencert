/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

public class StringUtil
{
    public static String ensureNotNull(String s)
    {
        return (s != null ? s : "");
    }

    public static boolean equal(String s1, String s2)
    {
        return equal(s1, s2, false);
    }
    
    public static boolean equal(String s1, String s2, boolean forceNotNull)
    {
        if (forceNotNull) {
            s1 = ensureNotNull(s1);
            s2 = ensureNotNull(s2);            
        }
        
        if (s1 == null && s2 == null) {
            return true;
        }
        if (s1 != null) {
            return s1.equals(s2);
        }
        return false;
    }
}
