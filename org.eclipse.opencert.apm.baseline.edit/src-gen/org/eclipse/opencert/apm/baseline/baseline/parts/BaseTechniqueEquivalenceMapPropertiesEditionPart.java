/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface BaseTechniqueEquivalenceMapPropertiesEditionPart {



	/**
	 * Init the equivalenceMap
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initEquivalenceMap(ReferencesTableSettings settings);

	/**
	 * Update the equivalenceMap
	 * @param newValue the equivalenceMap to update
	 * 
	 */
	public void updateEquivalenceMap();

	/**
	 * Adds the given filter to the equivalenceMap edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToEquivalenceMap(ViewerFilter filter);

	/**
	 * Adds the given filter to the equivalenceMap edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToEquivalenceMap(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the equivalenceMap table
	 * 
	 */
	public boolean isContainedInEquivalenceMapTable(EObject element);




	/**
	 * Init the techniqueEquivalenceMap
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initTechniqueEquivalenceMap(ReferencesTableSettings settings);

	/**
	 * Update the techniqueEquivalenceMap
	 * @param newValue the techniqueEquivalenceMap to update
	 * 
	 */
	public void updateTechniqueEquivalenceMap();

	/**
	 * Adds the given filter to the techniqueEquivalenceMap edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToTechniqueEquivalenceMap(ViewerFilter filter);

	/**
	 * Adds the given filter to the techniqueEquivalenceMap edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToTechniqueEquivalenceMap(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the techniqueEquivalenceMap table
	 * 
	 */
	public boolean isContainedInTechniqueEquivalenceMapTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
