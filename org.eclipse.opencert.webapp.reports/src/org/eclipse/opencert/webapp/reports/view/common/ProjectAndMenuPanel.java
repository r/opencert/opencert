/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import org.eclipse.opencert.webapp.reports.listeners.ReportChangeNotifier;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

@SuppressWarnings("serial")
public class ProjectAndMenuPanel 
    extends CustomComponent
{	
	private static final String REPORTS_MENU_ITEM = "Reports";
	private static final String ARGUMENTATION_MENU_ITEM = "Argumentation";
	private static final String EVIDENCE_MENU_ITEM = "Evidence";
	private static final String PROCESS_MENU_ITEM = "Process";
	private static final String GAP_ANALYSIS_REPORT_MENU_ITEM = "Gap Analysis report";
	private static final String COMPLIANCE_REPORT_MENU_ITEM = "Compliance report";
	private static final String COMPLIANCE_ESTIMATION_REPORT_MENU_ITEM = "Compliance Estimation report";
	private static final String METRICS_ESTIMATION_REPORT_MENU_ITEM = "Metrics Estimation report";
	private static final String EQUIVALENCE_MAP_REPORT_MENU_ITEM = "Equivalence Map report";
	
	private final ProjectComboWrapper projectComboProvider;

	private ReportProvider reportProvider;
	private ReportChangeNotifier reportChangeNotifier = null;

	public ProjectAndMenuPanel(ProjectComboWrapper projectComboProvider)
	{
	    this.projectComboProvider = projectComboProvider;
	    
		setWidth("100%");
		setStyleName("projectAndMenuPanel");
		HorizontalLayout compositionRoot = new HorizontalLayout();
		setCompositionRoot(compositionRoot);
		compositionRoot.setWidth("100%");
		
        final Component projectsPanel = projectComboProvider.getProjectsPanel();
        final Component spacer = initSpacer();
        final MenuBar menuBar = initMenuBar();
        
        compositionRoot.addComponent(projectsPanel);
        compositionRoot.addComponent(spacer);
        compositionRoot.setExpandRatio(spacer, 1.0f);
        compositionRoot.addComponent(menuBar);
	}

	
	private MenuBar initMenuBar() 
	{
		MenuBar menuBar = new MenuBar();
		menuBar.setStyleName("mainMenuBar");
        MenuBar.Command menuCommand = new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				String selection = selectedItem.getText();
				if (GAP_ANALYSIS_REPORT_MENU_ITEM.equals(selection)) 
				{
					IReport report = reportProvider.getOrCreateReport(ReportID.GAP_ANALYSIS);
					fireReportChanged(report);
				} 
                else if (COMPLIANCE_REPORT_MENU_ITEM.equals(selection)) 
                {
                    IReport report = reportProvider.getOrCreateReport(ReportID.COMPLIANCE);
                    fireReportChanged(report);
                    projectComboProvider.fireProjectChanged();   
                }
				else if (COMPLIANCE_ESTIMATION_REPORT_MENU_ITEM.equals(selection)) 
				{
				    IReport report = reportProvider.getOrCreateReport(ReportID.COMPLIANCE_ESTIMATION);
				    fireReportChanged(report);                    
				    projectComboProvider.fireProjectChanged();   
				}
				else if (METRICS_ESTIMATION_REPORT_MENU_ITEM.equals(selection)) 
				{				   
					IReport report = reportProvider.getOrCreateReport(ReportID.METRICS_ESTIMATION);
					fireReportChanged(report);
					projectComboProvider.fireProjectChanged();                       
				}
				else if (EQUIVALENCE_MAP_REPORT_MENU_ITEM.equals(selection)) 
				{				   
					IReport report = reportProvider.getOrCreateReport(ReportID.EQUIVALENCE_MAP);
					fireReportChanged(report);
                    
				}
				else if (ARGUMENTATION_MENU_ITEM.equals(selection)) 
				{
					IReport report = reportProvider.getOrCreateReport(ReportID.ARGUMENTATION_ITEMS);
					fireReportChanged(report);
				} 
				else if (EVIDENCE_MENU_ITEM.equals(selection)) 
				{
					IReport report = reportProvider.getOrCreateReport(ReportID.EVIDENCE_ITEMS);
					fireReportChanged(report);
				} 
				else if (PROCESS_MENU_ITEM.equals(selection)) 
				{
					IReport report = reportProvider.getOrCreateReport(ReportID.PROCESS_ITEMS);
					fireReportChanged(report);
				}
			}
		};
        MenuItem menuItem = addMenuItem(menuBar, null, REPORTS_MENU_ITEM, null, "mainReportsMenuItem");
        menuItem.addItem(COMPLIANCE_REPORT_MENU_ITEM, new ThemeResource("images/Report.png"), menuCommand);
        menuItem.addItem(COMPLIANCE_ESTIMATION_REPORT_MENU_ITEM, new ThemeResource("images/Report.png"), menuCommand);
        menuItem.addItem(GAP_ANALYSIS_REPORT_MENU_ITEM, new ThemeResource("images/Report.png"), menuCommand);
        menuItem.addSeparator();
        menuItem.addItem(METRICS_ESTIMATION_REPORT_MENU_ITEM, new ThemeResource("images/metrics.png"), menuCommand);
        menuItem.addItem(EQUIVALENCE_MAP_REPORT_MENU_ITEM, new ThemeResource("images/map.png"), menuCommand);
        
        addMenuItem(menuBar, menuCommand, ARGUMENTATION_MENU_ITEM, null, "mainArgumentationMenuItem");
        addMenuItem(menuBar, menuCommand, EVIDENCE_MENU_ITEM, null, "mainEvidenceMenuItem"); 
        addMenuItem(menuBar, menuCommand, PROCESS_MENU_ITEM, null, "mainProcessMenuItem");
		return menuBar;
	}

	private MenuItem addMenuItem(MenuBar menuBar, MenuBar.Command menuCommand, String menuItemText, ThemeResource themeResource, String styleName) 
	{
		MenuItem mi = menuBar.addItem(menuItemText, themeResource, menuCommand);
		mi.setStyleName(styleName);
		return mi;
	}

	private void fireReportChanged(IReport report)
	{	    
	    if (reportChangeNotifier != null) {
	        reportChangeNotifier.fireReportChanged(report);
	    }
	}

	private Component initSpacer() 
	{
		Label spacer = new Label();
		spacer.setWidth("50%");
		return spacer;
	}

	public void setReportChangeNotifier(ReportChangeNotifier notifier) 
	{
		reportChangeNotifier = notifier;
	}

    public void setReportProvider(ReportProvider reportProvider)
    {
        this.reportProvider = reportProvider;
    }
}
