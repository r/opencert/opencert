/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.providers;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.providers.impl.PropertiesEditingProviderImpl;

import org.eclipse.jface.viewers.IFilter;

import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

import org.eclipse.opencert.pkm.refframework.refframework.components.RefActivityActivityApplicabilityPropertiesEditionComponent;
import org.eclipse.opencert.pkm.refframework.refframework.components.RefActivityActivityEquivalenceMapPropertiesEditionComponent;
import org.eclipse.opencert.pkm.refframework.refframework.components.RefActivityActivityRequirementPropertiesEditionComponent;
import org.eclipse.opencert.pkm.refframework.refframework.components.RefActivityBasePropertiesEditionComponent;
import org.eclipse.opencert.pkm.refframework.refframework.components.RefActivityPropertiesEditionComponent;

/**
 * 
 * 
 */
public class RefActivityPropertiesEditionProvider extends PropertiesEditingProviderImpl {

	/**
	 * Constructor without provider for super types.
	 */
	public RefActivityPropertiesEditionProvider() {
		super();
	}

	/**
	 * Constructor with providers for super types.
	 * @param superProviders providers to use for super types.
	 */
	public RefActivityPropertiesEditionProvider(List<PropertiesEditingProvider> superProviders) {
		super(superProviders);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext) {
		return (editingContext.getEObject() instanceof RefActivity) 
					&& (RefframeworkPackage.Literals.REF_ACTIVITY == editingContext.getEObject().eClass());
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext, String part) {
		return (editingContext.getEObject() instanceof RefActivity) && (RefActivityBasePropertiesEditionComponent.BASE_PART.equals(part) || RefActivityActivityRequirementPropertiesEditionComponent.ACTIVITYREQUIREMENT_PART.equals(part) || RefActivityActivityApplicabilityPropertiesEditionComponent.ACTIVITYAPPLICABILITY_PART.equals(part) || RefActivityActivityEquivalenceMapPropertiesEditionComponent.ACTIVITYEQUIVALENCEMAP_PART.equals(part));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof RefActivity) && (refinement == RefActivityBasePropertiesEditionComponent.class || refinement == RefActivityActivityRequirementPropertiesEditionComponent.class || refinement == RefActivityActivityApplicabilityPropertiesEditionComponent.class || refinement == RefActivityActivityEquivalenceMapPropertiesEditionComponent.class);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, String part, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof RefActivity) && ((RefActivityBasePropertiesEditionComponent.BASE_PART.equals(part) && refinement == RefActivityBasePropertiesEditionComponent.class) || (RefActivityActivityRequirementPropertiesEditionComponent.ACTIVITYREQUIREMENT_PART.equals(part) && refinement == RefActivityActivityRequirementPropertiesEditionComponent.class) || (RefActivityActivityApplicabilityPropertiesEditionComponent.ACTIVITYAPPLICABILITY_PART.equals(part) && refinement == RefActivityActivityApplicabilityPropertiesEditionComponent.class) || (RefActivityActivityEquivalenceMapPropertiesEditionComponent.ACTIVITYEQUIVALENCEMAP_PART.equals(part) && refinement == RefActivityActivityEquivalenceMapPropertiesEditionComponent.class));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode) {
		if (editingContext.getEObject() instanceof RefActivity) {
			return new RefActivityPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part) {
		if (editingContext.getEObject() instanceof RefActivity) {
			if (RefActivityBasePropertiesEditionComponent.BASE_PART.equals(part))
				return new RefActivityBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefActivityActivityRequirementPropertiesEditionComponent.ACTIVITYREQUIREMENT_PART.equals(part))
				return new RefActivityActivityRequirementPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefActivityActivityApplicabilityPropertiesEditionComponent.ACTIVITYAPPLICABILITY_PART.equals(part))
				return new RefActivityActivityApplicabilityPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefActivityActivityEquivalenceMapPropertiesEditionComponent.ACTIVITYEQUIVALENCEMAP_PART.equals(part))
				return new RefActivityActivityEquivalenceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String, java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part, java.lang.Class refinement) {
		if (editingContext.getEObject() instanceof RefActivity) {
			if (RefActivityBasePropertiesEditionComponent.BASE_PART.equals(part)
				&& refinement == RefActivityBasePropertiesEditionComponent.class)
				return new RefActivityBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefActivityActivityRequirementPropertiesEditionComponent.ACTIVITYREQUIREMENT_PART.equals(part)
				&& refinement == RefActivityActivityRequirementPropertiesEditionComponent.class)
				return new RefActivityActivityRequirementPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefActivityActivityApplicabilityPropertiesEditionComponent.ACTIVITYAPPLICABILITY_PART.equals(part)
				&& refinement == RefActivityActivityApplicabilityPropertiesEditionComponent.class)
				return new RefActivityActivityApplicabilityPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefActivityActivityEquivalenceMapPropertiesEditionComponent.ACTIVITYEQUIVALENCEMAP_PART.equals(part)
				&& refinement == RefActivityActivityEquivalenceMapPropertiesEditionComponent.class)
				return new RefActivityActivityEquivalenceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part, refinement);
	}

	/**
	 * Provides the filter used by the plugin.xml to assign part forms.
	 */
	public static class EditionFilter implements IFilter {
		
		/**
		 * {@inheritDoc}
		 * 
		 * @see org.eclipse.jface.viewers.IFilter#select(java.lang.Object)
		 */
		public boolean select(Object toTest) {
			EObject eObj = EEFUtils.resolveSemanticObject(toTest);
			return eObj != null && RefframeworkPackage.Literals.REF_ACTIVITY == eObj.eClass();
		}
		
	}

}
