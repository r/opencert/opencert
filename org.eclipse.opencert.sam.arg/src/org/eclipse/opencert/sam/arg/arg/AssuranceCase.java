/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg;

import org.eclipse.emf.common.util.EList;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assurance Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.AssuranceCase#getHasArgument <em>Has Argument</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.AssuranceCase#getHasEvidence <em>Has Evidence</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.AssuranceCase#getComposedOf <em>Composed Of</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssuranceCase()
 * @model
 * @generated
 */
public interface AssuranceCase extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Has Argument</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.Argumentation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Argument</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Argument</em>' containment reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssuranceCase_HasArgument()
	 * @model containment="true"
	 * @generated
	 */
	EList<Argumentation> getHasArgument();

	/**
	 * Returns the value of the '<em><b>Has Evidence</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.Artefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Evidence</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Evidence</em>' containment reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssuranceCase_HasEvidence()
	 * @model containment="true"
	 * @generated
	 */
	EList<Artefact> getHasEvidence();

	/**
	 * Returns the value of the '<em><b>Composed Of</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.AssuranceCase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composed Of</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composed Of</em>' reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssuranceCase_ComposedOf()
	 * @model
	 * @generated
	 */
	EList<AssuranceCase> getComposedOf();

} // AssuranceCase
