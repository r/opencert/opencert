/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.expressions;

import java.util.Collections;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.ocl.Environment;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorPlugin;

/**
 * @generated
 */
public class ArgOCLFactory {

	/**
	 * @generated
	 */
	private final ArgAbstractExpression[] expressions;

	/**
	 * @generated
	 */
	private final String[] expressionBodies;

	/**
	 * @generated
	 */
	protected ArgOCLFactory() {
		this.expressions = new ArgAbstractExpression[10];
		this.expressionBodies = new String[] {
				"self.oclIsTypeOf(Choice) or self.oclIsTypeOf(Claim) or self.oclIsTypeOf(ArgumentElementCitation) or self.oclIsTypeOf(Argumentation) or self.oclIsTypeOf(ArgumentReasoning)", //$NON-NLS-1$
				"self.oclIsTypeOf(Claim) or self.oclIsTypeOf(Argumentation) or self.oclIsTypeOf(ArgumentReasoning) or self.oclIsTypeOf(Agreement)", //$NON-NLS-1$
				"self.oclIsTypeOf(Claim)", //$NON-NLS-1$
				"self.oclIsTypeOf(InformationElementCitation)", //$NON-NLS-1$
				"self.oclIsTypeOf(ArgumentReasoning) or self.oclIsTypeOf(Claim) or self.oclIsTypeOf(Argumentation)", //$NON-NLS-1$
				"self.oclIsTypeOf(ArgumentElementCitation) or self.oclIsTypeOf(InformationElementCitation) or self.oclIsTypeOf(Claim) or self.oclIsTypeOf(Argumentation)", //$NON-NLS-1$
				"self.oclIsTypeOf(Claim)", //$NON-NLS-1$
				"self.oclIsTypeOf(Claim) or self.oclIsTypeOf(AssertedInference) or self.oclIsTypeOf(AssertedEvidence) or self.oclIsTypeOf(AssertedContext) or self.oclIsTypeOf(AssertedChallenge) or self.oclIsTypeOf(AssertedCounterEvidence)", //$NON-NLS-1$
				"self.oclIsTypeOf(InformationElementCitation)", //$NON-NLS-1$
				"self.oclIsTypeOf(Claim) or self.oclIsTypeOf(AssertedInference) or self.oclIsTypeOf(AssertedEvidence) or self.oclIsTypeOf(AssertedContext) or self.oclIsTypeOf(AssertedChallenge) or self.oclIsTypeOf(AssertedCounterEvidence)", //$NON-NLS-1$
		};
	}

	/**
	 * @generated
	 */
	private static ArgOCLFactory getInstance() {
		ArgOCLFactory instance = ArgDiagramEditorPlugin.getInstance()
				.getArgOCLFactory();
		if (instance == null) {
			ArgDiagramEditorPlugin.getInstance().setArgOCLFactory(
					instance = new ArgOCLFactory());
		}
		return instance;
	}

	/**
	 * @generated
	 */
	public static String getExpressionBody(int index) {
		return getInstance().expressionBodies[index];
	}

	/**
	 * @generated
	 */
	public static ArgAbstractExpression getExpression(int index,
			EClassifier context, Map<String, EClassifier> environment) {
		ArgOCLFactory cached = getInstance();
		if (index < 0 || index >= cached.expressions.length) {
			throw new IllegalArgumentException();
		}
		if (cached.expressions[index] == null) {
			cached.expressions[index] = getExpression(
					cached.expressionBodies[index],
					context,
					environment == null ? Collections
							.<String, EClassifier> emptyMap() : environment);
		}
		return cached.expressions[index];
	}

	/**
	 * This is factory method, callers are responsible to keep reference to the return value if they want to reuse parsed expression
	 * @generated
	 */
	public static ArgAbstractExpression getExpression(String body,
			EClassifier context, Map<String, EClassifier> environment) {
		return new Expression(body, context, environment);
	}

	/**
	 * This method will become private in the next release
	 * @generated
	 */
	public static ArgAbstractExpression getExpression(String body,
			EClassifier context) {
		return getExpression(body, context,
				Collections.<String, EClassifier> emptyMap());
	}

	/**
	 * @generated
	 */
	private static class Expression extends ArgAbstractExpression {

		/**
		 * @generated
		 */
		private final org.eclipse.ocl.ecore.OCL oclInstance;

		/**
		 * @generated
		 */
		private OCLExpression oclExpression;

		/**
		 * @generated
		 */
		public Expression(String body, EClassifier context,
				Map<String, EClassifier> environment) {
			super(body, context);
			oclInstance = org.eclipse.ocl.ecore.OCL.newInstance();
			initCustomEnv(oclInstance.getEnvironment(), environment);
			Helper oclHelper = oclInstance.createOCLHelper();
			oclHelper.setContext(context());
			try {
				oclExpression = oclHelper.createQuery(body());
				setStatus(IStatus.OK, null, null);
			} catch (ParserException e) {
				setStatus(IStatus.ERROR, e.getMessage(), e);
			}
		}

		/**
		 * @generated
		 */
		@SuppressWarnings("rawtypes")
		protected Object doEvaluate(Object context, Map env) {
			if (oclExpression == null) {
				return null;
			}
			// on the first call, both evalEnvironment and extentMap are clear, for later we have finally, below.
			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = oclInstance
					.getEvaluationEnvironment();
			// initialize environment
			for (Object nextKey : env.keySet()) {
				evalEnv.replace((String) nextKey, env.get(nextKey));
			}
			try {
				Object result = oclInstance.evaluate(context, oclExpression);
				return oclInstance.isInvalid(result) ? null : result;
			} finally {
				evalEnv.clear();
				oclInstance.setExtentMap(null); // clear allInstances cache, and get the oclInstance ready for the next call
			}
		}

		/**
		 * @generated
		 */
		private static void initCustomEnv(
				Environment<?, EClassifier, ?, ?, ?, EParameter, ?, ?, ?, ?, ?, ?> ecoreEnv,
				Map<String, EClassifier> environment) {
			// Use EObject as implicit root class for any object, to allow eContainer() and other EObject operations from OCL expressions
			ParsingOptions.setOption(ecoreEnv,
					ParsingOptions.implicitRootClass(ecoreEnv),
					EcorePackage.eINSTANCE.getEObject());
			for (String varName : environment.keySet()) {
				EClassifier varType = environment.get(varName);
				ecoreEnv.addElement(varName,
						createVar(ecoreEnv, varName, varType), false);
			}
		}

		/**
		 * @generated
		 */
		private static Variable createVar(
				Environment<?, EClassifier, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> ecoreEnv,
				String name, EClassifier type) {
			Variable var = EcoreFactory.eINSTANCE.createVariable();
			var.setName(name);
			var.setType(ecoreEnv.getUMLReflection().getOCLType(type));
			return var;
		}
	}
}
