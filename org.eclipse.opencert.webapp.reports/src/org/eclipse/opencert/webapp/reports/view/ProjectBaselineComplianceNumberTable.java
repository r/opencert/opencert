/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import org.eclipse.opencert.webapp.reports.containers.BaseAssetComplianceNumber;
import org.eclipse.opencert.webapp.reports.listeners.util.ClickType;
import org.eclipse.opencert.webapp.reports.listeners.util.SelectionEvent;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceNumberDataRowProcessor;
import org.eclipse.opencert.webapp.reports.manager.compliance.IComplianceConsts;
import org.eclipse.opencert.webapp.reports.util.ComplianceType;
import org.eclipse.opencert.webapp.reports.view.common.AbstractProjectBaselineComplianceTable;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;

@Theme("opencerttheme")
public class ProjectBaselineComplianceNumberTable 
	extends AbstractProjectBaselineComplianceTable
	implements IComplianceNumberButtonCreator
{
    private static final long serialVersionUID = 6915474633643585105L;
        
    private static final String COMPLIANCE_TABLE_STYLE = "complianceTable";
    private static final String GREEN_STYLE = "greenbutton";
    private static final String ORANGE_STYLE = "orangebutton";
    
    private static final String TABLE_TITLE_LABEL = "Project Baseline Compliance";
    private static final String PROJECT_BASELINE_ELEMENT_NAME = "Baseline Element Name";    
    private static final String FULLY_COMPLIANT_LABEL = "Fully Compliant Assets";    
    private static final String PARTIALLY_COMPLIANT_LABEL = "Partially Compliant Assets";
    private static final String TYPE_LABEL = "Type";
    
    public ProjectBaselineComplianceNumberTable()
    {      
        _complianceManager = new ComplianceManager<BaseAssetComplianceNumber>(new ComplianceNumberDataRowProcessor(this));
        
        VerticalLayout mainPanel = new VerticalLayout();
        
        AbsoluteLayout topInfoPanel = new AbsoluteLayout();
        topInfoPanel.setHeight("35px");
        Label tableTitle = new Label(TABLE_TITLE_LABEL);
        tableTitle.addStyleName("complianceTableTitle");
        topInfoPanel.addComponent(tableTitle);
                
        mainPanel.addComponent(topInfoPanel);
        try {
            projectBaselineComplianceTable = createEmptyBaselineComplianceTable();
            mainPanel.addComponent(projectBaselineComplianceTable);
        } catch (NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        } 

        mainPanel.setExpandRatio(projectBaselineComplianceTable, 1.0f);
            
        setCompositionRoot(mainPanel);
        setSizeFull();
    }
    
    @Override
    public void setBaselineFramework(long baselineFrameworkID)
    {
    	super.setBaselineFramework(baselineFrameworkID);
    	
    	notifyAllProjectBaselineComplianceTableListeners(new SelectionEvent(ClickType.UNSELECTED, projectBaselineComplianceTable.size()));
    }   
    
    private TreeTable createEmptyBaselineComplianceTable() throws NoSuchFieldException, SecurityException 
    {    
    	final TreeTable resultTable = new TreeTable(null, _complianceManager.initEmptyTableDataContainer());
        
        resultTable.setColumnHeader(IComplianceConsts.TYPE_PROPERTY_ID, TYPE_LABEL);
        resultTable.setColumnHeader(IComplianceConsts.NAME_PROPERTY_ID, PROJECT_BASELINE_ELEMENT_NAME);
        resultTable.setColumnHeader(IComplianceConsts.FULLY_COMPLIANT_PROPERTY_ID, FULLY_COMPLIANT_LABEL);
        resultTable.setColumnHeader(IComplianceConsts.PARTIALLY_COMPLIANT_PROPERTY_ID, PARTIALLY_COMPLIANT_LABEL);
        
        resultTable.setColumnExpandRatio(IComplianceConsts.TYPE_PROPERTY_ID, 10);
        resultTable.setColumnExpandRatio(IComplianceConsts.NAME_PROPERTY_ID, 64);
        resultTable.setColumnExpandRatio(IComplianceConsts.FULLY_COMPLIANT_PROPERTY_ID, 13);
        resultTable.setColumnExpandRatio(IComplianceConsts.PARTIALLY_COMPLIANT_PROPERTY_ID, 13);
        
        resultTable.addStyleName(COMPLIANCE_TABLE_STYLE);    
        resultTable.setPageLength(0);
        resultTable.setBuffered(false);
        resultTable.setSelectable(true);
        resultTable.setNullSelectionAllowed(false);
        resultTable.setImmediate(true);
        resultTable.setSizeFull();
        
        resultTable.addValueChangeListener(new ValueChangeListener() 
        {
            private static final long serialVersionUID = 3366377116458737822L;

            public void valueChange(ValueChangeEvent event) 
            {
                if (resultTable.getValue() != null) {
                    BaseAssetComplianceNumber baseAssetComplianceNumber = ((BaseAssetComplianceNumber)resultTable.getValue());

                    int fullyCompliant = Integer.parseInt(baseAssetComplianceNumber.getFullyCompliant().getCaption());
                    int partiallyCompliant = Integer.parseInt(baseAssetComplianceNumber.getPartiallyCompliant().getCaption());
                                      
                    SelectionEvent sEvent = new SelectionEvent(ClickType.SELECTED, baseAssetComplianceNumber.getItemId(), _complianceManager
                            .findBaselineElementTypeById(baseAssetComplianceNumber.getItemId()), ComplianceType.COMPLIANCE_ALL, fullyCompliant, partiallyCompliant);
                    
                    notifyAllProjectBaselineComplianceTableListeners(sEvent); 
                }
            }
        });
                                    
        return resultTable;
    }
    

 
    @Override
    public Button createComplianceNumberButton(final long itemId, int numberOfComplianceAssets, BaseAssetComplianceNumber bAssetComplianceNumber, final boolean isFully) 
    {
        final Button button = new NativeButton(Integer.toString(numberOfComplianceAssets));
        button.addClickListener((e) ->
        { 
            String baselineElementType = _complianceManager.findBaselineElementTypeById(itemId);
            
            int numberOfCompliantAssets = Integer.parseInt(button.getCaption()); 
                    
            SelectionEvent sEvent = new SelectionEvent(ClickType.SELECTED, itemId, baselineElementType, (isFully ? ComplianceType.COMPLIANCE_FULLY
                    : ComplianceType.COMPLIANCE_PARTIALLY), numberOfCompliantAssets);
            notifyAllProjectBaselineComplianceTableListeners(sEvent); 
            
            projectBaselineComplianceTable.setValue(bAssetComplianceNumber);
        });
        
        button.setImmediate(true);
        button.addStyleName(isFully ? GREEN_STYLE : ORANGE_STYLE);   
        
        return button;
    }
}

