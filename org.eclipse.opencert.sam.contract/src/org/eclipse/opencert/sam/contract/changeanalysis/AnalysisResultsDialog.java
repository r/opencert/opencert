/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.changeanalysis;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.layout.GridData;



public class AnalysisResultsDialog extends Dialog {
	private ArrayList listItems;
	
	public AnalysisResultsDialog(Shell parentShell, ArrayList listItems) {
		super(parentShell);
		this.listItems = listItems;
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(1, false));
		Label lblResultsFromImpact = new Label(container, SWT.NONE);
		lblResultsFromImpact.setText("Results from Analysis");
		new Label(container, SWT.NONE);
		List list = new List(container, SWT.V_SCROLL | SWT.H_SCROLL);
		GridData gd_list = new GridData(GridData.FILL_BOTH);
		gd_list.heightHint = 167;
		gd_list.widthHint = 420;
		list.setLayoutData(gd_list);
		for(int i = 0 ; i < listItems.size(); i++)
		{
			list.add((String) listItems.get(i));
		}
		

		return container;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID,IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(450, 300);
	}

}
