/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import org.eclipse.opencert.pam.procspec.process.parts.ActivityParticipantsPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;

import org.eclipse.opencert.pam.procspec.process.providers.ProcessMessages;

// End of user code

/**
 * 
 * 
 */
public class ActivityParticipantsPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ActivityParticipantsPropertiesEditionPart {

	protected ReferencesTable participant;
	protected List<ViewerFilter> participantBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> participantFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ActivityParticipantsPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence activityParticipantsStep = new BindingCompositionSequence(propertiesEditionComponent);
		activityParticipantsStep
			.addStep(ProcessViewsRepository.ActivityParticipants.Properties.class)
			.addStep(ProcessViewsRepository.ActivityParticipants.Properties.participant);
		
		
		composer = new PartComposer(activityParticipantsStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ProcessViewsRepository.ActivityParticipants.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == ProcessViewsRepository.ActivityParticipants.Properties.participant) {
					return createParticipantAdvancedReferencesTable(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(ProcessMessages.ActivityParticipantsPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * 
	 */
	protected Composite createParticipantAdvancedReferencesTable(Composite parent) {
		String label = getDescription(ProcessViewsRepository.ActivityParticipants.Properties.participant, ProcessMessages.ActivityParticipantsPropertiesEditionPart_ParticipantLabel);		 
		this.participant = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addParticipant(); }
			public void handleEdit(EObject element) { editParticipant(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveParticipant(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromParticipant(element); }
			public void navigateTo(EObject element) { }
		});
		this.participant.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ActivityParticipants.Properties.participant, ProcessViewsRepository.SWT_KIND));
		this.participant.createControls(parent);
		this.participant.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityParticipantsPropertiesEditionPartImpl.this, ProcessViewsRepository.ActivityParticipants.Properties.participant, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData participantData = new GridData(GridData.FILL_HORIZONTAL);
		participantData.horizontalSpan = 3;
		this.participant.setLayoutData(participantData);
		this.participant.disableMove();
		participant.setID(ProcessViewsRepository.ActivityParticipants.Properties.participant);
		participant.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addParticipant() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(participant.getInput(), participantFilters, participantBusinessFilters,
		"participant", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityParticipantsPropertiesEditionPartImpl.this, ProcessViewsRepository.ActivityParticipants.Properties.participant,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				participant.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveParticipant(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityParticipantsPropertiesEditionPartImpl.this, ProcessViewsRepository.ActivityParticipants.Properties.participant, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		participant.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromParticipant(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityParticipantsPropertiesEditionPartImpl.this, ProcessViewsRepository.ActivityParticipants.Properties.participant, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		participant.refresh();
	}

	/**
	 * 
	 */
	protected void editParticipant(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				participant.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityParticipantsPropertiesEditionPart#initParticipant(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initParticipant(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		participant.setContentProvider(contentProvider);
		participant.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ActivityParticipants.Properties.participant);
		if (eefElementEditorReadOnlyState && participant.getTable().isEnabled()) {
			participant.setEnabled(false);
			participant.setToolTipText(ProcessMessages.ActivityParticipants_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !participant.getTable().isEnabled()) {
			participant.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityParticipantsPropertiesEditionPart#updateParticipant()
	 * 
	 */
	public void updateParticipant() {
	participant.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityParticipantsPropertiesEditionPart#addFilterParticipant(ViewerFilter filter)
	 * 
	 */
	public void addFilterToParticipant(ViewerFilter filter) {
		participantFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityParticipantsPropertiesEditionPart#addBusinessFilterParticipant(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToParticipant(ViewerFilter filter) {
		participantBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityParticipantsPropertiesEditionPart#isContainedInParticipantTable(EObject element)
	 * 
	 */
	public boolean isContainedInParticipantTable(EObject element) {
		return ((ReferencesTableSettings)participant.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ProcessMessages.ActivityParticipants_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
