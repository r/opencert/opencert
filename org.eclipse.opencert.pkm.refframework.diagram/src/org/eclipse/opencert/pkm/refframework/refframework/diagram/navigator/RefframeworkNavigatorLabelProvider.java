/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.pkm.refframework.refframework.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.CommonParserHint;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivity2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityName2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityNameEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityPrecedingActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityProducedArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRequiredArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefArtefactNameEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefFrameworkEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefRoleNameEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkDiagramEditorPlugin;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.providers.RefframeworkElementTypes;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.providers.RefframeworkParserProvider;

/**
 * @generated
 */
public class RefframeworkNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		RefframeworkDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		RefframeworkDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof RefframeworkNavigatorItem
				&& !isOwnView(((RefframeworkNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof RefframeworkNavigatorGroup) {
			RefframeworkNavigatorGroup group = (RefframeworkNavigatorGroup) element;
			return RefframeworkDiagramEditorPlugin.getInstance()
					.getBundledImage(group.getIcon());
		}

		if (element instanceof RefframeworkNavigatorItem) {
			RefframeworkNavigatorItem navigatorItem = (RefframeworkNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getImage(view);
			}
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (RefframeworkVisualIDRegistry.getVisualID(view)) {
		case RefFrameworkEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://refframework/1.0?RefFramework", RefframeworkElementTypes.RefFramework_1000); //$NON-NLS-1$
		case RefActivityEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://refframework/1.0?RefActivity", RefframeworkElementTypes.RefActivity_2001); //$NON-NLS-1$
		case RefArtefactEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://refframework/1.0?RefArtefact", RefframeworkElementTypes.RefArtefact_2002); //$NON-NLS-1$
		case RefRoleEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://refframework/1.0?RefRole", RefframeworkElementTypes.RefRole_2003); //$NON-NLS-1$
		case RefActivity2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://refframework/1.0?RefActivity", RefframeworkElementTypes.RefActivity_3001); //$NON-NLS-1$
		case RefActivityRequiredArtefactEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://refframework/1.0?RefActivity?requiredArtefact", RefframeworkElementTypes.RefActivityRequiredArtefact_4001); //$NON-NLS-1$
		case RefActivityProducedArtefactEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://refframework/1.0?RefActivity?producedArtefact", RefframeworkElementTypes.RefActivityProducedArtefact_4002); //$NON-NLS-1$
		case RefActivityPrecedingActivityEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://refframework/1.0?RefActivity?precedingActivity", RefframeworkElementTypes.RefActivityPrecedingActivity_4003); //$NON-NLS-1$
		case RefActivityRoleEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://refframework/1.0?RefActivity?role", RefframeworkElementTypes.RefActivityRole_4004); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = RefframeworkDiagramEditorPlugin
				.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& RefframeworkElementTypes.isKnownElementType(elementType)) {
			image = RefframeworkElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof RefframeworkNavigatorGroup) {
			RefframeworkNavigatorGroup group = (RefframeworkNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof RefframeworkNavigatorItem) {
			RefframeworkNavigatorItem navigatorItem = (RefframeworkNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getText(view);
			}
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (RefframeworkVisualIDRegistry.getVisualID(view)) {
		case RefFrameworkEditPart.VISUAL_ID:
			return getRefFramework_1000Text(view);
		case RefActivityEditPart.VISUAL_ID:
			return getRefActivity_2001Text(view);
		case RefArtefactEditPart.VISUAL_ID:
			return getRefArtefact_2002Text(view);
		case RefRoleEditPart.VISUAL_ID:
			return getRefRole_2003Text(view);
		case RefActivity2EditPart.VISUAL_ID:
			return getRefActivity_3001Text(view);
		case RefActivityRequiredArtefactEditPart.VISUAL_ID:
			return getRefActivityRequiredArtefact_4001Text(view);
		case RefActivityProducedArtefactEditPart.VISUAL_ID:
			return getRefActivityProducedArtefact_4002Text(view);
		case RefActivityPrecedingActivityEditPart.VISUAL_ID:
			return getRefActivityPrecedingActivity_4003Text(view);
		case RefActivityRoleEditPart.VISUAL_ID:
			return getRefActivityRole_4004Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getRefFramework_1000Text(View view) {
		RefFramework domainModelElement = (RefFramework) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			RefframeworkDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRefActivity_2001Text(View view) {
		IParser parser = RefframeworkParserProvider.getParser(
				RefframeworkElementTypes.RefActivity_2001,
				view.getElement() != null ? view.getElement() : view,
				RefframeworkVisualIDRegistry
						.getType(RefActivityNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			RefframeworkDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRefArtefact_2002Text(View view) {
		IParser parser = RefframeworkParserProvider.getParser(
				RefframeworkElementTypes.RefArtefact_2002,
				view.getElement() != null ? view.getElement() : view,
				RefframeworkVisualIDRegistry
						.getType(RefArtefactNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			RefframeworkDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRefRole_2003Text(View view) {
		IParser parser = RefframeworkParserProvider.getParser(
				RefframeworkElementTypes.RefRole_2003,
				view.getElement() != null ? view.getElement() : view,
				RefframeworkVisualIDRegistry
						.getType(RefRoleNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			RefframeworkDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRefActivity_3001Text(View view) {
		IParser parser = RefframeworkParserProvider.getParser(
				RefframeworkElementTypes.RefActivity_3001,
				view.getElement() != null ? view.getElement() : view,
				RefframeworkVisualIDRegistry
						.getType(RefActivityName2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			RefframeworkDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRefActivityRequiredArtefact_4001Text(View view) {
		IParser parser = RefframeworkParserProvider.getParser(
				RefframeworkElementTypes.RefActivityRequiredArtefact_4001,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			RefframeworkDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRefActivityProducedArtefact_4002Text(View view) {
		IParser parser = RefframeworkParserProvider.getParser(
				RefframeworkElementTypes.RefActivityProducedArtefact_4002,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			RefframeworkDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRefActivityPrecedingActivity_4003Text(View view) {
		IParser parser = RefframeworkParserProvider.getParser(
				RefframeworkElementTypes.RefActivityPrecedingActivity_4003,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			RefframeworkDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRefActivityRole_4004Text(View view) {
		IParser parser = RefframeworkParserProvider.getParser(
				RefframeworkElementTypes.RefActivityRole_4004,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			RefframeworkDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return RefFrameworkEditPart.MODEL_ID
				.equals(RefframeworkVisualIDRegistry.getModelID(view));
	}

}
