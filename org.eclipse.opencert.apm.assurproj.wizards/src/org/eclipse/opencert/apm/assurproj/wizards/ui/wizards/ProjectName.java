/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards.ui.wizards;

//import org.eclipse.emf.cdo.dawn.ui.composites.CDOResourceNodeChooserComposite;
import org.eclipse.emf.cdo.dawn.ui.composites.CDOResourceNodeSelectionWidget;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;



public class ProjectName extends WizardPage {

  private Text textName;

  private Composite container;

  private Button generateArgModel;
 
  private CDOResourceNodeSelectionWidget chooserComposite;

  private String cdoFolder="";
  
  public ProjectName() {

    super("Creation of Assurance Project");

    setTitle("Creation of Assurance Project");

    setDescription("Creation of Assurance Project");

  }



  @Override

  public void createControl(Composite parent) {

    container = new Composite(parent, SWT.NONE);
    
    
    
    GridLayout layout = new GridLayout();

    container.setLayout(layout);

    layout.numColumns = 2;
    
    CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
	CDOView view = sessionCDO.openView();
	
	Label label1 = new Label(container, SWT.NONE);

    label1.setText("Select Project folder:");
	
    GridData gd = new GridData(GridData.FILL_BOTH);

    
    
    chooserComposite = new CDOResourceNodeSelectionWidget(container, SWT.NONE);
    
    chooserComposite.setLayoutData(gd);
    
    chooserComposite.addSelectionChangedListener(new ISelectionChangedListener()
    {
      public void selectionChanged(SelectionChangedEvent event)
      {
    	  if (event.getSelection() instanceof IStructuredSelection)
          {
            IStructuredSelection selection = (IStructuredSelection)event.getSelection();
            Object element = selection.getFirstElement();
            if (element instanceof CDOResource)
            {
            	cdoFolder="";
            }
            else if (element instanceof CDOResourceNode)
            {
            	cdoFolder= ((CDOResourceNode)element).getPath();
            }
          }
    	  
    	  if (!textName.getText().isEmpty() && cdoFolder!= "") {

              setPageComplete(true);

            }
      }
    });
    		
    		//CDOResourceNodeChooserComposite(container, SWT.NONE, "", view);
    chooserComposite.setShowResources(false);
    //chooserComposite.se

    
    
    Label label2 = new Label(container, SWT.NONE);

    label2.setText("Project name:");



    textName = new Text(container, SWT.BORDER | SWT.SINGLE);

    textName.setText("");

    textName.addKeyListener(new KeyListener() {



      @Override

      public void keyPressed(KeyEvent e) {

      }



      @Override

      public void keyReleased(KeyEvent e) {    	     	  

        if (!textName.getText().isEmpty() && cdoFolder!= "") {

          setPageComplete(true);

        }

      }



    });

    GridData gd2 = new GridData(GridData.FILL_HORIZONTAL);

    textName.setLayoutData(gd2);
    
    generateArgModel= new Button(container, SWT.CHECK);
    generateArgModel.setText("Generate Argumentation model and diagram");
    generateArgModel.setSelection(false);

    GridData gridData = new GridData(GridData.VERTICAL_ALIGN_END);
    gridData.horizontalSpan = 2;
    generateArgModel.setLayoutData(gridData);
    
    // Required to avoid an error in the system

    setControl(container);

    setPageComplete(false);



  }



  public String getName() {

    return textName.getText();

  }
  
  public String getProjectFolder(){
	  return cdoFolder;
  }
  
  
  public boolean generateArgumentation() {

    return generateArgModel.getSelection();

  }

}
 