/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.autocomplete.proposalproviders;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.opencert.userguidance.autocomplete.ContentProposal2;

public abstract class AbstractProposalProvider implements
		IContentProposalProvider {
	private EObject eobj;
	private int featureID;
	private Image image;

	public AbstractProposalProvider(EObject eobj, int featureID) {
		this.eobj = eobj;
		this.featureID = featureID;
	}

	@Override
	public IContentProposal[] getProposals(String contents, int position) {
		String stringToComplete = getStringToComplete(contents, position);
		List<ContentProposal2> proposals = createProposals(stringToComplete, eobj,
				featureID);
		for (ContentProposal2 prop : proposals) {
			prop.setImage(image);
		}
		IContentProposal[] result = proposals
				.toArray(new IContentProposal[proposals.size()]);
		return result;
	}

	protected String getStringToComplete(String contents, int caretOffset) {
		String beforeCaret = contents.substring(0, caretOffset);
		int postWhiteSpaceString = getLastWhiteSpace(beforeCaret, caretOffset);
		return beforeCaret.substring(postWhiteSpaceString + 1);
	}

	// Gets last whitespace before the caret
	private int getLastWhiteSpace(String contents, int caretOffset) {
		for (int i = caretOffset - 1; i >= 0; i--) {
			char c = contents.charAt(i);
			if (Character.isWhitespace(c)) {
				return i;
			}
		}

		return -1;
	}

	protected abstract List<ContentProposal2> createProposals(String stringToComplete,
			EObject eobj, int featureID);

	public Image getImage() {
		if (image == null) {
			image = createImage();
		}

		return image;
	}
	
	protected abstract Image createImage();
}
