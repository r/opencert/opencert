package org.eclipse.opencert.sam.arg.arg.diagram.instantiation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.menus.ExtensionContributionFactory;
import org.eclipse.ui.menus.IContributionRoot;
import org.eclipse.ui.services.IServiceLocator;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditor;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem.ItemHeaderType;
import org.eclipse.opencert.userguidance.util.HitTestUtil;
import org.eclipse.opencert.userguidance.util.VocabFetcher;
import org.eclipse.opencert.vocabulary.Category;
import org.eclipse.opencert.vocabulary.Term;
import org.eclipse.opencert.vocabulary.Vocabulary;

public class InstantiationCommandFactory extends ExtensionContributionFactory {

	@Override
	public void createContributionItems(IServiceLocator serviceLocator,
			IContributionRoot additions) {
		List<CommandContributionItemParameter> parameters = createCommandParameters(serviceLocator);

		for (CommandContributionItemParameter parameter : parameters) {
			CommandContributionItem item = new CommandContributionItem(
					parameter);
			item.setVisible(true);
			item.setId(parameter.commandId);
			additions.addContributionItem(item, null);
		}
	}

	public List<CommandContributionItemParameter> createCommandParameters(
			final IServiceLocator serviceLocator) {
		final List<CommandContributionItemParameter> result = new ArrayList<CommandContributionItemParameter>();
		Point screenMouseLocation = PlatformUI.getWorkbench().getDisplay()
				.getCursorLocation();
		IEditorPart editorPart = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();

		if (editorPart instanceof ArgDiagramEditor) {
			final ArgDiagramEditor editor = (ArgDiagramEditor) editorPart;
			HighlightItem highlightItem = HitTestUtil.getHighlightItemScreen(
					editor, screenMouseLocation.x, screenMouseLocation.y);

			if (highlightItem != null
					&& highlightItem.getHeaderType() == ItemHeaderType.var) {
				final String itemBody = highlightItem.getBody();

				VocabFetcher fetcher = new VocabFetcher() {
					@Override
					public void readFromVocab(List<Vocabulary> vocabularies) {
						List<Term> terms = getSubTerms(itemBody, vocabularies);
						for (Term term : terms) {
							Map<String, Object> parameters = new HashMap<String, Object>();

							parameters.put(ParameterNames.itemBody, itemBody);

							String replacement = "";

							String name = term.getName();
							if (name.contains(" ")) {
								name = "\"" + name + "\"";
							}
							replacement = "voc:" + name;

							parameters.put(ParameterNames.replacement,
									replacement);
							parameters.put(ParameterNames.itemBody, itemBody);
							parameters.put(ParameterNames.commandName,
									"Instantiate " + itemBody);

							parameters.put(ParameterNames.editorHash,
									editor.hashCode() + "");

							CommandContributionItemParameter parameter = new CommandContributionItemParameter(
									serviceLocator, "",
									ParameterNames.commandID, SWT.PUSH);
							parameter.label = term.getName();
							parameter.parameters = parameters;

							result.add(parameter);
						}
					}
				};
				fetcher.execute();

			}
		}
		return result;
	}

	private List<Term> getSubTerms(String categoryName,
			List<Vocabulary> vocabularies) {
		List<Term> result = new ArrayList<Term>();

		for (Vocabulary vocabulary : vocabularies) {
			for (Category category : vocabulary.getCategories()) {
				if (category.getName().equalsIgnoreCase(categoryName)) {
					for (Term term : category.getTerms()) {
						result.add(term);
					}
				}
			}
		}

		return result;
	}
}