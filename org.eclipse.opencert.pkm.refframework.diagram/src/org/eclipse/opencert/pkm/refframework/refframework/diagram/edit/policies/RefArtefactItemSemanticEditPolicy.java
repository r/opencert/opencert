/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityProducedArtefactCreateCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityProducedArtefactReorientCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityRequiredArtefactCreateCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityRequiredArtefactReorientCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityProducedArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRequiredArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.providers.RefframeworkElementTypes;

/**
 * @generated
 */
public class RefArtefactItemSemanticEditPolicy extends
		RefframeworkBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RefArtefactItemSemanticEditPolicy() {
		super(RefframeworkElementTypes.RefArtefact_2002);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		View view = (View) getHost().getModel();
		CompositeTransactionalCommand cmd = new CompositeTransactionalCommand(
				getEditingDomain(), null);
		cmd.setTransactionNestingEnabled(false);
		for (Iterator<?> it = view.getTargetEdges().iterator(); it.hasNext();) {
			Edge incomingLink = (Edge) it.next();
			if (RefframeworkVisualIDRegistry.getVisualID(incomingLink) == RefActivityRequiredArtefactEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						incomingLink.getSource().getElement(), null,
						incomingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
			if (RefframeworkVisualIDRegistry.getVisualID(incomingLink) == RefActivityProducedArtefactEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						incomingLink.getSource().getElement(), null,
						incomingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
		}
		EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
		if (annotation == null) {
			// there are indirectly referenced children, need extra commands: false
			addDestroyShortcutsCommand(cmd, view);
			// delete host element
			cmd.add(new DestroyElementCommand(req));
		} else {
			cmd.add(new DeleteCommand(getEditingDomain(), view));
		}
		return getGEFWrapper(cmd.reduce());
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req)
				: getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super
				.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (RefframeworkElementTypes.RefActivityRequiredArtefact_4001 == req
				.getElementType()) {
			return null;
		}
		if (RefframeworkElementTypes.RefActivityProducedArtefact_4002 == req
				.getElementType()) {
			return null;
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (RefframeworkElementTypes.RefActivityRequiredArtefact_4001 == req
				.getElementType()) {
			return getGEFWrapper(new RefActivityRequiredArtefactCreateCommand(
					req, req.getSource(), req.getTarget()));
		}
		if (RefframeworkElementTypes.RefActivityProducedArtefact_4002 == req
				.getElementType()) {
			return getGEFWrapper(new RefActivityProducedArtefactCreateCommand(
					req, req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * Returns command to reorient EReference based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientReferenceRelationshipCommand(
			ReorientReferenceRelationshipRequest req) {
		switch (getVisualID(req)) {
		case RefActivityRequiredArtefactEditPart.VISUAL_ID:
			return getGEFWrapper(new RefActivityRequiredArtefactReorientCommand(
					req));
		case RefActivityProducedArtefactEditPart.VISUAL_ID:
			return getGEFWrapper(new RefActivityProducedArtefactReorientCommand(
					req));
		}
		return super.getReorientReferenceRelationshipCommand(req);
	}

}
