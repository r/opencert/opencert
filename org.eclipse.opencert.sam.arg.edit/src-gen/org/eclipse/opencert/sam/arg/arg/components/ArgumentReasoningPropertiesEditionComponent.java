/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentReasoning;
import org.eclipse.opencert.sam.arg.arg.Argumentation;

import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;
import org.eclipse.opencert.sam.arg.arg.parts.ArgumentReasoningPropertiesEditionPart;


// End of user code

/**
 * 
 * 
 */
public class ArgumentReasoningPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for hasStructure ReferencesTable
	 */
	private ReferencesTableSettings hasStructureSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ArgumentReasoningPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject argumentReasoning, String editing_mode) {
		super(editingContext, argumentReasoning, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = ArgViewsRepository.class;
		partKey = ArgViewsRepository.ArgumentReasoning.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final ArgumentReasoning argumentReasoning = (ArgumentReasoning)elt;
			final ArgumentReasoningPropertiesEditionPart basePart = (ArgumentReasoningPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, argumentReasoning.getId()));
			
			if (isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, argumentReasoning.getName()));
			
			if (isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, argumentReasoning.getDescription()));
			
			if (isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.content))
				basePart.setContent(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, argumentReasoning.getContent()));
			
			if (isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported))
				basePart.setToBeSupported(EEFConverterUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, argumentReasoning.getToBeSupported()));
			
			if (isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated))
				basePart.setToBeInstantiated(EEFConverterUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, argumentReasoning.getToBeInstantiated()));
			
			if (isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.hasStructure)) {
				hasStructureSettings = new ReferencesTableSettings(argumentReasoning, ArgPackage.eINSTANCE.getArgumentReasoning_HasStructure());
				basePart.initHasStructure(hasStructureSettings);
			}
			// init filters
			
			
			
			
			
			
			if (isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.hasStructure)) {
				basePart.addFilterToHasStructure(new EObjectFilter(ArgPackage.Literals.ARGUMENTATION));
				// Start of user code for additional businessfilters for hasStructure
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}










	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ArgViewsRepository.ArgumentReasoning.Properties.id) {
			return ArgPackage.eINSTANCE.getModelElement_Id();
		}
		if (editorKey == ArgViewsRepository.ArgumentReasoning.Properties.name) {
			return ArgPackage.eINSTANCE.getModelElement_Name();
		}
		if (editorKey == ArgViewsRepository.ArgumentReasoning.Properties.description) {
			return ArgPackage.eINSTANCE.getArgumentationElement_Description();
		}
		if (editorKey == ArgViewsRepository.ArgumentReasoning.Properties.content) {
			return ArgPackage.eINSTANCE.getArgumentationElement_Content();
		}
		if (editorKey == ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported) {
			return ArgPackage.eINSTANCE.getArgumentReasoning_ToBeSupported();
		}
		if (editorKey == ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated) {
			return ArgPackage.eINSTANCE.getArgumentReasoning_ToBeInstantiated();
		}
		if (editorKey == ArgViewsRepository.ArgumentReasoning.Properties.hasStructure) {
			return ArgPackage.eINSTANCE.getArgumentReasoning_HasStructure();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		ArgumentReasoning argumentReasoning = (ArgumentReasoning)semanticObject;
		if (ArgViewsRepository.ArgumentReasoning.Properties.id == event.getAffectedEditor()) {
			argumentReasoning.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.ArgumentReasoning.Properties.name == event.getAffectedEditor()) {
			argumentReasoning.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.ArgumentReasoning.Properties.description == event.getAffectedEditor()) {
			argumentReasoning.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.ArgumentReasoning.Properties.content == event.getAffectedEditor()) {
			argumentReasoning.setContent((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported == event.getAffectedEditor()) {
			argumentReasoning.setToBeSupported((java.lang.Boolean)EEFConverterUtil.createFromString(EcorePackage.Literals.EBOOLEAN_OBJECT, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated == event.getAffectedEditor()) {
			argumentReasoning.setToBeInstantiated((java.lang.Boolean)EEFConverterUtil.createFromString(EcorePackage.Literals.EBOOLEAN_OBJECT, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.ArgumentReasoning.Properties.hasStructure == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Argumentation) {
					hasStructureSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				hasStructureSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				hasStructureSettings.move(event.getNewIndex(), (Argumentation) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ArgumentReasoningPropertiesEditionPart basePart = (ArgumentReasoningPropertiesEditionPart)editingPart;
			if (ArgPackage.eINSTANCE.getModelElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (ArgPackage.eINSTANCE.getModelElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentationElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentationElement_Content().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.content)) {
				if (msg.getNewValue() != null) {
					basePart.setContent(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setContent("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentReasoning_ToBeSupported().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported)) {
				if (msg.getNewValue() != null) {
					basePart.setToBeSupported(EcoreUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, msg.getNewValue()));
				} else {
					basePart.setToBeSupported("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentReasoning_ToBeInstantiated().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated)) {
				if (msg.getNewValue() != null) {
					basePart.setToBeInstantiated(EcoreUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, msg.getNewValue()));
				} else {
					basePart.setToBeInstantiated("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentReasoning_HasStructure().equals(msg.getFeature())  && isAccessible(ArgViewsRepository.ArgumentReasoning.Properties.hasStructure))
				basePart.updateHasStructure();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			ArgPackage.eINSTANCE.getModelElement_Id(),
			ArgPackage.eINSTANCE.getModelElement_Name(),
			ArgPackage.eINSTANCE.getArgumentationElement_Description(),
			ArgPackage.eINSTANCE.getArgumentationElement_Content(),
			ArgPackage.eINSTANCE.getArgumentReasoning_ToBeSupported(),
			ArgPackage.eINSTANCE.getArgumentReasoning_ToBeInstantiated(),
			ArgPackage.eINSTANCE.getArgumentReasoning_HasStructure()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#isRequired(java.lang.Object, int)
	 * 
	 */
	public boolean isRequired(Object key, int kind) {
		return key == ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported || key == ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (ArgViewsRepository.ArgumentReasoning.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.ArgumentReasoning.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.ArgumentReasoning.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentationElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentationElement_Description().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.ArgumentReasoning.Properties.content == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentationElement_Content().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentationElement_Content().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.ArgumentReasoning.Properties.toBeSupported == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentReasoning_ToBeSupported().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentReasoning_ToBeSupported().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.ArgumentReasoning.Properties.toBeInstantiated == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentReasoning_ToBeInstantiated().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentReasoning_ToBeInstantiated().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
