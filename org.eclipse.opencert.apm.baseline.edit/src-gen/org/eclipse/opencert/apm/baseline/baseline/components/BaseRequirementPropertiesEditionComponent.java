/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementApplicabilityPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementSelectionPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

// End of user code

/**
 * 
 * 
 */
public class BaseRequirementPropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private BaseRequirementPropertiesEditionPart basePart;

	/**
	 * The BaseRequirementBasePropertiesEditionComponent sub component
	 * 
	 */
	protected BaseRequirementBasePropertiesEditionComponent baseRequirementBasePropertiesEditionComponent;

	/**
	 * The BaseRequirementSelection part
	 * 
	 */
	private BaseRequirementSelectionPropertiesEditionPart baseRequirementSelectionPart;

	/**
	 * The BaseRequirementBaseRequirementSelectionPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseRequirementBaseRequirementSelectionPropertiesEditionComponent baseRequirementBaseRequirementSelectionPropertiesEditionComponent;

	/**
	 * The BaseRequirementApplicability part
	 * 
	 */
	private BaseRequirementApplicabilityPropertiesEditionPart baseRequirementApplicabilityPart;

	/**
	 * The BaseRequirementBaseRequirementApplicabilityPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseRequirementBaseRequirementApplicabilityPropertiesEditionComponent baseRequirementBaseRequirementApplicabilityPropertiesEditionComponent;

	/**
	 * The BaseRequirementEquivalenceMap part
	 * 
	 */
	private BaseRequirementEquivalenceMapPropertiesEditionPart baseRequirementEquivalenceMapPart;

	/**
	 * The BaseRequirementBaseRequirementEquivalenceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseRequirementBaseRequirementEquivalenceMapPropertiesEditionComponent baseRequirementBaseRequirementEquivalenceMapPropertiesEditionComponent;

	/**
	 * The BaseRequerimentComplianceMap part
	 * 
	 */
	private BaseRequerimentComplianceMapPropertiesEditionPart baseRequerimentComplianceMapPart;

	/**
	 * The BaseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent baseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param baseRequirement the EObject to edit
	 * 
	 */
	public BaseRequirementPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseRequirement, String editing_mode) {
		super(editingContext, editing_mode);
		if (baseRequirement instanceof BaseRequirement) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseRequirement, PropertiesEditingProvider.class);
			baseRequirementBasePropertiesEditionComponent = (BaseRequirementBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseRequirementBasePropertiesEditionComponent.BASE_PART, BaseRequirementBasePropertiesEditionComponent.class);
			addSubComponent(baseRequirementBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseRequirement, PropertiesEditingProvider.class);
			baseRequirementBaseRequirementSelectionPropertiesEditionComponent = (BaseRequirementBaseRequirementSelectionPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseRequirementBaseRequirementSelectionPropertiesEditionComponent.BASEREQUIREMENTSELECTION_PART, BaseRequirementBaseRequirementSelectionPropertiesEditionComponent.class);
			addSubComponent(baseRequirementBaseRequirementSelectionPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseRequirement, PropertiesEditingProvider.class);
			baseRequirementBaseRequirementApplicabilityPropertiesEditionComponent = (BaseRequirementBaseRequirementApplicabilityPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseRequirementBaseRequirementApplicabilityPropertiesEditionComponent.BASEREQUIREMENTAPPLICABILITY_PART, BaseRequirementBaseRequirementApplicabilityPropertiesEditionComponent.class);
			addSubComponent(baseRequirementBaseRequirementApplicabilityPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseRequirement, PropertiesEditingProvider.class);
			baseRequirementBaseRequirementEquivalenceMapPropertiesEditionComponent = (BaseRequirementBaseRequirementEquivalenceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseRequirementBaseRequirementEquivalenceMapPropertiesEditionComponent.BASEREQUIREMENTEQUIVALENCEMAP_PART, BaseRequirementBaseRequirementEquivalenceMapPropertiesEditionComponent.class);
			addSubComponent(baseRequirementBaseRequirementEquivalenceMapPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseRequirement, PropertiesEditingProvider.class);
			baseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent = (BaseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent.BASEREQUERIMENTCOMPLIANCEMAP_PART, BaseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent.class);
			addSubComponent(baseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (BaseRequirementBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (BaseRequirementPropertiesEditionPart)baseRequirementBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (BaseRequirementBaseRequirementSelectionPropertiesEditionComponent.BASEREQUIREMENTSELECTION_PART.equals(key)) {
			baseRequirementSelectionPart = (BaseRequirementSelectionPropertiesEditionPart)baseRequirementBaseRequirementSelectionPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseRequirementSelectionPart;
		}
		if (BaseRequirementBaseRequirementApplicabilityPropertiesEditionComponent.BASEREQUIREMENTAPPLICABILITY_PART.equals(key)) {
			baseRequirementApplicabilityPart = (BaseRequirementApplicabilityPropertiesEditionPart)baseRequirementBaseRequirementApplicabilityPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseRequirementApplicabilityPart;
		}
		if (BaseRequirementBaseRequirementEquivalenceMapPropertiesEditionComponent.BASEREQUIREMENTEQUIVALENCEMAP_PART.equals(key)) {
			baseRequirementEquivalenceMapPart = (BaseRequirementEquivalenceMapPropertiesEditionPart)baseRequirementBaseRequirementEquivalenceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseRequirementEquivalenceMapPart;
		}
		if (BaseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent.BASEREQUERIMENTCOMPLIANCEMAP_PART.equals(key)) {
			baseRequerimentComplianceMapPart = (BaseRequerimentComplianceMapPropertiesEditionPart)baseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseRequerimentComplianceMapPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (BaselineViewsRepository.BaseRequirement.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (BaseRequirementPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseRequirementSelection.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseRequirementSelectionPart = (BaseRequirementSelectionPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseRequirementApplicability.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseRequirementApplicabilityPart = (BaseRequirementApplicabilityPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseRequirementEquivalenceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseRequirementEquivalenceMapPart = (BaseRequirementEquivalenceMapPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseRequerimentComplianceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseRequerimentComplianceMapPart = (BaseRequerimentComplianceMapPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == BaselineViewsRepository.BaseRequirement.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseRequirementSelection.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseRequirementApplicability.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseRequirementEquivalenceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseRequerimentComplianceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
