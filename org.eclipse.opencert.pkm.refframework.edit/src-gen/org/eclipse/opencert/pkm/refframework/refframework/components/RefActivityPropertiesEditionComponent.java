/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;

import org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityApplicabilityPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityRequirementPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;

// End of user code

/**
 * 
 * 
 */
public class RefActivityPropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private RefActivityPropertiesEditionPart basePart;

	/**
	 * The RefActivityBasePropertiesEditionComponent sub component
	 * 
	 */
	protected RefActivityBasePropertiesEditionComponent refActivityBasePropertiesEditionComponent;

	/**
	 * The ActivityRequirement part
	 * 
	 */
	private ActivityRequirementPropertiesEditionPart activityRequirementPart;

	/**
	 * The RefActivityActivityRequirementPropertiesEditionComponent sub component
	 * 
	 */
	protected RefActivityActivityRequirementPropertiesEditionComponent refActivityActivityRequirementPropertiesEditionComponent;

	/**
	 * The ActivityApplicability part
	 * 
	 */
	private ActivityApplicabilityPropertiesEditionPart activityApplicabilityPart;

	/**
	 * The RefActivityActivityApplicabilityPropertiesEditionComponent sub component
	 * 
	 */
	protected RefActivityActivityApplicabilityPropertiesEditionComponent refActivityActivityApplicabilityPropertiesEditionComponent;

	/**
	 * The ActivityEquivalenceMap part
	 * 
	 */
	private ActivityEquivalenceMapPropertiesEditionPart activityEquivalenceMapPart;

	/**
	 * The RefActivityActivityEquivalenceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected RefActivityActivityEquivalenceMapPropertiesEditionComponent refActivityActivityEquivalenceMapPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param refActivity the EObject to edit
	 * 
	 */
	public RefActivityPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refActivity, String editing_mode) {
		super(editingContext, editing_mode);
		if (refActivity instanceof RefActivity) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refActivity, PropertiesEditingProvider.class);
			refActivityBasePropertiesEditionComponent = (RefActivityBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefActivityBasePropertiesEditionComponent.BASE_PART, RefActivityBasePropertiesEditionComponent.class);
			addSubComponent(refActivityBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refActivity, PropertiesEditingProvider.class);
			refActivityActivityRequirementPropertiesEditionComponent = (RefActivityActivityRequirementPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefActivityActivityRequirementPropertiesEditionComponent.ACTIVITYREQUIREMENT_PART, RefActivityActivityRequirementPropertiesEditionComponent.class);
			addSubComponent(refActivityActivityRequirementPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refActivity, PropertiesEditingProvider.class);
			refActivityActivityApplicabilityPropertiesEditionComponent = (RefActivityActivityApplicabilityPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefActivityActivityApplicabilityPropertiesEditionComponent.ACTIVITYAPPLICABILITY_PART, RefActivityActivityApplicabilityPropertiesEditionComponent.class);
			addSubComponent(refActivityActivityApplicabilityPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refActivity, PropertiesEditingProvider.class);
			refActivityActivityEquivalenceMapPropertiesEditionComponent = (RefActivityActivityEquivalenceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefActivityActivityEquivalenceMapPropertiesEditionComponent.ACTIVITYEQUIVALENCEMAP_PART, RefActivityActivityEquivalenceMapPropertiesEditionComponent.class);
			addSubComponent(refActivityActivityEquivalenceMapPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (RefActivityBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (RefActivityPropertiesEditionPart)refActivityBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (RefActivityActivityRequirementPropertiesEditionComponent.ACTIVITYREQUIREMENT_PART.equals(key)) {
			activityRequirementPart = (ActivityRequirementPropertiesEditionPart)refActivityActivityRequirementPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)activityRequirementPart;
		}
		if (RefActivityActivityApplicabilityPropertiesEditionComponent.ACTIVITYAPPLICABILITY_PART.equals(key)) {
			activityApplicabilityPart = (ActivityApplicabilityPropertiesEditionPart)refActivityActivityApplicabilityPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)activityApplicabilityPart;
		}
		if (RefActivityActivityEquivalenceMapPropertiesEditionComponent.ACTIVITYEQUIVALENCEMAP_PART.equals(key)) {
			activityEquivalenceMapPart = (ActivityEquivalenceMapPropertiesEditionPart)refActivityActivityEquivalenceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)activityEquivalenceMapPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (RefframeworkViewsRepository.RefActivity.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (RefActivityPropertiesEditionPart)propertiesEditionPart;
		}
		if (RefframeworkViewsRepository.ActivityRequirement.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			activityRequirementPart = (ActivityRequirementPropertiesEditionPart)propertiesEditionPart;
		}
		if (RefframeworkViewsRepository.ActivityApplicability.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			activityApplicabilityPart = (ActivityApplicabilityPropertiesEditionPart)propertiesEditionPart;
		}
		if (RefframeworkViewsRepository.ActivityEquivalenceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			activityEquivalenceMapPart = (ActivityEquivalenceMapPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == RefframeworkViewsRepository.RefActivity.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == RefframeworkViewsRepository.ActivityRequirement.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == RefframeworkViewsRepository.ActivityApplicability.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == RefframeworkViewsRepository.ActivityEquivalenceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
