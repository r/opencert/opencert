/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

import org.eclipse.opencert.webapp.reports.containers.ComplianceAsset;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetEvaluation;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetProperty;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetResource;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetails;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetailsItem;
import org.eclipse.opencert.webapp.reports.containers.visitors.IComplianceDetailsVisitor;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;

public class ComplianceDetailsPanelVisitor implements
		IComplianceDetailsVisitor<ComplianceDetailsItem> {
	
	private final HierarchicalContainer _hcResult;
	private final String[] _itemProperties;
	
	private static final String COMPLIANCE_TYPE_FULLY_DESC = "Fully";
	private static final String COMPLIANCE_TYPE_PARTIALLY_DESC = "Partially";
	private static final String COMPLIANCE_JUSTIFICATION = "Compliance Justification ";	
	private static final String TREE_LEVEL_JUST = "levelJust";
	private static final String TREE_LEVEL_ASSET = "levelAsset";
	private static final String TREE_LEVEL_EVAL = "levelEval";
	private static final String TREE_LEVEL_EVAL_CHILD = "levelEvalChild";
	
	private int complianceDetailsCount = 0;
	private int complianceAssetCount = 0;
	private int complianceEvaluationCount = 0;
	
	public ComplianceDetailsPanelVisitor(HierarchicalContainer complianceDetailsContainer, String[] itemProperties) 
	{
		_hcResult = complianceDetailsContainer;
		_itemProperties = itemProperties;
	}

	@Override
	public ComplianceDetailsItem visitComplianceDetails(ComplianceDetails complianceDetails) {
		complianceDetailsCount++;
		complianceAssetCount = 0;
		complianceEvaluationCount = 0;
		String complianceJustificationName = buildComplianceJustificationName(complianceDetails, complianceDetailsCount);
		
		ComplianceDetailsItem item = new ComplianceDetailsItem(complianceJustificationName, "", TREE_LEVEL_JUST);
		addItemToContainer(_hcResult, item, null);
		
		return item;
	}

	@Override
	public ComplianceDetailsItem visitComplianceAsset(ComplianceAsset complianceAsset, ComplianceDetails rootCD, ComplianceDetailsItem parentNode) {
		complianceAssetCount++;
		complianceEvaluationCount = 0;
		String complianceAssetName = buildComplianceAssetName(complianceAsset, complianceDetailsCount, complianceAssetCount);
		String complianceAssetDesc = buildComplianceAssetDescription(complianceAsset);
		
		ComplianceDetailsItem item = new ComplianceDetailsItem(complianceAssetName, complianceAssetDesc, TREE_LEVEL_ASSET);
		addItemToContainer(_hcResult, item, parentNode);
		
		return item;
	}

	@Override
	public ComplianceDetailsItem visitComplianceResource(ComplianceAssetResource complianceResource, ComplianceDetails rootCD, ComplianceDetailsItem parentNode) {
		return null;
	}
	
	@Override
	public ComplianceDetailsItem visitComplianceEvaluation(ComplianceAssetEvaluation complianceEvaluation, ComplianceDetails rootCD, ComplianceDetailsItem parentNode) {
		complianceEvaluationCount++;
		String complianceEvaluationName = buildComplianceEvaluationName(complianceEvaluation, complianceDetailsCount, 
				complianceAssetCount, complianceEvaluationCount);
		
		ComplianceDetailsItem evaluationItem = new ComplianceDetailsItem(complianceEvaluationName, "", TREE_LEVEL_EVAL);
		addItemToContainer(_hcResult, evaluationItem, parentNode);
		
		//LEVEL IV:
		String complianceEvaluationCriterionId = buildComplianceEvaluationCriterionId(complianceEvaluation);
		addEvaluationItemToContainer(_hcResult, complianceEvaluationCriterionId, evaluationItem);
		
		String complianceEvaluationCriterionName = buildComplianceEvaluationCriterionName(complianceEvaluation);
		addEvaluationItemToContainer(_hcResult, complianceEvaluationCriterionName, evaluationItem);
		
		String complianceEvaluationCriterionDescName = buildComplianceEvaluationCriterionDescName(complianceEvaluation);
		addEvaluationItemToContainer(_hcResult, complianceEvaluationCriterionDescName, evaluationItem);
		
		String complianceEvaluationResultName = buildComplianceEvaluationResultName(complianceEvaluation);
		addEvaluationItemToContainer(_hcResult, complianceEvaluationResultName, evaluationItem);

		String complianceEvaluationRationalName = buildComplianceEvaluationRationaleName(complianceEvaluation);
		addEvaluationItemToContainer(_hcResult, complianceEvaluationRationalName, evaluationItem);
		
		String complianceEvaluationEventName = buildComplianceEvaluationEventName(complianceEvaluation);
		addEvaluationItemToContainer(_hcResult, complianceEvaluationEventName, evaluationItem);
		
		return evaluationItem;
	}
	
	public HierarchicalContainer getResult() 
	{
		return _hcResult;
	}
	
	@SuppressWarnings("unchecked")
	private void mergePropertiesWithData(Item item, ComplianceDetailsItem complianceDetailsItem) {
		item.getItemProperty(_itemProperties[0]).setValue(complianceDetailsItem.getName());
	    item.getItemProperty(_itemProperties[1]).setValue(complianceDetailsItem.getDescription());
	    item.getItemProperty(_itemProperties[2]).setValue(complianceDetailsItem.getLevel());
	}
	
	private String buildComplianceJustificationName(ComplianceDetails complianceJustification, int complianceJustificationCount) {
		String complianceExplanation = complianceJustification.getComplianceMapJustification();
		String complianceType = complianceJustification.getComplianceMapType();
		String complianceTypeDesc = "";
		if ((MapKind.FULL.getName()).equals(complianceType)) {
			complianceTypeDesc = COMPLIANCE_TYPE_FULLY_DESC;
		} else if ((MapKind.PARTIAL.getName()).equals(complianceType)) {
			complianceTypeDesc = COMPLIANCE_TYPE_PARTIALLY_DESC;
		}
		String complianceExplanationItemId = complianceJustificationCount + ". " + complianceTypeDesc + " " + 
					COMPLIANCE_JUSTIFICATION + complianceJustificationCount + ": \"" + complianceExplanation + "\"";
		
		return complianceExplanationItemId;
	}

	private void addItemToContainer(HierarchicalContainer hierarchicalContainer, ComplianceDetailsItem childItem, ComplianceDetailsItem parentItem) {
		Item item = hierarchicalContainer.addItem(childItem);
		mergePropertiesWithData(item, childItem);
		String itemLevel = childItem.getLevel();
		
		if (!TREE_LEVEL_JUST.equals(itemLevel)) {
			hierarchicalContainer.setParent(childItem, parentItem);
		}
	}
	
	private String buildComplianceAssetName(ComplianceAsset complianceAsset, int complianceJustificationCount, int complianceAssetCount) {
		String complianceAssetName = "";
		if (complianceAsset.getName() != null) {
			complianceAssetName = complianceAsset.getName();
		}
		String complianceAssetType = complianceAsset.getType();
		String complianceAssetItemId = complianceJustificationCount + "." + complianceAssetCount +  "." + " " + 
					complianceAssetType + ": " + complianceAssetName;

		return complianceAssetItemId;
	}
	
	private String buildComplianceAssetDescription(ComplianceAsset complianceAsset) {
		String complianceAssetDesc = "<b>" + complianceAsset.getType() + " description:</b> <br>";
		
		if (complianceAsset.getDescription() != null) {
			complianceAssetDesc += complianceAsset.getDescription();
			complianceAssetDesc += "<br>";
		}
		if (complianceAsset.getComplianceAssetProperties() != null && complianceAsset.getComplianceAssetProperties().size() > 0) {
			if (!"".equals(complianceAssetDesc)) {
				complianceAssetDesc += "<br>";
			}
			complianceAssetDesc += "<b>" + complianceAsset.getType() + " properties:</b><br>";
			for (ComplianceAssetProperty complianceProperty : complianceAsset.getComplianceAssetProperties()) {
				complianceAssetDesc += "<b><i>name:</i></b> " + complianceProperty.getPropertyName() + 
						"; <b><i>value:</i></b> " + complianceProperty.getPropertyValue() + 
						"<br>";
			}
		}
		return complianceAssetDesc;
	}
	
	private void addEvaluationItemToContainer(HierarchicalContainer hierarchicalContainer, String childItemName, ComplianceDetailsItem parentItem) {
		ComplianceDetailsItem complianceDetailsItem = new ComplianceDetailsItem(childItemName, "", TREE_LEVEL_EVAL_CHILD);
		addItemToContainer(hierarchicalContainer, complianceDetailsItem, parentItem);
	}
	
	private String buildComplianceEvaluationName(ComplianceAssetEvaluation complianceEvaluation, int complianceJustificationCount, 
			int complianceAssetCount, int complianceEvaluationCount) {
		String complianceEvaluationName = "";
		if (complianceEvaluation.getName() != null) {
			complianceEvaluationName = complianceEvaluation.getName();
		}
		String complianceEvaluationItemId = complianceJustificationCount + "." + complianceAssetCount + "." + complianceEvaluationCount + ". " 
				+ "Evaluation " + complianceEvaluationCount + ": " + complianceEvaluationName;

		return complianceEvaluationItemId;
	}
	
	private String buildComplianceEvaluationCriterionId(ComplianceAssetEvaluation complianceEvaluation) {
		String id = "";
		if (complianceEvaluation.getId() != null) {
			id = complianceEvaluation.getId();
		}
		return "Id: " + id;
	}
	
	private String buildComplianceEvaluationCriterionName(ComplianceAssetEvaluation complianceEvaluation) {
		String criterion = "";
		if (complianceEvaluation.getCriterion() != null) {
			criterion = complianceEvaluation.getCriterion();
		}
		return "Criterion: " + criterion;
	}
	
	private String buildComplianceEvaluationCriterionDescName(ComplianceAssetEvaluation complianceEvaluation) {
		String criterionDesc = "";
		if (complianceEvaluation.getCriterionDescription() != null) {
			criterionDesc = complianceEvaluation.getCriterionDescription();
		}
		return "Criterion description: " + criterionDesc;
	}
	
	private String buildComplianceEvaluationResultName(ComplianceAssetEvaluation complianceEvaluation) {
		String result = "";
		if (complianceEvaluation.getResult() != null) {
			result = complianceEvaluation.getResult();
		}
		return "Evaluation Result: " + result;
	}
	
	private String buildComplianceEvaluationRationaleName(ComplianceAssetEvaluation complianceEvaluation) {
		String rationale = "";
		if (complianceEvaluation.getRationale() != null) {
			rationale = complianceEvaluation.getRationale();
		}
		return "Evaluation Rationale: " + rationale;
	}
	
	private String buildComplianceEvaluationEventName(ComplianceAssetEvaluation complianceEvaluation) {
		String evaluationEvent = "";
		if (complianceEvaluation.getEvaluationEvent() != null) {
			evaluationEvent = complianceEvaluation.getEvaluationEvent();
		}
		return "Evaluation Event: " + evaluationEvent;
	}
}
