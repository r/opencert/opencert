/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Applicability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getApplicCritic <em>Applic Critic</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getComments <em>Comments</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getApplicTarget <em>Applic Target</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getOwnedRel <em>Owned Rel</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseApplicability()
 * @model
 * @generated
 */
public interface BaseApplicability extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Applic Critic</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applic Critic</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applic Critic</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseApplicability_ApplicCritic()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseCriticalityApplicability> getApplicCritic();

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comments</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' attribute.
	 * @see #setComments(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseApplicability_Comments()
	 * @model
	 * @generated
	 */
	String getComments();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getComments <em>Comments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comments</em>' attribute.
	 * @see #getComments()
	 * @generated
	 */
	void setComments(String value);

	/**
	 * Returns the value of the '<em><b>Applic Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applic Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applic Target</em>' reference.
	 * @see #setApplicTarget(BaseAssurableElement)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseApplicability_ApplicTarget()
	 * @model
	 * @generated
	 */
	BaseAssurableElement getApplicTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getApplicTarget <em>Applic Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Applic Target</em>' reference.
	 * @see #getApplicTarget()
	 * @generated
	 */
	void setApplicTarget(BaseAssurableElement value);

	/**
	 * Returns the value of the '<em><b>Owned Rel</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Rel</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Rel</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseApplicability_OwnedRel()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseApplicabilityRel> getOwnedRel();

} // BaseApplicability
