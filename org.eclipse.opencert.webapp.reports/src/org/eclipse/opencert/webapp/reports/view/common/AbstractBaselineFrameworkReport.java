/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.containers.FrameworkWrapper;
import org.eclipse.opencert.webapp.reports.listeners.IProjectChangeListener;
import org.eclipse.opencert.webapp.reports.util.VaadinUtil;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

public abstract class AbstractBaselineFrameworkReport implements IProjectChangeListener, IReport
{
    private BeanItemContainer<FrameworkWrapper> frameworksContainer = new BeanItemContainer<FrameworkWrapper>(
            FrameworkWrapper.class, new LinkedList<FrameworkWrapper>());
    
    private ComboBox frameworksCombo = new ComboBox(null, frameworksContainer);
    
    private List<IBaselineFrameworkChangeListener> _baselineChangeListeners = null;

    public abstract ReportID getReportID();

    public abstract String getTitle();

    public abstract Component getMainComponent(GUIMode guiMode);
    
    public abstract Component getExportToDocxComponent();

    @Override
    public void projectChanged(AssuranceProjectWrapper newProject)
    {
        if (newProject == null) {
            return;
        }
        List<FrameworkWrapper> frameworks = newProject.getBaseFrameworks();
        setFrameworksCombo(frameworks);
        
    }

    @Override
    public boolean isSupportsProjects()
    {
        return true;
    }

    @SuppressWarnings("serial")
    public Component getParametersComponent(GUIMode guiMode)
    {
        HorizontalLayout horizontalPanel = new HorizontalLayout();
        horizontalPanel.setMargin(false);
        horizontalPanel.setSpacing(false);
        horizontalPanel.setSizeFull();
        frameworksCombo.setItemCaptionPropertyId("name");
        VaadinUtil.initComboBox(frameworksCombo);
        
        if (guiMode == GUIMode.NORMAL) 
        {
            frameworksCombo.setStyleName("baseFramework");
        } 
        else if (guiMode == GUIMode.COMPACT) 
        {
            frameworksCombo.setStyleName("baseFrameworkCompact");
        }
        
        frameworksCombo.addValueChangeListener(new ValueChangeListener()
        {
            @Override
            public void valueChange(ValueChangeEvent event)
            {
                refreshBaselineTableBasedOnFrameworkCombo();
            }
        });
        Label label = new Label("Baseline Framework: ");
        label.setStyleName("baselineFrameworkLabel");
        horizontalPanel.addComponent(label);
        horizontalPanel.addComponent(frameworksCombo);
        horizontalPanel.setStyleName("baselineFramework");
        
        final Component docxPanel = getExportToDocxComponent();
        if (docxPanel != null) {
        	horizontalPanel.addComponent(docxPanel);
        }
        
        if (guiMode == GUIMode.COMPACT) {
            // override some layout for COMPACT GUI mode
            label.setVisible(false);
            if (docxPanel != null) {
                docxPanel.setVisible(false);
            }
            frameworksCombo.setDescription("Baseline Framework");
        }
        return horizontalPanel;
    }

    private void setFrameworksCombo(List<FrameworkWrapper> frameworks)
    {
        frameworksContainer.removeAllItems();
        frameworksContainer.addAll(frameworks);
        final Collection<?> items = frameworksCombo.getItemIds();
        if (items != null && items.size() > 0) {
            frameworksCombo.select(items.iterator().next());
            frameworksCombo.setEnabled(true);
            frameworksCombo.setImmediate(true);
        } else {
            frameworksCombo.select(null);
            frameworksCombo.setEnabled(false);
            frameworksCombo.setImmediate(true);
        }
        // force refresh! - ugly fix - the framework combo listener is NOT
        // always invoked by the above .select()!!! it is not invoked when flow
        // goes from report Menu selection!
        refreshBaselineTableBasedOnFrameworkCombo();
    }


    protected void refreshBaselineTableBasedOnFrameworkCombo()
    {
        final FrameworkWrapper newBaseline = (FrameworkWrapper) frameworksCombo.getValue();
        if (newBaseline != null) {
            notifyBaselineChangeListeners(newBaseline.getId());
        } else {
        	notifyBaselineChangeListeners(new Long(-1));
        }
    }  
    
    private void notifyBaselineChangeListeners(Long baselineID) 
    {
    	if (baselineID == null) {
    		throw new IllegalArgumentException();
    	}
    	if (_baselineChangeListeners == null) {
    		return;
    	}
    	
    	_baselineChangeListeners.forEach((l) -> l.setBaselineFramework(baselineID.longValue())); 
	}
    

	protected void addBaselineChangeListener(IBaselineFrameworkChangeListener baselineFrameworkChangeListener)
    {
        if (_baselineChangeListeners == null) {
        	_baselineChangeListeners = new LinkedList<IBaselineFrameworkChangeListener>();
        }
        _baselineChangeListeners.add(baselineFrameworkChangeListener);
    }  
	
	
}
