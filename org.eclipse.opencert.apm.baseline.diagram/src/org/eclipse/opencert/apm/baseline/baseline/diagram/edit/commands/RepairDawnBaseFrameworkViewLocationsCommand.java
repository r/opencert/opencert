/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;
import org.eclipse.emf.internal.cdo.object.CDOLegacyAdapter;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.SetViewMutabilityCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.CompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.resources.GMFResourceFactory;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.Bounds;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.LayoutConstraint;
import org.eclipse.gmf.runtime.notation.Location;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.gmf.tooling.runtime.update.UpdaterLinkDescriptor;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivity2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseFrameworkEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramUpdater;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineLinkDescriptor;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineNodeDescriptor;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;
import org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * 
 * @author  M� Carmen Palacios
 */
/**
 * @generated NOT
 */
public class RepairDawnBaseFrameworkViewLocationsCommand extends AbstractTransactionalCommand {
		private LoadViewLocations sourceViewLocations;
		private Point offset;
		//private List<View> listViews;	
	    private List<EObject> listObjects;
        private View parentView;
        private EditPart host;

        /**
         * @generated NOT
         */
        public RepairDawnBaseFrameworkViewLocationsCommand (
        				LoadViewLocations sourceViewLocations,
        				Point offset,
        				List<EObject> listNodes,
                        TransactionalEditingDomain editingDomain, View parentView,
                        EditPart host) {
                super(editingDomain, "Repair location of views", getWorkspaceFiles(parentView));
                this.sourceViewLocations = sourceViewLocations;
                this.parentView = parentView;
                this.host = host;
                //this.listViews = findViews(listNodes);
                this.listObjects = listNodes;
                this.offset = offset;
        }
        
        private List<View> findViews(List<EObject> listNodes)
        {
        	List<View> res = new ArrayList();
        	
        	for(EObject elem : listNodes)
        	{
        		EditPart  editPart = lookForEditPart(elem);
        		View view = ((IGraphicalEditPart)editPart).getNotationView();
        		res.add(view);
        	}
        	return res;
        }
        
    	private EditPart lookForEditPart(EObject semantic) {
    		Collection<EditPart> editPartSet = ((IGraphicalEditPart) host).getViewer().getEditPartRegistry().values();
    		Iterator<EditPart> editPartIterator = editPartSet.iterator();
    		EditPart existedEditPart = null;
    		while(editPartIterator.hasNext() && existedEditPart == null) {
    			EditPart currentEditPart = editPartIterator.next();

    			if(isEditPartTypeAdapted(currentEditPart.getClass(), semantic.eClass()) && semantic.equals(((GraphicalEditPart)currentEditPart).resolveSemanticElement())) {
    				existedEditPart = currentEditPart;
    			}
    		}
    		return existedEditPart;
    	}

    	private boolean isEditPartTypeAdapted(Class<? extends EditPart> editPartClass, EClass eClass) {
    		if(DiagramEditPart.class.isAssignableFrom(editPartClass) || CompartmentEditPart.class.isAssignableFrom(editPartClass)) {
    			// the edit part is disqualified, as a compartment or a diagram can not be dropped
    			return false;
    		} else if(GraphicalEditPart.class.isAssignableFrom(editPartClass)) {
    			// check the edit part type against advised ones
    			return isEditPartTypeSuitableForEClass(editPartClass.asSubclass(GraphicalEditPart.class), eClass);
    		} else {
    			// only a GraphicalEditPart must be selected
    			return false;
    		}
    	}

    	protected boolean isEditPartTypeSuitableForEClass(Class<? extends GraphicalEditPart> editPartClass, EClass eClass) {
    		return true;
    	}
    	
        
        /**
         * @generated NOT
         */
        protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
                        IAdaptable info) throws ExecutionException {
	        	//if (listViews == null || listViews.isEmpty()) return  CommandResult.newOKCommandResult();
        		if (listObjects == null || listObjects.isEmpty()) return  CommandResult.newOKCommandResult();
	        	
        		BaseFrameworkRepairDawnViewLocationsCanonicalEditPolicy kk = new BaseFrameworkRepairDawnViewLocationsCanonicalEditPolicy();
                //kk.setViews(listViews);
	        	kk.setObjects(listObjects);
                kk.setHost(host);
                kk.setoffset(offset);
                //kk.LoadObjectLocations(sourceFile);
                kk.refresh();                
                kk.deactivate(); // para que no invoque refreshSemantic() al eliminar del diagrama uno de los objetos
                
                return CommandResult.newOKCommandResult();
        }
        
		/**
		 * @generated NOT
		 */
        public class   BaseFrameworkRepairDawnViewLocationsCanonicalEditPolicy extends CanonicalEditPolicy {
        	//private Map<EObject, String> eObjectToIDMap = null;
        	private Map<String, String> eObjectToIDMap = null;        	
        	private Point offset;
        	//private List<View> listViews = null;
        	private List<EObject> listRootObjects = null;
        	
        	
    		/**
    		 * @generated NOT
    		 */
        	public void setObjects(List<EObject> lro)
        	{
        		listRootObjects = lro;
        		for(EObject element : this.listRootObjects)
        		{
        			if(element instanceof BaseFrameworkImpl)
        				System.out.println("BaseFrameworkRepairDawnViewLocationsCanonicalEditPolicy.setObject(): Error object not allowed - " + element.getClass().getCanonicalName());
        		}
        	}
        	
    		/**
    		 * @generated NOT
    		 */
        	public void setoffset(Point offset)
        	{
        		this.offset = offset;
        	}

    		/**
    		 * @generated NOT
    		 */
        	protected IStatus changeLocations(List<View> nodel, Location offset) {
        		
        		for(View elem : nodel)
        		{
        			changeLocation((Node)elem, offset);
        		}
        		
    			return Status.OK_STATUS;        		
        	}

    		/**
    		 * @generated NOT
    		 */
    		private IStatus changeLocation(Node node, Location offset) { 
    			Location lc = (Location) node.getLayoutConstraint();
    			int x = lc.getX(); int y = lc.getY();
    			lc.setX(offset.getX()+x);
    			lc.setY(offset.getY()+y);
    			return Status.OK_STATUS;
    		}
    		

    		/**
    		 * @generated
    		 */
    		private Set<EStructuralFeature> myFeaturesToSynchronize;

    		/**
    		 * @generated
    		 */
    		protected void refreshOnActivate() {
    			// Need to activate editpart children before invoking the canonical refresh for EditParts to add event listeners
    			List<?> c = getHost().getChildren();
    			for (int i = 0; i < c.size(); i++) {
    				((EditPart) c.get(i)).activate();
    			}
    			super.refreshOnActivate();
    		}

    		/**
    		 * @generated
    		 */
    		protected Set getFeaturesToSynchronize() {
    			if (myFeaturesToSynchronize == null) {
    				myFeaturesToSynchronize = new HashSet<EStructuralFeature>();
    				myFeaturesToSynchronize.add(BaselinePackage.eINSTANCE
    						.getBaseFramework_OwnedActivities());
    				myFeaturesToSynchronize.add(BaselinePackage.eINSTANCE
    						.getBaseFramework_OwnedArtefact());
    				myFeaturesToSynchronize.add(BaselinePackage.eINSTANCE
    						.getBaseFramework_OwnedRole());
    			}
    			return myFeaturesToSynchronize;
    		}

    		/**
    		 * @generated
    		 */
    		@SuppressWarnings("rawtypes")
    		protected List getSemanticChildrenList() {
    			View viewObject = (View) getHost().getModel();
    			LinkedList<EObject> result = new LinkedList<EObject>();
    			List<BaselineNodeDescriptor> childDescriptors = BaselineDiagramUpdater
    					.getBaseFramework_1000SemanticChildren(viewObject);
    			for (BaselineNodeDescriptor d : childDescriptors) {
    				result.add(d.getModelElement());
    			}
    			return result;
    		}

    		/**
    		 * @generated
    		 */
    		protected boolean isOrphaned(Collection<EObject> semanticChildren,
    				final View view) {
    			if (isShortcut(view)) {
    				return BaselineDiagramUpdater.isShortcutOrphaned(view);
    			}
    			return isMyDiagramElement(view)
    					&& !semanticChildren.contains(view.getElement());
    		}

    		/**
    		 * @generated NOT
    		 */
    		private boolean isMyDiagramElement(View view) {
    			int visualID = BaselineVisualIDRegistry.getVisualID(view);
    			// Start MCP
    			boolean res2 = false;
    			if(view.getElement() != null)
    			{
    				DiagramImpl	diagramImpl =	(DiagramImpl)getHost().getModel();
    				BaseFrameworkImpl cs = (BaseFrameworkImpl) diagramImpl.basicGetElement();
    				CDOResource bb = (CDOResource) cs.eResource();

    				switch (visualID) {
    				case BaseActivityEditPart.VISUAL_ID:		
    				case BaseArtefactEditPart.VISUAL_ID:
    				case BaseRoleEditPart.VISUAL_ID:			
    					EObject me = (EObject)ViewUtil.resolveSemanticElement(view);
    					if(me != null) 
    					{
    						String meID = "-1";
    				        CDOID mecdoID = CDOUtil.getCDOObject(me).cdoID();
    						if (bb != null) meID = getID(mecdoID.toString());
    						
    						for(EObject elem : this.listRootObjects)
    						{
    							String rootObjectId = "-1";
    					        CDOID elemcdoID = CDOUtil.getCDOObject(elem).cdoID();						
    							rootObjectId = getID(elemcdoID.toString());
    							
    							if(rootObjectId == null) // Dawn cdo. a veces incluyo las relaciones y dan problemas y otras veces no
    							{
    								continue;
    							}
    							
    							if(meID!= null && meID.compareToIgnoreCase(rootObjectId) == 0 &&
    							   me.getClass().equals(elem.getClass())) // hay varios elementos con el mismo UUID
    							{
    								res2 = true;
    								break;
    							}
    						}
    					}
    				}
    			}

    			
    			switch (visualID) {
    			case BaseActivityEditPart.VISUAL_ID:		
    			case BaseArtefactEditPart.VISUAL_ID:
    			case BaseRoleEditPart.VISUAL_ID:			
    				return true && res2;
    			}
    		
    			return false;
    			/*
    			return visualID == BaseActivityEditPart.VISUAL_ID
    					|| visualID == BaseArtefactEditPart.VISUAL_ID
    					|| visualID == BaseRoleEditPart.VISUAL_ID;
    	 
    			 */
    			// End MCP
    		}

    		/**
    		 * @generated
    		 */
    		protected boolean isShortcut(View view) {
    			return view.getEAnnotation("Shortcut") != null; //$NON-NLS-1$
    		}

    		/**
    		 * @generated NOT
    		 */
    		// Start MCP
    		private void setIDs() {
    			eObjectToIDMap = new HashMap<String, String>();
    			
    			List<EObject> result = getSemanticChildrenList();
    			
    			for(EObject elem : result)
    			{
    	    		// Si CDO Native
    		        CDOID cdoID = CDOUtil.getCDOObject(elem).cdoID();
    		        Long  cdoIdLong = new Long(cdoID.toURIFragment());
    				//Parece que cambian los objetos por su estado!!!
    				//eObjectToIDMap.put(elem, cdoID.toString());
    		        eObjectToIDMap.put(cdoID.toString(), cdoID.toString());
    		        NamedElement me = (NamedElement)elem;
    				System.out.println("key = "+ me.getId()+"|"+me.getName()+"|"+me.getClass().getName() + ". (" + cdoID.toString() + ")");    					
    				//eObjectToIDMap.put(elem, me.getId()+me.getName()+me.eClass().getName());
    			}
    		}	
    		// End MCP	
    		
    		/**
    		 * @generated NOT
    		 */
    		// Start MCP	
    		private String getID(String eObjectID) {
    		    if (eObjectToIDMap == null)
    		    {
    		      return null;
    		    }
    		    else
    		    {
    		      return eObjectToIDMap.get(eObjectID);
    		    }
    		  }
    		// End MCP
    		
    		
    		/**
    		 * @generated NOT
    		 */
    		protected void refreshSemantic() {
    			if (resolveSemanticElement() == null) {
    				return;
    			}
    			LinkedList<IAdaptable> createdViews = new LinkedList<IAdaptable>();
    			List<BaselineNodeDescriptor> childDescriptors = BaselineDiagramUpdater
    					.getBaseFramework_1000SemanticChildren((View) getHost()
    							.getModel());
    			// Start MCP
    			//eliminar los hijos que no son de este diagram
    			DiagramImpl	diagramImpl =	(DiagramImpl)getHost().getModel();
    			BaseFrameworkImpl cs = (BaseFrameworkImpl) diagramImpl.basicGetElement();
    			CDOResource bb =(CDOResource) cs.eResource();
    			//setIDs((CaseImpl)bb.eContents().get(0));
    			setIDs();
    			
    			Set<String> rootObjectIds = new HashSet<String>();
    			for(EObject elem : this.listRootObjects)
    			{
    				if(false ==  elem instanceof BaseFrameworkImpl)
    				{
    					String rootObjectId = "-1";
    			        CDOID elemcdoID = CDOUtil.getCDOObject(elem).cdoID();				
    					rootObjectId = getID(elemcdoID.toString()); 
    					rootObjectIds.add(rootObjectId);
    				}
    			}
    					
    			LinkedList<BaselineNodeDescriptor> c = new LinkedList<BaselineNodeDescriptor>();
    			for(BaselineNodeDescriptor next : childDescriptors)
    			{
    				EObject me = (EObject)next.getModelElement();
    				if(me != null) 
    				{
    					String meID = "-1";
    					CDOID mecdoID = CDOUtil.getCDOObject(me).cdoID();
    					meID = getID(mecdoID.toString());
    					
    					if(meID == null || !rootObjectIds.contains(meID))
    					{
    						c.add(next);
    					}
    					else if (meID != null && rootObjectIds.contains(meID))
    					{
    						boolean OK = false;
    						for(EObject elem : this.listRootObjects) // hay varios elementos con el mismo UUID
    						{
    							if(me.getClass().equals(elem.getClass()))
    							{
    								OK = true;
    								break;
    							}
    						}
    						if(!OK) c.add(next);
    					}
    				}			
    			}
    			childDescriptors.removeAll(c);
    			//End MCP
    			LinkedList<View> orphaned = new LinkedList<View>();
    			// we care to check only views we recognize as ours and not shortcuts
    			LinkedList<View> knownViewChildren = new LinkedList<View>();
    			for (View v : getViewChildren()) {
    				if (isShortcut(v)) {
    					if (BaselineDiagramUpdater.isShortcutOrphaned(v)) {
    						orphaned.add(v);
    					}
    					continue;
    				}
    				if (isMyDiagramElement(v)) {
    					knownViewChildren.add(v);
    				}
    			}
    			// Start MCP
    			/*
    			// alternative to #cleanCanonicalSemanticChildren(getViewChildren(), semanticChildren)
    			//
    			// iteration happens over list of desired semantic elements, trying to find best matching View, while original CEP
    			// iterates views, potentially losing view (size/bounds) information - i.e. if there are few views to reference same EObject, only last one 
    			// to answer isOrphaned == true will be used for the domain element representation, see #cleanCanonicalSemanticChildren()
				for (Iterator<BaselineNodeDescriptor> descriptorsIterator = childDescriptors
						.iterator(); descriptorsIterator.hasNext();) {
					BaselineNodeDescriptor next = descriptorsIterator.next();
					String hint = BaselineVisualIDRegistry.getType(next.getVisualID());
					LinkedList<View> perfectMatch = new LinkedList<View>(); // both semanticElement and hint match that of NodeDescriptor
					for (View childView : getViewChildren()) {
						EObject semanticElement = childView.getElement();
						if (next.getModelElement().equals(semanticElement)) {
							if (hint.equals(childView.getType())) {
								perfectMatch.add(childView);
								// actually, can stop iteration over view children here, but
								// may want to use not the first view but last one as a 'real' match (the way original CEP does
								// with its trick with viewToSemanticMap inside #cleanCanonicalSemanticChildren
							}
						}
					}
					if (perfectMatch.size() > 0) {
						descriptorsIterator.remove(); // precise match found no need to create anything for the NodeDescriptor
						// use only one view (first or last?), keep rest as orphaned for further consideration
						knownViewChildren.remove(perfectMatch.getFirst());
					}
				}
    			*/
    			// End MCP
    			// those left in knownViewChildren are subject to removal - they are our diagram elements we didn't find match to,
    			// or those we have potential matches to, and thus need to be recreated, preserving size/location information.
    			orphaned.addAll(knownViewChildren);
    			//
    			ArrayList<CreateViewRequest.ViewDescriptor> viewDescriptors = new ArrayList<CreateViewRequest.ViewDescriptor>(
    					childDescriptors.size());
    			for (BaselineNodeDescriptor next : childDescriptors) {
    				String hint = BaselineVisualIDRegistry.getType(next.getVisualID());
    				IAdaptable elementAdapter = new CanonicalElementAdapter(
    						next.getModelElement(), hint);
    				CreateViewRequest.ViewDescriptor descriptor = new CreateViewRequest.ViewDescriptor(
    						elementAdapter, Node.class, hint, ViewUtil.APPEND, false,
    						host().getDiagramPreferencesHint());
    				viewDescriptors.add(descriptor);
    			}

    			boolean changed = deleteViews(orphaned.iterator());
    			// Start MCP
    			for(ViewDescriptor oneviewDescriptor : viewDescriptors)
    			{
	    			//CreateViewRequest request = getCreateViewRequest(viewDescriptors);
    				List<ViewDescriptor> oneviewDescriptorl = new ArrayList(1); 
    				oneviewDescriptorl.add(oneviewDescriptor);
    				CreateViewRequest request = getCreateViewRequest(oneviewDescriptorl);
    				NamedElement me = (NamedElement)oneviewDescriptor.getElementAdapter().getAdapter(NamedElement.class);
    				//Point newlocation = objectLocations.get(me.getId());
					// hay varios elementos con el mismo UUID	
    				/* PROBLEMA los nuevos ficheros (diagrama y modelo tienen IDs muy diferentes!!!!)
    				System.out.println("key = "+ aa.getID(me)+ me.getClass().getName());
    				Point newlocation = objectLocations.get(aa.getID(me)+ me.getClass().getName());
    				*/
    				/* Dawn cdo. Esto deber�a cambiarlo si lo necesito al cambiar esta funcion
    				String objectId = ((XMLResource) me.eResource()).getID(me);
    				*/
    				System.out.println("key = "+ me.getId()+me.getName()+me.getClass().getName());
    				/*
    				Point newlocation = objectLocations.get(me.getId()+me.getName()+me.getClass().getName());
    				int x = newlocation.x(); int y  = newlocation.y();
    				*/
    				Bounds lc = (Bounds) sourceViewLocations.getObjectLocation(me.getId()+me.getName()+me.eClass().getName());
					//MCP: OJO los UUIDs han cambiado porque he copiado los modelos y ademas dan problemas las superclases en el caso de AgreementImpl!!!!
					// solo si guardo traza transformacion podre hacerlo correctamente
    				int x = 0; int y  = 0;
    				if(lc != null)
    				{
    					x = lc.getX(); y  = lc.getY();
    	    			Dimension newSize = new Dimension(lc.getWidth(), lc.getHeight());
    	    			request.setSize(newSize);
    				}
    				Point newlocation = new Point();
    				newlocation.setX(offset.x()+x);
    				newlocation.setY(offset.y()+y);
	    			request.setLocation(newlocation);
	    			Command cmd = getCreateViewCommand(request);
	    			if (cmd != null && cmd.canExecute()) {
	    				SetViewMutabilityCommand.makeMutable(
	    						new EObjectAdapter(host().getNotationView())).execute();
	    				executeCommand(cmd);
	    				@SuppressWarnings("unchecked")
	    				List<IAdaptable> nl = (List<IAdaptable>) request.getNewObject();
	    				createdViews.addAll(nl);
	    			}
    			}
    			// End MCP
    			if (changed || createdViews.size() > 0) {
    				postProcessRefreshSemantic(createdViews);
    			}

    			Collection<IAdaptable> createdConnectionViews = refreshConnections();

    			/*MCP
    			if (createdViews.size() > 1) {
    				// perform a layout of the container
    				DeferredLayoutCommand layoutCmd = new DeferredLayoutCommand(host()
    						.getEditingDomain(), createdViews, host());
    				executeCommand(new ICommandProxy(layoutCmd));
    			}
    			*/

    			createdViews.addAll(createdConnectionViews);

    			makeViewsImmutable(createdViews);
    		}

    		/**
    		 * @generated
    		 */
    		private Collection<IAdaptable> refreshConnections() {
    			Domain2Notation domain2NotationMap = new Domain2Notation();
    			Collection<BaselineLinkDescriptor> linkDescriptors = collectAllLinks(
    					getDiagram(), domain2NotationMap);
    			Collection existingLinks = new LinkedList(getDiagram().getEdges());
    			for (Iterator linksIterator = existingLinks.iterator(); linksIterator
    					.hasNext();) {
    				Edge nextDiagramLink = (Edge) linksIterator.next();
    				int diagramLinkVisualID = BaselineVisualIDRegistry
    						.getVisualID(nextDiagramLink);
    				if (diagramLinkVisualID == -1) {
    					if (nextDiagramLink.getSource() != null
    							&& nextDiagramLink.getTarget() != null) {
    						linksIterator.remove();
    					}
    					continue;
    				}
    				EObject diagramLinkObject = nextDiagramLink.getElement();
    				EObject diagramLinkSrc = nextDiagramLink.getSource().getElement();
    				EObject diagramLinkDst = nextDiagramLink.getTarget().getElement();
    				for (Iterator<BaselineLinkDescriptor> linkDescriptorsIterator = linkDescriptors
    						.iterator(); linkDescriptorsIterator.hasNext();) {
    					BaselineLinkDescriptor nextLinkDescriptor = linkDescriptorsIterator
    							.next();
    					if (diagramLinkObject == nextLinkDescriptor.getModelElement()
    							&& diagramLinkSrc == nextLinkDescriptor.getSource()
    							&& diagramLinkDst == nextLinkDescriptor
    									.getDestination()
    							&& diagramLinkVisualID == nextLinkDescriptor
    									.getVisualID()) {
    						linksIterator.remove();
    						linkDescriptorsIterator.remove();
    						break;
    					}
    				}
    			}
    			deleteViews(existingLinks.iterator());
    			return createConnections(linkDescriptors, domain2NotationMap);
    		}

    		/**
    		 * @generated
    		 */
    		private Collection<BaselineLinkDescriptor> collectAllLinks(View view,
    				Domain2Notation domain2NotationMap) {
    			if (!BaseFrameworkEditPart.MODEL_ID.equals(BaselineVisualIDRegistry
    					.getModelID(view))) {
    				return Collections.emptyList();
    			}
    			LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
    			switch (BaselineVisualIDRegistry.getVisualID(view)) {
    			case BaseFrameworkEditPart.VISUAL_ID: {
    				if (!domain2NotationMap.containsKey(view.getElement())) {
    					result.addAll(BaselineDiagramUpdater
    							.getBaseFramework_1000ContainedLinks(view));
    				}
    				domain2NotationMap.putView(view.getElement(), view);
    				break;
    			}
    			case BaseActivityEditPart.VISUAL_ID: {
    				if (!domain2NotationMap.containsKey(view.getElement())) {
    					result.addAll(BaselineDiagramUpdater
    							.getBaseActivity_2001ContainedLinks(view));
    				}
    				domain2NotationMap.putView(view.getElement(), view);
    				break;
    			}
    			case BaseArtefactEditPart.VISUAL_ID: {
    				if (!domain2NotationMap.containsKey(view.getElement())) {
    					result.addAll(BaselineDiagramUpdater
    							.getBaseArtefact_2002ContainedLinks(view));
    				}
    				domain2NotationMap.putView(view.getElement(), view);
    				break;
    			}
    			case BaseRoleEditPart.VISUAL_ID: {
    				if (!domain2NotationMap.containsKey(view.getElement())) {
    					result.addAll(BaselineDiagramUpdater
    							.getBaseRole_2003ContainedLinks(view));
    				}
    				domain2NotationMap.putView(view.getElement(), view);
    				break;
    			}
    			case BaseActivity2EditPart.VISUAL_ID: {
    				if (!domain2NotationMap.containsKey(view.getElement())) {
    					result.addAll(BaselineDiagramUpdater
    							.getBaseActivity_3001ContainedLinks(view));
    				}
    				domain2NotationMap.putView(view.getElement(), view);
    				break;
    			}
    			}
    			for (Iterator children = view.getChildren().iterator(); children
    					.hasNext();) {
    				result.addAll(collectAllLinks((View) children.next(),
    						domain2NotationMap));
    			}
    			for (Iterator edges = view.getSourceEdges().iterator(); edges.hasNext();) {
    				result.addAll(collectAllLinks((View) edges.next(),
    						domain2NotationMap));
    			}
    			return result;
    		}

    		/**
    		 * @generated
    		 */
    		private Collection<IAdaptable> createConnections(
    				Collection<BaselineLinkDescriptor> linkDescriptors,
    				Domain2Notation domain2NotationMap) {
    			LinkedList<IAdaptable> adapters = new LinkedList<IAdaptable>();
    			for (BaselineLinkDescriptor nextLinkDescriptor : linkDescriptors) {
    				EditPart sourceEditPart = getSourceEditPart(nextLinkDescriptor,
    						domain2NotationMap);
    				EditPart targetEditPart = getTargetEditPart(nextLinkDescriptor,
    						domain2NotationMap);
    				if (sourceEditPart == null || targetEditPart == null) {
    					continue;
    				}
    				CreateConnectionViewRequest.ConnectionViewDescriptor descriptor = new CreateConnectionViewRequest.ConnectionViewDescriptor(
    						nextLinkDescriptor.getSemanticAdapter(),
    						BaselineVisualIDRegistry.getType(nextLinkDescriptor
    								// Start MCP
    								// hacer persistentes los edges
    								//.getVisualID()), ViewUtil.APPEND, false,
    								.getVisualID()), ViewUtil.APPEND, true,
    								// End MCP
    						((IGraphicalEditPart) getHost())
    								.getDiagramPreferencesHint());
    				CreateConnectionViewRequest ccr = new CreateConnectionViewRequest(
    						descriptor);
    				ccr.setType(RequestConstants.REQ_CONNECTION_START);
    				ccr.setSourceEditPart(sourceEditPart);
    				sourceEditPart.getCommand(ccr);
    				ccr.setTargetEditPart(targetEditPart);
    				ccr.setType(RequestConstants.REQ_CONNECTION_END);
    				Command cmd = targetEditPart.getCommand(ccr);
    				if (cmd != null && cmd.canExecute()) {
    					executeCommand(cmd);
    					IAdaptable viewAdapter = (IAdaptable) ccr.getNewObject();
    					if (viewAdapter != null) {
    						adapters.add(viewAdapter);
    					}
    				}
    			}
    			return adapters;
    		}

    		/**
    		 * @generated
    		 */
    		private EditPart getEditPart(EObject domainModelElement,
    				Domain2Notation domain2NotationMap) {
    			View view = (View) domain2NotationMap.get(domainModelElement);
    			if (view != null) {
    				return (EditPart) getHost().getViewer().getEditPartRegistry()
    						.get(view);
    			}
    			return null;
    		}

    		/**
    		 * @generated
    		 */
    		private Diagram getDiagram() {
    			return ((View) getHost().getModel()).getDiagram();
    		}

    		/**
    		 * @generated
    		 */
    		private EditPart getSourceEditPart(UpdaterLinkDescriptor descriptor,
    				Domain2Notation domain2NotationMap) {
    			return getEditPart(descriptor.getSource(), domain2NotationMap);
    		}

    		/**
    		 * @generated
    		 */
    		private EditPart getTargetEditPart(UpdaterLinkDescriptor descriptor,
    				Domain2Notation domain2NotationMap) {
    			return getEditPart(descriptor.getDestination(), domain2NotationMap);
    		}

    		/**
    		 * @generated
    		 */
    		protected final EditPart getHintedEditPart(EObject domainModelElement,
    				Domain2Notation domain2NotationMap, int hintVisualId) {
    			View view = (View) domain2NotationMap.getHinted(domainModelElement,
    					BaselineVisualIDRegistry.getType(hintVisualId));
    			if (view != null) {
    				return (EditPart) getHost().getViewer().getEditPartRegistry()
    						.get(view);
    			}
    			return null;
    		}

    		/**
    		 * @generated
    		 */
    		@SuppressWarnings("serial")
    		protected class Domain2Notation extends HashMap<EObject, View> {
    			/**
    			 * @generated
    			 */
    			public boolean containsDomainElement(EObject domainElement) {
    				return this.containsKey(domainElement);
    			}

    			/**
    			 * @generated
    			 */
    			public View getHinted(EObject domainEObject, String hint) {
    				return this.get(domainEObject);
    			}

    			/**
    			 * @generated
    			 */
    			public void putView(EObject domainElement, View view) {
    				if (!containsKey(view.getElement()) || !isShortcut(view)) {
    					this.put(domainElement, view);
    				}
    			}

    		}
    	}
        
}
