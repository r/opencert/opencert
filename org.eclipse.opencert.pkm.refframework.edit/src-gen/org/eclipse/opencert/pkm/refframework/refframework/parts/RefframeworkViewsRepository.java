/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts;

/**
 * 
 * 
 */
public class RefframeworkViewsRepository {

	public static final int SWT_KIND = 0;

	public static final int FORM_KIND = 1;


	/**
	 * RefFramework view descriptor
	 * 
	 */
	public static class RefFramework_ {
		public static class Properties {
	
			
			public static String id = "refframework::RefFramework_::properties::id";
			
			
			public static String name = "refframework::RefFramework_::properties::name";
			
			
			public static String description = "refframework::RefFramework_::properties::description";
			
			
			public static String scope = "refframework::RefFramework_::properties::scope";
			
			
			public static String rev = "refframework::RefFramework_::properties::rev";
			
			
			public static String purpose = "refframework::RefFramework_::properties::purpose";
			
			
			public static String publisher = "refframework::RefFramework_::properties::publisher";
			
			
			public static String issued = "refframework::RefFramework_::properties::issued";
			
			
			public static String ownedActivities = "refframework::RefFramework_::properties::ownedActivities";
			
			
			public static String ownedArtefact = "refframework::RefFramework_::properties::ownedArtefact";
			
			
			public static String ownedRequirement = "refframework::RefFramework_::properties::ownedRequirement";
			
			
			public static String ownedApplicLevel = "refframework::RefFramework_::properties::ownedApplicLevel";
			
			
			public static String ownedCriticLevel = "refframework::RefFramework_::properties::ownedCriticLevel";
			
			
			public static String ownedRole = "refframework::RefFramework_::properties::ownedRole";
			
			
			public static String ownedTechnique = "refframework::RefFramework_::properties::ownedTechnique";
			
	
		}
	
	}

	/**
	 * RefRequirement view descriptor
	 * 
	 */
	public static class RefRequirement {
		public static class Properties {
	
			
			public static String id = "refframework::RefRequirement::properties::id";
			
			
			public static String name = "refframework::RefRequirement::properties::name";
			
			
			public static String description = "refframework::RefRequirement::properties::description";
			
			
			public static String reference = "refframework::RefRequirement::properties::reference";
			
			
			public static String assumptions = "refframework::RefRequirement::properties::assumptions";
			
			
			public static String rationale = "refframework::RefRequirement::properties::rationale";
			
			
			public static String image = "refframework::RefRequirement::properties::image";
			
			
			public static String annotations = "refframework::RefRequirement::properties::annotations";
			
			
			public static String ownedRel = "refframework::RefRequirement::properties::ownedRel";
			
			
			public static String subRequirement = "refframework::RefRequirement::properties::subRequirement";
			
	
		}
	
	}

	/**
	 * RefArtefact view descriptor
	 * 
	 */
	public static class RefArtefact {
		public static class Properties {
	
			
			public static String id = "refframework::RefArtefact::properties::id";
			
			
			public static String name = "refframework::RefArtefact::properties::name";
			
			
			public static String description = "refframework::RefArtefact::properties::description";
			
			
			public static String reference = "refframework::RefArtefact::properties::reference";
			
			
			public static String constrainingRequirement = "refframework::RefArtefact::properties::constrainingRequirement";
			
			
			public static String applicableTechnique = "refframework::RefArtefact::properties::applicableTechnique";
			
			
			public static String ownedRel = "refframework::RefArtefact::properties::ownedRel";
			
			
			public static String property = "refframework::RefArtefact::properties::property";
			
	
		}
	
	}

	/**
	 * RefActivity view descriptor
	 * 
	 */
	public static class RefActivity {
		public static class Properties {
	
			
			public static String id = "refframework::RefActivity::properties::id";
			
			
			public static String name = "refframework::RefActivity::properties::name";
			
			
			public static String description = "refframework::RefActivity::properties::description";
			
			
			public static String objective = "refframework::RefActivity::properties::objective";
			
			
			public static String scope = "refframework::RefActivity::properties::scope";
			
			
			public static String requiredArtefact = "refframework::RefActivity::properties::requiredArtefact";
			
			
			public static String producedArtefact = "refframework::RefActivity::properties::producedArtefact";
			
			
			public static String subActivity = "refframework::RefActivity::properties::subActivity";
			
			
			public static String precedingActivity = "refframework::RefActivity::properties::precedingActivity";
			
			
			public static String role = "refframework::RefActivity::properties::role";
			
			
			public static String applicableTechnique = "refframework::RefActivity::properties::applicableTechnique";
			
			
			public static String ownedRel = "refframework::RefActivity::properties::ownedRel";
			
	
		}
	
	}

	/**
	 * RefRequirementRel view descriptor
	 * 
	 */
	public static class RefRequirementRel {
		public static class Properties {
	
			
			public static String target = "refframework::RefRequirementRel::properties::target";
			
			
			public static String source = "refframework::RefRequirementRel::properties::source";
			
			
			public static String type = "refframework::RefRequirementRel::properties::type";
			
	
		}
	
	}

	/**
	 * RefRole view descriptor
	 * 
	 */
	public static class RefRole {
		public static class Properties {
	
			
			public static String id = "refframework::RefRole::properties::id";
			
			
			public static String name = "refframework::RefRole::properties::name";
			
			
			public static String description = "refframework::RefRole::properties::description";
			
	
		}
	
	}

	/**
	 * RefApplicabilityLevel view descriptor
	 * 
	 */
	public static class RefApplicabilityLevel {
		public static class Properties {
	
			
			public static String id = "refframework::RefApplicabilityLevel::properties::id";
			
			
			public static String name = "refframework::RefApplicabilityLevel::properties::name";
			
			
			public static String description = "refframework::RefApplicabilityLevel::properties::description";
			
	
		}
	
	}

	/**
	 * RefCriticalityLevel view descriptor
	 * 
	 */
	public static class RefCriticalityLevel {
		public static class Properties {
	
			
			public static String id = "refframework::RefCriticalityLevel::properties::id";
			
			
			public static String name = "refframework::RefCriticalityLevel::properties::name";
			
			
			public static String description = "refframework::RefCriticalityLevel::properties::description";
			
	
		}
	
	}

	/**
	 * RefTechnique view descriptor
	 * 
	 */
	public static class RefTechnique {
		public static class Properties {
	
			
			public static String id = "refframework::RefTechnique::properties::id";
			
			
			public static String name = "refframework::RefTechnique::properties::name";
			
			
			public static String description = "refframework::RefTechnique::properties::description";
			
			
			public static String criticApplic = "refframework::RefTechnique::properties::criticApplic";
			
			
			public static String aim = "refframework::RefTechnique::properties::aim";
			
	
		}
	
	}

	/**
	 * RefArtefactRel view descriptor
	 * 
	 */
	public static class RefArtefactRel {
		public static class Properties {
	
			
			public static String id = "refframework::RefArtefactRel::properties::id";
			
			
			public static String name = "refframework::RefArtefactRel::properties::name";
			
			
			public static String description = "refframework::RefArtefactRel::properties::description";
			
			
			public static String maxMultiplicitySource = "refframework::RefArtefactRel::properties::maxMultiplicitySource";
			
			
			public static String minMultiplicitySource = "refframework::RefArtefactRel::properties::minMultiplicitySource";
			
			
			public static String maxMultiplicityTarget = "refframework::RefArtefactRel::properties::maxMultiplicityTarget";
			
			
			public static String minMultiplicityTarget = "refframework::RefArtefactRel::properties::minMultiplicityTarget";
			
			
			public static String modificationEffect = "refframework::RefArtefactRel::properties::modificationEffect";
			
			
			public static String revocationEffect = "refframework::RefArtefactRel::properties::revocationEffect";
			
			
			public static String source = "refframework::RefArtefactRel::properties::source";
			
			
			public static String target = "refframework::RefArtefactRel::properties::target";
			
	
		}
	
	}

	/**
	 * RefCriticalityApplicability view descriptor
	 * 
	 */
	public static class RefCriticalityApplicability {
		public static class Properties {
	
			
			public static String applicLevel = "refframework::RefCriticalityApplicability::properties::applicLevel";
			
			
			public static String applicLevelCombo = "refframework::RefCriticalityApplicability::properties::applicLevelCombo";
			
			
			public static String criticLevel = "refframework::RefCriticalityApplicability::properties::criticLevel";
			
			
			public static String criticLevelCombo = "refframework::RefCriticalityApplicability::properties::criticLevelCombo";
			
			
			public static String comment = "refframework::RefCriticalityApplicability::properties::comment";
			
	
		}
	
	}

	/**
	 * RefActivityRel view descriptor
	 * 
	 */
	public static class RefActivityRel {
		public static class Properties {
	
			
			public static String type = "refframework::RefActivityRel::properties::type";
			
			
			public static String source = "refframework::RefActivityRel::properties::source";
			
			
			public static String target = "refframework::RefActivityRel::properties::target";
			
	
		}
	
	}

	/**
	 * RefIndependencyLevel view descriptor
	 * 
	 */
	public static class RefIndependencyLevel {
		public static class Properties {
	
			
			public static String id = "refframework::RefIndependencyLevel::properties::id";
			
			
			public static String name = "refframework::RefIndependencyLevel::properties::name";
			
			
			public static String description = "refframework::RefIndependencyLevel::properties::description";
			
	
		}
	
	}

	/**
	 * RefRecommendationLevel view descriptor
	 * 
	 */
	public static class RefRecommendationLevel {
		public static class Properties {
	
			
			public static String id = "refframework::RefRecommendationLevel::properties::id";
			
			
			public static String name = "refframework::RefRecommendationLevel::properties::name";
			
			
			public static String description = "refframework::RefRecommendationLevel::properties::description";
			
	
		}
	
	}

	/**
	 * RefControlCategory view descriptor
	 * 
	 */
	public static class RefControlCategory {
		public static class Properties {
	
			
			public static String id = "refframework::RefControlCategory::properties::id";
			
			
			public static String name = "refframework::RefControlCategory::properties::name";
			
			
			public static String description = "refframework::RefControlCategory::properties::description";
			
	
		}
	
	}

	/**
	 * RefApplicability view descriptor
	 * 
	 */
	public static class RefApplicability {
		public static class Properties {
	
			
			public static String id = "refframework::RefApplicability::properties::id";
			
			
			public static String name = "refframework::RefApplicability::properties::name";
			
			
			public static String applicCritic = "refframework::RefApplicability::properties::applicCritic";
			
			
			public static String applicCriticTable = "refframework::RefApplicability::properties::applicCriticTable";
			
			
			public static String comments = "refframework::RefApplicability::properties::comments";
			
			
			public static String applicTarget = "refframework::RefApplicability::properties::applicTarget";
			
			
			public static String ownedRel = "refframework::RefApplicability::properties::ownedRel";
			
	
		}
	
	}

	/**
	 * RefApplicabilityRel view descriptor
	 * 
	 */
	public static class RefApplicabilityRel {
		public static class Properties {
	
			
			public static String type = "refframework::RefApplicabilityRel::properties::type";
			
			
			public static String source = "refframework::RefApplicabilityRel::properties::source";
			
			
			public static String target = "refframework::RefApplicabilityRel::properties::target";
			
	
		}
	
	}

	/**
	 * RefEquivalenceMap view descriptor
	 * 
	 */
	public static class RefEquivalenceMap {
		public static class Properties {
	
			
			public static String id = "refframework::RefEquivalenceMap::properties::id";
			
			
			public static String name = "refframework::RefEquivalenceMap::properties::name";
			
			
			public static String mapGroup = "refframework::RefEquivalenceMap::properties::mapGroup";
			
			
			public static String mapGroupCombo = "refframework::RefEquivalenceMap::properties::mapGroupCombo";
			
			
			public static String type = "refframework::RefEquivalenceMap::properties::type";
			
			
			public static String justification = "refframework::RefEquivalenceMap::properties::justification";
			
			
			public static String target = "refframework::RefEquivalenceMap::properties::target";
			
	
		}
	
	}

	/**
	 * ActivityRequirement view descriptor
	 * 
	 */
	public static class ActivityRequirement {
		public static class Properties {
	
			
			public static String ownedRequirement = "refframework::ActivityRequirement::properties::ownedRequirement";
			
	
		}
	
	}

	/**
	 * ActivityApplicability view descriptor
	 * 
	 */
	public static class ActivityApplicability {
		public static class Properties {
	
			
			public static String applicability = "refframework::ActivityApplicability::properties::applicability";
			
			
			public static String applicabilityTable = "refframework::ActivityApplicability::properties::applicabilityTable";
			
	
		}
	
	}

	/**
	 * ActivityEquivalenceMap view descriptor
	 * 
	 */
	public static class ActivityEquivalenceMap {
		public static class Properties {
	
			
			public static String equivalence = "refframework::ActivityEquivalenceMap::properties::equivalence";
			
			
			public static String activityEquivalenceMap_ = "refframework::ActivityEquivalenceMap::properties::activityEquivalenceMap_";
			
	
		}
	
	}

	/**
	 * RequirementApplicability view descriptor
	 * 
	 */
	public static class RequirementApplicability {
		public static class Properties {
	
			
			public static String applicability = "refframework::RequirementApplicability::properties::applicability";
			
			
			public static String applicabilityTable = "refframework::RequirementApplicability::properties::applicabilityTable";
			
	
		}
	
	}

	/**
	 * RequirementEquivalenceMap view descriptor
	 * 
	 */
	public static class RequirementEquivalenceMap {
		public static class Properties {
	
			
			public static String equivalence = "refframework::RequirementEquivalenceMap::properties::equivalence";
			
			
			public static String requirementEquivalenceMap_ = "refframework::RequirementEquivalenceMap::properties::requirementEquivalenceMap_";
			
	
		}
	
	}

	/**
	 * ArtefactEquivalenceMap view descriptor
	 * 
	 */
	public static class ArtefactEquivalenceMap {
		public static class Properties {
	
			
			public static String equivalence = "refframework::ArtefactEquivalenceMap::properties::equivalence";
			
			
			public static String artefactEquivalenceMap_ = "refframework::ArtefactEquivalenceMap::properties::artefactEquivalenceMap_";
			
	
		}
	
	}

	/**
	 * RoleEquivalenceMap view descriptor
	 * 
	 */
	public static class RoleEquivalenceMap {
		public static class Properties {
	
			
			public static String equivalence = "refframework::RoleEquivalenceMap::properties::equivalence";
			
			
			public static String roleEquivalenceMap_ = "refframework::RoleEquivalenceMap::properties::roleEquivalenceMap_";
			
	
		}
	
	}

	/**
	 * TechniqueEquivalenceMap view descriptor
	 * 
	 */
	public static class TechniqueEquivalenceMap {
		public static class Properties {
	
			
			public static String equivalence = "refframework::TechniqueEquivalenceMap::properties::equivalence";
			
			
			public static String techniqueEquivalenceMap_ = "refframework::TechniqueEquivalenceMap::properties::techniqueEquivalenceMap_";
			
	
		}
	
	}

}
