/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.opencert.elastic.search.ElasticFinder;
import org.eclipse.opencert.elastic.search.ElasticFinderImpl;
import org.eclipse.opencert.elastic.search.Hit;
import org.eclipse.uml2.uml.Component;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class ElasticFinderTest {

	@BeforeClass
	public static void setup() throws Exception {
		ElasticClient client = ElasticClientImpl.on("localhost", 9200, "http");
		try {
			client.indexStatus("uml");
		} catch (Exception e) {
			// index not yet there
			System.out.println("Indexing UML classes...");
			List<ElasticDocument> documents = EObjectToDocument.INSTANCE.convert(TestData.umlClasses(), "uml");
			client.storeAll(documents.iterator()).close();
		}
	}
	
	@Test
	@Ignore
	public void testDeleteTestIndices() throws Exception {
		ElasticClientImpl.on("localhost", 9200, "http").delete("uml");
		ElasticClientImpl.on("localhost", 9200, "http").delete("finder-test");
	}
	
	@Test
	public void testSendAndSearch() throws Exception {
		Component object = TestData.createComponent();
		ElasticDocument document = EObjectToDocument.INSTANCE.convert(object, "finder-test");
		ElasticClient client = ElasticClientImpl.on("localhost", 9200, "http")/*.delete("finder-test")*/.store(document);
		
		ElasticFinder finder = ElasticFinderImpl.onClient(client);
		Set<Hit> hits = finder.within("finder-test").search(null, null);
		Assert.assertEquals(1, hits.size());
		//
		hits = finder.search("foobar", null);
		Assert.assertEquals(0, hits.size());
		client.close();
	}
	
	@Test
	public void testSearch() throws Exception {
		ElasticClient client = ElasticClientImpl.on("localhost", 9200, "http");
		Set<Hit> hits = ElasticFinderImpl.onClient(client).search("this AND that OR thus", null);
		Assert.assertNotNull("set expected", hits);
		Assert.assertTrue("empty set expected", hits.isEmpty());
	}

	@Test
	public void testDefaultClient() throws Exception {
		Set<Hit> hits = ElasticFinderImpl.onDefaultClient().search("this AND that OR thus", null);
		Assert.assertNotNull("set expected", hits);
		Assert.assertTrue("empty set expected", hits.isEmpty());
	}

	@Test
	public void testNoHit() throws Exception {
		testNoHit(ElasticFinderImpl.onDummy(TestData.uml()));
		// we use indexed UML meta model for testing
		testNoHit(ElasticFinderImpl.onDefaultClient());
	}
	
	private void testNoHit(ElasticFinder finder) throws Exception {
		// there is no "Requirement" in UML
		Set<Hit> hits = finder.search("Requirement", null);
		Assert.assertNotNull("set expected", hits);
		Assert.assertTrue("empty set expected", hits.isEmpty());
	}

	@Test
	public void testHit() throws Exception {
		// we use UML dummy meta model for testing
		testHit(ElasticFinderImpl.onDummy(TestData.uml()));
		// we use indexed UML meta model for testing
		testHit(ElasticFinderImpl.onDefaultClient());
	}
	
	private void testHit(ElasticFinder finder) throws Exception {
		// we should find the "Component" class
		Set<Hit> hits = finder.search("Component", null);
		Assert.assertNotNull("set expected", hits);
		Assert.assertFalse("filled set expected", hits.isEmpty());

		Hit firstHit = hits.iterator().next();
		Assert.assertTrue("score must be > 0", firstHit.score > 0);
	}
	
	@Test
	public void testHitWithExpression() throws Exception {
		testHitWithExpression(ElasticFinderImpl.onDummy(TestData.uml()));
		testHitWithExpression(ElasticFinderImpl.onDefaultClient());
	}
	
	private void testHitWithExpression(ElasticFinder finder) throws Exception {
		Set<Hit> hits = finder.search(".*ompon.*", null);
		Assert.assertNotNull("set expected", hits);
		Assert.assertFalse("filled set expected", hits.isEmpty());
	}

	@Test
	public void testHitWithNegativeFilter() throws Exception {
		testHitWithNegativeFilter(ElasticFinderImpl.onDummy(TestData.uml()));
		testHitWithNegativeFilter(ElasticFinderImpl.onDefaultClient());
	}

	private void testHitWithNegativeFilter(ElasticFinder finder) throws Exception {
		Map<String, Object> filters = new HashMap<>();
		filters.put("abstract", Boolean.TRUE);
		Set<Hit> hits = finder.search("Component", filters);
		Assert.assertNotNull("set expected", hits);
		Assert.assertTrue("empty set expected - Component is not abstract", hits.isEmpty());
	}

	@Test
	public void testHitWithPositiveFilter() throws Exception {
		testHitWithPositiveFilter(ElasticFinderImpl.onDummy(TestData.uml()));
		testHitWithPositiveFilter(ElasticFinderImpl.onDefaultClient());
	}
	
	private void testHitWithPositiveFilter(ElasticFinder finder) throws Exception {
		Map<String, Object> filters = new HashMap<>();
		filters.put("abstract", Boolean.FALSE);
		filters.put("interface", Boolean.FALSE);
		Set<Hit> hits = finder.search("Component", filters);
		Assert.assertNotNull("set expected", hits);
		Assert.assertFalse("filled set expected - Component is not abstract", hits.isEmpty());

		Hit firstHit = hits.iterator().next();
		Assert.assertTrue("score must be > 1 (due to filters matched)", firstHit.score > 1);
	}
}
