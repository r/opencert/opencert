/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.evm.evidspec.evidence.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.ManageableAssuranceAssetImpl;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.evm.evidspec.evidence.Resource;
import org.eclipse.opencert.evm.evidspec.evidence.Value;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Artefact</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getPropertyValue <em>Property Value</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getArtefactPart <em>Artefact Part</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getVersionID <em>Version ID</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getDate <em>Date</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getChanges <em>Changes</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#isIsLastVersion <em>Is Last Version</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#getPrecedentVersion <em>Precedent Version</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#isIsTemplate <em>Is Template</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl#isIsConfigurable <em>Is Configurable</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArtefactImpl extends ManageableAssuranceAssetImpl implements Artefact {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getVersionID() <em>Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersionID()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDate() <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getChanges() <em>Changes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChanges()
	 * @generated
	 * @ordered
	 */
	protected static final String CHANGES_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isIsLastVersion() <em>Is Last Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsLastVersion()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_LAST_VERSION_EDEFAULT = false;

	/**
	 * The default value of the '{@link #isIsTemplate() <em>Is Template</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsTemplate()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_TEMPLATE_EDEFAULT = false;

	/**
	 * The default value of the '{@link #isIsConfigurable() <em>Is Configurable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsConfigurable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CONFIGURABLE_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArtefactImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EvidencePackage.Literals.ARTEFACT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT__ID, GeneralPackage.Literals.NAMED_ELEMENT__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		eDynamicSet(EvidencePackage.ARTEFACT__ID, GeneralPackage.Literals.NAMED_ELEMENT__ID, newId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT__NAME, GeneralPackage.Literals.NAMED_ELEMENT__NAME, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eDynamicSet(EvidencePackage.ARTEFACT__NAME, GeneralPackage.Literals.NAMED_ELEMENT__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT__DESCRIPTION, GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		eDynamicSet(EvidencePackage.ARTEFACT__DESCRIPTION, GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, newDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Value> getPropertyValue() {
		return (EList<Value>)eDynamicGet(EvidencePackage.ARTEFACT__PROPERTY_VALUE, EvidencePackage.Literals.ARTEFACT__PROPERTY_VALUE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Artefact> getArtefactPart() {
		return (EList<Artefact>)eDynamicGet(EvidencePackage.ARTEFACT__ARTEFACT_PART, EvidencePackage.Literals.ARTEFACT__ARTEFACT_PART, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ArtefactRel> getOwnedRel() {
		return (EList<ArtefactRel>)eDynamicGet(EvidencePackage.ARTEFACT__OWNED_REL, EvidencePackage.Literals.ARTEFACT__OWNED_REL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersionID() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT__VERSION_ID, EvidencePackage.Literals.ARTEFACT__VERSION_ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersionID(String newVersionID) {
		eDynamicSet(EvidencePackage.ARTEFACT__VERSION_ID, EvidencePackage.Literals.ARTEFACT__VERSION_ID, newVersionID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getDate() {
		return (Date)eDynamicGet(EvidencePackage.ARTEFACT__DATE, EvidencePackage.Literals.ARTEFACT__DATE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDate(Date newDate) {
		eDynamicSet(EvidencePackage.ARTEFACT__DATE, EvidencePackage.Literals.ARTEFACT__DATE, newDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChanges() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT__CHANGES, EvidencePackage.Literals.ARTEFACT__CHANGES, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChanges(String newChanges) {
		eDynamicSet(EvidencePackage.ARTEFACT__CHANGES, EvidencePackage.Literals.ARTEFACT__CHANGES, newChanges);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsLastVersion() {
		return (Boolean)eDynamicGet(EvidencePackage.ARTEFACT__IS_LAST_VERSION, EvidencePackage.Literals.ARTEFACT__IS_LAST_VERSION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsLastVersion(boolean newIsLastVersion) {
		eDynamicSet(EvidencePackage.ARTEFACT__IS_LAST_VERSION, EvidencePackage.Literals.ARTEFACT__IS_LAST_VERSION, newIsLastVersion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Resource> getResource() {
		return (EList<Resource>)eDynamicGet(EvidencePackage.ARTEFACT__RESOURCE, EvidencePackage.Literals.ARTEFACT__RESOURCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Artefact getPrecedentVersion() {
		return (Artefact)eDynamicGet(EvidencePackage.ARTEFACT__PRECEDENT_VERSION, EvidencePackage.Literals.ARTEFACT__PRECEDENT_VERSION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Artefact basicGetPrecedentVersion() {
		return (Artefact)eDynamicGet(EvidencePackage.ARTEFACT__PRECEDENT_VERSION, EvidencePackage.Literals.ARTEFACT__PRECEDENT_VERSION, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrecedentVersion(Artefact newPrecedentVersion) {
		eDynamicSet(EvidencePackage.ARTEFACT__PRECEDENT_VERSION, EvidencePackage.Literals.ARTEFACT__PRECEDENT_VERSION, newPrecedentVersion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsTemplate() {
		return (Boolean)eDynamicGet(EvidencePackage.ARTEFACT__IS_TEMPLATE, EvidencePackage.Literals.ARTEFACT__IS_TEMPLATE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsTemplate(boolean newIsTemplate) {
		eDynamicSet(EvidencePackage.ARTEFACT__IS_TEMPLATE, EvidencePackage.Literals.ARTEFACT__IS_TEMPLATE, newIsTemplate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsConfigurable() {
		return (Boolean)eDynamicGet(EvidencePackage.ARTEFACT__IS_CONFIGURABLE, EvidencePackage.Literals.ARTEFACT__IS_CONFIGURABLE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsConfigurable(boolean newIsConfigurable) {
		eDynamicSet(EvidencePackage.ARTEFACT__IS_CONFIGURABLE, EvidencePackage.Literals.ARTEFACT__IS_CONFIGURABLE, newIsConfigurable);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT__PROPERTY_VALUE:
				return ((InternalEList<?>)getPropertyValue()).basicRemove(otherEnd, msgs);
			case EvidencePackage.ARTEFACT__ARTEFACT_PART:
				return ((InternalEList<?>)getArtefactPart()).basicRemove(otherEnd, msgs);
			case EvidencePackage.ARTEFACT__OWNED_REL:
				return ((InternalEList<?>)getOwnedRel()).basicRemove(otherEnd, msgs);
			case EvidencePackage.ARTEFACT__RESOURCE:
				return ((InternalEList<?>)getResource()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT__ID:
				return getId();
			case EvidencePackage.ARTEFACT__NAME:
				return getName();
			case EvidencePackage.ARTEFACT__DESCRIPTION:
				return getDescription();
			case EvidencePackage.ARTEFACT__PROPERTY_VALUE:
				return getPropertyValue();
			case EvidencePackage.ARTEFACT__ARTEFACT_PART:
				return getArtefactPart();
			case EvidencePackage.ARTEFACT__OWNED_REL:
				return getOwnedRel();
			case EvidencePackage.ARTEFACT__VERSION_ID:
				return getVersionID();
			case EvidencePackage.ARTEFACT__DATE:
				return getDate();
			case EvidencePackage.ARTEFACT__CHANGES:
				return getChanges();
			case EvidencePackage.ARTEFACT__IS_LAST_VERSION:
				return isIsLastVersion();
			case EvidencePackage.ARTEFACT__RESOURCE:
				return getResource();
			case EvidencePackage.ARTEFACT__PRECEDENT_VERSION:
				if (resolve) return getPrecedentVersion();
				return basicGetPrecedentVersion();
			case EvidencePackage.ARTEFACT__IS_TEMPLATE:
				return isIsTemplate();
			case EvidencePackage.ARTEFACT__IS_CONFIGURABLE:
				return isIsConfigurable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT__ID:
				setId((String)newValue);
				return;
			case EvidencePackage.ARTEFACT__NAME:
				setName((String)newValue);
				return;
			case EvidencePackage.ARTEFACT__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case EvidencePackage.ARTEFACT__PROPERTY_VALUE:
				getPropertyValue().clear();
				getPropertyValue().addAll((Collection<? extends Value>)newValue);
				return;
			case EvidencePackage.ARTEFACT__ARTEFACT_PART:
				getArtefactPart().clear();
				getArtefactPart().addAll((Collection<? extends Artefact>)newValue);
				return;
			case EvidencePackage.ARTEFACT__OWNED_REL:
				getOwnedRel().clear();
				getOwnedRel().addAll((Collection<? extends ArtefactRel>)newValue);
				return;
			case EvidencePackage.ARTEFACT__VERSION_ID:
				setVersionID((String)newValue);
				return;
			case EvidencePackage.ARTEFACT__DATE:
				setDate((Date)newValue);
				return;
			case EvidencePackage.ARTEFACT__CHANGES:
				setChanges((String)newValue);
				return;
			case EvidencePackage.ARTEFACT__IS_LAST_VERSION:
				setIsLastVersion((Boolean)newValue);
				return;
			case EvidencePackage.ARTEFACT__RESOURCE:
				getResource().clear();
				getResource().addAll((Collection<? extends Resource>)newValue);
				return;
			case EvidencePackage.ARTEFACT__PRECEDENT_VERSION:
				setPrecedentVersion((Artefact)newValue);
				return;
			case EvidencePackage.ARTEFACT__IS_TEMPLATE:
				setIsTemplate((Boolean)newValue);
				return;
			case EvidencePackage.ARTEFACT__IS_CONFIGURABLE:
				setIsConfigurable((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT__ID:
				setId(ID_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT__PROPERTY_VALUE:
				getPropertyValue().clear();
				return;
			case EvidencePackage.ARTEFACT__ARTEFACT_PART:
				getArtefactPart().clear();
				return;
			case EvidencePackage.ARTEFACT__OWNED_REL:
				getOwnedRel().clear();
				return;
			case EvidencePackage.ARTEFACT__VERSION_ID:
				setVersionID(VERSION_ID_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT__DATE:
				setDate(DATE_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT__CHANGES:
				setChanges(CHANGES_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT__IS_LAST_VERSION:
				setIsLastVersion(IS_LAST_VERSION_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT__RESOURCE:
				getResource().clear();
				return;
			case EvidencePackage.ARTEFACT__PRECEDENT_VERSION:
				setPrecedentVersion((Artefact)null);
				return;
			case EvidencePackage.ARTEFACT__IS_TEMPLATE:
				setIsTemplate(IS_TEMPLATE_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT__IS_CONFIGURABLE:
				setIsConfigurable(IS_CONFIGURABLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT__ID:
				return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
			case EvidencePackage.ARTEFACT__NAME:
				return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
			case EvidencePackage.ARTEFACT__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? getDescription() != null : !DESCRIPTION_EDEFAULT.equals(getDescription());
			case EvidencePackage.ARTEFACT__PROPERTY_VALUE:
				return !getPropertyValue().isEmpty();
			case EvidencePackage.ARTEFACT__ARTEFACT_PART:
				return !getArtefactPart().isEmpty();
			case EvidencePackage.ARTEFACT__OWNED_REL:
				return !getOwnedRel().isEmpty();
			case EvidencePackage.ARTEFACT__VERSION_ID:
				return VERSION_ID_EDEFAULT == null ? getVersionID() != null : !VERSION_ID_EDEFAULT.equals(getVersionID());
			case EvidencePackage.ARTEFACT__DATE:
				return DATE_EDEFAULT == null ? getDate() != null : !DATE_EDEFAULT.equals(getDate());
			case EvidencePackage.ARTEFACT__CHANGES:
				return CHANGES_EDEFAULT == null ? getChanges() != null : !CHANGES_EDEFAULT.equals(getChanges());
			case EvidencePackage.ARTEFACT__IS_LAST_VERSION:
				return isIsLastVersion() != IS_LAST_VERSION_EDEFAULT;
			case EvidencePackage.ARTEFACT__RESOURCE:
				return !getResource().isEmpty();
			case EvidencePackage.ARTEFACT__PRECEDENT_VERSION:
				return basicGetPrecedentVersion() != null;
			case EvidencePackage.ARTEFACT__IS_TEMPLATE:
				return isIsTemplate() != IS_TEMPLATE_EDEFAULT;
			case EvidencePackage.ARTEFACT__IS_CONFIGURABLE:
				return isIsConfigurable() != IS_CONFIGURABLE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case EvidencePackage.ARTEFACT__ID: return GeneralPackage.NAMED_ELEMENT__ID;
				case EvidencePackage.ARTEFACT__NAME: return GeneralPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (derivedFeatureID) {
				case EvidencePackage.ARTEFACT__DESCRIPTION: return GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.NAMED_ELEMENT__ID: return EvidencePackage.ARTEFACT__ID;
				case GeneralPackage.NAMED_ELEMENT__NAME: return EvidencePackage.ARTEFACT__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION: return EvidencePackage.ARTEFACT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ArtefactImpl
