/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;

// End of user code

/**
 * 
 * 
 */
public class BaseFrameworkPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseFrameworkPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text scope;
	protected Text rev;
	protected Text purpose;
	protected Text publisher;
	protected Text issued;
	protected ReferencesTable ownedActivities;
	protected List<ViewerFilter> ownedActivitiesBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedActivitiesFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedArtefact;
	protected List<ViewerFilter> ownedArtefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedArtefactFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedRequirement;
	protected List<ViewerFilter> ownedRequirementBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRequirementFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedApplicLevel;
	protected List<ViewerFilter> ownedApplicLevelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedApplicLevelFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedCriticLevel;
	protected List<ViewerFilter> ownedCriticLevelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedCriticLevelFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedRole;
	protected List<ViewerFilter> ownedRoleBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRoleFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedTechnique;
	protected List<ViewerFilter> ownedTechniqueBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedTechniqueFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseFrameworkPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseFrameworkStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseFrameworkStep.addStep(BaselineViewsRepository.BaseFramework.Properties.class);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.id);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.name);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.description);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.scope);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.rev);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.purpose);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.publisher);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.issued);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.ownedActivities);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.ownedArtefact);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.ownedRequirement);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.ownedRole);
		propertiesStep.addStep(BaselineViewsRepository.BaseFramework.Properties.ownedTechnique);
		
		
		composer = new PartComposer(baseFrameworkStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseFramework.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.id) {
					return createIdText(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.name) {
					return createNameText(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.description) {
					return createDescriptionTextarea(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.scope) {
					return createScopeTextarea(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.rev) {
					return createRevText(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.purpose) {
					return createPurposeTextarea(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.publisher) {
					return createPublisherText(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.issued) {
					return createIssuedText(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.ownedActivities) {
					return createOwnedActivitiesAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.ownedArtefact) {
					return createOwnedArtefactAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.ownedRequirement) {
					return createOwnedRequirementAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel) {
					return createOwnedApplicLevelAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel) {
					return createOwnedCriticLevelAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.ownedRole) {
					return createOwnedRoleAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseFramework.Properties.ownedTechnique) {
					return createOwnedTechniqueAdvancedTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseFrameworkPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseFramework.Properties.id, BaselineMessages.BaseFrameworkPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, BaselineViewsRepository.BaseFramework.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.id, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseFramework.Properties.name, BaselineMessages.BaseFrameworkPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, BaselineViewsRepository.BaseFramework.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.name, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionTextarea(Composite parent) {
		Label descriptionLabel = createDescription(parent, BaselineViewsRepository.BaseFramework.Properties.description, BaselineMessages.BaseFrameworkPropertiesEditionPart_DescriptionLabel);
		GridData descriptionLabelData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionLabelData.horizontalSpan = 3;
		descriptionLabel.setLayoutData(descriptionLabelData);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionData.horizontalSpan = 2;
		descriptionData.heightHint = 80;
		descriptionData.widthHint = 200;
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		EditingUtils.setID(description, BaselineViewsRepository.BaseFramework.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Textarea"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.description, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createScopeTextarea(Composite parent) {
		Label scopeLabel = createDescription(parent, BaselineViewsRepository.BaseFramework.Properties.scope, BaselineMessages.BaseFrameworkPropertiesEditionPart_ScopeLabel);
		GridData scopeLabelData = new GridData(GridData.FILL_HORIZONTAL);
		scopeLabelData.horizontalSpan = 3;
		scopeLabel.setLayoutData(scopeLabelData);
		scope = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
		GridData scopeData = new GridData(GridData.FILL_HORIZONTAL);
		scopeData.horizontalSpan = 2;
		scopeData.heightHint = 80;
		scopeData.widthHint = 200;
		scope.setLayoutData(scopeData);
		scope.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.scope, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, scope.getText()));
			}

		});
		EditingUtils.setID(scope, BaselineViewsRepository.BaseFramework.Properties.scope);
		EditingUtils.setEEFtype(scope, "eef::Textarea"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.scope, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createScopeTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createRevText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseFramework.Properties.rev, BaselineMessages.BaseFrameworkPropertiesEditionPart_RevLabel);
		rev = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData revData = new GridData(GridData.FILL_HORIZONTAL);
		rev.setLayoutData(revData);
		rev.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.rev, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, rev.getText()));
			}

		});
		rev.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.rev, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, rev.getText()));
				}
			}

		});
		EditingUtils.setID(rev, BaselineViewsRepository.BaseFramework.Properties.rev);
		EditingUtils.setEEFtype(rev, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.rev, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createRevText

		// End of user code
		return parent;
	}

	
	protected Composite createPurposeTextarea(Composite parent) {
		Label purposeLabel = createDescription(parent, BaselineViewsRepository.BaseFramework.Properties.purpose, BaselineMessages.BaseFrameworkPropertiesEditionPart_PurposeLabel);
		GridData purposeLabelData = new GridData(GridData.FILL_HORIZONTAL);
		purposeLabelData.horizontalSpan = 3;
		purposeLabel.setLayoutData(purposeLabelData);
		purpose = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
		GridData purposeData = new GridData(GridData.FILL_HORIZONTAL);
		purposeData.horizontalSpan = 2;
		purposeData.heightHint = 80;
		purposeData.widthHint = 200;
		purpose.setLayoutData(purposeData);
		purpose.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.purpose, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, purpose.getText()));
			}

		});
		EditingUtils.setID(purpose, BaselineViewsRepository.BaseFramework.Properties.purpose);
		EditingUtils.setEEFtype(purpose, "eef::Textarea"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.purpose, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createPurposeTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createPublisherText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseFramework.Properties.publisher, BaselineMessages.BaseFrameworkPropertiesEditionPart_PublisherLabel);
		publisher = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData publisherData = new GridData(GridData.FILL_HORIZONTAL);
		publisher.setLayoutData(publisherData);
		publisher.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.publisher, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, publisher.getText()));
			}

		});
		publisher.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.publisher, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, publisher.getText()));
				}
			}

		});
		EditingUtils.setID(publisher, BaselineViewsRepository.BaseFramework.Properties.publisher);
		EditingUtils.setEEFtype(publisher, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.publisher, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createPublisherText

		// End of user code
		return parent;
	}

	
	protected Composite createIssuedText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseFramework.Properties.issued, BaselineMessages.BaseFrameworkPropertiesEditionPart_IssuedLabel);
		issued = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData issuedData = new GridData(GridData.FILL_HORIZONTAL);
		issued.setLayoutData(issuedData);
		issued.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.issued, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, issued.getText()));
			}

		});
		issued.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.issued, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, issued.getText()));
				}
			}

		});
		EditingUtils.setID(issued, BaselineViewsRepository.BaseFramework.Properties.issued);
		EditingUtils.setEEFtype(issued, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.issued, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIssuedText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedActivitiesAdvancedTableComposition(Composite parent) {
		this.ownedActivities = new ReferencesTable(getDescription(BaselineViewsRepository.BaseFramework.Properties.ownedActivities, BaselineMessages.BaseFrameworkPropertiesEditionPart_OwnedActivitiesLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedActivities, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedActivities.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedActivities, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedActivities.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedActivities, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedActivities.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedActivities, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedActivities.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedActivitiesFilters) {
			this.ownedActivities.addFilter(filter);
		}
		this.ownedActivities.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.ownedActivities, BaselineViewsRepository.SWT_KIND));
		this.ownedActivities.createControls(parent);
		this.ownedActivities.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedActivities, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedActivitiesData = new GridData(GridData.FILL_HORIZONTAL);
		ownedActivitiesData.horizontalSpan = 3;
		this.ownedActivities.setLayoutData(ownedActivitiesData);
		this.ownedActivities.setLowerBound(0);
		this.ownedActivities.setUpperBound(-1);
		ownedActivities.setID(BaselineViewsRepository.BaseFramework.Properties.ownedActivities);
		ownedActivities.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedActivitiesAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedArtefactAdvancedTableComposition(Composite parent) {
		this.ownedArtefact = new ReferencesTable(getDescription(BaselineViewsRepository.BaseFramework.Properties.ownedArtefact, BaselineMessages.BaseFrameworkPropertiesEditionPart_OwnedArtefactLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedArtefact.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedArtefact.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedArtefact.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedArtefact.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedArtefactFilters) {
			this.ownedArtefact.addFilter(filter);
		}
		this.ownedArtefact.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.ownedArtefact, BaselineViewsRepository.SWT_KIND));
		this.ownedArtefact.createControls(parent);
		this.ownedArtefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedArtefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedArtefactData = new GridData(GridData.FILL_HORIZONTAL);
		ownedArtefactData.horizontalSpan = 3;
		this.ownedArtefact.setLayoutData(ownedArtefactData);
		this.ownedArtefact.setLowerBound(0);
		this.ownedArtefact.setUpperBound(-1);
		ownedArtefact.setID(BaselineViewsRepository.BaseFramework.Properties.ownedArtefact);
		ownedArtefact.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedArtefactAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRequirementAdvancedTableComposition(Composite parent) {
		this.ownedRequirement = new ReferencesTable(getDescription(BaselineViewsRepository.BaseFramework.Properties.ownedRequirement, BaselineMessages.BaseFrameworkPropertiesEditionPart_OwnedRequirementLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRequirement.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRequirement.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRequirement.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRequirement.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRequirementFilters) {
			this.ownedRequirement.addFilter(filter);
		}
		this.ownedRequirement.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.ownedRequirement, BaselineViewsRepository.SWT_KIND));
		this.ownedRequirement.createControls(parent);
		this.ownedRequirement.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedRequirement, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRequirementData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRequirementData.horizontalSpan = 3;
		this.ownedRequirement.setLayoutData(ownedRequirementData);
		this.ownedRequirement.setLowerBound(0);
		this.ownedRequirement.setUpperBound(-1);
		ownedRequirement.setID(BaselineViewsRepository.BaseFramework.Properties.ownedRequirement);
		ownedRequirement.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRequirementAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedApplicLevelAdvancedTableComposition(Composite parent) {
		this.ownedApplicLevel = new ReferencesTable(getDescription(BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel, BaselineMessages.BaseFrameworkPropertiesEditionPart_OwnedApplicLevelLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedApplicLevel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedApplicLevel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedApplicLevel.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedApplicLevel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedApplicLevelFilters) {
			this.ownedApplicLevel.addFilter(filter);
		}
		this.ownedApplicLevel.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel, BaselineViewsRepository.SWT_KIND));
		this.ownedApplicLevel.createControls(parent);
		this.ownedApplicLevel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedApplicLevelData = new GridData(GridData.FILL_HORIZONTAL);
		ownedApplicLevelData.horizontalSpan = 3;
		this.ownedApplicLevel.setLayoutData(ownedApplicLevelData);
		this.ownedApplicLevel.setLowerBound(0);
		this.ownedApplicLevel.setUpperBound(-1);
		ownedApplicLevel.setID(BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel);
		ownedApplicLevel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedApplicLevelAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedCriticLevelAdvancedTableComposition(Composite parent) {
		this.ownedCriticLevel = new ReferencesTable(getDescription(BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel, BaselineMessages.BaseFrameworkPropertiesEditionPart_OwnedCriticLevelLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedCriticLevel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedCriticLevel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedCriticLevel.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedCriticLevel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedCriticLevelFilters) {
			this.ownedCriticLevel.addFilter(filter);
		}
		this.ownedCriticLevel.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel, BaselineViewsRepository.SWT_KIND));
		this.ownedCriticLevel.createControls(parent);
		this.ownedCriticLevel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedCriticLevelData = new GridData(GridData.FILL_HORIZONTAL);
		ownedCriticLevelData.horizontalSpan = 3;
		this.ownedCriticLevel.setLayoutData(ownedCriticLevelData);
		this.ownedCriticLevel.setLowerBound(0);
		this.ownedCriticLevel.setUpperBound(-1);
		ownedCriticLevel.setID(BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel);
		ownedCriticLevel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedCriticLevelAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRoleAdvancedTableComposition(Composite parent) {
		this.ownedRole = new ReferencesTable(getDescription(BaselineViewsRepository.BaseFramework.Properties.ownedRole, BaselineMessages.BaseFrameworkPropertiesEditionPart_OwnedRoleLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedRole, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRole.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedRole, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRole.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedRole, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRole.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedRole, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRole.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRoleFilters) {
			this.ownedRole.addFilter(filter);
		}
		this.ownedRole.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.ownedRole, BaselineViewsRepository.SWT_KIND));
		this.ownedRole.createControls(parent);
		this.ownedRole.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedRole, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRoleData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRoleData.horizontalSpan = 3;
		this.ownedRole.setLayoutData(ownedRoleData);
		this.ownedRole.setLowerBound(0);
		this.ownedRole.setUpperBound(-1);
		ownedRole.setID(BaselineViewsRepository.BaseFramework.Properties.ownedRole);
		ownedRole.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRoleAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedTechniqueAdvancedTableComposition(Composite parent) {
		this.ownedTechnique = new ReferencesTable(getDescription(BaselineViewsRepository.BaseFramework.Properties.ownedTechnique, BaselineMessages.BaseFrameworkPropertiesEditionPart_OwnedTechniqueLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedTechnique.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedTechnique.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedTechnique.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedTechnique.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedTechniqueFilters) {
			this.ownedTechnique.addFilter(filter);
		}
		this.ownedTechnique.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseFramework.Properties.ownedTechnique, BaselineViewsRepository.SWT_KIND));
		this.ownedTechnique.createControls(parent);
		this.ownedTechnique.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseFrameworkPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseFramework.Properties.ownedTechnique, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedTechniqueData = new GridData(GridData.FILL_HORIZONTAL);
		ownedTechniqueData.horizontalSpan = 3;
		this.ownedTechnique.setLayoutData(ownedTechniqueData);
		this.ownedTechnique.setLowerBound(0);
		this.ownedTechnique.setUpperBound(-1);
		ownedTechnique.setID(BaselineViewsRepository.BaseFramework.Properties.ownedTechnique);
		ownedTechnique.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedTechniqueAdvancedTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setBackground(description.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			description.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#getScope()
	 * 
	 */
	public String getScope() {
		return scope.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#setScope(String newValue)
	 * 
	 */
	public void setScope(String newValue) {
		if (newValue != null) {
			scope.setText(newValue);
		} else {
			scope.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.scope);
		if (eefElementEditorReadOnlyState && scope.isEnabled()) {
			scope.setEnabled(false);
			scope.setBackground(scope.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			scope.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !scope.isEnabled()) {
			scope.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#getRev()
	 * 
	 */
	public String getRev() {
		return rev.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#setRev(String newValue)
	 * 
	 */
	public void setRev(String newValue) {
		if (newValue != null) {
			rev.setText(newValue);
		} else {
			rev.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.rev);
		if (eefElementEditorReadOnlyState && rev.isEnabled()) {
			rev.setEnabled(false);
			rev.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !rev.isEnabled()) {
			rev.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#getPurpose()
	 * 
	 */
	public String getPurpose() {
		return purpose.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#setPurpose(String newValue)
	 * 
	 */
	public void setPurpose(String newValue) {
		if (newValue != null) {
			purpose.setText(newValue);
		} else {
			purpose.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.purpose);
		if (eefElementEditorReadOnlyState && purpose.isEnabled()) {
			purpose.setEnabled(false);
			purpose.setBackground(purpose.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			purpose.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !purpose.isEnabled()) {
			purpose.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#getPublisher()
	 * 
	 */
	public String getPublisher() {
		return publisher.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#setPublisher(String newValue)
	 * 
	 */
	public void setPublisher(String newValue) {
		if (newValue != null) {
			publisher.setText(newValue);
		} else {
			publisher.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.publisher);
		if (eefElementEditorReadOnlyState && publisher.isEnabled()) {
			publisher.setEnabled(false);
			publisher.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !publisher.isEnabled()) {
			publisher.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#getIssued()
	 * 
	 */
	public String getIssued() {
		return issued.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#setIssued(String newValue)
	 * 
	 */
	public void setIssued(String newValue) {
		if (newValue != null) {
			issued.setText(newValue);
		} else {
			issued.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.issued);
		if (eefElementEditorReadOnlyState && issued.isEnabled()) {
			issued.setEnabled(false);
			issued.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !issued.isEnabled()) {
			issued.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#initOwnedActivities(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedActivities(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedActivities.setContentProvider(contentProvider);
		ownedActivities.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.ownedActivities);
		if (eefElementEditorReadOnlyState && ownedActivities.isEnabled()) {
			ownedActivities.setEnabled(false);
			ownedActivities.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedActivities.isEnabled()) {
			ownedActivities.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#updateOwnedActivities()
	 * 
	 */
	public void updateOwnedActivities() {
	ownedActivities.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addFilterOwnedActivities(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedActivities(ViewerFilter filter) {
		ownedActivitiesFilters.add(filter);
		if (this.ownedActivities != null) {
			this.ownedActivities.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addBusinessFilterOwnedActivities(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedActivities(ViewerFilter filter) {
		ownedActivitiesBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#isContainedInOwnedActivitiesTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedActivitiesTable(EObject element) {
		return ((ReferencesTableSettings)ownedActivities.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#initOwnedArtefact(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedArtefact.setContentProvider(contentProvider);
		ownedArtefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.ownedArtefact);
		if (eefElementEditorReadOnlyState && ownedArtefact.isEnabled()) {
			ownedArtefact.setEnabled(false);
			ownedArtefact.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedArtefact.isEnabled()) {
			ownedArtefact.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#updateOwnedArtefact()
	 * 
	 */
	public void updateOwnedArtefact() {
	ownedArtefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addFilterOwnedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedArtefact(ViewerFilter filter) {
		ownedArtefactFilters.add(filter);
		if (this.ownedArtefact != null) {
			this.ownedArtefact.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addBusinessFilterOwnedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedArtefact(ViewerFilter filter) {
		ownedArtefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#isContainedInOwnedArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedArtefactTable(EObject element) {
		return ((ReferencesTableSettings)ownedArtefact.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#initOwnedRequirement(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRequirement(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRequirement.setContentProvider(contentProvider);
		ownedRequirement.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.ownedRequirement);
		if (eefElementEditorReadOnlyState && ownedRequirement.isEnabled()) {
			ownedRequirement.setEnabled(false);
			ownedRequirement.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRequirement.isEnabled()) {
			ownedRequirement.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#updateOwnedRequirement()
	 * 
	 */
	public void updateOwnedRequirement() {
	ownedRequirement.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addFilterOwnedRequirement(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRequirement(ViewerFilter filter) {
		ownedRequirementFilters.add(filter);
		if (this.ownedRequirement != null) {
			this.ownedRequirement.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addBusinessFilterOwnedRequirement(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRequirement(ViewerFilter filter) {
		ownedRequirementBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#isContainedInOwnedRequirementTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRequirementTable(EObject element) {
		return ((ReferencesTableSettings)ownedRequirement.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#initOwnedApplicLevel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedApplicLevel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedApplicLevel.setContentProvider(contentProvider);
		ownedApplicLevel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel);
		if (eefElementEditorReadOnlyState && ownedApplicLevel.isEnabled()) {
			ownedApplicLevel.setEnabled(false);
			ownedApplicLevel.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedApplicLevel.isEnabled()) {
			ownedApplicLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#updateOwnedApplicLevel()
	 * 
	 */
	public void updateOwnedApplicLevel() {
	ownedApplicLevel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addFilterOwnedApplicLevel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedApplicLevel(ViewerFilter filter) {
		ownedApplicLevelFilters.add(filter);
		if (this.ownedApplicLevel != null) {
			this.ownedApplicLevel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addBusinessFilterOwnedApplicLevel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedApplicLevel(ViewerFilter filter) {
		ownedApplicLevelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#isContainedInOwnedApplicLevelTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedApplicLevelTable(EObject element) {
		return ((ReferencesTableSettings)ownedApplicLevel.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#initOwnedCriticLevel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedCriticLevel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedCriticLevel.setContentProvider(contentProvider);
		ownedCriticLevel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel);
		if (eefElementEditorReadOnlyState && ownedCriticLevel.isEnabled()) {
			ownedCriticLevel.setEnabled(false);
			ownedCriticLevel.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedCriticLevel.isEnabled()) {
			ownedCriticLevel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#updateOwnedCriticLevel()
	 * 
	 */
	public void updateOwnedCriticLevel() {
	ownedCriticLevel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addFilterOwnedCriticLevel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedCriticLevel(ViewerFilter filter) {
		ownedCriticLevelFilters.add(filter);
		if (this.ownedCriticLevel != null) {
			this.ownedCriticLevel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addBusinessFilterOwnedCriticLevel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedCriticLevel(ViewerFilter filter) {
		ownedCriticLevelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#isContainedInOwnedCriticLevelTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedCriticLevelTable(EObject element) {
		return ((ReferencesTableSettings)ownedCriticLevel.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#initOwnedRole(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRole(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRole.setContentProvider(contentProvider);
		ownedRole.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.ownedRole);
		if (eefElementEditorReadOnlyState && ownedRole.isEnabled()) {
			ownedRole.setEnabled(false);
			ownedRole.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRole.isEnabled()) {
			ownedRole.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#updateOwnedRole()
	 * 
	 */
	public void updateOwnedRole() {
	ownedRole.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addFilterOwnedRole(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRole(ViewerFilter filter) {
		ownedRoleFilters.add(filter);
		if (this.ownedRole != null) {
			this.ownedRole.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addBusinessFilterOwnedRole(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRole(ViewerFilter filter) {
		ownedRoleBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#isContainedInOwnedRoleTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRoleTable(EObject element) {
		return ((ReferencesTableSettings)ownedRole.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#initOwnedTechnique(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedTechnique(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedTechnique.setContentProvider(contentProvider);
		ownedTechnique.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseFramework.Properties.ownedTechnique);
		if (eefElementEditorReadOnlyState && ownedTechnique.isEnabled()) {
			ownedTechnique.setEnabled(false);
			ownedTechnique.setToolTipText(BaselineMessages.BaseFramework_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedTechnique.isEnabled()) {
			ownedTechnique.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#updateOwnedTechnique()
	 * 
	 */
	public void updateOwnedTechnique() {
	ownedTechnique.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addFilterOwnedTechnique(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedTechnique(ViewerFilter filter) {
		ownedTechniqueFilters.add(filter);
		if (this.ownedTechnique != null) {
			this.ownedTechnique.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#addBusinessFilterOwnedTechnique(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedTechnique(ViewerFilter filter) {
		ownedTechniqueBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart#isContainedInOwnedTechniqueTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedTechniqueTable(EObject element) {
		return ((ReferencesTableSettings)ownedTechnique.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseFramework_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
