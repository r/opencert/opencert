/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.listeners;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.view.common.IReport;

public class ReportChangeNotifier
{
    private List<IReportChangeListener> listeners = new LinkedList<IReportChangeListener>();

    public void fireReportChanged(IReport report)
    {
        OpencertLogger.info("Report changed. New report: " + report.getTitle());
        
        listeners.forEach((l) -> l.reportChanged(report));
    }

    public void addListener(IReportChangeListener listener)
    {
        listeners.add(listener);
    }
}
