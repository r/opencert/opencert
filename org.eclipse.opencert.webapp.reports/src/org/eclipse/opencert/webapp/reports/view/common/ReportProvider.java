/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.opencert.webapp.reports.view.ArgumentationItemsPage;
import org.eclipse.opencert.webapp.reports.view.ComplianceEstimationReport;
import org.eclipse.opencert.webapp.reports.view.ComplianceReport;
import org.eclipse.opencert.webapp.reports.view.ConfigurationSettingsPage;
import org.eclipse.opencert.webapp.reports.view.CreateSampleDataPage;
import org.eclipse.opencert.webapp.reports.view.EvidenceItemsPage;
import org.eclipse.opencert.webapp.reports.view.GapAnalysisReport;
import org.eclipse.opencert.webapp.reports.view.ProcessItemsPage;
import org.eclipse.opencert.webapp.reports.view.administration.ProjectAdministrationPage;
import org.eclipse.opencert.webapp.reports.view.metrics.EquivalenceMapReport;
import org.eclipse.opencert.webapp.reports.view.metrics.MetricsEstimationReport;
import org.eclipse.opencert.webapp.reports.view.url.URLParamsContainer;
import org.eclipse.opencert.webapp.reports.webapp.IProjectComboSelector;

public class ReportProvider 
{
	private Map<String, IReport> reports;
    private IProjectComboSelector projectComboSelector;
    private final URLParamsContainer urlParamsContainer;
	
	public ReportProvider(URLParamsContainer urlParamsContainer)
	{
		reports = new HashMap<>();
		this.urlParamsContainer = urlParamsContainer;
	}
	
	public IReport getOrCreateReport(ReportID reportId)
	{
	    return getOrCreateReport(reportId, GUIMode.NORMAL);
	}
	
	public IReport getOrCreateReport(ReportID reportId, GUIMode guiMode)
	{
	    final String reportKey = generateReportKey(reportId, guiMode);
		IReport report = reports.get(reportKey);
		if (report == null) {
			report = createReport(reportId, guiMode);
			reports.put(reportKey, report);
		}
		return report;
	}

	private String generateReportKey(ReportID reportId, GUIMode guiMode)
    {
	    return reportId.name() + guiMode.name();
    }

    public IReport getInitialReport(ReportID reportID, GUIMode guiMode) 
	{
	    if (reportID == null) {
	        reportID = ReportID.COMPLIANCE;
	    }
		return getOrCreateReport(reportID, guiMode);
	}
	
	private IReport createReport(ReportID reportId, GUIMode guiMode) 
	{
		IReport result = null;
		if (ReportID.GAP_ANALYSIS.equals(reportId)) {
			result = new GapAnalysisReport();
		}
		else if (ReportID.METRICS_ESTIMATION.equals(reportId)) {
			result = new MetricsEstimationReport();
		}
		else if (ReportID.EQUIVALENCE_MAP.equals(reportId)) {
			result = new EquivalenceMapReport();
		}
		else if (ReportID.ADMIN_CREATE_SAMPLE_DATA.equals(reportId)) {
			result = new CreateSampleDataPage();
		}
		else if (ReportID.ADMIN_CONFIGURATION_SETTINGS.equals(reportId)) {
			result = new ConfigurationSettingsPage();
		}
		else if (ReportID.ARGUMENTATION_ITEMS.equals(reportId)) {
			result = new ArgumentationItemsPage();
		}
		else if (ReportID.EVIDENCE_ITEMS.equals(reportId)) {
			result = new EvidenceItemsPage();
		}
		else if (ReportID.PROCESS_ITEMS.equals(reportId)) {
			result = new ProcessItemsPage();
		}
		else if (ReportID.COMPLIANCE_ESTIMATION.equals(reportId)) {
            result = new ComplianceEstimationReport(guiMode, urlParamsContainer);
        }
		else if (ReportID.COMPLIANCE.equals(reportId)) {
            result = new ComplianceReport(guiMode, urlParamsContainer);
        }
		else if (ReportID.ADMIN_PROJECT.equals(reportId)) {
            result = new ProjectAdministrationPage(projectComboSelector);
        }
		if (result == null) {
			throw new IllegalArgumentException("reportId: " + reportId);
		}
		return result;
	}

    public void setProjectComboSelector(IProjectComboSelector projectComboSelector)
    {
        this.projectComboSelector = projectComboSelector;
    }
}
