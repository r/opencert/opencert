/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;

// End of user code

/**
 * 
 * 
 */
public class BaseRequirementEquivalenceMapPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseRequirementEquivalenceMapPropertiesEditionPart {

	protected ReferencesTable equivalenceMap;
	protected List<ViewerFilter> equivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> equivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected TableViewer requirementEquivalenceMap;
	protected List<ViewerFilter> requirementEquivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> requirementEquivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addRequirementEquivalenceMap;
	protected Button removeRequirementEquivalenceMap;
	protected Button editRequirementEquivalenceMap;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseRequirementEquivalenceMapPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseRequirementEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseRequirementEquivalenceMapStep.addStep(BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap);
		// EndIRR
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap);
		
		
		composer = new PartComposer(baseRequirementEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap) {
					return createEquivalenceMapAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap) {
					return createRequirementEquivalenceMapTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseRequirementEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEquivalenceMapAdvancedTableComposition(Composite parent) {
		this.equivalenceMap = new ReferencesTable(getDescription(BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap, BaselineMessages.BaseRequirementEquivalenceMapPropertiesEditionPart_EquivalenceMapLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				equivalenceMap.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				equivalenceMap.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				equivalenceMap.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				equivalenceMap.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.equivalenceMapFilters) {
			this.equivalenceMap.addFilter(filter);
		}
		this.equivalenceMap.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap, BaselineViewsRepository.SWT_KIND));
		this.equivalenceMap.createControls(parent);
		this.equivalenceMap.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData equivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		equivalenceMapData.horizontalSpan = 3;
		this.equivalenceMap.setLayoutData(equivalenceMapData);
		this.equivalenceMap.setLowerBound(0);
		this.equivalenceMap.setUpperBound(-1);
		equivalenceMap.setID(BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap);
		equivalenceMap.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEquivalenceMapAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRequirementEquivalenceMapTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableRequirementEquivalenceMap = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableRequirementEquivalenceMap.setHeaderVisible(true);
		GridData gdRequirementEquivalenceMap = new GridData();
		gdRequirementEquivalenceMap.grabExcessHorizontalSpace = true;
		gdRequirementEquivalenceMap.horizontalAlignment = GridData.FILL;
		gdRequirementEquivalenceMap.grabExcessVerticalSpace = true;
		gdRequirementEquivalenceMap.verticalAlignment = GridData.FILL;
		tableRequirementEquivalenceMap.setLayoutData(gdRequirementEquivalenceMap);
		tableRequirementEquivalenceMap.setLinesVisible(true);

		// Start of user code for columns definition for
		// RequirementEquivalenceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableRequirementEquivalenceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */

		TableColumn name = new TableColumn(tableRequirementEquivalenceMap,
				SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableRequirementEquivalenceMap,
				SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableRequirementEquivalenceMap,
				SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Requirements"); //$NON-NLS-1$
		// End IRR
		// End of user code

		requirementEquivalenceMap = new TableViewer(tableRequirementEquivalenceMap);
		requirementEquivalenceMap.setContentProvider(new ArrayContentProvider());
		requirementEquivalenceMap.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for RequirementEquivalenceMap
			public String getColumnText(Object object, int columnIndex) {
				//Start IRR
				/*
				AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
				if (object instanceof EObject) {
					switch (columnIndex) {
					case 0:
						return labelProvider.getText(object);
					}
				}
				*/
				if (object instanceof EObject) {
					BaseEquivalenceMap baseEquivalenceMap = (BaseEquivalenceMap)object;
					switch (columnIndex) {
					case 0:							
						return baseEquivalenceMap.getId();
					case 1:							
						if (baseEquivalenceMap.getMapGroup() == null)
							return "";
						else
							return baseEquivalenceMap.getMapGroup().getName();
					case 2:							
						if (baseEquivalenceMap.getTarget() == null)
							return "";
						else{
							EList<RefAssurableElement> LstElement = baseEquivalenceMap.getTarget();
							Iterator<RefAssurableElement> iter = LstElement.iterator();
							String sElement = "[";
							while (iter.hasNext()) {
								RefAssurableElement assElement = iter.next();
								if (assElement instanceof  RefRequirement) {
									RefRequirement refRequ = (RefRequirement) assElement;
									sElement = sElement + refRequ.getName() + ",";  
								}
							}
							//delete ,
							sElement = sElement.substring(0, sElement.length()-1);
							sElement = sElement + "]";
							return sElement;
						}
							
					}
				}
				//End IRR
				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		requirementEquivalenceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (requirementEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) requirementEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						requirementEquivalenceMap.refresh();
					}
				}
			}

		});
		GridData requirementEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		requirementEquivalenceMapData.minimumHeight = 120;
		requirementEquivalenceMapData.heightHint = 120;
		requirementEquivalenceMap.getTable().setLayoutData(requirementEquivalenceMapData);
		for (ViewerFilter filter : this.requirementEquivalenceMapFilters) {
			requirementEquivalenceMap.addFilter(filter);
		}
		EditingUtils.setID(requirementEquivalenceMap.getTable(), BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap);
		EditingUtils.setEEFtype(requirementEquivalenceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createRequirementEquivalenceMapPanel(tableContainer);
		// Start of user code for createRequirementEquivalenceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRequirementEquivalenceMapPanel(Composite container) {
		Composite requirementEquivalenceMapPanel = new Composite(container, SWT.NONE);
		GridLayout requirementEquivalenceMapPanelLayout = new GridLayout();
		requirementEquivalenceMapPanelLayout.numColumns = 1;
		requirementEquivalenceMapPanel.setLayout(requirementEquivalenceMapPanelLayout);
		addRequirementEquivalenceMap = new Button(requirementEquivalenceMapPanel, SWT.NONE);
		addRequirementEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addRequirementEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addRequirementEquivalenceMap.setLayoutData(addRequirementEquivalenceMapData);
		addRequirementEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				requirementEquivalenceMap.refresh();
			}
		});
		EditingUtils.setID(addRequirementEquivalenceMap, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap);
		EditingUtils.setEEFtype(addRequirementEquivalenceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeRequirementEquivalenceMap = new Button(requirementEquivalenceMapPanel, SWT.NONE);
		removeRequirementEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeRequirementEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeRequirementEquivalenceMap.setLayoutData(removeRequirementEquivalenceMapData);
		removeRequirementEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (requirementEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) requirementEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						requirementEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeRequirementEquivalenceMap, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap);
		EditingUtils.setEEFtype(removeRequirementEquivalenceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editRequirementEquivalenceMap = new Button(requirementEquivalenceMapPanel, SWT.NONE);
		editRequirementEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editRequirementEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editRequirementEquivalenceMap.setLayoutData(editRequirementEquivalenceMapData);
		editRequirementEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (requirementEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) requirementEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						requirementEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editRequirementEquivalenceMap, BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap);
		EditingUtils.setEEFtype(editRequirementEquivalenceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createRequirementEquivalenceMapPanel

		// End of user code
		return requirementEquivalenceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart#initEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		equivalenceMap.setContentProvider(contentProvider);
		equivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.equivalenceMap);
		if (eefElementEditorReadOnlyState && equivalenceMap.isEnabled()) {
			equivalenceMap.setEnabled(false);
			equivalenceMap.setToolTipText(BaselineMessages.BaseRequirementEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !equivalenceMap.isEnabled()) {
			equivalenceMap.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart#updateEquivalenceMap()
	 * 
	 */
	public void updateEquivalenceMap() {
	equivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart#addFilterEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEquivalenceMap(ViewerFilter filter) {
		equivalenceMapFilters.add(filter);
		if (this.equivalenceMap != null) {
			this.equivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart#addBusinessFilterEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEquivalenceMap(ViewerFilter filter) {
		equivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart#isContainedInEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)equivalenceMap.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart#initRequirementEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initRequirementEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		requirementEquivalenceMap.setContentProvider(contentProvider);
		requirementEquivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirementEquivalenceMap.Properties.requirementEquivalenceMap);
		if (eefElementEditorReadOnlyState && requirementEquivalenceMap.getTable().isEnabled()) {
			requirementEquivalenceMap.getTable().setEnabled(false);
			requirementEquivalenceMap.getTable().setToolTipText(BaselineMessages.BaseRequirementEquivalenceMap_ReadOnly);
			addRequirementEquivalenceMap.setEnabled(false);
			addRequirementEquivalenceMap.setToolTipText(BaselineMessages.BaseRequirementEquivalenceMap_ReadOnly);
			removeRequirementEquivalenceMap.setEnabled(false);
			removeRequirementEquivalenceMap.setToolTipText(BaselineMessages.BaseRequirementEquivalenceMap_ReadOnly);
			editRequirementEquivalenceMap.setEnabled(false);
			editRequirementEquivalenceMap.setToolTipText(BaselineMessages.BaseRequirementEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !requirementEquivalenceMap.getTable().isEnabled()) {
			requirementEquivalenceMap.getTable().setEnabled(true);
			addRequirementEquivalenceMap.setEnabled(true);
			removeRequirementEquivalenceMap.setEnabled(true);
			editRequirementEquivalenceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart#updateRequirementEquivalenceMap()
	 * 
	 */
	public void updateRequirementEquivalenceMap() {
	requirementEquivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart#addFilterRequirementEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToRequirementEquivalenceMap(ViewerFilter filter) {
		requirementEquivalenceMapFilters.add(filter);
		if (this.requirementEquivalenceMap != null) {
			this.requirementEquivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart#addBusinessFilterRequirementEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToRequirementEquivalenceMap(ViewerFilter filter) {
		requirementEquivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementEquivalenceMapPropertiesEditionPart#isContainedInRequirementEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInRequirementEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)requirementEquivalenceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseRequirementEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
