/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkFactory;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class RefActivityBasePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for requiredArtefact ReferencesTable
	 */
	private ReferencesTableSettings requiredArtefactSettings;
	
	/**
	 * Settings for producedArtefact ReferencesTable
	 */
	private ReferencesTableSettings producedArtefactSettings;
	
	/**
	 * Settings for subActivity ReferencesTable
	 */
	protected ReferencesTableSettings subActivitySettings;
	
	/**
	 * Settings for precedingActivity ReferencesTable
	 */
	private ReferencesTableSettings precedingActivitySettings;
	
	/**
	 * Settings for role ReferencesTable
	 */
	private ReferencesTableSettings roleSettings;
	
	/**
	 * Settings for applicableTechnique EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings applicableTechniqueSettings;
	
	/**
	 * Settings for ownedRel ReferencesTable
	 */
	protected ReferencesTableSettings ownedRelSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public RefActivityBasePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refActivity, String editing_mode) {
		super(editingContext, refActivity, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = RefframeworkViewsRepository.class;
		partKey = RefframeworkViewsRepository.RefActivity.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final RefActivity refActivity = (RefActivity)elt;
			final RefActivityPropertiesEditionPart basePart = (RefActivityPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refActivity.getId()));
			
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refActivity.getName()));
			
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.description))
				basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, refActivity.getDescription()));
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.objective))
				basePart.setObjective(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, refActivity.getObjective()));
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.scope))
				basePart.setScope(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, refActivity.getScope()));
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact)) {
				requiredArtefactSettings = new ReferencesTableSettings(refActivity, RefframeworkPackage.eINSTANCE.getRefActivity_RequiredArtefact());
				basePart.initRequiredArtefact(requiredArtefactSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.producedArtefact)) {
				producedArtefactSettings = new ReferencesTableSettings(refActivity, RefframeworkPackage.eINSTANCE.getRefActivity_ProducedArtefact());
				basePart.initProducedArtefact(producedArtefactSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.subActivity)) {
				subActivitySettings = new ReferencesTableSettings(refActivity, RefframeworkPackage.eINSTANCE.getRefActivity_SubActivity());
				basePart.initSubActivity(subActivitySettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.precedingActivity)) {
				precedingActivitySettings = new ReferencesTableSettings(refActivity, RefframeworkPackage.eINSTANCE.getRefActivity_PrecedingActivity());
				basePart.initPrecedingActivity(precedingActivitySettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.role)) {
				roleSettings = new ReferencesTableSettings(refActivity, RefframeworkPackage.eINSTANCE.getRefActivity_Role());
				basePart.initRole(roleSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique)) {
				// init part
				applicableTechniqueSettings = new EObjectFlatComboSettings(refActivity, RefframeworkPackage.eINSTANCE.getRefActivity_ApplicableTechnique());
				basePart.initApplicableTechnique(applicableTechniqueSettings);
				// set the button mode
				basePart.setApplicableTechniqueButtonMode(ButtonsModeEnum.BROWSE);
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.ownedRel)) {
				ownedRelSettings = new ReferencesTableSettings(refActivity, RefframeworkPackage.eINSTANCE.getRefActivity_OwnedRel());
				basePart.initOwnedRel(ownedRelSettings);
			}
			// init filters
			
			
			
			
			
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact)) {
				basePart.addFilterToRequiredArtefact(new EObjectFilter(RefframeworkPackage.Literals.REF_ARTEFACT));
				// Start of user code for additional businessfilters for requiredArtefact
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.producedArtefact)) {
				basePart.addFilterToProducedArtefact(new EObjectFilter(RefframeworkPackage.Literals.REF_ARTEFACT));
				// Start of user code for additional businessfilters for producedArtefact
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.subActivity)) {
				basePart.addFilterToSubActivity(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefActivity); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for subActivity
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.precedingActivity)) {
				basePart.addFilterToPrecedingActivity(new EObjectFilter(RefframeworkPackage.Literals.REF_ACTIVITY));
				// Start of user code for additional businessfilters for precedingActivity
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.role)) {
				basePart.addFilterToRole(new EObjectFilter(RefframeworkPackage.Literals.REF_ROLE));
				// Start of user code for additional businessfilters for role
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique)) {
				basePart.addFilterToApplicableTechnique(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefTechnique); //$NON-NLS-1$ 
					}
					
				});
				// Start of user code for additional businessfilters for applicableTechnique
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefActivity.Properties.ownedRel)) {
				basePart.addFilterToOwnedRel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefActivityRel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRel
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}















	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.objective) {
			return RefframeworkPackage.eINSTANCE.getRefActivity_Objective();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.scope) {
			return RefframeworkPackage.eINSTANCE.getRefActivity_Scope();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact) {
			return RefframeworkPackage.eINSTANCE.getRefActivity_RequiredArtefact();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.producedArtefact) {
			return RefframeworkPackage.eINSTANCE.getRefActivity_ProducedArtefact();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.subActivity) {
			return RefframeworkPackage.eINSTANCE.getRefActivity_SubActivity();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.precedingActivity) {
			return RefframeworkPackage.eINSTANCE.getRefActivity_PrecedingActivity();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.role) {
			return RefframeworkPackage.eINSTANCE.getRefActivity_Role();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique) {
			return RefframeworkPackage.eINSTANCE.getRefActivity_ApplicableTechnique();
		}
		if (editorKey == RefframeworkViewsRepository.RefActivity.Properties.ownedRel) {
			return RefframeworkPackage.eINSTANCE.getRefActivity_OwnedRel();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		RefActivity refActivity = (RefActivity)semanticObject;
		if (RefframeworkViewsRepository.RefActivity.Properties.id == event.getAffectedEditor()) {
			refActivity.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.name == event.getAffectedEditor()) {
			refActivity.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.description == event.getAffectedEditor()) {
			refActivity.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.objective == event.getAffectedEditor()) {
			refActivity.setObjective((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.scope == event.getAffectedEditor()) {
			refActivity.setScope((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof RefArtefact) {
					requiredArtefactSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				requiredArtefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				requiredArtefactSettings.move(event.getNewIndex(), (RefArtefact) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.producedArtefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof RefArtefact) {
					producedArtefactSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				producedArtefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				producedArtefactSettings.move(event.getNewIndex(), (RefArtefact) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.subActivity == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, subActivitySettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				subActivitySettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				subActivitySettings.move(event.getNewIndex(), (RefActivity) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.precedingActivity == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof RefActivity) {
					precedingActivitySettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				precedingActivitySettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				precedingActivitySettings.move(event.getNewIndex(), (RefActivity) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.role == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof RefRole) {
					roleSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				roleSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				roleSettings.move(event.getNewIndex(), (RefRole) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				applicableTechniqueSettings.setToReference((RefTechnique)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				RefTechnique eObject = RefframeworkFactory.eINSTANCE.createRefTechnique();
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, eObject, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(eObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy != null) {
						policy.execute();
					}
				}
				applicableTechniqueSettings.setToReference(eObject);
			}
		}
		if (RefframeworkViewsRepository.RefActivity.Properties.ownedRel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRelSettings.move(event.getNewIndex(), (RefActivityRel) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			RefActivityPropertiesEditionPart basePart = (RefActivityPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.description)){
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefActivity_Objective().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.objective)){
				if (msg.getNewValue() != null) {
					basePart.setObjective(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setObjective("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefActivity_Scope().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.scope)){
				if (msg.getNewValue() != null) {
					basePart.setScope(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setScope("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefActivity_RequiredArtefact().equals(msg.getFeature())  && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact))
				basePart.updateRequiredArtefact();
			if (RefframeworkPackage.eINSTANCE.getRefActivity_ProducedArtefact().equals(msg.getFeature())  && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.producedArtefact))
				basePart.updateProducedArtefact();
			if (RefframeworkPackage.eINSTANCE.getRefActivity_SubActivity().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.subActivity))
				basePart.updateSubActivity();
			if (RefframeworkPackage.eINSTANCE.getRefActivity_PrecedingActivity().equals(msg.getFeature())  && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.precedingActivity))
				basePart.updatePrecedingActivity();
			if (RefframeworkPackage.eINSTANCE.getRefActivity_Role().equals(msg.getFeature())  && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.role))
				basePart.updateRole();
			if (RefframeworkPackage.eINSTANCE.getRefActivity_ApplicableTechnique().equals(msg.getFeature()) && basePart != null && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique))
				basePart.setApplicableTechnique((EObject)msg.getNewValue());
			if (RefframeworkPackage.eINSTANCE.getRefActivity_OwnedRel().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefActivity.Properties.ownedRel))
				basePart.updateOwnedRel();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			RefframeworkPackage.eINSTANCE.getRefActivity_Objective(),
			RefframeworkPackage.eINSTANCE.getRefActivity_Scope(),
			RefframeworkPackage.eINSTANCE.getRefActivity_RequiredArtefact(),
			RefframeworkPackage.eINSTANCE.getRefActivity_ProducedArtefact(),
			RefframeworkPackage.eINSTANCE.getRefActivity_SubActivity(),
			RefframeworkPackage.eINSTANCE.getRefActivity_PrecedingActivity(),
			RefframeworkPackage.eINSTANCE.getRefActivity_Role(),
			RefframeworkPackage.eINSTANCE.getRefActivity_ApplicableTechnique(),
			RefframeworkPackage.eINSTANCE.getRefActivity_OwnedRel()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (RefframeworkViewsRepository.RefActivity.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefActivity.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefActivity.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefActivity.Properties.objective == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefActivity_Objective().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefActivity_Objective().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefActivity.Properties.scope == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefActivity_Scope().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefActivity_Scope().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
