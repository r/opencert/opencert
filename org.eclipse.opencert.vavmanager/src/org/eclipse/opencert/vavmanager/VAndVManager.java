/*******************************************************************************
 * Copyright (c) 2017 Honeywell, Inc.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Vit Koksa  <Vit.Koksa@honeywell.com>
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.vavmanager;

import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.emf.ecore.EObject;
//org.eclipse.papyrus.commands.wrappers.GMFtoEMFCommandWrapper;
//import org.eclipse.papyrus.infra.services.validation.commands.ValidateModelCommand;
import org.eclipse.papyrus.infra.services.validation.handler.AbstractCommandHandler;

/**
 * Handler for validating a model
 */
public class VAndVManager extends AbstractCommandHandler {
	private VAndVManagerCommand vavCmd;
	
	@Override
	protected Command getCommand() {
		// not useful to cache command, since selected element may change
		List<EObject> selectedElements = getSelectedElements();
		if (selectedElements == null) {
			return UnexecutableCommand.INSTANCE;
		}
		String name = selectedElements.get(0).getClass().getName();
		if (!name.endsWith("ClassImpl")) {
			return UnexecutableCommand.INSTANCE;
		}
		vavCmd = new VAndVManagerCommand(selectedElements.get(0));
		return GMFtoEMFCommandWrapper.wrap(vavCmd);
	}
}
