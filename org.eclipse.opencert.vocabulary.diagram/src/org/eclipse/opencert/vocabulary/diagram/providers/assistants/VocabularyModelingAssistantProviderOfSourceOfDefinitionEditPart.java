/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionEditPart;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyElementTypes;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyModelingAssistantProvider;

/**
 * @generated
 */
public class VocabularyModelingAssistantProviderOfSourceOfDefinitionEditPart
		extends VocabularyModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((SourceOfDefinitionEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(
			SourceOfDefinitionEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(1);
		types.add(VocabularyElementTypes.TermDefinedBy_4003);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((SourceOfDefinitionEditPart) targetEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(
			SourceOfDefinitionEditPart target, IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == VocabularyElementTypes.TermDefinedBy_4003) {
			types.add(VocabularyElementTypes.Term_2001);
		}
		return types;
	}

}
