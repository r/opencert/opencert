/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.opencert.chess.traceability.propertytab;


import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.jface.viewers.IFilter;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Component;
import org.polarsys.chess.chessmlprofile.util.Constants;


public class ComponentFilter implements IFilter {
	
	
	@Override
	public boolean select(Object toTest) {	
		return isSelectionOk(toTest);
	}
	
	public static boolean isSelectionOk(Object selection) {
		if(selection instanceof GraphicalEditPart){
		
			if(!(((GraphicalEditPart)selection).resolveSemanticElement() instanceof Class))
				return false;
			
			Class clazz = (Class) (((GraphicalEditPart)selection).resolveSemanticElement());
			
			if(clazz instanceof Component ||		
				clazz.getAppliedStereotype(Constants.BLOCK)!= null 
					){
				
				if (!AnalysisContextFilter.isSelectionOk(selection))
	
					return true;
			}
		}
		return false;
	}

}
