/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.opencert.infra.general.general.GeneralPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectFactory
 * @model kind="package"
 * @generated
 */
public interface AssuranceprojectPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "assuranceproject";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://assuranceproject/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "assuranceproject";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AssuranceprojectPackage eINSTANCE = org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceprojectPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl <em>Assurance Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceprojectPackageImpl#getAssuranceProject()
	 * @generated
	 */
	int ASSURANCE_PROJECT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Created By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__CREATED_BY = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Responsible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__RESPONSIBLE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__DATE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__VERSION = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Assets Package</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__ASSETS_PACKAGE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Permission Conf</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__PERMISSION_CONF = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Baseline Config</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__BASELINE_CONFIG = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Sub Project</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT__SUB_PROJECT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Assurance Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Assurance Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_PROJECT_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.PermissionConfigImpl <em>Permission Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.PermissionConfigImpl
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceprojectPackageImpl#getPermissionConfig()
	 * @generated
	 */
	int PERMISSION_CONFIG = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_CONFIG__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_CONFIG__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_CONFIG__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_CONFIG__IS_ACTIVE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Permission Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_CONFIG_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Permission Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_CONFIG_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssetsPackageImpl <em>Assets Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssetsPackageImpl
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceprojectPackageImpl#getAssetsPackage()
	 * @generated
	 */
	int ASSETS_PACKAGE = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_PACKAGE__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_PACKAGE__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_PACKAGE__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_PACKAGE__IS_ACTIVE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Artefacts Model</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_PACKAGE__ARTEFACTS_MODEL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Argumentation Model</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_PACKAGE__ARGUMENTATION_MODEL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Process Model</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_PACKAGE__PROCESS_MODEL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Assets Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_PACKAGE_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Assets Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_PACKAGE_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.BaselineConfigImpl <em>Baseline Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.BaselineConfigImpl
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceprojectPackageImpl#getBaselineConfig()
	 * @generated
	 */
	int BASELINE_CONFIG = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_CONFIG__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_CONFIG__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_CONFIG__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Compliance Map Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_CONFIG__COMPLIANCE_MAP_GROUP = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_CONFIG__IS_ACTIVE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ref Framework</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_CONFIG__REF_FRAMEWORK = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Baseline Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_CONFIG_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Baseline Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_CONFIG_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject <em>Assurance Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assurance Project</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject
	 * @generated
	 */
	EClass getAssuranceProject();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getCreatedBy <em>Created By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Created By</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getCreatedBy()
	 * @see #getAssuranceProject()
	 * @generated
	 */
	EAttribute getAssuranceProject_CreatedBy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getResponsible <em>Responsible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Responsible</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getResponsible()
	 * @see #getAssuranceProject()
	 * @generated
	 */
	EAttribute getAssuranceProject_Responsible();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getDate()
	 * @see #getAssuranceProject()
	 * @generated
	 */
	EAttribute getAssuranceProject_Date();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getVersion()
	 * @see #getAssuranceProject()
	 * @generated
	 */
	EAttribute getAssuranceProject_Version();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getAssetsPackage <em>Assets Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Assets Package</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getAssetsPackage()
	 * @see #getAssuranceProject()
	 * @generated
	 */
	EReference getAssuranceProject_AssetsPackage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getPermissionConf <em>Permission Conf</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Permission Conf</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getPermissionConf()
	 * @see #getAssuranceProject()
	 * @generated
	 */
	EReference getAssuranceProject_PermissionConf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getBaselineConfig <em>Baseline Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Baseline Config</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getBaselineConfig()
	 * @see #getAssuranceProject()
	 * @generated
	 */
	EReference getAssuranceProject_BaselineConfig();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getSubProject <em>Sub Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Project</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getSubProject()
	 * @see #getAssuranceProject()
	 * @generated
	 */
	EReference getAssuranceProject_SubProject();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.PermissionConfig <em>Permission Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Permission Config</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.PermissionConfig
	 * @generated
	 */
	EClass getPermissionConfig();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.PermissionConfig#isIsActive <em>Is Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Active</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.PermissionConfig#isIsActive()
	 * @see #getPermissionConfig()
	 * @generated
	 */
	EAttribute getPermissionConfig_IsActive();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage <em>Assets Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assets Package</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage
	 * @generated
	 */
	EClass getAssetsPackage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#isIsActive <em>Is Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Active</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#isIsActive()
	 * @see #getAssetsPackage()
	 * @generated
	 */
	EAttribute getAssetsPackage_IsActive();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#getArtefactsModel <em>Artefacts Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Artefacts Model</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#getArtefactsModel()
	 * @see #getAssetsPackage()
	 * @generated
	 */
	EReference getAssetsPackage_ArtefactsModel();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#getArgumentationModel <em>Argumentation Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Argumentation Model</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#getArgumentationModel()
	 * @see #getAssetsPackage()
	 * @generated
	 */
	EReference getAssetsPackage_ArgumentationModel();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#getProcessModel <em>Process Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Process Model</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#getProcessModel()
	 * @see #getAssetsPackage()
	 * @generated
	 */
	EReference getAssetsPackage_ProcessModel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig <em>Baseline Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Baseline Config</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig
	 * @generated
	 */
	EClass getBaselineConfig();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#getComplianceMapGroup <em>Compliance Map Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Compliance Map Group</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#getComplianceMapGroup()
	 * @see #getBaselineConfig()
	 * @generated
	 */
	EReference getBaselineConfig_ComplianceMapGroup();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#isIsActive <em>Is Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Active</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#isIsActive()
	 * @see #getBaselineConfig()
	 * @generated
	 */
	EAttribute getBaselineConfig_IsActive();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#getRefFramework <em>Ref Framework</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ref Framework</em>'.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#getRefFramework()
	 * @see #getBaselineConfig()
	 * @generated
	 */
	EReference getBaselineConfig_RefFramework();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AssuranceprojectFactory getAssuranceprojectFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl <em>Assurance Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl
		 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceprojectPackageImpl#getAssuranceProject()
		 * @generated
		 */
		EClass ASSURANCE_PROJECT = eINSTANCE.getAssuranceProject();

		/**
		 * The meta object literal for the '<em><b>Created By</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSURANCE_PROJECT__CREATED_BY = eINSTANCE.getAssuranceProject_CreatedBy();

		/**
		 * The meta object literal for the '<em><b>Responsible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSURANCE_PROJECT__RESPONSIBLE = eINSTANCE.getAssuranceProject_Responsible();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSURANCE_PROJECT__DATE = eINSTANCE.getAssuranceProject_Date();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSURANCE_PROJECT__VERSION = eINSTANCE.getAssuranceProject_Version();

		/**
		 * The meta object literal for the '<em><b>Assets Package</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSURANCE_PROJECT__ASSETS_PACKAGE = eINSTANCE.getAssuranceProject_AssetsPackage();

		/**
		 * The meta object literal for the '<em><b>Permission Conf</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSURANCE_PROJECT__PERMISSION_CONF = eINSTANCE.getAssuranceProject_PermissionConf();

		/**
		 * The meta object literal for the '<em><b>Baseline Config</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSURANCE_PROJECT__BASELINE_CONFIG = eINSTANCE.getAssuranceProject_BaselineConfig();

		/**
		 * The meta object literal for the '<em><b>Sub Project</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSURANCE_PROJECT__SUB_PROJECT = eINSTANCE.getAssuranceProject_SubProject();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.PermissionConfigImpl <em>Permission Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.PermissionConfigImpl
		 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceprojectPackageImpl#getPermissionConfig()
		 * @generated
		 */
		EClass PERMISSION_CONFIG = eINSTANCE.getPermissionConfig();

		/**
		 * The meta object literal for the '<em><b>Is Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERMISSION_CONFIG__IS_ACTIVE = eINSTANCE.getPermissionConfig_IsActive();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssetsPackageImpl <em>Assets Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssetsPackageImpl
		 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceprojectPackageImpl#getAssetsPackage()
		 * @generated
		 */
		EClass ASSETS_PACKAGE = eINSTANCE.getAssetsPackage();

		/**
		 * The meta object literal for the '<em><b>Is Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSETS_PACKAGE__IS_ACTIVE = eINSTANCE.getAssetsPackage_IsActive();

		/**
		 * The meta object literal for the '<em><b>Artefacts Model</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSETS_PACKAGE__ARTEFACTS_MODEL = eINSTANCE.getAssetsPackage_ArtefactsModel();

		/**
		 * The meta object literal for the '<em><b>Argumentation Model</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSETS_PACKAGE__ARGUMENTATION_MODEL = eINSTANCE.getAssetsPackage_ArgumentationModel();

		/**
		 * The meta object literal for the '<em><b>Process Model</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSETS_PACKAGE__PROCESS_MODEL = eINSTANCE.getAssetsPackage_ProcessModel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.BaselineConfigImpl <em>Baseline Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.BaselineConfigImpl
		 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceprojectPackageImpl#getBaselineConfig()
		 * @generated
		 */
		EClass BASELINE_CONFIG = eINSTANCE.getBaselineConfig();

		/**
		 * The meta object literal for the '<em><b>Compliance Map Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASELINE_CONFIG__COMPLIANCE_MAP_GROUP = eINSTANCE.getBaselineConfig_ComplianceMapGroup();

		/**
		 * The meta object literal for the '<em><b>Is Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASELINE_CONFIG__IS_ACTIVE = eINSTANCE.getBaselineConfig_IsActive();

		/**
		 * The meta object literal for the '<em><b>Ref Framework</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASELINE_CONFIG__REF_FRAMEWORK = eINSTANCE.getBaselineConfig_RefFramework();

	}

} //AssuranceprojectPackage
