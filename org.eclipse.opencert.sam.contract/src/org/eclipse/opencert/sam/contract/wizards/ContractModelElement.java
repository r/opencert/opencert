/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.wizards;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.ecore.EObject;


public class ContractModelElement {
	
	final public static String REQUIRES_SUPPORT = "requires";
	
	final public static String PROVIDES_SUPPORT = "provides";
	
	private EObject modelElement;
	private EObject enclosingModule;
	private CDOResource cdoResource;
	private String contractRole;
	
	public ContractModelElement()
	{
		
	}
	public EObject getModelElement() {
		return modelElement;
	}

	public void setModelElement(EObject modelElement) {
		this.modelElement = modelElement;
	}

	public EObject getEnclosingModule() {
		return enclosingModule;
	}

	public void setEnclosingModule(EObject enclosingModule) {
		this.enclosingModule = enclosingModule;
	}

	public CDOResource getCdoResource() {
		return cdoResource;
	}

	public void setCdoResource(CDOResource cdoResource) {
		this.cdoResource = cdoResource;
	}

	public String getContractRole() {
		return contractRole;
	}

	public void setContractRole(String contractRole) {
		this.contractRole = contractRole;
	}

	
	public ContractModelElement(EObject modelElement,EObject enclosingModule,CDOResource cdoResource,String contractRole)
	{
		this.modelElement = modelElement;
		this.enclosingModule = enclosingModule;
		this.cdoResource = cdoResource;
		this.contractRole = contractRole;
	}
	

}
