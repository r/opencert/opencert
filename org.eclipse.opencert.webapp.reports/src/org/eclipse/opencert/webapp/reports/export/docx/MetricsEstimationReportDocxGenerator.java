/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.export.docx;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.wml.P;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.listeners.metrics.MetricsEstimationTreeMenuListener;
import org.eclipse.opencert.webapp.reports.view.metrics.ArgumentationChartsMetrics;
import org.eclipse.opencert.webapp.reports.view.metrics.AssuranceChartsMetrics;
import org.eclipse.opencert.webapp.reports.view.metrics.BaselineChartsMetrics;
import org.eclipse.opencert.webapp.reports.view.metrics.EquivalenceChartsMetrics;
import org.eclipse.opencert.webapp.reports.view.metrics.MappingChartsMetrics;
import org.eclipse.opencert.webapp.reports.view.metrics.MonitorProcessChartsMetrics;
import org.eclipse.opencert.webapp.reports.view.metrics.RefframeworkChartsMetrics;
import org.eclipse.opencert.webapp.reports.view.metrics.ResourceEfficiencyChartsMetrics;
import org.eclipse.opencert.webapp.reports.view.metrics.TimeEfficiencyChartsMetrics;

import com.vaadin.data.Container.Hierarchical;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
public class MetricsEstimationReportDocxGenerator implements IDocxGenerator, MetricsEstimationTreeMenuListener
{
    private final String _docTemplateBasePath;
    private AssuranceProjectWrapper project;
    private String type;
    private String reportName;
    
    private static final String ALL_METRICS_TITLE = "All Metrics";
    private static final String BASELINE_METRICS_TITLE = "Baseline Metrics";
    private static final String BASELINE_METRICS_GOAL = "Goal: Plan of certification for a given standard - Traceability of Requirements Maintenance";
    private static final String MAPPING_METRICS_TITLE = "Mapping Metrics";
    private static final String MAPPING_METRICS_GOAL = "Goal: Maintenance of mappings";
    private static final String ASSURANCE_ASSET_METRICS_TITLE = "Assurance Asset Metrics";
    private static final String ASSURANCE_ASSET_METRICS_GOAL = "Goal: Monitor Assurance Assets";
    private static final String REFFRAMEWORK_METRICS_TITLE = "Refframework Metrics";
    private static final String REFFRAMEWORK_METRICS_GOAL = "Goal: Coverage of Assurance Project";
    private static final String PROCESS_METRICS_TITLE = "Process Metrics";
    private static final String MONITOR_PROCESS_METRICS_TITLE = "Monitor of Process";
    private static final String MONITOR_PROCESS_METRICS_GOAL = "Goal: Monitor of Assurance Process";
    private static final String TIME_EFFICIENCY_METRICS_TITLE = "Time Efficiency";
    private static final String TIME_EFFICIENCY_METRICS_GOAL = "Goal: Time Efficiency";
    private static final String RESOURCE_EFFICIENCY_METRICS_TITLE = "Resource Efficiency";
    private static final String RESOURCE_EFFICIENCY_METRICS_GOAL = "Goal: Resource Efficiency";
    private static final String ARGUMENTATION_METRICS_TITLE = "Argumentation Metrics";  
    private static final String ARGUMENTATION_METRICS_GOAL = "Goal: Monitor of Argumentation Construction"; 
    private static final String EQUIVALENCE_MAP_TITLE = "Equivalence Map Metrics";
    private static final String EQUIVALENCE_MAP_GOAL = "Goal: Maintenance of Mappings";
    private static final String EQUIVALENCE_MAP_TIP = "Tip: This metric is used to represent the status of each type of equivalence mappings.";
    private static final String BASELINE_METRICS_TIP = "Tip: These metrics are used to have an overall view of the Traceability of the Requirements Maintenance as well as the different kind of Requirements and its levels of criticality and applicability defined by the reference assurance framework.";
    private static final String MAPPING_METRICS_TIP = "Tip: This metric is used to represent the status of each type of compliance mappings.";
    private static final String ASSURANCE_ASSET_METRICS_TIP = "Tip: This metric is used for monitoring the activities related with Assurance Asset. The monitored activities include modification, creation, evaluation, revocation, etc.";
    private static final String REFFRAMEWORK_METRICS_TIP = "Tip: The metric of the coverage between the Baseline Framework and a Reference Framework is to visually represent the relationship among them.";
    private static final String PROCESS_METRICS_TIP = "Tip: The purpose of these metrics is to give a specific overview of the relations between the participants and the organizations with the work they have done. As well as the time evolution of the project.";
    private static final String ARGUMENTATION_METRICS_TIP = "Tip: These metrics are used to monitor the argumentation process and to help the user to know the status of safety argumentation, e.g., how many undeveloped safety claims are left or whether all the evidences are connected with the safety claims.";
	
    
    public MetricsEstimationReportDocxGenerator(String basePath)
    {
        _docTemplateBasePath = basePath;
        type=ALL_METRICS_TITLE;
        reportName = "MetricsReport_";
    }
    
    @Override
	public ByteArrayInputStream generateProjectBaselineReport(long baselineFrameworkID, long projectID) throws Docx4JException 
	{
		DocxBuilder docxBuilder = initDocxFromTemplate(baselineFrameworkID, projectID);
		
		fillDocxFromMetricsData(docxBuilder, baselineFrameworkID);

		final ByteArrayInputStream result = docxBuilder.getResult();
		return result;
	}

	protected DocxBuilder initDocxFromTemplate(long baselineFrameworkID, long projectID) throws Docx4JException
	{
	    final String templatePath = _docTemplateBasePath + File.separator + "MetricsEstimationReportTemplate.docx";
	    final DocxBuilder docxBuilder =  new DocxBuilder(new File(templatePath), baselineFrameworkID, projectID,true);
	    
	    // ugly fix - for unknown reasons *first* usage of HEADER does not work, the subsequent usages work
	   // docxBuilder.appendHeaderNL("", DocxBuilder.HeaderStyle.HEADER_5);
	    
	    return docxBuilder;
	}

	private void fillDocxFromMetricsData(DocxBuilder docxBuilder, long baselineFrameworkID) 
	{
		if (ALL_METRICS_TITLE.equals(type)) {
			baseline(docxBuilder, baselineFrameworkID);
			mapping(docxBuilder, baselineFrameworkID);
			assurance(docxBuilder);
			refframework(docxBuilder, baselineFrameworkID);
			docxBuilder.appendHeaderNL(PROCESS_METRICS_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
			final P p8 = docxBuilder.startParagraph();
			docxBuilder.appendText(p8, PROCESS_METRICS_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
			docxBuilder.appendNL(p8);
			docxBuilder.finishParagraph(p8);
			monitor(docxBuilder);
			time(docxBuilder);
			resource(docxBuilder);
			argumentation(docxBuilder);
			reportName = "Metrics_Estimation_Report_";
		}
		else if (BASELINE_METRICS_TITLE.equals(type)){		
			baseline(docxBuilder, baselineFrameworkID);
		}
		else if (ASSURANCE_ASSET_METRICS_TITLE.equals(type)){
			assurance(docxBuilder);
		}
		else if (REFFRAMEWORK_METRICS_TITLE.equals(type)){
			refframework(docxBuilder, baselineFrameworkID);
		}
		else if (MAPPING_METRICS_TITLE.equals(type)){
			mapping(docxBuilder, baselineFrameworkID);
		}
		else if (PROCESS_METRICS_TITLE.equals(type)){
			docxBuilder.appendHeaderNL(PROCESS_METRICS_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
			final P p8 = docxBuilder.startParagraph();
			docxBuilder.appendText(p8, PROCESS_METRICS_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
			docxBuilder.appendNL(p8);
			docxBuilder.finishParagraph(p8);
			monitor(docxBuilder);
			time(docxBuilder);
			resource(docxBuilder);
			reportName = "Process_Metrics_";
		}
		else if (MONITOR_PROCESS_METRICS_TITLE.equals(type)){
			monitor(docxBuilder);
		}
		else if (TIME_EFFICIENCY_METRICS_TITLE.equals(type)){
			time(docxBuilder);
		}
		else if (RESOURCE_EFFICIENCY_METRICS_TITLE.equals(type)){
			resource(docxBuilder);
		}
		else if (ARGUMENTATION_METRICS_TITLE.equals(type)){
			argumentation(docxBuilder);
		}
		
		
	}

	private void argumentation(DocxBuilder docxBuilder) {
		reportName = "Argumentation_Metrics_";
		ArgumentationChartsMetrics argCharts = new ArgumentationChartsMetrics(project);
		docxBuilder.appendHeaderNL(ARGUMENTATION_METRICS_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
		final P p7 = docxBuilder.startParagraph();
		docxBuilder.appendText(p7, ARGUMENTATION_METRICS_GOAL, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE);
		docxBuilder.finishParagraph(p7);
		
		final P p8 = docxBuilder.startParagraph();
		docxBuilder.appendText(p8, ARGUMENTATION_METRICS_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
		docxBuilder.appendNL(p8);
		docxBuilder.finishParagraph(p8);
		

		P pImg1 = docxBuilder.startParagraphAndAppendImage(ARGUMENTATION_METRICS_GOAL, argCharts.getGraphicalRepr(), argCharts.getArgumentationChart());
		docxBuilder.appendNL(pImg1);
		docxBuilder.finishParagraph(pImg1);
		
		fillDocxFromMetricsData(docxBuilder, argCharts.getDataDescription());
		final P bp = docxBuilder.startParagraph();
		docxBuilder.pageBreak(bp);
		docxBuilder.finishParagraph(bp);
	}

	private void resource(DocxBuilder docxBuilder) {
		reportName = "Resource_Metrics_";
		ResourceEfficiencyChartsMetrics resourceCharts = new ResourceEfficiencyChartsMetrics(project);
		docxBuilder.appendHeaderNL(RESOURCE_EFFICIENCY_METRICS_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
		final P p6 = docxBuilder.startParagraph();
		docxBuilder.appendText(p6, RESOURCE_EFFICIENCY_METRICS_GOAL, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE);
		if(type.equals(PROCESS_METRICS_TITLE)||type.equals(ALL_METRICS_TITLE))docxBuilder.appendNL(p6);
		docxBuilder.finishParagraph(p6);
		
		if(!type.equals(PROCESS_METRICS_TITLE) && !type.equals(ALL_METRICS_TITLE)){
			final P p8 = docxBuilder.startParagraph();
			docxBuilder.appendText(p8, PROCESS_METRICS_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
			docxBuilder.appendNL(p8);
			docxBuilder.finishParagraph(p8);
		}
		
		fillDocxFromMetricsData(docxBuilder, resourceCharts.getDataDescription());
		final P bp = docxBuilder.startParagraph();
		docxBuilder.pageBreak(bp);
		docxBuilder.finishParagraph(bp);
	}

	private void time(DocxBuilder docxBuilder) {
		reportName = "Time_Metrics_";
		TimeEfficiencyChartsMetrics timeCharts = new TimeEfficiencyChartsMetrics(project);
		docxBuilder.appendHeaderNL(TIME_EFFICIENCY_METRICS_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
		final P p5 = docxBuilder.startParagraph();
		docxBuilder.appendText(p5, TIME_EFFICIENCY_METRICS_GOAL, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE);
		if(type.equals(PROCESS_METRICS_TITLE)||type.equals(ALL_METRICS_TITLE))docxBuilder.appendNL(p5);
		docxBuilder.finishParagraph(p5);

		if(!type.equals(PROCESS_METRICS_TITLE) && !type.equals(ALL_METRICS_TITLE)){
			final P p8 = docxBuilder.startParagraph();
			docxBuilder.appendText(p8, PROCESS_METRICS_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
			docxBuilder.appendNL(p8);
			docxBuilder.finishParagraph(p8);
		}
		
		P pImg1 = docxBuilder.startParagraphAndAppendImage(TIME_EFFICIENCY_METRICS_GOAL, timeCharts.getGhantChartTitle(), timeCharts.getGantt());
		docxBuilder.appendNL(pImg1);
		docxBuilder.finishParagraph(pImg1);
		
		fillDocxFromMetricsData(docxBuilder, timeCharts.getDataDescription());
		final P bp = docxBuilder.startParagraph();
		docxBuilder.pageBreak(bp);
		docxBuilder.finishParagraph(bp);
	}

	private void monitor(DocxBuilder docxBuilder) {
		reportName = "Monitor_Metrics_";
		MonitorProcessChartsMetrics processMetrics = new MonitorProcessChartsMetrics(project);
		docxBuilder.appendHeaderNL(MONITOR_PROCESS_METRICS_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
		final P p4 = docxBuilder.startParagraph();
		docxBuilder.appendText(p4, MONITOR_PROCESS_METRICS_GOAL, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE);
		
		if(type.equals(PROCESS_METRICS_TITLE)||type.equals(ALL_METRICS_TITLE))docxBuilder.appendNL(p4);
		
		docxBuilder.finishParagraph(p4);
		
		if(!type.equals(PROCESS_METRICS_TITLE) && !type.equals(ALL_METRICS_TITLE)){
			final P p8 = docxBuilder.startParagraph();
			docxBuilder.appendText(p8, PROCESS_METRICS_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
			docxBuilder.appendNL(p8);
			docxBuilder.finishParagraph(p8);
		}
		
		P pImg1 = docxBuilder.startParagraphAndAppendImage(MONITOR_PROCESS_METRICS_GOAL, processMetrics.getParticipantsEvents(), processMetrics.getParticipant());
		docxBuilder.appendNL(pImg1);
		docxBuilder.finishParagraph(pImg1);
		
		P pImg2 = docxBuilder.startParagraphAndAppendImage(MONITOR_PROCESS_METRICS_GOAL, processMetrics.getParticipantsActivities(), processMetrics.getPerformance());
		docxBuilder.appendNL(pImg2);
		docxBuilder.finishParagraph(pImg2);
		
		P pImg3 = docxBuilder.startParagraphAndAppendImage(MONITOR_PROCESS_METRICS_GOAL, processMetrics.getWeeklyActivities(), processMetrics.getEvolution());
		docxBuilder.appendNL(pImg3);
		docxBuilder.finishParagraph(pImg3);
		final P bp = docxBuilder.startParagraph();
		docxBuilder.pageBreak(bp);
		docxBuilder.finishParagraph(bp);

	}

	private void refframework(DocxBuilder docxBuilder, long baselineFrameworkID) {
		reportName = "Refframework_Metrics_";
		RefframeworkChartsMetrics refframeworkCharts = new RefframeworkChartsMetrics(baselineFrameworkID);
		docxBuilder.appendHeaderNL(REFFRAMEWORK_METRICS_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
		final P p2 = docxBuilder.startParagraph();
		docxBuilder.appendText(p2, REFFRAMEWORK_METRICS_GOAL, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE);
	
		docxBuilder.finishParagraph(p2);
		
		final P p8 = docxBuilder.startParagraph();
		docxBuilder.appendText(p8, REFFRAMEWORK_METRICS_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
		docxBuilder.appendNL(p8);
		docxBuilder.finishParagraph(p8);
		
		P pImg1 = docxBuilder.startParagraphAndAppendImage(REFFRAMEWORK_METRICS_GOAL, refframeworkCharts.getRefframeworkCoverage(), refframeworkCharts.getCoverage());
		docxBuilder.appendNL(pImg1);
		docxBuilder.finishParagraph(pImg1);
		
		fillDocxFromMetricsData(docxBuilder, refframeworkCharts.getDataContainer());	
		final P bp = docxBuilder.startParagraph();
		docxBuilder.pageBreak(bp);
		docxBuilder.finishParagraph(bp);
		
	}

	private void assurance(DocxBuilder docxBuilder) {
		reportName = "Assurance_Metrics_";
		AssuranceChartsMetrics assuranceCharts = new AssuranceChartsMetrics(project);
		docxBuilder.appendHeaderNL(ASSURANCE_ASSET_METRICS_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
		final P p1 = docxBuilder.startParagraph();
		docxBuilder.appendText(p1, ASSURANCE_ASSET_METRICS_GOAL, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE);

		docxBuilder.finishParagraph(p1);
		
		final P p8 = docxBuilder.startParagraph();
		docxBuilder.appendText(p8, ASSURANCE_ASSET_METRICS_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
		docxBuilder.appendNL(p8);
		docxBuilder.finishParagraph(p8);
		
		P pImg1 = docxBuilder.startParagraphAndAppendImage(ASSURANCE_ASSET_METRICS_GOAL, assuranceCharts.getWeeklyEvidenceAssuranceChartTitle(), assuranceCharts.getWeeklyEvidence());
		docxBuilder.appendNL(pImg1);
		docxBuilder.finishParagraph(pImg1);
		
		P pImg2 = docxBuilder.startParagraphAndAppendImage(ASSURANCE_ASSET_METRICS_GOAL, assuranceCharts.getHistogramEvidenceAssuranceChartTitle(), assuranceCharts.getHistogramEvidence());
		docxBuilder.appendNL(pImg2);
		docxBuilder.finishParagraph(pImg2);
		
		P pImg3 = docxBuilder.startParagraphAndAppendImage(ASSURANCE_ASSET_METRICS_GOAL, assuranceCharts.getWeeklyProcessAssuranceChartTitle(), assuranceCharts.getWeeklyProcess());
		docxBuilder.appendNL(pImg3);
		docxBuilder.finishParagraph(pImg3);
		
		P pImg4 = docxBuilder.startParagraphAndAppendImage(ASSURANCE_ASSET_METRICS_GOAL, assuranceCharts.getHistogramProcessAssuranceChartTitle(), assuranceCharts.getHistogramProcess());
		docxBuilder.appendNL(pImg4);
		docxBuilder.finishParagraph(pImg4);
		final P bp = docxBuilder.startParagraph();
		docxBuilder.pageBreak(bp);
		docxBuilder.finishParagraph(bp);
	}

	private void mapping(DocxBuilder docxBuilder, long baselineFrameworkID) {
		reportName = "Mapping_Metrics_";
		MappingChartsMetrics mappingCharts = new MappingChartsMetrics(baselineFrameworkID);
		docxBuilder.appendHeaderNL(MAPPING_METRICS_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
		final P p3 = docxBuilder.startParagraph();
		docxBuilder.appendText(p3, MAPPING_METRICS_GOAL, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE);

		docxBuilder.finishParagraph(p3);
		
		final P p8 = docxBuilder.startParagraph();
		docxBuilder.appendText(p8, MAPPING_METRICS_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
		docxBuilder.appendNL(p8);
		docxBuilder.finishParagraph(p8);
		
		P pImg = docxBuilder.startParagraphAndAppendImage(MAPPING_METRICS_GOAL, mappingCharts.getComplianceMapChartTitle(), mappingCharts.getComplianceMapsChart());
		docxBuilder.appendNL(pImg);
		docxBuilder.finishParagraph(pImg);
		
		fillDocxFromMetricsData(docxBuilder, mappingCharts.getDataContainer());
		final P bp = docxBuilder.startParagraph();
		docxBuilder.pageBreak(bp);
		docxBuilder.finishParagraph(bp);
	}


	

	private void baseline(DocxBuilder docxBuilder, long baselineFrameworkID) {
		reportName = "Baseline_Metrics_";
		BaselineChartsMetrics baselineCharts = new BaselineChartsMetrics(baselineFrameworkID);
		docxBuilder.appendHeaderNL(BASELINE_METRICS_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
		final P p = docxBuilder.startParagraph();
		docxBuilder.appendText(p, BASELINE_METRICS_GOAL, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE);

		docxBuilder.finishParagraph(p);
		
		final P p8 = docxBuilder.startParagraph();
		docxBuilder.appendText(p8, BASELINE_METRICS_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
		docxBuilder.appendNL(p8);
		docxBuilder.finishParagraph(p8);
		
		P pImg = docxBuilder.startParagraphAndAppendImage(BASELINE_METRICS_GOAL, baselineCharts.getBaseRequirementsChartTitle(), baselineCharts.getNumberOfApplicabilityAndCriticalityLevelsChart());
		docxBuilder.appendNL(pImg);
		docxBuilder.finishParagraph(pImg);

		
		fillDocxFromMetricsData(docxBuilder, baselineCharts.getDataContainer());
		
		P pImg2 = docxBuilder.startParagraphAndAppendImage(BASELINE_METRICS_GOAL, baselineCharts.getBaseAssetChartTitle(), baselineCharts.getAssetsChart());
		docxBuilder.appendNL(pImg2);
		docxBuilder.finishParagraph(pImg2);

		final P bp = docxBuilder.startParagraph();
		docxBuilder.pageBreak(bp);
		docxBuilder.finishParagraph(bp);
		
	}

	

	
	
	private void fillDocxFromMetricsData(DocxBuilder docxBuilder, Hierarchical dataContainer) {
		Collection<?> allItemsCollection = dataContainer.getItemIds();
		Set<Object> itemIDsProcessed = new HashSet<>();
		
		Collection<?> propertyIds = dataContainer.getContainerPropertyIds();
		List<String> properties = new ArrayList<String>();
		
		for(Object o : propertyIds){
			properties.add(o.toString());
		}

		final P p2 = docxBuilder.startParagraph();
		docxBuilder.appendText(p2, "Detailed Description", EnumSet.of(DocxBuilder.FontStyle.UNDERLINED, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE, DocxBuilder.FontSize.BIG);
		docxBuilder.finishParagraph(p2);
		
		fillDocxFromMetricsTypeData(docxBuilder, dataContainer, allItemsCollection, itemIDsProcessed, properties, 1);
		
	}

	private void fillDocxFromMetricsTypeData(DocxBuilder docxBuilder, Hierarchical dataContainer, Collection<?> allItemsCollection, Set<Object> itemIDsProcessed, List<String> properties, int k) {

		for (Object itemID : allItemsCollection)
		{			
			if (itemIDsProcessed.contains(itemID)) {
				continue;
			}
			itemIDsProcessed.add(itemID);
			
			Item i = dataContainer.getItem(itemID);	
			
			Property<?> main = i.getItemProperty(properties.get(0));
			String s1 = (String)main.getValue();
			
			Property<?> sec = i.getItemProperty(properties.get(properties.size()-1));
			String s2 = (String)sec.getValue();
			
			fillDocxForLine(docxBuilder, s1, s2, k);
			Collection<?> children = dataContainer.getChildren(itemID);
			if (children == null || children.size() == 0) {
				continue;
			}
			fillDocxFromMetricsTypeData(docxBuilder, dataContainer, children, itemIDsProcessed, properties, k+1);
		}
		
	}
	
	

	private void fillDocxForLine(DocxBuilder docxBuilder, String s1, String s2, int k) {
		
		final P p = docxBuilder.startParagraph();
		String preappend = "  ";
		String sang = "    ";
		if(k>1){
			for(int i = 0; i<k; i++)
				preappend += "   ";
		}
		if(k==1){
			docxBuilder.appendNL(p);
			docxBuilder.appendText(p, s1, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE);		
		}
		else{
			if(k==2){
				docxBuilder.appendText(p,preappend, DocxBuilder.FontStyle.BOLD); //bold white space is bigger than simple one
				docxBuilder.appendText(p, s1, EnumSet.of(DocxBuilder.FontStyle.UNDERLINED, DocxBuilder.FontStyle.BOLD), null);
			}
			else if (k==3)
				docxBuilder.appendText(p, preappend+"-"+s1, DocxBuilder.FontStyle.BOLD);
			else 
				docxBuilder.appendText(p, preappend+" "+s1);
		}
		
		docxBuilder.appendNL(p);
		
		if(s2!=null && !s2.equals(""))
			docxBuilder.appendText(p, preappend+sang+s2, DocxBuilder.FontStyle.ITALIC);
		else 
			docxBuilder.appendText(p, preappend+sang+"There is no detailed description", EnumSet.of(DocxBuilder.FontStyle.ITALIC), DocxBuilder.FontColor.RED);
		
		
		docxBuilder.finishParagraph(p);
		
	}

	@Override
	public void treeElementUnselected(long baselineFrameworkID,
			AssuranceProjectWrapper project) {
		this.project=project;
		
	}

	@Override
	public void treeElementSelected(String type,
			AssuranceProjectWrapper project) {
		this.project=project;
		this.type=type;
		
	}

	@Override
	public String getReportName() {
		
		return reportName;  
	}

	@Override
	public InputStream generateRefFrameworkToRefFrameworkReport(long frombaselineFrameworkID, long tobaselineFrameworkID) throws Docx4JException {

		this.type="Equivalence Map";
		
		final String templatePath = _docTemplateBasePath + File.separator + "EquivalenceMapReportTemplate.docx";
	    final DocxBuilder docxBuilder =  new DocxBuilder(new File(templatePath), frombaselineFrameworkID, tobaselineFrameworkID,false);
		
		createEquivalenceDoc(docxBuilder, frombaselineFrameworkID, tobaselineFrameworkID);

		final ByteArrayInputStream result = docxBuilder.getResult();
		return result;
		
	}

	private void createEquivalenceDoc(DocxBuilder docxBuilder, long frombaselineFrameworkID, long tobaselineFrameworkID) {
		
		reportName = "Equivalence_Metrics_Report_";
		EquivalenceChartsMetrics equivalenceChartsMetrics = new EquivalenceChartsMetrics(frombaselineFrameworkID, tobaselineFrameworkID);
		docxBuilder.appendHeaderNL(EQUIVALENCE_MAP_TITLE, DocxBuilder.HeaderStyle.HEADER_1);
		final P p3 = docxBuilder.startParagraph();
		docxBuilder.appendText(p3, EQUIVALENCE_MAP_GOAL, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.BLUE);

		docxBuilder.finishParagraph(p3);
		
		final P p8 = docxBuilder.startParagraph();
		docxBuilder.appendText(p8, EQUIVALENCE_MAP_TIP, EnumSet.of(DocxBuilder.FontStyle.ITALIC), null, DocxBuilder.FontSize.SMALL);
		docxBuilder.appendNL(p8);
		docxBuilder.finishParagraph(p8);
		
		
		P pImg = docxBuilder.startParagraphAndAppendImage(EQUIVALENCE_MAP_GOAL, equivalenceChartsMetrics.getRefframeworkMaps(), equivalenceChartsMetrics.getChart());
		docxBuilder.appendNL(pImg);
		docxBuilder.finishParagraph(pImg);
		
		fillDocxFromMetricsData(docxBuilder, equivalenceChartsMetrics.getDataDescription());
		final P bp = docxBuilder.startParagraph();
		docxBuilder.pageBreak(bp);
		docxBuilder.finishParagraph(bp);
		
	}
}
