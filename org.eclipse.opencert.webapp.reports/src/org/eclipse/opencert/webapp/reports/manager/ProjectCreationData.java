/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.opencert.webapp.reports.util.IdNameDesc;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

public class ProjectCreationData
{
    public long newProjectID;
    private Map<IdNameDesc, BaseArtefact> resultMap;
    private Map<Long, Long> oldIdToNewIdMap;
    private boolean idMapInitialized = false;
    
    public ProjectCreationData()
    {
        resultMap = new HashMap<>();
        oldIdToNewIdMap = new HashMap<>();
    }

    public Iterator<Entry<IdNameDesc, BaseArtefact>> getResultMapping()
    {
        return resultMap.entrySet().iterator();
    }

    public void addCreatedArtefact(IdNameDesc artefactToCreate, BaseArtefact createdBaseArtefact)
    {
        resultMap.put(artefactToCreate, createdBaseArtefact);
    }
    
    public Long getNewIDForOldID(Long oldRelatedID)
    {
        if (!idMapInitialized) 
        {
            Iterator<Entry<IdNameDesc, BaseArtefact>> iterator = resultMap.entrySet().iterator();
            while (iterator.hasNext())
            {
                Entry<IdNameDesc, BaseArtefact> next = iterator.next();
                final Long oldID = next.getKey().oldCDOId;
                final Long newID = CDOStorageUtil.getCDOId(next.getValue());
                oldIdToNewIdMap.put(oldID, newID);
            }
            idMapInitialized = true;
        }

        return oldIdToNewIdMap.get(oldRelatedID);
    }

    public long getNewProjectID()
    {
        return newProjectID;
    }

    public void setNewProjectID(long newProjectID)
    {
        this.newProjectID = newProjectID;
    }
}
