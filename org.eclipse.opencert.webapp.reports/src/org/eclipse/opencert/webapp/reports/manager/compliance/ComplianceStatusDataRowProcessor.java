/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.eclipse.opencert.webapp.reports.containers.BaseAssetComplianceStatus;
import org.eclipse.opencert.webapp.reports.exttools.connectors.NotDefinedConnector;
import org.eclipse.opencert.webapp.reports.exttools.connectors.NotNeededConnector;
import org.eclipse.opencert.webapp.reports.listeners.ExternalToolResultClickedListener;
import org.eclipse.opencert.webapp.reports.util.BaselineElementType;
import org.eclipse.opencert.webapp.reports.util.ComplianceStatusCalculator;
import org.eclipse.opencert.webapp.reports.util.ComplianceStatusType;
import org.eclipse.opencert.webapp.reports.util.ICommonCssStyles;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.externaltools.api.ExternalToolQuery;
import org.eclipse.opencert.externaltools.api.ExternalToolQueryResult;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQueryDAO;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQueryDAOImpl;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

public class ComplianceStatusDataRowProcessor
        implements IDataRowProcessor<BaseAssetComplianceStatus>
{
    private static final String NO_IA_ACTIVITIES_DESCRIPTION = "Not applicable";
    private static final String ARTEFACT = " artefact";
    private static final String ARTEFACTS = ARTEFACT + "s";
    private static final String REQUIRE = " require";
    private static final String REQUIRES = REQUIRE + "s";
    
    private static final String IA_ATTENTION_DESCRIPTION =  " attention due to Impact Analysis invalidation";
    private static final String NO_IA_ARTEFACTS_REQUIRE_ATTENTION_DESCRIPTION = "None of artefacts require attention due to Impact Analysis invalidation";
    private static final String NO_IA_ARTEFACTS = "No artefacts available for this entry";
    private static final String BASELINE_ARTEFACT_STATUS_DESC = "No compliance evidence provided yet.<br> In order to add evidence file, <br>"
            + "please select <b>Baseline Element</b> and <br> press <b>Upload</b> button on the right panel.";

    private ComplianceStatusCalculator complianceStatusCalculator = new ComplianceStatusCalculator();
    private ExternalToolResultClickedListener externalToolResultClickedListener;
    private boolean isShowExternalToolsPanels;

    public ComplianceStatusDataRowProcessor(boolean showExternalToolsPanels, ExternalToolResultClickedListener externalToolResultClickedListener)
    {
        isShowExternalToolsPanels = showExternalToolsPanels;
        this.externalToolResultClickedListener = externalToolResultClickedListener;
    }

    public ComplianceStatusDataRowProcessor(boolean showExternalToolsPanels)
    {
        isShowExternalToolsPanels = showExternalToolsPanels;
    }

    private void createClickListenerForExternalToolResultButton(Button externalToolResultButton, BaseAssetComplianceStatus bAssetComplianceStatus)
    {
        externalToolResultButton.addClickListener((e) -> {
            bAssetComplianceStatus.setExternalToolResultColumnClicked(true);
            externalToolResultClickedListener.externalResultClicked(bAssetComplianceStatus.getItemId());
        });
    }

    private Button generateExternalToolResult(long baseElementId)
    {
        ExternalToolQueryDAO externalToolQueryDAO = (ExternalToolQueryDAO) SpringContextHelper.getBeanFromWebappContext(ExternalToolQueryDAOImpl.SPRING_NAME);

        ExternalToolQuery externalToolQuery = externalToolQueryDAO.findRecentByBaseElementId(baseElementId);

        Button resultButton = new Button();

        if (externalToolQuery == null) {
            resultButton.setCaption("Not defined");
            resultButton.setDescription("In order to specify External Tool Connector, <br>please select <b>Baseline Element</b>,<br> "
                    + "go to the below <b>External Tool Configuration</b> tab and <br> click <b>Add Connector</b> button.");
            setStyleForStatusOfExternalToolResult(resultButton, externalToolQuery);
            return resultButton;
        }

        Set<ExternalToolQueryResult> externalToolQueryResults = externalToolQuery.getExternalToolQueryResults();

        if (externalToolQueryResults.size() < 1) {
            if (externalToolQuery.getExternalToolConnectorId().equals(NotNeededConnector.class.getName())) {
                resultButton.setCaption("Not needed");
                resultButton.setDescription("External Tool Connector has been marked as not needed for this Baseline Element");
            } else if (externalToolQuery.getExternalToolConnectorId().equals(NotDefinedConnector.class.getName())) {
                resultButton.setCaption("Not defined");
                resultButton.setDescription("In order to specify External Tool Connector, <br>please select <b>Baseline Element</b>,<br> "
                        + "go to the below <b>External Tool Configuration</b> tab and <br> click <b>Add Connector</b> button.");
            } else {
                resultButton.setCaption("Not executed yet");
                resultButton.setDescription("In order to execute the defined External Tool Connector, <br>please select <b>Baseline Element</b>,<br> "
                        + "go to the below <b>External Tool Configuration</b> tab and <br> click <b>Execute Connector</b> button.");
            }
            setStyleForStatusOfExternalToolResult(resultButton, externalToolQuery);
            return resultButton;
        }

        ExternalToolQueryResult externalToolQueryResult = (ExternalToolQueryResult) externalToolQueryResults.toArray()[0];
        resultButton.setCaption("" + externalToolQueryResult.getResult());
        setStyleForStatusOfExternalToolResult(resultButton, externalToolQuery, externalToolQueryResult.getDate());
        return resultButton;
    }

    private void setStyleForStatusOfExternalToolResult(Button resultButton, ExternalToolQuery externalToolQuery, Date date)
    {
        resultButton.setStyleName("externalToolButton_empty " + ICommonCssStyles.EXTERNAL_TOOL_COMMON_BUTTON);

        if (externalToolQuery == null || externalToolQuery.getExternalToolQueryResults().size() < 1) {
            return;
        }

        int result = Integer.parseInt(resultButton.getCaption());

        resultButton.setStyleName("externalToolButton_none " + ICommonCssStyles.EXTERNAL_TOOL_COMMON_BUTTON);
        final String descriptionPrefix = "<b>Result:</b> " + result + "<br> <b>Message:</b> ";

        if (result < externalToolQuery.getGreenStatusMaxValue()) {
            resultButton.setStyleName("externalToolButton_green " + ICommonCssStyles.EXTERNAL_TOOL_COMMON_BUTTON);
            final String description = descriptionPrefix + externalToolQuery.getGreenStatusMsg();
            resultButton.setDescription(addDateToDescription(description, date));
        }

        if (externalToolQuery.isYellowEnabled() && result < externalToolQuery.getYellowStatusMaxValue() && result > externalToolQuery.getYellowStatusMinValue()) {
            resultButton.setStyleName("externalToolButton_yellow " + ICommonCssStyles.EXTERNAL_TOOL_COMMON_BUTTON);
            final String description = descriptionPrefix + externalToolQuery.getYellowStatusMsg();
            resultButton.setDescription(addDateToDescription(description, date));
        }

        if (result > externalToolQuery.getRedStatusMinValue()) {
            resultButton.setStyleName("externalToolButton_red " + ICommonCssStyles.EXTERNAL_TOOL_COMMON_BUTTON);
            final String description = descriptionPrefix + externalToolQuery.getRedStatusMsg();
            resultButton.setDescription(addDateToDescription(description, date));
        }
    }

    private void setStyleForStatusOfExternalToolResult(Button resultButton, ExternalToolQuery externalToolQuery)
    {
        setStyleForStatusOfExternalToolResult(resultButton, externalToolQuery, null);
    }

    private String addDateToDescription(String description, Date date)
    {
        if (date != null) {
            description += "<br><b>Execution date:</b> ";

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            try {
                description += df.format(date.getTime());
            } catch (Exception ex) {
                OpencertLogger.error(ex);
            }
        }

        return description;
    }

    @Override
    public BaseAssetComplianceStatus createBaseAssetDataRow(long itemId, BaseAssurableElement baseAsset, BaseAssetComplianceStatus parentDataRow, Image img)
    {
        final ComplianceStatusType complianceStatusType = complianceStatusCalculator.calculateComplianceStatusForBaseElement(baseAsset);
        
        Label generateComplianceStatusLabel;
        Label iaStatusLabel;
        if (baseAsset instanceof BaseArtefact) 
        {
            generateComplianceStatusLabel = generateComplianceStatusLabel(complianceStatusType, BaselineElementType.BASE_ARTEFACT);
            iaStatusLabel = generateIAStatusForBaseArtefact(baseAsset);
            
        } else if (baseAsset instanceof BaseActivity) 
        {
            generateComplianceStatusLabel = generateComplianceStatusLabel(complianceStatusType, BaselineElementType.BASE_ACTIVITY);
            iaStatusLabel = generateIAStatusForBaseActivity();
            
        } else if (baseAsset instanceof BaseRequirement) 
        {
        	generateComplianceStatusLabel = generateComplianceStatusLabel(complianceStatusType, BaselineElementType.BASE_REQUIREMENT);
        	iaStatusLabel = generateIAStatusForBaseArtefact(baseAsset);
        } else throw new IllegalArgumentException();
        
        final Label nameLabel = createComplianceAssetNameLabel(baseAsset);
        final Button externalToolResultButton = generateExternalToolResult(itemId);
        BaseAssetComplianceStatus bAssetComplianceStatus = new BaseAssetComplianceStatus(itemId, img, nameLabel, externalToolResultButton);
        bAssetComplianceStatus.setStatus(generateComplianceStatusLabel);
        bAssetComplianceStatus.setIAStatus(iaStatusLabel);
        bAssetComplianceStatus.setBaseAssurableElement(baseAsset);
        bAssetComplianceStatus.setParent(parentDataRow);

        createClickListenerForExternalToolResultButton(externalToolResultButton, bAssetComplianceStatus);

        return bAssetComplianceStatus;
    }
    
    private Label generateComplianceStatusLabel(ComplianceStatusType complianceStatusType, String baselineElementType)
    {
        Label complianceStatusLabel = new Label(complianceStatusType.getValue());

        if (complianceStatusType.equals(ComplianceStatusType.OK)) {
            complianceStatusLabel.setStyleName("complianceStatusLabel_ok " + ICommonCssStyles.COMPLIANCE_STATUS_COMMON_LABEL);
        } else if (complianceStatusType.equals(ComplianceStatusType.PARTLY)) {
            complianceStatusLabel.setStyleName("complianceStatusLabel_partly " + ICommonCssStyles.COMPLIANCE_STATUS_COMMON_LABEL);
        } else if (complianceStatusType.equals(ComplianceStatusType.NO)) {
            complianceStatusLabel.setStyleName("complianceStatusLabel_no " + ICommonCssStyles.COMPLIANCE_STATUS_COMMON_LABEL);
        } else {
            complianceStatusLabel.setStyleName("complianceStatusLabel_empty " + ICommonCssStyles.COMPLIANCE_STATUS_COMMON_LABEL);
        }

        if (complianceStatusType.equals(ComplianceStatusType.EMPTY) && BaselineElementType.BASE_ARTEFACT.equals(baselineElementType)) {
            complianceStatusLabel.setDescription(BASELINE_ARTEFACT_STATUS_DESC);
        } else {
            complianceStatusLabel.setDescription(complianceStatusType.getDescription());
        }

        return complianceStatusLabel;
    }
    
    
    private Label generateIAStatusForBaseArtefact(BaseAssurableElement baseAssurableElement)
    {
        Label statusLabel = null;
        int artefactAffectedByIA = 0;
        int artefactsDetected = 0;
        
        for (BaseComplianceMap baseComplianceMap : baseAssurableElement.getComplianceMap()) {
            for (AssuranceAsset assuranceAsset : baseComplianceMap.getTarget()) {
                if (assuranceAsset instanceof Artefact) {
                    artefactsDetected++;
                    IAEvaluation iaEvaluation = IAEvaluation.createForArtefact((Artefact)assuranceAsset);
                    switch (iaEvaluation.getEvaluationStatus()) {
                        case NEEDS_VALIDATION : {
                            artefactAffectedByIA++;
                            break;
                        }
                        
                        case NEEDS_MODIFICATION : {
                            artefactAffectedByIA++;
                            break;
                        }
                        
                        case REVOKED : {
                            artefactAffectedByIA++;
                            break;
                        }
                        
                        default :
                            break;
                    }
                }
            }
        }
        
        if (artefactsDetected == 0) {
            statusLabel = new Label("-");
            statusLabel.setStyleName("complianceStatusLabel_empty " + ICommonCssStyles.COMPLIANCE_STATUS_COMMON_LABEL);
            statusLabel.setDescription(NO_IA_ARTEFACTS);
            return statusLabel;
        }
        
        if (artefactAffectedByIA == 0) {
            statusLabel = new Label(" ");
            statusLabel.setStyleName("complianceStatusLabel_ok " + ICommonCssStyles.COMPLIANCE_STATUS_COMMON_LABEL);
            statusLabel.setDescription(NO_IA_ARTEFACTS_REQUIRE_ATTENTION_DESCRIPTION);
            return statusLabel;
        }
        
        statusLabel = new Label(String.valueOf(artefactAffectedByIA));
        statusLabel.setStyleName("complianceStatusLabel_no " + ICommonCssStyles.COMPLIANCE_STATUS_COMMON_LABEL);
        
        String description = String.valueOf(artefactAffectedByIA);
        if (artefactAffectedByIA > 1) {
            description += ARTEFACTS + REQUIRE;
        } else {
            description += ARTEFACT + REQUIRES;
        }
        description += IA_ATTENTION_DESCRIPTION;
        
        statusLabel.setDescription(description);
        
        return statusLabel;
    }
    
    
    private Label generateIAStatusForBaseActivity()
    {
        Label statusLabel = new Label("-");
        statusLabel.setStyleName("complianceStatusLabel_empty " + ICommonCssStyles.COMPLIANCE_STATUS_COMMON_LABEL);
        statusLabel.setDescription(NO_IA_ACTIVITIES_DESCRIPTION);
        
        return statusLabel;
    }
    

    @Override
    @SuppressWarnings("unchecked")
    public void setItemPropsFromDataRow(Item item, BaseAssetComplianceStatus baseAssetComplianceStatus)
    {
        item.getItemProperty(IComplianceConsts.TYPE_PROPERTY_ID).setValue(baseAssetComplianceStatus.getType());
        item.getItemProperty(IComplianceConsts.NAME_PROPERTY_ID).setValue(baseAssetComplianceStatus.getName());
        item.getItemProperty(IComplianceConsts.STATUS_PROPERTY_ID).setValue(baseAssetComplianceStatus.getStatus());
        item.getItemProperty(IComplianceConsts.IA_STATUS_PROPERTY_ID).setValue(baseAssetComplianceStatus.getIAStatus());
        
        if (isShowExternalToolsPanels) {
            item.getItemProperty(IComplianceConsts.EXTERNAL_TOOL_PROPERTY_ID).setValue(baseAssetComplianceStatus.getExternalToolResult());
        }
    }
 
    @Override
    public void generateParentChildRelationsInContainer(HierarchicalContainer hierarchicalContainer)
    {
        for (Object baseAssetCompliance : hierarchicalContainer.getItemIds()) {
            if (baseAssetCompliance instanceof BaseAssetComplianceStatus) {
                if (((BaseAssetComplianceStatus) baseAssetCompliance).getParent() != null) {
                    hierarchicalContainer.setParent((BaseAssetComplianceStatus) baseAssetCompliance,
                            ((BaseAssetComplianceStatus) baseAssetCompliance).getParent());
                }
            }
        }
    }


    @Override
    public void initEmptyTableDataContainer(HierarchicalContainer hc)
    {
        hc.addContainerProperty(IComplianceConsts.TYPE_PROPERTY_ID, Image.class, null);
        hc.addContainerProperty(IComplianceConsts.NAME_PROPERTY_ID, Label.class, "");
        hc.addContainerProperty(IComplianceConsts.STATUS_PROPERTY_ID, Label.class, "");
        hc.addContainerProperty(IComplianceConsts.IA_STATUS_PROPERTY_ID, Label.class, "");
        if (isShowExternalToolsPanels) {
            hc.addContainerProperty(IComplianceConsts.EXTERNAL_TOOL_PROPERTY_ID, Button.class, "");
        }
    }

    @Override
    public boolean supportsActivities()
    {
        return true;
    }
}
