/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;
import org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart;

import org.eclipse.opencert.sam.arg.arg.providers.ArgMessages;

// End of user code

/**
 * 
 * 
 */
public class Case_PropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, Case_PropertiesEditionPart {

	protected ReferencesTable argument;
	protected List<ViewerFilter> argumentBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> argumentFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable argumentation;
	protected List<ViewerFilter> argumentationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> argumentationFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable agreement;
	protected List<ViewerFilter> agreementBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> agreementFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable cited;
	protected List<ViewerFilter> citedBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> citedFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable information;
	protected List<ViewerFilter> informationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> informationFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public Case_PropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public Case_PropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence case_Step = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = case_Step.addStep(ArgViewsRepository.Case_.Properties.class);
		propertiesStep.addStep(ArgViewsRepository.Case_.Properties.argument);
		propertiesStep.addStep(ArgViewsRepository.Case_.Properties.argumentation);
		propertiesStep.addStep(ArgViewsRepository.Case_.Properties.agreement);
		propertiesStep.addStep(ArgViewsRepository.Case_.Properties.cited);
		propertiesStep.addStep(ArgViewsRepository.Case_.Properties.information);
		
		
		composer = new PartComposer(case_Step) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ArgViewsRepository.Case_.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Case_.Properties.argument) {
					return createArgumentTableComposition(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Case_.Properties.argumentation) {
					return createArgumentationTableComposition(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Case_.Properties.agreement) {
					return createAgreementTableComposition(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Case_.Properties.cited) {
					return createCitedTableComposition(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Case_.Properties.information) {
					return createInformationTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(ArgMessages.Case_PropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArgumentTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.argument = new ReferencesTable(getDescription(ArgViewsRepository.Case_.Properties.argument, ArgMessages.Case_PropertiesEditionPart_ArgumentLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.argument, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				argument.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.argument, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				argument.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.argument, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				argument.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.argument, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				argument.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.argumentFilters) {
			this.argument.addFilter(filter);
		}
		this.argument.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Case_.Properties.argument, ArgViewsRepository.FORM_KIND));
		this.argument.createControls(parent, widgetFactory);
		this.argument.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.argument, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData argumentData = new GridData(GridData.FILL_HORIZONTAL);
		argumentData.horizontalSpan = 3;
		this.argument.setLayoutData(argumentData);
		this.argument.setLowerBound(0);
		this.argument.setUpperBound(-1);
		argument.setID(ArgViewsRepository.Case_.Properties.argument);
		argument.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createArgumentTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArgumentationTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.argumentation = new ReferencesTable(getDescription(ArgViewsRepository.Case_.Properties.argumentation, ArgMessages.Case_PropertiesEditionPart_ArgumentationLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.argumentation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				argumentation.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.argumentation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				argumentation.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.argumentation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				argumentation.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.argumentation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				argumentation.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.argumentationFilters) {
			this.argumentation.addFilter(filter);
		}
		this.argumentation.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Case_.Properties.argumentation, ArgViewsRepository.FORM_KIND));
		this.argumentation.createControls(parent, widgetFactory);
		this.argumentation.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.argumentation, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData argumentationData = new GridData(GridData.FILL_HORIZONTAL);
		argumentationData.horizontalSpan = 3;
		this.argumentation.setLayoutData(argumentationData);
		this.argumentation.setLowerBound(0);
		this.argumentation.setUpperBound(-1);
		argumentation.setID(ArgViewsRepository.Case_.Properties.argumentation);
		argumentation.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createArgumentationTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createAgreementTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.agreement = new ReferencesTable(getDescription(ArgViewsRepository.Case_.Properties.agreement, ArgMessages.Case_PropertiesEditionPart_AgreementLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.agreement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				agreement.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.agreement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				agreement.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.agreement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				agreement.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.agreement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				agreement.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.agreementFilters) {
			this.agreement.addFilter(filter);
		}
		this.agreement.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Case_.Properties.agreement, ArgViewsRepository.FORM_KIND));
		this.agreement.createControls(parent, widgetFactory);
		this.agreement.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.agreement, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData agreementData = new GridData(GridData.FILL_HORIZONTAL);
		agreementData.horizontalSpan = 3;
		this.agreement.setLayoutData(agreementData);
		this.agreement.setLowerBound(0);
		this.agreement.setUpperBound(-1);
		agreement.setID(ArgViewsRepository.Case_.Properties.agreement);
		agreement.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createAgreementTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createCitedTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.cited = new ReferencesTable(getDescription(ArgViewsRepository.Case_.Properties.cited, ArgMessages.Case_PropertiesEditionPart_CitedLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.cited, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				cited.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.cited, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				cited.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.cited, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				cited.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.cited, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				cited.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.citedFilters) {
			this.cited.addFilter(filter);
		}
		this.cited.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Case_.Properties.cited, ArgViewsRepository.FORM_KIND));
		this.cited.createControls(parent, widgetFactory);
		this.cited.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.cited, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData citedData = new GridData(GridData.FILL_HORIZONTAL);
		citedData.horizontalSpan = 3;
		this.cited.setLayoutData(citedData);
		this.cited.setLowerBound(0);
		this.cited.setUpperBound(-1);
		cited.setID(ArgViewsRepository.Case_.Properties.cited);
		cited.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createCitedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createInformationTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.information = new ReferencesTable(getDescription(ArgViewsRepository.Case_.Properties.information, ArgMessages.Case_PropertiesEditionPart_InformationLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.information, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				information.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.information, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				information.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.information, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				information.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.information, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				information.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.informationFilters) {
			this.information.addFilter(filter);
		}
		this.information.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Case_.Properties.information, ArgViewsRepository.FORM_KIND));
		this.information.createControls(parent, widgetFactory);
		this.information.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(Case_PropertiesEditionPartForm.this, ArgViewsRepository.Case_.Properties.information, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData informationData = new GridData(GridData.FILL_HORIZONTAL);
		informationData.horizontalSpan = 3;
		this.information.setLayoutData(informationData);
		this.information.setLowerBound(0);
		this.information.setUpperBound(-1);
		information.setID(ArgViewsRepository.Case_.Properties.information);
		information.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createInformationTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#initArgument(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initArgument(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		argument.setContentProvider(contentProvider);
		argument.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Case_.Properties.argument);
		if (eefElementEditorReadOnlyState && argument.isEnabled()) {
			argument.setEnabled(false);
			argument.setToolTipText(ArgMessages.Case__ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !argument.isEnabled()) {
			argument.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#updateArgument()
	 * 
	 */
	public void updateArgument() {
	argument.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#addFilterArgument(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArgument(ViewerFilter filter) {
		argumentFilters.add(filter);
		if (this.argument != null) {
			this.argument.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#addBusinessFilterArgument(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArgument(ViewerFilter filter) {
		argumentBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#isContainedInArgumentTable(EObject element)
	 * 
	 */
	public boolean isContainedInArgumentTable(EObject element) {
		return ((ReferencesTableSettings)argument.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#initArgumentation(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initArgumentation(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		argumentation.setContentProvider(contentProvider);
		argumentation.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Case_.Properties.argumentation);
		if (eefElementEditorReadOnlyState && argumentation.isEnabled()) {
			argumentation.setEnabled(false);
			argumentation.setToolTipText(ArgMessages.Case__ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !argumentation.isEnabled()) {
			argumentation.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#updateArgumentation()
	 * 
	 */
	public void updateArgumentation() {
	argumentation.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#addFilterArgumentation(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArgumentation(ViewerFilter filter) {
		argumentationFilters.add(filter);
		if (this.argumentation != null) {
			this.argumentation.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#addBusinessFilterArgumentation(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArgumentation(ViewerFilter filter) {
		argumentationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#isContainedInArgumentationTable(EObject element)
	 * 
	 */
	public boolean isContainedInArgumentationTable(EObject element) {
		return ((ReferencesTableSettings)argumentation.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#initAgreement(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initAgreement(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		agreement.setContentProvider(contentProvider);
		agreement.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Case_.Properties.agreement);
		if (eefElementEditorReadOnlyState && agreement.isEnabled()) {
			agreement.setEnabled(false);
			agreement.setToolTipText(ArgMessages.Case__ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !agreement.isEnabled()) {
			agreement.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#updateAgreement()
	 * 
	 */
	public void updateAgreement() {
	agreement.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#addFilterAgreement(ViewerFilter filter)
	 * 
	 */
	public void addFilterToAgreement(ViewerFilter filter) {
		agreementFilters.add(filter);
		if (this.agreement != null) {
			this.agreement.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#addBusinessFilterAgreement(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToAgreement(ViewerFilter filter) {
		agreementBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#isContainedInAgreementTable(EObject element)
	 * 
	 */
	public boolean isContainedInAgreementTable(EObject element) {
		return ((ReferencesTableSettings)agreement.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#initCited(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initCited(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		cited.setContentProvider(contentProvider);
		cited.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Case_.Properties.cited);
		if (eefElementEditorReadOnlyState && cited.isEnabled()) {
			cited.setEnabled(false);
			cited.setToolTipText(ArgMessages.Case__ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !cited.isEnabled()) {
			cited.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#updateCited()
	 * 
	 */
	public void updateCited() {
	cited.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#addFilterCited(ViewerFilter filter)
	 * 
	 */
	public void addFilterToCited(ViewerFilter filter) {
		citedFilters.add(filter);
		if (this.cited != null) {
			this.cited.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#addBusinessFilterCited(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToCited(ViewerFilter filter) {
		citedBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#isContainedInCitedTable(EObject element)
	 * 
	 */
	public boolean isContainedInCitedTable(EObject element) {
		return ((ReferencesTableSettings)cited.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#initInformation(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initInformation(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		information.setContentProvider(contentProvider);
		information.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Case_.Properties.information);
		if (eefElementEditorReadOnlyState && information.isEnabled()) {
			information.setEnabled(false);
			information.setToolTipText(ArgMessages.Case__ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !information.isEnabled()) {
			information.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#updateInformation()
	 * 
	 */
	public void updateInformation() {
	information.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#addFilterInformation(ViewerFilter filter)
	 * 
	 */
	public void addFilterToInformation(ViewerFilter filter) {
		informationFilters.add(filter);
		if (this.information != null) {
			this.information.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#addBusinessFilterInformation(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToInformation(ViewerFilter filter) {
		informationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart#isContainedInInformationTable(EObject element)
	 * 
	 */
	public boolean isContainedInInformationTable(EObject element) {
		return ((ReferencesTableSettings)information.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ArgMessages.Case__Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
