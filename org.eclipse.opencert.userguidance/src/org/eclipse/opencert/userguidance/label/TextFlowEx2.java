/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.label;

import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.gmf.runtime.draw2d.ui.text.TextFlowEx;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.TextLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem;
import org.eclipse.opencert.userguidance.labelparser.tokens.TextToken;
import org.eclipse.opencert.userguidance.labelparser.tokens.Token;

public class TextFlowEx2 extends TextFlowEx {
	private static TextLayout layout;

	public TextFlowEx2() {
		if (layout == null) {
			layout = new TextLayout(Display.getCurrent());
		}
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		// Fix the layout
		org.eclipse.draw2d.geometry.Rectangle labelBounds = getParent()
				.getParent().getBounds();
		layout.setWidth(labelBounds.width);

		// Paint the background
		graphics.pushState();
		boolean isSelected = ((WrappingLabel2) getParent().getParent())
				.isSelected();
		if (isSelected) {
			graphics.setBackgroundColor(ColorConstants.menuBackgroundSelected);

		} else {
			graphics.setBackgroundColor(getBackgroundColor());
		}
		graphics.fillRectangle(this.getBounds());
		graphics.popState();

		// Get the compact text and text styles
		LabelHighlighter highlighter = new LabelHighlighter(getText(),
				getFont(), getFont());
		highlighter.calculateHighlighting();
		List<StyleRange> ranges = highlighter.getStyles();
		String compactText = highlighter.getOutputText();
		// Need to set the text before the styles are applied
		layout.setText(compactText);
		applyStyles(isSelected, ranges);

		// Text box overflowing
		Rectangle layoutBounds = layout.getBounds();
		if (layoutBounds.height > labelBounds.height) {
			int lastVisibleLineOffset = layout.getOffset(labelBounds.width,
					labelBounds.height, new int[] { 0 });

			// Get the last visible line and the character offset at the
			// beginning of that line
			int lastVisibleLine = layout.getLineIndex(lastVisibleLineOffset);
			Rectangle lastLineBounds = layout.getLineBounds(lastVisibleLine);
			if (lastLineBounds.y + lastLineBounds.height > labelBounds.height) {
				lastVisibleLine = lastVisibleLine - 1;
				lastVisibleLineOffset = layout.getLineOffsets()[lastVisibleLine];
			}

			if (lastVisibleLine + 1 < layout.getLineCount()) {
				int offset = layout.getLineOffsets()[lastVisibleLine];
				String prevText = layout.getText().substring(0, offset);
				String nextText = getText(lastVisibleLine, layout)
						+ getText(lastVisibleLine + 1, layout);

				// Shorten the last line until it fits
				// This may be optimized with binary search
				for (int i = nextText.length(); i >= 0; i--) {
					String test = nextText.substring(0, i) + "...";
					int width = measureString(test, ranges, offset);
					if (width < layout.getBounds().width) {
						layout.setText(prevText + test);
						applyStyles(isSelected, ranges);
						break;
					}
				}
			}
		}

		// Paint the text
		graphics.drawTextLayout(layout, 0, 0);
	}

	Token getTokenAt(int x, int y) {
		if (getBounds().width > 0) {
			layout.setWidth(getBounds().width);
		} else {
			return null;
		}

		LabelHighlighter highlighter = new LabelHighlighter(getText(),
				getFont(), getFont());
		highlighter.calculateHighlighting();
		List<StyleRange> ranges = highlighter.getStyles();
		String compactText = highlighter.getOutputText();
		layout.setText(compactText);
		applyStyles(false, ranges);

		int cursorPosition = layout.getOffset(x, y, new int[] { 0 });
		List<Token> tokens = highlighter.getTokens();
		int offset = 0;
		for (int i = 0; i < highlighter.getTokens().size(); i++) {
			if (tokens.get(i) instanceof HighlightItem) {
				HighlightItem item = (HighlightItem) tokens.get(i);
				String representation = highlighter.getRepresentation(item);
				offset += representation.length();
			} else if (tokens.get(i) instanceof TextToken) {
				TextToken simpleText = (TextToken) tokens.get(i);
				offset += simpleText.getRepresentation().length();
			}

			if (offset > cursorPosition) {
				return tokens.get(i);
			}
		}

		return null;
	}

	private void applyStyles(boolean isSelected, List<StyleRange> ranges) {
		String text = layout.getText();
		for (StyleRange range : ranges) {
			int start = range.start;
			int length = range.length;

			if (start > text.length()) {
				continue;
			}
			if (length > text.length()) {
				length = text.length();
			}
			if (isSelected) {
				range.background = ColorConstants.menuBackgroundSelected;
				range.foreground = ColorConstants.white;
			}
			layout.setStyle(range, start, start + length);
		}
	}

	private String getText(int line, TextLayout layout) {
		int start = layout.getLineOffsets()[line];
		int end = layout.getLineOffsets()[line + 1];
		return layout.getText().substring(start, end);
	}

	private int measureString(String text, List<StyleRange> ranges, int offset) {
		TextLayout lineLayout = new TextLayout(layout.getDevice());
		lineLayout.setText(text);
		for (StyleRange range : ranges) {
			int start = range.start - offset;
			int length = range.length;

			if (start > text.length()) {
				continue;
			}
			if (start < 0) {
				length += start;
				start = 0;
			}
			if (length > text.length()) {
				length = text.length();
			}
			lineLayout.setStyle(range, start, start + length);

		}
		return lineLayout.getBounds().width;
	}
}
