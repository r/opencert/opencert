/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.sam.arg.arg.Case;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assets Package</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssetsPackageImpl#isIsActive <em>Is Active</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssetsPackageImpl#getArtefactsModel <em>Artefacts Model</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssetsPackageImpl#getArgumentationModel <em>Argumentation Model</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssetsPackageImpl#getProcessModel <em>Process Model</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssetsPackageImpl extends DescribableElementImpl implements AssetsPackage {
	/**
	 * The default value of the '{@link #isIsActive() <em>Is Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsActive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ACTIVE_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetsPackageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssuranceprojectPackage.Literals.ASSETS_PACKAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsActive() {
		return (Boolean)eDynamicGet(AssuranceprojectPackage.ASSETS_PACKAGE__IS_ACTIVE, AssuranceprojectPackage.Literals.ASSETS_PACKAGE__IS_ACTIVE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsActive(boolean newIsActive) {
		eDynamicSet(AssuranceprojectPackage.ASSETS_PACKAGE__IS_ACTIVE, AssuranceprojectPackage.Literals.ASSETS_PACKAGE__IS_ACTIVE, newIsActive);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ArtefactModel> getArtefactsModel() {
		return (EList<ArtefactModel>)eDynamicGet(AssuranceprojectPackage.ASSETS_PACKAGE__ARTEFACTS_MODEL, AssuranceprojectPackage.Literals.ASSETS_PACKAGE__ARTEFACTS_MODEL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Case> getArgumentationModel() {
		return (EList<Case>)eDynamicGet(AssuranceprojectPackage.ASSETS_PACKAGE__ARGUMENTATION_MODEL, AssuranceprojectPackage.Literals.ASSETS_PACKAGE__ARGUMENTATION_MODEL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ProcessModel> getProcessModel() {
		return (EList<ProcessModel>)eDynamicGet(AssuranceprojectPackage.ASSETS_PACKAGE__PROCESS_MODEL, AssuranceprojectPackage.Literals.ASSETS_PACKAGE__PROCESS_MODEL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AssuranceprojectPackage.ASSETS_PACKAGE__IS_ACTIVE:
				return isIsActive();
			case AssuranceprojectPackage.ASSETS_PACKAGE__ARTEFACTS_MODEL:
				return getArtefactsModel();
			case AssuranceprojectPackage.ASSETS_PACKAGE__ARGUMENTATION_MODEL:
				return getArgumentationModel();
			case AssuranceprojectPackage.ASSETS_PACKAGE__PROCESS_MODEL:
				return getProcessModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AssuranceprojectPackage.ASSETS_PACKAGE__IS_ACTIVE:
				setIsActive((Boolean)newValue);
				return;
			case AssuranceprojectPackage.ASSETS_PACKAGE__ARTEFACTS_MODEL:
				getArtefactsModel().clear();
				getArtefactsModel().addAll((Collection<? extends ArtefactModel>)newValue);
				return;
			case AssuranceprojectPackage.ASSETS_PACKAGE__ARGUMENTATION_MODEL:
				getArgumentationModel().clear();
				getArgumentationModel().addAll((Collection<? extends Case>)newValue);
				return;
			case AssuranceprojectPackage.ASSETS_PACKAGE__PROCESS_MODEL:
				getProcessModel().clear();
				getProcessModel().addAll((Collection<? extends ProcessModel>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AssuranceprojectPackage.ASSETS_PACKAGE__IS_ACTIVE:
				setIsActive(IS_ACTIVE_EDEFAULT);
				return;
			case AssuranceprojectPackage.ASSETS_PACKAGE__ARTEFACTS_MODEL:
				getArtefactsModel().clear();
				return;
			case AssuranceprojectPackage.ASSETS_PACKAGE__ARGUMENTATION_MODEL:
				getArgumentationModel().clear();
				return;
			case AssuranceprojectPackage.ASSETS_PACKAGE__PROCESS_MODEL:
				getProcessModel().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AssuranceprojectPackage.ASSETS_PACKAGE__IS_ACTIVE:
				return isIsActive() != IS_ACTIVE_EDEFAULT;
			case AssuranceprojectPackage.ASSETS_PACKAGE__ARTEFACTS_MODEL:
				return !getArtefactsModel().isEmpty();
			case AssuranceprojectPackage.ASSETS_PACKAGE__ARGUMENTATION_MODEL:
				return !getArgumentationModel().isEmpty();
			case AssuranceprojectPackage.ASSETS_PACKAGE__PROCESS_MODEL:
				return !getProcessModel().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AssetsPackageImpl
