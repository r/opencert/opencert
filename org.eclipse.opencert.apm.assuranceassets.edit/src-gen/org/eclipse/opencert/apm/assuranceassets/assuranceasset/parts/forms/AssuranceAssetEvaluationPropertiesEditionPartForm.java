/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceassetViewsRepository;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.providers.AssuranceassetMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

// End of user code

/**
 * 
 * 
 */
public class AssuranceAssetEvaluationPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, AssuranceAssetEvaluationPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected ReferencesTable evaluation;
	protected List<ViewerFilter> evaluationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> evaluationFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable lifecycleEvent;
	protected List<ViewerFilter> lifecycleEventBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> lifecycleEventFilters = new ArrayList<ViewerFilter>();
	protected Text criterion;
	protected Text criterionDescription;
	protected Text evaluationResult;
	protected Text rationale;



	/**
	 * For {@link ISection} use only.
	 */
	public AssuranceAssetEvaluationPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public AssuranceAssetEvaluationPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence assuranceAssetEvaluationStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = assuranceAssetEvaluationStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.class);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale);
		
		
		composer = new PartComposer(assuranceAssetEvaluationStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation) {
					return createEvaluationTableComposition(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent) {
					return createLifecycleEventTableComposition(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion) {
					return createCriterionText(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription) {
					return createCriterionDescriptionText(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult) {
					return createEvaluationResultText(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale) {
					return createRationaleText(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(AssuranceassetMessages.AssuranceAssetEvaluationPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id, AssuranceassetMessages.AssuranceAssetEvaluationPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceAssetEvaluationPropertiesEditionPartForm.this,
							AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id, AssuranceassetViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name, AssuranceassetMessages.AssuranceAssetEvaluationPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceAssetEvaluationPropertiesEditionPartForm.this,
							AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name, AssuranceassetViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEvaluationTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.evaluation = new ReferencesTable(getDescription(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation, AssuranceassetMessages.AssuranceAssetEvaluationPropertiesEditionPart_EvaluationLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				evaluation.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				evaluation.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				evaluation.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				evaluation.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.evaluationFilters) {
			this.evaluation.addFilter(filter);
		}
		this.evaluation.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation, AssuranceassetViewsRepository.FORM_KIND));
		this.evaluation.createControls(parent, widgetFactory);
		this.evaluation.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData evaluationData = new GridData(GridData.FILL_HORIZONTAL);
		evaluationData.horizontalSpan = 3;
		this.evaluation.setLayoutData(evaluationData);
		this.evaluation.setLowerBound(0);
		this.evaluation.setUpperBound(-1);
		evaluation.setID(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation);
		evaluation.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEvaluationTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createLifecycleEventTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.lifecycleEvent = new ReferencesTable(getDescription(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent, AssuranceassetMessages.AssuranceAssetEvaluationPropertiesEditionPart_LifecycleEventLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				lifecycleEvent.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				lifecycleEvent.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				lifecycleEvent.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				lifecycleEvent.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.lifecycleEventFilters) {
			this.lifecycleEvent.addFilter(filter);
		}
		this.lifecycleEvent.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent, AssuranceassetViewsRepository.FORM_KIND));
		this.lifecycleEvent.createControls(parent, widgetFactory);
		this.lifecycleEvent.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData lifecycleEventData = new GridData(GridData.FILL_HORIZONTAL);
		lifecycleEventData.horizontalSpan = 3;
		this.lifecycleEvent.setLayoutData(lifecycleEventData);
		this.lifecycleEvent.setLowerBound(0);
		this.lifecycleEvent.setUpperBound(-1);
		lifecycleEvent.setID(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent);
		lifecycleEvent.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createLifecycleEventTableComposition

		// End of user code
		return parent;
	}

	
	protected Composite createCriterionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion, AssuranceassetMessages.AssuranceAssetEvaluationPropertiesEditionPart_CriterionLabel);
		criterion = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		criterion.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData criterionData = new GridData(GridData.FILL_HORIZONTAL);
		criterion.setLayoutData(criterionData);
		criterion.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceAssetEvaluationPropertiesEditionPartForm.this,
							AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, criterion.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, criterion.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		criterion.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, criterion.getText()));
				}
			}
		});
		EditingUtils.setID(criterion, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion);
		EditingUtils.setEEFtype(criterion, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion, AssuranceassetViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createCriterionText

		// End of user code
		return parent;
	}

	
	protected Composite createCriterionDescriptionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription, AssuranceassetMessages.AssuranceAssetEvaluationPropertiesEditionPart_CriterionDescriptionLabel);
		criterionDescription = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		criterionDescription.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData criterionDescriptionData = new GridData(GridData.FILL_HORIZONTAL);
		criterionDescription.setLayoutData(criterionDescriptionData);
		criterionDescription.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceAssetEvaluationPropertiesEditionPartForm.this,
							AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, criterionDescription.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, criterionDescription.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		criterionDescription.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, criterionDescription.getText()));
				}
			}
		});
		EditingUtils.setID(criterionDescription, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription);
		EditingUtils.setEEFtype(criterionDescription, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription, AssuranceassetViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createCriterionDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createEvaluationResultText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult, AssuranceassetMessages.AssuranceAssetEvaluationPropertiesEditionPart_EvaluationResultLabel);
		evaluationResult = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		evaluationResult.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData evaluationResultData = new GridData(GridData.FILL_HORIZONTAL);
		evaluationResult.setLayoutData(evaluationResultData);
		evaluationResult.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceAssetEvaluationPropertiesEditionPartForm.this,
							AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, evaluationResult.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, evaluationResult.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		evaluationResult.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, evaluationResult.getText()));
				}
			}
		});
		EditingUtils.setID(evaluationResult, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult);
		EditingUtils.setEEFtype(evaluationResult, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult, AssuranceassetViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createEvaluationResultText

		// End of user code
		return parent;
	}

	
	protected Composite createRationaleText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale, AssuranceassetMessages.AssuranceAssetEvaluationPropertiesEditionPart_RationaleLabel);
		rationale = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		rationale.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData rationaleData = new GridData(GridData.FILL_HORIZONTAL);
		rationale.setLayoutData(rationaleData);
		rationale.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceAssetEvaluationPropertiesEditionPartForm.this,
							AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, rationale.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, rationale.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetEvaluationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		rationale.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetEvaluationPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, rationale.getText()));
				}
			}
		});
		EditingUtils.setID(rationale, AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale);
		EditingUtils.setEEFtype(rationale, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale, AssuranceassetViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createRationaleText

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(AssuranceassetMessages.AssuranceAssetEvaluation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(AssuranceassetMessages.AssuranceAssetEvaluation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#initEvaluation(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEvaluation(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		evaluation.setContentProvider(contentProvider);
		evaluation.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation);
		if (eefElementEditorReadOnlyState && evaluation.isEnabled()) {
			evaluation.setEnabled(false);
			evaluation.setToolTipText(AssuranceassetMessages.AssuranceAssetEvaluation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !evaluation.isEnabled()) {
			evaluation.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#updateEvaluation()
	 * 
	 */
	public void updateEvaluation() {
	evaluation.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#addFilterEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEvaluation(ViewerFilter filter) {
		evaluationFilters.add(filter);
		if (this.evaluation != null) {
			this.evaluation.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#addBusinessFilterEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEvaluation(ViewerFilter filter) {
		evaluationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#isContainedInEvaluationTable(EObject element)
	 * 
	 */
	public boolean isContainedInEvaluationTable(EObject element) {
		return ((ReferencesTableSettings)evaluation.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#initLifecycleEvent(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initLifecycleEvent(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		lifecycleEvent.setContentProvider(contentProvider);
		lifecycleEvent.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent);
		if (eefElementEditorReadOnlyState && lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(false);
			lifecycleEvent.setToolTipText(AssuranceassetMessages.AssuranceAssetEvaluation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#updateLifecycleEvent()
	 * 
	 */
	public void updateLifecycleEvent() {
	lifecycleEvent.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#addFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventFilters.add(filter);
		if (this.lifecycleEvent != null) {
			this.lifecycleEvent.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#addBusinessFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#isContainedInLifecycleEventTable(EObject element)
	 * 
	 */
	public boolean isContainedInLifecycleEventTable(EObject element) {
		return ((ReferencesTableSettings)lifecycleEvent.getInput()).contains(element);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#getCriterion()
	 * 
	 */
	public String getCriterion() {
		return criterion.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#setCriterion(String newValue)
	 * 
	 */
	public void setCriterion(String newValue) {
		if (newValue != null) {
			criterion.setText(newValue);
		} else {
			criterion.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion);
		if (eefElementEditorReadOnlyState && criterion.isEnabled()) {
			criterion.setEnabled(false);
			criterion.setToolTipText(AssuranceassetMessages.AssuranceAssetEvaluation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !criterion.isEnabled()) {
			criterion.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#getCriterionDescription()
	 * 
	 */
	public String getCriterionDescription() {
		return criterionDescription.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#setCriterionDescription(String newValue)
	 * 
	 */
	public void setCriterionDescription(String newValue) {
		if (newValue != null) {
			criterionDescription.setText(newValue);
		} else {
			criterionDescription.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription);
		if (eefElementEditorReadOnlyState && criterionDescription.isEnabled()) {
			criterionDescription.setEnabled(false);
			criterionDescription.setToolTipText(AssuranceassetMessages.AssuranceAssetEvaluation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !criterionDescription.isEnabled()) {
			criterionDescription.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#getEvaluationResult()
	 * 
	 */
	public String getEvaluationResult() {
		return evaluationResult.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#setEvaluationResult(String newValue)
	 * 
	 */
	public void setEvaluationResult(String newValue) {
		if (newValue != null) {
			evaluationResult.setText(newValue);
		} else {
			evaluationResult.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult);
		if (eefElementEditorReadOnlyState && evaluationResult.isEnabled()) {
			evaluationResult.setEnabled(false);
			evaluationResult.setToolTipText(AssuranceassetMessages.AssuranceAssetEvaluation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !evaluationResult.isEnabled()) {
			evaluationResult.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#getRationale()
	 * 
	 */
	public String getRationale() {
		return rationale.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart#setRationale(String newValue)
	 * 
	 */
	public void setRationale(String newValue) {
		if (newValue != null) {
			rationale.setText(newValue);
		} else {
			rationale.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale);
		if (eefElementEditorReadOnlyState && rationale.isEnabled()) {
			rationale.setEnabled(false);
			rationale.setToolTipText(AssuranceassetMessages.AssuranceAssetEvaluation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !rationale.isEnabled()) {
			rationale.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceassetMessages.AssuranceAssetEvaluation_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
