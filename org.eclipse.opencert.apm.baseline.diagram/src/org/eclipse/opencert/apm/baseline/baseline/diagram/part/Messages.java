/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String BaselineCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String BaselineCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String BaselineCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String BaselineCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String BaselineCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String BaselineCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String BaselineCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String BaselineCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String BaselineDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String BaselineDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String BaselineDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String BaselineDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String BaselineDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String BaselineDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String BaselineDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String BaselineDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String BaselineDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String BaselineDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String BaselineDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String BaselineDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String BaselineDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String BaselineNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String BaselineDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String BaselineDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String BaselineDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String BaselineDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String BaselineDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String BaselineElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Objects1Group_title;

	/**
	 * @generated
	 */
	public static String Connections2Group_title;

	/**
	 * @generated
	 */
	public static String BaseActivity1CreationTool_title;

	/**
	 * @generated
	 */
	public static String BaseActivity1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String BaseArtefact2CreationTool_title;

	/**
	 * @generated
	 */
	public static String BaseArtefact2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String BaseRole3CreationTool_title;

	/**
	 * @generated
	 */
	public static String BaseRole3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String PrecedingActivity1CreationTool_title;

	/**
	 * @generated
	 */
	public static String PrecedingActivity1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ProducedArtefact2CreationTool_title;

	/**
	 * @generated
	 */
	public static String ProducedArtefact2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RequiredArtefact3CreationTool_title;

	/**
	 * @generated
	 */
	public static String RequiredArtefact3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Role4CreationTool_title;

	/**
	 * @generated
	 */
	public static String Role4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String BaseActivityBaseActivitySubActivityCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String BaseActivityBaseActivitySubActivityCompartment2EditPart_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseFramework_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivity_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivity_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseArtefact_2002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseRole_2003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivity_3001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivity_3001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivityRequiredArtefact_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivityRequiredArtefact_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivityProducedArtefact_4002_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivityProducedArtefact_4002_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivityPrecedingActivity_4003_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivityPrecedingActivity_4003_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivityRole_4004_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_BaseActivityRole_4004_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String BaselineModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String BaselineModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
