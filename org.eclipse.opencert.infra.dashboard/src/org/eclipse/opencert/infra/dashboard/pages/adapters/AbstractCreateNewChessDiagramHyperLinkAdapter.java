/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.infra.dashboard.helpers.ChessPapyrusHelper;
import org.eclipse.opencert.infra.dashboard.helpers.EclipseHelper;
import org.eclipse.papyrus.commands.Activator;
import org.eclipse.papyrus.commands.ICreationCommand;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.views.modelexplorer.ModelExplorerPage;
import org.eclipse.papyrus.views.modelexplorer.ModelExplorerPageBookView;
import org.eclipse.papyrus.views.modelexplorer.ModelExplorerView;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.uml2.uml.Model;

/**
 * A {@link HyperlinkAdapter} to create a new CHESS diagram.
 */
public abstract class AbstractCreateNewChessDiagramHyperLinkAdapter
		extends AbstractCreateNewChessElementHyperLinkAdapter {

	/**
	 * {@inheritDoc}
	 *
	 * Overridden to create a new diagram.
	 */
	@Override
	protected void createChessElement() {
		// Create a new diagram command
		ICreationCommand vCreationCommand = getDiagramCreationCommand();

		// Get the first opened Chess Papyrus editor
		IMultiDiagramEditor vActivePapyrusEditor = (IMultiDiagramEditor) EclipseHelper.getFirstPapyrusDiagramEditorPart();
		
		// If there is one Chess graphical editor is opened,
		// try to goto the right package and create the new diagram there
		if (Objects.nonNull(vActivePapyrusEditor)) {
			ServicesRegistry vRegistry = vActivePapyrusEditor.getServicesRegistry();

			if (Objects.nonNull(vRegistry)) {
				ModelSet vModelSet = null;
				try {
					vModelSet = vRegistry.getService(ModelSet.class);
				} catch (ServiceException pException) {
					Activator.log.error(pException);
				}

				if (Objects.nonNull(vModelSet)) {
					// Get the root model
					Model vRootModel = ChessPapyrusHelper.INSTANCE.getUmlModelFromModelSet(vModelSet);

					// Get the target packageable element
					org.eclipse.uml2.uml.PackageableElement vTargetElement = getTargetElement(vRootModel);

					if (Objects.nonNull(vTargetElement)) {
						// First goto the active Papyrus editor
						EclipseHelper.getActiveWorkbenchPage().activate(vActivePapyrusEditor);

						// Then select the target element before creating a new diagram
						IViewPart vModelExplorer = null;
						try {
							vModelExplorer = EclipseHelper.getActiveWorkbenchPage().showView(MODEL_EXPLORER_VIEW_ID);
						} catch (PartInitException pException) {
							Activator.log.error(pException);
						}

						if (vModelExplorer instanceof ModelExplorerPageBookView) {
							ModelExplorerPageBookView vMEPBView = (ModelExplorerPageBookView) vModelExplorer;

							if (vMEPBView.getCurrentPage() instanceof ModelExplorerPage) {
								ModelExplorerPage vMEPage = (ModelExplorerPage) vMEPBView.getCurrentPage();

								ModelExplorerView vMEView = (ModelExplorerView) vMEPage.getViewer();
								List<EObject> vSelectElements = new ArrayList<>();
								vSelectElements.add(vTargetElement);
								vMEView.revealSemanticElement(vSelectElements);
							}

							// Create the new diagram inside the target element
							vCreationCommand.createDiagram(vModelSet, vTargetElement, null);
						}
					}
				}
			}
		} else {
			// Otherwise, no Chess graphical editor is already opened, ask user to open one
			ChessPapyrusHelper.INSTANCE.openNoActivePapyrusEditorMessageBox();
		}
	}

	/**
	 * @return The creation command to create a diagram
	 */
	protected abstract ICreationCommand getDiagramCreationCommand();

	/**
	 * Get the target packageable element in which the new diagram will be created.
	 *
	 * @param pRootModel
	 *            The root model
	 * @return The target package
	 */
	protected abstract org.eclipse.uml2.uml.PackageableElement getTargetElement(Model pRootModel);
}
