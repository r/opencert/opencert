/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

import com.vaadin.ui.Label;

public class EvidenceWrapper {
    private Long cdoId;
    private Label name;
    private Label id;
    private Label description;
    private Label artefactModel;
    private Label artefactDefinition;
    private Label versionId;
    private Label  properties;
    private Label  evaluation;
    private Label  resource;
    private Label  events;
    private Label  iaStatus;
    
    public EvidenceWrapper() {
    }
    
    public EvidenceWrapper(Long cdoId, Label name, Label id, Label description, Label artefactModel, Label artefactDefinition,
    		Label versionId, Label properties, Label evaluation, Label resource, Label events, Label isStatus) {
    	this.cdoId = cdoId;
    	this.name = name;
    	this.id = id;
    	this.description = description;
    	this.artefactModel = artefactModel;
    	this.artefactDefinition = artefactDefinition;
    	this.versionId = versionId;
    	this.properties = properties;
    	this.evaluation = evaluation;
    	this.resource = resource;
    	this.events = events;
    	this.iaStatus = isStatus;
    }
    
	public Long getCdoId() {
		return cdoId;
	}

	public void setCdoId(Long cdoId) {
		this.cdoId = cdoId;
	}
    
	public Label getName() {
		return name;
	}

	public void setName(Label name) {
		this.name = name;
	}
	
	public Label getId() {
		return id;
	}

	public void setId(Label id) {
		this.id = id;
	}
	
	public Label getDescription() {
		return description;
	}

	public void setDescription(Label description) {
		this.description = description;
	}
	
    
    public Label getArtefactModel() {
		return artefactModel;
	}

	public void setArtefactModel(Label artefactModel) {
		this.artefactModel = artefactModel;
	}

	public Label getArtefactDefinition() {
		return artefactDefinition;
	}

	public void setArtefactDefinition(Label artefactDefinition) {
		this.artefactDefinition = artefactDefinition;
	}

	public Label getVersionId() {
		return versionId;
	}

	public void setVersionId(Label versionId) {
		this.versionId = versionId;
	}
	
	public Label getProperties() {
		return properties;
	}
	
	public void setProperties(Label properties) {
		this.properties = properties;
	}
	
	public Label getEvaluation() {
		return evaluation;
	}
	
	public void setEvaluation(Label evaluation) {
		this.evaluation = evaluation;
	}
	
	public Label getResource() {
		return resource;
	}
	
	public void setResource(Label resource) {
		this.resource = resource;
	}
	
	public Label getEvents() {
		return events;
	}
	
	public void setEvents(Label events) {
		this.events = events;
	}

    public Label getIAStatus()
    {
        return iaStatus;
    }
}
