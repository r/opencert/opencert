/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation and KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza(Tecnalia) - initial API and implementation
 *   Alejandra Ru�z(Tecnalia) - initial API and implementation
 *   Idoya Del R�o(Tecnalia) - initial API and implementation
 *   Mari Carmen Palacios(Tecnalia) - initial API and implementation
 *   Angel L�pez(Tecnalia) - initial API and implementation
 *   Jan Mauersberger(KPIT)- Improvements implementation
 *   Sascha Baumgart(KPIT)- Improvements implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.svnkit;


import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;


public class Activator extends AbstractUIPlugin {

	// The plug-in ID
		public static final String PLUGIN_ID = "org.eclipse.opencert.infra.svnkit"; //$NON-NLS-1$

		// The shared instance
		private static Activator plugin;
		
		
		
		
		public static SVNRepository repository;
		
		/**
		 * The constructor
		 */
		public Activator() {
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
		 */
		public void start(BundleContext context) throws Exception {
			
			
			
			super.start(context);
			plugin = this;
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
		 */
		public void stop(BundleContext context) throws Exception {
			plugin = null;
			super.stop(context);
		}
		
		public static Activator getDefault() {
			return plugin;
		}
		
		
		public static SVNRepository getRepository() {
			return repository;
		}

}
