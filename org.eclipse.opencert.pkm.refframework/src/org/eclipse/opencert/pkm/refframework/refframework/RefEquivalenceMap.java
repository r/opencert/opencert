/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework;

import org.eclipse.emf.common.util.EList;

import org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ref Equivalence Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefEquivalenceMap()
 * @model
 * @generated
 */
public interface RefEquivalenceMap extends EquivalenceMap {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefEquivalenceMap_Target()
	 * @model
	 * @generated
	 */
	EList<RefAssurableElement> getTarget();

} // RefEquivalenceMap
