/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.forms;

// Start of user code for imports
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivitySelectionPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;

// End of user code

/**
 * 
 * 
 */
public class BaseActivitySelectionPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, BaseActivitySelectionPropertiesEditionPart {

	protected Button isSelected;
	protected Text selectionJustification;



	/**
	 * For {@link ISection} use only.
	 */
	public BaseActivitySelectionPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseActivitySelectionPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence baseActivitySelectionStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseActivitySelectionStep.addStep(BaselineViewsRepository.BaseActivitySelection.Properties.class);
		propertiesStep.addStep(BaselineViewsRepository.BaseActivitySelection.Properties.isSelected);
		propertiesStep.addStep(BaselineViewsRepository.BaseActivitySelection.Properties.selectionJustification);
		
		
		composer = new PartComposer(baseActivitySelectionStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseActivitySelection.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseActivitySelection.Properties.isSelected) {
					return createIsSelectedCheckbox(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseActivitySelection.Properties.selectionJustification) {
					return createSelectionJustificationText(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(BaselineMessages.BaseActivitySelectionPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIsSelectedCheckbox(FormToolkit widgetFactory, Composite parent) {
		isSelected = widgetFactory.createButton(parent, getDescription(BaselineViewsRepository.BaseActivitySelection.Properties.isSelected, BaselineMessages.BaseActivitySelectionPropertiesEditionPart_IsSelectedLabel), SWT.CHECK);
		isSelected.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 *
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 	
			 */
			public void widgetSelected(SelectionEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivitySelectionPropertiesEditionPartForm.this, BaselineViewsRepository.BaseActivitySelection.Properties.isSelected, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, new Boolean(isSelected.getSelection())));
			}

		});
		GridData isSelectedData = new GridData(GridData.FILL_HORIZONTAL);
		isSelectedData.horizontalSpan = 2;
		isSelected.setLayoutData(isSelectedData);
		EditingUtils.setID(isSelected, BaselineViewsRepository.BaseActivitySelection.Properties.isSelected);
		EditingUtils.setEEFtype(isSelected, "eef::Checkbox"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseActivitySelection.Properties.isSelected, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIsSelectedCheckbox

		// End of user code
		return parent;
	}

	
	protected Composite createSelectionJustificationText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseActivitySelection.Properties.selectionJustification, BaselineMessages.BaseActivitySelectionPropertiesEditionPart_SelectionJustificationLabel);
		selectionJustification = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		selectionJustification.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData selectionJustificationData = new GridData(GridData.FILL_HORIZONTAL);
		selectionJustification.setLayoutData(selectionJustificationData);
		selectionJustification.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							BaseActivitySelectionPropertiesEditionPartForm.this,
							BaselineViewsRepository.BaseActivitySelection.Properties.selectionJustification,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, selectionJustification.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseActivitySelectionPropertiesEditionPartForm.this,
									BaselineViewsRepository.BaseActivitySelection.Properties.selectionJustification,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, selectionJustification.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseActivitySelectionPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		selectionJustification.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivitySelectionPropertiesEditionPartForm.this, BaselineViewsRepository.BaseActivitySelection.Properties.selectionJustification, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, selectionJustification.getText()));
				}
			}
		});
		EditingUtils.setID(selectionJustification, BaselineViewsRepository.BaseActivitySelection.Properties.selectionJustification);
		EditingUtils.setEEFtype(selectionJustification, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseActivitySelection.Properties.selectionJustification, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createSelectionJustificationText

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivitySelectionPropertiesEditionPart#getIsSelected()
	 * 
	 */
	public Boolean getIsSelected() {
		return Boolean.valueOf(isSelected.getSelection());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivitySelectionPropertiesEditionPart#setIsSelected(Boolean newValue)
	 * 
	 */
	public void setIsSelected(Boolean newValue) {
		if (newValue != null) {
			isSelected.setSelection(newValue.booleanValue());
		} else {
			isSelected.setSelection(false);
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseActivitySelection.Properties.isSelected);
		if (eefElementEditorReadOnlyState && isSelected.isEnabled()) {
			isSelected.setEnabled(false);
			isSelected.setToolTipText(BaselineMessages.BaseActivitySelection_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !isSelected.isEnabled()) {
			isSelected.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivitySelectionPropertiesEditionPart#getSelectionJustification()
	 * 
	 */
	public String getSelectionJustification() {
		return selectionJustification.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivitySelectionPropertiesEditionPart#setSelectionJustification(String newValue)
	 * 
	 */
	public void setSelectionJustification(String newValue) {
		if (newValue != null) {
			selectionJustification.setText(newValue);
		} else {
			selectionJustification.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseActivitySelection.Properties.selectionJustification);
		if (eefElementEditorReadOnlyState && selectionJustification.isEnabled()) {
			selectionJustification.setEnabled(false);
			selectionJustification.setToolTipText(BaselineMessages.BaseActivitySelection_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !selectionJustification.isEnabled()) {
			selectionJustification.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseActivitySelection_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
