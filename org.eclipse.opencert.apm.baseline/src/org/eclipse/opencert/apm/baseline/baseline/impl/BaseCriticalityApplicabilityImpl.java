/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

import org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Criticality Applicability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityApplicabilityImpl#getApplicLevel <em>Applic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityApplicabilityImpl#getCriticLevel <em>Critic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityApplicabilityImpl#getComment <em>Comment</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BaseCriticalityApplicabilityImpl extends CDOObjectImpl implements BaseCriticalityApplicability {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseCriticalityApplicabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BaselinePackage.Literals.BASE_CRITICALITY_APPLICABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseApplicabilityLevel getApplicLevel() {
		return (BaseApplicabilityLevel)eGet(BaselinePackage.Literals.BASE_CRITICALITY_APPLICABILITY__APPLIC_LEVEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicLevel(BaseApplicabilityLevel newApplicLevel) {
		eSet(BaselinePackage.Literals.BASE_CRITICALITY_APPLICABILITY__APPLIC_LEVEL, newApplicLevel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseCriticalityLevel getCriticLevel() {
		return (BaseCriticalityLevel)eGet(BaselinePackage.Literals.BASE_CRITICALITY_APPLICABILITY__CRITIC_LEVEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCriticLevel(BaseCriticalityLevel newCriticLevel) {
		eSet(BaselinePackage.Literals.BASE_CRITICALITY_APPLICABILITY__CRITIC_LEVEL, newCriticLevel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComment() {
		return (String)eGet(BaselinePackage.Literals.BASE_CRITICALITY_APPLICABILITY__COMMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(String newComment) {
		eSet(BaselinePackage.Literals.BASE_CRITICALITY_APPLICABILITY__COMMENT, newComment);
	}

} //BaseCriticalityApplicabilityImpl
