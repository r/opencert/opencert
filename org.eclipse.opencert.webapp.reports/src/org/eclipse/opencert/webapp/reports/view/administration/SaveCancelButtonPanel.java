/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.administration;

import org.eclipse.opencert.webapp.reports.util.ICommonCssStyles;
import org.eclipse.opencert.webapp.reports.view.common.INotificationPresenter;
import org.eclipse.opencert.storage.cdo.property.OpencertPropertiesReader;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

@SuppressWarnings("serial")
public class SaveCancelButtonPanel
    extends HorizontalLayout
    implements INotificationPresenter
{
    private Button saveButton;
    private Button cancelButton;
    private Button deleteButton;
    private Button newButton;
    private Button cloneButton;
    private Label notificationLabel;
    
    public final static String SAVE_BUTTON_DESCRIPTION = "Save the current project definition. [ctrl+s]";
    public final static String CREATE_BUTTON_DESCRIPTION = "Create new Assurance Project. [ctrl+s]";
    public final static String DELETE_BUTTON_DESCRIPTION = "Delete the project definition from OPENCERT repository.";
    public final static String NEW_BUTTON_DESCRIPTION = "Create new project definition.";
    public final static String CLONE_BUTTON_DESCRIPTION = "Clone the current project definition.";

    public SaveCancelButtonPanel()
    {
    	setSizeFull();
    	
    	HorizontalLayout saveCancelLayout = new HorizontalLayout();
    	saveCancelLayout.setWidth("150px");
        saveButton = new Button("Save");
        saveButton.setStyleName(ICommonCssStyles.COMMON_BUTTON_STYLE);
        saveButton.setClickShortcut(KeyCode.S, ModifierKey.CTRL);
        saveButton.setDescription(SAVE_BUTTON_DESCRIPTION);
        
        cancelButton = new Button("Cancel");
        cancelButton.setStyleName(ICommonCssStyles.COMMON_BUTTON_STYLE);
        cancelButton.setDescription("Cancel the unsaved modifications.");
        saveCancelLayout.addComponent(saveButton);
        saveCancelLayout.addComponent(cancelButton);
        
        HorizontalLayout operationsLayout = new HorizontalLayout();
        deleteButton = new Button("Delete project");
        deleteButton.setStyleName(ICommonCssStyles.COMMON_BUTTON_STYLE + " DeleteButton");
        deleteButton.setDescription(DELETE_BUTTON_DESCRIPTION);
        
        newButton = new Button("New project");
        newButton.setStyleName(ICommonCssStyles.COMMON_BUTTON_STYLE + " CreateButton");
        newButton.setDescription(NEW_BUTTON_DESCRIPTION);
        
        cloneButton = new Button("Clone project");
        cloneButton.setStyleName(ICommonCssStyles.COMMON_BUTTON_STYLE + " CreateButton");
        cloneButton.setDescription(CLONE_BUTTON_DESCRIPTION);

        operationsLayout.addComponent(deleteButton);
        
        if (OpencertPropertiesReader.getInstance().isShowNewCloneProjectButtons()) 
        {
            operationsLayout.addComponent(newButton);
            operationsLayout.addComponent(cloneButton);
        }
        
        addComponent(saveCancelLayout);
        
        notificationLabel = createNotificationPanel();
        addComponent(notificationLabel);
        setExpandRatio(notificationLabel, 1.0f);
        
        addComponent(operationsLayout);
    }

    private Label createNotificationPanel()
    {
        Label result = new Label();
        result.setStyleName("NotificationPanel");
        result.setWidth("100%");
        
        return result;
    }

    public void addDeleteAction(ClickListener clickListener)
    {
        deleteButton.addClickListener(clickListener);
    }

    public void addCancelAction(ClickListener clickListener)
    {
        cancelButton.addClickListener(clickListener);
    }
    
    public void addSaveAction(ClickListener clickListener)
    {
        saveButton.addClickListener(clickListener);
    }
    
    public void addNewAction(ClickListener clickListener)
    {
        newButton.addClickListener(clickListener);
    }
    
    public void addCloneAction(ClickListener clickListener)
    {
        cloneButton.addClickListener(clickListener);
    }

    @Override
    public void showNotification(String text)
    {
        notificationLabel.setValue(text);
        notificationLabel.setVisible(true);
    }

    @Override
    public void clearNotification()
    {
        notificationLabel.setValue("");
        notificationLabel.setVisible(false);
    }
    
    public void updateSaveButtonCaption(String caption)
    {
    	saveButton.setCaption(caption);
    }
    
    public void updateSaveButtonDescription(String description)
    {
    	saveButton.setDescription(description);
    }
    
    public void deleteButtonEnabled(boolean enabled) {
    	deleteButton.setEnabled(enabled);
    }
    
    public void updateDeleteButtonDescription(String description) {
    	deleteButton.setDescription(description);
    }
    
    public void newButtonEnabled(boolean enabled) {
    	newButton.setEnabled(enabled);
    }
    
    public void updateNewButtonDescription(String description) {
    	newButton.setDescription(description);
    }
    
    public void cloneButtonEnabled(boolean enabled) {
    	cloneButton.setEnabled(enabled);
    }
    
    public void updateCloneButtonDescription(String description) {
    	cloneButton.setDescription(description);
    }
}
