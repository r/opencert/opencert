/*******************************************************************************
 * Copyright (c) 2017 Honeywell, Inc.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Vit Koksa  <Vit.Koksa@honeywell.com>
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.vavmanager;


import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.TextViewer;
//import org.eclipse.jface.text.
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.part.ViewPart;

public class ResultBrowser extends ViewPart {
	TextViewer tv;
	Document document;

	//ResultBrowser(String str) {
		//super();
		//document = new Document(str);
	//}
	public ResultBrowser() {
		super();
	}
	
	@Override
	public void createPartControl(Composite parent) {
		// TODO Auto-generated method stub
        tv = new TextViewer(parent, SWT.BORDER_DASH | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI );
        // Auto-hide scrollbars
        Listener scrollBarListener = new Listener () {
        	  @Override
        	  public void handleEvent(Event event) {
        		StyledText t = (StyledText)event.widget;
        	    Rectangle r1 = t.getClientArea();
        	    Rectangle r2 = t.computeTrim(r1.x, r1.y, r1.width, r1.height);
        	    Point p = t.computeSize(SWT.DEFAULT,  SWT.DEFAULT,  true);
        	    t.getHorizontalBar().setVisible(r2.width <= p.x);
        	    t.getVerticalBar().setVisible(r2.height <= p.y);
        	    if (event.type == SWT.Modify) {
        	      t.getParent().layout(true);
        	      t.showSelection();
        	    }
        	  }
        	};
        	tv.getTextWidget().addListener(SWT.Resize, scrollBarListener);
        	tv.getTextWidget().addListener(SWT.Modify, scrollBarListener);
        this.setContentDescription("Response from the Verification Server");
        document = new Document("");
        //document = new Document("All contracts have been proven consistent.\nThe following contracts are redundant: The contract ContractProperty1 imply part of ContractProperty3.");
        tv.setDocument(document);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		//tv.refresh();

	}
		
	public void changeDocument(String str) {
		document = new Document(str);
		try {
			if (tv != null) {
				tv.setDocument(document);
				tv.refresh();
				tv.getTextWidget().notifyListeners(SWT.Modify, new Event());
			} else {
				throw new RuntimeException("TextViewer not initialized.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
