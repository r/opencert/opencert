/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import java.util.function.Predicate;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Crawler to index objects, collection of objects or object trees.
 * 
 * @author mauersberger
 */
public class ElasticIndexer {

	ElasticClient client;

	ElasticConsole console;

	String indexName = "amass"; //$NON-NLS-1$

	Predicate<Object> filter = null;

	Gson gson = new GsonBuilder().setPrettyPrinting().create();

	/**
	 * Factory method to create a new indexer.
	 *
	 * @param client
	 *            using the given client
	 * @return the new instance
	 */
	public static ElasticIndexer on(ElasticClient client) {
		ElasticIndexer indexer = new ElasticIndexer();
		indexer.client = client;
		indexer.console = new NullConsole();
		return indexer;
	}

	/**
	 * @param newName
	 *            the name of the index
	 * @return this
	 */
	public ElasticIndexer onIndex(String newName) {
		this.indexName = newName;
		return this;
	}

	/**
	 * @param newFilter
	 *            the filter to use
	 * @return this
	 */
	public ElasticIndexer filtering(Predicate<Object> newFilter) {
		this.filter = newFilter;
		return this;
	}

	/**
	 * @param newConsole
	 *            the console to use
	 * @return this
	 */
	public ElasticIndexer using(ElasticConsole newConsole) {
		this.console = newConsole;
		return this;
	}

	/**
	 * @param element
	 *            to index
	 * @return this
	 */
	public ElasticIndexer indexObject(Object element) {
		if (element instanceof EObject) {
			index((EObject) element);
		}

		return this;
	}

	/**
	 * @param element
	 *            to index its resource
	 * @return this
	 */
	public ElasticIndexer indexResource(Object element) {
		if (element instanceof EObject) {
			TreeIterator<EObject> all = ((EObject) element).eResource().getAllContents();
			index(all);
		}

		return this;
	}

	/**
	 * @param element
	 *            to index its containment tree
	 * @return this
	 */
	public ElasticIndexer indexTree(Object element) {
		if (element instanceof EObject) {
			TreeIterator<EObject> all = ((EObject) element).eAllContents();
			index(all);
		}

		return this;
	}

	/*
	 * The real implementation: indexes over an iterator
	 */
	private void index(TreeIterator<EObject> all) {
		while (all.hasNext()) {
			EObject next = all.next();
			if (filter != null && !filter.test(next)) {
				continue;
			}
			index(next);
		}
	}

	/*
	 * The real implementation: indexes an EObject
	 */
	private void index(EObject object) {
		try {
			console.info("Convertign object to document..."); //$NON-NLS-1$
			ElasticDocument document = EObjectToDocument.INSTANCE.convert(object, this.indexName);
			console.info("Document endpoint: {0}", document.getEndPoint()); //$NON-NLS-1$
			console.info("JSON: {0}", gson.toJson(document.source)); //$NON-NLS-1$
			console.info("Storing document..."); //$NON-NLS-1$
			this.client.store(document);
		} catch (Exception e) {
			console.error("Failed to create or store document: {0}", e); //$NON-NLS-1$
			throw new RuntimeException("Failed to index an object", e); //$NON-NLS-1$
		}
	}

}
