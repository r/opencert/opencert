/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.containers.FrameworkWrapper;
import org.eclipse.opencert.webapp.reports.dao.AssuranceProjectDAO;
import org.eclipse.opencert.webapp.reports.dao.BaseArtefactDAO;
import org.eclipse.opencert.webapp.reports.security.SecurityUtil;
import org.eclipse.opencert.webapp.reports.util.IdNameDesc;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.view.administration.ProjectAdministrationPage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AssuranceProjectManager 
{
	public static final String SPRING_NAME = "assuranceProjectManager";
	
	@Autowired
	AssuranceProjectDAO assuranceProjectDAO;

	@Autowired
    private BaseArtefactDAO baseArtefactDAO;
	
	public List<AssuranceProjectWrapper> getProjects()
	{
	    return getProjects(false);
	}
	
	public List<AssuranceProjectWrapper> getProjects(boolean limitWithAccessRights)
	{
		final List<AssuranceProject> result = assuranceProjectDAO.getProjects();
		
		Iterator<AssuranceProject> it = result.iterator();
		while (it.hasNext())
		{
			AssuranceProject project = it.next();
			if (project.getName() == null || project.getName().length()==0)
			{
				it.remove();
				continue;
			}
			
			if (limitWithAccessRights && !SecurityUtil.hasAccessRight(SecurityUtil.ACCESS_RIGHT_PROJECT_PREFIX + project.getName())) {
			    it.remove();
			}
		}
		
		return convertAssuranceProjectsToWrappers(result);
	}
	
	public AssuranceProject getProject(long projectUid)
	{
		final AssuranceProject result = assuranceProjectDAO.getProject(projectUid);
		
		return result;
	}

    private List<AssuranceProjectWrapper> convertAssuranceProjectsToWrappers(List<AssuranceProject> result)
    {
        List<AssuranceProjectWrapper> assuranceProjectWrappers = new ArrayList<AssuranceProjectWrapper>();
        
        for (AssuranceProject assuranceProject : result) {   
            long cdoId = CDOStorageUtil.getCDOId(assuranceProject);
            String name = assuranceProject.getName();
            String desc = assuranceProject.getDescription();
                        
            List<FrameworkWrapper> baseFrameworkWrappers = new ArrayList<FrameworkWrapper>();
            
           
	        EList<BaseFramework> baselineFrameworks = findBaselineFrameworksInAssuranceProject(assuranceProject);
	        if (baselineFrameworks != null) {
		            for (BaseFramework baFramework : baselineFrameworks) {
		            	//After restore database: 10270624_1152_TRAINING.backup exception was occurred
		            	try {
		            		long baselineFrameworkCdoId = CDOStorageUtil.getCDOId(baFramework);
		            		String baselineFrameworkName = baFramework.getName();
		                
		            		baseFrameworkWrappers.add(new FrameworkWrapper(baselineFrameworkCdoId, baselineFrameworkName));
		            	} catch (Exception ex) {
		                	OpencertLogger.error(ex);
		                }
		            }
	            }
            
            
            assuranceProjectWrappers.add(new AssuranceProjectWrapper(cdoId, name, desc, baseFrameworkWrappers, assuranceProject));
        }
        return assuranceProjectWrappers;
    }

    private EList<BaseFramework> findBaselineFrameworksInAssuranceProject(AssuranceProject assuranceProject)
    {
        EList<BaselineConfig> baselineConfigs = assuranceProject.getBaselineConfig();
        
        EList<BaseFramework> frameworks = null;
        
        for (BaselineConfig config : baselineConfigs) {
            if (config.isIsActive()) {
                frameworks = config.getRefFramework();
                break;
            }
        }
        
        return frameworks;
    }
    
    public void delete(AssuranceProjectWrapper assuranceProjectWrapper) 
    {
        assuranceProjectDAO.delete(assuranceProjectWrapper);
    }

    public void update(AssuranceProjectWrapper assuranceProjectWrapper, String name, String description)
    {
        assuranceProjectDAO.update(assuranceProjectWrapper, name, description);  
    }
    
    public long createNewProject(String projectName, String projectDescription, String baselineName, Collection<IdNameDesc> baseArtefactsToAdd)
    {
        final ProjectCreationData projectCreationData = assuranceProjectDAO.createNewProject(projectName, projectDescription, baselineName, baseArtefactsToAdd);
        
        Iterator<Entry<IdNameDesc, BaseArtefact>> iterator = projectCreationData.getResultMapping();
        while (iterator.hasNext())
        {
            Entry<IdNameDesc, BaseArtefact> next = iterator.next();
            IdNameDesc wasToBeAdded = next.getKey();
            BaseArtefact addedArtefact = next.getValue();
            
            List<Long> newRelatedIDs = new LinkedList<>();
            for (Long oldRelatedID : wasToBeAdded.relatedIDs)
            {
                Long newID = projectCreationData.getNewIDForOldID(oldRelatedID);
                if (newID == null) 
                {
                    OpencertLogger.warn(String.format("Illegal state: cannot find newID for oldID: %d", oldRelatedID));
                    continue;
                }
                newRelatedIDs.add(newID);
            }
            baseArtefactDAO.updateAffectedBaseArtefacts(addedArtefact, newRelatedIDs, ProjectAdministrationPage.THE_EDITOR_SUPPORTED_IA_EFFECT);
        }
        
        return projectCreationData.newProjectID;
    }
    
    public String getAssuranceProjectResourcePath(AssuranceProject assuranceProject)
    {
    	return assuranceProjectDAO.getAssuranceProjectResourcePath(assuranceProject);
    }
}
