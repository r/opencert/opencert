/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface AssuranceProjectPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);


	/**
	 * @return the createdBy
	 * 
	 */
	public String getCreatedBy();

	/**
	 * Defines a new createdBy
	 * @param newValue the new createdBy to set
	 * 
	 */
	public void setCreatedBy(String newValue);


	/**
	 * @return the responsible
	 * 
	 */
	public String getResponsible();

	/**
	 * Defines a new responsible
	 * @param newValue the new responsible to set
	 * 
	 */
	public void setResponsible(String newValue);


	/**
	 * @return the date
	 * 
	 */
	public String getDate();

	/**
	 * Defines a new date
	 * @param newValue the new date to set
	 * 
	 */
	public void setDate(String newValue);


	/**
	 * @return the version
	 * 
	 */
	public String getVersion();

	/**
	 * Defines a new version
	 * @param newValue the new version to set
	 * 
	 */
	public void setVersion(String newValue);




	/**
	 * Init the subProject
	 * @param settings settings for the subProject ReferencesTable 
	 */
	public void initSubProject(ReferencesTableSettings settings);

	/**
	 * Update the subProject
	 * @param newValue the subProject to update
	 * 
	 */
	public void updateSubProject();

	/**
	 * Adds the given filter to the subProject edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToSubProject(ViewerFilter filter);

	/**
	 * Adds the given filter to the subProject edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToSubProject(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the subProject table
	 * 
	 */
	public boolean isContainedInSubProjectTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
