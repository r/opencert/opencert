/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts;

// Start of user code for imports



// End of user code

/**
 * 
 * 
 */
public interface ResourcePropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);


	/**
	 * @return the location
	 * 
	 */
	public String getLocation();

	/**
	 * Defines a new location
	 * @param newValue the new location to set
	 * 
	 */
	public void setLocation(String newValue);


	/**
	 * @return the format
	 * 
	 */
	public String getFormat();

	/**
	 * Defines a new format
	 * @param newValue the new format to set
	 * 
	 */
	public void setFormat(String newValue);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// Start IRR
		public void setUrl(String newvalue);
	    
	    public String getUrl();


	    public void setUser(String newvalue);

	    public String getUser();


	    public void setPassword(String newvalue);

	    public String getPassword();


	    public void setLocalPath(String newvalue);

	    public String getLocalPath();
	    
	    public void setUseLocalRepository(boolean newvalue);

	    public boolean getUseLocalRepository();       
	    
	    public void connectRepo();
	    //End IRR

	
	
	
	// End of user code

}
