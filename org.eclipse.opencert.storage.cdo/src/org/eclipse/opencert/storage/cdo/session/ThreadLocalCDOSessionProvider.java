/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.session;

import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.opencert.storage.cdo.StandaloneCDOAccessor;
import org.eclipse.opencert.storage.cdo.property.OpencertPropertiesReader;

public class ThreadLocalCDOSessionProvider
    implements ICDOSessionProvider
{
    private static final ThreadLocal<CDOSession> threadLocalCDOSessionHolder;
    
    static {
        threadLocalCDOSessionHolder = new ThreadLocal<CDOSession>();
    }
    

    @Override
    public CDOSession getActiveSession()
    {
        CDOSession cdoSession = threadLocalCDOSessionHolder.get();
        if (cdoSession == null) {
            
            StandaloneCDOAccessor standaloneCDOAccessor = StandaloneCDOAccessor.getStandaloneCDOAccessor(OpencertPropertiesReader.getInstance());
            cdoSession = standaloneCDOAccessor.openSession();
            
            System.out.println("CDOSession created: " + cdoSession.getSessionID());
            
            threadLocalCDOSessionHolder.set(cdoSession);
        }
        
        return cdoSession;
    }
    
    
    @Override
    public void close()
    {        
        CDOSession cdoSession = threadLocalCDOSessionHolder.get();
        
        if (cdoSession != null) {         
            System.out.println("CDOSession closed: " + cdoSession.getSessionID());
            cdoSession.close();
            threadLocalCDOSessionHolder.set(null);
        }
                
    }
}
