/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.providers;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.providers.impl.PropertiesEditingProviderImpl;

import org.eclipse.jface.viewers.IFilter;

import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;

import org.eclipse.opencert.apm.baseline.baseline.components.BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent;
import org.eclipse.opencert.apm.baseline.baseline.components.BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent;
import org.eclipse.opencert.apm.baseline.baseline.components.BaseArtefactBaseArtefactSelectionPropertiesEditionComponent;
import org.eclipse.opencert.apm.baseline.baseline.components.BaseArtefactBasePropertiesEditionComponent;
import org.eclipse.opencert.apm.baseline.baseline.components.BaseArtefactPropertiesEditionComponent;

/**
 * 
 * 
 */
public class BaseArtefactPropertiesEditionProvider extends PropertiesEditingProviderImpl {

	/**
	 * Constructor without provider for super types.
	 */
	public BaseArtefactPropertiesEditionProvider() {
		super();
	}

	/**
	 * Constructor with providers for super types.
	 * @param superProviders providers to use for super types.
	 */
	public BaseArtefactPropertiesEditionProvider(List<PropertiesEditingProvider> superProviders) {
		super(superProviders);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext) {
		return (editingContext.getEObject() instanceof BaseArtefact) 
					&& (BaselinePackage.Literals.BASE_ARTEFACT == editingContext.getEObject().eClass());
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext, String part) {
		return (editingContext.getEObject() instanceof BaseArtefact) && (BaseArtefactBasePropertiesEditionComponent.BASE_PART.equals(part) || BaseArtefactBaseArtefactSelectionPropertiesEditionComponent.BASEARTEFACTSELECTION_PART.equals(part) || BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.BASEARTEFACTEQUIVALENCEMAP_PART.equals(part) || BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.BASEARTEFACTCOMPLIANCEMAP_PART.equals(part));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof BaseArtefact) && (refinement == BaseArtefactBasePropertiesEditionComponent.class || refinement == BaseArtefactBaseArtefactSelectionPropertiesEditionComponent.class || refinement == BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.class || refinement == BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.class);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, String part, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof BaseArtefact) && ((BaseArtefactBasePropertiesEditionComponent.BASE_PART.equals(part) && refinement == BaseArtefactBasePropertiesEditionComponent.class) || (BaseArtefactBaseArtefactSelectionPropertiesEditionComponent.BASEARTEFACTSELECTION_PART.equals(part) && refinement == BaseArtefactBaseArtefactSelectionPropertiesEditionComponent.class) || (BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.BASEARTEFACTEQUIVALENCEMAP_PART.equals(part) && refinement == BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.class) || (BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.BASEARTEFACTCOMPLIANCEMAP_PART.equals(part) && refinement == BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.class));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode) {
		if (editingContext.getEObject() instanceof BaseArtefact) {
			return new BaseArtefactPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part) {
		if (editingContext.getEObject() instanceof BaseArtefact) {
			if (BaseArtefactBasePropertiesEditionComponent.BASE_PART.equals(part))
				return new BaseArtefactBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseArtefactBaseArtefactSelectionPropertiesEditionComponent.BASEARTEFACTSELECTION_PART.equals(part))
				return new BaseArtefactBaseArtefactSelectionPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.BASEARTEFACTEQUIVALENCEMAP_PART.equals(part))
				return new BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.BASEARTEFACTCOMPLIANCEMAP_PART.equals(part))
				return new BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String, java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part, java.lang.Class refinement) {
		if (editingContext.getEObject() instanceof BaseArtefact) {
			if (BaseArtefactBasePropertiesEditionComponent.BASE_PART.equals(part)
				&& refinement == BaseArtefactBasePropertiesEditionComponent.class)
				return new BaseArtefactBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseArtefactBaseArtefactSelectionPropertiesEditionComponent.BASEARTEFACTSELECTION_PART.equals(part)
				&& refinement == BaseArtefactBaseArtefactSelectionPropertiesEditionComponent.class)
				return new BaseArtefactBaseArtefactSelectionPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.BASEARTEFACTEQUIVALENCEMAP_PART.equals(part)
				&& refinement == BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.class)
				return new BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.BASEARTEFACTCOMPLIANCEMAP_PART.equals(part)
				&& refinement == BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.class)
				return new BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part, refinement);
	}

	/**
	 * Provides the filter used by the plugin.xml to assign part forms.
	 */
	public static class EditionFilter implements IFilter {
		
		/**
		 * {@inheritDoc}
		 * 
		 * @see org.eclipse.jface.viewers.IFilter#select(java.lang.Object)
		 */
		public boolean select(Object toTest) {
			EObject eObj = EEFUtils.resolveSemanticObject(toTest);
			return eObj != null && BaselinePackage.Literals.BASE_ARTEFACT == eObj.eClass();
		}
		
	}

}
