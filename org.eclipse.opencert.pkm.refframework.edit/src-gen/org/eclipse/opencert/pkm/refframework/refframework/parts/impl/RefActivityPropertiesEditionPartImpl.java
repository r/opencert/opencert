/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;

import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;

// End of user code

/**
 * 
 * 
 */
public class RefActivityPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, RefActivityPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text objective;
	protected Text scope;
	protected ReferencesTable requiredArtefact;
	protected List<ViewerFilter> requiredArtefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> requiredArtefactFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable producedArtefact;
	protected List<ViewerFilter> producedArtefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> producedArtefactFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable subActivity;
	protected List<ViewerFilter> subActivityBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> subActivityFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable precedingActivity;
	protected List<ViewerFilter> precedingActivityBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> precedingActivityFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable role;
	protected List<ViewerFilter> roleBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> roleFilters = new ArrayList<ViewerFilter>();
	protected EObjectFlatComboViewer applicableTechnique;
	protected ReferencesTable ownedRel;
	protected List<ViewerFilter> ownedRelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRelFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public RefActivityPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence refActivityStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = refActivityStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.class);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.id);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.name);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.description);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.objective);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.scope);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.producedArtefact);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.subActivity);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.precedingActivity);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.role);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique);
		propertiesStep.addStep(RefframeworkViewsRepository.RefActivity.Properties.ownedRel);
		
		
		composer = new PartComposer(refActivityStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.RefActivity.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.id) {
					return createIdText(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.name) {
					return createNameText(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.description) {
					return createDescriptionTextarea(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.objective) {
					return createObjectiveTextarea(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.scope) {
					return createScopeTextarea(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact) {
					return createRequiredArtefactAdvancedReferencesTable(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.producedArtefact) {
					return createProducedArtefactAdvancedReferencesTable(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.subActivity) {
					return createSubActivityAdvancedTableComposition(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.precedingActivity) {
					return createPrecedingActivityAdvancedReferencesTable(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.role) {
					return createRoleAdvancedReferencesTable(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique) {
					return createApplicableTechniqueFlatComboViewer(parent);
				}
				if (key == RefframeworkViewsRepository.RefActivity.Properties.ownedRel) {
					return createOwnedRelAdvancedTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(RefframeworkMessages.RefActivityPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefActivity.Properties.id, RefframeworkMessages.RefActivityPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, RefframeworkViewsRepository.RefActivity.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.id, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefActivity.Properties.name, RefframeworkMessages.RefActivityPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, RefframeworkViewsRepository.RefActivity.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.name, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionTextarea(Composite parent) {
		Label descriptionLabel = createDescription(parent, RefframeworkViewsRepository.RefActivity.Properties.description, RefframeworkMessages.RefActivityPropertiesEditionPart_DescriptionLabel);
		GridData descriptionLabelData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionLabelData.horizontalSpan = 3;
		descriptionLabel.setLayoutData(descriptionLabelData);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionData.horizontalSpan = 2;
		descriptionData.heightHint = 80;
		descriptionData.widthHint = 200;
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		EditingUtils.setID(description, RefframeworkViewsRepository.RefActivity.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Textarea"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.description, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createObjectiveTextarea(Composite parent) {
		Label objectiveLabel = createDescription(parent, RefframeworkViewsRepository.RefActivity.Properties.objective, RefframeworkMessages.RefActivityPropertiesEditionPart_ObjectiveLabel);
		GridData objectiveLabelData = new GridData(GridData.FILL_HORIZONTAL);
		objectiveLabelData.horizontalSpan = 3;
		objectiveLabel.setLayoutData(objectiveLabelData);
		objective = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
		GridData objectiveData = new GridData(GridData.FILL_HORIZONTAL);
		objectiveData.horizontalSpan = 2;
		objectiveData.heightHint = 80;
		objectiveData.widthHint = 200;
		objective.setLayoutData(objectiveData);
		objective.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.objective, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, objective.getText()));
			}

		});
		EditingUtils.setID(objective, RefframeworkViewsRepository.RefActivity.Properties.objective);
		EditingUtils.setEEFtype(objective, "eef::Textarea"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.objective, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createObjectiveTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createScopeTextarea(Composite parent) {
		Label scopeLabel = createDescription(parent, RefframeworkViewsRepository.RefActivity.Properties.scope, RefframeworkMessages.RefActivityPropertiesEditionPart_ScopeLabel);
		GridData scopeLabelData = new GridData(GridData.FILL_HORIZONTAL);
		scopeLabelData.horizontalSpan = 3;
		scopeLabel.setLayoutData(scopeLabelData);
		scope = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
		GridData scopeData = new GridData(GridData.FILL_HORIZONTAL);
		scopeData.horizontalSpan = 2;
		scopeData.heightHint = 80;
		scopeData.widthHint = 200;
		scope.setLayoutData(scopeData);
		scope.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.scope, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, scope.getText()));
			}

		});
		EditingUtils.setID(scope, RefframeworkViewsRepository.RefActivity.Properties.scope);
		EditingUtils.setEEFtype(scope, "eef::Textarea"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.scope, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createScopeTextArea

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createRequiredArtefactAdvancedReferencesTable(Composite parent) {
		String label = getDescription(RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact, RefframeworkMessages.RefActivityPropertiesEditionPart_RequiredArtefactLabel);		 
		this.requiredArtefact = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addRequiredArtefact(); }
			public void handleEdit(EObject element) { editRequiredArtefact(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveRequiredArtefact(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromRequiredArtefact(element); }
			public void navigateTo(EObject element) { }
		});
		this.requiredArtefact.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact, RefframeworkViewsRepository.SWT_KIND));
		this.requiredArtefact.createControls(parent);
		this.requiredArtefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData requiredArtefactData = new GridData(GridData.FILL_HORIZONTAL);
		requiredArtefactData.horizontalSpan = 3;
		this.requiredArtefact.setLayoutData(requiredArtefactData);
		this.requiredArtefact.disableMove();
		requiredArtefact.setID(RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact);
		requiredArtefact.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addRequiredArtefact() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(requiredArtefact.getInput(), requiredArtefactFilters, requiredArtefactBusinessFilters,
		"requiredArtefact", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				requiredArtefact.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveRequiredArtefact(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		requiredArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromRequiredArtefact(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		requiredArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void editRequiredArtefact(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				requiredArtefact.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createProducedArtefactAdvancedReferencesTable(Composite parent) {
		String label = getDescription(RefframeworkViewsRepository.RefActivity.Properties.producedArtefact, RefframeworkMessages.RefActivityPropertiesEditionPart_ProducedArtefactLabel);		 
		this.producedArtefact = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addProducedArtefact(); }
			public void handleEdit(EObject element) { editProducedArtefact(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveProducedArtefact(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromProducedArtefact(element); }
			public void navigateTo(EObject element) { }
		});
		this.producedArtefact.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.producedArtefact, RefframeworkViewsRepository.SWT_KIND));
		this.producedArtefact.createControls(parent);
		this.producedArtefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.producedArtefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData producedArtefactData = new GridData(GridData.FILL_HORIZONTAL);
		producedArtefactData.horizontalSpan = 3;
		this.producedArtefact.setLayoutData(producedArtefactData);
		this.producedArtefact.disableMove();
		producedArtefact.setID(RefframeworkViewsRepository.RefActivity.Properties.producedArtefact);
		producedArtefact.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addProducedArtefact() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(producedArtefact.getInput(), producedArtefactFilters, producedArtefactBusinessFilters,
		"producedArtefact", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.producedArtefact,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				producedArtefact.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveProducedArtefact(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.producedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		producedArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromProducedArtefact(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.producedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		producedArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void editProducedArtefact(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				producedArtefact.refresh();
			}
		}
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createSubActivityAdvancedTableComposition(Composite parent) {
		this.subActivity = new ReferencesTable(getDescription(RefframeworkViewsRepository.RefActivity.Properties.subActivity, RefframeworkMessages.RefActivityPropertiesEditionPart_SubActivityLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.subActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				subActivity.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.subActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				subActivity.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.subActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				subActivity.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.subActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				subActivity.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.subActivityFilters) {
			this.subActivity.addFilter(filter);
		}
		this.subActivity.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.subActivity, RefframeworkViewsRepository.SWT_KIND));
		this.subActivity.createControls(parent);
		this.subActivity.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.subActivity, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData subActivityData = new GridData(GridData.FILL_HORIZONTAL);
		subActivityData.horizontalSpan = 3;
		this.subActivity.setLayoutData(subActivityData);
		this.subActivity.setLowerBound(0);
		this.subActivity.setUpperBound(-1);
		subActivity.setID(RefframeworkViewsRepository.RefActivity.Properties.subActivity);
		subActivity.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createSubActivityAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createPrecedingActivityAdvancedReferencesTable(Composite parent) {
		String label = getDescription(RefframeworkViewsRepository.RefActivity.Properties.precedingActivity, RefframeworkMessages.RefActivityPropertiesEditionPart_PrecedingActivityLabel);		 
		this.precedingActivity = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addPrecedingActivity(); }
			public void handleEdit(EObject element) { editPrecedingActivity(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { movePrecedingActivity(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromPrecedingActivity(element); }
			public void navigateTo(EObject element) { }
		});
		this.precedingActivity.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.precedingActivity, RefframeworkViewsRepository.SWT_KIND));
		this.precedingActivity.createControls(parent);
		this.precedingActivity.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.precedingActivity, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData precedingActivityData = new GridData(GridData.FILL_HORIZONTAL);
		precedingActivityData.horizontalSpan = 3;
		this.precedingActivity.setLayoutData(precedingActivityData);
		this.precedingActivity.disableMove();
		precedingActivity.setID(RefframeworkViewsRepository.RefActivity.Properties.precedingActivity);
		precedingActivity.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addPrecedingActivity() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(precedingActivity.getInput(), precedingActivityFilters, precedingActivityBusinessFilters,
		"precedingActivity", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.precedingActivity,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				precedingActivity.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void movePrecedingActivity(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.precedingActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		precedingActivity.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromPrecedingActivity(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.precedingActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		precedingActivity.refresh();
	}

	/**
	 * 
	 */
	protected void editPrecedingActivity(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				precedingActivity.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createRoleAdvancedReferencesTable(Composite parent) {
		String label = getDescription(RefframeworkViewsRepository.RefActivity.Properties.role, RefframeworkMessages.RefActivityPropertiesEditionPart_RoleLabel);		 
		this.role = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addRole(); }
			public void handleEdit(EObject element) { editRole(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveRole(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromRole(element); }
			public void navigateTo(EObject element) { }
		});
		this.role.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.role, RefframeworkViewsRepository.SWT_KIND));
		this.role.createControls(parent);
		this.role.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.role, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData roleData = new GridData(GridData.FILL_HORIZONTAL);
		roleData.horizontalSpan = 3;
		this.role.setLayoutData(roleData);
		this.role.disableMove();
		role.setID(RefframeworkViewsRepository.RefActivity.Properties.role);
		role.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addRole() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(role.getInput(), roleFilters, roleBusinessFilters,
		"role", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.role,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				role.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveRole(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.role, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		role.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromRole(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.role, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		role.refresh();
	}

	/**
	 * 
	 */
	protected void editRole(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				role.refresh();
			}
		}
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createApplicableTechniqueFlatComboViewer(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique, RefframeworkMessages.RefActivityPropertiesEditionPart_ApplicableTechniqueLabel);
		applicableTechnique = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique, RefframeworkViewsRepository.SWT_KIND));
		applicableTechnique.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		applicableTechnique.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getApplicableTechnique()));
			}

		});
		GridData applicableTechniqueData = new GridData(GridData.FILL_HORIZONTAL);
		applicableTechnique.setLayoutData(applicableTechniqueData);
		applicableTechnique.setID(RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createApplicableTechniqueFlatComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRelAdvancedTableComposition(Composite parent) {
		this.ownedRel = new ReferencesTable(getDescription(RefframeworkViewsRepository.RefActivity.Properties.ownedRel, RefframeworkMessages.RefActivityPropertiesEditionPart_OwnedRelLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRel.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRelFilters) {
			this.ownedRel.addFilter(filter);
		}
		this.ownedRel.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefActivity.Properties.ownedRel, RefframeworkViewsRepository.SWT_KIND));
		this.ownedRel.createControls(parent);
		this.ownedRel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefActivityPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefActivity.Properties.ownedRel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRelData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRelData.horizontalSpan = 3;
		this.ownedRel.setLayoutData(ownedRelData);
		this.ownedRel.setLowerBound(0);
		this.ownedRel.setUpperBound(-1);
		ownedRel.setID(RefframeworkViewsRepository.RefActivity.Properties.ownedRel);
		ownedRel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRelAdvancedTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setBackground(description.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			description.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#getObjective()
	 * 
	 */
	public String getObjective() {
		return objective.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#setObjective(String newValue)
	 * 
	 */
	public void setObjective(String newValue) {
		if (newValue != null) {
			objective.setText(newValue);
		} else {
			objective.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.objective);
		if (eefElementEditorReadOnlyState && objective.isEnabled()) {
			objective.setEnabled(false);
			objective.setBackground(objective.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			objective.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !objective.isEnabled()) {
			objective.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#getScope()
	 * 
	 */
	public String getScope() {
		return scope.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#setScope(String newValue)
	 * 
	 */
	public void setScope(String newValue) {
		if (newValue != null) {
			scope.setText(newValue);
		} else {
			scope.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.scope);
		if (eefElementEditorReadOnlyState && scope.isEnabled()) {
			scope.setEnabled(false);
			scope.setBackground(scope.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			scope.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !scope.isEnabled()) {
			scope.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#initRequiredArtefact(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initRequiredArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		requiredArtefact.setContentProvider(contentProvider);
		requiredArtefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.requiredArtefact);
		if (eefElementEditorReadOnlyState && requiredArtefact.getTable().isEnabled()) {
			requiredArtefact.setEnabled(false);
			requiredArtefact.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !requiredArtefact.getTable().isEnabled()) {
			requiredArtefact.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#updateRequiredArtefact()
	 * 
	 */
	public void updateRequiredArtefact() {
	requiredArtefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addFilterRequiredArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToRequiredArtefact(ViewerFilter filter) {
		requiredArtefactFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addBusinessFilterRequiredArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToRequiredArtefact(ViewerFilter filter) {
		requiredArtefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#isContainedInRequiredArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInRequiredArtefactTable(EObject element) {
		return ((ReferencesTableSettings)requiredArtefact.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#initProducedArtefact(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initProducedArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		producedArtefact.setContentProvider(contentProvider);
		producedArtefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.producedArtefact);
		if (eefElementEditorReadOnlyState && producedArtefact.getTable().isEnabled()) {
			producedArtefact.setEnabled(false);
			producedArtefact.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !producedArtefact.getTable().isEnabled()) {
			producedArtefact.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#updateProducedArtefact()
	 * 
	 */
	public void updateProducedArtefact() {
	producedArtefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addFilterProducedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToProducedArtefact(ViewerFilter filter) {
		producedArtefactFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addBusinessFilterProducedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToProducedArtefact(ViewerFilter filter) {
		producedArtefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#isContainedInProducedArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInProducedArtefactTable(EObject element) {
		return ((ReferencesTableSettings)producedArtefact.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#initSubActivity(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initSubActivity(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		subActivity.setContentProvider(contentProvider);
		subActivity.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.subActivity);
		if (eefElementEditorReadOnlyState && subActivity.isEnabled()) {
			subActivity.setEnabled(false);
			subActivity.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !subActivity.isEnabled()) {
			subActivity.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#updateSubActivity()
	 * 
	 */
	public void updateSubActivity() {
	subActivity.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addFilterSubActivity(ViewerFilter filter)
	 * 
	 */
	public void addFilterToSubActivity(ViewerFilter filter) {
		subActivityFilters.add(filter);
		if (this.subActivity != null) {
			this.subActivity.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addBusinessFilterSubActivity(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToSubActivity(ViewerFilter filter) {
		subActivityBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#isContainedInSubActivityTable(EObject element)
	 * 
	 */
	public boolean isContainedInSubActivityTable(EObject element) {
		return ((ReferencesTableSettings)subActivity.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#initPrecedingActivity(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initPrecedingActivity(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		precedingActivity.setContentProvider(contentProvider);
		precedingActivity.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.precedingActivity);
		if (eefElementEditorReadOnlyState && precedingActivity.getTable().isEnabled()) {
			precedingActivity.setEnabled(false);
			precedingActivity.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !precedingActivity.getTable().isEnabled()) {
			precedingActivity.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#updatePrecedingActivity()
	 * 
	 */
	public void updatePrecedingActivity() {
	precedingActivity.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addFilterPrecedingActivity(ViewerFilter filter)
	 * 
	 */
	public void addFilterToPrecedingActivity(ViewerFilter filter) {
		precedingActivityFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addBusinessFilterPrecedingActivity(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToPrecedingActivity(ViewerFilter filter) {
		precedingActivityBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#isContainedInPrecedingActivityTable(EObject element)
	 * 
	 */
	public boolean isContainedInPrecedingActivityTable(EObject element) {
		return ((ReferencesTableSettings)precedingActivity.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#initRole(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initRole(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		role.setContentProvider(contentProvider);
		role.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.role);
		if (eefElementEditorReadOnlyState && role.getTable().isEnabled()) {
			role.setEnabled(false);
			role.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !role.getTable().isEnabled()) {
			role.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#updateRole()
	 * 
	 */
	public void updateRole() {
	role.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addFilterRole(ViewerFilter filter)
	 * 
	 */
	public void addFilterToRole(ViewerFilter filter) {
		roleFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addBusinessFilterRole(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToRole(ViewerFilter filter) {
		roleBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#isContainedInRoleTable(EObject element)
	 * 
	 */
	public boolean isContainedInRoleTable(EObject element) {
		return ((ReferencesTableSettings)role.getInput()).contains(element);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#getApplicableTechnique()
	 * 
	 */
	public EObject getApplicableTechnique() {
		if (applicableTechnique.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) applicableTechnique.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#initApplicableTechnique(EObjectFlatComboSettings)
	 */
	public void initApplicableTechnique(EObjectFlatComboSettings settings) {
		applicableTechnique.setInput(settings);
		if (current != null) {
			applicableTechnique.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique);
		if (eefElementEditorReadOnlyState && applicableTechnique.isEnabled()) {
			applicableTechnique.setEnabled(false);
			applicableTechnique.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicableTechnique.isEnabled()) {
			applicableTechnique.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#setApplicableTechnique(EObject newValue)
	 * 
	 */
	public void setApplicableTechnique(EObject newValue) {
		if (newValue != null) {
			applicableTechnique.setSelection(new StructuredSelection(newValue));
		} else {
			applicableTechnique.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.applicableTechnique);
		if (eefElementEditorReadOnlyState && applicableTechnique.isEnabled()) {
			applicableTechnique.setEnabled(false);
			applicableTechnique.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicableTechnique.isEnabled()) {
			applicableTechnique.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#setApplicableTechniqueButtonMode(ButtonsModeEnum newValue)
	 */
	public void setApplicableTechniqueButtonMode(ButtonsModeEnum newValue) {
		applicableTechnique.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addFilterApplicableTechnique(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicableTechnique(ViewerFilter filter) {
		applicableTechnique.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addBusinessFilterApplicableTechnique(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToApplicableTechnique(ViewerFilter filter) {
		applicableTechnique.addBusinessRuleFilter(filter);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#initOwnedRel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRel.setContentProvider(contentProvider);
		ownedRel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefActivity.Properties.ownedRel);
		if (eefElementEditorReadOnlyState && ownedRel.isEnabled()) {
			ownedRel.setEnabled(false);
			ownedRel.setToolTipText(RefframeworkMessages.RefActivity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRel.isEnabled()) {
			ownedRel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#updateOwnedRel()
	 * 
	 */
	public void updateOwnedRel() {
	ownedRel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRel(ViewerFilter filter) {
		ownedRelFilters.add(filter);
		if (this.ownedRel != null) {
			this.ownedRel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#addBusinessFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRel(ViewerFilter filter) {
		ownedRelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefActivityPropertiesEditionPart#isContainedInOwnedRelTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRelTable(EObject element) {
		return ((ReferencesTableSettings)ownedRel.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.RefActivity_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
