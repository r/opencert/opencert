/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;
import org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart;

import org.eclipse.opencert.sam.arg.arg.providers.ArgMessages;

// End of user code

/**
 * 
 * 
 */
public class ArgumentationPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ArgumentationPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text content;
	protected ReferencesTable evaluation;
	protected List<ViewerFilter> evaluationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> evaluationFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable lifecycleEvent;
	protected List<ViewerFilter> lifecycleEventBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> lifecycleEventFilters = new ArrayList<ViewerFilter>();
	protected Text location;
	protected ReferencesTable argumentation;
	protected List<ViewerFilter> argumentationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> argumentationFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable consistOf;
	protected List<ViewerFilter> consistOfBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> consistOfFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public ArgumentationPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArgumentationPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence argumentationStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = argumentationStep.addStep(ArgViewsRepository.Argumentation.Properties.class);
		propertiesStep.addStep(ArgViewsRepository.Argumentation.Properties.id);
		propertiesStep.addStep(ArgViewsRepository.Argumentation.Properties.name);
		propertiesStep.addStep(ArgViewsRepository.Argumentation.Properties.description);
		propertiesStep.addStep(ArgViewsRepository.Argumentation.Properties.content);
		propertiesStep.addStep(ArgViewsRepository.Argumentation.Properties.evaluation);
		propertiesStep.addStep(ArgViewsRepository.Argumentation.Properties.lifecycleEvent);
		propertiesStep.addStep(ArgViewsRepository.Argumentation.Properties.location);
		propertiesStep.addStep(ArgViewsRepository.Argumentation.Properties.argumentation_);
		propertiesStep.addStep(ArgViewsRepository.Argumentation.Properties.consistOf);
		
		
		composer = new PartComposer(argumentationStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ArgViewsRepository.Argumentation.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Argumentation.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Argumentation.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Argumentation.Properties.description) {
					return createDescriptionText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Argumentation.Properties.content) {
					return createContentText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Argumentation.Properties.evaluation) {
					return createEvaluationTableComposition(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Argumentation.Properties.lifecycleEvent) {
					return createLifecycleEventTableComposition(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Argumentation.Properties.location) {
					return createLocationText(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Argumentation.Properties.argumentation_) {
					return createArgumentationReferencesTable(widgetFactory, parent);
				}
				if (key == ArgViewsRepository.Argumentation.Properties.consistOf) {
					return createConsistOfReferencesTable(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(ArgMessages.ArgumentationPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.Argumentation.Properties.id, ArgMessages.ArgumentationPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentationPropertiesEditionPartForm.this,
							ArgViewsRepository.Argumentation.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentationPropertiesEditionPartForm.this,
									ArgViewsRepository.Argumentation.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, ArgViewsRepository.Argumentation.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.Argumentation.Properties.id, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.Argumentation.Properties.name, ArgMessages.ArgumentationPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentationPropertiesEditionPartForm.this,
							ArgViewsRepository.Argumentation.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentationPropertiesEditionPartForm.this,
									ArgViewsRepository.Argumentation.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, ArgViewsRepository.Argumentation.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.Argumentation.Properties.name, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.Argumentation.Properties.description, ArgMessages.ArgumentationPropertiesEditionPart_DescriptionLabel);
		description = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		description.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentationPropertiesEditionPartForm.this,
							ArgViewsRepository.Argumentation.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentationPropertiesEditionPartForm.this,
									ArgViewsRepository.Argumentation.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		description.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}
		});
		EditingUtils.setID(description, ArgViewsRepository.Argumentation.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.Argumentation.Properties.description, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createContentText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.Argumentation.Properties.content, ArgMessages.ArgumentationPropertiesEditionPart_ContentLabel);
		content = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		content.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData contentData = new GridData(GridData.FILL_HORIZONTAL);
		content.setLayoutData(contentData);
		content.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentationPropertiesEditionPartForm.this,
							ArgViewsRepository.Argumentation.Properties.content,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, content.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentationPropertiesEditionPartForm.this,
									ArgViewsRepository.Argumentation.Properties.content,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, content.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		content.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.content, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, content.getText()));
				}
			}
		});
		EditingUtils.setID(content, ArgViewsRepository.Argumentation.Properties.content);
		EditingUtils.setEEFtype(content, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.Argumentation.Properties.content, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createContentText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEvaluationTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.evaluation = new ReferencesTable(getDescription(ArgViewsRepository.Argumentation.Properties.evaluation, ArgMessages.ArgumentationPropertiesEditionPart_EvaluationLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				evaluation.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				evaluation.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				evaluation.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				evaluation.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.evaluationFilters) {
			this.evaluation.addFilter(filter);
		}
		this.evaluation.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Argumentation.Properties.evaluation, ArgViewsRepository.FORM_KIND));
		this.evaluation.createControls(parent, widgetFactory);
		this.evaluation.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.evaluation, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData evaluationData = new GridData(GridData.FILL_HORIZONTAL);
		evaluationData.horizontalSpan = 3;
		this.evaluation.setLayoutData(evaluationData);
		this.evaluation.setLowerBound(0);
		this.evaluation.setUpperBound(-1);
		evaluation.setID(ArgViewsRepository.Argumentation.Properties.evaluation);
		evaluation.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEvaluationTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createLifecycleEventTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.lifecycleEvent = new ReferencesTable(getDescription(ArgViewsRepository.Argumentation.Properties.lifecycleEvent, ArgMessages.ArgumentationPropertiesEditionPart_LifecycleEventLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				lifecycleEvent.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				lifecycleEvent.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				lifecycleEvent.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				lifecycleEvent.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.lifecycleEventFilters) {
			this.lifecycleEvent.addFilter(filter);
		}
		this.lifecycleEvent.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Argumentation.Properties.lifecycleEvent, ArgViewsRepository.FORM_KIND));
		this.lifecycleEvent.createControls(parent, widgetFactory);
		this.lifecycleEvent.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.lifecycleEvent, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData lifecycleEventData = new GridData(GridData.FILL_HORIZONTAL);
		lifecycleEventData.horizontalSpan = 3;
		this.lifecycleEvent.setLayoutData(lifecycleEventData);
		this.lifecycleEvent.setLowerBound(0);
		this.lifecycleEvent.setUpperBound(-1);
		lifecycleEvent.setID(ArgViewsRepository.Argumentation.Properties.lifecycleEvent);
		lifecycleEvent.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createLifecycleEventTableComposition

		// End of user code
		return parent;
	}

	
	protected Composite createLocationText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ArgViewsRepository.Argumentation.Properties.location, ArgMessages.ArgumentationPropertiesEditionPart_LocationLabel);
		location = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		location.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData locationData = new GridData(GridData.FILL_HORIZONTAL);
		location.setLayoutData(locationData);
		location.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ArgumentationPropertiesEditionPartForm.this,
							ArgViewsRepository.Argumentation.Properties.location,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentationPropertiesEditionPartForm.this,
									ArgViewsRepository.Argumentation.Properties.location,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, location.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ArgumentationPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		location.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
				}
			}
		});
		EditingUtils.setID(location, ArgViewsRepository.Argumentation.Properties.location);
		EditingUtils.setEEFtype(location, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ArgViewsRepository.Argumentation.Properties.location, ArgViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createLocationText

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createArgumentationReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.argumentation = new ReferencesTable(getDescription(ArgViewsRepository.Argumentation.Properties.argumentation_, ArgMessages.ArgumentationPropertiesEditionPart_ArgumentationLabel), new ReferencesTableListener	() {
			public void handleAdd() { addArgumentation(); }
			public void handleEdit(EObject element) { editArgumentation(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveArgumentation(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromArgumentation(element); }
			public void navigateTo(EObject element) { }
		});
		this.argumentation.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Argumentation.Properties.argumentation_, ArgViewsRepository.FORM_KIND));
		this.argumentation.createControls(parent, widgetFactory);
		this.argumentation.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.argumentation_, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData argumentationData = new GridData(GridData.FILL_HORIZONTAL);
		argumentationData.horizontalSpan = 3;
		this.argumentation.setLayoutData(argumentationData);
		this.argumentation.disableMove();
		argumentation.setID(ArgViewsRepository.Argumentation.Properties.argumentation_);
		argumentation.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createArgumentationReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addArgumentation() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(argumentation.getInput(), argumentationFilters, argumentationBusinessFilters,
		"argumentation", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.argumentation_,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				argumentation.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveArgumentation(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.argumentation_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		argumentation.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromArgumentation(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.argumentation_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		argumentation.refresh();
	}

	/**
	 * 
	 */
	protected void editArgumentation(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				argumentation.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createConsistOfReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.consistOf = new ReferencesTable(getDescription(ArgViewsRepository.Argumentation.Properties.consistOf, ArgMessages.ArgumentationPropertiesEditionPart_ConsistOfLabel), new ReferencesTableListener	() {
			public void handleAdd() { addConsistOf(); }
			public void handleEdit(EObject element) { editConsistOf(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveConsistOf(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromConsistOf(element); }
			public void navigateTo(EObject element) { }
		});
		this.consistOf.setHelpText(propertiesEditionComponent.getHelpContent(ArgViewsRepository.Argumentation.Properties.consistOf, ArgViewsRepository.FORM_KIND));
		this.consistOf.createControls(parent, widgetFactory);
		this.consistOf.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.consistOf, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData consistOfData = new GridData(GridData.FILL_HORIZONTAL);
		consistOfData.horizontalSpan = 3;
		this.consistOf.setLayoutData(consistOfData);
		this.consistOf.disableMove();
		consistOf.setID(ArgViewsRepository.Argumentation.Properties.consistOf);
		consistOf.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createConsistOfReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addConsistOf() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(consistOf.getInput(), consistOfFilters, consistOfBusinessFilters,
		"consistOf", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.consistOf,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				consistOf.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveConsistOf(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.consistOf, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		consistOf.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromConsistOf(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArgumentationPropertiesEditionPartForm.this, ArgViewsRepository.Argumentation.Properties.consistOf, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		consistOf.refresh();
	}

	/**
	 * 
	 */
	protected void editConsistOf(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				consistOf.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Argumentation.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(ArgMessages.Argumentation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Argumentation.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(ArgMessages.Argumentation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Argumentation.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(ArgMessages.Argumentation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#getContent()
	 * 
	 */
	public String getContent() {
		return content.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#setContent(String newValue)
	 * 
	 */
	public void setContent(String newValue) {
		if (newValue != null) {
			content.setText(newValue);
		} else {
			content.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Argumentation.Properties.content);
		if (eefElementEditorReadOnlyState && content.isEnabled()) {
			content.setEnabled(false);
			content.setToolTipText(ArgMessages.Argumentation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !content.isEnabled()) {
			content.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#initEvaluation(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEvaluation(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		evaluation.setContentProvider(contentProvider);
		evaluation.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Argumentation.Properties.evaluation);
		if (eefElementEditorReadOnlyState && evaluation.isEnabled()) {
			evaluation.setEnabled(false);
			evaluation.setToolTipText(ArgMessages.Argumentation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !evaluation.isEnabled()) {
			evaluation.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#updateEvaluation()
	 * 
	 */
	public void updateEvaluation() {
	evaluation.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#addFilterEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEvaluation(ViewerFilter filter) {
		evaluationFilters.add(filter);
		if (this.evaluation != null) {
			this.evaluation.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#addBusinessFilterEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEvaluation(ViewerFilter filter) {
		evaluationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#isContainedInEvaluationTable(EObject element)
	 * 
	 */
	public boolean isContainedInEvaluationTable(EObject element) {
		return ((ReferencesTableSettings)evaluation.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#initLifecycleEvent(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initLifecycleEvent(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		lifecycleEvent.setContentProvider(contentProvider);
		lifecycleEvent.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Argumentation.Properties.lifecycleEvent);
		if (eefElementEditorReadOnlyState && lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(false);
			lifecycleEvent.setToolTipText(ArgMessages.Argumentation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#updateLifecycleEvent()
	 * 
	 */
	public void updateLifecycleEvent() {
	lifecycleEvent.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#addFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventFilters.add(filter);
		if (this.lifecycleEvent != null) {
			this.lifecycleEvent.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#addBusinessFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#isContainedInLifecycleEventTable(EObject element)
	 * 
	 */
	public boolean isContainedInLifecycleEventTable(EObject element) {
		return ((ReferencesTableSettings)lifecycleEvent.getInput()).contains(element);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#getLocation()
	 * 
	 */
	public String getLocation() {
		return location.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#setLocation(String newValue)
	 * 
	 */
	public void setLocation(String newValue) {
		if (newValue != null) {
			location.setText(newValue);
		} else {
			location.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Argumentation.Properties.location);
		if (eefElementEditorReadOnlyState && location.isEnabled()) {
			location.setEnabled(false);
			location.setToolTipText(ArgMessages.Argumentation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !location.isEnabled()) {
			location.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#initArgumentation(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initArgumentation(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		argumentation.setContentProvider(contentProvider);
		argumentation.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Argumentation.Properties.argumentation_);
		if (eefElementEditorReadOnlyState && argumentation.getTable().isEnabled()) {
			argumentation.setEnabled(false);
			argumentation.setToolTipText(ArgMessages.Argumentation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !argumentation.getTable().isEnabled()) {
			argumentation.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#updateArgumentation()
	 * 
	 */
	public void updateArgumentation() {
	argumentation.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#addFilterArgumentation(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArgumentation(ViewerFilter filter) {
		argumentationFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#addBusinessFilterArgumentation(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArgumentation(ViewerFilter filter) {
		argumentationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#isContainedInArgumentationTable(EObject element)
	 * 
	 */
	public boolean isContainedInArgumentationTable(EObject element) {
		return ((ReferencesTableSettings)argumentation.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#initConsistOf(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initConsistOf(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		consistOf.setContentProvider(contentProvider);
		consistOf.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ArgViewsRepository.Argumentation.Properties.consistOf);
		if (eefElementEditorReadOnlyState && consistOf.getTable().isEnabled()) {
			consistOf.setEnabled(false);
			consistOf.setToolTipText(ArgMessages.Argumentation_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !consistOf.getTable().isEnabled()) {
			consistOf.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#updateConsistOf()
	 * 
	 */
	public void updateConsistOf() {
	consistOf.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#addFilterConsistOf(ViewerFilter filter)
	 * 
	 */
	public void addFilterToConsistOf(ViewerFilter filter) {
		consistOfFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#addBusinessFilterConsistOf(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToConsistOf(ViewerFilter filter) {
		consistOfBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.sam.arg.arg.parts.ArgumentationPropertiesEditionPart#isContainedInConsistOfTable(EObject element)
	 * 
	 */
	public boolean isContainedInConsistOfTable(EObject element) {
		return ((ReferencesTableSettings)consistOf.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ArgMessages.Argumentation_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
