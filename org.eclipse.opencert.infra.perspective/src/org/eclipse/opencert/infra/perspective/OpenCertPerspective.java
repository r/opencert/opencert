/*******************************************************************************
 * Copyright (c) 2017 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.perspective;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class OpenCertPerspective implements IPerspectiveFactory {

	private final static String ID_DAWN_EXPLORER ="org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer";
	private final static String ID_TEMPLATE ="org.eclipse.opencert.sam.arg.ui.views.ExpandBarView";
	private final static String ID_REUSE ="org.eclipse.opencert.apm.assurproj.reuse.views.ReuseAssistanceView";

	public void createInitialLayout(IPageLayout layout) {
	    defineActions(layout);
	    try {
			defineLayout(layout);
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void defineActions(IPageLayout layout) {

        // Add "new wizards".
		layout.addNewWizardShortcut("org.eclipse.opencert.apm.assurproj.wizards.NewAssurProjWizard");
		layout.addNewWizardShortcut("org.eclipse.opencert.pkm.refframework.refframework.diagram.part.DawnRefframeworkCreationWizard");
		layout.addNewWizardShortcut("org.eclipse.opencert.apm.assurproj.wizards.NewBaselineWizard");
		layout.addNewWizardShortcut("org.eclipse.opencert.sam.arg.arg.diagram.part.DawnArgCreationWizard");
		layout.addNewWizardShortcut("org.eclipse.opencert.evm.evidspec.presentation.DawnEvidenceModelWizardID");
		layout.addNewWizardShortcut("org.eclipse.opencert.pam.procspec.presentation.DawnProcessModelWizardID");
		
		// Add "show views".
        layout.addShowViewShortcut(IPageLayout.ID_PROJECT_EXPLORER);
        layout.addShowViewShortcut(IPageLayout.ID_OUTLINE);
        layout.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);
        layout.addShowViewShortcut(IPageLayout.ID_PROBLEM_VIEW);
        layout.addShowViewShortcut(IPageLayout.ID_EDITOR_AREA);
        layout.addShowViewShortcut(ID_DAWN_EXPLORER);
        layout.addShowViewShortcut(ID_TEMPLATE);
        layout.addShowViewShortcut(ID_REUSE);
}
	
	public void defineLayout(IPageLayout layout) throws PartInitException {
        // Editors are placed for free.
		IFolderLayout leftTopLayout=layout.createFolder("leftTop", IPageLayout.LEFT, 0.26f, IPageLayout.ID_EDITOR_AREA);
		IFolderLayout leftBottomLayout=layout.createFolder("leftBottom", IPageLayout.BOTTOM, 0.50f, "leftTop");
		IFolderLayout bottomLayout=layout.createFolder("bottom", IPageLayout.BOTTOM, 0.6f, IPageLayout.ID_EDITOR_AREA);

        leftTopLayout.addView(IPageLayout.ID_PROJECT_EXPLORER);
        leftTopLayout.addView(ID_DAWN_EXPLORER);
        leftBottomLayout.addView(IPageLayout.ID_OUTLINE);
        leftBottomLayout.addView(ID_TEMPLATE);
        leftBottomLayout.addView(ID_REUSE);
        bottomLayout.addView(IPageLayout.ID_PROP_SHEET);
        PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(ID_DAWN_EXPLORER);
    }
	
}
