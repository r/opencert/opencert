/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.containers.FrameworkWrapper;
import org.eclipse.opencert.webapp.reports.listeners.IProjectChangeListener;
import org.eclipse.opencert.webapp.reports.manager.RefframeworkManager;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.util.VaadinUtil;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public abstract class AbstractEquivalenceFrameworkReport implements IProjectChangeListener, IReport
{
    private BeanItemContainer<FrameworkWrapper> frameworksContainer = new BeanItemContainer<FrameworkWrapper>(
            FrameworkWrapper.class, new LinkedList<FrameworkWrapper>());
    
    private ComboBox fromFrameworksCombo = new ComboBox(null, frameworksContainer);
    private ComboBox toFrameworksCombo = new ComboBox(null, frameworksContainer);
    
    private List<IRefFrameworksChangeListener> _fromReffChangeListeners = null;
    private List<IRefFrameworksChangeListener> _toReffChangeListeners = null;

    public abstract ReportID getReportID();

    public abstract String getTitle();

    public abstract Component getMainComponent(GUIMode guiMode);
    
    public abstract Component getExportToDocxComponent();

 
    public void projectChanged(AssuranceProjectWrapper newProject){
    	//No project changed
    }

    @Override
	public boolean isSupportsProjects(){
		return false;
	} 

    @SuppressWarnings("serial")
    public Component getParametersComponent(GUIMode guiMode)
    {
        HorizontalLayout horizontalPanel = new HorizontalLayout();
        horizontalPanel.setMargin(false);
        horizontalPanel.setSpacing(false);
        horizontalPanel.setSizeFull();
        
        setFrameworksComboBoxes(getRefframeworksfromDB());
        
        fromFrameworksCombo.setItemCaptionPropertyId("name");
        toFrameworksCombo.setItemCaptionPropertyId("name");
        VaadinUtil.initComboBox(fromFrameworksCombo);
        VaadinUtil.initComboBox(toFrameworksCombo);
        fromFrameworksCombo.setStyleName("baseFramework");
        toFrameworksCombo.setStyleName("baseFramework");
        
        fromFrameworksCombo.addValueChangeListener(new ValueChangeListener()
        {
            @Override
            public void valueChange(ValueChangeEvent event)
            {            	
            	
                refreshEquivalenceMeticsBasedOnFrameworkCombo(true);
            }
        });
        
        toFrameworksCombo.addValueChangeListener(new ValueChangeListener()
        {
            @Override
            public void valueChange(ValueChangeEvent event)
            {
            	            	
            	refreshEquivalenceMeticsBasedOnFrameworkCombo(false);
            }
        });
        
        Label labelFrom = new Label("From Reference Framework:",ContentMode.HTML);
        Label labelTo = new Label("To Reference Framework: &nbsp;&nbsp;&nbsp;&nbsp;",ContentMode.HTML);
        labelFrom.setStyleName("baselineFrameworkLabel");
        labelTo.setStyleName("baselineFrameworkLabel");
        
        VerticalLayout refframeworkLayout = new VerticalLayout();
        refframeworkLayout.setMargin(false);
        refframeworkLayout.setSpacing(false);
        refframeworkLayout.setSizeFull();
        
        HorizontalLayout fromRef = new HorizontalLayout();
        fromRef.setMargin(false);
        fromRef.setSpacing(false);
        fromRef.setSizeFull();
        fromRef.addComponent(labelFrom);
        fromRef.addComponent(fromFrameworksCombo);
        
        refframeworkLayout.addComponent(fromRef);
        
        HorizontalLayout toRef = new HorizontalLayout();
        toRef.setMargin(false);
        toRef.setSpacing(false);
        toRef.setSizeFull();
        toRef.addComponent(labelTo);
        toRef.addComponent(toFrameworksCombo);
        toRef.setStyleName("baselineFramework");
        
        refframeworkLayout.addComponent(toRef);
        refframeworkLayout.setStyleName("baselineFramework");
        
        horizontalPanel.addComponent(refframeworkLayout);
        horizontalPanel.setStyleName("baselineFramework");
        final Component b = getExportToDocxComponent();
        if (b != null) {
        	horizontalPanel.addComponent(b);
        }
        
        return horizontalPanel;
    }

    private List<FrameworkWrapper> getRefframeworksfromDB() {


    	RefframeworkManager refframeworkManager = (RefframeworkManager)SpringContextHelper.getBeanFromWebappContext(RefframeworkManager.SPRING_NAME);
        List<RefFramework> allFrameworks = refframeworkManager.getAllRefframeworks();
        
        List<FrameworkWrapper> allRefframeworksWrapped = new ArrayList<FrameworkWrapper>();
        
        if(!allFrameworks.isEmpty()){
        	for(RefFramework refFramework : allFrameworks){
        		if(refFramework.getName() != null && !refFramework.getName().equals("")){
        			allRefframeworksWrapped.add(new FrameworkWrapper(CDOStorageUtil.getCDOId(refFramework),refFramework.getName()));
        		}
        	}
        }
        
        else allRefframeworksWrapped=null;
        
        return allRefframeworksWrapped;
	
	}

	private void setFrameworksComboBoxes(List<FrameworkWrapper> frameworks)
    {
        frameworksContainer.removeAllItems();
        frameworksContainer.addAll(frameworks);
        final Collection<?> itemsFrom = fromFrameworksCombo.getItemIds();
        final Collection<?> itemsTo = toFrameworksCombo.getItemIds();
        
        if (itemsFrom != null && itemsFrom.size() > 0) {
        	fromFrameworksCombo.select(itemsFrom.iterator().next());
        	fromFrameworksCombo.setEnabled(true);
        	fromFrameworksCombo.setImmediate(true);
        } else {
        	fromFrameworksCombo.select(null);
        	fromFrameworksCombo.setEnabled(false);
        	fromFrameworksCombo.setImmediate(true);
        }
        
        if (itemsTo != null && itemsTo.size() > 0) {
        	toFrameworksCombo.select(itemsTo.iterator().next());
        	toFrameworksCombo.setEnabled(true);
        	toFrameworksCombo.setImmediate(true);
        } else {
        	toFrameworksCombo.select(null);
        	toFrameworksCombo.setEnabled(false);
        	toFrameworksCombo.setImmediate(true);
        }
        // force refresh! - ugly fix - the framework combo listener is NOT
        // always invoked by the above .select()!!! it is not invoked when flow
        // goes from report Menu selection!
        refreshEquivalenceMeticsBasedOnFrameworkCombo(true);
    }


    protected void refreshEquivalenceMeticsBasedOnFrameworkCombo(boolean from)
    {
        final FrameworkWrapper newFromRefframework = (FrameworkWrapper) fromFrameworksCombo.getValue();
        final FrameworkWrapper newToRefframework = (FrameworkWrapper) toFrameworksCombo.getValue();
        
        if (newFromRefframework != null && newToRefframework != null) {
            notifyRefframeworksChangeListeners(newFromRefframework.getId(), newToRefframework.getId(),from);
        } else {
        	notifyRefframeworksChangeListeners(new Long(-1), new Long(-1),from);
        }
        
    }  
    

    
    private void notifyRefframeworksChangeListeners(Long fromReffID, Long toRefFramework, boolean from) 
    {
    	if (fromReffID == null || toRefFramework == null) {
    		throw new IllegalArgumentException();
    	}
    	if (_toReffChangeListeners == null || _fromReffChangeListeners == null) {
    		return;
    	}
    	
    	if(from) _fromReffChangeListeners.forEach((l) -> l.setEquivalenceFrameworks(fromReffID.longValue(), toRefFramework));
    	else _toReffChangeListeners.forEach((l) -> l.setEquivalenceFrameworks(fromReffID.longValue(), toRefFramework)); 
	}
    

	protected void addFromRefframeworkChangeListener(IRefFrameworksChangeListener refFrameworkChangeListener)
    {
        if (_fromReffChangeListeners == null) {
        	_fromReffChangeListeners = new LinkedList<IRefFrameworksChangeListener>();
        }
        _fromReffChangeListeners.add(refFrameworkChangeListener);
    }  
	
	protected void addToRefframeworkChangeListener(IRefFrameworksChangeListener refFrameworkChangeListener)
    {
        if (_toReffChangeListeners == null) {
        	_toReffChangeListeners = new LinkedList<IRefFrameworksChangeListener>();
        }
        _toReffChangeListeners.add(refFrameworkChangeListener);
    }  
	
	protected void wakeUp(){
		refreshEquivalenceMeticsBasedOnFrameworkCombo(true);
	}
	
}
