/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
// Generated from Label.g4 by ANTLR 4.1

package org.eclipse.opencert.userguidance.labelparser.generated;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LabelParser}.
 */
public interface LabelListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link LabelParser#itembody}.
	 * @param ctx the parse tree
	 */
	void enterItembody(@NotNull LabelParser.ItembodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link LabelParser#itembody}.
	 * @param ctx the parse tree
	 */
	void exitItembody(@NotNull LabelParser.ItembodyContext ctx);

	/**
	 * Enter a parse tree produced by {@link LabelParser#text}.
	 * @param ctx the parse tree
	 */
	void enterText(@NotNull LabelParser.TextContext ctx);
	/**
	 * Exit a parse tree produced by {@link LabelParser#text}.
	 * @param ctx the parse tree
	 */
	void exitText(@NotNull LabelParser.TextContext ctx);

	/**
	 * Enter a parse tree produced by {@link LabelParser#item}.
	 * @param ctx the parse tree
	 */
	void enterItem(@NotNull LabelParser.ItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link LabelParser#item}.
	 * @param ctx the parse tree
	 */
	void exitItem(@NotNull LabelParser.ItemContext ctx);
}