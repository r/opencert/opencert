/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;

import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;

// End of user code

/**
 * 
 * 
 */
public class ArtefactPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ArtefactPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected EObjectFlatComboViewer precedentVersion;
	protected ReferencesTable ownedRel;
	protected List<ViewerFilter> ownedRelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRelFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable artefactPart;
	protected List<ViewerFilter> artefactPartBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> artefactPartFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArtefactPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence artefactStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = artefactStep.addStep(EvidenceViewsRepository.Artefact.Properties.class);
		propertiesStep.addStep(EvidenceViewsRepository.Artefact.Properties.id);
		propertiesStep.addStep(EvidenceViewsRepository.Artefact.Properties.name);
		propertiesStep.addStep(EvidenceViewsRepository.Artefact.Properties.description);
		propertiesStep.addStep(EvidenceViewsRepository.Artefact.Properties.precedentVersion);
		propertiesStep.addStep(EvidenceViewsRepository.Artefact.Properties.ownedRel);
		propertiesStep.addStep(EvidenceViewsRepository.Artefact.Properties.artefactPart);
		
		
		composer = new PartComposer(artefactStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.Artefact.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == EvidenceViewsRepository.Artefact.Properties.id) {
					return createIdText(parent);
				}
				if (key == EvidenceViewsRepository.Artefact.Properties.name) {
					return createNameText(parent);
				}
				if (key == EvidenceViewsRepository.Artefact.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == EvidenceViewsRepository.Artefact.Properties.precedentVersion) {
					return createPrecedentVersionFlatComboViewer(parent);
				}
				if (key == EvidenceViewsRepository.Artefact.Properties.ownedRel) {
					return createOwnedRelAdvancedTableComposition(parent);
				}
				if (key == EvidenceViewsRepository.Artefact.Properties.artefactPart) {
					return createArtefactPartAdvancedTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(EvidenceMessages.ArtefactPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.Artefact.Properties.id, EvidenceMessages.ArtefactPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, EvidenceViewsRepository.Artefact.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Artefact.Properties.id, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.Artefact.Properties.name, EvidenceMessages.ArtefactPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, EvidenceViewsRepository.Artefact.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Artefact.Properties.name, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.Artefact.Properties.description, EvidenceMessages.ArtefactPropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, EvidenceViewsRepository.Artefact.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Artefact.Properties.description, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createPrecedentVersionFlatComboViewer(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.Artefact.Properties.precedentVersion, EvidenceMessages.ArtefactPropertiesEditionPart_PrecedentVersionLabel);
		precedentVersion = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(EvidenceViewsRepository.Artefact.Properties.precedentVersion, EvidenceViewsRepository.SWT_KIND));
		precedentVersion.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		precedentVersion.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.precedentVersion, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getPrecedentVersion()));
			}

		});
		GridData precedentVersionData = new GridData(GridData.FILL_HORIZONTAL);
		precedentVersion.setLayoutData(precedentVersionData);
		precedentVersion.setID(EvidenceViewsRepository.Artefact.Properties.precedentVersion);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Artefact.Properties.precedentVersion, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createPrecedentVersionFlatComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRelAdvancedTableComposition(Composite parent) {
		this.ownedRel = new ReferencesTable(getDescription(EvidenceViewsRepository.Artefact.Properties.ownedRel, EvidenceMessages.ArtefactPropertiesEditionPart_OwnedRelLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRel.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRelFilters) {
			this.ownedRel.addFilter(filter);
		}
		this.ownedRel.setHelpText(propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Artefact.Properties.ownedRel, EvidenceViewsRepository.SWT_KIND));
		this.ownedRel.createControls(parent);
		this.ownedRel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.ownedRel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRelData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRelData.horizontalSpan = 3;
		this.ownedRel.setLayoutData(ownedRelData);
		this.ownedRel.setLowerBound(0);
		this.ownedRel.setUpperBound(-1);
		ownedRel.setID(EvidenceViewsRepository.Artefact.Properties.ownedRel);
		ownedRel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRelAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactPartAdvancedTableComposition(Composite parent) {
		this.artefactPart = new ReferencesTable(getDescription(EvidenceViewsRepository.Artefact.Properties.artefactPart, EvidenceMessages.ArtefactPropertiesEditionPart_ArtefactPartLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.artefactPart, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				artefactPart.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.artefactPart, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				artefactPart.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.artefactPart, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				artefactPart.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.artefactPart, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				artefactPart.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.artefactPartFilters) {
			this.artefactPart.addFilter(filter);
		}
		this.artefactPart.setHelpText(propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Artefact.Properties.artefactPart, EvidenceViewsRepository.SWT_KIND));
		this.artefactPart.createControls(parent);
		this.artefactPart.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.Artefact.Properties.artefactPart, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData artefactPartData = new GridData(GridData.FILL_HORIZONTAL);
		artefactPartData.horizontalSpan = 3;
		this.artefactPart.setLayoutData(artefactPartData);
		this.artefactPart.setLowerBound(0);
		this.artefactPart.setUpperBound(-1);
		artefactPart.setID(EvidenceViewsRepository.Artefact.Properties.artefactPart);
		artefactPart.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createArtefactPartAdvancedTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Artefact.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(EvidenceMessages.Artefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Artefact.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(EvidenceMessages.Artefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Artefact.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(EvidenceMessages.Artefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#getPrecedentVersion()
	 * 
	 */
	public EObject getPrecedentVersion() {
		if (precedentVersion.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) precedentVersion.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#initPrecedentVersion(EObjectFlatComboSettings)
	 */
	public void initPrecedentVersion(EObjectFlatComboSettings settings) {
		precedentVersion.setInput(settings);
		if (current != null) {
			precedentVersion.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Artefact.Properties.precedentVersion);
		if (eefElementEditorReadOnlyState && precedentVersion.isEnabled()) {
			precedentVersion.setEnabled(false);
			precedentVersion.setToolTipText(EvidenceMessages.Artefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !precedentVersion.isEnabled()) {
			precedentVersion.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#setPrecedentVersion(EObject newValue)
	 * 
	 */
	public void setPrecedentVersion(EObject newValue) {
		if (newValue != null) {
			precedentVersion.setSelection(new StructuredSelection(newValue));
		} else {
			precedentVersion.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Artefact.Properties.precedentVersion);
		if (eefElementEditorReadOnlyState && precedentVersion.isEnabled()) {
			precedentVersion.setEnabled(false);
			precedentVersion.setToolTipText(EvidenceMessages.Artefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !precedentVersion.isEnabled()) {
			precedentVersion.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#setPrecedentVersionButtonMode(ButtonsModeEnum newValue)
	 */
	public void setPrecedentVersionButtonMode(ButtonsModeEnum newValue) {
		precedentVersion.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#addFilterPrecedentVersion(ViewerFilter filter)
	 * 
	 */
	public void addFilterToPrecedentVersion(ViewerFilter filter) {
		precedentVersion.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#addBusinessFilterPrecedentVersion(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToPrecedentVersion(ViewerFilter filter) {
		precedentVersion.addBusinessRuleFilter(filter);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#initOwnedRel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRel.setContentProvider(contentProvider);
		ownedRel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Artefact.Properties.ownedRel);
		if (eefElementEditorReadOnlyState && ownedRel.isEnabled()) {
			ownedRel.setEnabled(false);
			ownedRel.setToolTipText(EvidenceMessages.Artefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRel.isEnabled()) {
			ownedRel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#updateOwnedRel()
	 * 
	 */
	public void updateOwnedRel() {
	ownedRel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#addFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRel(ViewerFilter filter) {
		ownedRelFilters.add(filter);
		if (this.ownedRel != null) {
			this.ownedRel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#addBusinessFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRel(ViewerFilter filter) {
		ownedRelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#isContainedInOwnedRelTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRelTable(EObject element) {
		return ((ReferencesTableSettings)ownedRel.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#initArtefactPart(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initArtefactPart(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		artefactPart.setContentProvider(contentProvider);
		artefactPart.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Artefact.Properties.artefactPart);
		if (eefElementEditorReadOnlyState && artefactPart.isEnabled()) {
			artefactPart.setEnabled(false);
			artefactPart.setToolTipText(EvidenceMessages.Artefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !artefactPart.isEnabled()) {
			artefactPart.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#updateArtefactPart()
	 * 
	 */
	public void updateArtefactPart() {
	artefactPart.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#addFilterArtefactPart(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArtefactPart(ViewerFilter filter) {
		artefactPartFilters.add(filter);
		if (this.artefactPart != null) {
			this.artefactPart.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#addBusinessFilterArtefactPart(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArtefactPart(ViewerFilter filter) {
		artefactPartBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart#isContainedInArtefactPartTable(EObject element)
	 * 
	 */
	public boolean isContainedInArtefactPartTable(EObject element) {
		return ((ReferencesTableSettings)artefactPart.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.Artefact_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
