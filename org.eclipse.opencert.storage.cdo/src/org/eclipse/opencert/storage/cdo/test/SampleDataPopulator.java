/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.test;

import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.storage.cdo.StandaloneCDOAccessor;
import org.eclipse.opencert.storage.cdo.property.OpencertPropertiesReader;

public class SampleDataPopulator
{
    public static void main(String[] args)
    {
        SampleDataPopulator sampleDataPopulator = new SampleDataPopulator();
        sampleDataPopulator.populateWithArtefactHierarchy();
        
        sampleDataPopulator.populateMandatoryHierarchiesForEmptyDatabase();
        
    }
    
    public void populateWithArtefactHierarchy()
    {
        StandaloneCDOAccessor standaloneCDOAccessor = StandaloneCDOAccessor.getStandaloneCDOAccessor(OpencertPropertiesReader.getInstance());
        CDOSession cdoSession = null;
        try {
            cdoSession = standaloneCDOAccessor.openSession();
            CDOTransaction transaction = cdoSession.openTransaction();
            Resource resource = StandaloneCDOAccessor.getDefaultResource(transaction);
            
            SampleDataProvider.populateWithArtefactHierarchy(resource);
            
            transaction.setCommitComment("populateWithArtefactHierarchy()");
            transaction.commit();
            
            transaction.getSession().close();
            
        } catch (CommitException e) {
            e.printStackTrace();
        } finally {
            cdoSession.close();
        }
    }
    
    
    public void populateMandatoryHierarchiesForEmptyDatabase()
    {
        StandaloneCDOAccessor standaloneCDOAccessor = StandaloneCDOAccessor.getStandaloneCDOAccessor(OpencertPropertiesReader.getInstance());
        CDOSession cdoSession = null;
        try {
            cdoSession = standaloneCDOAccessor.openSession();
            CDOTransaction transaction = cdoSession.openTransaction();
            Resource resource = StandaloneCDOAccessor.getDefaultResource(transaction);
            
            SampleDataProvider.populateArg(resource);
            SampleDataProvider.populateAssuranceProject(resource);
            
            transaction.setCommitComment("populateAssuranceProject");
            transaction.commit();
            
            transaction.getSession().close();
            
        } catch (CommitException e) {
            e.printStackTrace();
        } finally {
            cdoSession.close();
        }
    }
    
    
    public void updateArtefact()
    {
        StandaloneCDOAccessor standaloneCDOAccessor = StandaloneCDOAccessor.getStandaloneCDOAccessor(OpencertPropertiesReader.getInstance());
        CDOSession cdoSession = null;
        try {
            cdoSession = standaloneCDOAccessor.openSession();
            CDOTransaction transaction = cdoSession.openTransaction();
            
            CDOQuery cqo = transaction
                    .createQuery("sql",
                            "select * from evidence_Artefact where id ilike '0'");
            
            Artefact artefact = cqo.getResultValue(Artefact.class);
            
            artefact.setName(artefact.getName() + "_updated_" + 2);
            
            transaction.setCommitComment("Changing name of artefact");
            
            transaction.commit();
        } catch (CommitException e) {
            e.printStackTrace();
        } finally {
            cdoSession.close();
        }
    }
}
