/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.properties.property.parts;

// Start of user code for imports
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.Enumerator;


// End of user code

/**
 * 
 * 
 */
public interface PropertyPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the datatype
	 * 
	 */
	public Enumerator getDatatype();

	/**
	 * Init the datatype
	 * @param input the viewer input
	 * @param current the current value
	 */
	public void initDatatype(Object input, Enumerator current);

	/**
	 * Defines a new datatype
	 * @param newValue the new datatype to set
	 * 
	 */
	public void setDatatype(Enumerator newValue);


	/**
	 * @return the enumValues
	 * 
	 */
	public EList getEnumValues();

	/**
	 * Defines a new enumValues
	 * @param newValue the new enumValues to set
	 * 
	 */
	public void setEnumValues(EList newValue);

	/**
	 * Add a value to the enumValues multivalued attribute.
	 * @param newValue the value to add
	 */
	public void addToEnumValues(Object newValue);

	/**
	 * Remove a value to the enumValues multivalued attribute.
	 * @param newValue the value to remove
	 */
	public void removeToEnumValues(Object newValue);


	/**
	 * @return the unit
	 * 
	 */
	public String getUnit();

	/**
	 * Defines a new unit
	 * @param newValue the new unit to set
	 * 
	 */
	public void setUnit(String newValue);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
