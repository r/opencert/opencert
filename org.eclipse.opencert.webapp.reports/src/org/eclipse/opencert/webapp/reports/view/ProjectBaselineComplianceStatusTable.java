/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import org.eclipse.opencert.webapp.reports.containers.BaseAssetComplianceStatus;
import org.eclipse.opencert.webapp.reports.listeners.ExternalToolResultClickedListener;
import org.eclipse.opencert.webapp.reports.listeners.ExternalToolResultOrQueryAddedListener;
import org.eclipse.opencert.webapp.reports.listeners.UnassignOrAssignArtefactListener;
import org.eclipse.opencert.webapp.reports.listeners.util.ClickType;
import org.eclipse.opencert.webapp.reports.listeners.util.SelectionEvent;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceStatusDataRowProcessor;
import org.eclipse.opencert.webapp.reports.manager.compliance.IComplianceConsts;
import org.eclipse.opencert.webapp.reports.util.ComplianceStatusCalculator;
import org.eclipse.opencert.webapp.reports.util.ComplianceStatusType;
import org.eclipse.opencert.webapp.reports.util.ICommonCssStyles;
import org.eclipse.opencert.webapp.reports.view.common.AbstractProjectBaselineComplianceTable;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.url.URLParamHandler;
import org.eclipse.opencert.webapp.reports.view.url.URLParamsContainer;

import com.vaadin.data.Container.Hierarchical;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Link;
import com.vaadin.ui.Table;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;

public class ProjectBaselineComplianceStatusTable
        extends AbstractProjectBaselineComplianceTable
        implements ExternalToolResultOrQueryAddedListener, UnassignOrAssignArtefactListener, ExternalToolResultClickedListener
{
    private static final long serialVersionUID = 6915474633643585105L;

    private static final String COMPLIANCE_TABLE_STYLE = "complianceTable";
    private static final String TABLE_TITLE_LABEL = "Project Compliance";
    private static final String PROJECT_BASELINE_ELEMENT_NAME = "Baseline Element Name";

    private static final String TYPE_LABEL = "Type";
    private static final String STATUS_LABEL = "Compliance Status";
    private static final String EXTERNAL_TOOL_LABEL = "External Tools";
    private static final String IA_STATUS_LABEL = "IA Status";
    
    private static final String OVERALL_STATUS_OK_DESC = "All of your project baseline elements are <br>fully compliant with baseline framework.";
    private static final String OVERALL_STATUS_PARTLY_DESC = "At least one of your project baseline elements has<br> <b>Partial</b> Compliance Evidence provided.";
    private static final String OVERALL_STATUS_NO_DESC = "At least one of your project baseline elements has <br> <b>Not Compliant</b> Evidence provided.";
    private static final String OVERALL_STATUS_EMPTY_DESC = "At least one of your project baseline elements has<br> no Compliance Evidence provided yet.";
    
    private Label overallStatusPrefixLabel = new Label("");
    private Label overallStatusLabel = new Label("");

    public ProjectBaselineComplianceStatusTable(boolean isShowExternalToolsPanels, GUIMode guiMode, URLParamsContainer urlParamsContainer)
    {
        _complianceManager = new ComplianceManager<BaseAssetComplianceStatus>(new ComplianceStatusDataRowProcessor(isShowExternalToolsPanels, this));

        HorizontalLayout topInfoPanel = new HorizontalLayout();
        topInfoPanel.setWidth("100%");
        
        Component tableTitle;
        
        if (guiMode == GUIMode.NORMAL) 
        {
            tableTitle = new Label(TABLE_TITLE_LABEL);
            tableTitle.addStyleName("complianceTableTitle");
        } 
        else 
        {
            final String url = URLParamHandler.generateWebURL(urlParamsContainer, GUIMode.NORMAL);
            tableTitle = new Link(TABLE_TITLE_LABEL, new ExternalResource(url));
            ((Link)tableTitle).setTargetName("_blank");           
            tableTitle.addStyleName("complianceTableTitleCompact");
            
            urlParamsContainer.addURLParamsChangeListener(() ->
            {
                final String newURL = URLParamHandler.generateWebURL(urlParamsContainer, GUIMode.NORMAL);
                ((Link)tableTitle).setResource(new ExternalResource(newURL));
            });
        }
        topInfoPanel.addComponent(tableTitle);

        Layout overallStatusLabelLayout = createOverallStatusLabelLayout();

        topInfoPanel.addComponent(overallStatusLabelLayout);
        topInfoPanel.setComponentAlignment(overallStatusLabelLayout, Alignment.MIDDLE_RIGHT);

        Layout mainPanel = new VerticalLayout();
        mainPanel.addComponent(topInfoPanel);

        try {
            mainPanel.addComponent(initEmptyBaselineComplianceTable(isShowExternalToolsPanels));
        } catch (NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }

        setCompositionRoot(mainPanel);
        setSizeFull();
    }

    private Layout createOverallStatusLabelLayout()
    {
        HorizontalLayout overallStatusLabelLayout = new HorizontalLayout();

        overallStatusPrefixLabel = new Label("Overall compliance status: ");
        overallStatusPrefixLabel.setStyleName("overallStatusPrefixLabel");

        overallStatusLabelLayout.addComponent(overallStatusPrefixLabel);
        overallStatusLabelLayout.addComponent(overallStatusLabel);

        return overallStatusLabelLayout;
    }

    private void generateOverallStatusLabel()
    {
        if (projectBaselineComplianceTable.getContainerDataSource().size() == 0) {
            overallStatusLabel.setVisible(false);
            overallStatusPrefixLabel.setVisible(false);
            return;
        }

        overallStatusPrefixLabel.setVisible(true);
        overallStatusLabel.setVisible(true);

        ComplianceStatusCalculator complianceStatusCalculator = new ComplianceStatusCalculator();
        ComplianceStatusType overallStatusType = complianceStatusCalculator.calculateSummaryComplianceStatusForProject(projectBaselineComplianceTable
                .getContainerDataSource());

        overallStatusLabel.setValue(overallStatusType.getValue());

        if (overallStatusType.equals(ComplianceStatusType.OK)) {
            overallStatusLabel.setStyleName("overallStatusLabel_ok " + ICommonCssStyles.OVERALL_STATUS_COMMON_LABEL);
            overallStatusLabel.setDescription(OVERALL_STATUS_OK_DESC);
        } else if (overallStatusType.equals(ComplianceStatusType.PARTLY)) {
            overallStatusLabel.setStyleName("overallStatusLabel_partly " + ICommonCssStyles.OVERALL_STATUS_COMMON_LABEL);
            overallStatusLabel.setDescription(OVERALL_STATUS_PARTLY_DESC);
        } else if (overallStatusType.equals(ComplianceStatusType.NO)) {
            overallStatusLabel.setStyleName("overallStatusLabel_no " + ICommonCssStyles.OVERALL_STATUS_COMMON_LABEL);
            overallStatusLabel.setDescription(OVERALL_STATUS_NO_DESC);
        } else {
            overallStatusLabel.setStyleName("overallStatusLabel_empty " + ICommonCssStyles.OVERALL_STATUS_COMMON_LABEL);
            overallStatusLabel.setDescription(OVERALL_STATUS_EMPTY_DESC);
        }
    }

    // private Button createExecuteAllQueriesButton()
    // {
    // Button executeAllToolsButton = new Button("Execute All Queries");
    // executeAllToolsButton.addClickListener((e) -> {
    // Hierarchical container =
    // projectBaselineComplianceTable.getContainerDataSource();
    // ExternalToolInstanceExecutor executor = new
    // ExternalToolInstanceExecutor();
    // for (Object baseAssetComplianceStatus : container.getItemIds()) {
    // try {
    // executor.executeAndSaveExternalToolResult(((BaseAssetComplianceStatus)
    // baseAssetComplianceStatus).getItemId());
    // } catch (InstantiationException | IllegalAccessException |
    // ClassNotFoundException | IOException | DataAccessException ex) {
    // Notification.show("Problem with connection: " + ex.getMessage(),
    // Type.ERROR_MESSAGE);
    // ex.printStackTrace();
    // }
    // }
    // });
    //
    // return executeAllToolsButton;
    // }

    @Override
    public void setBaselineFramework(long baselineFrameworkID)
    {
        super.setBaselineFramework(baselineFrameworkID);
        
        notifyAllProjectBaselineComplianceTableListeners(new SelectionEvent(ClickType.UNSELECTED, projectBaselineComplianceTable.size()));

        generateOverallStatusLabel();
    }

    private Table initEmptyBaselineComplianceTable(boolean isShowExternalToolsPanels) throws NoSuchFieldException, SecurityException
    {
        projectBaselineComplianceTable = new TreeTable(null, _complianceManager.initEmptyTableDataContainer());

        projectBaselineComplianceTable.setColumnHeader(IComplianceConsts.TYPE_PROPERTY_ID, TYPE_LABEL);
        projectBaselineComplianceTable.setColumnHeader(IComplianceConsts.NAME_PROPERTY_ID, PROJECT_BASELINE_ELEMENT_NAME);
        projectBaselineComplianceTable.setColumnHeader(IComplianceConsts.STATUS_PROPERTY_ID, STATUS_LABEL);
        projectBaselineComplianceTable.setColumnHeader(IComplianceConsts.IA_STATUS_PROPERTY_ID, IA_STATUS_LABEL);

        projectBaselineComplianceTable.setColumnExpandRatio(IComplianceConsts.TYPE_PROPERTY_ID, 4);
        projectBaselineComplianceTable.setColumnExpandRatio(IComplianceConsts.NAME_PROPERTY_ID, 60);
        projectBaselineComplianceTable.setColumnExpandRatio(IComplianceConsts.STATUS_PROPERTY_ID, 18);
        projectBaselineComplianceTable.setColumnExpandRatio(IComplianceConsts.IA_STATUS_PROPERTY_ID, 7);

        if (isShowExternalToolsPanels) {
            projectBaselineComplianceTable.setColumnHeader(IComplianceConsts.EXTERNAL_TOOL_PROPERTY_ID, EXTERNAL_TOOL_LABEL);
            projectBaselineComplianceTable.setColumnExpandRatio(IComplianceConsts.EXTERNAL_TOOL_PROPERTY_ID, 18);
        }

        projectBaselineComplianceTable.addStyleName(COMPLIANCE_TABLE_STYLE);
        projectBaselineComplianceTable.setPageLength(0);
        projectBaselineComplianceTable.setBuffered(false);
        projectBaselineComplianceTable.setSelectable(true);
        projectBaselineComplianceTable.setImmediate(true);
        projectBaselineComplianceTable.setSizeFull();
        projectBaselineComplianceTable.setColumnExpandRatio(TYPE_LABEL, 1.0f);

        projectBaselineComplianceTable.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = 3366377116458737822L;

            private BaseAssetComplianceStatus currentProjectBaselineComplianceTableValue;

            private void prepareSelectionEventAndNotifyListeners(BaseAssetComplianceStatus baseAssetComplianceStatus)
            {
                long cdoItemId = baseAssetComplianceStatus.getItemId();

                SelectionEvent sEvent = new SelectionEvent(ClickType.SELECTED, cdoItemId, _complianceManager.findBaselineElementTypeById(cdoItemId),
                        baseAssetComplianceStatus.getName().getValue(), baseAssetComplianceStatus);
                notifyAllProjectBaselineComplianceTableListeners(sEvent);
            }

            public void valueChange(ValueChangeEvent event)
            {
                BaseAssetComplianceStatus newProjectBaselineComplianceStatusTableValue = ((BaseAssetComplianceStatus) projectBaselineComplianceTable.getValue());

                if (newProjectBaselineComplianceStatusTableValue != null) {
                    currentProjectBaselineComplianceTableValue = newProjectBaselineComplianceStatusTableValue;
                } else {
                    currentProjectBaselineComplianceTableValue.setExternalToolResultColumnClicked(false);
                    projectBaselineComplianceTable.setValue(currentProjectBaselineComplianceTableValue);
                }
                prepareSelectionEventAndNotifyListeners(currentProjectBaselineComplianceTableValue);
            }
        });

        return projectBaselineComplianceTable;
    }

    @Override
    public void resultOrQueryAdded(long cdoId)
    {
        BaseAssetComplianceStatus baseAssetComplianceStatus = findBaseAssetComplianceStatusById(cdoId);

        if (baseAssetComplianceStatus != null) {
            baseAssetComplianceStatus.setExternalToolResultColumnClicked(true); //after refreshing the table this value is false (default value)
            prepareSelectionEventAndNotifyListeners(cdoId, baseAssetComplianceStatus);
        }
    }

    @Override
    public void artefactAssignedOrUnassigned(long cdoId)
    {
        BaseAssetComplianceStatus baseAssetComplianceStatus = findBaseAssetComplianceStatusById(cdoId);

        if (baseAssetComplianceStatus != null) {
            prepareSelectionEventAndNotifyListeners(cdoId, baseAssetComplianceStatus);
        }
    }

    @Override
    public void externalResultClicked(long cdoId)
    {
        artefactAssignedOrUnassigned(cdoId);
    }

    private BaseAssetComplianceStatus findBaseAssetComplianceStatusById(long cdoId)
    {
        Hierarchical container = projectBaselineComplianceTable.getContainerDataSource();

        for (Object baseAssetComplianceStatus : container.getItemIds()) {
            if (((BaseAssetComplianceStatus) baseAssetComplianceStatus).getItemId() == cdoId) {
                return (BaseAssetComplianceStatus) baseAssetComplianceStatus;
            }
        }
        return null;
    }

    private void prepareSelectionEventAndNotifyListeners(long cdoItemId, BaseAssetComplianceStatus baseAssetComplianceStatus)
    {
        SelectionEvent sEvent = new SelectionEvent(ClickType.SELECTED, cdoItemId, _complianceManager.findBaselineElementTypeById(cdoItemId),
                baseAssetComplianceStatus.getName().getValue(), baseAssetComplianceStatus);

        notifyAllProjectBaselineComplianceTableListeners(sEvent);

        projectBaselineComplianceTable.setValue(baseAssetComplianceStatus);
    }
}
