/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.messages;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.Destination;

import org.springframework.jms.core.JmsTemplate;

public class OpencertPersistenceMessagesProducer
{
    private JmsTemplate jmsTemplate;
    private Destination allEntitiesEventsDestination;
    private Map<String, Destination> perEntityTypeEventsDestinations = new HashMap<String, Destination>();
    
    public OpencertPersistenceMessagesProducer(JmsTemplate jmsTemplate, Destination allEntitiesEventsDestination)
    {
        this.jmsTemplate = jmsTemplate;
        this.allEntitiesEventsDestination = allEntitiesEventsDestination;
    }
    
    public void setPerEntityTypeEventsDestinationsList(List<Destination> destinations)
    {
        for (Destination destination : destinations) {
            
            String key = destination.toString();
            String entityOnlyKey = key.replaceAll("topic://opencert.persistence.", "");
            
            perEntityTypeEventsDestinations.put(entityOnlyKey, destination);
        }
    }
    
    
    public void send(PersistenceEventMessage entityPersistenceEventMessage)
    {
        Destination targetDestination = perEntityTypeEventsDestinations.get(entityPersistenceEventMessage.getEntityType());
        if (targetDestination != null) {
            jmsTemplate.send(targetDestination, new SimpleMessageCreator(entityPersistenceEventMessage));
        }
        
        jmsTemplate.send(allEntitiesEventsDestination, new SimpleMessageCreator(entityPersistenceEventMessage));
    }
}
