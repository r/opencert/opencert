/*******************************************************************************
 * Copyright (c) 2017 Honeywell, Inc.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Vit Koksa  <Vit.Koksa@honeywell.com>
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.vavmanager;

public class Request {
	String method;
	String URL;
	String type;
	//String protocol;
	String content;
	String id = "";
	
	static Request example;
	static Request monitor;
	
	public static Request setExample(String fileIdLtl, String fileIdIO, int numberOfReqs) {
		example = new Request();
		example.method = "POST";
		//example.URL = "http://158.138.138.152:6000/";
		example.URL = "http://147.251.51.238:8080";		// pleiada pleiada01.fi.muni.cz
		//example.URL = "http://138.90.228.243:6080";		// Rak
		//example.URL = "http://158.138.138.152:6080";
		example.type = "verify";
		//example.protocol = "HTTP/1.1";
		//example.protocol = "HTTP/1.0";
		String reqNumbers = "";
		StringBuffer sBuf = new StringBuffer();
		for (int i = 0; i < numberOfReqs; i++) {
			sBuf.append(Integer.toString(i) + ",");
		}
		reqNumbers = sBuf.toString();
		String systempathName = "";
		example.content = 
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				+ "<rdf:RDF\n"
				+ "   xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
				+ "   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
				+ "   xmlns:oslc=\"http://open-services.net/ns/core#\"\n"
				+ "   xmlns:oslc_auto=\"http://open-services.net/ns/auto#\">\n\n"
				+ "<oslc_auto:AutomationPlan rdf:about=\"http://honeywell.com/autoplans/blockName0_1_2_3_\">\n"
				+ "   <dcterms:title>Requirement Semantic Analysis</dcterms:title>\n"
                + "   <dcterms:identifier>" + "requirementDocumentName" + "</dcterms:identifier>\n"
                + "   <dcterms:description>" + systempathName + "</dcterms:description>\n"
				+ "   <oslc_auto:usesExecutionEnvironment rdf:resource=\"http://honeywell.com/tools/looney\"/>\n"
                + "   <dcterms:creator rdf:resource=\"ForReq - GLOBAL\\E334628\" />\n"
				+ "   <dcterms:created>2/13/2018 3:17:47 PM</dcterms:created>\n"
                + "</oslc_auto:AutomationPlan>\n\n"
                + "<oslc_auto:AutomationRequest>\n"
                + "   <dcterms:title>Requirement Semantic Analysis</dcterms:title>\n"
                + "   <dcterms:identifier>C:\\ZTEMP\\ForMeth\\honeywell_sw\\ForReq\\Examples\\Gesture_Recognition_Requirements-minimum.docx</dcterms:identifier>\n"
                + "   <dcterms:description></dcterms:description>\n"
                + "   <oslc_auto:inputParameter><oslc_auto:ParameterInstance>\n"
                + "      <oslc:name>CallSchema</oslc:name>\n"
                + "      <rdf:value rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">i0,i1,p0</rdf:value>\n"
                + "   </oslc_auto:ParameterInstance></oslc_auto:inputParameter>\n"
                + "   <oslc_auto:inputParameter><oslc_auto:ParameterInstance>\n"
                + "      <oslc:name>CallParameters</oslc:name>\n"
                + "      <rdf:value rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">" + reqNumbers + "</rdf:value>\n"
                + "   </oslc_auto:ParameterInstance></oslc_auto:inputParameter>\n"
				//+ "<oslc_auto:parameterDefinition rdf:ID=\"link1\" rdf:resource=\"" + reqNumbers + "\"/>\n"
				//+ "<oslc_auto:parameterDefinition rdf:ID=\"link1\" rdf:resource=\"0,1,3,4,5,2,\"/>"
				+ "   <oslc_auto:inputParameter><oslc_auto:ParameterInstance>\n"
				+ "      <oslc:name>InputFiles</oslc:name><rdf:value rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">" + fileIdLtl + "</rdf:value>\n"
				+ "   </oslc_auto:ParameterInstance></oslc_auto:inputParameter>\n"
				//+ "<oslc_auto:inputParameter rdf:ID=\"link1\" rdf:resource=\"" + fileIdLtl + "\"/>\n"
				+ "   <oslc_auto:inputParameter><oslc_auto:ParameterInstance>\n"
				+ "      <oslc:name>InputFiles</oslc:name><rdf:value rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">" + fileIdIO + "</rdf:value>\n"
				+ "   </oslc_auto:ParameterInstance></oslc_auto:inputParameter>\n"
				// + "<oslc_auto:inputParameter rdf:ID=\"link1\" rdf:resource=\"" + fileIdIO + "\"/>\n"
				//+ "<oslc_auto:callSchema rdf:ID=\"link1\" rdf:resource=\"i0,i1,p0\"/>\n"
				+ "   <oslc_auto:state rdf:resource=\"http://open-services.net/ns/auto#new\"/>\n"
				+ "   <oslc_auto:executesAutomationPlan rdf:resource=\"http://honeywell.com/autoplans/blockName0_1_2_3_\"/>\n"
				+ "</oslc_auto:AutomationRequest>\n"
				+ "</rdf:RDF>";
		/*
		example.content = //"--6675fade-a79f-4756-b292-f2ca3f5e7530 "
				//+ 
				"<rdf:RDF "
				+ "xmlns:dcterms=\"http://purl.org/dc/terms/\" "
				+ "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" "
				+ "xmlns:oslc_auto=\"http://open-services.net/ns/auto#\">"
				+ "<oslc_auto:AutomationPlan rdf:about=\"http://example.com/results/4321\">"
				+ "<oslc_auto:toolName rdf:ID=\"link1\" rdf:resource=\"looney\"/>"
				+ "<oslc_auto:parameterDefinition rdf:ID=\"link1\" rdf:resource=\"" + reqNumbers + "\"/>"
				//+ "<oslc_auto:parameterDefinition rdf:ID=\"link1\" rdf:resource=\"0,1,3,4,5,2,\"/>"
				+ "<oslc_auto:inputParameter rdf:ID=\"link1\" rdf:resource=\"" + fileIdLtl + "\"/>"
				+ "<oslc_auto:inputParameter rdf:ID=\"link1\" rdf:resource=\"" + fileIdIO + "\"/>"
				+ "<oslc_auto:callSchema rdf:ID=\"link1\" rdf:resource=\"i0,i1,p0\"/>"
				+ "</oslc_auto:AutomationPlan></rdf:RDF>";
				//+ "--6675fade-a79f-4756-b292-f2ca3f5e7530--";
		*/
		return example;
	}

	public static Request setMonitor(String jobId) {
		monitor = new Request();
		monitor.method = "POST";
		//monitor.URL = "http://158.138.138.152:6000/";
		monitor.URL = "http://147.251.51.238:8080";		// pleiada pleiada01.fi.muni.cz
		//monitor.URL = "http://138.90.228.243:6080";		// Rak
		//monitor.URL = "http://158.138.138.152:6080";
		monitor.type = "monitor";
		//monitor.protocol = "HTTP/1.0";
		monitor.content = "";
		monitor.id = jobId;

		return monitor;
	}
}
