/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface ParticipantPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);




	/**
	 * Init the ownedArtefact
	 * @param settings settings for the ownedArtefact ReferencesTable 
	 */
	public void initOwnedArtefact(ReferencesTableSettings settings);

	/**
	 * Update the ownedArtefact
	 * @param newValue the ownedArtefact to update
	 * 
	 */
	public void updateOwnedArtefact();

	/**
	 * Adds the given filter to the ownedArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedArtefact(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedArtefact(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedArtefact table
	 * 
	 */
	public boolean isContainedInOwnedArtefactTable(EObject element);




	/**
	 * Init the triggeredAssetEvent
	 * @param settings settings for the triggeredAssetEvent ReferencesTable 
	 */
	public void initTriggeredAssetEvent(ReferencesTableSettings settings);

	/**
	 * Update the triggeredAssetEvent
	 * @param newValue the triggeredAssetEvent to update
	 * 
	 */
	public void updateTriggeredAssetEvent();

	/**
	 * Adds the given filter to the triggeredAssetEvent edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToTriggeredAssetEvent(ViewerFilter filter);

	/**
	 * Adds the given filter to the triggeredAssetEvent edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToTriggeredAssetEvent(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the triggeredAssetEvent table
	 * 
	 */
	public boolean isContainedInTriggeredAssetEventTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
