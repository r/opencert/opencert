/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.pkm.refframework.refframework.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String RefframeworkCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String RefframeworkCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String RefframeworkCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String RefframeworkCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String RefframeworkCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String RefframeworkDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String RefframeworkDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String RefframeworkDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String RefframeworkDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String RefframeworkDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String RefframeworkDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String RefframeworkDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String RefframeworkDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String RefframeworkDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String RefframeworkDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String RefframeworkDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String RefframeworkDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String RefframeworkNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String RefframeworkDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String RefframeworkDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String RefframeworkDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String RefframeworkElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Objects1Group_title;

	/**
	 * @generated
	 */
	public static String Connections2Group_title;

	/**
	 * @generated
	 */
	public static String RefActivity1CreationTool_title;

	/**
	 * @generated
	 */
	public static String RefActivity1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RefArtefact2CreationTool_title;

	/**
	 * @generated
	 */
	public static String RefArtefact2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RefRole3CreationTool_title;

	/**
	 * @generated
	 */
	public static String RefRole3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String PrecedingActivity1CreationTool_title;

	/**
	 * @generated
	 */
	public static String PrecedingActivity1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ProducedArtefact2CreationTool_title;

	/**
	 * @generated
	 */
	public static String ProducedArtefact2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RequiredArtefact3CreationTool_title;

	/**
	 * @generated
	 */
	public static String RequiredArtefact3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Role4CreationTool_title;

	/**
	 * @generated
	 */
	public static String Role4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RefActivityRefActivitySubActivityCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String RefActivityRefActivitySubActivityCompartment2EditPart_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefFramework_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivity_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivity_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefArtefact_2002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefRole_2003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivity_3001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivity_3001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivityRequiredArtefact_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivityRequiredArtefact_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivityProducedArtefact_4002_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivityProducedArtefact_4002_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivityPrecedingActivity_4003_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivityPrecedingActivity_4003_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivityRole_4004_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RefActivityRole_4004_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String RefframeworkModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String RefframeworkModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
