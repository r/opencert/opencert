/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pam.procspec.process;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.infra.general.general.GeneralPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.pam.procspec.process.ProcessFactory
 * @model kind="package"
 * @generated
 */
public interface ProcessPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "process";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://process/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "process";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ProcessPackage eINSTANCE = org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pam.procspec.process.impl.ProcessModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessModelImpl
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getProcessModel()
	 * @generated
	 */
	int PROCESS_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_MODEL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_MODEL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_MODEL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Owned Activity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_MODEL__OWNED_ACTIVITY = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Owned Participant</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_MODEL__OWNED_PARTICIPANT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Owned Technique</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_MODEL__OWNED_TECHNIQUE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_MODEL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_MODEL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl <em>Activity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getActivity()
	 * @generated
	 */
	int ACTIVITY = 1;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__EVALUATION = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__EVALUATION;

	/**
	 * The feature id for the '<em><b>Lifecycle Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__LIFECYCLE_EVENT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__ID = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__NAME = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__DESCRIPTION = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__START_TIME = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__END_TIME = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Sub Activity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__SUB_ACTIVITY = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Preceding Activity</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__PRECEDING_ACTIVITY = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Participant</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__PARTICIPANT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Technique</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__TECHNIQUE = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Owned Rel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__OWNED_REL = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Required Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__REQUIRED_ARTEFACT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Produced Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__PRODUCED_ARTEFACT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Asset Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__ASSET_EVENT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FEATURE_COUNT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 13;

	/**
	 * The number of operations of the '<em>Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_OPERATION_COUNT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pam.procspec.process.impl.ParticipantImpl <em>Participant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ParticipantImpl
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getParticipant()
	 * @generated
	 */
	int PARTICIPANT = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT__ID = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT__NAME = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT__DESCRIPTION = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Owned Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT__OWNED_ARTEFACT = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Triggered Asset Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT__TRIGGERED_ASSET_EVENT = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Participant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT_FEATURE_COUNT = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Participant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT_OPERATION_COUNT = AssuranceassetPackage.ASSURANCE_ASSET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pam.procspec.process.impl.TechniqueImpl <em>Technique</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pam.procspec.process.impl.TechniqueImpl
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getTechnique()
	 * @generated
	 */
	int TECHNIQUE = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNIQUE__ID = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNIQUE__NAME = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNIQUE__DESCRIPTION = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Created Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNIQUE__CREATED_ARTEFACT = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Technique</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNIQUE_FEATURE_COUNT = AssuranceassetPackage.ASSURANCE_ASSET_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Technique</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TECHNIQUE_OPERATION_COUNT = AssuranceassetPackage.ASSURANCE_ASSET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pam.procspec.process.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pam.procspec.process.impl.PersonImpl
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__ID = PARTICIPANT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__NAME = PARTICIPANT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__DESCRIPTION = PARTICIPANT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Owned Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__OWNED_ARTEFACT = PARTICIPANT__OWNED_ARTEFACT;

	/**
	 * The feature id for the '<em><b>Triggered Asset Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__TRIGGERED_ASSET_EVENT = PARTICIPANT__TRIGGERED_ASSET_EVENT;

	/**
	 * The feature id for the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__EMAIL = PARTICIPANT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Organization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__ORGANIZATION = PARTICIPANT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = PARTICIPANT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = PARTICIPANT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pam.procspec.process.impl.ToolImpl <em>Tool</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ToolImpl
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getTool()
	 * @generated
	 */
	int TOOL = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__ID = PARTICIPANT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__NAME = PARTICIPANT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__DESCRIPTION = PARTICIPANT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Owned Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__OWNED_ARTEFACT = PARTICIPANT__OWNED_ARTEFACT;

	/**
	 * The feature id for the '<em><b>Triggered Asset Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__TRIGGERED_ASSET_EVENT = PARTICIPANT__TRIGGERED_ASSET_EVENT;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__VERSION = PARTICIPANT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Tool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL_FEATURE_COUNT = PARTICIPANT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Tool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL_OPERATION_COUNT = PARTICIPANT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pam.procspec.process.impl.OrganizationImpl <em>Organization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pam.procspec.process.impl.OrganizationImpl
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getOrganization()
	 * @generated
	 */
	int ORGANIZATION = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__ID = PARTICIPANT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__NAME = PARTICIPANT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__DESCRIPTION = PARTICIPANT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Owned Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__OWNED_ARTEFACT = PARTICIPANT__OWNED_ARTEFACT;

	/**
	 * The feature id for the '<em><b>Triggered Asset Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__TRIGGERED_ASSET_EVENT = PARTICIPANT__TRIGGERED_ASSET_EVENT;

	/**
	 * The feature id for the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__ADDRESS = PARTICIPANT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sub Organization</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__SUB_ORGANIZATION = PARTICIPANT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Organization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_FEATURE_COUNT = PARTICIPANT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Organization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_OPERATION_COUNT = PARTICIPANT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityRelImpl <em>Activity Rel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ActivityRelImpl
	 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getActivityRel()
	 * @generated
	 */
	int ACTIVITY_REL = 7;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_REL__TARGET = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_REL__SOURCE = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_REL__TYPE = 2;

	/**
	 * The number of structural features of the '<em>Activity Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_REL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Activity Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_REL_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pam.procspec.process.ProcessModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessModel
	 * @generated
	 */
	EClass getProcessModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pam.procspec.process.ProcessModel#getOwnedActivity <em>Owned Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Activity</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessModel#getOwnedActivity()
	 * @see #getProcessModel()
	 * @generated
	 */
	EReference getProcessModel_OwnedActivity();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pam.procspec.process.ProcessModel#getOwnedParticipant <em>Owned Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Participant</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessModel#getOwnedParticipant()
	 * @see #getProcessModel()
	 * @generated
	 */
	EReference getProcessModel_OwnedParticipant();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pam.procspec.process.ProcessModel#getOwnedTechnique <em>Owned Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Technique</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessModel#getOwnedTechnique()
	 * @see #getProcessModel()
	 * @generated
	 */
	EReference getProcessModel_OwnedTechnique();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pam.procspec.process.Activity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activity</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity
	 * @generated
	 */
	EClass getActivity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pam.procspec.process.Activity#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity#getStartTime()
	 * @see #getActivity()
	 * @generated
	 */
	EAttribute getActivity_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pam.procspec.process.Activity#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity#getEndTime()
	 * @see #getActivity()
	 * @generated
	 */
	EAttribute getActivity_EndTime();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pam.procspec.process.Activity#getSubActivity <em>Sub Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Activity</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity#getSubActivity()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_SubActivity();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pam.procspec.process.Activity#getPrecedingActivity <em>Preceding Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Preceding Activity</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity#getPrecedingActivity()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_PrecedingActivity();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pam.procspec.process.Activity#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Participant</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity#getParticipant()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_Participant();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pam.procspec.process.Activity#getTechnique <em>Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Technique</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity#getTechnique()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_Technique();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pam.procspec.process.Activity#getOwnedRel <em>Owned Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Rel</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity#getOwnedRel()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_OwnedRel();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pam.procspec.process.Activity#getRequiredArtefact <em>Required Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Artefact</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity#getRequiredArtefact()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_RequiredArtefact();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pam.procspec.process.Activity#getProducedArtefact <em>Produced Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Produced Artefact</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity#getProducedArtefact()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_ProducedArtefact();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pam.procspec.process.Activity#getAssetEvent <em>Asset Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Asset Event</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Activity#getAssetEvent()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_AssetEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pam.procspec.process.Participant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Participant</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Participant
	 * @generated
	 */
	EClass getParticipant();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pam.procspec.process.Participant#getOwnedArtefact <em>Owned Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Owned Artefact</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Participant#getOwnedArtefact()
	 * @see #getParticipant()
	 * @generated
	 */
	EReference getParticipant_OwnedArtefact();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pam.procspec.process.Participant#getTriggeredAssetEvent <em>Triggered Asset Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Triggered Asset Event</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Participant#getTriggeredAssetEvent()
	 * @see #getParticipant()
	 * @generated
	 */
	EReference getParticipant_TriggeredAssetEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pam.procspec.process.Technique <em>Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Technique</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Technique
	 * @generated
	 */
	EClass getTechnique();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pam.procspec.process.Technique#getCreatedArtefact <em>Created Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Created Artefact</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Technique#getCreatedArtefact()
	 * @see #getTechnique()
	 * @generated
	 */
	EReference getTechnique_CreatedArtefact();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pam.procspec.process.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pam.procspec.process.Person#getEmail <em>Email</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Email</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Person#getEmail()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_Email();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pam.procspec.process.Person#getOrganization <em>Organization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Organization</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Person#getOrganization()
	 * @see #getPerson()
	 * @generated
	 */
	EReference getPerson_Organization();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pam.procspec.process.Tool <em>Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tool</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Tool
	 * @generated
	 */
	EClass getTool();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pam.procspec.process.Tool#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Tool#getVersion()
	 * @see #getTool()
	 * @generated
	 */
	EAttribute getTool_Version();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pam.procspec.process.Organization <em>Organization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organization</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Organization
	 * @generated
	 */
	EClass getOrganization();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pam.procspec.process.Organization#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Organization#getAddress()
	 * @see #getOrganization()
	 * @generated
	 */
	EAttribute getOrganization_Address();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pam.procspec.process.Organization#getSubOrganization <em>Sub Organization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Organization</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.Organization#getSubOrganization()
	 * @see #getOrganization()
	 * @generated
	 */
	EReference getOrganization_SubOrganization();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pam.procspec.process.ActivityRel <em>Activity Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activity Rel</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.ActivityRel
	 * @generated
	 */
	EClass getActivityRel();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pam.procspec.process.ActivityRel#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.ActivityRel#getTarget()
	 * @see #getActivityRel()
	 * @generated
	 */
	EReference getActivityRel_Target();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pam.procspec.process.ActivityRel#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.ActivityRel#getSource()
	 * @see #getActivityRel()
	 * @generated
	 */
	EReference getActivityRel_Source();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pam.procspec.process.ActivityRel#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.opencert.pam.procspec.process.ActivityRel#getType()
	 * @see #getActivityRel()
	 * @generated
	 */
	EAttribute getActivityRel_Type();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ProcessFactory getProcessFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pam.procspec.process.impl.ProcessModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessModelImpl
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getProcessModel()
		 * @generated
		 */
		EClass PROCESS_MODEL = eINSTANCE.getProcessModel();

		/**
		 * The meta object literal for the '<em><b>Owned Activity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_MODEL__OWNED_ACTIVITY = eINSTANCE.getProcessModel_OwnedActivity();

		/**
		 * The meta object literal for the '<em><b>Owned Participant</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_MODEL__OWNED_PARTICIPANT = eINSTANCE.getProcessModel_OwnedParticipant();

		/**
		 * The meta object literal for the '<em><b>Owned Technique</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_MODEL__OWNED_TECHNIQUE = eINSTANCE.getProcessModel_OwnedTechnique();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl <em>Activity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getActivity()
		 * @generated
		 */
		EClass ACTIVITY = eINSTANCE.getActivity();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY__START_TIME = eINSTANCE.getActivity_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY__END_TIME = eINSTANCE.getActivity_EndTime();

		/**
		 * The meta object literal for the '<em><b>Sub Activity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__SUB_ACTIVITY = eINSTANCE.getActivity_SubActivity();

		/**
		 * The meta object literal for the '<em><b>Preceding Activity</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__PRECEDING_ACTIVITY = eINSTANCE.getActivity_PrecedingActivity();

		/**
		 * The meta object literal for the '<em><b>Participant</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__PARTICIPANT = eINSTANCE.getActivity_Participant();

		/**
		 * The meta object literal for the '<em><b>Technique</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__TECHNIQUE = eINSTANCE.getActivity_Technique();

		/**
		 * The meta object literal for the '<em><b>Owned Rel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__OWNED_REL = eINSTANCE.getActivity_OwnedRel();

		/**
		 * The meta object literal for the '<em><b>Required Artefact</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__REQUIRED_ARTEFACT = eINSTANCE.getActivity_RequiredArtefact();

		/**
		 * The meta object literal for the '<em><b>Produced Artefact</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__PRODUCED_ARTEFACT = eINSTANCE.getActivity_ProducedArtefact();

		/**
		 * The meta object literal for the '<em><b>Asset Event</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__ASSET_EVENT = eINSTANCE.getActivity_AssetEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pam.procspec.process.impl.ParticipantImpl <em>Participant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ParticipantImpl
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getParticipant()
		 * @generated
		 */
		EClass PARTICIPANT = eINSTANCE.getParticipant();

		/**
		 * The meta object literal for the '<em><b>Owned Artefact</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARTICIPANT__OWNED_ARTEFACT = eINSTANCE.getParticipant_OwnedArtefact();

		/**
		 * The meta object literal for the '<em><b>Triggered Asset Event</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARTICIPANT__TRIGGERED_ASSET_EVENT = eINSTANCE.getParticipant_TriggeredAssetEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pam.procspec.process.impl.TechniqueImpl <em>Technique</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pam.procspec.process.impl.TechniqueImpl
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getTechnique()
		 * @generated
		 */
		EClass TECHNIQUE = eINSTANCE.getTechnique();

		/**
		 * The meta object literal for the '<em><b>Created Artefact</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TECHNIQUE__CREATED_ARTEFACT = eINSTANCE.getTechnique_CreatedArtefact();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pam.procspec.process.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pam.procspec.process.impl.PersonImpl
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>Email</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__EMAIL = eINSTANCE.getPerson_Email();

		/**
		 * The meta object literal for the '<em><b>Organization</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON__ORGANIZATION = eINSTANCE.getPerson_Organization();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pam.procspec.process.impl.ToolImpl <em>Tool</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ToolImpl
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getTool()
		 * @generated
		 */
		EClass TOOL = eINSTANCE.getTool();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOOL__VERSION = eINSTANCE.getTool_Version();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pam.procspec.process.impl.OrganizationImpl <em>Organization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pam.procspec.process.impl.OrganizationImpl
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getOrganization()
		 * @generated
		 */
		EClass ORGANIZATION = eINSTANCE.getOrganization();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORGANIZATION__ADDRESS = eINSTANCE.getOrganization_Address();

		/**
		 * The meta object literal for the '<em><b>Sub Organization</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION__SUB_ORGANIZATION = eINSTANCE.getOrganization_SubOrganization();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityRelImpl <em>Activity Rel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ActivityRelImpl
		 * @see org.eclipse.opencert.pam.procspec.process.impl.ProcessPackageImpl#getActivityRel()
		 * @generated
		 */
		EClass ACTIVITY_REL = eINSTANCE.getActivityRel();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_REL__TARGET = eINSTANCE.getActivityRel_Target();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_REL__SOURCE = eINSTANCE.getActivityRel_Source();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_REL__TYPE = eINSTANCE.getActivityRel_Type();

	}

} //ProcessPackage
