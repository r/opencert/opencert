/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.wizard.IWizard;



public class WizardEMFContext {

	private TransactionalEditingDomain editingDomain;

	private IWizard wizard = null;

	public WizardEMFContext(IWizard wizard) {
		this.wizard = wizard;
	}

	public WizardEMFContext(TransactionalEditingDomain editingDomain) {
		this.editingDomain = editingDomain;
	}

	public boolean executeCommand(Command command) {
		if (command != null && command.canExecute()) {
			getEditingDomain().getCommandStack().execute(command);
			return true;
		}
		else {
			return false;
		}
	}

	public final TransactionalEditingDomain getEditingDomain() {
		if (editingDomain == null) {
			editingDomain = createEditingDomain(wizard);
		}
		return editingDomain;
	}

	protected TransactionalEditingDomain createEditingDomain(IWizard wizard) {
		return EditingDomainFactory.createEditingDomain(wizard);
	}

}
