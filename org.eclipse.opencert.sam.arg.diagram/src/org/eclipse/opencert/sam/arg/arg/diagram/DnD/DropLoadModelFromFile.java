/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.DnD;


import org.eclipse.opencert.sam.arg.arg.ArgPackage;

import java.io.File;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

/**
 * 
 * @author M� Carmen Palacios
 */
public class DropLoadModelFromFile {
	public EList<EObject> load(String fileFullName) {
		EList<EObject> ret=null;
		
		// Create a resource set to hold the resources.
		//
		ResourceSet resourceSet = new ResourceSetImpl();
		
		// Register the appropriate resource factory to handle all file extensions.
		//
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
			(Resource.Factory.Registry.DEFAULT_EXTENSION, 
			 new XMIResourceFactoryImpl());
	
		// Register the package to ensure it is available during loading.
		//
		resourceSet.getPackageRegistry().put
			(ArgPackage.eNS_URI, 
			 ArgPackage.eINSTANCE);
    
		// Construct the URI for the instance file.
		// The argument is treated as a file path only if it denotes an existing file.
		// Otherwise, it's directly treated as a URL.
		//
		File file = new File(fileFullName);
		URI uri = file.isFile() ? URI.createFileURI(file.getAbsolutePath()): URI.createURI(fileFullName);

		try {
			// Demand load resource for this file.
			//
			Resource resource = resourceSet.getResource(uri, true);
			System.out.println("Loaded " + uri);

			// Validate the contents of the loaded resource.
			//
			/*
			for (EObject eObject : resource.getContents()) {
				Diagnostic diagnostic = Diagnostician.INSTANCE.validate(eObject);
				if (diagnostic.getSeverity() != Diagnostic.OK) {
					printDiagnostic(diagnostic, "");
				}
			}
			*/
			ret = resource.getContents();
		}
		catch (RuntimeException exception) {
			System.out.println("Problem loading " + uri);
			exception.printStackTrace();
		}

        return ret;
	}
	

}
