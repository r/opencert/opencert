/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;

import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;

// End of user code

/**
 * 
 * 
 */
public class RefArtefactPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, RefArtefactPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text reference;
	protected ReferencesTable constrainingRequirement;
	protected List<ViewerFilter> constrainingRequirementBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> constrainingRequirementFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable applicableTechnique;
	protected List<ViewerFilter> applicableTechniqueBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> applicableTechniqueFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedRel;
	protected List<ViewerFilter> ownedRelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRelFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable property;
	protected List<ViewerFilter> propertyBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> propertyFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public RefArtefactPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence refArtefactStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = refArtefactStep.addStep(RefframeworkViewsRepository.RefArtefact.Properties.class);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefact.Properties.id);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefact.Properties.name);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefact.Properties.description);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefact.Properties.reference);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefact.Properties.ownedRel);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefact.Properties.property);
		
		
		composer = new PartComposer(refArtefactStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.RefArtefact.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefact.Properties.id) {
					return createIdText(parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefact.Properties.name) {
					return createNameText(parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefact.Properties.description) {
					return createDescriptionTextarea(parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefact.Properties.reference) {
					return createReferenceText(parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement) {
					return createConstrainingRequirementAdvancedReferencesTable(parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique) {
					return createApplicableTechniqueAdvancedReferencesTable(parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefact.Properties.ownedRel) {
					return createOwnedRelAdvancedTableComposition(parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefact.Properties.property) {
					return createPropertyAdvancedReferencesTable(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(RefframeworkMessages.RefArtefactPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefact.Properties.id, RefframeworkMessages.RefArtefactPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, RefframeworkViewsRepository.RefArtefact.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefact.Properties.id, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefact.Properties.name, RefframeworkMessages.RefArtefactPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, RefframeworkViewsRepository.RefArtefact.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefact.Properties.name, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionTextarea(Composite parent) {
		Label descriptionLabel = createDescription(parent, RefframeworkViewsRepository.RefArtefact.Properties.description, RefframeworkMessages.RefArtefactPropertiesEditionPart_DescriptionLabel);
		GridData descriptionLabelData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionLabelData.horizontalSpan = 3;
		descriptionLabel.setLayoutData(descriptionLabelData);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionData.horizontalSpan = 2;
		descriptionData.heightHint = 80;
		descriptionData.widthHint = 200;
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		EditingUtils.setID(description, RefframeworkViewsRepository.RefArtefact.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Textarea"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefact.Properties.description, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createReferenceText(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefact.Properties.reference, RefframeworkMessages.RefArtefactPropertiesEditionPart_ReferenceLabel);
		reference = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData referenceData = new GridData(GridData.FILL_HORIZONTAL);
		reference.setLayoutData(referenceData);
		reference.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.reference, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, reference.getText()));
			}

		});
		reference.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.reference, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, reference.getText()));
				}
			}

		});
		EditingUtils.setID(reference, RefframeworkViewsRepository.RefArtefact.Properties.reference);
		EditingUtils.setEEFtype(reference, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefact.Properties.reference, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createReferenceText

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createConstrainingRequirementAdvancedReferencesTable(Composite parent) {
		String label = getDescription(RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement, RefframeworkMessages.RefArtefactPropertiesEditionPart_ConstrainingRequirementLabel);		 
		this.constrainingRequirement = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addConstrainingRequirement(); }
			public void handleEdit(EObject element) { editConstrainingRequirement(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveConstrainingRequirement(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromConstrainingRequirement(element); }
			public void navigateTo(EObject element) { }
		});
		this.constrainingRequirement.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement, RefframeworkViewsRepository.SWT_KIND));
		this.constrainingRequirement.createControls(parent);
		this.constrainingRequirement.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData constrainingRequirementData = new GridData(GridData.FILL_HORIZONTAL);
		constrainingRequirementData.horizontalSpan = 3;
		this.constrainingRequirement.setLayoutData(constrainingRequirementData);
		this.constrainingRequirement.disableMove();
		constrainingRequirement.setID(RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement);
		constrainingRequirement.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addConstrainingRequirement() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(constrainingRequirement.getInput(), constrainingRequirementFilters, constrainingRequirementBusinessFilters,
		"constrainingRequirement", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				constrainingRequirement.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveConstrainingRequirement(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		constrainingRequirement.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromConstrainingRequirement(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		constrainingRequirement.refresh();
	}

	/**
	 * 
	 */
	protected void editConstrainingRequirement(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				constrainingRequirement.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createApplicableTechniqueAdvancedReferencesTable(Composite parent) {
		String label = getDescription(RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique, RefframeworkMessages.RefArtefactPropertiesEditionPart_ApplicableTechniqueLabel);		 
		this.applicableTechnique = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addApplicableTechnique(); }
			public void handleEdit(EObject element) { editApplicableTechnique(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveApplicableTechnique(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromApplicableTechnique(element); }
			public void navigateTo(EObject element) { }
		});
		this.applicableTechnique.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique, RefframeworkViewsRepository.SWT_KIND));
		this.applicableTechnique.createControls(parent);
		this.applicableTechnique.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData applicableTechniqueData = new GridData(GridData.FILL_HORIZONTAL);
		applicableTechniqueData.horizontalSpan = 3;
		this.applicableTechnique.setLayoutData(applicableTechniqueData);
		this.applicableTechnique.disableMove();
		applicableTechnique.setID(RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique);
		applicableTechnique.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addApplicableTechnique() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(applicableTechnique.getInput(), applicableTechniqueFilters, applicableTechniqueBusinessFilters,
		"applicableTechnique", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				applicableTechnique.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveApplicableTechnique(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		applicableTechnique.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromApplicableTechnique(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		applicableTechnique.refresh();
	}

	/**
	 * 
	 */
	protected void editApplicableTechnique(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				applicableTechnique.refresh();
			}
		}
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRelAdvancedTableComposition(Composite parent) {
		this.ownedRel = new ReferencesTable(getDescription(RefframeworkViewsRepository.RefArtefact.Properties.ownedRel, RefframeworkMessages.RefArtefactPropertiesEditionPart_OwnedRelLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRel.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRelFilters) {
			this.ownedRel.addFilter(filter);
		}
		this.ownedRel.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefact.Properties.ownedRel, RefframeworkViewsRepository.SWT_KIND));
		this.ownedRel.createControls(parent);
		this.ownedRel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.ownedRel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRelData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRelData.horizontalSpan = 3;
		this.ownedRel.setLayoutData(ownedRelData);
		this.ownedRel.setLowerBound(0);
		this.ownedRel.setUpperBound(-1);
		ownedRel.setID(RefframeworkViewsRepository.RefArtefact.Properties.ownedRel);
		ownedRel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRelAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createPropertyAdvancedReferencesTable(Composite parent) {
		String label = getDescription(RefframeworkViewsRepository.RefArtefact.Properties.property, RefframeworkMessages.RefArtefactPropertiesEditionPart_PropertyLabel);		 
		this.property = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addProperty(); }
			public void handleEdit(EObject element) { editProperty(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveProperty(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromProperty(element); }
			public void navigateTo(EObject element) { }
		});
		this.property.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefact.Properties.property, RefframeworkViewsRepository.SWT_KIND));
		this.property.createControls(parent);
		this.property.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.property, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData propertyData = new GridData(GridData.FILL_HORIZONTAL);
		propertyData.horizontalSpan = 3;
		this.property.setLayoutData(propertyData);
		this.property.disableMove();
		property.setID(RefframeworkViewsRepository.RefArtefact.Properties.property);
		property.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addProperty() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(property.getInput(), propertyFilters, propertyBusinessFilters,
		"property", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.property,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				property.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveProperty(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.property, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		property.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromProperty(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefArtefact.Properties.property, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		property.refresh();
	}

	/**
	 * 
	 */
	protected void editProperty(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				property.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefact.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(RefframeworkMessages.RefArtefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefact.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(RefframeworkMessages.RefArtefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefact.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setBackground(description.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			description.setToolTipText(RefframeworkMessages.RefArtefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#getReference()
	 * 
	 */
	public String getReference() {
		return reference.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#setReference(String newValue)
	 * 
	 */
	public void setReference(String newValue) {
		if (newValue != null) {
			reference.setText(newValue);
		} else {
			reference.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefact.Properties.reference);
		if (eefElementEditorReadOnlyState && reference.isEnabled()) {
			reference.setEnabled(false);
			reference.setToolTipText(RefframeworkMessages.RefArtefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !reference.isEnabled()) {
			reference.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#initConstrainingRequirement(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initConstrainingRequirement(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		constrainingRequirement.setContentProvider(contentProvider);
		constrainingRequirement.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefact.Properties.constrainingRequirement);
		if (eefElementEditorReadOnlyState && constrainingRequirement.getTable().isEnabled()) {
			constrainingRequirement.setEnabled(false);
			constrainingRequirement.setToolTipText(RefframeworkMessages.RefArtefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !constrainingRequirement.getTable().isEnabled()) {
			constrainingRequirement.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#updateConstrainingRequirement()
	 * 
	 */
	public void updateConstrainingRequirement() {
	constrainingRequirement.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#addFilterConstrainingRequirement(ViewerFilter filter)
	 * 
	 */
	public void addFilterToConstrainingRequirement(ViewerFilter filter) {
		constrainingRequirementFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#addBusinessFilterConstrainingRequirement(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToConstrainingRequirement(ViewerFilter filter) {
		constrainingRequirementBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#isContainedInConstrainingRequirementTable(EObject element)
	 * 
	 */
	public boolean isContainedInConstrainingRequirementTable(EObject element) {
		return ((ReferencesTableSettings)constrainingRequirement.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#initApplicableTechnique(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initApplicableTechnique(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		applicableTechnique.setContentProvider(contentProvider);
		applicableTechnique.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefact.Properties.applicableTechnique);
		if (eefElementEditorReadOnlyState && applicableTechnique.getTable().isEnabled()) {
			applicableTechnique.setEnabled(false);
			applicableTechnique.setToolTipText(RefframeworkMessages.RefArtefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicableTechnique.getTable().isEnabled()) {
			applicableTechnique.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#updateApplicableTechnique()
	 * 
	 */
	public void updateApplicableTechnique() {
	applicableTechnique.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#addFilterApplicableTechnique(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicableTechnique(ViewerFilter filter) {
		applicableTechniqueFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#addBusinessFilterApplicableTechnique(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToApplicableTechnique(ViewerFilter filter) {
		applicableTechniqueBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#isContainedInApplicableTechniqueTable(EObject element)
	 * 
	 */
	public boolean isContainedInApplicableTechniqueTable(EObject element) {
		return ((ReferencesTableSettings)applicableTechnique.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#initOwnedRel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRel.setContentProvider(contentProvider);
		ownedRel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefact.Properties.ownedRel);
		if (eefElementEditorReadOnlyState && ownedRel.isEnabled()) {
			ownedRel.setEnabled(false);
			ownedRel.setToolTipText(RefframeworkMessages.RefArtefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRel.isEnabled()) {
			ownedRel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#updateOwnedRel()
	 * 
	 */
	public void updateOwnedRel() {
	ownedRel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#addFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRel(ViewerFilter filter) {
		ownedRelFilters.add(filter);
		if (this.ownedRel != null) {
			this.ownedRel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#addBusinessFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRel(ViewerFilter filter) {
		ownedRelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#isContainedInOwnedRelTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRelTable(EObject element) {
		return ((ReferencesTableSettings)ownedRel.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#initProperty(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initProperty(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		property.setContentProvider(contentProvider);
		property.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefact.Properties.property);
		if (eefElementEditorReadOnlyState && property.getTable().isEnabled()) {
			property.setEnabled(false);
			property.setToolTipText(RefframeworkMessages.RefArtefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !property.getTable().isEnabled()) {
			property.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#updateProperty()
	 * 
	 */
	public void updateProperty() {
	property.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#addFilterProperty(ViewerFilter filter)
	 * 
	 */
	public void addFilterToProperty(ViewerFilter filter) {
		propertyFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#addBusinessFilterProperty(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToProperty(ViewerFilter filter) {
		propertyBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart#isContainedInPropertyTable(EObject element)
	 * 
	 */
	public boolean isContainedInPropertyTable(EObject element) {
		return ((ReferencesTableSettings)property.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.RefArtefact_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
