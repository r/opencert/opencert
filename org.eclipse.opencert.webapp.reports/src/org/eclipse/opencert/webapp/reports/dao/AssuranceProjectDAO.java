/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.manager.ProjectCreationData;
import org.eclipse.opencert.webapp.reports.util.IdNameDesc;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.executors.HttpRequestDrivenInTransactionExecutor;
import org.springframework.stereotype.Component;

@Component
public class AssuranceProjectDAO
{

    public List<AssuranceProject> getProjects()
    {
        final List<AssuranceProject> result = new ArrayList<AssuranceProject>();

        new HttpRequestDrivenInTransactionExecutor() {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                try {
                    String projectsSQL = "select * from assuranceproject_assuranceproject where " + CDOStorageUtil.getMandatoryCDOQuerySuffix(false)
                            + " order by name";
                    CDOQuery projectsQuery = cdoTransaction.createQuery("sql", projectsSQL);

                    result.addAll(projectsQuery.getResult(AssuranceProject.class));
                } catch (Exception ex) {
                    OpencertLogger.error("Error reading projects!" + ex.toString());
                }
            }
        }.executeReadOnlyOperation();

        return result;
    }

    public AssuranceProject getProject(final long projectUid)
    {
        final List<AssuranceProject> result = new ArrayList<AssuranceProject>();

        new HttpRequestDrivenInTransactionExecutor() {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                try {
                    String projectsSQL = "select * from assuranceproject_assuranceproject where cdo_id = '" + projectUid + "'"
                            + CDOStorageUtil.getMandatoryCDOQuerySuffix();
                    CDOQuery projectsQuery = cdoTransaction.createQuery("sql", projectsSQL);

                    result.addAll(projectsQuery.getResult(AssuranceProject.class));
                } catch (Exception ex) {
                    OpencertLogger.error("Error reading projectUid: " + projectUid, ex);
                }
            }
        }.executeReadOnlyOperation();

        return result.get(0);
    }

    public void delete(final AssuranceProjectWrapper assuranceProjectWrapper)
    {
        new HttpRequestDrivenInTransactionExecutor() {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                try {
//                  Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
//                  CDOObject assuranceProj = (CDOObject) cdoTransaction.getObject(CDOUtil.getCDOObject(assuranceProjectWrapper.getAssuranceProject()));
//    
//                  resource.getContents().remove(assuranceProj);
//                  resource.setModified(true);

                    CDOObject assuranceProj = (CDOObject) cdoTransaction.getObject(CDOUtil.getCDOObject(assuranceProjectWrapper.getAssuranceProject()));
                    Resource r = ((AssuranceProject) assuranceProj).eResource();
                    r.getContents().remove(assuranceProj);
                    
                    r.setModified(true);

                } catch (Exception ex) {
                    OpencertLogger.error("Error while deleting the project!", ex);
                }
            }
        }.executeReadWriteOperation();
    }

    public void update(AssuranceProjectWrapper assuranceProjectWrapper, String name, String description)
    {
        new HttpRequestDrivenInTransactionExecutor() {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                try {
                    AssuranceProject assuranceProj = (AssuranceProject) cdoTransaction.getObject(CDOUtil.getCDOObject(assuranceProjectWrapper
                            .getAssuranceProject()));
                    Resource resource = assuranceProj.eResource();

                    assuranceProj.setName(name);
                    assuranceProj.setDescription(description);

                    resource.setModified(true);

                } catch (Exception ex) {
                    OpencertLogger.error("Problem with AssuranceProject update name: " + name, ex);
                }
            }
        }.executeReadWriteOperation();
    }
    
    public ProjectCreationData createNewProject(String projectName, String projectDescription, String baselineFrameworkName, Collection<IdNameDesc> addedBaseArtefacts)
    {
        ProjectCreationData result = new ProjectCreationData();
        
        new HttpRequestDrivenInTransactionExecutor() {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                try {
                	CDOResourceFolder projectFolder = cdoTransaction.getOrCreateResourceFolder(projectName);
                	CDOResourceFolder assuranceprojectFolder =  projectFolder.addResourceFolder("ASSURANCE_PROJECT");
                	projectFolder.addResourceFolder("EVIDENCE");
                	projectFolder.addResourceFolder("PROCESSES");
                	
                    AssuranceProject assuranceProject = AssuranceprojectFactory.eINSTANCE.createAssuranceProject();
                    assuranceProject.setDescription(projectDescription);
                    assuranceProject.setName(projectName);

                    BaselineConfig baselineConfig = AssuranceprojectFactory.eINSTANCE.createBaselineConfig();
                    baselineConfig.setName(baselineFrameworkName);
                    baselineConfig.setIsActive(true);     
                    
                    CDOResource assuranceprojectResource  = assuranceprojectFolder.addResource(projectName + ".assuranceproject");  
                    assuranceprojectResource.getContents().add(baselineConfig); 
                    assuranceProject.getBaselineConfig().add(baselineConfig);
                    
                    CDOResource targetResource  = cdoTransaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineFrameworkName + ".baseline");
                    BaseFramework baselineFramework = BaselineFactory.eINSTANCE.createBaseFramework();
                    baselineFramework.setName(baselineFrameworkName); 
                    targetResource.getContents().add(baselineFramework);

                    //Adding BaseArtefacts:
                    if (addedBaseArtefacts != null) {
                        for (IdNameDesc artefactToCreate : addedBaseArtefacts)
                        {
                        	BaseArtefact createdBaseArtefact = BaselineFactory.eINSTANCE.createBaseArtefact();
                        	createdBaseArtefact.setId(artefactToCreate.name);
                            createdBaseArtefact.setName(artefactToCreate.name);
                            createdBaseArtefact.setDescription(artefactToCreate.desc);
                            createdBaseArtefact.setIsSelected(true);
                            baselineFramework.getOwnedArtefact().add(createdBaseArtefact);
                            
                            result.addCreatedArtefact(artefactToCreate, createdBaseArtefact);
                        }
                    }
                    
                    baselineConfig.getRefFramework().add(baselineFramework);
                    
                    AssetsPackage assetsPackage = AssuranceprojectFactory.eINSTANCE.createAssetsPackage();
                    assetsPackage.setName(baselineFrameworkName);
                    assetsPackage.setIsActive(true);
                    
                    assuranceProject.getAssetsPackage().add(assetsPackage);
                    
                    assuranceprojectResource.getContents().add(assuranceProject);
                    assuranceprojectResource.setModified(true);
                    
                } catch (Exception ex) {
                    OpencertLogger.error("Error while creating a new project, name: " + projectName, ex);
                }
            }
        }.executeReadWriteOperation();
        
        long lastProjectID = getLastProjectID(projectName, projectDescription);
        result.setNewProjectID(lastProjectID);
        
        return result;
    }
    
    public String getAssuranceProjectResourcePath(AssuranceProject assuranceProject)
    {
    	final List<String> results = new ArrayList<String>();
    	
        new HttpRequestDrivenInTransactionExecutor() {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                try {
                    AssuranceProject assuranceProj = (AssuranceProject) cdoTransaction.getObject(CDOUtil.getCDOObject(assuranceProject));
                    Resource resource = assuranceProj.eResource();
 
                    String resourcePath = resource.getURI().path();
                    if (resourcePath.startsWith("/")) {
                    	resourcePath = resourcePath.substring(1);
                    }
                    results.add(resourcePath);
                } catch (Exception ex) {
                    OpencertLogger.error("Problem with getting Resource from AssuranceProject", ex);
                }
            }
        }.executeReadOnlyOperation();
        
        return results.get(0);
    }

    private long getLastProjectID(String projectName, String projectDescription)
    {
        final List<AssuranceProject> results = new LinkedList<>();

        new HttpRequestDrivenInTransactionExecutor() {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                try {
                    String projectsSQL = "select cdo_id from assuranceproject_assuranceproject where "
                            + String.format(" name='%s' and description='%s' ", projectName, projectDescription) 
                            + CDOStorageUtil.getMandatoryCDOQuerySuffix(true)
                            + " order by cdo_id desc";
                    CDOQuery projectsQuery = cdoTransaction.createQuery("sql", projectsSQL);

                    results.addAll(projectsQuery.getResult(AssuranceProject.class));
                    
                } catch (Exception ex) {
                    OpencertLogger.error(String.format("Error reading details of project name:[%s] descroption:[%s]", projectName, projectDescription), ex);
                }
            }
        }.executeReadOnlyOperation();

        final long res = results.stream().map(el -> CDOStorageUtil.getCDOId(el)).findFirst().orElse(-1L);
        
        return res;
    }
}
