/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage
 * @generated
 */
public class OpenCertTraceLinkMetaModelSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OpenCertTraceLinkMetaModelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpenCertTraceLinkMetaModelSwitch() {
		if (modelPackage == null) {
			modelPackage = OpenCertTraceLinkMetaModelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OpenCertTraceLinkMetaModelPackage.CONTRACT_CLAIM_LINK: {
				ContractClaimLink contractClaimLink = (ContractClaimLink)theEObject;
				T result = caseContractClaimLink(contractClaimLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OpenCertTraceLinkMetaModelPackage.CONTRACT_ARTEFACT_LINK: {
				ContractArtefactLink contractArtefactLink = (ContractArtefactLink)theEObject;
				T result = caseContractArtefactLink(contractArtefactLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK: {
				FormalPropertyClaimLink formalPropertyClaimLink = (FormalPropertyClaimLink)theEObject;
				T result = caseFormalPropertyClaimLink(formalPropertyClaimLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OpenCertTraceLinkMetaModelPackage.CONTRACT_AGREEMENT_LINK: {
				ContractAgreementLink contractAgreementLink = (ContractAgreementLink)theEObject;
				T result = caseContractAgreementLink(contractAgreementLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OpenCertTraceLinkMetaModelPackage.ANALYSIS_CONTEXT_ARTEFACT_LINK: {
				AnalysisContextArtefactLink analysisContextArtefactLink = (AnalysisContextArtefactLink)theEObject;
				T result = caseAnalysisContextArtefactLink(analysisContextArtefactLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK: {
				ComponentArgumentationElementLink componentArgumentationElementLink = (ComponentArgumentationElementLink)theEObject;
				T result = caseComponentArgumentationElementLink(componentArgumentationElementLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OpenCertTraceLinkMetaModelPackage.REQUIREMENT_CLAIM_LINK: {
				RequirementClaimLink requirementClaimLink = (RequirementClaimLink)theEObject;
				T result = caseRequirementClaimLink(requirementClaimLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contract Claim Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contract Claim Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContractClaimLink(ContractClaimLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contract Artefact Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contract Artefact Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContractArtefactLink(ContractArtefactLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Property Claim Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Property Claim Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalPropertyClaimLink(FormalPropertyClaimLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contract Agreement Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contract Agreement Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContractAgreementLink(ContractAgreementLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Analysis Context Artefact Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Analysis Context Artefact Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnalysisContextArtefactLink(AnalysisContextArtefactLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Argumentation Element Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Argumentation Element Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentArgumentationElementLink(ComponentArgumentationElementLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requirement Claim Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requirement Claim Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequirementClaimLink(RequirementClaimLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OpenCertTraceLinkMetaModelSwitch
