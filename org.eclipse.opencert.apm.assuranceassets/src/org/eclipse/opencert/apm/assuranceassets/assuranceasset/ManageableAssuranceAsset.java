/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Manageable Assurance Asset</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset#getEvaluation <em>Evaluation</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset#getLifecycleEvent <em>Lifecycle Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getManageableAssuranceAsset()
 * @model
 * @generated
 */
public interface ManageableAssuranceAsset extends AssuranceAsset {
	/**
	 * Returns the value of the '<em><b>Evaluation</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluation</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getManageableAssuranceAsset_Evaluation()
	 * @model containment="true"
	 * @generated
	 */
	EList<AssuranceAssetEvaluation> getEvaluation();

	/**
	 * Returns the value of the '<em><b>Lifecycle Event</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lifecycle Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lifecycle Event</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getManageableAssuranceAsset_LifecycleEvent()
	 * @model containment="true"
	 * @generated
	 */
	EList<AssuranceAssetEvent> getLifecycleEvent();

} // ManageableAssuranceAsset
