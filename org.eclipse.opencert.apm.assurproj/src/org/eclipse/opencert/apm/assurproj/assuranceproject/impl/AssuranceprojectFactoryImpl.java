/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.opencert.apm.assurproj.assuranceproject.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AssuranceprojectFactoryImpl extends EFactoryImpl implements AssuranceprojectFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AssuranceprojectFactory init() {
		try {
			AssuranceprojectFactory theAssuranceprojectFactory = (AssuranceprojectFactory)EPackage.Registry.INSTANCE.getEFactory(AssuranceprojectPackage.eNS_URI);
			if (theAssuranceprojectFactory != null) {
				return theAssuranceprojectFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AssuranceprojectFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssuranceprojectFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AssuranceprojectPackage.ASSURANCE_PROJECT: return (EObject)createAssuranceProject();
			case AssuranceprojectPackage.PERMISSION_CONFIG: return (EObject)createPermissionConfig();
			case AssuranceprojectPackage.ASSETS_PACKAGE: return (EObject)createAssetsPackage();
			case AssuranceprojectPackage.BASELINE_CONFIG: return (EObject)createBaselineConfig();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssuranceProject createAssuranceProject() {
		AssuranceProjectImpl assuranceProject = new AssuranceProjectImpl();
		return assuranceProject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PermissionConfig createPermissionConfig() {
		PermissionConfigImpl permissionConfig = new PermissionConfigImpl();
		return permissionConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetsPackage createAssetsPackage() {
		AssetsPackageImpl assetsPackage = new AssetsPackageImpl();
		return assetsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaselineConfig createBaselineConfig() {
		BaselineConfigImpl baselineConfig = new BaselineConfigImpl();
		return baselineConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssuranceprojectPackage getAssuranceprojectPackage() {
		return (AssuranceprojectPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AssuranceprojectPackage getPackage() {
		return AssuranceprojectPackage.eINSTANCE;
	}

} //AssuranceprojectFactoryImpl
