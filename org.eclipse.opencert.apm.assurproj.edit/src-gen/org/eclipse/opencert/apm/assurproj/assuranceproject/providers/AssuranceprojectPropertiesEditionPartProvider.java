/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.providers;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.api.providers.IPropertiesEditionPartProvider;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.forms.AssetsPackagePropertiesEditionPartForm;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.forms.AssuranceAssetsPropertiesEditionPartForm;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.forms.AssuranceProjectPropertiesEditionPartForm;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.forms.BaselineConfigPropertiesEditionPartForm;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.forms.PermissionConfigPropertiesEditionPartForm;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.forms.PermissionConfigurationsPropertiesEditionPartForm;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.forms.ProjectBaselinesPropertiesEditionPartForm;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl.AssetsPackagePropertiesEditionPartImpl;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl.AssuranceAssetsPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl.AssuranceProjectPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl.BaselineConfigPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl.PermissionConfigPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl.PermissionConfigurationsPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl.ProjectBaselinesPropertiesEditionPartImpl;

/**
 * 
 * 
 */
public class AssuranceprojectPropertiesEditionPartProvider implements IPropertiesEditionPartProvider {

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#provides(java.lang.Object)
	 * 
	 */
	public boolean provides(Object key) {
		return key == AssuranceprojectViewsRepository.class;
	}

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#getPropertiesEditionPart(java.lang.Object, int, org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(Object key, int kind, IPropertiesEditionComponent component) {
		if (key == AssuranceprojectViewsRepository.AssuranceProject_.class) {
			if (kind == AssuranceprojectViewsRepository.SWT_KIND)
				return new AssuranceProjectPropertiesEditionPartImpl(component);
			if (kind == AssuranceprojectViewsRepository.FORM_KIND)
				return new AssuranceProjectPropertiesEditionPartForm(component);
		}
		if (key == AssuranceprojectViewsRepository.PermissionConfig.class) {
			if (kind == AssuranceprojectViewsRepository.SWT_KIND)
				return new PermissionConfigPropertiesEditionPartImpl(component);
			if (kind == AssuranceprojectViewsRepository.FORM_KIND)
				return new PermissionConfigPropertiesEditionPartForm(component);
		}
		if (key == AssuranceprojectViewsRepository.AssetsPackage.class) {
			if (kind == AssuranceprojectViewsRepository.SWT_KIND)
				return new AssetsPackagePropertiesEditionPartImpl(component);
			if (kind == AssuranceprojectViewsRepository.FORM_KIND)
				return new AssetsPackagePropertiesEditionPartForm(component);
		}
		if (key == AssuranceprojectViewsRepository.BaselineConfig.class) {
			if (kind == AssuranceprojectViewsRepository.SWT_KIND)
				return new BaselineConfigPropertiesEditionPartImpl(component);
			if (kind == AssuranceprojectViewsRepository.FORM_KIND)
				return new BaselineConfigPropertiesEditionPartForm(component);
		}
		if (key == AssuranceprojectViewsRepository.AssuranceAssets.class) {
			if (kind == AssuranceprojectViewsRepository.SWT_KIND)
				return new AssuranceAssetsPropertiesEditionPartImpl(component);
			if (kind == AssuranceprojectViewsRepository.FORM_KIND)
				return new AssuranceAssetsPropertiesEditionPartForm(component);
		}
		if (key == AssuranceprojectViewsRepository.PermissionConfigurations.class) {
			if (kind == AssuranceprojectViewsRepository.SWT_KIND)
				return new PermissionConfigurationsPropertiesEditionPartImpl(component);
			if (kind == AssuranceprojectViewsRepository.FORM_KIND)
				return new PermissionConfigurationsPropertiesEditionPartForm(component);
		}
		if (key == AssuranceprojectViewsRepository.ProjectBaselines.class) {
			if (kind == AssuranceprojectViewsRepository.SWT_KIND)
				return new ProjectBaselinesPropertiesEditionPartImpl(component);
			if (kind == AssuranceprojectViewsRepository.FORM_KIND)
				return new ProjectBaselinesPropertiesEditionPartForm(component);
		}
		return null;
	}

}
