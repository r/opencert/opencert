/**
 * [2012,2018] - ALL4TEC COPYRIGHT
 *
 * CONFIDENTIAL, All Rights Reserved
 */
package org.eclipse.opencert.infra.dashboard.helpers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * Utility class used to provide methods to work on the Eclipse workbench easily.
 */
public final class EclipseHelper {

	/**
	 * Default constructor.
	 */
	private EclipseHelper() {
		super();
	}

	/**
	 * Return the current active workbench window, or if none is active, the first workbench window known by Eclipse.
	 *
	 * @return The current workbench window
	 */
	public static IWorkbenchWindow getWorkbenchWindow() {
		IWorkbench vWorkbench = PlatformUI.getWorkbench();

		IWorkbenchWindow vCurrentWorkbenchWindow = vWorkbench.getActiveWorkbenchWindow();

		// If current workbench window is null, get the first workbench of eclipse to used it
		if (Objects.isNull(vCurrentWorkbenchWindow)) {
			IWorkbenchWindow[] vWorkbenchWindows = vWorkbench.getWorkbenchWindows();
			if (ArrayUtils.isNotEmpty(vWorkbenchWindows)) {
				vCurrentWorkbenchWindow = vWorkbenchWindows[0];
			}
		}

		return vCurrentWorkbenchWindow;
	}

	/**
	 * Get the active workbench page.
	 *
	 * @return The current active page, or <code>null</code>
	 */
	public static IWorkbenchPage getActiveWorkbenchPage() {
		IWorkbenchPage vWorkbenchPage = null;

		IWorkbenchWindow vCurrentWorkbenchWindow = getWorkbenchWindow();

		if (Objects.nonNull(vCurrentWorkbenchWindow)) {
			vWorkbenchPage = vCurrentWorkbenchWindow.getActivePage();
		}

		return vWorkbenchPage;
	}

	/**
	 * Get the current active editor.
	 *
	 * @return The current editor, or <code>null</code> if none is open
	 */
	public static IEditorPart getActiveEditor() {
		IEditorPart vActiveEditorPart = null;

		IWorkbenchPage vWorkbenchPage = getActiveWorkbenchPage();

		if (Objects.nonNull(vWorkbenchPage)) {
			vActiveEditorPart = vWorkbenchPage.getActiveEditor();
		}

		return vActiveEditorPart;
	}

	/**
	 * Get the current active part.
	 *
	 * @return The current part, or <code>null</code> if none is open
	 */
	public static IWorkbenchPart getActivePart() {
		IWorkbenchPart vActivePart = null;

		IWorkbenchPage vWorkbenchPage = getActiveWorkbenchPage();

		if (Objects.nonNull(vWorkbenchPage)) {
			vActivePart = vWorkbenchPage.getActivePart();
		}

		return vActivePart;
	}

	/**
	 * Get the current active part site.
	 *
	 * @return The current part site, or <code>null</code> if none is open
	 */
	public static IWorkbenchPartSite getActivePartSite() {
		IWorkbenchPartSite vActivePart = null;

		IWorkbenchPart vWorkbenchPart = getActivePart();

		if (Objects.nonNull(vWorkbenchPart)) {
			vActivePart = vWorkbenchPart.getSite();
		}

		return vActivePart;
	}

	/**
	 * Get {@link Shell} from active workbench or create a new.
	 *
	 * @return {@link Shell} of active workbench if it is possible, otherwise a new one
	 */
	public static Shell getActiveShell() {
		Shell vActiveShell = new Shell();

		// Check if workbench is running
		if (PlatformUI.isWorkbenchRunning()) {
			IWorkbenchWindow vWorkbenchWindow = getWorkbenchWindow();

			// Verify if there is an active window to get its own shell
			if (Objects.nonNull(vWorkbenchWindow)) {
				vActiveShell = vWorkbenchWindow.getShell();
			}
		}

		return vActiveShell;
	}

	/**
	 * Get all the editor references in all pages of all workbench windows.
	 *
	 * @return A set of all the editor references
	 */
	public static Set<IEditorReference> getAllEditorReferences() {
		Set<IEditorReference> vAllEditorReferencesSet = new HashSet<>();

		IWorkbench vWorkbench = PlatformUI.getWorkbench();

		for (IWorkbenchWindow vWindow : vWorkbench.getWorkbenchWindows()) {
			for (IWorkbenchPage vPage : vWindow.getPages()) {
				vAllEditorReferencesSet.addAll(Arrays.asList(vPage.getEditorReferences()));
			}
		}

		return vAllEditorReferencesSet;
	}

	/**
	 * @return The first Papyrus diagram editor part from all editors, or {@code null} if not found
	 */
	public static IMultiDiagramEditor getFirstPapyrusDiagramEditorPart() {
		IMultiDiagramEditor vEditorPart = null;
		Iterator<IEditorReference> vIter = EclipseHelper.getAllEditorReferences().iterator();

		while (vIter.hasNext() && Objects.isNull(vEditorPart)) {
			IEditorReference vEditorReference = vIter.next();

			if (vEditorReference.getEditor(false) instanceof IMultiDiagramEditor) {
				vEditorPart = (IMultiDiagramEditor) vEditorReference.getEditor(false);
			}
		}

		return vEditorPart;
	}
}
