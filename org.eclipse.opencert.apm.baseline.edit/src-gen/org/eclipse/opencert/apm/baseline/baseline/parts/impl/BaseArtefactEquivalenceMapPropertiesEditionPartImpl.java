/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;

// End of user code

/**
 * 
 * 
 */
public class BaseArtefactEquivalenceMapPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseArtefactEquivalenceMapPropertiesEditionPart {

	protected ReferencesTable equivalenceMap;
	protected List<ViewerFilter> equivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> equivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected TableViewer artefactEquivalenceMap;
	protected List<ViewerFilter> artefactEquivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> artefactEquivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addArtefactEquivalenceMap;
	protected Button removeArtefactEquivalenceMap;
	protected Button editArtefactEquivalenceMap;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseArtefactEquivalenceMapPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseArtefactEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseArtefactEquivalenceMapStep.addStep(BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap);
		
		
		composer = new PartComposer(baseArtefactEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap) {
					return createEquivalenceMapAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap) {
					return createArtefactEquivalenceMapTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseArtefactEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEquivalenceMapAdvancedTableComposition(Composite parent) {
		this.equivalenceMap = new ReferencesTable(getDescription(BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap, BaselineMessages.BaseArtefactEquivalenceMapPropertiesEditionPart_EquivalenceMapLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				equivalenceMap.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				equivalenceMap.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				equivalenceMap.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				equivalenceMap.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.equivalenceMapFilters) {
			this.equivalenceMap.addFilter(filter);
		}
		this.equivalenceMap.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap, BaselineViewsRepository.SWT_KIND));
		this.equivalenceMap.createControls(parent);
		this.equivalenceMap.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData equivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		equivalenceMapData.horizontalSpan = 3;
		this.equivalenceMap.setLayoutData(equivalenceMapData);
		this.equivalenceMap.setLowerBound(0);
		this.equivalenceMap.setUpperBound(-1);
		equivalenceMap.setID(BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap);
		equivalenceMap.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEquivalenceMapAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactEquivalenceMapTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableArtefactEquivalenceMap = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableArtefactEquivalenceMap.setHeaderVisible(true);
		GridData gdArtefactEquivalenceMap = new GridData();
		gdArtefactEquivalenceMap.grabExcessHorizontalSpace = true;
		gdArtefactEquivalenceMap.horizontalAlignment = GridData.FILL;
		gdArtefactEquivalenceMap.grabExcessVerticalSpace = true;
		gdArtefactEquivalenceMap.verticalAlignment = GridData.FILL;
		tableArtefactEquivalenceMap.setLayoutData(gdArtefactEquivalenceMap);
		tableArtefactEquivalenceMap.setLinesVisible(true);

		// Start of user code for columns definition for ArtefactEquivalenceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableArtefactEquivalenceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableArtefactEquivalenceMap,
				SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableArtefactEquivalenceMap,
				SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableArtefactEquivalenceMap,
				SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Artefacts"); //$NON-NLS-1$
		// End IRR
		// End of user code

		artefactEquivalenceMap = new TableViewer(tableArtefactEquivalenceMap);
		artefactEquivalenceMap.setContentProvider(new ArrayContentProvider());
		artefactEquivalenceMap.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for ArtefactEquivalenceMap
			public String getColumnText(Object object, int columnIndex) {
				//Start IRR
				/*
				AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
				if (object instanceof EObject) {
					switch (columnIndex) {
					case 0:
						return labelProvider.getText(object);
					}
				}
				*/
				if (object instanceof EObject) {
					BaseEquivalenceMap baseEquivalenceMap = (BaseEquivalenceMap)object;
					switch (columnIndex) {
					case 0:							
						return baseEquivalenceMap.getId();
					case 1:							
						if (baseEquivalenceMap.getMapGroup() == null)
							return "";
						else
							return baseEquivalenceMap.getMapGroup().getName();
					case 2:							
						if (baseEquivalenceMap.getTarget() == null)
							return "";
						else{
							EList<RefAssurableElement> LstElement = baseEquivalenceMap.getTarget();
							Iterator<RefAssurableElement> iter = LstElement.iterator();
							String sElement = "[";
							while (iter.hasNext()) {
								RefAssurableElement assElement = iter.next();
								if (assElement instanceof  RefArtefact) {
								RefArtefact refArt = (RefArtefact) assElement;
								sElement = sElement + refArt.getName() + ",";
								}
							}
							//delete ,
							sElement = sElement.substring(0, sElement.length()-1);
							sElement = sElement + "]";
							return sElement;
						}
							
					}
				}
				//End IRR
				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		artefactEquivalenceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (artefactEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						artefactEquivalenceMap.refresh();
					}
				}
			}

		});
		GridData artefactEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		artefactEquivalenceMapData.minimumHeight = 120;
		artefactEquivalenceMapData.heightHint = 120;
		artefactEquivalenceMap.getTable().setLayoutData(artefactEquivalenceMapData);
		for (ViewerFilter filter : this.artefactEquivalenceMapFilters) {
			artefactEquivalenceMap.addFilter(filter);
		}
		EditingUtils.setID(artefactEquivalenceMap.getTable(), BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap);
		EditingUtils.setEEFtype(artefactEquivalenceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createArtefactEquivalenceMapPanel(tableContainer);
		// Start of user code for createArtefactEquivalenceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactEquivalenceMapPanel(Composite container) {
		Composite artefactEquivalenceMapPanel = new Composite(container, SWT.NONE);
		GridLayout artefactEquivalenceMapPanelLayout = new GridLayout();
		artefactEquivalenceMapPanelLayout.numColumns = 1;
		artefactEquivalenceMapPanel.setLayout(artefactEquivalenceMapPanelLayout);
		addArtefactEquivalenceMap = new Button(artefactEquivalenceMapPanel, SWT.NONE);
		addArtefactEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addArtefactEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addArtefactEquivalenceMap.setLayoutData(addArtefactEquivalenceMapData);
		addArtefactEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				artefactEquivalenceMap.refresh();
			}
		});
		EditingUtils.setID(addArtefactEquivalenceMap, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap);
		EditingUtils.setEEFtype(addArtefactEquivalenceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeArtefactEquivalenceMap = new Button(artefactEquivalenceMapPanel, SWT.NONE);
		removeArtefactEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeArtefactEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeArtefactEquivalenceMap.setLayoutData(removeArtefactEquivalenceMapData);
		removeArtefactEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (artefactEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						artefactEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeArtefactEquivalenceMap, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap);
		EditingUtils.setEEFtype(removeArtefactEquivalenceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editArtefactEquivalenceMap = new Button(artefactEquivalenceMapPanel, SWT.NONE);
		editArtefactEquivalenceMap.setText(BaselineMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editArtefactEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editArtefactEquivalenceMap.setLayoutData(editArtefactEquivalenceMapData);
		editArtefactEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (artefactEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactEquivalenceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						artefactEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editArtefactEquivalenceMap, BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap);
		EditingUtils.setEEFtype(editArtefactEquivalenceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createArtefactEquivalenceMapPanel

		// End of user code
		return artefactEquivalenceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart#initEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		equivalenceMap.setContentProvider(contentProvider);
		equivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.equivalenceMap);
		if (eefElementEditorReadOnlyState && equivalenceMap.isEnabled()) {
			equivalenceMap.setEnabled(false);
			equivalenceMap.setToolTipText(BaselineMessages.BaseArtefactEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !equivalenceMap.isEnabled()) {
			equivalenceMap.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart#updateEquivalenceMap()
	 * 
	 */
	public void updateEquivalenceMap() {
	equivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart#addFilterEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEquivalenceMap(ViewerFilter filter) {
		equivalenceMapFilters.add(filter);
		if (this.equivalenceMap != null) {
			this.equivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart#addBusinessFilterEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEquivalenceMap(ViewerFilter filter) {
		equivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart#isContainedInEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)equivalenceMap.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart#initArtefactEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initArtefactEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		artefactEquivalenceMap.setContentProvider(contentProvider);
		artefactEquivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactEquivalenceMap.Properties.artefactEquivalenceMap);
		if (eefElementEditorReadOnlyState && artefactEquivalenceMap.getTable().isEnabled()) {
			artefactEquivalenceMap.getTable().setEnabled(false);
			artefactEquivalenceMap.getTable().setToolTipText(BaselineMessages.BaseArtefactEquivalenceMap_ReadOnly);
			addArtefactEquivalenceMap.setEnabled(false);
			addArtefactEquivalenceMap.setToolTipText(BaselineMessages.BaseArtefactEquivalenceMap_ReadOnly);
			removeArtefactEquivalenceMap.setEnabled(false);
			removeArtefactEquivalenceMap.setToolTipText(BaselineMessages.BaseArtefactEquivalenceMap_ReadOnly);
			editArtefactEquivalenceMap.setEnabled(false);
			editArtefactEquivalenceMap.setToolTipText(BaselineMessages.BaseArtefactEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !artefactEquivalenceMap.getTable().isEnabled()) {
			artefactEquivalenceMap.getTable().setEnabled(true);
			addArtefactEquivalenceMap.setEnabled(true);
			removeArtefactEquivalenceMap.setEnabled(true);
			editArtefactEquivalenceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart#updateArtefactEquivalenceMap()
	 * 
	 */
	public void updateArtefactEquivalenceMap() {
	artefactEquivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart#addFilterArtefactEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArtefactEquivalenceMap(ViewerFilter filter) {
		artefactEquivalenceMapFilters.add(filter);
		if (this.artefactEquivalenceMap != null) {
			this.artefactEquivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart#addBusinessFilterArtefactEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArtefactEquivalenceMap(ViewerFilter filter) {
		artefactEquivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart#isContainedInArtefactEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInArtefactEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)artefactEquivalenceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseArtefactEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
