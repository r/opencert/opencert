/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.assuranceproject.utils;

import java.util.ArrayList;

import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.List;

public class ListAssuranceProject extends Dialog {
	
	private static final String ASSURANCEPROJECT = ".assuranceproject";	
	private CDOView viewCDO = null;
	protected ArrayList<String> sResult = null;
	protected List refList;
	protected ArrayList<String> refListDirMap;	
	protected boolean bcheck = true;

	public ListAssuranceProject(Shell parentShell, CDOSession sessionCDO) {
		super(parentShell);
			viewCDO = CDOConnectionUtil.instance.openView(sessionCDO);
			setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX );
	}

	public ListAssuranceProject(IShellProvider parentShell) {
		super(parentShell);
	}

	@Override
	protected Control createDialogArea(final Composite parent) {

		final Composite contents = (Composite)super.createDialogArea(parent);

		GridLayout contentsGridLayout = (GridLayout)contents.getLayout();
		contentsGridLayout.numColumns = 1;

		GridData contentsGridData = (GridData)contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;

		// Label
		Label idLabel = new Label(contents, SWT.NONE);
		idLabel.setText("Select source assurance project:");
		GridData idLabelGridData = new GridData();
		idLabelGridData.horizontalAlignment = SWT.FILL;
		idLabelGridData.verticalAlignment = SWT.FILL;
		idLabel.setLayoutData(idLabelGridData);
		
		//List
		refList = new List(contents, SWT.FILL | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);
		refList.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		refListDirMap = new ArrayList<String>();
		
		CDOResourceNode[] listR=  viewCDO.getElements();			
		
		for (int i = 0; i < listR.length; i++) {
			if (listR[i] instanceof CDOResourceFolder) {
				checkFolderContents((CDOResourceFolder) listR[i],ASSURANCEPROJECT );
			} else if (listR[i].getName().endsWith(ASSURANCEPROJECT)) {
				refList.add(listR[i].getName());
				refListDirMap.add(listR[i].getPath());
				//System.out.println(listR[i].getPath());
			}
		}			
		return contents;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("List Source Assurance Projects");
	}

	@Override
	protected void okPressed()
	{	
		if (refList.getSelectionCount()  == 0) {
			MessageDialog.openError(getShell(),
					"Select source assurance project", "It's neccesary to select a project.");
		} else if (refList.getSelectionCount() != 1) {
			MessageDialog.openError(getShell(),
					"Select source assurance project", "It's neccesary to select only one project.");
		} else {
			//System.out.println(refList.getItem(refList.getSelectionIndex()));
			int i = refList.getSelectionIndex();
			//System.out.println(i);
			String sSelectedProject = refList.getItem(i);
			//System.out.println(pepe);
			sResult = new ArrayList<String>();
			sResult.add(sSelectedProject);
			sResult.add(refListDirMap.get(refList.getSelectionIndex()));
			super.okPressed();
		}
	}

	@Override
	public boolean close()
	{
		return super.close();
	}
	public ArrayList<String> getResult()
	{	
		return sResult;
	}
	
	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	private void checkFolderContents(CDOResourceFolder cdoResourceFolder, String sCadena) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN = cdoResourceFolder.getNodes();
			for (int i = 0; i < listN.size(); i++) {
				if (listN.get(i) instanceof CDOResourceFolder) {
					checkFolderContents((CDOResourceFolder) listN.get(i), sCadena);
				} else if (listN.get(i).getName().endsWith(sCadena)) {
					refList.add(listN.get(i).getName());
					refListDirMap.add(listN.get(i).getPath());
					//System.out.println(listN.get(i).getPath());
				}
			}
		}
	}
	
	

}
