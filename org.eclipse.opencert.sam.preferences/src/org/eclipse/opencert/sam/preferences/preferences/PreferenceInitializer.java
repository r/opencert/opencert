/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.preferences.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.opencert.sam.preferences.Activator;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.Module_PATH,"");
		store.setDefault(PreferenceConstants.Pattern_PATH,"");
	
		/*
		store.setDefault(PreferenceConstants.Module_PATH, 
				ResourcesPlugin.getWorkspace().getRoot().getLocation().toFile().getPath().toString().concat("\\Modules"));
		store.setDefault(PreferenceConstants.Pattern_PATH, 
				ResourcesPlugin.getWorkspace().getRoot().getLocation().toFile().getPath().toString().concat("\\Patterns"));
		*/
		// Start MC
		/* Dawn cdo */
		//ARL For the first prototype, argumentation contracts are not available
//		store.setDefault(PreferenceConstants.Agreement_PATH, 
//				ResourcesPlugin.getWorkspace().getRoot().getLocation().toFile().getPath().toString().concat("\\Agreements"));
		// End MCP
	}

}
