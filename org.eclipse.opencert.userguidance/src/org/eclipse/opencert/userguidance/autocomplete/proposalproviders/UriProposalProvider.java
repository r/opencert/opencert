/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.autocomplete.proposalproviders;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.opencert.userguidance.autocomplete.ContentProposal2;
import org.eclipse.opencert.userguidance.util.ImageUtil;

public class UriProposalProvider extends AbstractProposalProvider {
	private static Image icon = null;
	private final static String path = "/icons/famfamfam/world.png";

	public UriProposalProvider(EObject eobj, int featureID) {
		super(eobj, featureID);

	}

	@Override
	protected List<ContentProposal2> createProposals(String stringToComplete,
			EObject eobj, int featureID) {
		List<ContentProposal2> result = new ArrayList<ContentProposal2>();

		for (Entry<String, String> entry : createDummyValues()) {
			if (entry.getKey().startsWith(stringToComplete)) {
				ContentProposal2 proposal = new ContentProposal2(
						entry.getKey(), entry.getValue(), getImage());
				result.add(proposal);
			}
		}

		return result;
	}

	private List<Entry<String, String>> createDummyValues() {
		List<Entry<String, String>> result = new ArrayList<Entry<String, String>>();
		result.add(new SimpleEntry<String, String>("uri:http://www.ikv.de",
				"ikv++"));
		result.add(new SimpleEntry<String, String>(
				"uri:http://www.tecnalia.com", "Tecnalia"));
		result.add(new SimpleEntry<String, String>("uri:http://www.tue.nl",
				"Technische Universiteit Eindhoven"));
		return result;
	}

	@Override
	protected Image createImage() {
		if (icon == null) {
			icon = ImageUtil.createImage(this, path);
		}

		return icon;
	}
}
