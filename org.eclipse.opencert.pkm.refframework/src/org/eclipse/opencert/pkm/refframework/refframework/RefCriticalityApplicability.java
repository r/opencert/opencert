/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ref Criticality Applicability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getApplicLevel <em>Applic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getCriticLevel <em>Critic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getComment <em>Comment</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefCriticalityApplicability()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface RefCriticalityApplicability extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Applic Level</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applic Level</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applic Level</em>' reference.
	 * @see #setApplicLevel(RefApplicabilityLevel)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefCriticalityApplicability_ApplicLevel()
	 * @model required="true"
	 * @generated
	 */
	RefApplicabilityLevel getApplicLevel();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getApplicLevel <em>Applic Level</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Applic Level</em>' reference.
	 * @see #getApplicLevel()
	 * @generated
	 */
	void setApplicLevel(RefApplicabilityLevel value);

	/**
	 * Returns the value of the '<em><b>Critic Level</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Critic Level</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Critic Level</em>' reference.
	 * @see #setCriticLevel(RefCriticalityLevel)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefCriticalityApplicability_CriticLevel()
	 * @model required="true"
	 * @generated
	 */
	RefCriticalityLevel getCriticLevel();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getCriticLevel <em>Critic Level</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Critic Level</em>' reference.
	 * @see #getCriticLevel()
	 * @generated
	 */
	void setCriticLevel(RefCriticalityLevel value);

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' attribute.
	 * @see #setComment(String)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefCriticalityApplicability_Comment()
	 * @model
	 * @generated
	 */
	String getComment();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getComment <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comment</em>' attribute.
	 * @see #getComment()
	 * @generated
	 */
	void setComment(String value);

} // RefCriticalityApplicability
