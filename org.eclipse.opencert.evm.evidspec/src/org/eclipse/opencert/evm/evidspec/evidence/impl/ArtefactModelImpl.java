/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.evm.evidspec.evidence.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Artefact Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl#getArtefact <em>Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl#getRepoUrl <em>Repo Url</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl#getRepoUser <em>Repo User</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl#getRepoPassword <em>Repo Password</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl#getRepoLocalPath <em>Repo Local Path</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl#isRepoUsesLocal <em>Repo Uses Local</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArtefactModelImpl extends DescribableElementImpl implements ArtefactModel {
	/**
	 * The default value of the '{@link #getRepoUrl() <em>Repo Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepoUrl()
	 * @generated
	 * @ordered
	 */
	protected static final String REPO_URL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getRepoUser() <em>Repo User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepoUser()
	 * @generated
	 * @ordered
	 */
	protected static final String REPO_USER_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getRepoPassword() <em>Repo Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepoPassword()
	 * @generated
	 * @ordered
	 */
	protected static final String REPO_PASSWORD_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getRepoLocalPath() <em>Repo Local Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepoLocalPath()
	 * @generated
	 * @ordered
	 */
	protected static final String REPO_LOCAL_PATH_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isRepoUsesLocal() <em>Repo Uses Local</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRepoUsesLocal()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REPO_USES_LOCAL_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArtefactModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EvidencePackage.Literals.ARTEFACT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ArtefactDefinition> getArtefact() {
		return (EList<ArtefactDefinition>)eDynamicGet(EvidencePackage.ARTEFACT_MODEL__ARTEFACT, EvidencePackage.Literals.ARTEFACT_MODEL__ARTEFACT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRepoUrl() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT_MODEL__REPO_URL, EvidencePackage.Literals.ARTEFACT_MODEL__REPO_URL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepoUrl(String newRepoUrl) {
		eDynamicSet(EvidencePackage.ARTEFACT_MODEL__REPO_URL, EvidencePackage.Literals.ARTEFACT_MODEL__REPO_URL, newRepoUrl);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRepoUser() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT_MODEL__REPO_USER, EvidencePackage.Literals.ARTEFACT_MODEL__REPO_USER, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepoUser(String newRepoUser) {
		eDynamicSet(EvidencePackage.ARTEFACT_MODEL__REPO_USER, EvidencePackage.Literals.ARTEFACT_MODEL__REPO_USER, newRepoUser);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRepoPassword() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT_MODEL__REPO_PASSWORD, EvidencePackage.Literals.ARTEFACT_MODEL__REPO_PASSWORD, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepoPassword(String newRepoPassword) {
		eDynamicSet(EvidencePackage.ARTEFACT_MODEL__REPO_PASSWORD, EvidencePackage.Literals.ARTEFACT_MODEL__REPO_PASSWORD, newRepoPassword);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRepoLocalPath() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT_MODEL__REPO_LOCAL_PATH, EvidencePackage.Literals.ARTEFACT_MODEL__REPO_LOCAL_PATH, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepoLocalPath(String newRepoLocalPath) {
		eDynamicSet(EvidencePackage.ARTEFACT_MODEL__REPO_LOCAL_PATH, EvidencePackage.Literals.ARTEFACT_MODEL__REPO_LOCAL_PATH, newRepoLocalPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRepoUsesLocal() {
		return (Boolean)eDynamicGet(EvidencePackage.ARTEFACT_MODEL__REPO_USES_LOCAL, EvidencePackage.Literals.ARTEFACT_MODEL__REPO_USES_LOCAL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepoUsesLocal(boolean newRepoUsesLocal) {
		eDynamicSet(EvidencePackage.ARTEFACT_MODEL__REPO_USES_LOCAL, EvidencePackage.Literals.ARTEFACT_MODEL__REPO_USES_LOCAL, newRepoUsesLocal);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_MODEL__ARTEFACT:
				return ((InternalEList<?>)getArtefact()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_MODEL__ARTEFACT:
				return getArtefact();
			case EvidencePackage.ARTEFACT_MODEL__REPO_URL:
				return getRepoUrl();
			case EvidencePackage.ARTEFACT_MODEL__REPO_USER:
				return getRepoUser();
			case EvidencePackage.ARTEFACT_MODEL__REPO_PASSWORD:
				return getRepoPassword();
			case EvidencePackage.ARTEFACT_MODEL__REPO_LOCAL_PATH:
				return getRepoLocalPath();
			case EvidencePackage.ARTEFACT_MODEL__REPO_USES_LOCAL:
				return isRepoUsesLocal();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_MODEL__ARTEFACT:
				getArtefact().clear();
				getArtefact().addAll((Collection<? extends ArtefactDefinition>)newValue);
				return;
			case EvidencePackage.ARTEFACT_MODEL__REPO_URL:
				setRepoUrl((String)newValue);
				return;
			case EvidencePackage.ARTEFACT_MODEL__REPO_USER:
				setRepoUser((String)newValue);
				return;
			case EvidencePackage.ARTEFACT_MODEL__REPO_PASSWORD:
				setRepoPassword((String)newValue);
				return;
			case EvidencePackage.ARTEFACT_MODEL__REPO_LOCAL_PATH:
				setRepoLocalPath((String)newValue);
				return;
			case EvidencePackage.ARTEFACT_MODEL__REPO_USES_LOCAL:
				setRepoUsesLocal((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_MODEL__ARTEFACT:
				getArtefact().clear();
				return;
			case EvidencePackage.ARTEFACT_MODEL__REPO_URL:
				setRepoUrl(REPO_URL_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT_MODEL__REPO_USER:
				setRepoUser(REPO_USER_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT_MODEL__REPO_PASSWORD:
				setRepoPassword(REPO_PASSWORD_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT_MODEL__REPO_LOCAL_PATH:
				setRepoLocalPath(REPO_LOCAL_PATH_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT_MODEL__REPO_USES_LOCAL:
				setRepoUsesLocal(REPO_USES_LOCAL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_MODEL__ARTEFACT:
				return !getArtefact().isEmpty();
			case EvidencePackage.ARTEFACT_MODEL__REPO_URL:
				return REPO_URL_EDEFAULT == null ? getRepoUrl() != null : !REPO_URL_EDEFAULT.equals(getRepoUrl());
			case EvidencePackage.ARTEFACT_MODEL__REPO_USER:
				return REPO_USER_EDEFAULT == null ? getRepoUser() != null : !REPO_USER_EDEFAULT.equals(getRepoUser());
			case EvidencePackage.ARTEFACT_MODEL__REPO_PASSWORD:
				return REPO_PASSWORD_EDEFAULT == null ? getRepoPassword() != null : !REPO_PASSWORD_EDEFAULT.equals(getRepoPassword());
			case EvidencePackage.ARTEFACT_MODEL__REPO_LOCAL_PATH:
				return REPO_LOCAL_PATH_EDEFAULT == null ? getRepoLocalPath() != null : !REPO_LOCAL_PATH_EDEFAULT.equals(getRepoLocalPath());
			case EvidencePackage.ARTEFACT_MODEL__REPO_USES_LOCAL:
				return isRepoUsesLocal() != REPO_USES_LOCAL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //ArtefactModelImpl
