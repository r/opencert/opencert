/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.actions;

import org.eclipse.amalgam.explorer.activity.ui.api.editor.pages.helper.SessionHelper;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.opencert.infra.dashboard.DashboardActivator;
import org.eclipse.opencert.infra.dashboard.helpers.ChessProjectVisitor;
import org.eclipse.opencert.infra.dashboard.helpers.OpenCertExtensions;
import org.eclipse.sirius.business.api.session.Session;

/**
 * Action to go to the Activity Explorer for the current OpenCert file (if the related session is opened).
 * 
 * TODO: Get the opened session then add this action to the context menu.
 */
public class GoToDashboardAction
		extends org.eclipse.amalgam.explorer.activity.ui.api.actions.GoToActivityExplorerAction {

	/**
	 * Default constructor.
	 */
	protected GoToDashboardAction() {
		super();
	}

	/**
	 * Get the session from the given selection. It can be retrieved from a DI file or from an {@link IProject}.
	 *
	 * @return The session or <code>null</code> if not found
	 */
	@Override
	protected Session getSession() {
		Session vSession = null;

		IStructuredSelection vSelection = getStructuredSelection();
		if (!vSelection.isEmpty()) {
			Object vSelectedElement = vSelection.getFirstElement();

			// Ensure that the given selection is a Chess DI file with an open session
			if ((vSelectedElement instanceof IFile)
					&& OpenCertExtensions.DI_EXTENSION.equals(((IFile) vSelectedElement).getFileExtension())) {
				vSession = SessionHelper.getSessionForDiagramFile((IFile) vSelectedElement);

			} else if (vSelectedElement instanceof IProject && ((IProject) vSelectedElement).isOpen()) {
				// If the selection is a project, search for the DI file at the first level
				ChessProjectVisitor vVisitor = new ChessProjectVisitor();

				try {
					((IProject) vSelectedElement).accept(vVisitor, IResource.DEPTH_ONE, IResource.NONE);
				} catch (final CoreException pException) {
					DashboardActivator.getDefault().getLog().log(pException.getStatus());
				}

				vSession = SessionHelper.getSessionForDiagramFile(vVisitor.getFirstGraphicalFile());
			}
		}

		return vSession;
	}
}
