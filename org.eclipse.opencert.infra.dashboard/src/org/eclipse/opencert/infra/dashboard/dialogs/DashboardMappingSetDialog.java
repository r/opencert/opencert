/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.dialogs;

import java.util.Iterator;
import java.util.Objects;

import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.utils.MappingSet;
import org.eclipse.opencert.pkm.refframework.refframework.utils.TreeViewerExt;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * A custom {@link MappingSet} dialog to use for the dashboard.
 */
public class DashboardMappingSetDialog extends MappingSet {

	protected Combo mComboSource = null;

	/**
	 * Default constructor.
	 *
	 * @param pParentShell The parent shell
	 */
	public DashboardMappingSetDialog(Shell pParentShell) {
		super(pParentShell);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Overridden to create a custom from dialog area with:
	 * <ul>
	 * <li>a combo box to select the source refframework</li>
	 * <li>a tree viewer to show its content</li>
	 * </ul>
	 */
	@Override
	protected void createFromDialogArea(final Composite pParent, ComposedAdapterFactory pAdapterFactory) {

		sourceComposite = new Composite(pParent, SWT.NONE);
		GridData vGridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		sourceComposite.setLayoutData(vGridData);
		GridLayout vGridLayout = new GridLayout();
		vGridLayout.marginHeight = 0;
		vGridLayout.marginWidth = 0;
		vGridLayout.numColumns = 1;
		sourceComposite.setLayout(vGridLayout);

		// Create the From label
		sourceLabel = new Label(sourceComposite, SWT.NONE);
		sourceLabel.setText("From");
		GridData vSourceLabelGridData = new GridData();
		vSourceLabelGridData.horizontalSpan = 2;
		vSourceLabelGridData.horizontalAlignment = SWT.FILL;
		vSourceLabelGridData.verticalAlignment = SWT.FILL;
		sourceLabel.setLayoutData(vSourceLabelGridData);

		// Create the combo box to select source refframework
		mComboSource = new Combo(sourceComposite, SWT.READ_ONLY);
		GridData vSourceGridData = new GridData();
		vSourceGridData.verticalAlignment = SWT.FILL;
		vSourceGridData.horizontalAlignment = SWT.FILL;
		vSourceGridData.widthHint = Display.getCurrent().getBounds().width / 5;
		vSourceGridData.heightHint = Display.getCurrent().getBounds().height / 5;
		vSourceGridData.grabExcessHorizontalSpace = true;
		vSourceGridData.grabExcessVerticalSpace = false;
		mComboSource.setLayoutData(vSourceGridData);

		CDOResourceNode[] vNodesArray = mCDOView.getElements();
		for (int i = 0; i < vNodesArray.length; i++) {
			if (vNodesArray[i] instanceof CDOResourceFolder) {
				checkFolderContentsREF((CDOResourceFolder) vNodesArray[i], REFFRAMEWORK);
			} else if (vNodesArray[i].getName().endsWith(REFFRAMEWORK)) {
				refListDirRef.add(vNodesArray[i].getPath());
			}
		}

		// Add the refframework's name to the combo
		Iterator<String> vIterRefframework = refListDirRef.iterator();
		while (vIterRefframework.hasNext()) {
			mComboSource.add(vIterRefframework.next().toString());
		}

		mComboSource.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent pEvent) {
				if (cbMapGroup.getSelectionIndex() != -1) {
					sourceComposite.setEnabled(true);
				}
				resourceFrom = mCDOTransaction.getResource(refListDirRef.get(mComboSource.getSelectionIndex()));
				createModelFromCDOResource(modelViewerSource, resourceFrom);
				modelViewerSource.expandAll();
				
				createModelSourceAll(modelViewerSourceAll, resourceFrom);
				modelViewerSourceAll.expandAll();
			}
		});

		// Create a tree viewer to display the source refframework
		modelViewerSource = new TreeViewerExt(sourceComposite, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);

		GridData modelViewerGridData = new GridData();
		modelViewerGridData.verticalAlignment = SWT.FILL;
		modelViewerGridData.horizontalAlignment = SWT.FILL;
		modelViewerGridData.widthHint = Display.getCurrent().getBounds().width / 3;
		modelViewerGridData.heightHint = Display.getCurrent().getBounds().height / 3;
		modelViewerGridData.grabExcessHorizontalSpace = true;
		modelViewerGridData.grabExcessVerticalSpace = true;

		modelViewerSource.getTree().setLayoutData(modelViewerGridData);
		modelViewerSource.setContentProvider(new AdapterFactoryContentProvider(pAdapterFactory));
		modelViewerSource.setLabelProvider(new AdapterFactoryLabelProvider(pAdapterFactory));

		bElementTreeSelected = false;

		modelViewerSource.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent pEvent) {

				cbEquivalenceMap.removeAll();
				txtJustification.setText("");
				idText.setText("");
				nameText.setText("");
				cbType.select(0);

				bElementTreeSelected = true;
				bNewEquivalenceMap = false;

				modelViewerTarget.expandAll();
				modelViewerTarget.setAllChecked(false);

				modelViewerSourceAll.setAllChecked(false);

				object = null;

				if (pEvent.getSelection() instanceof IStructuredSelection) {

					IStructuredSelection sel = (IStructuredSelection) pEvent.getSelection();
					object = (EObject) sel.getFirstElement();

					if (object instanceof RefFramework || object instanceof RefEquivalenceMap) {
						bElementTreeSelected = false;
						return;
					}

					if (sElementFilter.contentEquals("Requirement")) {
						if (object instanceof RefActivity) {
							MessageDialog.openError(getShell(), "Equivalence Map",
									"It's neccessary to select a requirement, not an activity.");
							return;
						}
					}

					if (object instanceof EObject) {
						// Verify selection mapgroup and cbtarget
						mapGroup = null;
						if ((cbMapGroup.getSelectionIndex() == -1) || (cbTarget.getSelectionIndex() == -1)) {
							MessageDialog.openError(getShell(), "Map Group",
									"It's neccessary to select a map group and a target reference framework.");

						} else {
							// mapGroup is selected
							mapGroup = lstMapGroup.get(cbMapGroup.getSelectionIndex());

							// Reset the list Equivalence Map of the selected element tree
							lstrefEquiMapFrom = null;

							sElementFilter = cbFilterElementMap.getItem(cbFilterElementMap.getSelectionIndex());

							// Object is the assurableElement selected from the source Tree
							if (sElementFilter.contentEquals("Activity")) {
								RefActivity vRefActFrom = (RefActivity) object;
								lstrefEquiMapFrom = vRefActFrom.getEquivalence();
							} else if (sElementFilter.contentEquals("Artefact")) {
								RefArtefact vRefArtefactFrom = (RefArtefact) object;
								lstrefEquiMapFrom = vRefArtefactFrom.getEquivalence();
							} else if (sElementFilter.contentEquals("Requirement")) {
								RefRequirement vRefReqFrom = (RefRequirement) object;
								lstrefEquiMapFrom = vRefReqFrom.getEquivalence();
							} else if (sElementFilter.contentEquals("Role")) {
								RefRole vRefRoleFrom = (RefRole) object;
								lstrefEquiMapFrom = vRefRoleFrom.getEquivalence();
							} else {
								RefTechnique vRefTechniqueFrom = (RefTechnique) object;
								lstrefEquiMapFrom = vRefTechniqueFrom.getEquivalence();
							}

							lstEquivalenceMap.clear();
							if (Objects.isNull(lstrefEquiMapFrom) || lstrefEquiMapFrom.isEmpty()) {
								bNewEquivalenceMap = true;
							} else {
								Iterator<RefEquivalenceMap> vIterEquipMap = lstrefEquiMapFrom.iterator();
								bEqualMapGroup = false;
								boolean vIsFirst = true;
								while (vIterEquipMap.hasNext()) {
									final RefEquivalenceMap vRefEquiMapFrom = (RefEquivalenceMap) vIterEquipMap.next();

									if (vRefEquiMapFrom.getMapGroup() != null) {
										MapGroup vMapGroupFrom = vRefEquiMapFrom.getMapGroup();
										if (CDOUtil.getCDOObject(vMapGroupFrom).cdoID()
												.equals(CDOUtil.getCDOObject(mapGroup).cdoID())) {
											bEqualMapGroup = true;

											lstEquivalenceMap.add(vRefEquiMapFrom);
											cbEquivalenceMap.add(vRefEquiMapFrom.getName());
											if (vIsFirst) {
												cbEquivalenceMap.select(0);
												vIsFirst = false;
												cbEquivalenceMap.notifyListeners(SWT.Selection, new Event());
											}
										}
									}
								}
							}
						}
					}
				}
			}
		});
	}
}
