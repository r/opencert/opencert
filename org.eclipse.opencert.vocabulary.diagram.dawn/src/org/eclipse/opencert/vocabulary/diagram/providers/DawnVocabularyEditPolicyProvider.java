/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.diagram.providers;

import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.services.editpolicy.CreateEditPoliciesOperation;
import org.eclipse.gmf.runtime.diagram.ui.services.editpolicy.IEditPolicyProvider;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.VocabularyEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.policies.DawnVocabularyCanonicalEditPolicy;

public class DawnVocabularyEditPolicyProvider extends AbstractProvider
		implements IEditPolicyProvider {

	public static String ID = "org.eclipse.opencert.vocabulary.diagram.providers.DawnVocabularyEditPolicyProvider";

	public boolean provides(IOperation operation) {
		if (operation instanceof CreateEditPoliciesOperation) {
			CreateEditPoliciesOperation editPoliciesOperation = (CreateEditPoliciesOperation) operation;
			if (editPoliciesOperation.getEditPart() instanceof VocabularyEditPart) {
				return true;
			}
		}
		return false;
	}

	public void createEditPolicies(EditPart editPart) {
		if (editPart instanceof VocabularyEditPart) {
			editPart.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
					new DawnVocabularyCanonicalEditPolicy());
		}
	}
}
