/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.messages;

import java.io.Serializable;

import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.ecore.EClass;

public class PersistenceEventMessage
    implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    private final PersistenceEventType persistenceEventType;
    private final String entityType;
    private final long entityUid;
    
    
    public PersistenceEventMessage(PersistenceEventType persistenceEventType,
            EClass eClass, CDOID uid)
    {
        this.persistenceEventType = persistenceEventType;
        this.entityType = eClass.getEPackage().getName() + "." + eClass.getName();
        this.entityUid = Long.parseLong(uid.toURIFragment());
    }
    
    
    public PersistenceEventType getPersistenceEventType()
    {
        return persistenceEventType;
    }


    public String getEntityType()
    {
        return entityType;
    }


    public long getEntityUid()
    {
        return entityUid;
    }
    
    
    public String toString()
    {
        return persistenceEventType  + "|" + entityType + "|" + entityUid;
    }
}
