/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.relations;

import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.impactanalysis.eventcreators.ChangeDrivenEventsCreatorsFactory;
import org.eclipse.opencert.impactanalysis.eventcreators.IChangeDrivenEventsCreator;

abstract class AbstractArtefactsRelationImpact
    implements IArtefactRelationImpact
{
    private final int recursionDepth;
    private ChangeEffectKind impactedChangeEffectKind; // which exact ModificationEffect type will be executed 
    private final Long impactingArtefactCDOId;
    private final Long impactedArtefactCDOId;
    private final String impactingArtefactRelationUniqueId;
    private boolean suppressed;
    private EventKind impactingEventKind;
    private final ArtefactRelationType artefactRelationType;
    
    
    protected AbstractArtefactsRelationImpact(int recursionDepth, ArtefactRelationType artefactRelationType, 
            ChangeEffectKind impactedChangeEffectKind, EventKind impactingEventKind,
            String impactingArtefactRelationUniqueId, Long impactingArtefactCDOId, Long impactedArtefactCDOId)
    {
        this.impactingArtefactRelationUniqueId = impactingArtefactRelationUniqueId;
        this.impactingArtefactCDOId = impactingArtefactCDOId;
        this.impactedArtefactCDOId = impactedArtefactCDOId;
        
        this.recursionDepth = recursionDepth;
        this.impactedChangeEffectKind = impactedChangeEffectKind;
        this.impactingEventKind = impactingEventKind;
        this.artefactRelationType = artefactRelationType;
    }
    
    /**
     * This is default implementation. Note that "proxy" relations (EVIDENCE_TO_BASELINE_VIA_MAP_RELATION,BASELINE_TO_EVIDENCE_VIA_MAP_RELATION)
     * will override this and simple forward impactingEventKind
     */
    @Override
    public EventKind generateNewEventBasedOnChangeEffectKindOrForwardCurrent(
            EventKind impactingEventKind)
    {
        IChangeDrivenEventsCreator changeDrivenEventsCreator = ChangeDrivenEventsCreatorsFactory.
                getProperChangeDrivenEventsCreator(getImpactedChangeEffectKind());
        //none found, abort
        if (changeDrivenEventsCreator == null) {
            return null;
        }
        
        return changeDrivenEventsCreator.getEventKind();
    }
    

   @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append("[" + getRecursionDepth() + "] ");
        stringBuilder.append(getRelationTypeExtraInfo());                    
        stringBuilder.append(getUserFriendlyImpactingArtefactName() + " (" + impactingArtefactCDOId + ")");
        stringBuilder.append(" : ");
        stringBuilder.append(impactingEventKind);
        stringBuilder.append(" ---- ");
        stringBuilder.append(getUserFriendlyRelationName() + " (" + impactingArtefactRelationUniqueId + ") : ");
        stringBuilder.append(getImpactedChangeEffectKind());
        stringBuilder.append(" ---> ");
        stringBuilder.append(getUserFriendlyImpactedArtefactName() + " (" + impactedArtefactCDOId + ")");
        
        return stringBuilder.toString();
    }
    
    
    protected String getRelationTypeExtraInfo()
    {
        return "";
    }

    
    @Override
    public ChangeEffectKind getImpactedChangeEffectKind()
    {
        return impactedChangeEffectKind;
    }

    
    @Override
    public int getRecursionDepth()
    {
        return recursionDepth;
    }

    @Override
    public Long getImpactingArtefactCDOId()
    {
        return impactingArtefactCDOId;
    }

    
    @Override
    public Long getImpactedArtefactCDOId()
    {
        return impactedArtefactCDOId;
    }
    
    
    @Override
    public String getImpactingArtefactRelationUniqueId()
    {
        return impactingArtefactRelationUniqueId;
    }


    @Override
    public boolean isSuppressed()
    {
        return suppressed;
    }


    @Override
    public void setSuppressed(boolean suppressed)
    {
        this.suppressed = suppressed;
    }

    
    @Override
    public EventKind getImpactingEventKind()
    {
        return impactingEventKind;
    }
    
    @Override
    public ArtefactRelationType getArtefactRelationType()
    {
        return artefactRelationType;
    }
}
