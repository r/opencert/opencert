/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.vocabulary;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vocabulary</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.vocabulary.Vocabulary#getTerms <em>Terms</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.Vocabulary#getCategories <em>Categories</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.Vocabulary#getSourcesOfDefinition <em>Sources Of Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getVocabulary()
 * @model
 * @generated
 */
public interface Vocabulary extends VocabularyElement
{
  /**
   * Returns the value of the '<em><b>Terms</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.opencert.vocabulary.Term}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Terms</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Terms</em>' containment reference list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getVocabulary_Terms()
   * @model containment="true"
   * @generated
   */
  EList<Term> getTerms();

  /**
   * Returns the value of the '<em><b>Categories</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.opencert.vocabulary.Category}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Categories</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Categories</em>' containment reference list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getVocabulary_Categories()
   * @model containment="true"
   * @generated
   */
  EList<Category> getCategories();

  /**
   * Returns the value of the '<em><b>Sources Of Definition</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.opencert.vocabulary.SourceOfDefinition}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sources Of Definition</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sources Of Definition</em>' containment reference list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getVocabulary_SourcesOfDefinition()
   * @model containment="true"
   * @generated
   */
  EList<SourceOfDefinition> getSourcesOfDefinition();

} // Vocabulary
