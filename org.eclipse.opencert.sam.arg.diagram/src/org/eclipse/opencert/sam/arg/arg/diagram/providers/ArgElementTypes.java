/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedChallengeEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedContextEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedCounterEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedInferenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.CaseEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.Choice2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorPlugin;
import org.eclipse.swt.graphics.Image;

/**
 * @generated
 */
public class ArgElementTypes {

	/**
	 * @generated
	 */
	private ArgElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			ArgDiagramEditorPlugin.getInstance()
					.getItemProvidersAdapterFactory());

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType Case_1000 = getElementType("org.eclipse.opencert.sam.arg.diagram.Case_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Claim_2001 = getElementType("org.eclipse.opencert.sam.arg.diagram.Claim_2001"); //$NON-NLS-1$
	/**
	 * ARL changes
	 */
	public static final IElementType Assumption_2000 = getElementType("org.eclipse.opencert.sam.arg.diagram.Assumption_2000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Agreement_2002 = getElementType("org.eclipse.opencert.sam.arg.diagram.Agreement_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ArgumentReasoning_2003 = getElementType("org.eclipse.opencert.sam.arg.diagram.ArgumentReasoning_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Argumentation_2004 = getElementType("org.eclipse.opencert.sam.arg.diagram.Argumentation_2004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType InformationElementCitation_2005 = getElementType("org.eclipse.opencert.sam.arg.diagram.InformationElementCitation_2005"); //$NON-NLS-1$
	/**
	 * ARL changes
	 * Includes Justification, Context and Solution (GSN naming)
	 */
	public static final IElementType Justification_3002 = getElementType("org.eclipse.opencert.sam.arg.diagram.Justification_3002"); //$NON-NLS-1$
	public static final IElementType Context_3003 = getElementType("org.eclipse.opencert.sam.arg.diagram.Context_3003"); //$NON-NLS-1$
	public static final IElementType Solution_3004 = getElementType("org.eclipse.opencert.sam.arg.diagram.Solution_3004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ArgumentElementCitation_2006 = getElementType("org.eclipse.opencert.sam.arg.diagram.ArgumentElementCitation_2006"); //$NON-NLS-1$
	/**
	 * ARL change
	 * Includes AwayClaim, AwayContest and AwaySolution
	 */
	public static final IElementType AwayClaim_5001 = getElementType("org.eclipse.opencert.sam.arg.diagram.AwayClaim_5001"); //$NON-NLS-1$
	public static final IElementType AwayContext_5002 = getElementType("org.eclipse.opencert.sam.arg.diagram.AwayContext_5002"); //$NON-NLS-1$
	public static final IElementType AwaySolution_5003 = getElementType("org.eclipse.opencert.sam.arg.diagram.AwaySolution_5003"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Choice_2007 = getElementType("org.eclipse.opencert.sam.arg.diagram.Choice_2007"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Choice_3001 = getElementType("org.eclipse.opencert.sam.arg.diagram.Choice_3001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AssertedInference_4001 = getElementType("org.eclipse.opencert.sam.arg.diagram.AssertedInference_4001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AssertedEvidence_4002 = getElementType("org.eclipse.opencert.sam.arg.diagram.AssertedEvidence_4002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AssertedContext_4003 = getElementType("org.eclipse.opencert.sam.arg.diagram.AssertedContext_4003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AssertedChallenge_4004 = getElementType("org.eclipse.opencert.sam.arg.diagram.AssertedChallenge_4004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AssertedCounterEvidence_4005 = getElementType("org.eclipse.opencert.sam.arg.diagram.AssertedCounterEvidence_4005"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(Case_1000, ArgPackage.eINSTANCE.getCase());

			elements.put(Claim_2001, ArgPackage.eINSTANCE.getClaim());
			
			elements.put(Agreement_2002, ArgPackage.eINSTANCE.getAgreement());

			elements.put(ArgumentReasoning_2003,
					ArgPackage.eINSTANCE.getArgumentReasoning());

			elements.put(Argumentation_2004,
					ArgPackage.eINSTANCE.getArgumentation());

			elements.put(InformationElementCitation_2005,
					ArgPackage.eINSTANCE.getInformationElementCitation());

			elements.put(ArgumentElementCitation_2006,
					ArgPackage.eINSTANCE.getArgumentElementCitation());

			elements.put(Choice_2007, ArgPackage.eINSTANCE.getChoice());

			elements.put(Choice_3001, ArgPackage.eINSTANCE.getChoice());

			elements.put(AssertedInference_4001,
					ArgPackage.eINSTANCE.getAssertedInference());

			elements.put(AssertedEvidence_4002,
					ArgPackage.eINSTANCE.getAssertedEvidence());

			elements.put(AssertedContext_4003,
					ArgPackage.eINSTANCE.getAssertedContext());

			elements.put(AssertedChallenge_4004,
					ArgPackage.eINSTANCE.getAssertedChallenge());

			elements.put(AssertedCounterEvidence_4005,
					ArgPackage.eINSTANCE.getAssertedCounterEvidence());
			//ARLchanges ini
			//Change to GSN naming
			elements.put(Assumption_2000, ArgPackage.eINSTANCE.getClaim());
			elements.put(Justification_3002,
					ArgPackage.eINSTANCE.getInformationElementCitation());
			elements.put(Context_3003,
					ArgPackage.eINSTANCE.getInformationElementCitation());
			elements.put(Solution_3004,
					ArgPackage.eINSTANCE.getInformationElementCitation());
			elements.put(AwayClaim_5001,
					ArgPackage.eINSTANCE.getArgumentElementCitation());
			elements.put(AwayContext_5002,
					ArgPackage.eINSTANCE.getArgumentElementCitation());
			elements.put(AwaySolution_5003,
					ArgPackage.eINSTANCE.getArgumentElementCitation());			
			//ARL end
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(Case_1000);
			KNOWN_ELEMENT_TYPES.add(Claim_2001);
			KNOWN_ELEMENT_TYPES.add(Agreement_2002);
			KNOWN_ELEMENT_TYPES.add(ArgumentReasoning_2003);
			KNOWN_ELEMENT_TYPES.add(Argumentation_2004);
			KNOWN_ELEMENT_TYPES.add(InformationElementCitation_2005);
			KNOWN_ELEMENT_TYPES.add(ArgumentElementCitation_2006);
			KNOWN_ELEMENT_TYPES.add(Choice_2007);
			KNOWN_ELEMENT_TYPES.add(Choice_3001);
			KNOWN_ELEMENT_TYPES.add(AssertedInference_4001);
			KNOWN_ELEMENT_TYPES.add(AssertedEvidence_4002);
			KNOWN_ELEMENT_TYPES.add(AssertedContext_4003);
			KNOWN_ELEMENT_TYPES.add(AssertedChallenge_4004);
			KNOWN_ELEMENT_TYPES.add(AssertedCounterEvidence_4005);
			/**
			 * ARL changes
			 * Change to GSN naming
			 */
			KNOWN_ELEMENT_TYPES.add(Assumption_2000);
			KNOWN_ELEMENT_TYPES.add(Justification_3002);
			KNOWN_ELEMENT_TYPES.add(Context_3003);
			KNOWN_ELEMENT_TYPES.add(Solution_3004);
			KNOWN_ELEMENT_TYPES.add(AwayClaim_5001);
			KNOWN_ELEMENT_TYPES.add(AwayContext_5002);
			KNOWN_ELEMENT_TYPES.add(AwaySolution_5003);

		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case CaseEditPart.VISUAL_ID:
			return Case_1000;
		case ClaimEditPart.VISUAL_ID:
			return Claim_2001;	
		case AgreementEditPart.VISUAL_ID:
			return Agreement_2002;
		case ArgumentReasoningEditPart.VISUAL_ID:
			return ArgumentReasoning_2003;
		case ArgumentationEditPart.VISUAL_ID:
			return Argumentation_2004;
		case InformationElementCitationEditPart.VISUAL_ID:
			return InformationElementCitation_2005;
		case ArgumentElementCitationEditPart.VISUAL_ID:
			return ArgumentElementCitation_2006;
		case ChoiceEditPart.VISUAL_ID:
			return Choice_2007;
		case Choice2EditPart.VISUAL_ID:
			return Choice_3001;
		case AssertedInferenceEditPart.VISUAL_ID:
			return AssertedInference_4001;
		case AssertedEvidenceEditPart.VISUAL_ID:
			return AssertedEvidence_4002;
		case AssertedContextEditPart.VISUAL_ID:
			return AssertedContext_4003;
		case AssertedChallengeEditPart.VISUAL_ID:
			return AssertedChallenge_4004;
		case AssertedCounterEvidenceEditPart.VISUAL_ID:
			return AssertedCounterEvidence_4005;
		}
		return null;
	}

	/**
	 * @generated
	 */
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(
			elementTypeImages) {

		/**
		 * @generated
		 */
		@Override
		public boolean isKnownElementType(IElementType elementType) {
			return org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes
					.isKnownElementType(elementType);
		}

		/**
		 * @generated
		 */
		@Override
		public IElementType getElementTypeForVisualId(int visualID) {
			return org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes
					.getElementType(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public ENamedElement getDefiningNamedElement(
				IAdaptable elementTypeAdapter) {
			return org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes
					.getElement(elementTypeAdapter);
		}
	};

}
