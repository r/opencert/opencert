/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefFrameworkPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class RefFrameworkPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for ownedActivities ReferencesTable
	 */
	protected ReferencesTableSettings ownedActivitiesSettings;
	
	/**
	 * Settings for ownedArtefact ReferencesTable
	 */
	protected ReferencesTableSettings ownedArtefactSettings;
	
	/**
	 * Settings for ownedRequirement ReferencesTable
	 */
	protected ReferencesTableSettings ownedRequirementSettings;
	
	/**
	 * Settings for ownedApplicLevel ReferencesTable
	 */
	protected ReferencesTableSettings ownedApplicLevelSettings;
	
	/**
	 * Settings for ownedCriticLevel ReferencesTable
	 */
	protected ReferencesTableSettings ownedCriticLevelSettings;
	
	/**
	 * Settings for ownedRole ReferencesTable
	 */
	protected ReferencesTableSettings ownedRoleSettings;
	
	/**
	 * Settings for ownedTechnique ReferencesTable
	 */
	protected ReferencesTableSettings ownedTechniqueSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public RefFrameworkPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refFramework, String editing_mode) {
		super(editingContext, refFramework, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = RefframeworkViewsRepository.class;
		partKey = RefframeworkViewsRepository.RefFramework_.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final RefFramework refFramework = (RefFramework)elt;
			final RefFrameworkPropertiesEditionPart basePart = (RefFrameworkPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refFramework.getId()));
			
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refFramework.getName()));
			
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.description))
				basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, refFramework.getDescription()));
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.scope))
				basePart.setScope(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, refFramework.getScope()));
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.rev))
				basePart.setRev(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, refFramework.getRev()));
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.purpose))
				basePart.setPurpose(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refFramework.getPurpose()));
			
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.publisher))
				basePart.setPublisher(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refFramework.getPublisher()));
			
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.issued))
				basePart.setIssued(EEFConverterUtil.convertToString(EcorePackage.Literals.EDATE, refFramework.getIssued()));
			
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities)) {
				ownedActivitiesSettings = new ReferencesTableSettings(refFramework, RefframeworkPackage.eINSTANCE.getRefFramework_OwnedActivities());
				basePart.initOwnedActivities(ownedActivitiesSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact)) {
				ownedArtefactSettings = new ReferencesTableSettings(refFramework, RefframeworkPackage.eINSTANCE.getRefFramework_OwnedArtefact());
				basePart.initOwnedArtefact(ownedArtefactSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement)) {
				ownedRequirementSettings = new ReferencesTableSettings(refFramework, RefframeworkPackage.eINSTANCE.getRefFramework_OwnedRequirement());
				basePart.initOwnedRequirement(ownedRequirementSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel)) {
				ownedApplicLevelSettings = new ReferencesTableSettings(refFramework, RefframeworkPackage.eINSTANCE.getRefFramework_OwnedApplicLevel());
				basePart.initOwnedApplicLevel(ownedApplicLevelSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel)) {
				ownedCriticLevelSettings = new ReferencesTableSettings(refFramework, RefframeworkPackage.eINSTANCE.getRefFramework_OwnedCriticLevel());
				basePart.initOwnedCriticLevel(ownedCriticLevelSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedRole)) {
				ownedRoleSettings = new ReferencesTableSettings(refFramework, RefframeworkPackage.eINSTANCE.getRefFramework_OwnedRole());
				basePart.initOwnedRole(ownedRoleSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique)) {
				ownedTechniqueSettings = new ReferencesTableSettings(refFramework, RefframeworkPackage.eINSTANCE.getRefFramework_OwnedTechnique());
				basePart.initOwnedTechnique(ownedTechniqueSettings);
			}
			// init filters
			
			
			
			
			
			
			
			
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities)) {
				basePart.addFilterToOwnedActivities(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefActivity); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedActivities
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact)) {
				basePart.addFilterToOwnedArtefact(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefArtefact); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedArtefact
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement)) {
				basePart.addFilterToOwnedRequirement(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefRequirement); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRequirement
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel)) {
				basePart.addFilterToOwnedApplicLevel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefApplicabilityLevel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedApplicLevel
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel)) {
				basePart.addFilterToOwnedCriticLevel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefCriticalityLevel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedCriticLevel
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedRole)) {
				basePart.addFilterToOwnedRole(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefRole); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRole
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique)) {
				basePart.addFilterToOwnedTechnique(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefTechnique); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedTechnique
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}


















	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.scope) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_Scope();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.rev) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_Rev();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.purpose) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_Purpose();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.publisher) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_Publisher();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.issued) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_Issued();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_OwnedActivities();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_OwnedArtefact();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_OwnedRequirement();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_OwnedApplicLevel();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_OwnedCriticLevel();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.ownedRole) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_OwnedRole();
		}
		if (editorKey == RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique) {
			return RefframeworkPackage.eINSTANCE.getRefFramework_OwnedTechnique();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		RefFramework refFramework = (RefFramework)semanticObject;
		if (RefframeworkViewsRepository.RefFramework_.Properties.id == event.getAffectedEditor()) {
			refFramework.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.name == event.getAffectedEditor()) {
			refFramework.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.description == event.getAffectedEditor()) {
			refFramework.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.scope == event.getAffectedEditor()) {
			refFramework.setScope((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.rev == event.getAffectedEditor()) {
			refFramework.setRev((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.purpose == event.getAffectedEditor()) {
			refFramework.setPurpose((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.publisher == event.getAffectedEditor()) {
			refFramework.setPublisher((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.issued == event.getAffectedEditor()) {
			refFramework.setIssued((java.util.Date)EEFConverterUtil.createFromString(EcorePackage.Literals.EDATE, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedActivitiesSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedActivitiesSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedActivitiesSettings.move(event.getNewIndex(), (RefActivity) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedArtefactSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedArtefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedArtefactSettings.move(event.getNewIndex(), (RefArtefact) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRequirementSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRequirementSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRequirementSettings.move(event.getNewIndex(), (RefRequirement) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedApplicLevelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedApplicLevelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedApplicLevelSettings.move(event.getNewIndex(), (RefApplicabilityLevel) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedCriticLevelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedCriticLevelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedCriticLevelSettings.move(event.getNewIndex(), (RefCriticalityLevel) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.ownedRole == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRoleSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRoleSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRoleSettings.move(event.getNewIndex(), (RefRole) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedTechniqueSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedTechniqueSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedTechniqueSettings.move(event.getNewIndex(), (RefTechnique) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			RefFrameworkPropertiesEditionPart basePart = (RefFrameworkPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.description)){
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefFramework_Scope().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.scope)){
				if (msg.getNewValue() != null) {
					basePart.setScope(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setScope("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefFramework_Rev().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.rev)){
				if (msg.getNewValue() != null) {
					basePart.setRev(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setRev("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefFramework_Purpose().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.purpose)) {
				if (msg.getNewValue() != null) {
					basePart.setPurpose(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setPurpose("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefFramework_Publisher().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.publisher)) {
				if (msg.getNewValue() != null) {
					basePart.setPublisher(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setPublisher("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefFramework_Issued().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.issued)) {
				if (msg.getNewValue() != null) {
					basePart.setIssued(EcoreUtil.convertToString(EcorePackage.Literals.EDATE, msg.getNewValue()));
				} else {
					basePart.setIssued("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefFramework_OwnedActivities().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedActivities))
				basePart.updateOwnedActivities();
			if (RefframeworkPackage.eINSTANCE.getRefFramework_OwnedArtefact().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedArtefact))
				basePart.updateOwnedArtefact();
			if (RefframeworkPackage.eINSTANCE.getRefFramework_OwnedRequirement().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedRequirement))
				basePart.updateOwnedRequirement();
			if (RefframeworkPackage.eINSTANCE.getRefFramework_OwnedApplicLevel().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedApplicLevel))
				basePart.updateOwnedApplicLevel();
			if (RefframeworkPackage.eINSTANCE.getRefFramework_OwnedCriticLevel().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedCriticLevel))
				basePart.updateOwnedCriticLevel();
			if (RefframeworkPackage.eINSTANCE.getRefFramework_OwnedRole().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedRole))
				basePart.updateOwnedRole();
			if (RefframeworkPackage.eINSTANCE.getRefFramework_OwnedTechnique().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefFramework_.Properties.ownedTechnique))
				basePart.updateOwnedTechnique();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			RefframeworkPackage.eINSTANCE.getRefFramework_Scope(),
			RefframeworkPackage.eINSTANCE.getRefFramework_Rev(),
			RefframeworkPackage.eINSTANCE.getRefFramework_Purpose(),
			RefframeworkPackage.eINSTANCE.getRefFramework_Publisher(),
			RefframeworkPackage.eINSTANCE.getRefFramework_Issued(),
			RefframeworkPackage.eINSTANCE.getRefFramework_OwnedActivities(),
			RefframeworkPackage.eINSTANCE.getRefFramework_OwnedArtefact(),
			RefframeworkPackage.eINSTANCE.getRefFramework_OwnedRequirement(),
			RefframeworkPackage.eINSTANCE.getRefFramework_OwnedApplicLevel(),
			RefframeworkPackage.eINSTANCE.getRefFramework_OwnedCriticLevel(),
			RefframeworkPackage.eINSTANCE.getRefFramework_OwnedRole(),
			RefframeworkPackage.eINSTANCE.getRefFramework_OwnedTechnique()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (RefframeworkViewsRepository.RefFramework_.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefFramework_.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefFramework_.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefFramework_.Properties.scope == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefFramework_Scope().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefFramework_Scope().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefFramework_.Properties.rev == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefFramework_Rev().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefFramework_Rev().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefFramework_.Properties.purpose == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefFramework_Purpose().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefFramework_Purpose().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefFramework_.Properties.publisher == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefFramework_Publisher().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefFramework_Publisher().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefFramework_.Properties.issued == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefFramework_Issued().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefFramework_Issued().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
