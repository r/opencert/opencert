/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.listeners.util;

import org.eclipse.opencert.webapp.reports.containers.BaseAssetComplianceStatus;

public class SelectionEvent
{
    private long cdoId;
    private String baselineElementType;
    private String baselineElementName;
    private BaseAssetComplianceStatus baseAssetComplianceStatus;

    private String complianceType;
    private int numberOfFullyAssets;
    private int numberOfPartialAssets;

    private Integer numberOfAssets;
    
    private Integer numberOfBaseAssets;

    private ClickType clickType;

    public SelectionEvent(ClickType clickType)
    {
        this.clickType = clickType;
    }
    
    public SelectionEvent(ClickType clickType, int numberOfBaseAssets)
    {
        this.clickType = clickType;
        this.numberOfBaseAssets = numberOfBaseAssets;
    }

    public SelectionEvent(ClickType clickType, long cdoId, String baselineElementType, String baselineElementName,
            BaseAssetComplianceStatus baseAssetComplianceStatus)
    {
        this.clickType = clickType;
        this.cdoId = cdoId;
        this.baselineElementType = baselineElementType;
        this.baselineElementName = baselineElementName;
        this.baseAssetComplianceStatus = baseAssetComplianceStatus;
    }

    public SelectionEvent(ClickType clickType, long cdoId, String baselineElementType, String complianceType, int numberOfFullyAssets, int numberOfPartialAssets)
    {
        this.clickType = clickType;
        this.cdoId = cdoId;
        this.baselineElementType = baselineElementType;
        this.complianceType = complianceType;
        this.numberOfFullyAssets = numberOfFullyAssets;
        this.numberOfPartialAssets = numberOfPartialAssets;
    }

    public SelectionEvent(ClickType clickType, long cdoId, String baselineElementType, String complianceType, int numberOfAssets)
    {
        this.clickType = clickType;
        this.cdoId = cdoId;
        this.baselineElementType = baselineElementType;
        this.complianceType = complianceType;
        this.numberOfAssets = numberOfAssets;
    }

    public long getCdoId()
    {
        return cdoId;
    }

    public String getBaselineElementType()
    {
        return baselineElementType;
    }

    public String getBaselineElementName()
    {
        return baselineElementName;
    }

    public BaseAssetComplianceStatus getBaseAssetComplianceStatus()
    {
        return baseAssetComplianceStatus;
    }

    public String getComplianceType()
    {
        return complianceType;
    }

    public int getNumberOfFullyAssets()
    {
        return numberOfFullyAssets;
    }

    public int getNumberOfPartialAssets()
    {
        return numberOfPartialAssets;
    }

    public Integer getNumberOfAssets()
    {
        return numberOfAssets;
    }
    
    public Integer getNumberOfBaseAssets()
    {
        return numberOfBaseAssets;
    }

    public ClickType getClickType()
    {
        return clickType;
    }

    @Override
    public String toString()
    {
        return "SelectionEvent [cdoId=" + cdoId + ", baselineElementType=" + baselineElementType + ", baselineElementName=" + baselineElementName
                + ", baseAssetComplianceStatus=" + baseAssetComplianceStatus + ", complianceType=" + complianceType + ", numberOfFullyAssets="
                + numberOfFullyAssets + ", numberOfPartialAssets=" + numberOfPartialAssets + ", numberOfAssets=" + numberOfAssets + ", clickType=" + clickType
                + "]";
    }
}
