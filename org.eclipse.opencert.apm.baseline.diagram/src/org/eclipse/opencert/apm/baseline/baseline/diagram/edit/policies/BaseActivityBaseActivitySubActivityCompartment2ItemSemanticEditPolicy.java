/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands.BaseActivity2CreateCommand;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes;

/**
 * @generated
 */
public class BaseActivityBaseActivitySubActivityCompartment2ItemSemanticEditPolicy
		extends BaselineBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public BaseActivityBaseActivitySubActivityCompartment2ItemSemanticEditPolicy() {
		super(BaselineElementTypes.BaseActivity_3001);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (BaselineElementTypes.BaseActivity_3001 == req.getElementType()) {
			return getGEFWrapper(new BaseActivity2CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
