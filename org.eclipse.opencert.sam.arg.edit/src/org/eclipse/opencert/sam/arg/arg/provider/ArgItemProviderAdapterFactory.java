/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.provider;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ArgItemProviderAdapterFactory extends ArgAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.Case} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaseItemProvider caseItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.Case}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCaseAdapter() {
		if (caseItemProvider == null) {
			caseItemProvider = new CaseItemProvider(this);
		}

		return caseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.AssuranceCase} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssuranceCaseItemProvider assuranceCaseItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.AssuranceCase}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssuranceCaseAdapter() {
		if (assuranceCaseItemProvider == null) {
			assuranceCaseItemProvider = new AssuranceCaseItemProvider(this);
		}

		return assuranceCaseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.Argumentation} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArgumentationItemProvider argumentationItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.Argumentation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createArgumentationAdapter() {
		if (argumentationItemProvider == null) {
			argumentationItemProvider = new ArgumentationItemProvider(this);
		}

		return argumentationItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InformationElementCitationItemProvider informationElementCitationItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createInformationElementCitationAdapter() {
		if (informationElementCitationItemProvider == null) {
			informationElementCitationItemProvider = new InformationElementCitationItemProvider(this);
		}

		return informationElementCitationItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArgumentElementCitationItemProvider argumentElementCitationItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createArgumentElementCitationAdapter() {
		if (argumentElementCitationItemProvider == null) {
			argumentElementCitationItemProvider = new ArgumentElementCitationItemProvider(this);
		}

		return argumentElementCitationItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArgumentReasoningItemProvider argumentReasoningItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createArgumentReasoningAdapter() {
		if (argumentReasoningItemProvider == null) {
			argumentReasoningItemProvider = new ArgumentReasoningItemProvider(this);
		}

		return argumentReasoningItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.Claim} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClaimItemProvider claimItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.Claim}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createClaimAdapter() {
		if (claimItemProvider == null) {
			claimItemProvider = new ClaimItemProvider(this);
		}

		return claimItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.Choice} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChoiceItemProvider choiceItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.Choice}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createChoiceAdapter() {
		if (choiceItemProvider == null) {
			choiceItemProvider = new ChoiceItemProvider(this);
		}

		return choiceItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.AssertedInference} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertedInferenceItemProvider assertedInferenceItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.AssertedInference}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssertedInferenceAdapter() {
		if (assertedInferenceItemProvider == null) {
			assertedInferenceItemProvider = new AssertedInferenceItemProvider(this);
		}

		return assertedInferenceItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.AssertedEvidence} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertedEvidenceItemProvider assertedEvidenceItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.AssertedEvidence}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssertedEvidenceAdapter() {
		if (assertedEvidenceItemProvider == null) {
			assertedEvidenceItemProvider = new AssertedEvidenceItemProvider(this);
		}

		return assertedEvidenceItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.AssertedContext} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertedContextItemProvider assertedContextItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.AssertedContext}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssertedContextAdapter() {
		if (assertedContextItemProvider == null) {
			assertedContextItemProvider = new AssertedContextItemProvider(this);
		}

		return assertedContextItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.AssertedChallenge} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertedChallengeItemProvider assertedChallengeItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.AssertedChallenge}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssertedChallengeAdapter() {
		if (assertedChallengeItemProvider == null) {
			assertedChallengeItemProvider = new AssertedChallengeItemProvider(this);
		}

		return assertedChallengeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertedCounterEvidenceItemProvider assertedCounterEvidenceItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssertedCounterEvidenceAdapter() {
		if (assertedCounterEvidenceItemProvider == null) {
			assertedCounterEvidenceItemProvider = new AssertedCounterEvidenceItemProvider(this);
		}

		return assertedCounterEvidenceItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.eclipse.opencert.sam.arg.arg.Agreement} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgreementItemProvider agreementItemProvider;

	/**
	 * This creates an adapter for a {@link org.eclipse.opencert.sam.arg.arg.Agreement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAgreementAdapter() {
		if (agreementItemProvider == null) {
			agreementItemProvider = new AgreementItemProvider(this);
		}

		return agreementItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>)type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (caseItemProvider != null) caseItemProvider.dispose();
		if (assuranceCaseItemProvider != null) assuranceCaseItemProvider.dispose();
		if (argumentationItemProvider != null) argumentationItemProvider.dispose();
		if (informationElementCitationItemProvider != null) informationElementCitationItemProvider.dispose();
		if (argumentElementCitationItemProvider != null) argumentElementCitationItemProvider.dispose();
		if (argumentReasoningItemProvider != null) argumentReasoningItemProvider.dispose();
		if (claimItemProvider != null) claimItemProvider.dispose();
		if (choiceItemProvider != null) choiceItemProvider.dispose();
		if (assertedInferenceItemProvider != null) assertedInferenceItemProvider.dispose();
		if (assertedEvidenceItemProvider != null) assertedEvidenceItemProvider.dispose();
		if (assertedContextItemProvider != null) assertedContextItemProvider.dispose();
		if (assertedChallengeItemProvider != null) assertedChallengeItemProvider.dispose();
		if (assertedCounterEvidenceItemProvider != null) assertedCounterEvidenceItemProvider.dispose();
		if (agreementItemProvider != null) agreementItemProvider.dispose();
	}

}
