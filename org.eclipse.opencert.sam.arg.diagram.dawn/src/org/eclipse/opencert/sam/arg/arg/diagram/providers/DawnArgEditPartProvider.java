/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.providers;

import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.DawnArgEditPartFactory;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorPlugin;

public class DawnArgEditPartProvider extends ArgEditPartProvider {

	public DawnArgEditPartProvider() {
		super();
		setFactory(new DawnArgEditPartFactory());
	}
}
