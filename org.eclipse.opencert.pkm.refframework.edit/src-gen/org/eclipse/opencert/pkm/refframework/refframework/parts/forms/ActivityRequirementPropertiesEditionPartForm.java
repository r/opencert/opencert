/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityRequirementPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;

import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;

// End of user code

/**
 * 
 * 
 */
public class ActivityRequirementPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ActivityRequirementPropertiesEditionPart {

	protected ReferencesTable ownedRequirement;
	protected List<ViewerFilter> ownedRequirementBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRequirementFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public ActivityRequirementPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ActivityRequirementPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence activityRequirementStep = new BindingCompositionSequence(propertiesEditionComponent);
		activityRequirementStep
			.addStep(RefframeworkViewsRepository.ActivityRequirement.Properties.class)
			.addStep(RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement);
		
		
		composer = new PartComposer(activityRequirementStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.ActivityRequirement.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement) {
					return createOwnedRequirementTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(RefframeworkMessages.ActivityRequirementPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRequirementTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedRequirement = new ReferencesTable(getDescription(RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement, RefframeworkMessages.ActivityRequirementPropertiesEditionPart_OwnedRequirementLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityRequirementPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRequirement.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityRequirementPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRequirement.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityRequirementPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRequirement.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityRequirementPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRequirement.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRequirementFilters) {
			this.ownedRequirement.addFilter(filter);
		}
		this.ownedRequirement.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement, RefframeworkViewsRepository.FORM_KIND));
		this.ownedRequirement.createControls(parent, widgetFactory);
		this.ownedRequirement.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityRequirementPropertiesEditionPartForm.this, RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRequirementData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRequirementData.horizontalSpan = 3;
		this.ownedRequirement.setLayoutData(ownedRequirementData);
		this.ownedRequirement.setLowerBound(0);
		this.ownedRequirement.setUpperBound(-1);
		ownedRequirement.setID(RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement);
		ownedRequirement.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRequirementTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityRequirementPropertiesEditionPart#initOwnedRequirement(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRequirement(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRequirement.setContentProvider(contentProvider);
		ownedRequirement.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.ActivityRequirement.Properties.ownedRequirement);
		if (eefElementEditorReadOnlyState && ownedRequirement.isEnabled()) {
			ownedRequirement.setEnabled(false);
			ownedRequirement.setToolTipText(RefframeworkMessages.ActivityRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRequirement.isEnabled()) {
			ownedRequirement.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityRequirementPropertiesEditionPart#updateOwnedRequirement()
	 * 
	 */
	public void updateOwnedRequirement() {
	ownedRequirement.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityRequirementPropertiesEditionPart#addFilterOwnedRequirement(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRequirement(ViewerFilter filter) {
		ownedRequirementFilters.add(filter);
		if (this.ownedRequirement != null) {
			this.ownedRequirement.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityRequirementPropertiesEditionPart#addBusinessFilterOwnedRequirement(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRequirement(ViewerFilter filter) {
		ownedRequirementBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ActivityRequirementPropertiesEditionPart#isContainedInOwnedRequirementTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRequirementTable(EObject element) {
		return ((ReferencesTableSettings)ownedRequirement.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.ActivityRequirement_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
