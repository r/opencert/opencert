/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.ResizableCompartmentEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateUnspecifiedTypeConnectionRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.edit.policies.reparent.CreationEditPolicyWithCustomReparent;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands.LoadViewLocations;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.BaseActivityBaseActivitySubActivityCompartmentCanonicalEditPolicy;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.BaseActivityBaseActivitySubActivityCompartmentItemSemanticEditPolicy;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.BaseActivityBaseActivitySubActivityCompartmentRepairViewCanonicalEditPolicy;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.Messages;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes;

/**
 * @generated
 */
public class BaseActivityBaseActivitySubActivityCompartmentEditPart extends
		ShapeCompartmentEditPart {

	// MCP Start para multdiagrama y heredar posiciones del refframewoerk
	/**
	 * @generated NOT
	 */
	public static int NOT_FROM_REFFRAMEWORK = 1;
	public static LoadViewLocations sourceViewLocations = null;	
	// MCP End
	
	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 7001;

	/**
	 * @generated
	 */
	public BaseActivityBaseActivitySubActivityCompartmentEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	public String getCompartmentName() {
		return Messages.BaseActivityBaseActivitySubActivityCompartmentEditPart_title;
	}

	/**
	 * @generated
	 */
	public IFigure createFigure() {
		ResizableCompartmentFigure result = (ResizableCompartmentFigure) super
				.createFigure();
		result.setTitleVisibility(false);
		return result;
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				new ResizableCompartmentEditPolicy());
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new BaseActivityBaseActivitySubActivityCompartmentItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicyWithCustomReparent(
						BaselineVisualIDRegistry.TYPED_INSTANCE));
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE,
				new DragDropEditPolicy());
		// MCP Start
		if(NOT_FROM_REFFRAMEWORK == 1)
		{
		installEditPolicy(
				EditPolicyRoles.CANONICAL_ROLE,
				new BaseActivityBaseActivitySubActivityCompartmentCanonicalEditPolicy());
		}
		else
		{
			BaseActivityBaseActivitySubActivityCompartmentRepairViewCanonicalEditPolicy cp = new BaseActivityBaseActivitySubActivityCompartmentRepairViewCanonicalEditPolicy();
			cp.setSourceDiagram(sourceViewLocations);
			installEditPolicy(
					EditPolicyRoles.CANONICAL_ROLE,
					cp);
		}
		// MCP End
	}

	/**
	 * @generated
	 */
	protected void setRatio(Double ratio) {
		if (getFigure().getParent().getLayoutManager() instanceof ConstrainedToolbarLayout) {
			super.setRatio(ratio);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getTargetEditPart(Request request) {
		if (request instanceof CreateViewAndElementRequest) {
			CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request)
					.getViewAndElementDescriptor()
					.getCreateElementRequestAdapter();
			IElementType type = (IElementType) adapter
					.getAdapter(IElementType.class);
			if (type == BaselineElementTypes.BaseActivity_3001) {
				return this;
			}
			return getParent().getTargetEditPart(request);
		}
		if (request instanceof CreateUnspecifiedTypeConnectionRequest) {
			if (RequestConstants.REQ_CONNECTION_END.equals(request.getType())) {
				for (Object type : ((CreateUnspecifiedTypeConnectionRequest) request)
						.getElementTypes()) {
					if (type instanceof IElementType) {
						IElementType elementType = (IElementType) type;
						if (elementType
								.equals(BaselineElementTypes.BaseActivityPrecedingActivity_4003))
							return super.getTargetEditPart(request);
					}
				}
			}
			return getParent().getTargetEditPart(request);
		}
		return super.getTargetEditPart(request);
	}

}
