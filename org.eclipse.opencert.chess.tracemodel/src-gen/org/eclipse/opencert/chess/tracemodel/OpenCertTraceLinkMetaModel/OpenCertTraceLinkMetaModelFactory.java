/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage
 * @generated
 */
public interface OpenCertTraceLinkMetaModelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OpenCertTraceLinkMetaModelFactory eINSTANCE = org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.OpenCertTraceLinkMetaModelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Contract Claim Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Contract Claim Link</em>'.
	 * @generated
	 */
	ContractClaimLink createContractClaimLink();

	/**
	 * Returns a new object of class '<em>Contract Artefact Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Contract Artefact Link</em>'.
	 * @generated
	 */
	ContractArtefactLink createContractArtefactLink();

	/**
	 * Returns a new object of class '<em>Formal Property Claim Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Property Claim Link</em>'.
	 * @generated
	 */
	FormalPropertyClaimLink createFormalPropertyClaimLink();

	/**
	 * Returns a new object of class '<em>Contract Agreement Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Contract Agreement Link</em>'.
	 * @generated
	 */
	ContractAgreementLink createContractAgreementLink();

	/**
	 * Returns a new object of class '<em>Analysis Context Artefact Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Analysis Context Artefact Link</em>'.
	 * @generated
	 */
	AnalysisContextArtefactLink createAnalysisContextArtefactLink();

	/**
	 * Returns a new object of class '<em>Component Argumentation Element Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Argumentation Element Link</em>'.
	 * @generated
	 */
	ComponentArgumentationElementLink createComponentArgumentationElementLink();

	/**
	 * Returns a new object of class '<em>Requirement Claim Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requirement Claim Link</em>'.
	 * @generated
	 */
	RequirementClaimLink createRequirementClaimLink();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OpenCertTraceLinkMetaModelPackage getOpenCertTraceLinkMetaModelPackage();

} //OpenCertTraceLinkMetaModelFactory
