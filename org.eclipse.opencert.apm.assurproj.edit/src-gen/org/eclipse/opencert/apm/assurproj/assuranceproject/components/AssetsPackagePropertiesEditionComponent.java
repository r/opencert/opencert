/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.components;

// Start of user code for imports
import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Case;


// End of user code

/**
 * 
 * 
 */
public class AssetsPackagePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for artefactsModel ReferencesTable
	 */
	private ReferencesTableSettings artefactsModelSettings;
	
	/**
	 * Settings for argumentationModel ReferencesTable
	 */
	private ReferencesTableSettings argumentationModelSettings;
	
	/**
	 * Settings for processModel ReferencesTable
	 */
	private ReferencesTableSettings processModelSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public AssetsPackagePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject assetsPackage, String editing_mode) {
		super(editingContext, assetsPackage, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = AssuranceprojectViewsRepository.class;
		partKey = AssuranceprojectViewsRepository.AssetsPackage.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final AssetsPackage assetsPackage = (AssetsPackage)elt;
			final AssetsPackagePropertiesEditionPart basePart = (AssetsPackagePropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assetsPackage.getId()));
			
			if (isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assetsPackage.getName()));
			
			if (isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assetsPackage.getDescription()));
			
			if (isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive)) {
				basePart.setIsActive(assetsPackage.isIsActive());
			}
			if (isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel)) {
				artefactsModelSettings = new ReferencesTableSettings(assetsPackage, AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ArtefactsModel());
				basePart.initArtefactsModel(artefactsModelSettings);
			}
			if (isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel)) {
				argumentationModelSettings = new ReferencesTableSettings(assetsPackage, AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ArgumentationModel());
				basePart.initArgumentationModel(argumentationModelSettings);
			}
			if (isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel)) {
				processModelSettings = new ReferencesTableSettings(assetsPackage, AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ProcessModel());
				basePart.initProcessModel(processModelSettings);
			}
			// init filters
			
			
			
			
			if (isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel)) {
				basePart.addFilterToArtefactsModel(new EObjectFilter(EvidencePackage.Literals.ARTEFACT_MODEL));
				// Start of user code for additional businessfilters for artefactsModel
				// End of user code
			}
			if (isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel)) {
				basePart.addFilterToArgumentationModel(new EObjectFilter(ArgPackage.Literals.CASE));
				// Start of user code for additional businessfilters for argumentationModel
				// End of user code
			}
			if (isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel)) {
				basePart.addFilterToProcessModel(new EObjectFilter(ProcessPackage.Literals.PROCESS_MODEL));
				// Start of user code for additional businessfilters for processModel
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}










	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == AssuranceprojectViewsRepository.AssetsPackage.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssetsPackage.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssetsPackage.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive) {
			return AssuranceprojectPackage.eINSTANCE.getAssetsPackage_IsActive();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel) {
			return AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ArtefactsModel();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel) {
			return AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ArgumentationModel();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel) {
			return AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ProcessModel();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		AssetsPackage assetsPackage = (AssetsPackage)semanticObject;
		if (AssuranceprojectViewsRepository.AssetsPackage.Properties.id == event.getAffectedEditor()) {
			assetsPackage.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.AssetsPackage.Properties.name == event.getAffectedEditor()) {
			assetsPackage.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.AssetsPackage.Properties.description == event.getAffectedEditor()) {
			assetsPackage.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive == event.getAffectedEditor()) {
			
			//Start IRR

			if ((Boolean)event.getNewValue()) {
				AssuranceProject assProject =(AssuranceProject)assetsPackage.eContainer();
				EList<AssetsPackage> allAssets = assProject.getAssetsPackage();
				Iterator<AssetsPackage> iter = allAssets.iterator();

				while (iter.hasNext()) {
					AssetsPackage oneAsset = (AssetsPackage) iter.next();
					oneAsset.setIsActive(false);
				}
			}

			//End IRR
			
			assetsPackage.setIsActive((Boolean)event.getNewValue());
		}
		if (AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof ArtefactModel) {
					artefactsModelSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				artefactsModelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				artefactsModelSettings.move(event.getNewIndex(), (ArtefactModel) event.getNewValue());
			}
		}
		if (AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Case) {
					argumentationModelSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				argumentationModelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				argumentationModelSettings.move(event.getNewIndex(), (Case) event.getNewValue());
			}
		}
		if (AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof ProcessModel) {
					processModelSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				processModelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				processModelSettings.move(event.getNewIndex(), (ProcessModel) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			AssetsPackagePropertiesEditionPart basePart = (AssetsPackagePropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (AssuranceprojectPackage.eINSTANCE.getAssetsPackage_IsActive().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive))
				basePart.setIsActive((Boolean)msg.getNewValue());
			
			if (AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ArtefactsModel().equals(msg.getFeature())  && isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel))
				basePart.updateArtefactsModel();
			if (AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ArgumentationModel().equals(msg.getFeature())  && isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel))
				basePart.updateArgumentationModel();
			if (AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ProcessModel().equals(msg.getFeature())  && isAccessible(AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel))
				basePart.updateProcessModel();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			AssuranceprojectPackage.eINSTANCE.getAssetsPackage_IsActive(),
			AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ArtefactsModel(),
			AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ArgumentationModel(),
			AssuranceprojectPackage.eINSTANCE.getAssetsPackage_ProcessModel()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (AssuranceprojectViewsRepository.AssetsPackage.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.AssetsPackage.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.AssetsPackage.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(AssuranceprojectPackage.eINSTANCE.getAssetsPackage_IsActive().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(AssuranceprojectPackage.eINSTANCE.getAssetsPackage_IsActive().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
