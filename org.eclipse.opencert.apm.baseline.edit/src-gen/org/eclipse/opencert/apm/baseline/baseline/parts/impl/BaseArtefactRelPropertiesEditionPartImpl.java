/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import org.eclipse.emf.common.util.Enumerator;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;

import org.eclipse.emf.eef.runtime.EEFRuntimePlugin;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;

// End of user code

/**
 * 
 * 
 */
public class BaseArtefactRelPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseArtefactRelPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text maxMultiplicitySource;
	protected Text minMultiplicitySource;
	protected Text maxMultiplicityTarget;
	protected Text minMultiplicityTarget;
	protected EMFComboViewer modificationEffect;
	protected EMFComboViewer revocationEffect;
	protected EObjectFlatComboViewer source;
	protected EObjectFlatComboViewer target;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseArtefactRelPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseArtefactRelStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseArtefactRelStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.class);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.id);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.name);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.description);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.source);
		propertiesStep.addStep(BaselineViewsRepository.BaseArtefactRel.Properties.target);
		
		
		composer = new PartComposer(baseArtefactRelStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.id) {
					return createIdText(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.name) {
					return createNameText(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.description) {
					return createDescriptionTextarea(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource) {
					return createMaxMultiplicitySourceText(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource) {
					return createMinMultiplicitySourceText(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget) {
					return createMaxMultiplicityTargetText(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget) {
					return createMinMultiplicityTargetText(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect) {
					return createModificationEffectEMFComboViewer(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect) {
					return createRevocationEffectEMFComboViewer(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.source) {
					return createSourceFlatComboViewer(parent);
				}
				if (key == BaselineViewsRepository.BaseArtefactRel.Properties.target) {
					return createTargetFlatComboViewer(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseArtefactRelPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.id, BaselineMessages.BaseArtefactRelPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, BaselineViewsRepository.BaseArtefactRel.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.id, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.name, BaselineMessages.BaseArtefactRelPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, BaselineViewsRepository.BaseArtefactRel.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.name, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionTextarea(Composite parent) {
		Label descriptionLabel = createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.description, BaselineMessages.BaseArtefactRelPropertiesEditionPart_DescriptionLabel);
		GridData descriptionLabelData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionLabelData.horizontalSpan = 3;
		descriptionLabel.setLayoutData(descriptionLabelData);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionData.horizontalSpan = 2;
		descriptionData.heightHint = 80;
		descriptionData.widthHint = 200;
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		EditingUtils.setID(description, BaselineViewsRepository.BaseArtefactRel.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Textarea"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.description, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createMaxMultiplicitySourceText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource, BaselineMessages.BaseArtefactRelPropertiesEditionPart_MaxMultiplicitySourceLabel);
		maxMultiplicitySource = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData maxMultiplicitySourceData = new GridData(GridData.FILL_HORIZONTAL);
		maxMultiplicitySource.setLayoutData(maxMultiplicitySourceData);
		maxMultiplicitySource.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, maxMultiplicitySource.getText()));
			}

		});
		maxMultiplicitySource.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, maxMultiplicitySource.getText()));
				}
			}

		});
		EditingUtils.setID(maxMultiplicitySource, BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource);
		EditingUtils.setEEFtype(maxMultiplicitySource, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createMaxMultiplicitySourceText

		// End of user code
		return parent;
	}

	
	protected Composite createMinMultiplicitySourceText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource, BaselineMessages.BaseArtefactRelPropertiesEditionPart_MinMultiplicitySourceLabel);
		minMultiplicitySource = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData minMultiplicitySourceData = new GridData(GridData.FILL_HORIZONTAL);
		minMultiplicitySource.setLayoutData(minMultiplicitySourceData);
		minMultiplicitySource.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, minMultiplicitySource.getText()));
			}

		});
		minMultiplicitySource.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, minMultiplicitySource.getText()));
				}
			}

		});
		EditingUtils.setID(minMultiplicitySource, BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource);
		EditingUtils.setEEFtype(minMultiplicitySource, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createMinMultiplicitySourceText

		// End of user code
		return parent;
	}

	
	protected Composite createMaxMultiplicityTargetText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget, BaselineMessages.BaseArtefactRelPropertiesEditionPart_MaxMultiplicityTargetLabel);
		maxMultiplicityTarget = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData maxMultiplicityTargetData = new GridData(GridData.FILL_HORIZONTAL);
		maxMultiplicityTarget.setLayoutData(maxMultiplicityTargetData);
		maxMultiplicityTarget.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, maxMultiplicityTarget.getText()));
			}

		});
		maxMultiplicityTarget.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, maxMultiplicityTarget.getText()));
				}
			}

		});
		EditingUtils.setID(maxMultiplicityTarget, BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget);
		EditingUtils.setEEFtype(maxMultiplicityTarget, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createMaxMultiplicityTargetText

		// End of user code
		return parent;
	}

	
	protected Composite createMinMultiplicityTargetText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget, BaselineMessages.BaseArtefactRelPropertiesEditionPart_MinMultiplicityTargetLabel);
		minMultiplicityTarget = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData minMultiplicityTargetData = new GridData(GridData.FILL_HORIZONTAL);
		minMultiplicityTarget.setLayoutData(minMultiplicityTargetData);
		minMultiplicityTarget.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, minMultiplicityTarget.getText()));
			}

		});
		minMultiplicityTarget.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, minMultiplicityTarget.getText()));
				}
			}

		});
		EditingUtils.setID(minMultiplicityTarget, BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget);
		EditingUtils.setEEFtype(minMultiplicityTarget, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createMinMultiplicityTargetText

		// End of user code
		return parent;
	}

	
	protected Composite createModificationEffectEMFComboViewer(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect, BaselineMessages.BaseArtefactRelPropertiesEditionPart_ModificationEffectLabel);
		modificationEffect = new EMFComboViewer(parent);
		modificationEffect.setContentProvider(new ArrayContentProvider());
		modificationEffect.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData modificationEffectData = new GridData(GridData.FILL_HORIZONTAL);
		modificationEffect.getCombo().setLayoutData(modificationEffectData);
		modificationEffect.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getModificationEffect()));
			}

		});
		modificationEffect.setID(BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createModificationEffectEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createRevocationEffectEMFComboViewer(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect, BaselineMessages.BaseArtefactRelPropertiesEditionPart_RevocationEffectLabel);
		revocationEffect = new EMFComboViewer(parent);
		revocationEffect.setContentProvider(new ArrayContentProvider());
		revocationEffect.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData revocationEffectData = new GridData(GridData.FILL_HORIZONTAL);
		revocationEffect.getCombo().setLayoutData(revocationEffectData);
		revocationEffect.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getRevocationEffect()));
			}

		});
		revocationEffect.setID(BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createRevocationEffectEMFComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createSourceFlatComboViewer(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.source, BaselineMessages.BaseArtefactRelPropertiesEditionPart_SourceLabel);
		source = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(BaselineViewsRepository.BaseArtefactRel.Properties.source, BaselineViewsRepository.SWT_KIND));
		source.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		source.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.source, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getSource()));
			}

		});
		GridData sourceData = new GridData(GridData.FILL_HORIZONTAL);
		source.setLayoutData(sourceData);
		source.setID(BaselineViewsRepository.BaseArtefactRel.Properties.source);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.source, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createSourceFlatComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createTargetFlatComboViewer(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseArtefactRel.Properties.target, BaselineMessages.BaseArtefactRelPropertiesEditionPart_TargetLabel);
		target = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(BaselineViewsRepository.BaseArtefactRel.Properties.target, BaselineViewsRepository.SWT_KIND));
		target.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		target.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseArtefactRelPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseArtefactRel.Properties.target, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getTarget()));
			}

		});
		GridData targetData = new GridData(GridData.FILL_HORIZONTAL);
		target.setLayoutData(targetData);
		target.setID(BaselineViewsRepository.BaseArtefactRel.Properties.target);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseArtefactRel.Properties.target, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createTargetFlatComboViewer

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setBackground(description.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			description.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getMaxMultiplicitySource()
	 * 
	 */
	public String getMaxMultiplicitySource() {
		return maxMultiplicitySource.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setMaxMultiplicitySource(String newValue)
	 * 
	 */
	public void setMaxMultiplicitySource(String newValue) {
		if (newValue != null) {
			maxMultiplicitySource.setText(newValue);
		} else {
			maxMultiplicitySource.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicitySource);
		if (eefElementEditorReadOnlyState && maxMultiplicitySource.isEnabled()) {
			maxMultiplicitySource.setEnabled(false);
			maxMultiplicitySource.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !maxMultiplicitySource.isEnabled()) {
			maxMultiplicitySource.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getMinMultiplicitySource()
	 * 
	 */
	public String getMinMultiplicitySource() {
		return minMultiplicitySource.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setMinMultiplicitySource(String newValue)
	 * 
	 */
	public void setMinMultiplicitySource(String newValue) {
		if (newValue != null) {
			minMultiplicitySource.setText(newValue);
		} else {
			minMultiplicitySource.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicitySource);
		if (eefElementEditorReadOnlyState && minMultiplicitySource.isEnabled()) {
			minMultiplicitySource.setEnabled(false);
			minMultiplicitySource.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !minMultiplicitySource.isEnabled()) {
			minMultiplicitySource.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getMaxMultiplicityTarget()
	 * 
	 */
	public String getMaxMultiplicityTarget() {
		return maxMultiplicityTarget.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setMaxMultiplicityTarget(String newValue)
	 * 
	 */
	public void setMaxMultiplicityTarget(String newValue) {
		if (newValue != null) {
			maxMultiplicityTarget.setText(newValue);
		} else {
			maxMultiplicityTarget.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.maxMultiplicityTarget);
		if (eefElementEditorReadOnlyState && maxMultiplicityTarget.isEnabled()) {
			maxMultiplicityTarget.setEnabled(false);
			maxMultiplicityTarget.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !maxMultiplicityTarget.isEnabled()) {
			maxMultiplicityTarget.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getMinMultiplicityTarget()
	 * 
	 */
	public String getMinMultiplicityTarget() {
		return minMultiplicityTarget.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setMinMultiplicityTarget(String newValue)
	 * 
	 */
	public void setMinMultiplicityTarget(String newValue) {
		if (newValue != null) {
			minMultiplicityTarget.setText(newValue);
		} else {
			minMultiplicityTarget.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.minMultiplicityTarget);
		if (eefElementEditorReadOnlyState && minMultiplicityTarget.isEnabled()) {
			minMultiplicityTarget.setEnabled(false);
			minMultiplicityTarget.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !minMultiplicityTarget.isEnabled()) {
			minMultiplicityTarget.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getModificationEffect()
	 * 
	 */
	public Enumerator getModificationEffect() {
		Enumerator selection = (Enumerator) ((StructuredSelection) modificationEffect.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#initModificationEffect(Object input, Enumerator current)
	 */
	public void initModificationEffect(Object input, Enumerator current) {
		modificationEffect.setInput(input);
		modificationEffect.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect);
		if (eefElementEditorReadOnlyState && modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(false);
			modificationEffect.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setModificationEffect(Enumerator newValue)
	 * 
	 */
	public void setModificationEffect(Enumerator newValue) {
		modificationEffect.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.modificationEffect);
		if (eefElementEditorReadOnlyState && modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(false);
			modificationEffect.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getRevocationEffect()
	 * 
	 */
	public Enumerator getRevocationEffect() {
		Enumerator selection = (Enumerator) ((StructuredSelection) revocationEffect.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#initRevocationEffect(Object input, Enumerator current)
	 */
	public void initRevocationEffect(Object input, Enumerator current) {
		revocationEffect.setInput(input);
		revocationEffect.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect);
		if (eefElementEditorReadOnlyState && revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(false);
			revocationEffect.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setRevocationEffect(Enumerator newValue)
	 * 
	 */
	public void setRevocationEffect(Enumerator newValue) {
		revocationEffect.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.revocationEffect);
		if (eefElementEditorReadOnlyState && revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(false);
			revocationEffect.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getSource()
	 * 
	 */
	public EObject getSource() {
		if (source.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) source.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#initSource(EObjectFlatComboSettings)
	 */
	public void initSource(EObjectFlatComboSettings settings) {
		source.setInput(settings);
		if (current != null) {
			source.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.source);
		if (eefElementEditorReadOnlyState && source.isEnabled()) {
			source.setEnabled(false);
			source.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !source.isEnabled()) {
			source.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setSource(EObject newValue)
	 * 
	 */
	public void setSource(EObject newValue) {
		if (newValue != null) {
			source.setSelection(new StructuredSelection(newValue));
		} else {
			source.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.source);
		if (eefElementEditorReadOnlyState && source.isEnabled()) {
			source.setEnabled(false);
			source.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !source.isEnabled()) {
			source.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setSourceButtonMode(ButtonsModeEnum newValue)
	 */
	public void setSourceButtonMode(ButtonsModeEnum newValue) {
		source.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#addFilterSource(ViewerFilter filter)
	 * 
	 */
	public void addFilterToSource(ViewerFilter filter) {
		source.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#addBusinessFilterSource(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToSource(ViewerFilter filter) {
		source.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#getTarget()
	 * 
	 */
	public EObject getTarget() {
		if (target.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) target.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#initTarget(EObjectFlatComboSettings)
	 */
	public void initTarget(EObjectFlatComboSettings settings) {
		target.setInput(settings);
		if (current != null) {
			target.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.target);
		if (eefElementEditorReadOnlyState && target.isEnabled()) {
			target.setEnabled(false);
			target.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !target.isEnabled()) {
			target.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setTarget(EObject newValue)
	 * 
	 */
	public void setTarget(EObject newValue) {
		if (newValue != null) {
			target.setSelection(new StructuredSelection(newValue));
		} else {
			target.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseArtefactRel.Properties.target);
		if (eefElementEditorReadOnlyState && target.isEnabled()) {
			target.setEnabled(false);
			target.setToolTipText(BaselineMessages.BaseArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !target.isEnabled()) {
			target.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#setTargetButtonMode(ButtonsModeEnum newValue)
	 */
	public void setTargetButtonMode(ButtonsModeEnum newValue) {
		target.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#addFilterTarget(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTarget(ViewerFilter filter) {
		target.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactRelPropertiesEditionPart#addBusinessFilterTarget(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTarget(ViewerFilter filter) {
		target.addBusinessRuleFilter(filter);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseArtefactRel_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
