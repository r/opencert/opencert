/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;
import org.eclipse.opencert.pam.procspec.process.Activity;

// End of user code

/**
 * 
 * 
 */
public class BaseActivityComplianceMapPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseActivityComplianceMapPropertiesEditionPart {

	protected ReferencesTable complianceMap;
	protected List<ViewerFilter> complianceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> complianceMapFilters = new ArrayList<ViewerFilter>();
	protected TableViewer activityComplianceMap;
	protected List<ViewerFilter> activityComplianceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> activityComplianceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addActivityComplianceMap;
	protected Button removeActivityComplianceMap;
	protected Button editActivityComplianceMap;


	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseActivityComplianceMapPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseActivityComplianceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseActivityComplianceMapStep.addStep(BaselineViewsRepository.BaseActivityComplianceMap.Properties.class);
		// Start IRR
		//propertiesStep.addStep(BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap);
		
		
		composer = new PartComposer(baseActivityComplianceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseActivityComplianceMap.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap) {
					return createComplianceMapAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap) {
					return createActivityComplianceMapTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseActivityComplianceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createComplianceMapAdvancedTableComposition(Composite parent) {
		this.complianceMap = new ReferencesTable(getDescription(BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap, BaselineMessages.BaseActivityComplianceMapPropertiesEditionPart_ComplianceMapLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				complianceMap.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				complianceMap.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				complianceMap.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				complianceMap.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.complianceMapFilters) {
			this.complianceMap.addFilter(filter);
		}
		this.complianceMap.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap, BaselineViewsRepository.SWT_KIND));
		this.complianceMap.createControls(parent);
		this.complianceMap.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData complianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		complianceMapData.horizontalSpan = 3;
		this.complianceMap.setLayoutData(complianceMapData);
		this.complianceMap.setLowerBound(0);
		this.complianceMap.setUpperBound(-1);
		complianceMap.setID(BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap);
		complianceMap.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createComplianceMapAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createActivityComplianceMapTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableActivityComplianceMap = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableActivityComplianceMap.setHeaderVisible(true);
		GridData gdActivityComplianceMap = new GridData();
		gdActivityComplianceMap.grabExcessHorizontalSpace = true;
		gdActivityComplianceMap.horizontalAlignment = GridData.FILL;
		gdActivityComplianceMap.grabExcessVerticalSpace = true;
		gdActivityComplianceMap.verticalAlignment = GridData.FILL;
		tableActivityComplianceMap.setLayoutData(gdActivityComplianceMap);
		tableActivityComplianceMap.setLinesVisible(true);

		// Start of user code for columns definition for ActivityComplianceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableComplianceMap, SWT.NONE);
		 * name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableActivityComplianceMap, SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableActivityComplianceMap,
				SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableActivityComplianceMap,
				SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Activities"); //$NON-NLS-1$
		// End IRR
		// End of user code

		activityComplianceMap = new TableViewer(tableActivityComplianceMap);
		activityComplianceMap.setContentProvider(new ArrayContentProvider());
		activityComplianceMap.setLabelProvider(new ITableLabelProvider() {
			// Start of user code for label provider definition for
			// ActivityComplianceMap
			public String getColumnText(Object object, int columnIndex) {
				// Start IRR
				/*
				 * AdapterFactoryLabelProvider labelProvider = new
				 * AdapterFactoryLabelProvider(adapterFactory); if (object
				 * instanceof EObject) { switch (columnIndex) { case 0: return
				 * labelProvider.getText(object); case 1: return
				 * labelProvider.getText(object); } }
				 */
				if (object instanceof EObject) {
					BaseComplianceMap baselineComplianceMapObject = (BaseComplianceMap) object;
					switch (columnIndex) {
					case 0:
						return baselineComplianceMapObject.getId();
					case 1:
						if (baselineComplianceMapObject.getMapGroup() == null)
							return "";
						else
							return baselineComplianceMapObject.getMapGroup()
									.getName();
					case 2:
						if (baselineComplianceMapObject.getTarget() == null)
							return "";
						else {
							EList<AssuranceAsset> LstAssurance = baselineComplianceMapObject
									.getTarget();
							Iterator<AssuranceAsset> iter = LstAssurance
									.iterator();
							String sArtefact = "[";
							while (iter.hasNext()) {
								AssuranceAsset refManAss = (AssuranceAsset) iter
										.next();
								if (refManAss instanceof Activity) {
									Activity art = (Activity) refManAss;
									sArtefact = sArtefact + art.getName() + ",";
								}
							}
							
							sArtefact = sArtefact.substring(0,
									sArtefact.length() - 1);
							sArtefact = sArtefact + "]";
							return sArtefact;
						}

					}
				}
				// End IRR
				return ""; //$NON-NLS-1$
			}

			public Image getColumnImage(Object element, int columnIndex) {
				return null;
			}

			// End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		activityComplianceMap.getTable().addListener(SWT.MouseDoubleClick,
				new Listener() {

			public void handleEvent(Event event) {
				if (activityComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) activityComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						activityComplianceMap.refresh();
					}
				}
			}

		});
		GridData activityComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		activityComplianceMapData.minimumHeight = 120;
		activityComplianceMapData.heightHint = 120;
		activityComplianceMap.getTable().setLayoutData(activityComplianceMapData);
		for (ViewerFilter filter : this.activityComplianceMapFilters) {
			activityComplianceMap.addFilter(filter);
		}
		EditingUtils.setID(activityComplianceMap.getTable(), BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap);
		EditingUtils.setEEFtype(activityComplianceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createActivityComplianceMapPanel(tableContainer);
		// Start of user code for createActivityComplianceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createActivityComplianceMapPanel(Composite container) {
		Composite activityComplianceMapPanel = new Composite(container, SWT.NONE);
		GridLayout activityComplianceMapPanelLayout = new GridLayout();
		activityComplianceMapPanelLayout.numColumns = 1;
		activityComplianceMapPanel.setLayout(activityComplianceMapPanelLayout);
		addActivityComplianceMap = new Button(activityComplianceMapPanel, SWT.NONE);
		addActivityComplianceMap.setText(BaselineMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addActivityComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addActivityComplianceMap.setLayoutData(addActivityComplianceMapData);
		addActivityComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				activityComplianceMap.refresh();
			}
		});
		EditingUtils.setID(addActivityComplianceMap, BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap);
		EditingUtils.setEEFtype(addActivityComplianceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeActivityComplianceMap = new Button(activityComplianceMapPanel, SWT.NONE);
		removeActivityComplianceMap.setText(BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeActivityComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeActivityComplianceMap.setLayoutData(removeActivityComplianceMapData);
		removeActivityComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (activityComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) activityComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						activityComplianceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeActivityComplianceMap, BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap);
		EditingUtils.setEEFtype(removeActivityComplianceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editActivityComplianceMap = new Button(activityComplianceMapPanel, SWT.NONE);
		editActivityComplianceMap.setText(BaselineMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editActivityComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editActivityComplianceMap.setLayoutData(editActivityComplianceMapData);
		editActivityComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (activityComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) activityComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseActivityComplianceMapPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						activityComplianceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editActivityComplianceMap, BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap);
		EditingUtils.setEEFtype(editActivityComplianceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createActivityComplianceMapPanel

		// End of user code
		return activityComplianceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart#initComplianceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initComplianceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		complianceMap.setContentProvider(contentProvider);
		complianceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseActivityComplianceMap.Properties.complianceMap);
		if (eefElementEditorReadOnlyState && complianceMap.isEnabled()) {
			complianceMap.setEnabled(false);
			complianceMap.setToolTipText(BaselineMessages.BaseActivityComplianceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !complianceMap.isEnabled()) {
			complianceMap.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart#updateComplianceMap()
	 * 
	 */
	public void updateComplianceMap() {
	complianceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart#addFilterComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToComplianceMap(ViewerFilter filter) {
		complianceMapFilters.add(filter);
		if (this.complianceMap != null) {
			this.complianceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart#addBusinessFilterComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToComplianceMap(ViewerFilter filter) {
		complianceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart#isContainedInComplianceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInComplianceMapTable(EObject element) {
		return ((ReferencesTableSettings)complianceMap.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart#initActivityComplianceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initActivityComplianceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		activityComplianceMap.setContentProvider(contentProvider);
		activityComplianceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseActivityComplianceMap.Properties.activityComplianceMap);
		if (eefElementEditorReadOnlyState && activityComplianceMap.getTable().isEnabled()) {
			activityComplianceMap.getTable().setEnabled(false);
			activityComplianceMap.getTable().setToolTipText(BaselineMessages.BaseActivityComplianceMap_ReadOnly);
			addActivityComplianceMap.setEnabled(false);
			addActivityComplianceMap.setToolTipText(BaselineMessages.BaseActivityComplianceMap_ReadOnly);
			removeActivityComplianceMap.setEnabled(false);
			removeActivityComplianceMap.setToolTipText(BaselineMessages.BaseActivityComplianceMap_ReadOnly);
			editActivityComplianceMap.setEnabled(false);
			editActivityComplianceMap.setToolTipText(BaselineMessages.BaseActivityComplianceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !activityComplianceMap.getTable().isEnabled()) {
			activityComplianceMap.getTable().setEnabled(true);
			addActivityComplianceMap.setEnabled(true);
			removeActivityComplianceMap.setEnabled(true);
			editActivityComplianceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart#updateActivityComplianceMap()
	 * 
	 */
	public void updateActivityComplianceMap() {
	activityComplianceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart#addFilterActivityComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToActivityComplianceMap(ViewerFilter filter) {
		activityComplianceMapFilters.add(filter);
		if (this.activityComplianceMap != null) {
			this.activityComplianceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart#addBusinessFilterActivityComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToActivityComplianceMap(ViewerFilter filter) {
		activityComplianceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityComplianceMapPropertiesEditionPart#isContainedInActivityComplianceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInActivityComplianceMapTable(EObject element) {
		return ((ReferencesTableSettings)activityComplianceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseActivityComplianceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
