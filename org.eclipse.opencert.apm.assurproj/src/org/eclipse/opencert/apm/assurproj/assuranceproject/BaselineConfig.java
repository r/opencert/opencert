/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Baseline Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#getComplianceMapGroup <em>Compliance Map Group</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#isIsActive <em>Is Active</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#getRefFramework <em>Ref Framework</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getBaselineConfig()
 * @model
 * @generated
 */
public interface BaselineConfig extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Compliance Map Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compliance Map Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compliance Map Group</em>' reference.
	 * @see #setComplianceMapGroup(MapGroup)
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getBaselineConfig_ComplianceMapGroup()
	 * @model
	 * @generated
	 */
	MapGroup getComplianceMapGroup();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#getComplianceMapGroup <em>Compliance Map Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compliance Map Group</em>' reference.
	 * @see #getComplianceMapGroup()
	 * @generated
	 */
	void setComplianceMapGroup(MapGroup value);

	/**
	 * Returns the value of the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Active</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Active</em>' attribute.
	 * @see #setIsActive(boolean)
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getBaselineConfig_IsActive()
	 * @model
	 * @generated
	 */
	boolean isIsActive();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig#isIsActive <em>Is Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Active</em>' attribute.
	 * @see #isIsActive()
	 * @generated
	 */
	void setIsActive(boolean value);

	/**
	 * Returns the value of the '<em><b>Ref Framework</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref Framework</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Framework</em>' reference list.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getBaselineConfig_RefFramework()
	 * @model
	 * @generated
	 */
	EList<BaseFramework> getRefFramework();

} // BaselineConfig
