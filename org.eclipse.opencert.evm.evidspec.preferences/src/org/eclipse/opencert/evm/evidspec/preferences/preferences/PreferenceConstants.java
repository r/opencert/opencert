/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.evm.evidspec.preferences.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String SVNInfo_REPOSITORY_TYPE = "OPENCERT_SVN_REPOSITORY_TYPE";
	public static final String SVNInfo_LOCAL_REPOSITORY_URL = "OPENCERT_SVN_LOCAL_REPOSITORY_URL";
	public static final String SVNInfo_REMOTE_REPOSITORY_URL = "OPENCERT_SVN_REMOTE_REPOSITORY_URL";
	public static final String SVNInfo_USER = "OPENCERT_SVN_USER";
	public static final String SVNInfo_PASS = "OPENCERT_SVN_PASS";
	
}
