/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.externaltools.api.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.eclipse.opencert.externaltools.api.ExternalToolQuerySetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ExternalToolQuerySettingDAOImpl
        implements ExternalToolQuerySettingDAO
{
    @Autowired
    @Qualifier("hibernate4AnnotatedSessionFactory")
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void save(ExternalToolQuerySetting externalToolQuerySetting)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(externalToolQuerySetting);
    }

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<ExternalToolQuerySetting> list()
    {
        Session session = this.sessionFactory.getCurrentSession();
        List<ExternalToolQuerySetting> externalToolQuerySettingList = session.createQuery("from ExternalToolQuerySetting").list();

        return externalToolQuerySettingList;
    }

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public ExternalToolQuerySetting findByNameAndValue(String name, String value)
    {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("from ExternalToolQuerySetting e where e.name = :name and e.value = :value");
        query.setParameter("name", name);
        query.setParameter("value", value);

        List<ExternalToolQuerySetting> externalToolQuerySettingList = query.list();

        return externalToolQuerySettingList.size() > 0 ? externalToolQuerySettingList.get(0) : null;
    }
}
