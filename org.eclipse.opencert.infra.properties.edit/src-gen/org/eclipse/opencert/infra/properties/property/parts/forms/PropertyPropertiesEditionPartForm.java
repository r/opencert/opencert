/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.properties.property.parts.forms;

// Start of user code for imports
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.util.EcoreAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.EEFRuntimePlugin;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.EEFFeatureEditorDialog;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.opencert.infra.properties.property.PropertyPackage;
import org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart;
import org.eclipse.opencert.infra.properties.property.parts.PropertyViewsRepository;
import org.eclipse.opencert.infra.properties.property.providers.PropertyMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

// End of user code

/**
 * 
 * 
 */
public class PropertyPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, PropertyPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected EMFComboViewer datatype;
	protected Text enumValues;
	protected Button editEnumValues;
	private EList enumValuesList;
	protected Text unit;



	/**
	 * For {@link ISection} use only.
	 */
	public PropertyPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public PropertyPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence property_Step = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = property_Step.addStep(PropertyViewsRepository.Property_.Properties.class);
		propertiesStep.addStep(PropertyViewsRepository.Property_.Properties.id);
		propertiesStep.addStep(PropertyViewsRepository.Property_.Properties.name);
		propertiesStep.addStep(PropertyViewsRepository.Property_.Properties.datatype);
		propertiesStep.addStep(PropertyViewsRepository.Property_.Properties.enumValues);
		propertiesStep.addStep(PropertyViewsRepository.Property_.Properties.unit);
		
		
		composer = new PartComposer(property_Step) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == PropertyViewsRepository.Property_.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == PropertyViewsRepository.Property_.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == PropertyViewsRepository.Property_.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == PropertyViewsRepository.Property_.Properties.datatype) {
					return createDatatypeEMFComboViewer(widgetFactory, parent);
				}
				if (key == PropertyViewsRepository.Property_.Properties.enumValues) {
					return createEnumValuesMultiValuedEditor(widgetFactory, parent);
				}
				if (key == PropertyViewsRepository.Property_.Properties.unit) {
					return createUnitText(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(PropertyMessages.PropertyPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, PropertyViewsRepository.Property_.Properties.id, PropertyMessages.PropertyPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							PropertyPropertiesEditionPartForm.this,
							PropertyViewsRepository.Property_.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PropertyPropertiesEditionPartForm.this,
									PropertyViewsRepository.Property_.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PropertyPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartForm.this, PropertyViewsRepository.Property_.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, PropertyViewsRepository.Property_.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(PropertyViewsRepository.Property_.Properties.id, PropertyViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, PropertyViewsRepository.Property_.Properties.name, PropertyMessages.PropertyPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							PropertyPropertiesEditionPartForm.this,
							PropertyViewsRepository.Property_.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PropertyPropertiesEditionPartForm.this,
									PropertyViewsRepository.Property_.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PropertyPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartForm.this, PropertyViewsRepository.Property_.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, PropertyViewsRepository.Property_.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(PropertyViewsRepository.Property_.Properties.name, PropertyViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDatatypeEMFComboViewer(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, PropertyViewsRepository.Property_.Properties.datatype, PropertyMessages.PropertyPropertiesEditionPart_DatatypeLabel);
		datatype = new EMFComboViewer(parent);
		datatype.setContentProvider(new ArrayContentProvider());
		datatype.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData datatypeData = new GridData(GridData.FILL_HORIZONTAL);
		datatype.getCombo().setLayoutData(datatypeData);
		datatype.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartForm.this, PropertyViewsRepository.Property_.Properties.datatype, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getDatatype()));
			}

		});
		datatype.setID(PropertyViewsRepository.Property_.Properties.datatype);
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(PropertyViewsRepository.Property_.Properties.datatype, PropertyViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDatatypeEMFComboViewer

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createEnumValuesMultiValuedEditor(FormToolkit widgetFactory, Composite parent) {
		enumValues = widgetFactory.createText(parent, "", SWT.READ_ONLY); //$NON-NLS-1$
		GridData enumValuesData = new GridData(GridData.FILL_HORIZONTAL);
		enumValuesData.horizontalSpan = 2;
		enumValues.setLayoutData(enumValuesData);
		EditingUtils.setID(enumValues, PropertyViewsRepository.Property_.Properties.enumValues);
		EditingUtils.setEEFtype(enumValues, "eef::MultiValuedEditor::field"); //$NON-NLS-1$
		editEnumValues = widgetFactory.createButton(parent, getDescription(PropertyViewsRepository.Property_.Properties.enumValues, PropertyMessages.PropertyPropertiesEditionPart_EnumValuesLabel), SWT.NONE);
		GridData editEnumValuesData = new GridData();
		editEnumValues.setLayoutData(editEnumValuesData);
		editEnumValues.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				EEFFeatureEditorDialog dialog = new EEFFeatureEditorDialog(
						enumValues.getShell(), "Property", new AdapterFactoryLabelProvider(adapterFactory), //$NON-NLS-1$
						enumValuesList, PropertyPackage.eINSTANCE.getProperty_EnumValues().getEType(), null,
						false, true, 
						null, null);
				if (dialog.open() == Window.OK) {
					enumValuesList = dialog.getResult();
					if (enumValuesList == null) {
						enumValuesList = new BasicEList();
					}
					enumValues.setText(enumValuesList.toString());
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartForm.this, PropertyViewsRepository.Property_.Properties.enumValues, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, new BasicEList(enumValuesList)));
					setHasChanged(true);
				}
			}
		});
		EditingUtils.setID(editEnumValues, PropertyViewsRepository.Property_.Properties.enumValues);
		EditingUtils.setEEFtype(editEnumValues, "eef::MultiValuedEditor::browsebutton"); //$NON-NLS-1$
		// Start of user code for createEnumValuesMultiValuedEditor

		// End of user code
		return parent;
	}

	
	protected Composite createUnitText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, PropertyViewsRepository.Property_.Properties.unit, PropertyMessages.PropertyPropertiesEditionPart_UnitLabel);
		unit = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		unit.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData unitData = new GridData(GridData.FILL_HORIZONTAL);
		unit.setLayoutData(unitData);
		unit.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							PropertyPropertiesEditionPartForm.this,
							PropertyViewsRepository.Property_.Properties.unit,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, unit.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PropertyPropertiesEditionPartForm.this,
									PropertyViewsRepository.Property_.Properties.unit,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, unit.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PropertyPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		unit.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartForm.this, PropertyViewsRepository.Property_.Properties.unit, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, unit.getText()));
				}
			}
		});
		EditingUtils.setID(unit, PropertyViewsRepository.Property_.Properties.unit);
		EditingUtils.setEEFtype(unit, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(PropertyViewsRepository.Property_.Properties.unit, PropertyViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createUnitText

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#getDatatype()
	 * 
	 */
	public Enumerator getDatatype() {
		Enumerator selection = (Enumerator) ((StructuredSelection) datatype.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#initDatatype(Object input, Enumerator current)
	 */
	public void initDatatype(Object input, Enumerator current) {
		datatype.setInput(input);
		datatype.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.datatype);
		if (eefElementEditorReadOnlyState && datatype.isEnabled()) {
			datatype.setEnabled(false);
			datatype.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !datatype.isEnabled()) {
			datatype.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#setDatatype(Enumerator newValue)
	 * 
	 */
	public void setDatatype(Enumerator newValue) {
		datatype.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.datatype);
		if (eefElementEditorReadOnlyState && datatype.isEnabled()) {
			datatype.setEnabled(false);
			datatype.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !datatype.isEnabled()) {
			datatype.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#getEnumValues()
	 * 
	 */
	public EList getEnumValues() {
		return enumValuesList;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#setEnumValues(EList newValue)
	 * 
	 */
	public void setEnumValues(EList newValue) {
		enumValuesList = newValue;
		if (newValue != null) {
			enumValues.setText(enumValuesList.toString());
		} else {
			enumValues.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.enumValues);
		if (eefElementEditorReadOnlyState && enumValues.isEnabled()) {
			enumValues.setEnabled(false);
			enumValues.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !enumValues.isEnabled()) {
			enumValues.setEnabled(true);
		}	
		
	}

	public void addToEnumValues(Object newValue) {
		enumValuesList.add(newValue);
		if (newValue != null) {
			enumValues.setText(enumValuesList.toString());
		} else {
			enumValues.setText(""); //$NON-NLS-1$
		}
	}

	public void removeToEnumValues(Object newValue) {
		enumValuesList.remove(newValue);
		if (newValue != null) {
			enumValues.setText(enumValuesList.toString());
		} else {
			enumValues.setText(""); //$NON-NLS-1$
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#getUnit()
	 * 
	 */
	public String getUnit() {
		return unit.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#setUnit(String newValue)
	 * 
	 */
	public void setUnit(String newValue) {
		if (newValue != null) {
			unit.setText(newValue);
		} else {
			unit.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.unit);
		if (eefElementEditorReadOnlyState && unit.isEnabled()) {
			unit.setEnabled(false);
			unit.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !unit.isEnabled()) {
			unit.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return PropertyMessages.Property_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
