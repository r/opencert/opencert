/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.views.properties.tabbed.ISection;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;
import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;

// End of user code

/**
 * 
 * 
 */
public class ArtefactEquivalenceMapPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ArtefactEquivalenceMapPropertiesEditionPart {

	protected ReferencesTable equivalence;
	protected List<ViewerFilter> equivalenceBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> equivalenceFilters = new ArrayList<ViewerFilter>();
	protected TableViewer artefactEquivalenceMap;
	protected List<ViewerFilter> artefactEquivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> artefactEquivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addArtefactEquivalenceMap;
	protected Button removeArtefactEquivalenceMap;
	protected Button editArtefactEquivalenceMap;



	/**
	 * For {@link ISection} use only.
	 */
	public ArtefactEquivalenceMapPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArtefactEquivalenceMapPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence artefactEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = artefactEquivalenceMapStep.addStep(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence);
		// End IRR
		propertiesStep.addStep(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_);
		
		
		composer = new PartComposer(artefactEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence) {
					return createEquivalenceTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_) {
					return createArtefactEquivalenceMapTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(RefframeworkMessages.ArtefactEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEquivalenceTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.equivalence = new ReferencesTable(getDescription(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence, RefframeworkMessages.ArtefactEquivalenceMapPropertiesEditionPart_EquivalenceLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				equivalence.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				equivalence.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				equivalence.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				equivalence.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.equivalenceFilters) {
			this.equivalence.addFilter(filter);
		}
		this.equivalence.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence, RefframeworkViewsRepository.FORM_KIND));
		this.equivalence.createControls(parent, widgetFactory);
		this.equivalence.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData equivalenceData = new GridData(GridData.FILL_HORIZONTAL);
		equivalenceData.horizontalSpan = 3;
		this.equivalence.setLayoutData(equivalenceData);
		this.equivalence.setLowerBound(0);
		this.equivalence.setUpperBound(-1);
		equivalence.setID(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence);
		equivalence.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEquivalenceTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactEquivalenceMapTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableArtefactEquivalenceMap = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableArtefactEquivalenceMap.setHeaderVisible(true);
		GridData gdArtefactEquivalenceMap = new GridData();
		gdArtefactEquivalenceMap.grabExcessHorizontalSpace = true;
		gdArtefactEquivalenceMap.horizontalAlignment = GridData.FILL;
		gdArtefactEquivalenceMap.grabExcessVerticalSpace = true;
		gdArtefactEquivalenceMap.verticalAlignment = GridData.FILL;
		tableArtefactEquivalenceMap.setLayoutData(gdArtefactEquivalenceMap);
		tableArtefactEquivalenceMap.setLinesVisible(true);

		// Start of user code for columns definition for ArtefactEquivalenceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableArtefactEquivalenceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableArtefactEquivalenceMap,
				SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableArtefactEquivalenceMap,
				SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableArtefactEquivalenceMap,
				SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Artefacts"); //$NON-NLS-1$
		// End IRR
		// End of user code

		artefactEquivalenceMap = new TableViewer(tableArtefactEquivalenceMap);
		artefactEquivalenceMap.setContentProvider(new ArrayContentProvider());
		artefactEquivalenceMap.setLabelProvider(new ITableLabelProvider() {
			// Start of user code for label provider definition for
			// ArtefactEquivalenceMap
			public String getColumnText(Object object, int columnIndex) {
				// Start IRR
				/*
				 * AdapterFactoryLabelProvider labelProvider = new
				 * AdapterFactoryLabelProvider(adapterFactory); if (object
				 * instanceof EObject) { switch (columnIndex) { case 0: return
				 * labelProvider.getText(object); } }
				 */
				if (object instanceof EObject) {
					RefEquivalenceMap refEquivalenceMap = (RefEquivalenceMap) object;
					switch (columnIndex) {
					case 0:
						return refEquivalenceMap.getId();
					case 1:
						if (refEquivalenceMap.getMapGroup() == null)
							return "";
						else
							return refEquivalenceMap.getMapGroup().getName();
					case 2:
						if (refEquivalenceMap.getTarget() == null)
							return "";
						else {
							EList<RefAssurableElement> LstElement = refEquivalenceMap
									.getTarget();
							Iterator<RefAssurableElement> iter = LstElement
									.iterator();
							String sElement = "[";
							
							// Start IRR 20141205 
							
							EObject object1 = null;
							while (iter.hasNext()) {
								object1 = iter.next();
								if (object1 instanceof RefArtefact) {
									RefArtefact refElement = (RefArtefact) object1;
									if (!(refEquivalenceMap.cdoResource().toString().equals(refElement.cdoResource().toString()))) {
										sElement = sElement + refElement.getName() + ",";
									}
								}
							}
							
							// End IRR 20141205
							
							// delete ,
							sElement = sElement.substring(0,
									sElement.length() - 1);
							sElement = sElement + "]";
							return sElement;
						}

					}
				}
				// End IRR
				return ""; //$NON-NLS-1$
			}

			public Image getColumnImage(Object element, int columnIndex) {
				return null;
			}

			// End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		artefactEquivalenceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (artefactEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						artefactEquivalenceMap.refresh();
					}
				}
			}

		});
		GridData artefactEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		artefactEquivalenceMapData.minimumHeight = 120;
		artefactEquivalenceMapData.heightHint = 120;
		artefactEquivalenceMap.getTable().setLayoutData(artefactEquivalenceMapData);
		for (ViewerFilter filter : this.artefactEquivalenceMapFilters) {
			artefactEquivalenceMap.addFilter(filter);
		}
		EditingUtils.setID(artefactEquivalenceMap.getTable(), RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_);
		EditingUtils.setEEFtype(artefactEquivalenceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createArtefactEquivalenceMapPanel(widgetFactory, tableContainer);
		// Start of user code for createArtefactEquivalenceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactEquivalenceMapPanel(FormToolkit widgetFactory, Composite container) {
		Composite artefactEquivalenceMapPanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout artefactEquivalenceMapPanelLayout = new GridLayout();
		artefactEquivalenceMapPanelLayout.numColumns = 1;
		artefactEquivalenceMapPanel.setLayout(artefactEquivalenceMapPanelLayout);
		addArtefactEquivalenceMap = widgetFactory.createButton(artefactEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addArtefactEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addArtefactEquivalenceMap.setLayoutData(addArtefactEquivalenceMapData);
		addArtefactEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				artefactEquivalenceMap.refresh();
			}
		});
		EditingUtils.setID(addArtefactEquivalenceMap, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_);
		EditingUtils.setEEFtype(addArtefactEquivalenceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeArtefactEquivalenceMap = widgetFactory.createButton(artefactEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeArtefactEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeArtefactEquivalenceMap.setLayoutData(removeArtefactEquivalenceMapData);
		removeArtefactEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (artefactEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						artefactEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeArtefactEquivalenceMap, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_);
		EditingUtils.setEEFtype(removeArtefactEquivalenceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editArtefactEquivalenceMap = widgetFactory.createButton(artefactEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editArtefactEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editArtefactEquivalenceMap.setLayoutData(editArtefactEquivalenceMapData);
		editArtefactEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (artefactEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						artefactEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editArtefactEquivalenceMap, RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_);
		EditingUtils.setEEFtype(editArtefactEquivalenceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createArtefactEquivalenceMapPanel

		// End of user code
		return artefactEquivalenceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart#initEquivalence(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEquivalence(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		equivalence.setContentProvider(contentProvider);
		equivalence.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence);
		if (eefElementEditorReadOnlyState && equivalence.isEnabled()) {
			equivalence.setEnabled(false);
			equivalence.setToolTipText(RefframeworkMessages.ArtefactEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !equivalence.isEnabled()) {
			equivalence.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart#updateEquivalence()
	 * 
	 */
	public void updateEquivalence() {
	equivalence.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart#addFilterEquivalence(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEquivalence(ViewerFilter filter) {
		equivalenceFilters.add(filter);
		if (this.equivalence != null) {
			this.equivalence.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart#addBusinessFilterEquivalence(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEquivalence(ViewerFilter filter) {
		equivalenceBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart#isContainedInEquivalenceTable(EObject element)
	 * 
	 */
	public boolean isContainedInEquivalenceTable(EObject element) {
		return ((ReferencesTableSettings)equivalence.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart#initArtefactEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initArtefactEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		artefactEquivalenceMap.setContentProvider(contentProvider);
		artefactEquivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_);
		if (eefElementEditorReadOnlyState && artefactEquivalenceMap.getTable().isEnabled()) {
			artefactEquivalenceMap.getTable().setEnabled(false);
			artefactEquivalenceMap.getTable().setToolTipText(RefframeworkMessages.ArtefactEquivalenceMap_ReadOnly);
			addArtefactEquivalenceMap.setEnabled(false);
			addArtefactEquivalenceMap.setToolTipText(RefframeworkMessages.ArtefactEquivalenceMap_ReadOnly);
			removeArtefactEquivalenceMap.setEnabled(false);
			removeArtefactEquivalenceMap.setToolTipText(RefframeworkMessages.ArtefactEquivalenceMap_ReadOnly);
			editArtefactEquivalenceMap.setEnabled(false);
			editArtefactEquivalenceMap.setToolTipText(RefframeworkMessages.ArtefactEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !artefactEquivalenceMap.getTable().isEnabled()) {
			artefactEquivalenceMap.getTable().setEnabled(true);
			addArtefactEquivalenceMap.setEnabled(true);
			removeArtefactEquivalenceMap.setEnabled(true);
			editArtefactEquivalenceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart#updateArtefactEquivalenceMap()
	 * 
	 */
	public void updateArtefactEquivalenceMap() {
	artefactEquivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart#addFilterArtefactEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArtefactEquivalenceMap(ViewerFilter filter) {
		artefactEquivalenceMapFilters.add(filter);
		if (this.artefactEquivalenceMap != null) {
			this.artefactEquivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart#addBusinessFilterArtefactEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArtefactEquivalenceMap(ViewerFilter filter) {
		artefactEquivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart#isContainedInArtefactEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInArtefactEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)artefactEquivalenceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.ArtefactEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
