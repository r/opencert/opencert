/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.assuranceproject.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.emf.cdo.dawn.commands.CreateSemanticResourceRecordingCommand;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.DawnEditorInput;
import org.eclipse.emf.cdo.dawn.transaction.DawnGMFEditingDomainFactory;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.swt.SWTException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.LoadViewLocations;
import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.ModelElement;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.CaseCanonicalEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.CasePersistedCanonicalEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.DawnCasePersistedCanonicalEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.DawnCasePersistedSemiCanonicalEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorPlugin;
import org.eclipse.opencert.sam.arg.arg.diagram.part.DawnArgDiagramEditor;
import org.eclipse.opencert.sam.arg.arg.diagram.part.Messages;
import org.eclipse.opencert.sam.arg.arg.impl.CaseImpl;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.RepairDawnViewLocationsCommand;
import org.eclipse.gmf.runtime.notation.View;

// Start MCP
public class DawnArgDiagramUtil {
	private CDOResource sourceDiagramResource; // for location and size views
	private URI modelURI;
	private URI diagramURI;
	protected Resource diagram;
	private CDOResourceFolder assuranceprojectFolder;
	private CDOTransaction transaction;
	private List<EObject> listRootObjects = null;
	
	private static String DawnArgDiagramEditor_ID = "org.eclipse.opencert.sam.arg.arg.diagram.part.DawnArgDiagramEditor";
	private final static String DawnCaseEditPart_MODEL_ID = "Arg";
	
	/**
	 * Constructor 
	 */
	public DawnArgDiagramUtil(CDOResource sourceDiagramResource, List<EObject> lro, URI model, URI diagram, CDOResourceFolder assuranceprojectFolder, CDOTransaction transaction) {
		super();
		this.sourceDiagramResource = sourceDiagramResource;
		this.listRootObjects = lro;
		this.modelURI = model;
		this.diagramURI = diagram;
		this.assuranceprojectFolder = assuranceprojectFolder;
		this.transaction = transaction;
	}


	public void generateDiagram(IProgressMonitor monitor) {
		try {
			generateDiagram0(monitor);
			/* MCP
			IWorkspace workspace = ResourcesPlugin.getWorkspace();
			workspace.getRoot().refreshLocal(IResource.DEPTH_INFINITE, null);
			*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	//generates a diagram for the given domain model file name
	public void generateDiagram0(IProgressMonitor monitor) throws IOException {
		//diagram = DawnArgDiagramEditorUtil.createDiagram(diagramURI, modelURI, monitor);
		diagram = createDiagram(diagramURI, modelURI, monitor);
		if (diagram != null) {
			try {
				//DawnArgDiagramEditorUtil.openDiagram(diagram);
				openDiagram(diagram); // para que el editor de arg_diagram este activo
				// Start MCP
				IEditorPart editorPart = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.getActiveEditor();
				if (editorPart instanceof IDiagramWorkbenchPart) {
					DiagramEditPart  editp = ((IDiagramWorkbenchPart) editorPart).getDiagramEditPart();

					editp.removeEditPolicy(EditPolicyRoles.CANONICAL_ROLE);
					/* MCP: se ejecuta todo 2 veces!!!
					editp.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
							new CasePersistedCanonicalEditPolicy()); //MCP OJO: para evitar bucle infinito
					*/
					
					/* FUNCIONA PERO CREA TODAS LAS VISTAS!!!!
 					editp.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
							new DawnCasePersistedCanonicalEditPolicy());
					*/
					// crear solo vistas del diagrama
					/* No es necesario
		        	List<EObject> sublist = listRootObjects;
		        	List<EObject> toRemove = new ArrayList<EObject>();
		        	for( EObject element : sublist)
		        	{
		        		// no se puede copiar desde Case o elementos de otros modelos
		        	    if(false == element instanceof ModelElement)
		        	    	toRemove.add(element);
		        	}
		        	listRootObjects.removeAll(toRemove);
		        	*/
					LoadViewLocations sourceViewLocations = new LoadViewLocations(sourceDiagramResource);
					sourceViewLocations.LoadObjectLocations();
					DawnCasePersistedSemiCanonicalEditPolicy scp = new DawnCasePersistedSemiCanonicalEditPolicy();
					scp.setObjects(listRootObjects);
					//scp.setSourceDiagram(sourceDiagramResource);					
					scp.setSourceDiagram(sourceViewLocations);
 					//editp.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE, scp);
					scp.setHost(editp);
					scp.refresh();                
					scp.deactivate(); // para que no invoque refreshSemantic() al eliminar del diagrama uno de los objetos

 					
	        		// No puedo cerrar el diagrama porque no se crean las views!!! y pierdo la perspectiva
	        		DawnArgDiagramEditor dawnEditorPart = (DawnArgDiagramEditor)editorPart;
	        		dawnEditorPart.doSave(monitor);


	        		if(editorPart instanceof DiagramEditor) {
	        			boolean save = true;

	        	        try {
	            			DiagramEditor deditor = (DiagramEditor)editorPart;
	            			deditor.getSite().getPage().closeEditor(deditor, save); //MCP comentar para ver el resultado (views temporales)
	        	        } catch (SWTException e) {
	        	        	e.printStackTrace();
	        	        }      			
	        		}
				}				
				// End MCP
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Resource createDiagram(URI diagramURI, URI modelURI,
			IProgressMonitor progressMonitor) {
		TransactionalEditingDomain editingDomain = DawnGMFEditingDomainFactory
				.getInstance().createEditingDomain();

		progressMonitor
				.beginTask(
						Messages.ArgDiagramEditorUtil_CreateDiagramProgressTask,
						3);

		/*
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOConnectionUtil.instance.getCurrentSession();
		ResourceSet resourceSet = editingDomain.getResourceSet();

		CDOTransaction transaction = CDOConnectionUtil.instance
				.openCurrentTransaction(resourceSet, diagramURI.toString());

		final Resource diagramResource = resourceSet.createResource(diagramURI);
		*/
		String diagramResourceStr = diagramURI.toString().substring("cdo://opencert".length(), diagramURI.toString().length());
		final Resource diagramResource = transaction.getOrCreateResource(diagramResourceStr);

		CreateSemanticResourceRecordingCommand createSemanticResourceCommand = new CreateSemanticResourceRecordingCommand(
				editingDomain, transaction, modelURI.path());

		editingDomain.getCommandStack().execute(createSemanticResourceCommand);
		final Resource modelResource = createSemanticResourceCommand
				.getResource();

		final String diagramName = diagramURI.lastSegment();
		AbstractTransactionalCommand command = new AbstractTransactionalCommand(
				editingDomain,
				Messages.ArgDiagramEditorUtil_CreateDiagramCommandLabel,
				Collections.EMPTY_LIST) {
			@Override
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {
				EObject model = null;
				if (modelResource.getContents().size() > 0) {
					model = modelResource.getContents().get(0);
				} else {
					model = createInitialModel();
					attachModelToResource(model, modelResource);
				}

				Diagram diagram = ViewService.createDiagram(model,
						DawnCaseEditPart_MODEL_ID,
						ArgDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
				if (diagram != null) {
					diagramResource.getContents().add(diagram);
					diagram.setName(diagramName);
					diagram.setElement(model);
				}

				try {
					modelResource.save(Collections.EMPTY_MAP);
					diagramResource.save(Collections.EMPTY_MAP);
				} catch (IOException e) {
					ArgDiagramEditorPlugin.getInstance().logError(
							"Unable to store model and diagram resources", e); //$NON-NLS-1$
				}

				return CommandResult.newOKCommandResult();
			}
		};
		try {
			OperationHistoryFactory.getOperationHistory().execute(command,
					new SubProgressMonitor(progressMonitor, 1), null);
		} catch (ExecutionException e) {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Unable to create model and diagram", e); //$NON-NLS-1$
		}
		setCharset(WorkspaceSynchronizer.getFile(modelResource));
		setCharset(WorkspaceSynchronizer.getFile(diagramResource));
		return diagramResource;
	}

	private static EObject createInitialModel() {
		return ArgFactory.eINSTANCE.createCase();
	}

	private static void attachModelToResource(EObject model, Resource resource) {
		resource.getContents().add(model);
	}
	
	public static boolean openDiagram(Resource diagram)
			throws PartInitException {
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		return null != page.openEditor(new DawnEditorInput(diagram.getURI()),
				DawnArgDiagramEditor_ID);
	}
	
	public static void setCharset(IFile file) {
		if (file == null) {
			return;
		}
		try {
			file.setCharset("UTF-8", new NullProgressMonitor()); //$NON-NLS-1$
		} catch (CoreException e) {
			ArgDiagramEditorPlugin.getInstance().logError(
					"Unable to set charset for file " + file.getFullPath(), e); //$NON-NLS-1$
		}
	}
}
// End MCP