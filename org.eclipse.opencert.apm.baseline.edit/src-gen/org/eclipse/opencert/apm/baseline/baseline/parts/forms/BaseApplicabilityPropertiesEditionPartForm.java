/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.forms;

// Start of user code for imports


import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;

// End of user code

/**
 * 
 * 
 */
public class BaseApplicabilityPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, BaseApplicabilityPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected ReferencesTable applicCritic;
	protected List<ViewerFilter> applicCriticBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> applicCriticFilters = new ArrayList<ViewerFilter>();
	protected TableViewer applicCriticTable;
	protected List<ViewerFilter> applicCriticTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> applicCriticTableFilters = new ArrayList<ViewerFilter>();
	protected Button addApplicCriticTable;
	protected Button removeApplicCriticTable;
	protected Button editApplicCriticTable;
	protected Text comments;
	protected EObjectFlatComboViewer applicTarget;
	protected ReferencesTable ownedRel;
	protected List<ViewerFilter> ownedRelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRelFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public BaseApplicabilityPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseApplicabilityPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence baseApplicabilityStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseApplicabilityStep.addStep(BaselineViewsRepository.BaseApplicability.Properties.class);
		propertiesStep.addStep(BaselineViewsRepository.BaseApplicability.Properties.id);
		propertiesStep.addStep(BaselineViewsRepository.BaseApplicability.Properties.name);
		//Start IRR
		//propertiesStep.addStep(RefframeworkViewsRepository.RefApplicability.Properties.applicCritic);
		//End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable);
		propertiesStep.addStep(BaselineViewsRepository.BaseApplicability.Properties.comments);
		propertiesStep.addStep(BaselineViewsRepository.BaseApplicability.Properties.applicTarget);
		propertiesStep.addStep(BaselineViewsRepository.BaseApplicability.Properties.ownedRel);
		
		
		composer = new PartComposer(baseApplicabilityStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseApplicability.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseApplicability.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseApplicability.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseApplicability.Properties.applicCritic) {
					return createApplicCriticTableComposition(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable) {
					return createApplicCriticTableTableComposition(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseApplicability.Properties.comments) {
					return createCommentsTextarea(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseApplicability.Properties.applicTarget) {
					return createApplicTargetFlatComboViewer(parent, widgetFactory);
				}
				if (key == BaselineViewsRepository.BaseApplicability.Properties.ownedRel) {
					return createOwnedRelTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(BaselineMessages.BaseApplicabilityPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseApplicability.Properties.id, BaselineMessages.BaseApplicabilityPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							BaseApplicabilityPropertiesEditionPartForm.this,
							BaselineViewsRepository.BaseApplicability.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseApplicabilityPropertiesEditionPartForm.this,
									BaselineViewsRepository.BaseApplicability.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseApplicabilityPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, BaselineViewsRepository.BaseApplicability.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseApplicability.Properties.id, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseApplicability.Properties.name, BaselineMessages.BaseApplicabilityPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							BaseApplicabilityPropertiesEditionPartForm.this,
							BaselineViewsRepository.BaseApplicability.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseApplicabilityPropertiesEditionPartForm.this,
									BaselineViewsRepository.BaseApplicability.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseApplicabilityPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, BaselineViewsRepository.BaseApplicability.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseApplicability.Properties.name, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createApplicCriticTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.applicCritic = new ReferencesTable(getDescription(BaselineViewsRepository.BaseApplicability.Properties.applicCritic, BaselineMessages.BaseApplicabilityPropertiesEditionPart_ApplicCriticLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.applicCritic, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				applicCritic.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.applicCritic, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				applicCritic.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.applicCritic, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				applicCritic.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.applicCritic, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				applicCritic.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.applicCriticFilters) {
			this.applicCritic.addFilter(filter);
		}
		this.applicCritic.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseApplicability.Properties.applicCritic, BaselineViewsRepository.FORM_KIND));
		this.applicCritic.createControls(parent, widgetFactory);
		this.applicCritic.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.applicCritic, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData applicCriticData = new GridData(GridData.FILL_HORIZONTAL);
		applicCriticData.horizontalSpan = 3;
		this.applicCritic.setLayoutData(applicCriticData);
		this.applicCritic.setLowerBound(0);
		this.applicCritic.setUpperBound(-1);
		applicCritic.setID(BaselineViewsRepository.BaseApplicability.Properties.applicCritic);
		applicCritic.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createApplicCriticTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createApplicCriticTableTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableApplicCriticTable = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableApplicCriticTable.setHeaderVisible(true);
		GridData gdApplicCriticTable = new GridData();
		gdApplicCriticTable.grabExcessHorizontalSpace = true;
		gdApplicCriticTable.horizontalAlignment = GridData.FILL;
		gdApplicCriticTable.grabExcessVerticalSpace = true;
		gdApplicCriticTable.verticalAlignment = GridData.FILL;
		tableApplicCriticTable.setLayoutData(gdApplicCriticTable);
		tableApplicCriticTable.setLinesVisible(true);

		// Start of user code for columns definition for ApplicCriticTable
		// Start IRR
		TableColumn name = new TableColumn(tableApplicCriticTable, SWT.NONE);
		name.setWidth(100);
		name.setText("Criticality"); //$NON-NLS-1$
		TableColumn name1 = new TableColumn(tableApplicCriticTable, SWT.NONE);
		name1.setWidth(100);
		name1.setText("Applicability"); //$NON-NLS-1$
		// End IRR
		// End of user code
		
		applicCriticTable = new TableViewer(tableApplicCriticTable);
		applicCriticTable.setContentProvider(new ArrayContentProvider());
		applicCriticTable.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for ApplicCriticTable
			public String getColumnText(Object object, int columnIndex) {
				//Start IRR
				//AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
				
				/*if (object instanceof EObject) {
					switch (columnIndex) {
					case 0:
						return labelProvider.getText(object);
					}
				}*/
				
				if (object instanceof EObject) {

					BaseCriticalityApplicability refCritAppli = (BaseCriticalityApplicability) object;
	

					switch (columnIndex) {
					case 0:	
						BaseCriticalityLevel refCriticalityLevel = refCritAppli.getCriticLevel();
						if (refCriticalityLevel != null)
							return refCriticalityLevel.getId();
					case 1:	
						
						BaseApplicabilityLevel refapp = (BaseApplicabilityLevel) refCritAppli.getApplicLevel();
						if (refapp != null)
							return refapp.getId();
						else
							return "";
					} // End switch

				} 
				
				//End IRR

				return ""; //$NON-NLS-1$
			}

			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		applicCriticTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (applicCriticTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) applicCriticTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						applicCriticTable.refresh();
					}
				}
			}

		});
		GridData applicCriticTableData = new GridData(GridData.FILL_HORIZONTAL);
		applicCriticTableData.minimumHeight = 120;
		applicCriticTableData.heightHint = 120;
		applicCriticTable.getTable().setLayoutData(applicCriticTableData);
		for (ViewerFilter filter : this.applicCriticTableFilters) {
			applicCriticTable.addFilter(filter);
		}
		EditingUtils.setID(applicCriticTable.getTable(), BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable);
		EditingUtils.setEEFtype(applicCriticTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createApplicCriticTablePanel(widgetFactory, tableContainer);
		// Start of user code for createApplicCriticTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createApplicCriticTablePanel(FormToolkit widgetFactory, Composite container) {
		Composite applicCriticTablePanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout applicCriticTablePanelLayout = new GridLayout();
		applicCriticTablePanelLayout.numColumns = 1;
		applicCriticTablePanel.setLayout(applicCriticTablePanelLayout);
		addApplicCriticTable = widgetFactory.createButton(applicCriticTablePanel, BaselineMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addApplicCriticTableData = new GridData(GridData.FILL_HORIZONTAL);
		addApplicCriticTable.setLayoutData(addApplicCriticTableData);
		addApplicCriticTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				applicCriticTable.refresh();
			}
		});
		EditingUtils.setID(addApplicCriticTable, BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable);
		EditingUtils.setEEFtype(addApplicCriticTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeApplicCriticTable = widgetFactory.createButton(applicCriticTablePanel, BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeApplicCriticTableData = new GridData(GridData.FILL_HORIZONTAL);
		removeApplicCriticTable.setLayoutData(removeApplicCriticTableData);
		removeApplicCriticTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (applicCriticTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) applicCriticTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						applicCriticTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeApplicCriticTable, BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable);
		EditingUtils.setEEFtype(removeApplicCriticTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editApplicCriticTable = widgetFactory.createButton(applicCriticTablePanel, BaselineMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editApplicCriticTableData = new GridData(GridData.FILL_HORIZONTAL);
		editApplicCriticTable.setLayoutData(editApplicCriticTableData);
		editApplicCriticTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (applicCriticTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) applicCriticTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						applicCriticTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editApplicCriticTable, BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable);
		EditingUtils.setEEFtype(editApplicCriticTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createApplicCriticTablePanel

		// End of user code
		return applicCriticTablePanel;
	}

	
	protected Composite createCommentsTextarea(FormToolkit widgetFactory, Composite parent) {
		Label commentsLabel = createDescription(parent, BaselineViewsRepository.BaseApplicability.Properties.comments, BaselineMessages.BaseApplicabilityPropertiesEditionPart_CommentsLabel);
		GridData commentsLabelData = new GridData(GridData.FILL_HORIZONTAL);
		commentsLabelData.horizontalSpan = 3;
		commentsLabel.setLayoutData(commentsLabelData);
		comments = widgetFactory.createText(parent, "", SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL); //$NON-NLS-1$
		GridData commentsData = new GridData(GridData.FILL_HORIZONTAL);
		commentsData.horizontalSpan = 2;
		commentsData.heightHint = 80;
		commentsData.widthHint = 200;
		comments.setLayoutData(commentsData);
		comments.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							BaseApplicabilityPropertiesEditionPartForm.this,
							BaselineViewsRepository.BaseApplicability.Properties.comments,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, comments.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseApplicabilityPropertiesEditionPartForm.this,
									BaselineViewsRepository.BaseApplicability.Properties.comments,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, comments.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									BaseApplicabilityPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		EditingUtils.setID(comments, BaselineViewsRepository.BaseApplicability.Properties.comments);
		EditingUtils.setEEFtype(comments, "eef::Textarea"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseApplicability.Properties.comments, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createCommentsTextArea

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * @param widgetFactory factory to use to instanciante widget of the form
	 * 
	 */
	protected Composite createApplicTargetFlatComboViewer(Composite parent, FormToolkit widgetFactory) {
		createDescription(parent, BaselineViewsRepository.BaseApplicability.Properties.applicTarget, BaselineMessages.BaseApplicabilityPropertiesEditionPart_ApplicTargetLabel);
		applicTarget = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(BaselineViewsRepository.BaseApplicability.Properties.applicTarget, BaselineViewsRepository.FORM_KIND));
		widgetFactory.adapt(applicTarget);
		applicTarget.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		GridData applicTargetData = new GridData(GridData.FILL_HORIZONTAL);
		applicTarget.setLayoutData(applicTargetData);
		applicTarget.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.applicTarget, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getApplicTarget()));
			}

		});
		applicTarget.setID(BaselineViewsRepository.BaseApplicability.Properties.applicTarget);
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseApplicability.Properties.applicTarget, BaselineViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createApplicTargetFlatComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRelTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedRel = new ReferencesTable(getDescription(BaselineViewsRepository.BaseApplicability.Properties.ownedRel, BaselineMessages.BaseApplicabilityPropertiesEditionPart_OwnedRelLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRel.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRelFilters) {
			this.ownedRel.addFilter(filter);
		}
		this.ownedRel.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseApplicability.Properties.ownedRel, BaselineViewsRepository.FORM_KIND));
		this.ownedRel.createControls(parent, widgetFactory);
		this.ownedRel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseApplicabilityPropertiesEditionPartForm.this, BaselineViewsRepository.BaseApplicability.Properties.ownedRel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRelData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRelData.horizontalSpan = 3;
		this.ownedRel.setLayoutData(ownedRelData);
		this.ownedRel.setLowerBound(0);
		this.ownedRel.setUpperBound(-1);
		ownedRel.setID(BaselineViewsRepository.BaseApplicability.Properties.ownedRel);
		ownedRel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRelTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseApplicability.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseApplicability.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#initApplicCritic(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initApplicCritic(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		applicCritic.setContentProvider(contentProvider);
		applicCritic.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseApplicability.Properties.applicCritic);
		if (eefElementEditorReadOnlyState && applicCritic.isEnabled()) {
			applicCritic.setEnabled(false);
			applicCritic.setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicCritic.isEnabled()) {
			applicCritic.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#updateApplicCritic()
	 * 
	 */
	public void updateApplicCritic() {
	applicCritic.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#addFilterApplicCritic(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicCritic(ViewerFilter filter) {
		applicCriticFilters.add(filter);
		if (this.applicCritic != null) {
			this.applicCritic.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#addBusinessFilterApplicCritic(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToApplicCritic(ViewerFilter filter) {
		applicCriticBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#isContainedInApplicCriticTable(EObject element)
	 * 
	 */
	public boolean isContainedInApplicCriticTable(EObject element) {
		return ((ReferencesTableSettings)applicCritic.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#initApplicCriticTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initApplicCriticTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		applicCriticTable.setContentProvider(contentProvider);
		applicCriticTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseApplicability.Properties.applicCriticTable);
		if (eefElementEditorReadOnlyState && applicCriticTable.getTable().isEnabled()) {
			applicCriticTable.getTable().setEnabled(false);
			applicCriticTable.getTable().setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
			addApplicCriticTable.setEnabled(false);
			addApplicCriticTable.setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
			removeApplicCriticTable.setEnabled(false);
			removeApplicCriticTable.setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
			editApplicCriticTable.setEnabled(false);
			editApplicCriticTable.setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicCriticTable.getTable().isEnabled()) {
			applicCriticTable.getTable().setEnabled(true);
			addApplicCriticTable.setEnabled(true);
			removeApplicCriticTable.setEnabled(true);
			editApplicCriticTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#updateApplicCriticTable()
	 * 
	 */
	public void updateApplicCriticTable() {
	applicCriticTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#addFilterApplicCriticTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicCriticTable(ViewerFilter filter) {
		applicCriticTableFilters.add(filter);
		if (this.applicCriticTable != null) {
			this.applicCriticTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#addBusinessFilterApplicCriticTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToApplicCriticTable(ViewerFilter filter) {
		applicCriticTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#isContainedInApplicCriticTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInApplicCriticTableTable(EObject element) {
		return ((ReferencesTableSettings)applicCriticTable.getInput()).contains(element);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#getComments()
	 * 
	 */
	public String getComments() {
		return comments.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#setComments(String newValue)
	 * 
	 */
	public void setComments(String newValue) {
		if (newValue != null) {
			comments.setText(newValue);
		} else {
			comments.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseApplicability.Properties.comments);
		if (eefElementEditorReadOnlyState && comments.isEnabled()) {
			comments.setEnabled(false);
			comments.setBackground(comments.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			comments.setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !comments.isEnabled()) {
			comments.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#getApplicTarget()
	 * 
	 */
	public EObject getApplicTarget() {
		if (applicTarget.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) applicTarget.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#initApplicTarget(EObjectFlatComboSettings)
	 */
	public void initApplicTarget(EObjectFlatComboSettings settings) {
		applicTarget.setInput(settings);
		if (current != null) {
			applicTarget.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseApplicability.Properties.applicTarget);
		if (eefElementEditorReadOnlyState && applicTarget.isEnabled()) {
			applicTarget.setEnabled(false);
			applicTarget.setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicTarget.isEnabled()) {
			applicTarget.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#setApplicTarget(EObject newValue)
	 * 
	 */
	public void setApplicTarget(EObject newValue) {
		if (newValue != null) {
			applicTarget.setSelection(new StructuredSelection(newValue));
		} else {
			applicTarget.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseApplicability.Properties.applicTarget);
		if (eefElementEditorReadOnlyState && applicTarget.isEnabled()) {
			applicTarget.setEnabled(false);
			applicTarget.setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !applicTarget.isEnabled()) {
			applicTarget.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#setApplicTargetButtonMode(ButtonsModeEnum newValue)
	 */
	public void setApplicTargetButtonMode(ButtonsModeEnum newValue) {
		applicTarget.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#addFilterApplicTarget(ViewerFilter filter)
	 * 
	 */
	public void addFilterToApplicTarget(ViewerFilter filter) {
		applicTarget.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#addBusinessFilterApplicTarget(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToApplicTarget(ViewerFilter filter) {
		applicTarget.addBusinessRuleFilter(filter);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#initOwnedRel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRel.setContentProvider(contentProvider);
		ownedRel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseApplicability.Properties.ownedRel);
		if (eefElementEditorReadOnlyState && ownedRel.isEnabled()) {
			ownedRel.setEnabled(false);
			ownedRel.setToolTipText(BaselineMessages.BaseApplicability_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRel.isEnabled()) {
			ownedRel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#updateOwnedRel()
	 * 
	 */
	public void updateOwnedRel() {
	ownedRel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#addFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRel(ViewerFilter filter) {
		ownedRelFilters.add(filter);
		if (this.ownedRel != null) {
			this.ownedRel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#addBusinessFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRel(ViewerFilter filter) {
		ownedRelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseApplicabilityPropertiesEditionPart#isContainedInOwnedRelTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRelTable(EObject element) {
		return ((ReferencesTableSettings)ownedRel.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseApplicability_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
