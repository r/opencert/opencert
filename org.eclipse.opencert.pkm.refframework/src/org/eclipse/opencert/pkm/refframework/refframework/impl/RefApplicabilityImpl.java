/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.infra.general.general.impl.NamedElementImpl;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ref Applicability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityImpl#getApplicCritic <em>Applic Critic</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityImpl#getComments <em>Comments</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityImpl#getApplicTarget <em>Applic Target</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityImpl#getOwnedRel <em>Owned Rel</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RefApplicabilityImpl extends NamedElementImpl implements RefApplicability {
	/**
	 * The default value of the '{@link #getComments() <em>Comments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComments()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMENTS_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefApplicabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RefframeworkPackage.Literals.REF_APPLICABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefCriticalityApplicability> getApplicCritic() {
		return (EList<RefCriticalityApplicability>)eDynamicGet(RefframeworkPackage.REF_APPLICABILITY__APPLIC_CRITIC, RefframeworkPackage.Literals.REF_APPLICABILITY__APPLIC_CRITIC, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComments() {
		return (String)eDynamicGet(RefframeworkPackage.REF_APPLICABILITY__COMMENTS, RefframeworkPackage.Literals.REF_APPLICABILITY__COMMENTS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComments(String newComments) {
		eDynamicSet(RefframeworkPackage.REF_APPLICABILITY__COMMENTS, RefframeworkPackage.Literals.REF_APPLICABILITY__COMMENTS, newComments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefAssurableElement getApplicTarget() {
		return (RefAssurableElement)eDynamicGet(RefframeworkPackage.REF_APPLICABILITY__APPLIC_TARGET, RefframeworkPackage.Literals.REF_APPLICABILITY__APPLIC_TARGET, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefAssurableElement basicGetApplicTarget() {
		return (RefAssurableElement)eDynamicGet(RefframeworkPackage.REF_APPLICABILITY__APPLIC_TARGET, RefframeworkPackage.Literals.REF_APPLICABILITY__APPLIC_TARGET, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicTarget(RefAssurableElement newApplicTarget) {
		eDynamicSet(RefframeworkPackage.REF_APPLICABILITY__APPLIC_TARGET, RefframeworkPackage.Literals.REF_APPLICABILITY__APPLIC_TARGET, newApplicTarget);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefApplicabilityRel> getOwnedRel() {
		return (EList<RefApplicabilityRel>)eDynamicGet(RefframeworkPackage.REF_APPLICABILITY__OWNED_REL, RefframeworkPackage.Literals.REF_APPLICABILITY__OWNED_REL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RefframeworkPackage.REF_APPLICABILITY__APPLIC_CRITIC:
				return ((InternalEList<?>)getApplicCritic()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_APPLICABILITY__OWNED_REL:
				return ((InternalEList<?>)getOwnedRel()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RefframeworkPackage.REF_APPLICABILITY__APPLIC_CRITIC:
				return getApplicCritic();
			case RefframeworkPackage.REF_APPLICABILITY__COMMENTS:
				return getComments();
			case RefframeworkPackage.REF_APPLICABILITY__APPLIC_TARGET:
				if (resolve) return getApplicTarget();
				return basicGetApplicTarget();
			case RefframeworkPackage.REF_APPLICABILITY__OWNED_REL:
				return getOwnedRel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RefframeworkPackage.REF_APPLICABILITY__APPLIC_CRITIC:
				getApplicCritic().clear();
				getApplicCritic().addAll((Collection<? extends RefCriticalityApplicability>)newValue);
				return;
			case RefframeworkPackage.REF_APPLICABILITY__COMMENTS:
				setComments((String)newValue);
				return;
			case RefframeworkPackage.REF_APPLICABILITY__APPLIC_TARGET:
				setApplicTarget((RefAssurableElement)newValue);
				return;
			case RefframeworkPackage.REF_APPLICABILITY__OWNED_REL:
				getOwnedRel().clear();
				getOwnedRel().addAll((Collection<? extends RefApplicabilityRel>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_APPLICABILITY__APPLIC_CRITIC:
				getApplicCritic().clear();
				return;
			case RefframeworkPackage.REF_APPLICABILITY__COMMENTS:
				setComments(COMMENTS_EDEFAULT);
				return;
			case RefframeworkPackage.REF_APPLICABILITY__APPLIC_TARGET:
				setApplicTarget((RefAssurableElement)null);
				return;
			case RefframeworkPackage.REF_APPLICABILITY__OWNED_REL:
				getOwnedRel().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_APPLICABILITY__APPLIC_CRITIC:
				return !getApplicCritic().isEmpty();
			case RefframeworkPackage.REF_APPLICABILITY__COMMENTS:
				return COMMENTS_EDEFAULT == null ? getComments() != null : !COMMENTS_EDEFAULT.equals(getComments());
			case RefframeworkPackage.REF_APPLICABILITY__APPLIC_TARGET:
				return basicGetApplicTarget() != null;
			case RefframeworkPackage.REF_APPLICABILITY__OWNED_REL:
				return !getOwnedRel().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RefApplicabilityImpl
