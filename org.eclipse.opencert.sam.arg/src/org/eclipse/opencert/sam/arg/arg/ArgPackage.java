/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.sam.arg.arg.ArgFactory
 * @model kind="package"
 * @generated
 */
public interface ArgPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "arg";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "arg";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "arg";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArgPackage eINSTANCE = org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.CaseImpl <em>Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.CaseImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getCase()
	 * @generated
	 */
	int CASE = 0;

	/**
	 * The feature id for the '<em><b>Argument</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE__ARGUMENT = 0;

	/**
	 * The feature id for the '<em><b>Argumentation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE__ARGUMENTATION = 1;

	/**
	 * The feature id for the '<em><b>Agreement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE__AGREEMENT = 2;

	/**
	 * The feature id for the '<em><b>Cited</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE__CITED = 3;

	/**
	 * The feature id for the '<em><b>Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE__INFORMATION = 4;

	/**
	 * The number of structural features of the '<em>Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ModelElementImpl <em>Model Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ModelElementImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getModelElement()
	 * @generated
	 */
	int MODEL_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT__ID = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT__NAME = 1;

	/**
	 * The number of structural features of the '<em>Model Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Model Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssuranceCaseImpl <em>Assurance Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.AssuranceCaseImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssuranceCase()
	 * @generated
	 */
	int ASSURANCE_CASE = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_CASE__ID = MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_CASE__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Has Argument</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_CASE__HAS_ARGUMENT = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Evidence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_CASE__HAS_EVIDENCE = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Composed Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_CASE__COMPOSED_OF = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Assurance Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_CASE_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Assurance Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_CASE_OPERATION_COUNT = MODEL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentationElementImpl <em>Argumentation Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgumentationElementImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getArgumentationElement()
	 * @generated
	 */
	int ARGUMENTATION_ELEMENT = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION_ELEMENT__ID = MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION_ELEMENT__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION_ELEMENT__DESCRIPTION = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION_ELEMENT__CONTENT = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Argumentation Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION_ELEMENT_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Argumentation Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION_ELEMENT_OPERATION_COUNT = MODEL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl <em>Argumentation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getArgumentation()
	 * @generated
	 */
	int ARGUMENTATION = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION__ID = ARGUMENTATION_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION__NAME = ARGUMENTATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION__DESCRIPTION = ARGUMENTATION_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION__CONTENT = ARGUMENTATION_ELEMENT__CONTENT;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION__EVALUATION = ARGUMENTATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lifecycle Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION__LIFECYCLE_EVENT = ARGUMENTATION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION__LOCATION = ARGUMENTATION_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Argumentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION__ARGUMENTATION = ARGUMENTATION_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Consist Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION__CONSIST_OF = ARGUMENTATION_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Argumentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION_FEATURE_COUNT = ARGUMENTATION_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Argumentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENTATION_OPERATION_COUNT = ARGUMENTATION_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementImpl <em>Argument Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getArgumentElement()
	 * @generated
	 */
	int ARGUMENT_ELEMENT = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT__ID = ARGUMENTATION_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT__NAME = ARGUMENTATION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT__DESCRIPTION = ARGUMENTATION_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT__CONTENT = ARGUMENTATION_ELEMENT__CONTENT;

	/**
	 * The number of structural features of the '<em>Argument Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT_FEATURE_COUNT = ARGUMENTATION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Argument Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT_OPERATION_COUNT = ARGUMENTATION_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.InformationElementCitationImpl <em>Information Element Citation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.InformationElementCitationImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getInformationElementCitation()
	 * @generated
	 */
	int INFORMATION_ELEMENT_CITATION = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATION_ELEMENT_CITATION__ID = ARGUMENT_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATION_ELEMENT_CITATION__NAME = ARGUMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATION_ELEMENT_CITATION__DESCRIPTION = ARGUMENT_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATION_ELEMENT_CITATION__CONTENT = ARGUMENT_ELEMENT__CONTENT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATION_ELEMENT_CITATION__TYPE = ARGUMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>To Be Instantiated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED = ARGUMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATION_ELEMENT_CITATION__URL = ARGUMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATION_ELEMENT_CITATION__ARTEFACT = ARGUMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Information Element Citation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATION_ELEMENT_CITATION_FEATURE_COUNT = ARGUMENT_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Information Element Citation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATION_ELEMENT_CITATION_OPERATION_COUNT = ARGUMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ReasoningElementImpl <em>Reasoning Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ReasoningElementImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getReasoningElement()
	 * @generated
	 */
	int REASONING_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REASONING_ELEMENT__ID = ARGUMENT_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REASONING_ELEMENT__NAME = ARGUMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REASONING_ELEMENT__DESCRIPTION = ARGUMENT_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REASONING_ELEMENT__CONTENT = ARGUMENT_ELEMENT__CONTENT;

	/**
	 * The number of structural features of the '<em>Reasoning Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REASONING_ELEMENT_FEATURE_COUNT = ARGUMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Reasoning Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REASONING_ELEMENT_OPERATION_COUNT = ARGUMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementCitationImpl <em>Argument Element Citation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementCitationImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getArgumentElementCitation()
	 * @generated
	 */
	int ARGUMENT_ELEMENT_CITATION = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT_CITATION__ID = ARGUMENT_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT_CITATION__NAME = ARGUMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT_CITATION__DESCRIPTION = ARGUMENT_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT_CITATION__CONTENT = ARGUMENT_ELEMENT__CONTENT;

	/**
	 * The feature id for the '<em><b>Cited Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT_CITATION__CITED_TYPE = ARGUMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Argumentation Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE = ARGUMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Argument Element Citation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT_CITATION_FEATURE_COUNT = ARGUMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Argument Element Citation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_ELEMENT_CITATION_OPERATION_COUNT = ARGUMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertionImpl <em>Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertionImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertion()
	 * @generated
	 */
	int ASSERTION = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__ID = REASONING_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__NAME = REASONING_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__DESCRIPTION = REASONING_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__CONTENT = REASONING_ELEMENT__CONTENT;

	/**
	 * The number of structural features of the '<em>Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_FEATURE_COUNT = REASONING_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_OPERATION_COUNT = REASONING_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentReasoningImpl <em>Argument Reasoning</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgumentReasoningImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getArgumentReasoning()
	 * @generated
	 */
	int ARGUMENT_REASONING = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_REASONING__ID = REASONING_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_REASONING__NAME = REASONING_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_REASONING__DESCRIPTION = REASONING_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_REASONING__CONTENT = REASONING_ELEMENT__CONTENT;

	/**
	 * The feature id for the '<em><b>To Be Supported</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_REASONING__TO_BE_SUPPORTED = REASONING_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>To Be Instantiated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_REASONING__TO_BE_INSTANTIATED = REASONING_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has Structure</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_REASONING__HAS_STRUCTURE = REASONING_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Argument Reasoning</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_REASONING_FEATURE_COUNT = REASONING_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Argument Reasoning</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_REASONING_OPERATION_COUNT = REASONING_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl <em>Claim</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getClaim()
	 * @generated
	 */
	int CLAIM = 11;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__ID = ASSERTION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__NAME = ASSERTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__DESCRIPTION = ASSERTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__CONTENT = ASSERTION__CONTENT;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__EVALUATION = ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lifecycle Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__LIFECYCLE_EVENT = ASSERTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__PUBLIC = ASSERTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Assumed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__ASSUMED = ASSERTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>To Be Supported</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__TO_BE_SUPPORTED = ASSERTION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>To Be Instantiated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__TO_BE_INSTANTIATED = ASSERTION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Choice</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM__CHOICE = ASSERTION_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Claim</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_FEATURE_COUNT = ASSERTION_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Claim</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAIM_OPERATION_COUNT = ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedRelationshipImpl <em>Asserted Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedRelationshipImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedRelationship()
	 * @generated
	 */
	int ASSERTED_RELATIONSHIP = 12;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_RELATIONSHIP__ID = ASSERTION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_RELATIONSHIP__NAME = ASSERTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_RELATIONSHIP__DESCRIPTION = ASSERTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_RELATIONSHIP__CONTENT = ASSERTION__CONTENT;

	/**
	 * The number of structural features of the '<em>Asserted Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_RELATIONSHIP_FEATURE_COUNT = ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Asserted Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_RELATIONSHIP_OPERATION_COUNT = ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ChoiceImpl <em>Choice</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ChoiceImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getChoice()
	 * @generated
	 */
	int CHOICE = 13;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE__ID = ARGUMENT_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE__NAME = ARGUMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE__DESCRIPTION = ARGUMENT_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE__CONTENT = ARGUMENT_ELEMENT__CONTENT;

	/**
	 * The feature id for the '<em><b>Source Multiextension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE__SOURCE_MULTIEXTENSION = ARGUMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE__SOURCE_CARDINALITY = ARGUMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Optionality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE__OPTIONALITY = ARGUMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_FEATURE_COUNT = ARGUMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_OPERATION_COUNT = ARGUMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedInferenceImpl <em>Asserted Inference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedInferenceImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedInference()
	 * @generated
	 */
	int ASSERTED_INFERENCE = 14;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_INFERENCE__ID = ASSERTED_RELATIONSHIP__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_INFERENCE__NAME = ASSERTED_RELATIONSHIP__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_INFERENCE__DESCRIPTION = ASSERTED_RELATIONSHIP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_INFERENCE__CONTENT = ASSERTED_RELATIONSHIP__CONTENT;

	/**
	 * The feature id for the '<em><b>Multiextension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_INFERENCE__MULTIEXTENSION = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_INFERENCE__CARDINALITY = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_INFERENCE__SOURCE = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_INFERENCE__TARGET = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Asserted Inference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_INFERENCE_FEATURE_COUNT = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Asserted Inference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_INFERENCE_OPERATION_COUNT = ASSERTED_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedEvidenceImpl <em>Asserted Evidence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedEvidenceImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedEvidence()
	 * @generated
	 */
	int ASSERTED_EVIDENCE = 15;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_EVIDENCE__ID = ASSERTED_RELATIONSHIP__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_EVIDENCE__NAME = ASSERTED_RELATIONSHIP__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_EVIDENCE__DESCRIPTION = ASSERTED_RELATIONSHIP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_EVIDENCE__CONTENT = ASSERTED_RELATIONSHIP__CONTENT;

	/**
	 * The feature id for the '<em><b>Multiextension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_EVIDENCE__MULTIEXTENSION = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_EVIDENCE__CARDINALITY = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_EVIDENCE__SOURCE = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_EVIDENCE__TARGET = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Asserted Evidence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_EVIDENCE_FEATURE_COUNT = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Asserted Evidence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_EVIDENCE_OPERATION_COUNT = ASSERTED_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl <em>Asserted Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedContext()
	 * @generated
	 */
	int ASSERTED_CONTEXT = 16;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CONTEXT__ID = ASSERTED_RELATIONSHIP__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CONTEXT__NAME = ASSERTED_RELATIONSHIP__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CONTEXT__DESCRIPTION = ASSERTED_RELATIONSHIP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CONTEXT__CONTENT = ASSERTED_RELATIONSHIP__CONTENT;

	/**
	 * The feature id for the '<em><b>Multiextension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CONTEXT__MULTIEXTENSION = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CONTEXT__CARDINALITY = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CONTEXT__SOURCE = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CONTEXT__TARGET = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Asserted Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CONTEXT_FEATURE_COUNT = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Asserted Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CONTEXT_OPERATION_COUNT = ASSERTED_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedChallengeImpl <em>Asserted Challenge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedChallengeImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedChallenge()
	 * @generated
	 */
	int ASSERTED_CHALLENGE = 17;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CHALLENGE__ID = ASSERTED_RELATIONSHIP__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CHALLENGE__NAME = ASSERTED_RELATIONSHIP__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CHALLENGE__DESCRIPTION = ASSERTED_RELATIONSHIP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CHALLENGE__CONTENT = ASSERTED_RELATIONSHIP__CONTENT;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CHALLENGE__SOURCE = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CHALLENGE__TARGET = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Asserted Challenge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CHALLENGE_FEATURE_COUNT = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Asserted Challenge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_CHALLENGE_OPERATION_COUNT = ASSERTED_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedCounterEvidenceImpl <em>Asserted Counter Evidence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedCounterEvidenceImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedCounterEvidence()
	 * @generated
	 */
	int ASSERTED_COUNTER_EVIDENCE = 18;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_COUNTER_EVIDENCE__ID = ASSERTED_RELATIONSHIP__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_COUNTER_EVIDENCE__NAME = ASSERTED_RELATIONSHIP__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_COUNTER_EVIDENCE__DESCRIPTION = ASSERTED_RELATIONSHIP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_COUNTER_EVIDENCE__CONTENT = ASSERTED_RELATIONSHIP__CONTENT;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_COUNTER_EVIDENCE__SOURCE = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_COUNTER_EVIDENCE__TARGET = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Asserted Counter Evidence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_COUNTER_EVIDENCE_FEATURE_COUNT = ASSERTED_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Asserted Counter Evidence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTED_COUNTER_EVIDENCE_OPERATION_COUNT = ASSERTED_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AgreementImpl <em>Agreement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.impl.AgreementImpl
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAgreement()
	 * @generated
	 */
	int AGREEMENT = 19;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT__ID = ARGUMENTATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT__NAME = ARGUMENTATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT__DESCRIPTION = ARGUMENTATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT__CONTENT = ARGUMENTATION__CONTENT;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT__EVALUATION = ARGUMENTATION__EVALUATION;

	/**
	 * The feature id for the '<em><b>Lifecycle Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT__LIFECYCLE_EVENT = ARGUMENTATION__LIFECYCLE_EVENT;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT__LOCATION = ARGUMENTATION__LOCATION;

	/**
	 * The feature id for the '<em><b>Argumentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT__ARGUMENTATION = ARGUMENTATION__ARGUMENTATION;

	/**
	 * The feature id for the '<em><b>Consist Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT__CONSIST_OF = ARGUMENTATION__CONSIST_OF;

	/**
	 * The feature id for the '<em><b>Between</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT__BETWEEN = ARGUMENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Agreement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT_FEATURE_COUNT = ARGUMENTATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Agreement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGREEMENT_OPERATION_COUNT = ARGUMENTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension <em>Asserted By Multiplicity Extension</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedByMultiplicityExtension()
	 * @generated
	 */
	int ASSERTED_BY_MULTIPLICITY_EXTENSION = 20;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.InformationElementType <em>Information Element Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.InformationElementType
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getInformationElementType()
	 * @generated
	 */
	int INFORMATION_ELEMENT_TYPE = 21;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.sam.arg.arg.CitationElementType <em>Citation Element Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.sam.arg.arg.CitationElementType
	 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getCitationElementType()
	 * @generated
	 */
	int CITATION_ELEMENT_TYPE = 22;


	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.Case <em>Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Case
	 * @generated
	 */
	EClass getCase();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.sam.arg.arg.Case#getArgument <em>Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Argument</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Case#getArgument()
	 * @see #getCase()
	 * @generated
	 */
	EReference getCase_Argument();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.sam.arg.arg.Case#getArgumentation <em>Argumentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Argumentation</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Case#getArgumentation()
	 * @see #getCase()
	 * @generated
	 */
	EReference getCase_Argumentation();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.sam.arg.arg.Case#getAgreement <em>Agreement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Agreement</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Case#getAgreement()
	 * @see #getCase()
	 * @generated
	 */
	EReference getCase_Agreement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.sam.arg.arg.Case#getCited <em>Cited</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cited</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Case#getCited()
	 * @see #getCase()
	 * @generated
	 */
	EReference getCase_Cited();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.sam.arg.arg.Case#getInformation <em>Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Information</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Case#getInformation()
	 * @see #getCase()
	 * @generated
	 */
	EReference getCase_Information();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.ModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Element</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ModelElement
	 * @generated
	 */
	EClass getModelElement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.ModelElement#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ModelElement#getId()
	 * @see #getModelElement()
	 * @generated
	 */
	EAttribute getModelElement_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.ModelElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ModelElement#getName()
	 * @see #getModelElement()
	 * @generated
	 */
	EAttribute getModelElement_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.AssuranceCase <em>Assurance Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assurance Case</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssuranceCase
	 * @generated
	 */
	EClass getAssuranceCase();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.sam.arg.arg.AssuranceCase#getHasArgument <em>Has Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Argument</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssuranceCase#getHasArgument()
	 * @see #getAssuranceCase()
	 * @generated
	 */
	EReference getAssuranceCase_HasArgument();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.sam.arg.arg.AssuranceCase#getHasEvidence <em>Has Evidence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Evidence</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssuranceCase#getHasEvidence()
	 * @see #getAssuranceCase()
	 * @generated
	 */
	EReference getAssuranceCase_HasEvidence();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssuranceCase#getComposedOf <em>Composed Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Composed Of</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssuranceCase#getComposedOf()
	 * @see #getAssuranceCase()
	 * @generated
	 */
	EReference getAssuranceCase_ComposedOf();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.ArgumentationElement <em>Argumentation Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Argumentation Element</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentationElement
	 * @generated
	 */
	EClass getArgumentationElement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.ArgumentationElement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentationElement#getDescription()
	 * @see #getArgumentationElement()
	 * @generated
	 */
	EAttribute getArgumentationElement_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.ArgumentationElement#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentationElement#getContent()
	 * @see #getArgumentationElement()
	 * @generated
	 */
	EAttribute getArgumentationElement_Content();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.Argumentation <em>Argumentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Argumentation</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Argumentation
	 * @generated
	 */
	EClass getArgumentation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.Argumentation#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Argumentation#getLocation()
	 * @see #getArgumentation()
	 * @generated
	 */
	EAttribute getArgumentation_Location();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.Argumentation#getArgumentation <em>Argumentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Argumentation</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Argumentation#getArgumentation()
	 * @see #getArgumentation()
	 * @generated
	 */
	EReference getArgumentation_Argumentation();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.Argumentation#getConsistOf <em>Consist Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Consist Of</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Argumentation#getConsistOf()
	 * @see #getArgumentation()
	 * @generated
	 */
	EReference getArgumentation_ConsistOf();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.ArgumentElement <em>Argument Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Argument Element</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentElement
	 * @generated
	 */
	EClass getArgumentElement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation <em>Information Element Citation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Information Element Citation</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.InformationElementCitation
	 * @generated
	 */
	EClass getInformationElementCitation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getType()
	 * @see #getInformationElementCitation()
	 * @generated
	 */
	EAttribute getInformationElementCitation_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getToBeInstantiated <em>To Be Instantiated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To Be Instantiated</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getToBeInstantiated()
	 * @see #getInformationElementCitation()
	 * @generated
	 */
	EAttribute getInformationElementCitation_ToBeInstantiated();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getUrl()
	 * @see #getInformationElementCitation()
	 * @generated
	 */
	EAttribute getInformationElementCitation_Url();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getArtefact <em>Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Artefact</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getArtefact()
	 * @see #getInformationElementCitation()
	 * @generated
	 */
	EReference getInformationElementCitation_Artefact();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.ReasoningElement <em>Reasoning Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reasoning Element</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ReasoningElement
	 * @generated
	 */
	EClass getReasoningElement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation <em>Argument Element Citation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Argument Element Citation</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation
	 * @generated
	 */
	EClass getArgumentElementCitation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation#getCitedType <em>Cited Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cited Type</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation#getCitedType()
	 * @see #getArgumentElementCitation()
	 * @generated
	 */
	EAttribute getArgumentElementCitation_CitedType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation#getArgumentationReference <em>Argumentation Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Argumentation Reference</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation#getArgumentationReference()
	 * @see #getArgumentElementCitation()
	 * @generated
	 */
	EAttribute getArgumentElementCitation_ArgumentationReference();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.Assertion <em>Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Assertion
	 * @generated
	 */
	EClass getAssertion();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning <em>Argument Reasoning</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Argument Reasoning</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentReasoning
	 * @generated
	 */
	EClass getArgumentReasoning();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getToBeSupported <em>To Be Supported</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To Be Supported</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getToBeSupported()
	 * @see #getArgumentReasoning()
	 * @generated
	 */
	EAttribute getArgumentReasoning_ToBeSupported();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getToBeInstantiated <em>To Be Instantiated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To Be Instantiated</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getToBeInstantiated()
	 * @see #getArgumentReasoning()
	 * @generated
	 */
	EAttribute getArgumentReasoning_ToBeInstantiated();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getHasStructure <em>Has Structure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Has Structure</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getHasStructure()
	 * @see #getArgumentReasoning()
	 * @generated
	 */
	EReference getArgumentReasoning_HasStructure();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.Claim <em>Claim</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Claim</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Claim
	 * @generated
	 */
	EClass getClaim();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.Claim#getPublic <em>Public</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Public</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Claim#getPublic()
	 * @see #getClaim()
	 * @generated
	 */
	EAttribute getClaim_Public();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.Claim#getAssumed <em>Assumed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assumed</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Claim#getAssumed()
	 * @see #getClaim()
	 * @generated
	 */
	EAttribute getClaim_Assumed();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.Claim#getToBeSupported <em>To Be Supported</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To Be Supported</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Claim#getToBeSupported()
	 * @see #getClaim()
	 * @generated
	 */
	EAttribute getClaim_ToBeSupported();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.Claim#getToBeInstantiated <em>To Be Instantiated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To Be Instantiated</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Claim#getToBeInstantiated()
	 * @see #getClaim()
	 * @generated
	 */
	EAttribute getClaim_ToBeInstantiated();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.opencert.sam.arg.arg.Claim#getChoice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Choice</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Claim#getChoice()
	 * @see #getClaim()
	 * @generated
	 */
	EReference getClaim_Choice();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.AssertedRelationship <em>Asserted Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asserted Relationship</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedRelationship
	 * @generated
	 */
	EClass getAssertedRelationship();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.Choice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Choice</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Choice
	 * @generated
	 */
	EClass getChoice();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.Choice#getSourceMultiextension <em>Source Multiextension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Multiextension</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Choice#getSourceMultiextension()
	 * @see #getChoice()
	 * @generated
	 */
	EAttribute getChoice_SourceMultiextension();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.Choice#getSourceCardinality <em>Source Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Cardinality</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Choice#getSourceCardinality()
	 * @see #getChoice()
	 * @generated
	 */
	EAttribute getChoice_SourceCardinality();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.Choice#getOptionality <em>Optionality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optionality</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Choice#getOptionality()
	 * @see #getChoice()
	 * @generated
	 */
	EAttribute getChoice_Optionality();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.AssertedInference <em>Asserted Inference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asserted Inference</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedInference
	 * @generated
	 */
	EClass getAssertedInference();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.AssertedInference#getMultiextension <em>Multiextension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multiextension</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedInference#getMultiextension()
	 * @see #getAssertedInference()
	 * @generated
	 */
	EAttribute getAssertedInference_Multiextension();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.AssertedInference#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedInference#getCardinality()
	 * @see #getAssertedInference()
	 * @generated
	 */
	EAttribute getAssertedInference_Cardinality();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssertedInference#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedInference#getSource()
	 * @see #getAssertedInference()
	 * @generated
	 */
	EReference getAssertedInference_Source();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssertedInference#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedInference#getTarget()
	 * @see #getAssertedInference()
	 * @generated
	 */
	EReference getAssertedInference_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.AssertedEvidence <em>Asserted Evidence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asserted Evidence</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedEvidence
	 * @generated
	 */
	EClass getAssertedEvidence();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.AssertedEvidence#getMultiextension <em>Multiextension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multiextension</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedEvidence#getMultiextension()
	 * @see #getAssertedEvidence()
	 * @generated
	 */
	EAttribute getAssertedEvidence_Multiextension();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.AssertedEvidence#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedEvidence#getCardinality()
	 * @see #getAssertedEvidence()
	 * @generated
	 */
	EAttribute getAssertedEvidence_Cardinality();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssertedEvidence#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedEvidence#getSource()
	 * @see #getAssertedEvidence()
	 * @generated
	 */
	EReference getAssertedEvidence_Source();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssertedEvidence#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedEvidence#getTarget()
	 * @see #getAssertedEvidence()
	 * @generated
	 */
	EReference getAssertedEvidence_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.AssertedContext <em>Asserted Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asserted Context</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedContext
	 * @generated
	 */
	EClass getAssertedContext();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.AssertedContext#getMultiextension <em>Multiextension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multiextension</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedContext#getMultiextension()
	 * @see #getAssertedContext()
	 * @generated
	 */
	EAttribute getAssertedContext_Multiextension();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.sam.arg.arg.AssertedContext#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedContext#getCardinality()
	 * @see #getAssertedContext()
	 * @generated
	 */
	EAttribute getAssertedContext_Cardinality();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssertedContext#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedContext#getSource()
	 * @see #getAssertedContext()
	 * @generated
	 */
	EReference getAssertedContext_Source();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssertedContext#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedContext#getTarget()
	 * @see #getAssertedContext()
	 * @generated
	 */
	EReference getAssertedContext_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.AssertedChallenge <em>Asserted Challenge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asserted Challenge</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedChallenge
	 * @generated
	 */
	EClass getAssertedChallenge();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssertedChallenge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedChallenge#getSource()
	 * @see #getAssertedChallenge()
	 * @generated
	 */
	EReference getAssertedChallenge_Source();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssertedChallenge#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedChallenge#getTarget()
	 * @see #getAssertedChallenge()
	 * @generated
	 */
	EReference getAssertedChallenge_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence <em>Asserted Counter Evidence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asserted Counter Evidence</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence
	 * @generated
	 */
	EClass getAssertedCounterEvidence();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence#getSource()
	 * @see #getAssertedCounterEvidence()
	 * @generated
	 */
	EReference getAssertedCounterEvidence_Source();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence#getTarget()
	 * @see #getAssertedCounterEvidence()
	 * @generated
	 */
	EReference getAssertedCounterEvidence_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.sam.arg.arg.Agreement <em>Agreement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agreement</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Agreement
	 * @generated
	 */
	EClass getAgreement();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.sam.arg.arg.Agreement#getBetween <em>Between</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Between</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.Agreement#getBetween()
	 * @see #getAgreement()
	 * @generated
	 */
	EReference getAgreement_Between();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension <em>Asserted By Multiplicity Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Asserted By Multiplicity Extension</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension
	 * @generated
	 */
	EEnum getAssertedByMultiplicityExtension();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.opencert.sam.arg.arg.InformationElementType <em>Information Element Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Information Element Type</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.InformationElementType
	 * @generated
	 */
	EEnum getInformationElementType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.opencert.sam.arg.arg.CitationElementType <em>Citation Element Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Citation Element Type</em>'.
	 * @see org.eclipse.opencert.sam.arg.arg.CitationElementType
	 * @generated
	 */
	EEnum getCitationElementType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ArgFactory getArgFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.CaseImpl <em>Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.CaseImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getCase()
		 * @generated
		 */
		EClass CASE = eINSTANCE.getCase();

		/**
		 * The meta object literal for the '<em><b>Argument</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE__ARGUMENT = eINSTANCE.getCase_Argument();

		/**
		 * The meta object literal for the '<em><b>Argumentation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE__ARGUMENTATION = eINSTANCE.getCase_Argumentation();

		/**
		 * The meta object literal for the '<em><b>Agreement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE__AGREEMENT = eINSTANCE.getCase_Agreement();

		/**
		 * The meta object literal for the '<em><b>Cited</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE__CITED = eINSTANCE.getCase_Cited();

		/**
		 * The meta object literal for the '<em><b>Information</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE__INFORMATION = eINSTANCE.getCase_Information();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ModelElementImpl <em>Model Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ModelElementImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getModelElement()
		 * @generated
		 */
		EClass MODEL_ELEMENT = eINSTANCE.getModelElement();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_ELEMENT__ID = eINSTANCE.getModelElement_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_ELEMENT__NAME = eINSTANCE.getModelElement_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssuranceCaseImpl <em>Assurance Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.AssuranceCaseImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssuranceCase()
		 * @generated
		 */
		EClass ASSURANCE_CASE = eINSTANCE.getAssuranceCase();

		/**
		 * The meta object literal for the '<em><b>Has Argument</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSURANCE_CASE__HAS_ARGUMENT = eINSTANCE.getAssuranceCase_HasArgument();

		/**
		 * The meta object literal for the '<em><b>Has Evidence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSURANCE_CASE__HAS_EVIDENCE = eINSTANCE.getAssuranceCase_HasEvidence();

		/**
		 * The meta object literal for the '<em><b>Composed Of</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSURANCE_CASE__COMPOSED_OF = eINSTANCE.getAssuranceCase_ComposedOf();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentationElementImpl <em>Argumentation Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgumentationElementImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getArgumentationElement()
		 * @generated
		 */
		EClass ARGUMENTATION_ELEMENT = eINSTANCE.getArgumentationElement();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARGUMENTATION_ELEMENT__DESCRIPTION = eINSTANCE.getArgumentationElement_Description();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARGUMENTATION_ELEMENT__CONTENT = eINSTANCE.getArgumentationElement_Content();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl <em>Argumentation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getArgumentation()
		 * @generated
		 */
		EClass ARGUMENTATION = eINSTANCE.getArgumentation();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARGUMENTATION__LOCATION = eINSTANCE.getArgumentation_Location();

		/**
		 * The meta object literal for the '<em><b>Argumentation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARGUMENTATION__ARGUMENTATION = eINSTANCE.getArgumentation_Argumentation();

		/**
		 * The meta object literal for the '<em><b>Consist Of</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARGUMENTATION__CONSIST_OF = eINSTANCE.getArgumentation_ConsistOf();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementImpl <em>Argument Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getArgumentElement()
		 * @generated
		 */
		EClass ARGUMENT_ELEMENT = eINSTANCE.getArgumentElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.InformationElementCitationImpl <em>Information Element Citation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.InformationElementCitationImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getInformationElementCitation()
		 * @generated
		 */
		EClass INFORMATION_ELEMENT_CITATION = eINSTANCE.getInformationElementCitation();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INFORMATION_ELEMENT_CITATION__TYPE = eINSTANCE.getInformationElementCitation_Type();

		/**
		 * The meta object literal for the '<em><b>To Be Instantiated</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INFORMATION_ELEMENT_CITATION__TO_BE_INSTANTIATED = eINSTANCE.getInformationElementCitation_ToBeInstantiated();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INFORMATION_ELEMENT_CITATION__URL = eINSTANCE.getInformationElementCitation_Url();

		/**
		 * The meta object literal for the '<em><b>Artefact</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INFORMATION_ELEMENT_CITATION__ARTEFACT = eINSTANCE.getInformationElementCitation_Artefact();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ReasoningElementImpl <em>Reasoning Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ReasoningElementImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getReasoningElement()
		 * @generated
		 */
		EClass REASONING_ELEMENT = eINSTANCE.getReasoningElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementCitationImpl <em>Argument Element Citation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementCitationImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getArgumentElementCitation()
		 * @generated
		 */
		EClass ARGUMENT_ELEMENT_CITATION = eINSTANCE.getArgumentElementCitation();

		/**
		 * The meta object literal for the '<em><b>Cited Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARGUMENT_ELEMENT_CITATION__CITED_TYPE = eINSTANCE.getArgumentElementCitation_CitedType();

		/**
		 * The meta object literal for the '<em><b>Argumentation Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE = eINSTANCE.getArgumentElementCitation_ArgumentationReference();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertionImpl <em>Assertion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertionImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertion()
		 * @generated
		 */
		EClass ASSERTION = eINSTANCE.getAssertion();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentReasoningImpl <em>Argument Reasoning</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgumentReasoningImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getArgumentReasoning()
		 * @generated
		 */
		EClass ARGUMENT_REASONING = eINSTANCE.getArgumentReasoning();

		/**
		 * The meta object literal for the '<em><b>To Be Supported</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARGUMENT_REASONING__TO_BE_SUPPORTED = eINSTANCE.getArgumentReasoning_ToBeSupported();

		/**
		 * The meta object literal for the '<em><b>To Be Instantiated</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARGUMENT_REASONING__TO_BE_INSTANTIATED = eINSTANCE.getArgumentReasoning_ToBeInstantiated();

		/**
		 * The meta object literal for the '<em><b>Has Structure</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARGUMENT_REASONING__HAS_STRUCTURE = eINSTANCE.getArgumentReasoning_HasStructure();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl <em>Claim</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getClaim()
		 * @generated
		 */
		EClass CLAIM = eINSTANCE.getClaim();

		/**
		 * The meta object literal for the '<em><b>Public</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLAIM__PUBLIC = eINSTANCE.getClaim_Public();

		/**
		 * The meta object literal for the '<em><b>Assumed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLAIM__ASSUMED = eINSTANCE.getClaim_Assumed();

		/**
		 * The meta object literal for the '<em><b>To Be Supported</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLAIM__TO_BE_SUPPORTED = eINSTANCE.getClaim_ToBeSupported();

		/**
		 * The meta object literal for the '<em><b>To Be Instantiated</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLAIM__TO_BE_INSTANTIATED = eINSTANCE.getClaim_ToBeInstantiated();

		/**
		 * The meta object literal for the '<em><b>Choice</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLAIM__CHOICE = eINSTANCE.getClaim_Choice();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedRelationshipImpl <em>Asserted Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedRelationshipImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedRelationship()
		 * @generated
		 */
		EClass ASSERTED_RELATIONSHIP = eINSTANCE.getAssertedRelationship();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.ChoiceImpl <em>Choice</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ChoiceImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getChoice()
		 * @generated
		 */
		EClass CHOICE = eINSTANCE.getChoice();

		/**
		 * The meta object literal for the '<em><b>Source Multiextension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHOICE__SOURCE_MULTIEXTENSION = eINSTANCE.getChoice_SourceMultiextension();

		/**
		 * The meta object literal for the '<em><b>Source Cardinality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHOICE__SOURCE_CARDINALITY = eINSTANCE.getChoice_SourceCardinality();

		/**
		 * The meta object literal for the '<em><b>Optionality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHOICE__OPTIONALITY = eINSTANCE.getChoice_Optionality();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedInferenceImpl <em>Asserted Inference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedInferenceImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedInference()
		 * @generated
		 */
		EClass ASSERTED_INFERENCE = eINSTANCE.getAssertedInference();

		/**
		 * The meta object literal for the '<em><b>Multiextension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERTED_INFERENCE__MULTIEXTENSION = eINSTANCE.getAssertedInference_Multiextension();

		/**
		 * The meta object literal for the '<em><b>Cardinality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERTED_INFERENCE__CARDINALITY = eINSTANCE.getAssertedInference_Cardinality();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTED_INFERENCE__SOURCE = eINSTANCE.getAssertedInference_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTED_INFERENCE__TARGET = eINSTANCE.getAssertedInference_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedEvidenceImpl <em>Asserted Evidence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedEvidenceImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedEvidence()
		 * @generated
		 */
		EClass ASSERTED_EVIDENCE = eINSTANCE.getAssertedEvidence();

		/**
		 * The meta object literal for the '<em><b>Multiextension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERTED_EVIDENCE__MULTIEXTENSION = eINSTANCE.getAssertedEvidence_Multiextension();

		/**
		 * The meta object literal for the '<em><b>Cardinality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERTED_EVIDENCE__CARDINALITY = eINSTANCE.getAssertedEvidence_Cardinality();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTED_EVIDENCE__SOURCE = eINSTANCE.getAssertedEvidence_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTED_EVIDENCE__TARGET = eINSTANCE.getAssertedEvidence_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl <em>Asserted Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedContext()
		 * @generated
		 */
		EClass ASSERTED_CONTEXT = eINSTANCE.getAssertedContext();

		/**
		 * The meta object literal for the '<em><b>Multiextension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERTED_CONTEXT__MULTIEXTENSION = eINSTANCE.getAssertedContext_Multiextension();

		/**
		 * The meta object literal for the '<em><b>Cardinality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERTED_CONTEXT__CARDINALITY = eINSTANCE.getAssertedContext_Cardinality();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTED_CONTEXT__SOURCE = eINSTANCE.getAssertedContext_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTED_CONTEXT__TARGET = eINSTANCE.getAssertedContext_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedChallengeImpl <em>Asserted Challenge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedChallengeImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedChallenge()
		 * @generated
		 */
		EClass ASSERTED_CHALLENGE = eINSTANCE.getAssertedChallenge();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTED_CHALLENGE__SOURCE = eINSTANCE.getAssertedChallenge_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTED_CHALLENGE__TARGET = eINSTANCE.getAssertedChallenge_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedCounterEvidenceImpl <em>Asserted Counter Evidence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.AssertedCounterEvidenceImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedCounterEvidence()
		 * @generated
		 */
		EClass ASSERTED_COUNTER_EVIDENCE = eINSTANCE.getAssertedCounterEvidence();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTED_COUNTER_EVIDENCE__SOURCE = eINSTANCE.getAssertedCounterEvidence_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTED_COUNTER_EVIDENCE__TARGET = eINSTANCE.getAssertedCounterEvidence_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.impl.AgreementImpl <em>Agreement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.impl.AgreementImpl
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAgreement()
		 * @generated
		 */
		EClass AGREEMENT = eINSTANCE.getAgreement();

		/**
		 * The meta object literal for the '<em><b>Between</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGREEMENT__BETWEEN = eINSTANCE.getAgreement_Between();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension <em>Asserted By Multiplicity Extension</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getAssertedByMultiplicityExtension()
		 * @generated
		 */
		EEnum ASSERTED_BY_MULTIPLICITY_EXTENSION = eINSTANCE.getAssertedByMultiplicityExtension();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.InformationElementType <em>Information Element Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.InformationElementType
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getInformationElementType()
		 * @generated
		 */
		EEnum INFORMATION_ELEMENT_TYPE = eINSTANCE.getInformationElementType();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.sam.arg.arg.CitationElementType <em>Citation Element Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.sam.arg.arg.CitationElementType
		 * @see org.eclipse.opencert.sam.arg.arg.impl.ArgPackageImpl#getCitationElementType()
		 * @generated
		 */
		EEnum CITATION_ELEMENT_TYPE = eINSTANCE.getCitationElementType();

	}

} //ArgPackage
