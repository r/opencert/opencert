/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages;

/**
 * Page describing the activities related to the standard compliance definition.
 */
public class StandardComplianceDefinitionPage extends AbstractActivityPage {

	/**
	 * Default constructor.
	 */
	public StandardComplianceDefinitionPage() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getHeaderTitle() {
		return "Standard Compliance Definition";
	}
}
