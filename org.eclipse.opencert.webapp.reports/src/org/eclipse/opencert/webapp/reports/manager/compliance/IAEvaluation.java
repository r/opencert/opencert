/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.impactanalysis.eventcreators.IChangeDrivenEventsCreator;

public class IAEvaluation
{
    private final IAEvaluationStatus evaluationStatus;
    private final AssuranceAssetEvent assuranceAssetEvent;
    
    public static IAEvaluation createForArtefact(Artefact artefact)
    {
        int eventsCount = artefact.getLifecycleEvent().size();
        if (eventsCount == 0) {
            return new IAEvaluation(IAEvaluationStatus.OK, null);
        }
        
        for (AssuranceAssetEvent assuranceAssetEvent : artefact.getLifecycleEvent()) {
            if(EventKind.MODIFICATION.equals(assuranceAssetEvent.getType())
                    && (assuranceAssetEvent.getTime() == null)
                    && isValidEventName(assuranceAssetEvent)
            ) {
                return new IAEvaluation(IAEvaluationStatus.NEEDS_MODIFICATION, assuranceAssetEvent);
            }
            
            if(EventKind.EVALUATION.equals(assuranceAssetEvent.getType())
                    && (assuranceAssetEvent.getTime() == null)
                    && isValidEventName(assuranceAssetEvent)
            ) {
                    return new IAEvaluation(IAEvaluationStatus.NEEDS_VALIDATION, assuranceAssetEvent);
            }
            
            if(EventKind.REVOCATION.equals(assuranceAssetEvent.getType())
                    && isValidEventName(assuranceAssetEvent)
            ) {
                return new IAEvaluation(IAEvaluationStatus.REVOKED, assuranceAssetEvent);
            }
        }
        
        return new IAEvaluation(IAEvaluationStatus.OK, null);
    }


    private static boolean isValidEventName(
            AssuranceAssetEvent assuranceAssetEvent)
    {
        //this ugly name check is due to the fact that CCL team, despite our
        //requirement to be able to determine IA driven event, didn't provide any way before model freeze 
        
        return IChangeDrivenEventsCreator.EVENT_CREATED_BY_IMPACT_ANALYSIS.equals(assuranceAssetEvent.getName());
    }
    
    
    private IAEvaluation(IAEvaluationStatus evaluationStatus,
            AssuranceAssetEvent assuranceAssetEvent)
    {
        this.evaluationStatus = evaluationStatus;
        this.assuranceAssetEvent = assuranceAssetEvent;
    }

    
    public IAEvaluationStatus getEvaluationStatus()
    {
        return evaluationStatus;
    }

    
    public AssuranceAssetEvent getAssuranceAssetEvent()
    {
        return assuranceAssetEvent;
    }
    
    
    public boolean needsAttention()
    {
        return !IAEvaluationStatus.OK.equals(evaluationStatus) && !IAEvaluationStatus.NOT_AVAILABLE.equals(evaluationStatus);
    }
    
    
    public String generateIAStatusDescription(String actionInformation)
    {
        String description = "This artefact needs attention due to an Impact Analysis invalidation.<br/> Description of the invalidating event:<br/><b>" +
                getAssuranceAssetEvent().getDescription() + "</b>";
        if (actionInformation != null) { 
            description += actionInformation;
        }
        
        return description;
    }
}
