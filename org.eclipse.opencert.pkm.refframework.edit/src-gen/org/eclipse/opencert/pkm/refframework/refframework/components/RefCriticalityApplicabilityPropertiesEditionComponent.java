/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;

import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkFactory;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

import org.eclipse.opencert.pkm.refframework.refframework.parts.RefCriticalityApplicabilityPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class RefCriticalityApplicabilityPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for applicLevel EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings applicLevelSettings;
	
	/**
	 * Settings for criticLevel EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings criticLevelSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public RefCriticalityApplicabilityPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refCriticalityApplicability, String editing_mode) {
		super(editingContext, refCriticalityApplicability, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = RefframeworkViewsRepository.class;
		partKey = RefframeworkViewsRepository.RefCriticalityApplicability.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final RefCriticalityApplicability refCriticalityApplicability = (RefCriticalityApplicability)elt;
			final RefCriticalityApplicabilityPropertiesEditionPart basePart = (RefCriticalityApplicabilityPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel)) {
				// init part
				applicLevelSettings = new EObjectFlatComboSettings(refCriticalityApplicability, RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_ApplicLevel());
				basePart.initApplicLevel(applicLevelSettings);
				// set the button mode
				basePart.setApplicLevelButtonMode(ButtonsModeEnum.BROWSE);
			}
			if (isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel)) {
				// init part
				criticLevelSettings = new EObjectFlatComboSettings(refCriticalityApplicability, RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_CriticLevel());
				basePart.initCriticLevel(criticLevelSettings);
				// set the button mode
				basePart.setCriticLevelButtonMode(ButtonsModeEnum.BROWSE);
			}
			if (isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo)) {
				basePart.initApplicLevelCombo(EEFUtils.choiceOfValues(refCriticalityApplicability, RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_ApplicLevel()), refCriticalityApplicability.getApplicLevel());
			}
			if (isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo)) {
				basePart.initCriticLevelCombo(EEFUtils.choiceOfValues(refCriticalityApplicability, RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_CriticLevel()), refCriticalityApplicability.getCriticLevel());
			}
			if (isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment))
				basePart.setComment(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refCriticalityApplicability.getComment()));
			
			// init filters
			if (isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel)) {
				basePart.addFilterToApplicLevel(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof RefApplicabilityLevel);
					}
					
				});
				// Start of user code for additional businessfilters for applicLevel
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel)) {
				basePart.addFilterToCriticLevel(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof RefCriticalityLevel);
					}
					
				});
				// Start of user code for additional businessfilters for criticLevel
				// End of user code
			}
			// Start of user code for additional businessfilters for applicLevelCombo
			// End of user code
			
			// Start of user code for additional businessfilters for criticLevelCombo
			// End of user code
			
			
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}








	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel) {
			return RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_ApplicLevel();
		}
		if (editorKey == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel) {
			return RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_CriticLevel();
		}
		if (editorKey == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo) {
			return RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_ApplicLevel();
		}
		if (editorKey == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo) {
			return RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_CriticLevel();
		}
		if (editorKey == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment) {
			return RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_Comment();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		RefCriticalityApplicability refCriticalityApplicability = (RefCriticalityApplicability)semanticObject;
		if (RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				applicLevelSettings.setToReference((RefApplicabilityLevel)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				RefApplicabilityLevel eObject = RefframeworkFactory.eINSTANCE.createRefApplicabilityLevel();
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, eObject, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(eObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy != null) {
						policy.execute();
					}
				}
				applicLevelSettings.setToReference(eObject);
			}
		}
		if (RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				criticLevelSettings.setToReference((RefCriticalityLevel)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				RefCriticalityLevel eObject = RefframeworkFactory.eINSTANCE.createRefCriticalityLevel();
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, eObject, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(eObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy != null) {
						policy.execute();
					}
				}
				criticLevelSettings.setToReference(eObject);
			}
		}
		if (RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo == event.getAffectedEditor()) {
			refCriticalityApplicability.setApplicLevel(!"".equals(event.getNewValue()) ? (RefApplicabilityLevel) event.getNewValue() : null);
		}
		if (RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo == event.getAffectedEditor()) {
			refCriticalityApplicability.setCriticLevel(!"".equals(event.getNewValue()) ? (RefCriticalityLevel) event.getNewValue() : null);
		}
		if (RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment == event.getAffectedEditor()) {
			refCriticalityApplicability.setComment((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			RefCriticalityApplicabilityPropertiesEditionPart basePart = (RefCriticalityApplicabilityPropertiesEditionPart)editingPart;
			if (RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_ApplicLevel().equals(msg.getFeature()) && basePart != null && isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel))
				basePart.setApplicLevel((EObject)msg.getNewValue());
			if (RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_CriticLevel().equals(msg.getFeature()) && basePart != null && isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel))
				basePart.setCriticLevel((EObject)msg.getNewValue());
			if (RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_ApplicLevel().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo))
				basePart.setApplicLevelCombo((Object)msg.getNewValue());
			if (RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_CriticLevel().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo))
				basePart.setCriticLevelCombo((Object)msg.getNewValue());
			if (RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_Comment().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment)) {
				if (msg.getNewValue() != null) {
					basePart.setComment(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setComment("");
				}
			}
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_ApplicLevel(),
			RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_CriticLevel(),
			RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_ApplicLevel(),
			RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_CriticLevel(),
			RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_Comment()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#isRequired(java.lang.Object, int)
	 * 
	 */
	public boolean isRequired(Object key, int kind) {
		return key == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevel || key == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevel || key == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.applicLevelCombo || key == RefframeworkViewsRepository.RefCriticalityApplicability.Properties.criticLevelCombo;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (RefframeworkViewsRepository.RefCriticalityApplicability.Properties.comment == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_Comment().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefCriticalityApplicability_Comment().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
