/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.evm.evidspec.evidence.presentation;

import java.util.ArrayList;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assurproj.utils.widget.CheckboxTreeViewerExt;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;


public class SearchCriteria extends Dialog {

	protected IEditorPart editor;
	protected URI resourceURI;	
		
	protected Table tableHistory;
	
	protected Text txtJustify;
	
	protected ListViewer targetsList;
	
	protected org.eclipse.swt.widgets.Combo refList;
	protected ArrayList<String> refListDirMap;
	

	

	protected Button btnAutoselect;
	
	protected Button btnSearch;
	
	protected String qref;
	protected CDOResource baselineResource;
	protected Color green;
	protected Color orange;
	protected Color red;
	
	private Text searchText;
	
	private static final String CRITICALITY_SELECTION = "Select the Criticality level:";
	private static final String APPLICABILITY_SELECTION = "Select the Applicability level:";
	protected CheckboxTreeViewerExt criticalityViewer;
	protected CheckboxTreeViewerExt aplicabilityViewer;
	//protected BaseCriticalityLevel[] critState;
	
	
	
	private BaseCriticalityLevel selectedCrit;
    private BaseApplicabilityLevel selectAplic;
    private boolean autoselect;
    private String searchingText;
    
    private ArrayList critLevels = new ArrayList<BaseCriticalityLevel>();
	boolean writePermission=false;
	
	
	
	public SearchCriteria(Shell parentShell, CDOResource baselineResource) {
		super(parentShell);

				
		//System.out.println("MODEL _used: " + .g resourceURI.toString());
						
		this.baselineResource=baselineResource;
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
	}
	
	/*public ComplianceTable(Shell parentShell,URI resourceURI) {
		super(parentShell);

		
		this.resourceURI = resourceURI;
		
		//System.out.println("MODELO _usar: " + resourceURI.toString());
		
		
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		viewCDO = CDOConnectionUtil.instance.openView(sessionCDO);
		transaction = sessionCDO.openTransaction();

		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
	}*/
	
	public boolean isAutoselect() {
		return autoselect;
	}
	
	public String getText() {
		return searchingText;
	}
	
	public ArrayList getCritLevel(){
		return critLevels;
	}
	
	@Override
	protected void constrainShellSize() {
		// TODO Auto-generated method stub
		super.constrainShellSize();
		//getShell().setMaximized(true);
	}
	
	@Override
	protected Control createDialogArea(final Composite parent) {						

		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		
		final Composite contents = (Composite) super.createDialogArea(parent);
		
		GridLayout contentsGridLayout = (GridLayout) contents.getLayout();
		contentsGridLayout.numColumns = 1;
		
		GridData contentsGridData = (GridData) contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		//contentsGridData.heightHint =500;
		createSearchArea(contents);			
		
		return contents;
	}

	protected void createSearchArea(final Composite contents) {

		/*Composite controlSearch = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			data.verticalAlignment= SWT.FILL;
			data.horizontalSpan=2;
			controlSearch.setLayoutData(data);
			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 3; 
			controlSearch.setLayout(layout);
		}*/

		/*Composite controlMapping = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			data.verticalAlignment= SWT.FILL;
			controlMapping.setLayoutData(data);
			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1; 
			controlMapping.setLayout(layout);
		}*/
			
		Label groupFilter = new Label(contents, SWT.NULL);
		
		groupFilter.setLayoutData(new GridData(SWT.FILL,SWT.NONE,false,true,1,1));
		groupFilter.setText("Search Text");
		GridLayout filterLayout= new GridLayout();		
		
		searchText = new Text(contents, SWT.SEARCH);
		searchText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));						
		
		Label criticalityLabel = new Label(contents, SWT.NONE);
		
		
		
		
		
		criticalityLabel.setText(CRITICALITY_SELECTION); 
	    criticalityViewer = new CheckboxTreeViewerExt(contents, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		criticalityViewer.getTree().setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,1,1));
	    criticalityViewer.expandAll();

		ComposedAdapterFactory critAdapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		criticalityViewer.setContentProvider(new AdapterFactoryContentProvider(critAdapterFactory));
		criticalityViewer.setLabelProvider(new AdapterFactoryLabelProvider(critAdapterFactory));
		
		CreateCriticalModelFromFile(criticalityViewer);
		ICheckStateListener checkCritStateListener = new ICheckStateListener(){
			public void checkStateChanged (CheckStateChangedEvent event){
				Object changedElement = event.getElement();
				boolean status = criticalityViewer.getChecked(changedElement);
				boolean isExpanded = criticalityViewer.getExpandedState(changedElement);
				criticalityViewer.expandToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				criticalityViewer.setSubtreeChecked(changedElement, status);
				if (!isExpanded){
					criticalityViewer.collapseToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				}
				if (status){
					criticalityViewer.setParentsChecked(changedElement,true);
				}
				//critState=(BaseCriticalityLevel[]) criticalityViewer.getCheckedElements();
				
			}

		};
		
		
		criticalityViewer.addCheckStateListener(checkCritStateListener);
		
		Label aplicabilityLabel = new Label(contents, SWT.NONE);
		aplicabilityLabel.setText(APPLICABILITY_SELECTION); 
		aplicabilityViewer = new CheckboxTreeViewerExt(contents, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		aplicabilityViewer.getTree().setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false,1,1));
		aplicabilityViewer.expandAll();

		ComposedAdapterFactory appliAdapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		aplicabilityViewer.setContentProvider(new AdapterFactoryContentProvider(appliAdapterFactory));
		aplicabilityViewer.setLabelProvider(new AdapterFactoryLabelProvider(appliAdapterFactory));
		CreateApplicabilityList (aplicabilityViewer);
		
		ICheckStateListener checkAppStateListener = new ICheckStateListener(){
			public void checkStateChanged (CheckStateChangedEvent event){
				Object changedElement = event.getElement();
				boolean status = aplicabilityViewer.getChecked(changedElement);
				boolean isExpanded = aplicabilityViewer.getExpandedState(changedElement);

				aplicabilityViewer.expandToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				aplicabilityViewer.setSubtreeChecked(changedElement, status);
				if (!isExpanded){
					aplicabilityViewer.collapseToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				}
				if (status){
					aplicabilityViewer.setParentsChecked(changedElement,true);
				}
				
			}

		};
		
		aplicabilityViewer.addCheckStateListener(checkAppStateListener);
		
		
		
		//Label autoSelect = new Label(contents, SWT.NULL);
		
		//groupFilter.setLayoutData(new GridData(SWT.FILL,SWT.NONE,false,true,1,1));
		//autoSelect.setText("Modify selection according search results");
		//filterLayout= new GridLayout();		
		
		btnAutoselect = new Button(contents, SWT.CHECK);
		btnAutoselect.setText("Modify selection according search results");
		searchText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		
		
		btnSearch = new Button(contents, SWT.PUSH);
		btnSearch.setText("  SEARCH  ");
		btnSearch.setVisible(false);
		btnSearch.addSelectionListener(new SelectionAdapter() {
		
			public void widgetSelected(SelectionEvent e) {	
				
			}
		});
		
	}		
		
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Search criteria for reusable assets");
	}


	@Override
	protected void okPressed() {	
		this.autoselect=btnAutoselect.getSelection();	
		this.searchingText=searchText.getText();
		
		Object[] selectedCrit =  criticalityViewer.getCheckedElements();
		
		
		for(Object oneCrit:selectedCrit){
			if(oneCrit instanceof BaseCriticalityLevel){
				critLevels.add((BaseCriticalityLevel)oneCrit);
			}			
		}
		
		super.okPressed();
	}

	@Override
	public boolean close() {		
		return super.close();
	}

	@Override
	protected Button createButton(Composite parent, int id,
	        String label, boolean defaultButton) {
	    if (id == IDialogConstants.CANCEL_ID) return null;
	    return super.createButton(parent, id, label, defaultButton);
	}
	
	public Boolean getResult() {
		return true;
	}

	
	public void CreateCriticalModelFromFile(CheckboxTreeViewerExt tviewer){					   
			try {				
				tviewer.setInput(null);
				tviewer.setInput(baselineResource);
				
			    final ViewerFilter modelFilter = new ViewerFilter() {
			        public boolean select(
			          Viewer viewer,
			          Object parentElement,
			          Object element) {
			        	if (element instanceof BaseFramework)
			        		return true;
			        	if (element instanceof BaseCriticalityLevel)
			        		return true;
		        		else
		        			return false;
			        }
			    };
			    tviewer.addFilter(modelFilter);
			    tviewer.expandAll();			    
			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
				tviewer.setInput(null);
			}		
	}
	private void CreateApplicabilityList( CheckboxTreeViewerExt viewer) {
		
			   Resource resource = null;
			   try {
					viewer.setInput(null);
					viewer.setInput(baselineResource);
					
				    final ViewerFilter modelFilter = new ViewerFilter() {
				        public boolean select(
				          Viewer viewer,
				          Object parentElement,
				          Object element) {
				        	if (element instanceof BaseFramework)
				        		return true;
				        	if (element instanceof BaseApplicabilityLevel)
				        		return true;
			        		else
			        			return false;
				        }
				    };
				    viewer.addFilter(modelFilter);
				    viewer.expandAll();				   
				   // viewCDO.getSession().close();
			   }catch (Exception ex) {
					MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
					viewer.setInput(null);
				}	
	}
	
		
}
