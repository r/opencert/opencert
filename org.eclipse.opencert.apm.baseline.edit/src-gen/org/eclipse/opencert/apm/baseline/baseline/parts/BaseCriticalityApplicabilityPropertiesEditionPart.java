/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;

import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface BaseCriticalityApplicabilityPropertiesEditionPart {

	/**
	 * @return the applicLevel
	 * 
	 */
	public EObject getApplicLevel();

	/**
	 * Init the applicLevel
	 * @param settings the combo setting
	 */
	public void initApplicLevel(EObjectFlatComboSettings settings);

	/**
	 * Defines a new applicLevel
	 * @param newValue the new applicLevel to set
	 * 
	 */
	public void setApplicLevel(EObject newValue);

	/**
	 * Defines the button mode
	 * @param newValue the new mode to set
	 * 
	 */
	public void setApplicLevelButtonMode(ButtonsModeEnum newValue);

	/**
	 * Adds the given filter to the applicLevel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToApplicLevel(ViewerFilter filter);

	/**
	 * Adds the given filter to the applicLevel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToApplicLevel(ViewerFilter filter);


	/**
	 * @return the criticLevel
	 * 
	 */
	public EObject getCriticLevel();

	/**
	 * Init the criticLevel
	 * @param settings the combo setting
	 */
	public void initCriticLevel(EObjectFlatComboSettings settings);

	/**
	 * Defines a new criticLevel
	 * @param newValue the new criticLevel to set
	 * 
	 */
	public void setCriticLevel(EObject newValue);

	/**
	 * Defines the button mode
	 * @param newValue the new mode to set
	 * 
	 */
	public void setCriticLevelButtonMode(ButtonsModeEnum newValue);

	/**
	 * Adds the given filter to the criticLevel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToCriticLevel(ViewerFilter filter);

	/**
	 * Adds the given filter to the criticLevel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToCriticLevel(ViewerFilter filter);


	/**
	 * @return the applicLevelCombo
	 * 
	 */
	public Object getApplicLevelCombo();

	/**
	 * Init the applicLevelCombo
	 * @param input choice of values
	 * @param currentValue the current value
	 */
	public void initApplicLevelCombo(Object input, Object currentValue);

	/**
	 * Defines a new applicLevelCombo
	 * @param newValue the new applicLevelCombo to set
	 * 
	 */
	public void setApplicLevelCombo(Object newValue);

	/**
	 * Adds the given filter to the applicLevelCombo edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToApplicLevelCombo(ViewerFilter filter);


	/**
	 * @return the criticLevelCombo
	 * 
	 */
	public Object getCriticLevelCombo();

	/**
	 * Init the criticLevelCombo
	 * @param input choice of values
	 * @param currentValue the current value
	 */
	public void initCriticLevelCombo(Object input, Object currentValue);

	/**
	 * Defines a new criticLevelCombo
	 * @param newValue the new criticLevelCombo to set
	 * 
	 */
	public void setCriticLevelCombo(Object newValue);

	/**
	 * Adds the given filter to the criticLevelCombo edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToCriticLevelCombo(ViewerFilter filter);


	/**
	 * @return the comment
	 * 
	 */
	public String getComment();

	/**
	 * Defines a new comment
	 * @param newValue the new comment to set
	 * 
	 */
	public void setComment(String newValue);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
