/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;

import org.eclipse.opencert.pam.procspec.process.providers.ProcessMessages;

// End of user code

/**
 * 
 * 
 */
public class ActivityArtefactsPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ActivityArtefactsPropertiesEditionPart {

	protected ReferencesTable requiredArtefact;
	protected List<ViewerFilter> requiredArtefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> requiredArtefactFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable producedArtefact;
	protected List<ViewerFilter> producedArtefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> producedArtefactFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public ActivityArtefactsPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ActivityArtefactsPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence activityArtefactsStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = activityArtefactsStep.addStep(ProcessViewsRepository.ActivityArtefacts.Properties.class);
		propertiesStep.addStep(ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact);
		propertiesStep.addStep(ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact);
		
		
		composer = new PartComposer(activityArtefactsStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ProcessViewsRepository.ActivityArtefacts.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact) {
					return createRequiredArtefactReferencesTable(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact) {
					return createProducedArtefactReferencesTable(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(ProcessMessages.ActivityArtefactsPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * 
	 */
	protected Composite createRequiredArtefactReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.requiredArtefact = new ReferencesTable(getDescription(ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact, ProcessMessages.ActivityArtefactsPropertiesEditionPart_RequiredArtefactLabel), new ReferencesTableListener	() {
			public void handleAdd() { addRequiredArtefact(); }
			public void handleEdit(EObject element) { editRequiredArtefact(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveRequiredArtefact(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromRequiredArtefact(element); }
			public void navigateTo(EObject element) { }
		});
		this.requiredArtefact.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact, ProcessViewsRepository.FORM_KIND));
		this.requiredArtefact.createControls(parent, widgetFactory);
		this.requiredArtefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityArtefactsPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData requiredArtefactData = new GridData(GridData.FILL_HORIZONTAL);
		requiredArtefactData.horizontalSpan = 3;
		this.requiredArtefact.setLayoutData(requiredArtefactData);
		this.requiredArtefact.disableMove();
		requiredArtefact.setID(ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact);
		requiredArtefact.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createRequiredArtefactReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addRequiredArtefact() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(requiredArtefact.getInput(), requiredArtefactFilters, requiredArtefactBusinessFilters,
		"requiredArtefact", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityArtefactsPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				requiredArtefact.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveRequiredArtefact(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityArtefactsPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		requiredArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromRequiredArtefact(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityArtefactsPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		requiredArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void editRequiredArtefact(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				requiredArtefact.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createProducedArtefactReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.producedArtefact = new ReferencesTable(getDescription(ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact, ProcessMessages.ActivityArtefactsPropertiesEditionPart_ProducedArtefactLabel), new ReferencesTableListener	() {
			public void handleAdd() { addProducedArtefact(); }
			public void handleEdit(EObject element) { editProducedArtefact(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveProducedArtefact(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromProducedArtefact(element); }
			public void navigateTo(EObject element) { }
		});
		this.producedArtefact.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact, ProcessViewsRepository.FORM_KIND));
		this.producedArtefact.createControls(parent, widgetFactory);
		this.producedArtefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityArtefactsPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData producedArtefactData = new GridData(GridData.FILL_HORIZONTAL);
		producedArtefactData.horizontalSpan = 3;
		this.producedArtefact.setLayoutData(producedArtefactData);
		this.producedArtefact.disableMove();
		producedArtefact.setID(ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact);
		producedArtefact.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createProducedArtefactReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addProducedArtefact() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(producedArtefact.getInput(), producedArtefactFilters, producedArtefactBusinessFilters,
		"producedArtefact", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityArtefactsPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				producedArtefact.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveProducedArtefact(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityArtefactsPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		producedArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromProducedArtefact(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityArtefactsPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		producedArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void editProducedArtefact(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				producedArtefact.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart#initRequiredArtefact(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initRequiredArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		requiredArtefact.setContentProvider(contentProvider);
		requiredArtefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact);
		if (eefElementEditorReadOnlyState && requiredArtefact.getTable().isEnabled()) {
			requiredArtefact.setEnabled(false);
			requiredArtefact.setToolTipText(ProcessMessages.ActivityArtefacts_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !requiredArtefact.getTable().isEnabled()) {
			requiredArtefact.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart#updateRequiredArtefact()
	 * 
	 */
	public void updateRequiredArtefact() {
	requiredArtefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart#addFilterRequiredArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToRequiredArtefact(ViewerFilter filter) {
		requiredArtefactFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart#addBusinessFilterRequiredArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToRequiredArtefact(ViewerFilter filter) {
		requiredArtefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart#isContainedInRequiredArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInRequiredArtefactTable(EObject element) {
		return ((ReferencesTableSettings)requiredArtefact.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart#initProducedArtefact(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initProducedArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		producedArtefact.setContentProvider(contentProvider);
		producedArtefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact);
		if (eefElementEditorReadOnlyState && producedArtefact.getTable().isEnabled()) {
			producedArtefact.setEnabled(false);
			producedArtefact.setToolTipText(ProcessMessages.ActivityArtefacts_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !producedArtefact.getTable().isEnabled()) {
			producedArtefact.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart#updateProducedArtefact()
	 * 
	 */
	public void updateProducedArtefact() {
	producedArtefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart#addFilterProducedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToProducedArtefact(ViewerFilter filter) {
		producedArtefactFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart#addBusinessFilterProducedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToProducedArtefact(ViewerFilter filter) {
		producedArtefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart#isContainedInProducedArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInProducedArtefactTable(EObject element) {
		return ((ReferencesTableSettings)producedArtefact.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ProcessMessages.ActivityArtefacts_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
