/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

import org.eclipse.opencert.webapp.reports.containers.BaseAssetComplianceStatus;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;

import com.vaadin.data.Container.Hierarchical;

public class ComplianceStatusCalculator
{
    public ComplianceStatusType calculateComplianceStatusForBaseElement(BaseAssurableElement baseElement)
    {
        ComplianceStatusType complianceStatusType = ComplianceStatusType.EMPTY;

        for (BaseComplianceMap baseComplianceMap : baseElement.getComplianceMap()) {
            if (baseComplianceMap.getType().equals(MapKind.FULL)) {
                complianceStatusType = getBiggerPriorityStatusType(ComplianceStatusType.OK, complianceStatusType);
            } else if (baseComplianceMap.getType().equals(MapKind.PARTIAL)) {
                complianceStatusType = getBiggerPriorityStatusType(ComplianceStatusType.PARTLY, complianceStatusType);
            } else if (baseComplianceMap.getType().equals(MapKind.NO_MAP)) {
                complianceStatusType = getBiggerPriorityStatusType(ComplianceStatusType.NO, complianceStatusType);
            }
        }

        return complianceStatusType;
    }

    public ComplianceStatusType calculateSummaryComplianceStatusForProject(Hierarchical hierarchical)
    {
        ComplianceStatusType complianceStatusType = ComplianceStatusType.OK;

        for (Object baseAssetComplianceStatus : hierarchical.getItemIds()) {
            if (((BaseAssetComplianceStatus) baseAssetComplianceStatus).getStatus().getValue().equals(ComplianceStatusType.EMPTY.getValue())) {
                return ComplianceStatusType.EMPTY;
            } else if (((BaseAssetComplianceStatus) baseAssetComplianceStatus).getStatus().getValue().equals(ComplianceStatusType.OK.getValue())) {
                complianceStatusType = getBiggerPriorityStatusType(ComplianceStatusType.OK, complianceStatusType);
            } else if (((BaseAssetComplianceStatus) baseAssetComplianceStatus).getStatus().getValue().equals(ComplianceStatusType.PARTLY.getValue())) {
                complianceStatusType = getBiggerPriorityStatusType(ComplianceStatusType.PARTLY, complianceStatusType);
            } else if (((BaseAssetComplianceStatus) baseAssetComplianceStatus).getStatus().getValue().equals(ComplianceStatusType.NO.getValue())) {
                complianceStatusType = getBiggerPriorityStatusType(ComplianceStatusType.NO, complianceStatusType);
            }
        }

        return complianceStatusType;
    }

    private ComplianceStatusType getBiggerPriorityStatusType(ComplianceStatusType newStatus, ComplianceStatusType complianceStatusType)
    {
        if (newStatus.getPriority() > complianceStatusType.getPriority()) {
            complianceStatusType = newStatus;
        }

        return complianceStatusType;
    }

}
