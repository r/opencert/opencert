/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asserted Challenge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.AssertedChallenge#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.AssertedChallenge#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssertedChallenge()
 * @model annotation="gmf.link source='source' target='target' color='255,0,0' style='solid' width='1' source.constraint='self.oclIsTypeOf(Claim)' target.constraint='self.oclIsTypeOf(Claim) or self.oclIsTypeOf(AssertedInference) or self.oclIsTypeOf(AssertedEvidence) or self.oclIsTypeOf(AssertedContext) or self.oclIsTypeOf(AssertedChallenge) or self.oclIsTypeOf(AssertedCounterEvidence)' target.decoration='filledclosedarrow' tool.small.path='SACEM_tooling_icons/Challenge.gif' tool.large.path='SACEM_tooling_icons/Challenge.gif' tool.small.bundle='org.eclipse.opencert.sam.arg' tool.large.bundle='org.eclipse.opencert.sam.arg'"
 * @generated
 */
public interface AssertedChallenge extends AssertedRelationship {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.Claim}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssertedChallenge_Source()
	 * @model
	 * @generated
	 */
	EList<Claim> getSource();

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.Assertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssertedChallenge_Target()
	 * @model
	 * @generated
	 */
	EList<Assertion> getTarget();

} // AssertedChallenge
