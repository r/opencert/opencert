/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.externaltools.api;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ExternalToolQueryResult
        implements Serializable, Comparable<ExternalToolQueryResult>
{
    private static final long serialVersionUID = 8966171531522277055L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long externalToolQueryResultId;

    private Date date;

    @ManyToOne
    @JoinColumn(name = "externalToolQueryId")
    private ExternalToolQuery externalToolQuery;

    private int result;
    
    public ExternalToolQueryResult() 
    {
        
    }

    public ExternalToolQueryResult(int result, ExternalToolQuery externalToolQuery)
    {
        this.result = result;
        this.externalToolQuery = externalToolQuery;
        this.date = new Date();
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public ExternalToolQuery getExternalToolQuery()
    {
        return externalToolQuery;
    }

    public void setExternalToolQuery(ExternalToolQuery externalToolQuery)
    {
        this.externalToolQuery = externalToolQuery;
    }

    public int getResult()
    {
        return result;
    }

    public void setResult(int result)
    {
        this.result = result;
    }

    public long getId()
    {
        return getExternalToolQueryResultId();
    }

    public void setId(long externalToolQueryResultId)
    {
        this.setExternalToolQueryResultId(externalToolQueryResultId);
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getDate() == null) ? 0 : getDate().hashCode());
        result = prime * result + this.getResult();
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ExternalToolQueryResult other = (ExternalToolQueryResult) obj;
        if (getDate() == null) {
            if (other.getDate() != null)
                return false;
        } else if (!getDate().equals(other.getDate()))
            return false;
        if (getExternalToolQuery() == null) {
            if (other.getExternalToolQuery() != null)
                return false;
        } else if (!getExternalToolQuery().equals(other.getExternalToolQuery()))
            return false;
        if (getResult() != other.getResult())
            return false;
        return true;
    }

    private long getExternalToolQueryResultId()
    {
        return externalToolQueryResultId;
    }

    private void setExternalToolQueryResultId(long externalToolQueryResultId)
    {
        this.externalToolQueryResultId = externalToolQueryResultId;
    }

    @Override
    public int compareTo(ExternalToolQueryResult externalToolQueryResult)
    {
        return date.compareTo(externalToolQueryResult.date);
    }

}
