/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers.visitors;

import org.eclipse.opencert.webapp.reports.containers.ComplianceAsset;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetEvaluation;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetResource;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetails;

public interface IComplianceDetailsVisitor<PRODUCED_NODE>
{
	public PRODUCED_NODE visitComplianceDetails(ComplianceDetails rootCD);

	public PRODUCED_NODE visitComplianceAsset(ComplianceAsset complianceAsset, ComplianceDetails rootCD, PRODUCED_NODE parentNode);

	public PRODUCED_NODE visitComplianceResource(ComplianceAssetResource complianceResource, ComplianceDetails rootCD, PRODUCED_NODE parentNode);
	
	public PRODUCED_NODE visitComplianceEvaluation(ComplianceAssetEvaluation complianceEvaluation, ComplianceDetails rootCD, PRODUCED_NODE parentNode);

}
