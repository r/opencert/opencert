/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;
import org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart;

import org.eclipse.opencert.pam.procspec.process.providers.ProcessMessages;

// End of user code

/**
 * 
 * 
 */
public class ToolPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ToolPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected ReferencesTable ownedArtefact;
	protected List<ViewerFilter> ownedArtefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedArtefactFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable triggeredAssetEvent;
	protected List<ViewerFilter> triggeredAssetEventBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> triggeredAssetEventFilters = new ArrayList<ViewerFilter>();
	protected Text version;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ToolPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence toolStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = toolStep.addStep(ProcessViewsRepository.Tool.Properties.class);
		propertiesStep.addStep(ProcessViewsRepository.Tool.Properties.id);
		propertiesStep.addStep(ProcessViewsRepository.Tool.Properties.name);
		propertiesStep.addStep(ProcessViewsRepository.Tool.Properties.description);
		propertiesStep.addStep(ProcessViewsRepository.Tool.Properties.ownedArtefact);
		propertiesStep.addStep(ProcessViewsRepository.Tool.Properties.triggeredAssetEvent);
		propertiesStep.addStep(ProcessViewsRepository.Tool.Properties.version);
		
		
		composer = new PartComposer(toolStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ProcessViewsRepository.Tool.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == ProcessViewsRepository.Tool.Properties.id) {
					return createIdText(parent);
				}
				if (key == ProcessViewsRepository.Tool.Properties.name) {
					return createNameText(parent);
				}
				if (key == ProcessViewsRepository.Tool.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == ProcessViewsRepository.Tool.Properties.ownedArtefact) {
					return createOwnedArtefactAdvancedReferencesTable(parent);
				}
				if (key == ProcessViewsRepository.Tool.Properties.triggeredAssetEvent) {
					return createTriggeredAssetEventAdvancedReferencesTable(parent);
				}
				if (key == ProcessViewsRepository.Tool.Properties.version) {
					return createVersionText(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(ProcessMessages.ToolPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, ProcessViewsRepository.Tool.Properties.id, ProcessMessages.ToolPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, ProcessViewsRepository.Tool.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Tool.Properties.id, ProcessViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, ProcessViewsRepository.Tool.Properties.name, ProcessMessages.ToolPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, ProcessViewsRepository.Tool.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Tool.Properties.name, ProcessViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, ProcessViewsRepository.Tool.Properties.description, ProcessMessages.ToolPropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, ProcessViewsRepository.Tool.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Tool.Properties.description, ProcessViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createOwnedArtefactAdvancedReferencesTable(Composite parent) {
		String label = getDescription(ProcessViewsRepository.Tool.Properties.ownedArtefact, ProcessMessages.ToolPropertiesEditionPart_OwnedArtefactLabel);		 
		this.ownedArtefact = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addOwnedArtefact(); }
			public void handleEdit(EObject element) { editOwnedArtefact(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveOwnedArtefact(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromOwnedArtefact(element); }
			public void navigateTo(EObject element) { }
		});
		this.ownedArtefact.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Tool.Properties.ownedArtefact, ProcessViewsRepository.SWT_KIND));
		this.ownedArtefact.createControls(parent);
		this.ownedArtefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.ownedArtefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedArtefactData = new GridData(GridData.FILL_HORIZONTAL);
		ownedArtefactData.horizontalSpan = 3;
		this.ownedArtefact.setLayoutData(ownedArtefactData);
		this.ownedArtefact.disableMove();
		ownedArtefact.setID(ProcessViewsRepository.Tool.Properties.ownedArtefact);
		ownedArtefact.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addOwnedArtefact() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(ownedArtefact.getInput(), ownedArtefactFilters, ownedArtefactBusinessFilters,
		"ownedArtefact", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.ownedArtefact,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				ownedArtefact.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveOwnedArtefact(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		ownedArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromOwnedArtefact(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		ownedArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void editOwnedArtefact(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				ownedArtefact.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createTriggeredAssetEventAdvancedReferencesTable(Composite parent) {
		String label = getDescription(ProcessViewsRepository.Tool.Properties.triggeredAssetEvent, ProcessMessages.ToolPropertiesEditionPart_TriggeredAssetEventLabel);		 
		this.triggeredAssetEvent = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addTriggeredAssetEvent(); }
			public void handleEdit(EObject element) { editTriggeredAssetEvent(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveTriggeredAssetEvent(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromTriggeredAssetEvent(element); }
			public void navigateTo(EObject element) { }
		});
		this.triggeredAssetEvent.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Tool.Properties.triggeredAssetEvent, ProcessViewsRepository.SWT_KIND));
		this.triggeredAssetEvent.createControls(parent);
		this.triggeredAssetEvent.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.triggeredAssetEvent, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData triggeredAssetEventData = new GridData(GridData.FILL_HORIZONTAL);
		triggeredAssetEventData.horizontalSpan = 3;
		this.triggeredAssetEvent.setLayoutData(triggeredAssetEventData);
		this.triggeredAssetEvent.disableMove();
		triggeredAssetEvent.setID(ProcessViewsRepository.Tool.Properties.triggeredAssetEvent);
		triggeredAssetEvent.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addTriggeredAssetEvent() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(triggeredAssetEvent.getInput(), triggeredAssetEventFilters, triggeredAssetEventBusinessFilters,
		"triggeredAssetEvent", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.triggeredAssetEvent,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				triggeredAssetEvent.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveTriggeredAssetEvent(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.triggeredAssetEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		triggeredAssetEvent.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromTriggeredAssetEvent(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.triggeredAssetEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		triggeredAssetEvent.refresh();
	}

	/**
	 * 
	 */
	protected void editTriggeredAssetEvent(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				triggeredAssetEvent.refresh();
			}
		}
	}

	
	protected Composite createVersionText(Composite parent) {
		createDescription(parent, ProcessViewsRepository.Tool.Properties.version, ProcessMessages.ToolPropertiesEditionPart_VersionLabel);
		version = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData versionData = new GridData(GridData.FILL_HORIZONTAL);
		version.setLayoutData(versionData);
		version.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.version, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, version.getText()));
			}

		});
		version.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ToolPropertiesEditionPartImpl.this, ProcessViewsRepository.Tool.Properties.version, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, version.getText()));
				}
			}

		});
		EditingUtils.setID(version, ProcessViewsRepository.Tool.Properties.version);
		EditingUtils.setEEFtype(version, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Tool.Properties.version, ProcessViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createVersionText

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Tool.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(ProcessMessages.Tool_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Tool.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(ProcessMessages.Tool_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Tool.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(ProcessMessages.Tool_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#initOwnedArtefact(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initOwnedArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedArtefact.setContentProvider(contentProvider);
		ownedArtefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Tool.Properties.ownedArtefact);
		if (eefElementEditorReadOnlyState && ownedArtefact.getTable().isEnabled()) {
			ownedArtefact.setEnabled(false);
			ownedArtefact.setToolTipText(ProcessMessages.Tool_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedArtefact.getTable().isEnabled()) {
			ownedArtefact.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#updateOwnedArtefact()
	 * 
	 */
	public void updateOwnedArtefact() {
	ownedArtefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#addFilterOwnedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedArtefact(ViewerFilter filter) {
		ownedArtefactFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#addBusinessFilterOwnedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedArtefact(ViewerFilter filter) {
		ownedArtefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#isContainedInOwnedArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedArtefactTable(EObject element) {
		return ((ReferencesTableSettings)ownedArtefact.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#initTriggeredAssetEvent(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initTriggeredAssetEvent(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		triggeredAssetEvent.setContentProvider(contentProvider);
		triggeredAssetEvent.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Tool.Properties.triggeredAssetEvent);
		if (eefElementEditorReadOnlyState && triggeredAssetEvent.getTable().isEnabled()) {
			triggeredAssetEvent.setEnabled(false);
			triggeredAssetEvent.setToolTipText(ProcessMessages.Tool_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !triggeredAssetEvent.getTable().isEnabled()) {
			triggeredAssetEvent.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#updateTriggeredAssetEvent()
	 * 
	 */
	public void updateTriggeredAssetEvent() {
	triggeredAssetEvent.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#addFilterTriggeredAssetEvent(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTriggeredAssetEvent(ViewerFilter filter) {
		triggeredAssetEventFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#addBusinessFilterTriggeredAssetEvent(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTriggeredAssetEvent(ViewerFilter filter) {
		triggeredAssetEventBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#isContainedInTriggeredAssetEventTable(EObject element)
	 * 
	 */
	public boolean isContainedInTriggeredAssetEventTable(EObject element) {
		return ((ReferencesTableSettings)triggeredAssetEvent.getInput()).contains(element);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#getVersion()
	 * 
	 */
	public String getVersion() {
		return version.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart#setVersion(String newValue)
	 * 
	 */
	public void setVersion(String newValue) {
		if (newValue != null) {
			version.setText(newValue);
		} else {
			version.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Tool.Properties.version);
		if (eefElementEditorReadOnlyState && version.isEnabled()) {
			version.setEnabled(false);
			version.setToolTipText(ProcessMessages.Tool_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !version.isEnabled()) {
			version.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ProcessMessages.Tool_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
