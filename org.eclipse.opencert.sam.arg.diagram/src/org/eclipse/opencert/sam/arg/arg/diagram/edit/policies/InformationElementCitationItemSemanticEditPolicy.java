/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedContextCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedContextReorientCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedCounterEvidenceCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedCounterEvidenceReorientCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedEvidenceCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedEvidenceReorientCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedInferenceCreateCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.commands.AssertedInferenceReorientCommand;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedContextEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedCounterEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedInferenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;

/**
 * @generated
 */
public class InformationElementCitationItemSemanticEditPolicy extends
		ArgBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public InformationElementCitationItemSemanticEditPolicy() {
		super(ArgElementTypes.InformationElementCitation_2005);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		View view = (View) getHost().getModel();
		CompositeTransactionalCommand cmd = new CompositeTransactionalCommand(
				getEditingDomain(), null);
		cmd.setTransactionNestingEnabled(false);
		for (Iterator<?> it = view.getTargetEdges().iterator(); it.hasNext();) {
			Edge incomingLink = (Edge) it.next();
			if (ArgVisualIDRegistry.getVisualID(incomingLink) == AssertedInferenceEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
			if (ArgVisualIDRegistry.getVisualID(incomingLink) == AssertedEvidenceEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
			if (ArgVisualIDRegistry.getVisualID(incomingLink) == AssertedContextEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
		}
		for (Iterator<?> it = view.getSourceEdges().iterator(); it.hasNext();) {
			Edge outgoingLink = (Edge) it.next();
			if (ArgVisualIDRegistry.getVisualID(outgoingLink) == AssertedInferenceEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (ArgVisualIDRegistry.getVisualID(outgoingLink) == AssertedEvidenceEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (ArgVisualIDRegistry.getVisualID(outgoingLink) == AssertedContextEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (ArgVisualIDRegistry.getVisualID(outgoingLink) == AssertedCounterEvidenceEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(
						outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
		}
		EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
		if (annotation == null) {
			// there are indirectly referenced children, need extra commands: false
			addDestroyShortcutsCommand(cmd, view);
			// delete host element
			cmd.add(new DestroyElementCommand(req));
		} else {
			cmd.add(new DeleteCommand(getEditingDomain(), view));
		}
		return getGEFWrapper(cmd.reduce());
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req)
				: getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super
				.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (ArgElementTypes.AssertedInference_4001 == req.getElementType()) {
			return getGEFWrapper(new AssertedInferenceCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedEvidence_4002 == req.getElementType()) {
			return getGEFWrapper(new AssertedEvidenceCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedContext_4003 == req.getElementType()) {
			return getGEFWrapper(new AssertedContextCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedCounterEvidence_4005 == req
				.getElementType()) {
			return getGEFWrapper(new AssertedCounterEvidenceCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (ArgElementTypes.AssertedInference_4001 == req.getElementType()) {
			return getGEFWrapper(new AssertedInferenceCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedEvidence_4002 == req.getElementType()) {
			return getGEFWrapper(new AssertedEvidenceCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedContext_4003 == req.getElementType()) {
			return getGEFWrapper(new AssertedContextCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		if (ArgElementTypes.AssertedCounterEvidence_4005 == req
				.getElementType()) {
			return null;
		}
		return null;
	}

	/**
	 * Returns command to reorient EClass based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientRelationshipCommand(
			ReorientRelationshipRequest req) {
		switch (getVisualID(req)) {
		case AssertedInferenceEditPart.VISUAL_ID:
			return getGEFWrapper(new AssertedInferenceReorientCommand(req));
		case AssertedEvidenceEditPart.VISUAL_ID:
			return getGEFWrapper(new AssertedEvidenceReorientCommand(req));
		case AssertedContextEditPart.VISUAL_ID:
			return getGEFWrapper(new AssertedContextReorientCommand(req));
		case AssertedCounterEvidenceEditPart.VISUAL_ID:
			return getGEFWrapper(new AssertedCounterEvidenceReorientCommand(req));
		}
		return super.getReorientRelationshipCommand(req);
	}

}
