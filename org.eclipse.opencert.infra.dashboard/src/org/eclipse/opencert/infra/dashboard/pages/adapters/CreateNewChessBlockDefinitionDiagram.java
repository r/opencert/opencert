/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages.adapters;

import org.eclipse.opencert.infra.dashboard.helpers.ChessPapyrusHelper;
import org.eclipse.papyrus.commands.ICreationCommand;
import org.eclipse.papyrus.sysml.diagram.blockdefinition.BlockDefinitionDiagramCreateCommand;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.PackageableElement;

/**
 * A {@link HyperlinkAdapter} to create a new CHESS block definition diagram.
 */
public class CreateNewChessBlockDefinitionDiagram extends AbstractCreateNewChessDiagramHyperLinkAdapter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ICreationCommand getDiagramCreationCommand() {
		return new BlockDefinitionDiagramCreateCommand();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected PackageableElement getTargetElement(final Model pRootModel) {
		return ChessPapyrusHelper.INSTANCE.getPackageFromModelByName(pRootModel,
				ChessPapyrusHelper.MODEL_SYSTEM_VIEW_NAME);
	}
}
