/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationArgumentationReferenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedChallengeEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedContextCardinalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedContextEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedCounterEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedEvidenceCardinalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedInferenceCardinalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedInferenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.CaseEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.Choice2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceDescription2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceOptionality2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceOptionalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationIdEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class ArgVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "org.eclipse.opencert.sam.arg.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (CaseEditPart.MODEL_ID.equals(view.getType())) {
				return CaseEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				ArgDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (ArgPackage.eINSTANCE.getCase()
				.isSuperTypeOf(domainElement.eClass())
				&& isDiagram((Case) domainElement)) {
			return CaseEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
				.getModelID(containerView);
		if (!CaseEditPart.MODEL_ID.equals(containerModelID)
				&& !"arg".equals(containerModelID)) { //$NON-NLS-1$
			return -1;
		}
		int containerVisualID;
		if (CaseEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = CaseEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case CaseEditPart.VISUAL_ID:
			if (ArgPackage.eINSTANCE.getClaim().isSuperTypeOf(
					domainElement.eClass())) {
				return ClaimEditPart.VISUAL_ID;
			}
			if (ArgPackage.eINSTANCE.getAgreement().isSuperTypeOf(
					domainElement.eClass())) {
				return AgreementEditPart.VISUAL_ID;
			}
			if (ArgPackage.eINSTANCE.getArgumentReasoning().isSuperTypeOf(
					domainElement.eClass())) {
				return ArgumentReasoningEditPart.VISUAL_ID;
			}
			if (ArgPackage.eINSTANCE.getArgumentation().isSuperTypeOf(
					domainElement.eClass())) {
				return ArgumentationEditPart.VISUAL_ID;
			}
			if (ArgPackage.eINSTANCE.getInformationElementCitation()
					.isSuperTypeOf(domainElement.eClass())) {
				return InformationElementCitationEditPart.VISUAL_ID;
			}
			if (ArgPackage.eINSTANCE.getArgumentElementCitation()
					.isSuperTypeOf(domainElement.eClass())) {
				return ArgumentElementCitationEditPart.VISUAL_ID;
			}
			if (ArgPackage.eINSTANCE.getChoice().isSuperTypeOf(
					domainElement.eClass())) {
				return ChoiceEditPart.VISUAL_ID;
			}
			break;
		case ClaimEditPart.VISUAL_ID:
			if (ArgPackage.eINSTANCE.getChoice().isSuperTypeOf(
					domainElement.eClass())) {
				return Choice2EditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
				.getModelID(containerView);
		if (!CaseEditPart.MODEL_ID.equals(containerModelID)
				&& !"arg".equals(containerModelID)) { //$NON-NLS-1$
			return false;
		}
		int containerVisualID;
		if (CaseEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = CaseEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case CaseEditPart.VISUAL_ID:
			if (ClaimEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (AgreementEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ArgumentReasoningEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ArgumentationEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InformationElementCitationEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ArgumentElementCitationEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ChoiceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ClaimEditPart.VISUAL_ID:
			if (ClaimIdEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ClaimDescriptionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Choice2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AgreementEditPart.VISUAL_ID:
			if (AgreementIdEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (AgreementDescriptionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ArgumentReasoningEditPart.VISUAL_ID:
			if (ArgumentReasoningIdEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ArgumentReasoningDescriptionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ArgumentationEditPart.VISUAL_ID:
			if (ArgumentationIdEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ArgumentationDescriptionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case InformationElementCitationEditPart.VISUAL_ID:
			if (InformationElementCitationIdEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InformationElementCitationDescriptionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ArgumentElementCitationEditPart.VISUAL_ID:
			if (ArgumentElementCitationIdEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ArgumentElementCitationDescriptionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ArgumentElementCitationArgumentationReferenceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ChoiceEditPart.VISUAL_ID:
			if (ChoiceOptionalityEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ChoiceDescriptionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Choice2EditPart.VISUAL_ID:
			if (ChoiceOptionality2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ChoiceDescription2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AssertedInferenceEditPart.VISUAL_ID:
			if (AssertedInferenceCardinalityEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AssertedEvidenceEditPart.VISUAL_ID:
			if (AssertedEvidenceCardinalityEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AssertedContextEditPart.VISUAL_ID:
			if (AssertedContextCardinalityEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (ArgPackage.eINSTANCE.getAssertedInference().isSuperTypeOf(
				domainElement.eClass())) {
			return AssertedInferenceEditPart.VISUAL_ID;
		}
		if (ArgPackage.eINSTANCE.getAssertedEvidence().isSuperTypeOf(
				domainElement.eClass())) {
			return AssertedEvidenceEditPart.VISUAL_ID;
		}
		if (ArgPackage.eINSTANCE.getAssertedContext().isSuperTypeOf(
				domainElement.eClass())) {
			return AssertedContextEditPart.VISUAL_ID;
		}
		if (ArgPackage.eINSTANCE.getAssertedChallenge().isSuperTypeOf(
				domainElement.eClass())) {
			return AssertedChallengeEditPart.VISUAL_ID;
		}
		if (ArgPackage.eINSTANCE.getAssertedCounterEvidence().isSuperTypeOf(
				domainElement.eClass())) {
			return AssertedCounterEvidenceEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(Case element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case CaseEditPart.VISUAL_ID:
			return false;
		case AgreementEditPart.VISUAL_ID:
		case ArgumentReasoningEditPart.VISUAL_ID:
		case ArgumentationEditPart.VISUAL_ID:
		case InformationElementCitationEditPart.VISUAL_ID:
		case ArgumentElementCitationEditPart.VISUAL_ID:
		case ChoiceEditPart.VISUAL_ID:
		case Choice2EditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
					.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
					.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
					.getNodeVisualID(containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
					.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
