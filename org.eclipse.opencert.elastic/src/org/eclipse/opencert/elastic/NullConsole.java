/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

/*
 * Logs all to /dev/null
 *
 */
class NullConsole implements ElasticConsole {

	@Override
	public void log(Object any) {
		// ignore
	}

	@Override
	public void info(String format, Object... arguments) {
		// ignore
	}

	@Override
	public void log(String format, Object... arguments) {
		// ignore
	}

	@Override
	public void error(String format, Object... arguments) {
		// ignore
	}
}
