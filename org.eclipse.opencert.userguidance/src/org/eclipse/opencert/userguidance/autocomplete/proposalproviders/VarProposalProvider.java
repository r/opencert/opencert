/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.autocomplete.proposalproviders;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.opencert.userguidance.autocomplete.ContentProposal2;
import org.eclipse.opencert.userguidance.util.ImageUtil;

public class VarProposalProvider extends AbstractProposalProvider {

	private final static String path = "/icons/proposalicons/var.png";

	private static Image icon = null;

	public VarProposalProvider(EObject eobj, int featureID) {
		super(eobj, featureID);
	}

	@Override
	protected List<ContentProposal2> createProposals(String stringToComplete,
			EObject eobj, int featureID) {
//		Set<String> alreadyUsed = new HashSet<String>();
//		List<ContentProposal2> result = new ArrayList<ContentProposal2>();
//		SafetyCase safetyCase = (SafetyCase) EcoreUtil.getRootContainer(eobj);
//		for (NodeItem item : safetyCase.getItems()) {
//			if (item instanceof DescribedItem) {
//				String description = ((DescribedItem) item).getDescription();
//				List<Token> tokens = LabelParserUtil.parse(description);
//				for (Token token : tokens) {
//					if (token instanceof HighlightItem) {
//						HighlightItem highlightItem = (HighlightItem) token;
//						if (highlightItem.getItemType() == ItemHeaderType.var) {
//							String varString = "var:" + highlightItem.getBody();
//							if (varString.toLowerCase().startsWith(
//									stringToComplete.toLowerCase())) {
//								if (!alreadyUsed.contains(varString)) {
//									alreadyUsed.add(varString);
//									ContentProposal2 proposal = new ContentProposal2(
//											varString, null, getImage());
//									result.add(proposal);
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//		return result;
		return null;
	}

	@Override
	protected Image createImage() {
		if (icon == null) {
			icon = ImageUtil.createImage(this, path);
		}

		return icon;
	}
}
