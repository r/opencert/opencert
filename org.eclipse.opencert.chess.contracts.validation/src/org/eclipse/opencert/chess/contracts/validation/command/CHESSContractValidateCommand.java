/*******************************************************************************
 *
 * Copyright (c) 2017 Intecs SpA 
*
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.contracts.validation.command;


import org.eclipse.emf.ecore.EObject;
import org.polarsys.chess.validator.command.ChessGenericValidateCommand;

public class CHESSContractValidateCommand extends ChessGenericValidateCommand  {

	public CHESSContractValidateCommand(String label, String pluginId,
			EObject selectedElement) {
		super(label, pluginId, selectedElement);
	}
	
}
