/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.listeners.metrics.MetricsEstimationTreeMenuListener;
import org.eclipse.opencert.webapp.reports.view.common.AbstractComplianceDetailsPanel;

import com.vaadin.annotations.Theme;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@Theme("opencerttheme")
public class MetricsImplementationPanel extends AbstractComplianceDetailsPanel implements MetricsEstimationTreeMenuListener {



	private static final long serialVersionUID = 7480397371114139317L;
	private VerticalLayout mainPanel;
	private VerticalLayout titles;
	private VerticalLayout charts;
	private Label selectMetricsLabel;
	private Label metricsTitleLabel;
	private Label metricsGoalTitleLabel;
	private Label metricsTipTitleLabel;
	
	private long baselineFrameworkID;
	
	private AssuranceProjectWrapper project;
	
    private static final String ALL_METRICS_TITLE = "All Metrics";
    
    private static final String BASELINE_METRICS_TITLE = "Baseline Metrics";
    private static final String BASELINE_METRICS_GOAL = "Goal: Plan of certification for a given standard - Traceability of Requirements Maintenance";
    private static final String BASELINE_METRICS_TIP = "Tip: These metrics are used to have an overall view of the Traceability of the Requirements Maintenance as well as the different kind of Requirements and its levels of criticality and applicability defined by the reference assurance framework.";
    
    private static final String MAPPING_METRICS_TITLE = "Mapping Metrics";
    private static final String MAPPING_METRICS_GOAL = "Goal: Maintenance of mappings";
    private static final String MAPPING_METRICS_TIP = "Tip: This metric is used to represent the status of each type of compliance mappings.";
   
    private static final String ASSURANCE_ASSET_METRICS_TITLE = "Assurance Asset Metrics";
    private static final String ASSURANCE_ASSET_METRICS_GOAL = "Goal: Monitor Assurance Assets";
    private static final String ASSURANCE_ASSET_METRICS_TIP = "Tip: This metric is used for monitoring the activities related with Assurance Asset. The monitored activities include modification, creation, evaluation, revocation, etc.";
    
    private static final String REFFRAMEWORK_METRICS_TITLE = "Refframework Metrics";
    private static final String REFFRAMEWORK_METRICS_GOAL = "Goal: Coverage of Assurance Project";
    private static final String REFFRAMEWORK_METRICS_TIP = "Tip: The metric of the coverage between the Baseline Framework and a Reference Framework is to visually represent the relationship among them.";
    
    private static final String PROCESS_METRICS_TITLE = "Process Metrics";
    private static final String PROCESS_METRICS_TIP = "Tip: The purpose of these metrics is to give a specific overview of the relations between the participants and the organizations with the work they have done. As well as the time evolution of the project.";
    
    private static final String MONITOR_PROCESS_METRICS_TITLE = "Monitor of Process";
    private static final String MONITOR_PROCESS_METRICS_GOAL = "Goal: Monitor of Assurance Process";
    
    private static final String TIME_EFFICIENCY_METRICS_TITLE = "Time Efficiency";
    private static final String TIME_EFFICIENCY_METRICS_GOAL = "Goal: Time Efficiency";
   
    private static final String RESOURCE_EFFICIENCY_METRICS_TITLE = "Resource Efficiency";
    private static final String RESOURCE_EFFICIENCY_METRICS_GOAL = "Goal: Resource Efficiency";
    
    private static final String ARGUMENTATION_METRICS_TITLE = "Argumentation Metrics";  
    private static final String ARGUMENTATION_METRICS_GOAL = "Goal: Monitor of Argumentation Construction";
    private static final String ARGUMENTATION_METRICS_TIP = "Tip: These metrics are used to monitor the argumentation process and to help the user to know the status of safety argumentation, e.g., how many undeveloped safety claims are left or whether all the evidences are connected with the safety claims.";
	
    
    
    
	public MetricsImplementationPanel() {
		init();
    }
	
	
	
	public void init() {
		mainPanel = new VerticalLayout();
		mainPanel.setWidth("97%");
		mainPanel.setStyleName("layoutSpacingMetrics");
		mainPanel.setSpacing(true);
		
		
		
		titles = new VerticalLayout();		
		titles.setStyleName("layoutSpacing");
		titles.setSpacing(true);
		
		mainPanel.addComponent(titles);
		
		charts = new VerticalLayout();
		charts.setWidth("100%");
		charts.setHeight("100%");
		charts.setStyleName("layoutSpacing");
		charts.setSpacing(true);
		charts.setSizeFull();
		
		mainPanel.addComponent(charts);
		
		
		titleInitLayout();
		chartsInitLayout();
		
		//add mainPanel component
		setCompositionRoot(mainPanel);
		setSizeFull();
	}
	
	
	private void chartsInitLayout() {
		selectMetricsLabel = new Label("Please select specific <b>Metrics Type</b> in <b>Metrics Menu</b> "
				+ "panel to see <b>Metrics Details</b>.", ContentMode.HTML);
		selectMetricsLabel.setStyleName("metricsHelp");
		charts.addComponent(selectMetricsLabel);
	}
	

	
	
	private void titleInitLayout() {
		
		metricsTitleLabel = new Label("Metrics Details");
		metricsTitleLabel.addStyleName("metricsMainTitle");
		titles.addComponent(metricsTitleLabel);
		metricsGoalTitleLabel = new Label("");
		titles.addComponent(metricsGoalTitleLabel);
		metricsTipTitleLabel = new Label("");
		titles.addComponent(metricsTipTitleLabel);
	}
	



	@Override
	public void treeElementUnselected(long baselineFrameworkID, AssuranceProjectWrapper project) {
		this.project=project;
		this.baselineFrameworkID=baselineFrameworkID;
		init();
	}




	@Override
	public void treeElementSelected(String type, AssuranceProjectWrapper newProject) {

		this.project=newProject;
		titles.removeComponent(metricsTitleLabel);
		titles.removeComponent(metricsGoalTitleLabel);
		titles.removeComponent(metricsTipTitleLabel);
		charts.removeAllComponents();
		
		if (ALL_METRICS_TITLE.equals(type)) {
			metricsTitleLabel = new Label(ALL_METRICS_TITLE);
			metricsTitleLabel.addStyleName("metricsMainTitle");
			metricsGoalTitleLabel = new Label("");
			metricsTipTitleLabel = new Label("");
			
			//baseline
			Label baselineTitle = new Label(BASELINE_METRICS_TITLE);
			baselineTitle.addStyleName("metricsMainTitle");
			charts.addComponent(baselineTitle);
			Label baselineGoal = new Label(BASELINE_METRICS_GOAL);
			baselineGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(baselineGoal);
			Label baselineTip = new Label(BASELINE_METRICS_TIP);
			baselineTip.addStyleName("metricsTipTitle");
			charts.addComponent(baselineTip);
			BaselineChartsMetrics baselineCharts = new BaselineChartsMetrics(baselineFrameworkID);
			charts.addComponent(baselineCharts);
			
			//Mapping
			Label mappingTitle = new Label(MAPPING_METRICS_TITLE);
			mappingTitle.addStyleName("metricsMainTitle");
			charts.addComponent(mappingTitle);
			Label mappingGoal = new Label(MAPPING_METRICS_GOAL);
			mappingGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(mappingGoal);
			Label mappingTip = new Label(MAPPING_METRICS_TIP);
			mappingTip.addStyleName("metricsTipTitle");
			charts.addComponent(mappingTip);
			MappingChartsMetrics mappingCharts = new MappingChartsMetrics(baselineFrameworkID);
			charts.addComponent(mappingCharts);
			
			
			//Assurance
			Label assuranceTitle = new Label(ASSURANCE_ASSET_METRICS_TITLE);
			assuranceTitle.addStyleName("metricsMainTitle");
			charts.addComponent(assuranceTitle);
			Label assuranceGoal = new Label(ASSURANCE_ASSET_METRICS_GOAL);
			assuranceGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(assuranceGoal);
			Label assuranceTip = new Label(ASSURANCE_ASSET_METRICS_TIP);
			assuranceTip.addStyleName("metricsTipTitle");
			charts.addComponent(assuranceTip);
			AssuranceChartsMetrics assuranceCharts = new AssuranceChartsMetrics(project);
			charts.addComponent(assuranceCharts);
			
			
			//Refframework
			Label reffTitle = new Label(REFFRAMEWORK_METRICS_TITLE);
			reffTitle.addStyleName("metricsMainTitle");
			charts.addComponent(reffTitle);
			Label reffGoal = new Label(REFFRAMEWORK_METRICS_GOAL);
			reffGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(reffGoal);
			Label reffTip = new Label(REFFRAMEWORK_METRICS_TIP);
			reffTip.addStyleName("metricsTipTitle");
			charts.addComponent(reffTip);
			RefframeworkChartsMetrics refframeworkCharts = new RefframeworkChartsMetrics(baselineFrameworkID);
			charts.addComponent(refframeworkCharts);
			
			//process
			Label metricsProcessMainTitle = new Label(PROCESS_METRICS_TITLE);
			metricsProcessMainTitle.addStyleName("metricsMainTitle");
			charts.addComponent(metricsProcessMainTitle);
			Label processTip = new Label(PROCESS_METRICS_TIP);
			processTip.addStyleName("metricsTipTitle");
			charts.addComponent(processTip);
			
			//monitor process metrics
			Label metricsProcessTitle = new Label(MONITOR_PROCESS_METRICS_TITLE);
			metricsProcessTitle.addStyleName("metricsMainTitle");
			charts.addComponent(metricsProcessTitle);
			Label metricsProcessGoal = new Label(MONITOR_PROCESS_METRICS_GOAL);
			metricsProcessGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(metricsProcessGoal);
			MonitorProcessChartsMetrics processMetrics = new MonitorProcessChartsMetrics(project);
			charts.addComponent(processMetrics);
			
			//time efficiency
			
			Label metricsTimeTitle = new Label(TIME_EFFICIENCY_METRICS_TITLE);
			metricsTimeTitle.addStyleName("metricsMainTitle");
			charts.addComponent(metricsTimeTitle);
			Label metricsTimeGoal = new Label(MONITOR_PROCESS_METRICS_GOAL);
			metricsTimeGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(metricsTimeGoal);
			TimeEfficiencyChartsMetrics timeCharts = new TimeEfficiencyChartsMetrics(project);
			charts.addComponent(timeCharts);
			
			//resource efficiencty
			Label metricsResourceTitle =  new Label(RESOURCE_EFFICIENCY_METRICS_TITLE);
			metricsResourceTitle.addStyleName("metricsMainTitle");
			charts.addComponent(metricsResourceTitle);
			Label metricsResourceGoal =  new Label(RESOURCE_EFFICIENCY_METRICS_GOAL);
			metricsResourceGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(metricsResourceGoal);
			ResourceEfficiencyChartsMetrics resourceCharts = new ResourceEfficiencyChartsMetrics(project);
			charts.addComponent(resourceCharts);
			
			//Argumentation
			Label argumentationTitle = new Label(ARGUMENTATION_METRICS_TITLE);
			argumentationTitle.addStyleName("metricsMainTitle");	
			charts.addComponent(argumentationTitle);
			Label argumentationGoal = new Label(ARGUMENTATION_METRICS_GOAL);
			argumentationGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(argumentationGoal);
			Label argumentationTip = new Label(ARGUMENTATION_METRICS_TIP);
			argumentationTip.addStyleName("metricsTipTitle");
			charts.addComponent(argumentationTip);
			ArgumentationChartsMetrics argCharts = new ArgumentationChartsMetrics(project);
			charts.addComponent(argCharts);
			
			
			charts.setSizeFull();
			
		}
		else if (BASELINE_METRICS_TITLE.equals(type)){
			metricsTitleLabel = new Label(BASELINE_METRICS_TITLE);
			metricsTitleLabel.addStyleName("metricsMainTitle");
			metricsGoalTitleLabel = new Label(BASELINE_METRICS_GOAL);
			metricsGoalTitleLabel.addStyleName("metricsGoalTitle");
			metricsTipTitleLabel = new Label(BASELINE_METRICS_TIP);
			metricsTipTitleLabel.addStyleName("metricsTipTitle");
			BaselineChartsMetrics baselineCharts = new BaselineChartsMetrics(baselineFrameworkID);
			charts.addComponent(baselineCharts);
			charts.setSizeFull();
			
		}
		else if (ASSURANCE_ASSET_METRICS_TITLE.equals(type)){
			metricsTitleLabel = new Label(ASSURANCE_ASSET_METRICS_TITLE);
			metricsTitleLabel.addStyleName("metricsMainTitle");
			metricsGoalTitleLabel = new Label(ASSURANCE_ASSET_METRICS_GOAL);
			metricsGoalTitleLabel.addStyleName("metricsGoalTitle");
			metricsTipTitleLabel = new Label(ASSURANCE_ASSET_METRICS_TIP);
			metricsTipTitleLabel.addStyleName("metricsTipTitle");
			AssuranceChartsMetrics assuranceCharts = new AssuranceChartsMetrics(project);
			charts.addComponent(assuranceCharts);
			charts.setSizeFull();
		}
		else if (REFFRAMEWORK_METRICS_TITLE.equals(type)){
			metricsTitleLabel = new Label(REFFRAMEWORK_METRICS_TITLE);
			metricsTitleLabel.addStyleName("metricsMainTitle");
			metricsGoalTitleLabel = new Label(REFFRAMEWORK_METRICS_GOAL);
			metricsGoalTitleLabel.addStyleName("metricsGoalTitle");
			metricsTipTitleLabel = new Label(REFFRAMEWORK_METRICS_TIP);
			metricsTipTitleLabel.addStyleName("metricsTipTitle");
			RefframeworkChartsMetrics refframeworkCharts = new RefframeworkChartsMetrics(baselineFrameworkID);
			charts.addComponent(refframeworkCharts);
			charts.setSizeFull();
		}
		else if (MAPPING_METRICS_TITLE.equals(type)){
			metricsTitleLabel = new Label(MAPPING_METRICS_TITLE);
			metricsTitleLabel.addStyleName("metricsMainTitle");
			metricsGoalTitleLabel = new Label(MAPPING_METRICS_GOAL);
			metricsGoalTitleLabel.addStyleName("metricsGoalTitle");
			metricsTipTitleLabel = new Label(MAPPING_METRICS_TIP);
			metricsTipTitleLabel.addStyleName("metricsTipTitle");
			MappingChartsMetrics mappingCharts = new MappingChartsMetrics(baselineFrameworkID);
			charts.addComponent(mappingCharts);
			charts.setSizeFull();
		}
		else if (PROCESS_METRICS_TITLE.equals(type)){
			metricsTitleLabel = new Label(PROCESS_METRICS_TITLE);
			metricsTitleLabel.addStyleName("metricsMainTitle");
			metricsGoalTitleLabel = new Label("");
			metricsTipTitleLabel = new Label(PROCESS_METRICS_TIP);
			metricsTipTitleLabel.addStyleName("metricsTipTitle");
			
			//monitor process metrics
			Label metricsProcessTitle = new Label(MONITOR_PROCESS_METRICS_TITLE);
			metricsProcessTitle.addStyleName("metricsMainTitle");
			charts.addComponent(metricsProcessTitle);
			Label metricsProcessGoal = new Label(MONITOR_PROCESS_METRICS_GOAL);
			metricsProcessGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(metricsProcessGoal);
			MonitorProcessChartsMetrics processMetrics = new MonitorProcessChartsMetrics(project);
			charts.addComponent(processMetrics);
			
			//time efficiency
			
			Label metricsTimeTitle = new Label(TIME_EFFICIENCY_METRICS_TITLE);
			metricsTimeTitle.addStyleName("metricsMainTitle");
			charts.addComponent(metricsTimeTitle);
			Label metricsTimeGoal = new Label(MONITOR_PROCESS_METRICS_GOAL);
			metricsTimeGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(metricsTimeGoal);
			TimeEfficiencyChartsMetrics timeCharts = new TimeEfficiencyChartsMetrics(project);
			charts.addComponent(timeCharts);
			
			//resource efficiencty
			Label metricsResourceTitle =  new Label(RESOURCE_EFFICIENCY_METRICS_TITLE);
			metricsResourceTitle.addStyleName("metricsMainTitle");
			charts.addComponent(metricsResourceTitle);
			Label metricsResourceGoal =  new Label(RESOURCE_EFFICIENCY_METRICS_GOAL);
			metricsResourceGoal.addStyleName("metricsGoalTitle");
			charts.addComponent(metricsResourceGoal);
			ResourceEfficiencyChartsMetrics resourceCharts = new ResourceEfficiencyChartsMetrics(project);
			charts.addComponent(resourceCharts);
			
			charts.setSizeFull();
			
			
		}
		else if (MONITOR_PROCESS_METRICS_TITLE.equals(type)){
			metricsTitleLabel = new Label(MONITOR_PROCESS_METRICS_TITLE);
			metricsTitleLabel.addStyleName("metricsMainTitle");
			metricsGoalTitleLabel = new Label(MONITOR_PROCESS_METRICS_GOAL);
			metricsGoalTitleLabel.addStyleName("metricsGoalTitle");
			metricsTipTitleLabel = new Label(PROCESS_METRICS_TIP);
			metricsTipTitleLabel.addStyleName("metricsTipTitle");
			MonitorProcessChartsMetrics processMetrics = new MonitorProcessChartsMetrics(project);
			charts.addComponent(processMetrics);
			charts.setSizeFull();
		}
		else if (TIME_EFFICIENCY_METRICS_TITLE.equals(type)){
			metricsTitleLabel = new Label(TIME_EFFICIENCY_METRICS_TITLE);
			metricsTitleLabel.addStyleName("metricsMainTitle");
			metricsGoalTitleLabel = new Label(TIME_EFFICIENCY_METRICS_GOAL);
			metricsGoalTitleLabel.addStyleName("metricsGoalTitle");
			metricsTipTitleLabel = new Label(PROCESS_METRICS_TIP);
			metricsTipTitleLabel.addStyleName("metricsTipTitle");
			TimeEfficiencyChartsMetrics timeCharts = new TimeEfficiencyChartsMetrics(project);
			charts.addComponent(timeCharts);
			charts.setSizeFull();
		}
		else if (RESOURCE_EFFICIENCY_METRICS_TITLE.equals(type)){
			metricsTitleLabel = new Label(RESOURCE_EFFICIENCY_METRICS_TITLE);
			metricsTitleLabel.addStyleName("metricsMainTitle");
			metricsGoalTitleLabel = new Label(RESOURCE_EFFICIENCY_METRICS_GOAL);
			metricsGoalTitleLabel.addStyleName("metricsGoalTitle");
			metricsTipTitleLabel = new Label(PROCESS_METRICS_TIP);
			metricsTipTitleLabel.addStyleName("metricsTipTitle");
			ResourceEfficiencyChartsMetrics resourceCharts = new ResourceEfficiencyChartsMetrics(project);
			charts.addComponent(resourceCharts);
			charts.setSizeFull();
		}
		else if (ARGUMENTATION_METRICS_TITLE.equals(type)){
			metricsTitleLabel = new Label(ARGUMENTATION_METRICS_TITLE);
			metricsTitleLabel.addStyleName("metricsMainTitle");			
			metricsGoalTitleLabel = new Label(ARGUMENTATION_METRICS_GOAL);
			metricsGoalTitleLabel.addStyleName("metricsGoalTitle");
			metricsTipTitleLabel = new Label(ARGUMENTATION_METRICS_TIP);
			metricsTipTitleLabel.addStyleName("metricsTipTitle");
			ArgumentationChartsMetrics argCharts = new ArgumentationChartsMetrics(project);
			charts.addComponent(argCharts);
			charts.setSizeFull();
		}
		else {
			chartsInitLayout();
			metricsTitleLabel = new Label("Metrics Details");
			metricsTitleLabel.addStyleName("metricsMainTitle");
			metricsGoalTitleLabel = new Label("");
			metricsTipTitleLabel = new Label("");
		}
		
		titles.addComponent(metricsTitleLabel);
		titles.addComponent(metricsGoalTitleLabel);
		titles.addComponent(metricsTipTitleLabel);

	}







}
