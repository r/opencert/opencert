// EPF -> LIB;CONF
// OpenCert -> ART;PRO;AA


pre {
  'Running ETL'.println();
  var conSelection;	
  // We detect the situation in which there are more than one process component
  var processComponentList=CONF!ProcessComponent.allInstances;
  if (processComponentList.size()>1){
  	// We need to inspect the opencert_export category
  	var exportCategory=CONF!CustomCategory.allInstances.selectOne(cc|cc.name="opencert_export");
  	var processId=exportCategory.categorizedElement.first();
  	var containerProcessComponent=LIB!ProcessComponent.allInstances.selectOne(pc|pc.process.id=processId);
  	conSelection=new Sequence;
  	conSelection.add(containerProcessComponent.id);
  	// We add the process component of the extended capability patterns
  	for(patterns in containerProcessComponent.process.includesPattern){
  		conSelection.add(LIB!ProcessComponent.allInstances.selectOne(pc|pc.process.id=patterns).id);
  	} 
  }else{
  	var methodConfSelectionValue=CONF!MethodElementProperty.
  		allInstances.selectOne(ite : CONF!MethodElementProperty | ite.name.equals("Config_doneLoadCheckPkgs")).value;
  	conSelection=methodConfSelectionValue.split("/");	
  }
}

rule ProcessComponent2ProcessModel
	transform l : LIB!ProcessComponent
	to p : PRO!ProcessModel,artm:ART!ArtefactModel{
	guard :conSelection.includes(l.id) and l.process.isTypeOf(LIB!DeliveryProcess)
		p.name=l.process.name;
		p.id=l.process.id;
		p.description=l.process.briefDescription;
		artm.name=l.name;
		artm.id=l.id;
		artm.description=l.briefDescription;
		
		for(bde in l.process.breakdownElement.select(wbd|wbd.isKindOf(LIB!WorkBreakdownElement))){
			p.ownedActivity.add(bde.equivalent());
		}
		
		/*for (task in LIB!TaskDescriptor.allInstances.select(td|td.isPPEInConfiguration())){//l.getTaskDescriptors(l.process.breakdownElement)) {
			//"Tasks".println();
			//task.name.println();
			p.ownedActivity.add(task.equivalent());
		}
		for(activity in LIB!Activity.allInstances.select(act|act.isPPEInConfiguration() and act.isTypeOf(LIB!Activity))){//l.getActivities(l.process.breakdownElement)){
			//activity.name.println();
			p.ownedActivity.add(activity.equivalent());
		}
		for(pattern in LIB!CapabilityPattern.allInstances.select(cp|cp.isCapabilityPatternInConfiguration() and (not cp.isOriginalCapabilityPattern()))){//l.getCapabilityPatterns(l.process.breakdownElement)){
			//pattern.name.println();
			p.ownedActivity.add(pattern.equivalent());
		}*/
		
		for(rd in LIB!RoleDescriptor.allInstances.select(r|r.isPPEInConfiguration())){//l.getRoleDescriptors(l.process.breakdownElement)){
			//rd.name.println();
			p.ownedParticipant.add(rd.equivalent());
		}
		
		// Guidelines -> Techniques
		for(guideline in LIB!Guideline.allInstances.select(gui|gui.isGuidelineInConfiguration())){
			p.ownedTechnique.add(guideline.equivalent());
		}
		
		// Practices -> Techniques
		for(practice in LIB!Practice.allInstances.select(pra|pra.isPracticeInConfiguration())){
			p.ownedTechnique.add(practice.equivalent());
		}
		
		for(toolMentor in LIB!ToolMentor.allInstances.select(tm|tm.isToolMentorInConfiguration())){
			p.ownedParticipant.add(toolMentor.equivalent());
		}
		
		var artD;
		for(art in LIB!WorkProductDescriptor.allInstances.select(wp|wp.isArtifact() and wp.isPPEInConfiguration())){
			artD=new ART!ArtefactDefinition;
			artD.name=art.name;
			artD.id=art.id;
			artD.description=art.briefDescription;
			artD.artefact.add(art.equivalent());
			artm.artefact.add(artD);
		}
		// Organizations can come from TeamProfiles and RoleSets
		// Only rolesets with roles that are used in the process are added to the process model
		for(rs in LIB!RoleSet.allInstances.select(ins|ins.isRoleSetInConfiguration())){
			p.ownedParticipant.add(rs.equivalent());
		}
		// All TeamProfiles that are in the configuration can be an organization
		for(tp in l.process.breakdownElement.select(be|be.isTypeOf(LIB!TeamProfile))){//LIB!TeamProfile.allInstances.select(tpins|tpins.isPPEInConfiguration())){
			p.ownedParticipant.add(tp.equivalent());	
		}
			
}

rule Practice2Technique
	transform pra:LIB!Practice
	to te:PRO!Technique{
		guard : pra.isPracticeInConfiguration()
		te.id=pra.id;
		te.name=pra.name;
		te.description=pra.briefDescription;
}

rule TeamProfile2Organization
	transform tp:LIB!TeamProfile
	to org:PRO!Organization{
		guard : tp.isPPEInConfiguration()
		org.id=tp.id;
		org.name=tp.name;
		org.description=tp.briefDescription;
		for(subTeam in tp.subTeam){
			org.subOrganization.add(tp.getTeamProfile(subTeam).equivalent());
		}	
	}

rule RoleSet2Organization
	transform rs:LIB!RoleSet
	to org:PRO!Organization{
		guard :rs.isRoleSetInConfiguration()
		//"El nombre del RoleSet es ".concat(rs.name).println();
		org.id=rs.id;
		org.name=rs.name;
		org.description=rs.briefDescription;
	}

/*rule Artifact2ArtifactDefinition
	transform arti:LIB!Artifact
	to artDef:ART!ArtefactDefinition{
		guard: (arti.isArtifactInConfiguration())
		artDef.name=arti.name;
		artDef.id=arti.id;
		artDef.description=arti.briefDescription;
		// obtenemos la referencia a todos los artefactos
		var wpDescriptorList=LIB!WorkProductDescriptor.allInstances.select(wpd|wpd.workProduct=arti.id and wpd.isPPEInConfiguration());
		for(wpi in wpDescriptorList){
			//wpi.name.println();
			artDef.artefact.add(wpi.equivalent());
		}
	}*/

rule WorkProductDescriptor2Artefact
	transform wp:LIB!WorkProductDescriptor
	to art:ART!Artefact{
		guard :(wp.isArtifact() and wp.isPPEInConfiguration())//conSelection.contains(wp.getContainerProcessComponent().id))
		art.name=wp.name;
		art.id=wp.id;
		art.description=wp.briefDescription;
}

	
rule Guideline2Technique
	transform gu:LIB!Guideline
	to tec:PRO!Technique{
		guard :gu.isGuidelineInConfiguration()
		tec.name=gu.name;
		tec.id=gu.id;
		tec.description=gu.briefDescription;
	}

rule RoleDescriptor2Person
	transform rd:LIB!RoleDescriptor
	to per:PRO!Person{
		guard :rd.isPPEInConfiguration()
		per.name=rd.name;
		per.id=rd.id;
		per.description=rd.briefDescription;
		for(art in rd.getResponsibleForArtefacts()){
			per.ownedArtefact.add(art.equivalent());
		}
		// Membership from organizations
		// RoleSet -> If the Role associated to the RoleDescriptor is member of the organization
		if(rd.role<>null){
			for(rs in LIB!RoleSet.allInstances.select(roleSet|roleSet.role.includes(rd.role))){
				per.organization.add(rs.equivalent());
			}
		}
		// TeamProfile
		for(tp in LIB!TeamProfile.allInstances.select(tpIns|tpIns.isPPEInConfiguration() and tpIns.role.includes(rd.id))){
			per.organization.add(tp.equivalent());	
		}
}

rule RoleSet2Organization
	transform rs:LIB!RoleSet
	to org:PRO!Organization{
		guard : rs.isRoleSetInConfiguration()
		org.name=rs.name;
		org.id=rs.id;
		org.description=rs.briefDescription;
}

rule Milestone2Activity
	transform mil:LIB!Milestone
	to act:PRO!Activity{
	guard: mil.isPPEInConfiguration()
		act.name="Milestone "+mil.name;
		act.id=mil.id;
		act.description=mil.briefDescription;
		// artefacts produced
		for(id in mil.requiredResult){
			act.producedArtefact.add(LIB!WorkProductDescriptor.allInstances.selectOne(wpd|wpd.id=id).equivalent());
		}	
	}

rule TaskDescriptor2Activity
	transform l: LIB!TaskDescriptor
	to act:PRO!Activity{
	guard :l.isPPEInConfiguration()//conSelection.contains(l.getContainerProcessComponent().id)
		
		act.name=l.name;
		act.id=l.id;
		act.description=l.briefDescription;
		// Participants equivalent to roles
		for(rd in l.getTaskParticipants()){
			act.participant.add(rd.equivalent());
		}
		// Participant equivalent to tools
		if(l.task<>null){
			var task=LIB!Task.allInstances.selectOne(t|t.id=l.task);
			var toolMentor;
			for (toolMentorId in task.toolMentor){
				act.participant.add(LIB!ToolMentor.allInstances.selectOne(tm|tm.id=toolMentorId).equivalent());
			}
		}
		// artefacts required
		// mandatoryInput, externalInput and optionalInput
		for(id in l.externalInput){
			act.requiredArtefact.add(LIB!WorkProductDescriptor.allInstances.selectOne(wpd|wpd.id=id).equivalent());
		}
		for(id in l.optionalInput){
			act.requiredArtefact.add(LIB!WorkProductDescriptor.allInstances.selectOne(wpd|wpd.id=id).equivalent());
		}
		for(id in l.mandatoryInput){
			act.requiredArtefact.add(LIB!WorkProductDescriptor.allInstances.selectOne(wpd|wpd.id=id).equivalent());
		}
		// artefacts produced
		for(id in l.output){
			act.producedArtefact.add(LIB!WorkProductDescriptor.allInstances.selectOne(wpd|wpd.id=id).equivalent());
		}
		// Techniques associated to this task can come from guidelines associated to the taskdescriptor
		for(guid in l.guideline){
			act.technique.add(l.getGuideline(guid).equivalent());
		}
		// precedent activities -> WorkOrder of the task
		if(l.predecessor.size()>0){
			for(pred in l.predecessor.select(p|p.linkType=LIB!WorkOrderType#finishToStart)){
				act.precedingActivity.add(getWorkBreakdownElementById(pred.value).equivalent());
			}		
		}
	}
	

	
rule TooMentor2Tool
	transform et:LIB!ToolMentor
	to ot:PRO!Tool{
	guard :et.isToolMentorInConfiguration()
		var tool=et.getTool();
		ot.id=tool.id;
		ot.name=tool.name;
		ot.description=tool.briefDescription;
	}

rule CapabilityPattern2Activity
	transform cp: LIB!CapabilityPattern
	to act:PRO!Activity{
	guard : ((cp.isCapabilityPatternInConfiguration()) and (not cp.isExtendedCapabilityPattern())
			and (not cp.isOriginalCapabilityPattern()))//not cp.isContainedInProcessComponent()
		//cp.getProcessComponentName().println();
		//"Se ejecuta original capability pattern".println();
		act.name=cp.name;
		act.id=cp.id;
		
		for(bd in cp.breakdownElement.select(wbd|wbd.isKindOf(LIB!WorkBreakdownElement))){
			act.subActivity.add(bd.equivalent());
		}
		
		/*for (task in cp.getTaskDescriptors(cp.breakdownElement)) {
			act.subActivity.add(task.equivalent());
		}
		for(activity in cp.getActivities(cp.breakdownElement)){
			//activity.name.println();
			act.subActivity.add(activity.equivalent());
		}
		for (pattern in cp.getCapabilityPatterns(cp.breakdownElement)){
			//pattern.name.println();
			act.subActivity.add(pattern.equivalent());
		}*/
		// Techniques associated to this capability pattern can come from guidelines
		for(guid in cp.guideline){
			act.technique.add(cp.getGuideline(guid).equivalent());
		}
		// precedent activities -> WorkOrder of the task
		if(cp.predecessor.size()>0){
			for(pred in cp.predecessor.select(p|p.linkType=LIB!WorkOrderType#finishToStart)){
				act.precedingActivity.add(getWorkBreakdownElementById(pred.value).equivalent());
			}		
		}	
	}

rule ExtendedCapabilityPattern2Activity
	transform cp: LIB!CapabilityPattern
	to act:PRO!Activity{
	guard : ((cp.isCapabilityPatternInConfiguration()) and (cp.isExtendedCapabilityPattern()) 
	and (not cp.isOriginalCapabilityPattern()))
		// We obtain the original capability pattern.
		var originalCP=LIB!CapabilityPattern.allInstances.selectOne(ocp|ocp.id=cp.variabilityBasedOnElement);
		act.name=cp.name;
		act.id=cp.id;
		var auxActivity;
		
		for(bd in originalCP.breakdownElement.select(wbd|wbd.isKindOf(LIB!WorkBreakdownElement))){
			act.subActivity.add(bd.equivalent());
		}
		
		/*for (task in originalCP.getTaskDescriptors(originalCP.breakdownElement)) {
			act.subActivity.add(task.equivalent());
		}
		for(activity in originalCP.getActivities(originalCP.breakdownElement)){
			//activity.name.println();
			act.subActivity.add(activity.equivalent());
		}
		for (pattern in originalCP.getCapabilityPatterns(originalCP.breakdownElement)){
			//pattern.name.println();
			act.subActivity.add(pattern.equivalent());
		}*/
		// Techniques associated to this capability pattern can come from guidelines
		for(guid in originalCP.guideline){
			act.technique.add(cp.getGuideline(guid).equivalent());
		}
		// precedent activities -> WorkOrder of the task
		if(cp.predecessor.size()>0){
			for(pred in cp.predecessor.select(p|p.linkType=LIB!WorkOrderType#finishToStart)){
				act.precedingActivity.add(getWorkBreakdownElementById(pred.value).equivalent());
			}		
		}	
	}
	
rule Phase2Activity
	transform p: LIB!Phase
	to act:PRO!Activity{
		guard :conSelection.contains(p.getContainerProcessComponent().id)
		//"Se aplica Activity2Activity a ".concat(a.name).println();
		//l.name.println();
		act.name="Phase "+p.name;
		act.id=p.id;
		
		for(bd in p.breakdownElement.select(wbd|wbd.isKindOf(LIB!WorkBreakdownElement))){
			act.subActivity.add(bd.equivalent());
		}
		
		/*for (task in a.getTaskDescriptors(a.breakdownElement)) {
			act.subActivity.add(task.equivalent());
		}
		for(activity in a.getActivities(a.breakdownElement)){
			//activity.name.println();
			act.subActivity.add(activity.equivalent());
		}
		
		for (pattern in a.getCapabilityPatterns(a.breakdownElement)){
			//pattern.name.println();
			act.subActivity.add(pattern.equivalent());
		}*/
		// Techniques associated to this capability pattern can come from guidelines aand practices
		// definition
		for(guid in p.guideline){
			act.technique.add(p.getGuideline(guid).equivalent());
		}
		
		// precedent activities -> WorkOrder of the task
		if(p.predecessor.size()>0){
			for(pred in p.predecessor.select(prede|prede.linkType=LIB!WorkOrderType#finishToStart)){
				act.precedingActivity.add(getWorkBreakdownElementById(pred.value).equivalent());
			}		
		}
	}
	
rule Iteration2Activity
	transform ite: LIB!Iteration
	to act:PRO!Activity{
		guard :conSelection.contains(ite.getContainerProcessComponent().id)
		act.name="Iteration "+p.name;
		act.id=p.id;
		
		for(bd in ite.breakdownElement.select(wbd|wbd.isKindOf(LIB!WorkBreakdownElement))){
			act.subActivity.add(bd.equivalent());
		}
		// Techniques associated to this capability pattern can come from guidelines aand practices
		// definition
		for(guid in ite.guideline){
			act.technique.add(ite.getGuideline(guid).equivalent());
		}
		// precedent activities -> WorkOrder of the task
		if(ite.predecessor.size()>0){
			for(pred in ite.predecessor.select(prede|prede.linkType=LIB!WorkOrderType#finishToStart)){
				act.precedingActivity.add(getWorkBreakdownElementById(pred.value).equivalent());
			}		
		}
	}


rule Activity2Activity
	transform a : LIB!Activity
	to act:PRO!Activity{
	guard :conSelection.contains(a.getContainerProcessComponent().id)
		//"Se aplica Activity2Activity a ".concat(a.name).println();
		//l.name.println();
		act.name=a.name;
		act.id=a.id;
		
		for(bd in a.breakdownElement.select(wbd|wbd.isKindOf(LIB!WorkBreakdownElement))){
			act.subActivity.add(bd.equivalent());
		}
		
		/*for (task in a.getTaskDescriptors(a.breakdownElement)) {
			act.subActivity.add(task.equivalent());
		}
		for(activity in a.getActivities(a.breakdownElement)){
			//activity.name.println();
			act.subActivity.add(activity.equivalent());
		}
		
		for (pattern in a.getCapabilityPatterns(a.breakdownElement)){
			//pattern.name.println();
			act.subActivity.add(pattern.equivalent());
		}*/
		// Techniques associated to this capability pattern can come from guidelines aand practices
		// definition
		for(guid in a.guideline){
			act.technique.add(a.getGuideline(guid).equivalent());
		}
		
		// precedent activities -> WorkOrder of the task
		if(a.predecessor.size()>0){
			for(pred in a.predecessor.select(p|p.linkType=LIB!WorkOrderType#finishToStart)){
				act.precedingActivity.add(getWorkBreakdownElementById(pred.value).equivalent());
			}		
		}
	}
	
operation Any getTaskDescriptors(source : Set(LIB!BreadkDownElement)):Set(LIB!TaskDescriptor) {
	var res = source.select(act|act.isTypeOf(LIB!TaskDescriptor));
	//res.size().println();
	return res;
}

operation Any getActivities(source : Set(LIB!BreadkDownElement)):Set(LIB!Activity) {
	var res = source.select(act|act.isTypeOf(LIB!Activity));
	//res.size().println();
	return res;
}	

operation Any getCapabilityPatterns(source : Set(LIB!BreadkDownElement)):Set(LIB!CapabilityPattern) {
	var res = source.select(act|act.isTypeOf(LIB!CapabilityPattern));
	//res.size().println();
	return res;
}

operation Any getRoleDescriptors(source : Set(LIB!BreadkDownElement)):Set(LIB!RoleDescriptor){
	return source.select(rd|rd.isTypeOf(LIB!RoleDescriptor));
}

operation Any getWorkProductDescriptors(source : Set(LIB!BreadkDownElement)):Set(LIB!WorkProductDescriptor){
	return source.select(rd|rd.isTypeOf(LIB!WorkProductDescriptor));
}

operation LIB!WorkProductDescriptor isArtifact():Boolean{
	return LIB!Artifact.allInstances.exists(art|art.id=self.workProduct);
}

/*operation LIB!CapabilityPattern isContainedInProcessComponent():Boolean{
	var res=LIB!ProcessComponent.allInstances.exists(pc|pc.process.id=self.id);
	//self.name.println();
	//res.println();
	return res;
}*/

operation LIB!Process getProcessComponentName():String{
	var pc=LIB!ProcessComponent.allInstances.select(pc|pc.process.id=self.id);
	if(pc.isEmpty()){
		return "No tiene processComponent";
	}else{
		return pc.first().id;
	}
}

// A process element can be contained in a process in two ways
// As a part of its list of processElement
// As one of the breakdown elements included in its contained process.
operation Any getContainerProcessComponent():LIB!ProcessComponent{
	var res;
	// 1. List of processElements of the process components included in the library.
	res=LIB!ProcessComponent.allInstances.selectOne(pe|pe.processElement.includes(self));
	if(res=null){
	// Breakdown element
		if (self.superActivity=null){
			res=LIB!ProcessComponent.allInstances.selectOne(pc|pc.process.id=self.id);	
		}else{
			res=LIB!BreakdownElement.allInstances.selectOne(su|su.id=self.superActivity).getContainerProcessComponent();	
		}
	}
	return res;
}

operation Any getRoleDescriptor(id:String):LIB!BreakdownElement{
	return LIB!RoleDescriptor.allInstance.selectOne(rd|rd.id=id);
}

operation LIB!TaskDescriptor getTaskParticipants():Set(LIB!RoleDescriptor){
	var res=new Set(LIB!RoleDescriptor);
	// performedPrimarilyBy; additionallyPerformedBy; assistedBy
	for(p in self.performedPrimarilyBy){
		res.add(LIB!RoleDescriptor.allInstances.selectOne(role|role.id=p));
	}
	for(a in self.additionallyPerformedBy){
		res.add(LIB!RoleDescriptor.allInstances.selectOne(role|role.id=a));
	}
	for(asi in self.assistedBy){
		res.add(LIB!RoleDescriptor.allInstances.selectOne(role|role.id=asi));
	}
	return res;
}

operation LIB!RoleDescriptor getResponsibleForArtefacts():Set(LIB!WorkProductDescriptor){
	var res=new Set(LIB!WorkProductDescriptor);
	var wp;
	for(wp_id in self.responsibleFor){
		wp=LIB!WorkProductDescriptor.allInstances.selectOne(ins|ins.id=wp_id);
		if(wp.isArtifact()){
			res.add(wp);
		}
	}
	//res.size().println();
	return res;
}

operation LIB!Process getGuidelines():Set(LIB!Guideline){
	var res=new Set(LIB!Guideline);
	for(gId in self.guideline){
		res.add(LIB!Guideline.allInstances.selectOne(guideline|guideline.id=gId));
	}
	return res;
}

operation Any getGuideline(guidelineId:String):LIB!Guideline{
	return LIB!Guideline.allInstances.selectOne(guid|guid.id=guidelineId);
}

// Esto hay que modificarlo para tener en cuenta que una guideline puede estar contenida en cualquier tipo de elemento que desciende 
// de breakdownelement
// Una guideline está incluida en la configuración si esta enlazada a un elemento que está en la configuración.
operation LIB!Guideline isGuidelineInConfiguration():Boolean{
	var breakDownElements=LIB!BreakdownElement.allInstances.select(be|be.guideline.includes(self.id));
	var res=false;
	var i=0;
	while(i<breakDownElements.size() and not res){
		if(breakDownElements.at(i).isPPEInConfiguration()){
			res=true;
		}
		i++;
	}
	return res;
}

operation LIB!ToolMentor isToolMentorInConfiguration():Boolean{
	var taskSet=LIB!Task.allInstances.select(t|t.toolMentor.contains(self.id));
	var taskSetString=new Set(String);
	for(task in taskSet){
		taskSetString.add(task.id);
	}
	// Se seleccionan todas las tareas que tienen referencia al task que incluye el toolMentor
	var taskDescriptorSet=LIB!TaskDescriptor.allInstances.select(td|taskSetString.includes(td.task));
	var res=false;
	var i=0;
	var pc;
	// Si alguna de estos task descriptors está en la configuración -> el ToolMentor está en la configuración
	while(i<taskDescriptorSet.size() and not res){
		if(taskDescriptorSet.at(i).isPPEInConfiguration()){
			res=true;
		}
		i++;
	}
	return res;
}

operation LIB!TaskDescriptor getTools():Set(LIB!Tool){
	var res=new Set(LIB!Tool);
	var candTool;
	// 1. Encontramos la tarea asociada
	var task=LIB!Task.allInstances.selectOne(t|t.id=self.task);
	// 2. Encontramos todos los tools asociados a los tool mentors de la tarea
	if(task<>null){
		for(tool in LIB!Tool.allInstances){
			for(toolMentor in task.toolMentor){
				if(tool.toolMentor.includes(toolMentor)){
					if(not res.incudes(tool)){
						res.add(tool);
					}
				}
			}
		}
	}
	return res;
}

operation LIB!ToolMentor getTool():LIB!Tool{
	return LIB!Tool.allInstances.selectOne(tool|tool.toolMentor.contains(self.id));
}

// This method returns true if the process element is part of the configuration that is passed as second model
operation Any isPPEInConfiguration():Boolean{
	//self.name.println();
	var pc=self.getContainerProcessComponent();
	return conSelection.contains(pc.id);
}

// This method determine if a RoleSet is involved in the process given in the 
// configuration.
// roleIdList is the list of the id of the Role used in the process
operation LIB!RoleSet isRoleSetInConfiguration():Boolean{
		var roleList=new Set(String);
		roleList=self.getRolesFromConfiguration();
		return self.role.exists(roleId|roleList.includes(roleId));
}

// A capability pattern can be part of the configuration in two cases
// 1. It is contained in a process component that is the process component selected in the configuration.
// 2. It is included in a process or other capability pattern which is part of the configuration.
operation LIB!CapabilityPattern isCapabilityPatternInConfiguration():Boolean{
	// The process component associated to the capability pattern is the process component of the configuration
	var res=false;
	if (LIB!ProcessComponent.allInstances.exists(pc|pc.process.id=self.id)){
		if(conSelection.contains(LIB!ProcessComponent.allInstances.selectOne(pc|pc.process.id=self.id).id)){
			res=true;
		}		
	}else{
		res=self.isPPEInConfiguration();	
	}
	return res;	
}

// This operation obtain all the identifications fo the role defintions associated to the roledescriptors in the process passed in the configuration
operation Any getRolesFromConfiguration():Set(String){
	var roleIdList=new Set(String);
	for(rd in LIB!RoleDescriptor.allInstances.select(r|r.isPPEInConfiguration())){//l.getRoleDescriptors(l.process.breakdownElement)){
			if(rd.role<>null){
				roleIdList.add(rd.role);
			}
		}
	return roleIdList;
}

// Get the TeamProfile element with the id passed as argument.
operation LIB!TeamProfile getTeamProfile(subTeamId:String):LIB!TeamProfile{
	return LIB!TeamProfile.allInstances.selectOne(tp|tp.id=subTeamId);
}

// A practice is in a configuration if one of the elements of its reference section (for content or activities) is in the configuration
operation LIB!Practice isPracticeInConfiguration():Boolean{
	// First checking the activities
	var res=false;
	var activityReferenceList=self.activityReference.getListActivitiesById();
	if(activityReferenceList.exists(ca|ca.isPPEInConfiguration())){
		res=true;
	// Checking method content elements and practices -> contentReference (Guidance is considered content element)
	}else{
		var i=0;
		var descriptorList=getConfigurationDescriptors();
		// Checking Descriptors -> Descriptor can be a WorkProductDescriptor or a RoleDescriptor 
		while((not res) and (i<descriptorList.size())){
			if(descriptorList.at(i).isTypeOf(LIB!WorkProductDescriptor)){
				if(descriptorList.at(i).workProduct<>null){
					res=self.contentReference.includes(descriptorList.at(i).workProduct);
				}
			}else{
				if(descriptorList.at(i).role<>null){
					res=self.contentReference.includes(descriptorList.at(i).role);
				}
			}
			i++;
		}
		// Checking the taskdescriptors
		if(not res){
			var taskDescriptorList=getConfigurationTaskDescriptors();
			i=0;
			while((not res) and (i<taskDescriptorList.size())){
				if(taskDescriptorList.at(i).task<>null){
					res=self.contentReference.includes(taskDescriptorList.at(i).task);
				}
				i++;
			}	
		}
		// Checking the guidelines, which is the only type of guidance of relevance for the process model.
		// Practices are of relevance too but they cannot be attached to a process.
		if(not res){
			var configurationGuidelineList=getGuidelinesInConfiguration();
			i=0;
			while((not res) and (i<configurationGuidelineList.size())){
				res=self.contentReference.includes(configurationGuidelineList.at(i).id);
				i++;
			}		
		}
			
	}
	return res;
}

// Get the content element with the id passed in self
operation String getContentElementById():LIB!ContentElement{
	return LIB!ContentElement.allInstances.selectOne(ce|ce.id=self);
}

// Get a list of content elements whose id is included in the list passed in self
operation Set(String) getListContentElementById():Set(LIB!ContentElement){
	var res=new Set(LIB!ContentElement);
	for(ce in LIB!ContentElement.allInstances){
		if(self.includes(ce.id)){
			res.add(ce);
		}
	}
	return res;
}

// Get a list of activities whose id is included in the list passed in Self
operation Sequence(String) getListActivitiesById():Set(LIB!Activity){
	var res=new Set(LIB!Activity);
	for(act in LIB!Activity.allInstances){
		if(self.includes(act.id)){
			res.add(act);
		}
	}
	return res;
}

operation getConfigurationDescriptors():Sequence(LIB!Descriptor){
	return LIB!Descriptor.allInstances.select(des|des.isPPEInConfiguration());
}

operation getConfigurationTaskDescriptors():Sequence(LIB!TaskDescriptor){
	return LIB!TaskDescriptor.allInstances.select(td|td.isPPEInConfiguration());
}

operation getGuidelinesInConfiguration():Sequence(LIB!Guideline){
	return LIB!Guideline.allInstances.select(gui|gui.isGuidelineInConfiguration());
}

operation LIB!WorkOrder isWorkOrderInConfiguration():Boolean{
	var res=false;
	var workBreakdownInConf=LIB!WorkBreakdownElement.allInstances.select(wbe|wbe.isPPEInConfiguration);
	var i=0;
	while((not res) and (i<workBreakdownInConf.size())){
		if(workBreakdownInConf.at(i).predecessor.includes(self)){
			res=true;
		}
		i++;
	}
	return res;
}

operation getWorkBreakdownElementById(id : String):LIB!WorkBreakdownElement{
	return LIB!WorkBreakdownElement.allInstances.selectOne(wbe|wbe.id=id);
}

operation LIB!CapabilityPattern isExtendedCapabilityPattern():Boolean{
	return self.variabilityType<>LIB!VariabilityType#na and self.variabilityType<>LIB!VariabilityType#contributes
		and self.variabilityType<>LIB!VariabilityType#replaces and self.variabilityType<>LIB!VariabilityType#localContribution
		and self.variabilityType<>LIB!VariabilityType#localReplacement and self.variabilityType<>LIB!VariabilityType#extendsReplaces;
}

operation LIB!CapabilityPattern isOriginalCapabilityPattern():Boolean{
	var res=false;
	var processComponent=LIB!ProcessComponent.allInstances.selectOne(pc|pc.process.id=self.id);
	if(processComponent<>null){
		res=true;
	}
	return res;
	
}

operation LIB!Artifact isArtifactInConfiguration():Boolean{
	return LIB!WorkProductDescriptor.allInstances.exists(wpd|wpd.isPPEInConfiguration() and wpd.workProduct=self.id);
}


post{
"End of transformation".println();
}


