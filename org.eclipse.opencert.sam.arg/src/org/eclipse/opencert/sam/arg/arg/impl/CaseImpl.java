/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.CaseImpl#getArgument <em>Argument</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.CaseImpl#getArgumentation <em>Argumentation</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.CaseImpl#getAgreement <em>Agreement</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.CaseImpl#getCited <em>Cited</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.CaseImpl#getInformation <em>Information</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CaseImpl extends CDOObjectImpl implements Case {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ArgumentElement> getArgument() {
		return (EList<ArgumentElement>)eDynamicGet(ArgPackage.CASE__ARGUMENT, ArgPackage.Literals.CASE__ARGUMENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Argumentation> getArgumentation() {
		return (EList<Argumentation>)eDynamicGet(ArgPackage.CASE__ARGUMENTATION, ArgPackage.Literals.CASE__ARGUMENTATION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Agreement> getAgreement() {
		return (EList<Agreement>)eDynamicGet(ArgPackage.CASE__AGREEMENT, ArgPackage.Literals.CASE__AGREEMENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ArgumentElementCitation> getCited() {
		return (EList<ArgumentElementCitation>)eDynamicGet(ArgPackage.CASE__CITED, ArgPackage.Literals.CASE__CITED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<InformationElementCitation> getInformation() {
		return (EList<InformationElementCitation>)eDynamicGet(ArgPackage.CASE__INFORMATION, ArgPackage.Literals.CASE__INFORMATION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArgPackage.CASE__ARGUMENT:
				return ((InternalEList<?>)getArgument()).basicRemove(otherEnd, msgs);
			case ArgPackage.CASE__ARGUMENTATION:
				return ((InternalEList<?>)getArgumentation()).basicRemove(otherEnd, msgs);
			case ArgPackage.CASE__AGREEMENT:
				return ((InternalEList<?>)getAgreement()).basicRemove(otherEnd, msgs);
			case ArgPackage.CASE__CITED:
				return ((InternalEList<?>)getCited()).basicRemove(otherEnd, msgs);
			case ArgPackage.CASE__INFORMATION:
				return ((InternalEList<?>)getInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.CASE__ARGUMENT:
				return getArgument();
			case ArgPackage.CASE__ARGUMENTATION:
				return getArgumentation();
			case ArgPackage.CASE__AGREEMENT:
				return getAgreement();
			case ArgPackage.CASE__CITED:
				return getCited();
			case ArgPackage.CASE__INFORMATION:
				return getInformation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.CASE__ARGUMENT:
				getArgument().clear();
				getArgument().addAll((Collection<? extends ArgumentElement>)newValue);
				return;
			case ArgPackage.CASE__ARGUMENTATION:
				getArgumentation().clear();
				getArgumentation().addAll((Collection<? extends Argumentation>)newValue);
				return;
			case ArgPackage.CASE__AGREEMENT:
				getAgreement().clear();
				getAgreement().addAll((Collection<? extends Agreement>)newValue);
				return;
			case ArgPackage.CASE__CITED:
				getCited().clear();
				getCited().addAll((Collection<? extends ArgumentElementCitation>)newValue);
				return;
			case ArgPackage.CASE__INFORMATION:
				getInformation().clear();
				getInformation().addAll((Collection<? extends InformationElementCitation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.CASE__ARGUMENT:
				getArgument().clear();
				return;
			case ArgPackage.CASE__ARGUMENTATION:
				getArgumentation().clear();
				return;
			case ArgPackage.CASE__AGREEMENT:
				getAgreement().clear();
				return;
			case ArgPackage.CASE__CITED:
				getCited().clear();
				return;
			case ArgPackage.CASE__INFORMATION:
				getInformation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.CASE__ARGUMENT:
				return !getArgument().isEmpty();
			case ArgPackage.CASE__ARGUMENTATION:
				return !getArgumentation().isEmpty();
			case ArgPackage.CASE__AGREEMENT:
				return !getAgreement().isEmpty();
			case ArgPackage.CASE__CITED:
				return !getCited().isEmpty();
			case ArgPackage.CASE__INFORMATION:
				return !getInformation().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CaseImpl
