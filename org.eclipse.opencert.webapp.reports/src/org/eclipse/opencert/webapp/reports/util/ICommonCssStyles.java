/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

public interface ICommonCssStyles
{
    final String COMMON_BUTTON_STYLE = "commonButton";
    
    final String IA_ACTION_BUTTON_STYLE =  "iaActionButton";
    
    final String EXTERNAL_TOOL_COMMON_BUTTON = "externalToolCommonButton";
    
    final String COMPLIANCE_STATUS_COMMON_LABEL = "complianceStatusCommonLabel";
    
    final String OVERALL_STATUS_COMMON_LABEL = "overallStatusCommonLabel";
}
