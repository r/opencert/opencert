/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pam.procspec.process.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetImpl;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;
import org.eclipse.opencert.pam.procspec.process.Technique;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Technique</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.TechniqueImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.TechniqueImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.TechniqueImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.TechniqueImpl#getCreatedArtefact <em>Created Artefact</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TechniqueImpl extends AssuranceAssetImpl implements Technique {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TechniqueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProcessPackage.Literals.TECHNIQUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return (String)eDynamicGet(ProcessPackage.TECHNIQUE__ID, GeneralPackage.Literals.NAMED_ELEMENT__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		eDynamicSet(ProcessPackage.TECHNIQUE__ID, GeneralPackage.Literals.NAMED_ELEMENT__ID, newId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eDynamicGet(ProcessPackage.TECHNIQUE__NAME, GeneralPackage.Literals.NAMED_ELEMENT__NAME, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eDynamicSet(ProcessPackage.TECHNIQUE__NAME, GeneralPackage.Literals.NAMED_ELEMENT__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return (String)eDynamicGet(ProcessPackage.TECHNIQUE__DESCRIPTION, GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		eDynamicSet(ProcessPackage.TECHNIQUE__DESCRIPTION, GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, newDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Artefact> getCreatedArtefact() {
		return (EList<Artefact>)eDynamicGet(ProcessPackage.TECHNIQUE__CREATED_ARTEFACT, ProcessPackage.Literals.TECHNIQUE__CREATED_ARTEFACT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ProcessPackage.TECHNIQUE__ID:
				return getId();
			case ProcessPackage.TECHNIQUE__NAME:
				return getName();
			case ProcessPackage.TECHNIQUE__DESCRIPTION:
				return getDescription();
			case ProcessPackage.TECHNIQUE__CREATED_ARTEFACT:
				return getCreatedArtefact();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ProcessPackage.TECHNIQUE__ID:
				setId((String)newValue);
				return;
			case ProcessPackage.TECHNIQUE__NAME:
				setName((String)newValue);
				return;
			case ProcessPackage.TECHNIQUE__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ProcessPackage.TECHNIQUE__CREATED_ARTEFACT:
				getCreatedArtefact().clear();
				getCreatedArtefact().addAll((Collection<? extends Artefact>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ProcessPackage.TECHNIQUE__ID:
				setId(ID_EDEFAULT);
				return;
			case ProcessPackage.TECHNIQUE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ProcessPackage.TECHNIQUE__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ProcessPackage.TECHNIQUE__CREATED_ARTEFACT:
				getCreatedArtefact().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ProcessPackage.TECHNIQUE__ID:
				return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
			case ProcessPackage.TECHNIQUE__NAME:
				return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
			case ProcessPackage.TECHNIQUE__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? getDescription() != null : !DESCRIPTION_EDEFAULT.equals(getDescription());
			case ProcessPackage.TECHNIQUE__CREATED_ARTEFACT:
				return !getCreatedArtefact().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case ProcessPackage.TECHNIQUE__ID: return GeneralPackage.NAMED_ELEMENT__ID;
				case ProcessPackage.TECHNIQUE__NAME: return GeneralPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (derivedFeatureID) {
				case ProcessPackage.TECHNIQUE__DESCRIPTION: return GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.NAMED_ELEMENT__ID: return ProcessPackage.TECHNIQUE__ID;
				case GeneralPackage.NAMED_ELEMENT__NAME: return ProcessPackage.TECHNIQUE__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION: return ProcessPackage.TECHNIQUE__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //TechniqueImpl
