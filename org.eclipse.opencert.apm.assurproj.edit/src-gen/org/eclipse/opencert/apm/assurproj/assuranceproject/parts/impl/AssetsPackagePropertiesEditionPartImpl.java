/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.providers.AssuranceprojectMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

// End of user code

/**
 * 
 * 
 */
public class AssetsPackagePropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, AssetsPackagePropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Button isActive;
	protected ReferencesTable artefactsModel;
	protected List<ViewerFilter> artefactsModelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> artefactsModelFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable argumentationModel;
	protected List<ViewerFilter> argumentationModelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> argumentationModelFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable processModel;
	protected List<ViewerFilter> processModelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> processModelFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public AssetsPackagePropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence assetsPackageStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = assetsPackageStep.addStep(AssuranceprojectViewsRepository.AssetsPackage.Properties.class);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssetsPackage.Properties.id);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssetsPackage.Properties.name);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssetsPackage.Properties.description);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel);
		
		
		composer = new PartComposer(assetsPackageStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceprojectViewsRepository.AssetsPackage.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssetsPackage.Properties.id) {
					return createIdText(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssetsPackage.Properties.name) {
					return createNameText(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssetsPackage.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive) {
					return createIsActiveCheckbox(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel) {
					return createArtefactsModelAdvancedReferencesTable(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel) {
					return createArgumentationModelAdvancedReferencesTable(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel) {
					return createProcessModelAdvancedReferencesTable(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(AssuranceprojectMessages.AssetsPackagePropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssetsPackage.Properties.id, AssuranceprojectMessages.AssetsPackagePropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, AssuranceprojectViewsRepository.AssetsPackage.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssetsPackage.Properties.id, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssetsPackage.Properties.name, AssuranceprojectMessages.AssetsPackagePropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, AssuranceprojectViewsRepository.AssetsPackage.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssetsPackage.Properties.name, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssetsPackage.Properties.description, AssuranceprojectMessages.AssetsPackagePropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, AssuranceprojectViewsRepository.AssetsPackage.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssetsPackage.Properties.description, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createIsActiveCheckbox(Composite parent) {
		isActive = new Button(parent, SWT.CHECK);
		isActive.setText(getDescription(AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive, AssuranceprojectMessages.AssetsPackagePropertiesEditionPart_IsActiveLabel));
		isActive.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 *
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 	
			 */
			public void widgetSelected(SelectionEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, new Boolean(isActive.getSelection())));
			}

		});
		GridData isActiveData = new GridData(GridData.FILL_HORIZONTAL);
		isActiveData.horizontalSpan = 2;
		isActive.setLayoutData(isActiveData);
		EditingUtils.setID(isActive, AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive);
		EditingUtils.setEEFtype(isActive, "eef::Checkbox"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIsActiveCheckbox

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createArtefactsModelAdvancedReferencesTable(Composite parent) {
		String label = getDescription(AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel, AssuranceprojectMessages.AssetsPackagePropertiesEditionPart_ArtefactsModelLabel);		 
		this.artefactsModel = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addArtefactsModel(); }
			public void handleEdit(EObject element) { editArtefactsModel(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveArtefactsModel(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromArtefactsModel(element); }
			public void navigateTo(EObject element) { }
		});
		this.artefactsModel.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel, AssuranceprojectViewsRepository.SWT_KIND));
		this.artefactsModel.createControls(parent);
		this.artefactsModel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData artefactsModelData = new GridData(GridData.FILL_HORIZONTAL);
		artefactsModelData.horizontalSpan = 3;
		this.artefactsModel.setLayoutData(artefactsModelData);
		this.artefactsModel.disableMove();
		artefactsModel.setID(AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel);
		artefactsModel.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addArtefactsModel() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(artefactsModel.getInput(), artefactsModelFilters, artefactsModelBusinessFilters,
		"artefactsModel", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				artefactsModel.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveArtefactsModel(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		artefactsModel.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromArtefactsModel(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		artefactsModel.refresh();
	}

	/**
	 * 
	 */
	protected void editArtefactsModel(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				artefactsModel.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createArgumentationModelAdvancedReferencesTable(Composite parent) {
		String label = getDescription(AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel, AssuranceprojectMessages.AssetsPackagePropertiesEditionPart_ArgumentationModelLabel);		 
		this.argumentationModel = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addArgumentationModel(); }
			public void handleEdit(EObject element) { editArgumentationModel(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveArgumentationModel(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromArgumentationModel(element); }
			public void navigateTo(EObject element) { }
		});
		this.argumentationModel.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel, AssuranceprojectViewsRepository.SWT_KIND));
		this.argumentationModel.createControls(parent);
		this.argumentationModel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData argumentationModelData = new GridData(GridData.FILL_HORIZONTAL);
		argumentationModelData.horizontalSpan = 3;
		this.argumentationModel.setLayoutData(argumentationModelData);
		this.argumentationModel.disableMove();
		argumentationModel.setID(AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel);
		argumentationModel.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addArgumentationModel() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(argumentationModel.getInput(), argumentationModelFilters, argumentationModelBusinessFilters,
		"argumentationModel", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				argumentationModel.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveArgumentationModel(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		argumentationModel.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromArgumentationModel(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		argumentationModel.refresh();
	}

	/**
	 * 
	 */
	protected void editArgumentationModel(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				argumentationModel.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createProcessModelAdvancedReferencesTable(Composite parent) {
		String label = getDescription(AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel, AssuranceprojectMessages.AssetsPackagePropertiesEditionPart_ProcessModelLabel);		 
		this.processModel = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addProcessModel(); }
			public void handleEdit(EObject element) { editProcessModel(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveProcessModel(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromProcessModel(element); }
			public void navigateTo(EObject element) { }
		});
		this.processModel.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel, AssuranceprojectViewsRepository.SWT_KIND));
		this.processModel.createControls(parent);
		this.processModel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData processModelData = new GridData(GridData.FILL_HORIZONTAL);
		processModelData.horizontalSpan = 3;
		this.processModel.setLayoutData(processModelData);
		this.processModel.disableMove();
		processModel.setID(AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel);
		processModel.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addProcessModel() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(processModel.getInput(), processModelFilters, processModelBusinessFilters,
		"processModel", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				processModel.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveProcessModel(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		processModel.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromProcessModel(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssetsPackagePropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		processModel.refresh();
	}

	/**
	 * 
	 */
	protected void editProcessModel(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				processModel.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssetsPackage.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(AssuranceprojectMessages.AssetsPackage_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssetsPackage.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(AssuranceprojectMessages.AssetsPackage_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssetsPackage.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(AssuranceprojectMessages.AssetsPackage_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#getIsActive()
	 * 
	 */
	public Boolean getIsActive() {
		return Boolean.valueOf(isActive.getSelection());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#setIsActive(Boolean newValue)
	 * 
	 */
	public void setIsActive(Boolean newValue) {
		if (newValue != null) {
			isActive.setSelection(newValue.booleanValue());
		} else {
			isActive.setSelection(false);
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssetsPackage.Properties.isActive);
		if (eefElementEditorReadOnlyState && isActive.isEnabled()) {
			isActive.setEnabled(false);
			isActive.setToolTipText(AssuranceprojectMessages.AssetsPackage_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !isActive.isEnabled()) {
			isActive.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#initArtefactsModel(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initArtefactsModel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		artefactsModel.setContentProvider(contentProvider);
		artefactsModel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssetsPackage.Properties.artefactsModel);
		if (eefElementEditorReadOnlyState && artefactsModel.getTable().isEnabled()) {
			artefactsModel.setEnabled(false);
			artefactsModel.setToolTipText(AssuranceprojectMessages.AssetsPackage_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !artefactsModel.getTable().isEnabled()) {
			artefactsModel.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#updateArtefactsModel()
	 * 
	 */
	public void updateArtefactsModel() {
	artefactsModel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#addFilterArtefactsModel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArtefactsModel(ViewerFilter filter) {
		artefactsModelFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#addBusinessFilterArtefactsModel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArtefactsModel(ViewerFilter filter) {
		artefactsModelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#isContainedInArtefactsModelTable(EObject element)
	 * 
	 */
	public boolean isContainedInArtefactsModelTable(EObject element) {
		return ((ReferencesTableSettings)artefactsModel.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#initArgumentationModel(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initArgumentationModel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		argumentationModel.setContentProvider(contentProvider);
		argumentationModel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssetsPackage.Properties.argumentationModel);
		if (eefElementEditorReadOnlyState && argumentationModel.getTable().isEnabled()) {
			argumentationModel.setEnabled(false);
			argumentationModel.setToolTipText(AssuranceprojectMessages.AssetsPackage_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !argumentationModel.getTable().isEnabled()) {
			argumentationModel.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#updateArgumentationModel()
	 * 
	 */
	public void updateArgumentationModel() {
	argumentationModel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#addFilterArgumentationModel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArgumentationModel(ViewerFilter filter) {
		argumentationModelFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#addBusinessFilterArgumentationModel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArgumentationModel(ViewerFilter filter) {
		argumentationModelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#isContainedInArgumentationModelTable(EObject element)
	 * 
	 */
	public boolean isContainedInArgumentationModelTable(EObject element) {
		return ((ReferencesTableSettings)argumentationModel.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#initProcessModel(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initProcessModel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		processModel.setContentProvider(contentProvider);
		processModel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssetsPackage.Properties.processModel);
		if (eefElementEditorReadOnlyState && processModel.getTable().isEnabled()) {
			processModel.setEnabled(false);
			processModel.setToolTipText(AssuranceprojectMessages.AssetsPackage_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !processModel.getTable().isEnabled()) {
			processModel.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#updateProcessModel()
	 * 
	 */
	public void updateProcessModel() {
	processModel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#addFilterProcessModel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToProcessModel(ViewerFilter filter) {
		processModelFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#addBusinessFilterProcessModel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToProcessModel(ViewerFilter filter) {
		processModelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssetsPackagePropertiesEditionPart#isContainedInProcessModelTable(EObject element)
	 * 
	 */
	public boolean isContainedInProcessModelTable(EObject element) {
		return ((ReferencesTableSettings)processModel.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceprojectMessages.AssetsPackage_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
