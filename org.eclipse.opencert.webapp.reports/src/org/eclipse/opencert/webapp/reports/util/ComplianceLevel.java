/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;


public enum ComplianceLevel {
    FULL("Fully Compliant"),
    PARTIAL("Partially Compliant"),
    NO_MAP("Not Compliant (counter evidence)");
 
    private String value;
    
    private static final ComplianceLevel[] VALUES_ARRAY = new ComplianceLevel[]
            {
            FULL, PARTIAL, NO_MAP,
            };


    private ComplianceLevel(String value) 
    {
        this.value = value;
    }
    
    public String getValue() 
    {
        return value;
    }
    
    
    public static ComplianceLevel getTypeByValue(String value) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ComplianceLevel type = VALUES_ARRAY[i];
            if (type.getValue().equals(value)) {
                return type;
            }
        }
        return null;
    }
}
