/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.opencert.vocabulary.VocabularyPackage;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategorySubCategoriesEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryTermsEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermDefinedByEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermHasAEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermIsAEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermRefersToEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.VocabularyEditPart;
import org.eclipse.opencert.vocabulary.diagram.part.VocabularyDiagramEditorPlugin;

/**
 * @generated
 */
public class VocabularyElementTypes {

	/**
	 * @generated
	 */
	private VocabularyElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			VocabularyDiagramEditorPlugin.getInstance()
					.getItemProvidersAdapterFactory());

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType Vocabulary_1000 = getElementType("org.eclipse.opencert.vocabulary.diagram.Vocabulary_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Term_2001 = getElementType("org.eclipse.opencert.vocabulary.diagram.Term_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Category_2002 = getElementType("org.eclipse.opencert.vocabulary.diagram.Category_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType SourceOfDefinition_2003 = getElementType("org.eclipse.opencert.vocabulary.diagram.SourceOfDefinition_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType CategoryTerms_4001 = getElementType("org.eclipse.opencert.vocabulary.diagram.CategoryTerms_4001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType CategorySubCategories_4002 = getElementType("org.eclipse.opencert.vocabulary.diagram.CategorySubCategories_4002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType TermDefinedBy_4003 = getElementType("org.eclipse.opencert.vocabulary.diagram.TermDefinedBy_4003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType TermIsA_4004 = getElementType("org.eclipse.opencert.vocabulary.diagram.TermIsA_4004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType TermHasA_4005 = getElementType("org.eclipse.opencert.vocabulary.diagram.TermHasA_4005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType TermRefersTo_4006 = getElementType("org.eclipse.opencert.vocabulary.diagram.TermRefersTo_4006"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(Vocabulary_1000,
					VocabularyPackage.eINSTANCE.getVocabulary());

			elements.put(Term_2001, VocabularyPackage.eINSTANCE.getTerm());

			elements.put(Category_2002,
					VocabularyPackage.eINSTANCE.getCategory());

			elements.put(SourceOfDefinition_2003,
					VocabularyPackage.eINSTANCE.getSourceOfDefinition());

			elements.put(CategoryTerms_4001,
					VocabularyPackage.eINSTANCE.getCategory_Terms());

			elements.put(CategorySubCategories_4002,
					VocabularyPackage.eINSTANCE.getCategory_SubCategories());

			elements.put(TermDefinedBy_4003,
					VocabularyPackage.eINSTANCE.getTerm_DefinedBy());

			elements.put(TermIsA_4004,
					VocabularyPackage.eINSTANCE.getTerm_IsA());

			elements.put(TermHasA_4005,
					VocabularyPackage.eINSTANCE.getTerm_HasA());

			elements.put(TermRefersTo_4006,
					VocabularyPackage.eINSTANCE.getTerm_RefersTo());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(Vocabulary_1000);
			KNOWN_ELEMENT_TYPES.add(Term_2001);
			KNOWN_ELEMENT_TYPES.add(Category_2002);
			KNOWN_ELEMENT_TYPES.add(SourceOfDefinition_2003);
			KNOWN_ELEMENT_TYPES.add(CategoryTerms_4001);
			KNOWN_ELEMENT_TYPES.add(CategorySubCategories_4002);
			KNOWN_ELEMENT_TYPES.add(TermDefinedBy_4003);
			KNOWN_ELEMENT_TYPES.add(TermIsA_4004);
			KNOWN_ELEMENT_TYPES.add(TermHasA_4005);
			KNOWN_ELEMENT_TYPES.add(TermRefersTo_4006);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case VocabularyEditPart.VISUAL_ID:
			return Vocabulary_1000;
		case TermEditPart.VISUAL_ID:
			return Term_2001;
		case CategoryEditPart.VISUAL_ID:
			return Category_2002;
		case SourceOfDefinitionEditPart.VISUAL_ID:
			return SourceOfDefinition_2003;
		case CategoryTermsEditPart.VISUAL_ID:
			return CategoryTerms_4001;
		case CategorySubCategoriesEditPart.VISUAL_ID:
			return CategorySubCategories_4002;
		case TermDefinedByEditPart.VISUAL_ID:
			return TermDefinedBy_4003;
		case TermIsAEditPart.VISUAL_ID:
			return TermIsA_4004;
		case TermHasAEditPart.VISUAL_ID:
			return TermHasA_4005;
		case TermRefersToEditPart.VISUAL_ID:
			return TermRefersTo_4006;
		}
		return null;
	}

	/**
	 * @generated
	 */
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(
			elementTypeImages) {

		/**
		 * @generated
		 */
		@Override
		public boolean isKnownElementType(IElementType elementType) {
			return org.eclipse.opencert.vocabulary.diagram.providers.VocabularyElementTypes
					.isKnownElementType(elementType);
		}

		/**
		 * @generated
		 */
		@Override
		public IElementType getElementTypeForVisualId(int visualID) {
			return org.eclipse.opencert.vocabulary.diagram.providers.VocabularyElementTypes
					.getElementType(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public ENamedElement getDefiningNamedElement(
				IAdaptable elementTypeAdapter) {
			return org.eclipse.opencert.vocabulary.diagram.providers.VocabularyElementTypes
					.getElement(elementTypeAdapter);
		}
	};

}
