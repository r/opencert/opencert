/*******************************************************************************
 * Copyright (c) 2017 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
  * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.diagram.DnD;


import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;


/**
 * @generated NOT
 */

/**
 * The Class AddEObjectReferencesToArgumentModel.
 * 
 * @author Alejandra Ruiz</a>
 */
public class AddEObjectReferencesToArgumentModel extends AbstractTransactionalCommand {
//	/** EAnnotation Source for elements that belong to this <Diagram> */
//	public static final String BelongToDiagramSource = "es.cv.gvcase.mdt.uml2.diagram.common.Belongs_To_This_Diagram";
	
//	// Start MCP
//    private DiagramImpl diagramImpl = null;
//	// End MCP
//	
//	/** The diagram. */
//	private Diagram diagram = null;
	
	/** The argument Model	 */
	private Case case1=null;
	/** The e objects. */
	private List<EObject> eObjects = null;

	/**
	 * Flag to add the EObject's contents.
	 */
	protected boolean addContents = true;

	/**
	 * Instantiates a new adds the e object references to diagram.
	 * 
	 * @param domain
	 *        the domain
	 * @param diagram
	 *        the diagram
	 * @param eObjects
	 *        the e objects
	 */
	public AddEObjectReferencesToArgumentModel(TransactionalEditingDomain domain, Case case1, List<EObject> eObjects) {
		super(domain, "Add EObject references to Argument Model", null);
		this.case1=case1;
		this.eObjects = eObjects;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.operations.AbstractOperation#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return case1 != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gmf.runtime.emf.commands.core.command. AbstractTransactionalCommand
	 * #doExecuteWithResult(org.eclipse.core.runtime.IProgressMonitor,
	 * org.eclipse.core.runtime.IAdaptable)
	 */
	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		for(EObject eObject : eObjects) {
			addReferences(eObject);
			if(eObject instanceof ArgumentElement)
				case1.getArgument().add((ArgumentElement)eObject);
			else if(eObject instanceof Argumentation)
				case1.getArgumentation().add((Argumentation)eObject);
			else if(eObject instanceof Agreement)			
				case1.getAgreement().add((Agreement)eObject);
			else if(eObject instanceof ArgumentElementCitation)			
				case1.getCited().add((ArgumentElementCitation) eObject);
			else if(eObject instanceof InformationElementCitation)			
				case1.getInformation().add((InformationElementCitation)eObject);
			// End MCP
		}
		return CommandResult.newOKCommandResult();
	}

	/**
	 * Adds the references.
	 * 
	 * @param eObject
	 *        the e object
	 */
	private void addReferences(EObject eObject) {
		if(addContents) {
			for(EObject e : eObject.eContents()) {
				addReferences(e);
			}
		}
		//MultiDiagramUtil.AddEAnnotationReferenceToDiagram(diagram, eObject);
//		AddEAnnotationReferenceToDiagram(diagram, eObject);
	}
	
	/**
	 * Adds the e annotation reference to diagram.
	 * 
	 * @param diagram
	 *            the diagram
	 * @param eObject
	 *            the e object
	 * 
	 * @return true, if successful
	 */
//	public static boolean AddEAnnotationReferenceToDiagram(Diagram diagram,
//			EObject eObject) {
//
//		if (diagram != null) {
//			EAnnotation eAnnotation = diagram.getEAnnotation(BelongToDiagramSource);
//			if (eAnnotation == null) {
//				eAnnotation = EcoreFactory.eINSTANCE.createEAnnotation();
//				eAnnotation.setSource(BelongToDiagramSource);
//				diagram.getEAnnotations().add(eAnnotation);
//			}
//			// if (eAnnotation.getReferences().contains(eObject) == false) {
//			eAnnotation.getReferences().add(eObject);
//			// }
//			return true;
//		}
//		return false;
//	}


}
