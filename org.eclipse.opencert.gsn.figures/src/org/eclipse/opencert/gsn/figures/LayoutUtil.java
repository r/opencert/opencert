/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation and KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza(Tecnalia) - initial API and implementation
 *   Alejandra Ru�z(Tecnalia) - initial API and implementation
 *   Idoya Del R�o(Tecnalia) - initial API and implementation
 *   Mari Carmen Palacios(Tecnalia) - initial API and implementation
 *   Angel L�pez(Tecnalia) - initial API and implementation
 *   Jan Mauersberger(KPIT)- LayoutUtil implementation
 *   Sascha Baumgart(KPIT)- LayoutUtil implementation
 *******************************************************************************/

package org.eclipse.opencert.gsn.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;

public class LayoutUtil {

	public static void moveToCenterAndUp(WrappingLabel label, Figure parent,
			Graphics graphics) {

		Rectangle r = parent.getBounds();
		Rectangle newLabelBounds = new Rectangle();
		Point middle;
		if (parent instanceof GSNArgumentModule
				|| parent instanceof GSNAssumption
				|| parent instanceof GSNJustification
				|| parent instanceof GSNSolution
				|| parent instanceof GSNAwaySolution
				|| parent instanceof GSNContract) {
			middle = new Point(r.x + r.width / 6, r.y + r.height / 6);
		} else
			middle = new Point(r.x + r.width / 6, r.y);

		newLabelBounds.x = middle.x;
		newLabelBounds.y = middle.y;
		newLabelBounds.width = r.width;
		newLabelBounds.height = r.height;
		label.setBounds(newLabelBounds);
		label.setTextWrap(true);
		label.setMaximumSize(new Dimension(2 * r.width / 3, r.height / 8));
		label.setAlignment(PositionConstants.TOP);

	}

	public static void moveToCenterAndDown(WrappingLabel label, Figure parent,
			Graphics graphics) {
		Rectangle r = parent.getBounds();
		Rectangle newLabelBounds = new Rectangle();
		Point middle;

		if (parent instanceof GSNGoal || parent instanceof GSNContext
				|| parent instanceof GSNStrategy
				|| parent instanceof GSNAwayContext
				|| parent instanceof GSNAwayGoal)
			middle = new Point(r.x + r.width / 6, r.y + r.height / 3);
		else
			middle = new Point(r.x + r.width / 6, r.y + r.height / 2);

		newLabelBounds.x = middle.x;
		newLabelBounds.y = middle.y;

		// Sascha
		// The width and height of the label needs to be smaller than the
		// surrounding figure
		newLabelBounds.width = r.width * 2 / 3;
		if (parent instanceof GSNGoal) {
			int shape = ((GSNGoal) parent).getShape();

			switch (shape) {
			case 1:
			case 2:
			case 3:
			case 10:
			case 11:
			case 12:
			case 13:
				newLabelBounds.height = r.height * 2 / 3 * 4 / 8;
				break;
		
			case 0:
				newLabelBounds.height = r.height * 2 / 3 - 5;
				break;
			
			default:
				throw new IllegalArgumentException();
			}
		} else if (parent instanceof GSNContext
				|| parent instanceof GSNStrategy
				|| parent instanceof GSNAwayContext
				|| parent instanceof GSNAwayGoal)
			newLabelBounds.height = r.height * 2 / 3 - 5;
		else if (parent instanceof GSNSolution)
			newLabelBounds.height = r.height * 1 / 4;
		else
			newLabelBounds.height = r.height / 2 - 5;
		// Sascha

		label.setBounds(newLabelBounds);
		label.setTextWrap(true);
		label.setAlignment(PositionConstants.TOP);
		if (parent instanceof GSNAssumption) {
			if (((GSNAssumption) parent).getShape() != 0)
				label.setMaximumSize(new Dimension(2 * r.width / 3,
						r.height / 4));
			else
				label.setMaximumSize(new Dimension(2 * r.width / 3,
						3 * r.height / 7));

		} else if (parent instanceof GSNSolution) {
			if (((GSNSolution) parent).getShape() != 0)
				label.setMaximumSize(new Dimension(2 * r.width / 3,
						r.height / 4));
			else
				label.setMaximumSize(new Dimension(2 * r.width / 3,
						3 * r.height / 7));

		}

		else if (parent instanceof GSNJustification) {
			if (((GSNJustification) parent).getShape() != 0)
				label.setMaximumSize(new Dimension(2 * r.width / 3,
						r.height / 4));
			else
				label.setMaximumSize(new Dimension(2 * r.width / 3,
						3 * r.height / 7));

		} else if (parent instanceof GSNContract
				|| parent instanceof GSNAwaySolution)
			label.setMaximumSize(new Dimension(2 * r.width / 3, r.height / 4));

		else
			label.setMaximumSize(new Dimension(2 * r.width / 3,
					3 * r.height / 7));
	}

	public static void moveToCenterAndBottom(WrappingLabel label,
			Figure parent, Graphics graphics) {
		Rectangle r = parent.getBounds();
		Rectangle newLabelBounds = new Rectangle();
		Point middle = new Point(r.x + r.width / 2, r.y + 3 * r.height / 4);

		int textWidth = (label.getText().length() - 3)
				* graphics.getFontMetrics().getAverageCharWidth();
		int textHeight = graphics.getFontMetrics().getHeight();

		newLabelBounds.x = middle.x - r.width / 5;
		newLabelBounds.y = middle.y;
		newLabelBounds.width = textWidth;
		newLabelBounds.height = textHeight;

		label.setBounds(newLabelBounds);
		label.setTextWrap(true);
		label.setMaximumSize(new Dimension(r.width / 2, r.height / 4));
		label.setAlignment(PositionConstants.TOP);

	}

}
