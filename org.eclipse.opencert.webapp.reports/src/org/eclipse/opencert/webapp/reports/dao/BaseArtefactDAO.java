/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.dao;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.opencert.webapp.reports.util.IdNameDesc;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.view.administration.ProjectAdministrationPage;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.StandaloneCDOAccessor;
import org.eclipse.opencert.storage.cdo.executors.HttpRequestDrivenInTransactionExecutor;
import org.springframework.stereotype.Component;

@Component
public class BaseArtefactDAO
{
    public BaseArtefact getBaseArtefact(final long baseArtefactUid)
    {
        final List<BaseArtefact> result = new ArrayList<BaseArtefact>();

        new HttpRequestDrivenInTransactionExecutor() {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                String baseArtefactSQL = createBaseArtefactSelect(baseArtefactUid);
                CDOQuery baseArtefactQuery = cdoTransaction.createQuery("sql", baseArtefactSQL);

                result.addAll(baseArtefactQuery.getResult(BaseArtefact.class));
            }

        }.executeReadOnlyOperation();

        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    public void updateBaseArtefact(IdNameDesc chArt)
    {        
        final List<BaseArtefact> baResult = new LinkedList<>();
        
        new HttpRequestDrivenInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                String baseArtefactSQL = createBaseArtefactSelect(chArt.id);
                CDOQuery baseArtefactQuery = cdoTransaction.createQuery("sql", baseArtefactSQL);

                BaseArtefact baseArtefact = baseArtefactQuery.getResultValue(BaseArtefact.class);
                
                if (baseArtefact == null) {
                    OpencertLogger.error("No baseArtefact id: " + chArt.id);
                    return;
                }
                baseArtefact.setName(chArt.name);
                baseArtefact.setDescription(chArt.desc);
                baResult.add(baseArtefact);
            }
        }.executeReadWriteOperation();
        
        if (chArt.relatedIDs != null) 
        {
            final BaseArtefact baseArtefact = baResult.stream().findFirst().get();
            updateAffectedBaseArtefacts(baseArtefact, chArt.relatedIDs, ProjectAdministrationPage.THE_EDITOR_SUPPORTED_IA_EFFECT);
        }
    }

    public void updateAffectedBaseArtefacts(BaseArtefact theChangedArtefact, List<Long> relatedIDsFromGUI, ChangeEffectKind changeEffectKind)
    {
        final long theChangedArtefactID = CDOStorageUtil.getCDOId(theChangedArtefact);
        List<BaseArtefact> currentAffectedArtefactsFromDb = getAfftectedBaseArtefacts(theChangedArtefactID, changeEffectKind);
        List<Long> currentAffectedIDsFromDb = currentAffectedArtefactsFromDb.stream().map(e -> CDOStorageUtil.getCDOId(e)).collect(Collectors.toList());
        List<Long> addedAffected = relatedIDsFromGUI.stream().filter(e -> !currentAffectedIDsFromDb.contains(e)).collect(Collectors.toList());
        
        if (addedAffected.size() > 0) {
            addAffectedBaseArtefactRelations(theChangedArtefact, addedAffected, ProjectAdministrationPage.THE_EDITOR_SUPPORTED_IA_EFFECT);
        }
        
        List<Long> removedAffected = currentAffectedIDsFromDb.stream().filter(e -> !relatedIDsFromGUI.contains(e)).collect(Collectors.toList());
        if (removedAffected.size() > 0) {
            deleteAffectedBaseArtefactRelations(theChangedArtefactID, removedAffected, ProjectAdministrationPage.THE_EDITOR_SUPPORTED_IA_EFFECT);
        }
    }

    private void deleteAffectedBaseArtefactRelations(long id, List<Long> affectedToRemove, ChangeEffectKind changeEffectKind)
    {
        for (final long affectedToRemoveID : affectedToRemove)
        {
            final BaseArtefactRel baseArtefactRel = getBaseArtefactRelation(id, affectedToRemoveID, changeEffectKind);
            if (baseArtefactRel == null) {
                throw new IllegalStateException(String.format("Cannot find base artefact rel for targetId: %d and source: %d", id, affectedToRemove));
            }
            
            // remove the relation
            new HttpRequestDrivenInTransactionExecutor() {
                @Override
                public void executeInTransaction(CDOTransaction cdoTransaction)
                {
                    String baseArtefactOwnerSQL = "select cdo_container from baseline_baseartefactrel where cdo_id = '" 
                            + CDOStorageUtil.getCDOId(baseArtefactRel) + "'"
                            + CDOStorageUtil.getMandatoryCDOQuerySuffix();
                    CDOQuery query = cdoTransaction.createQuery("sql", baseArtefactOwnerSQL);
                    BaseArtefact baseArtefact = query.getResultValue(BaseArtefact.class);
                    EList<BaseArtefactRel> ownedRel = baseArtefact.getOwnedRel();
                    if (ownedRel == null) {
                        throw new IllegalStateException();
                    }
                    BaseArtefactRel cdoBaseArtefactRel = (BaseArtefactRel)cdoTransaction.getObject(CDOUtil.getCDOObject(baseArtefactRel));
                    boolean bRemoved = ownedRel.remove(cdoBaseArtefactRel);
                    if (!bRemoved) {
                        OpencertLogger.warn("Illegal state cannot remove baseArtefactRel: " + cdoBaseArtefactRel);
                    }
                    
                    if (baseArtefact.eResource() != null) {
                        baseArtefact.eResource().setModified(true);
                    }
                    
                    // another way of data deletion in CDO:
//                    EObject eContainer = baseArtefact.eContainer();
//                    if (eContainer != null) {
//                        eContainer.eContents().remove(baseArtefactRel)
//                    } else {
//                        baseArtefact.eResource().getContents().remove(baseArtefactRel);
//                    }
                }
            }.executeReadWriteOperation();
        }
    }

    private void addAffectedBaseArtefactRelations(final BaseArtefact theChangedArtefact, List<Long> addedAffected, ChangeEffectKind changeEffectKind)
    {
        for (long addedAffectedID : addedAffected)
        {
            final BaseArtefact affectedArtefact = getBaseArtefact(addedAffectedID);
            
            new HttpRequestDrivenInTransactionExecutor()
            {
                @Override
                public void executeInTransaction(CDOTransaction cdoTransaction)
                {
                    //IMPORTANT! CDO update will only work if we operate on CDO objects for *this* transaction, thus we need the below:
                    final BaseArtefact cdoTheChangedArtefact = (BaseArtefact)cdoTransaction.getObject(CDOUtil.getCDOObject(theChangedArtefact));                    
                    final BaseArtefact cdoAffectedArtefact = (BaseArtefact)cdoTransaction.getObject(CDOUtil.getCDOObject(affectedArtefact));

                    BaseArtefactRel baseArtefactRel = BaselineFactory.eINSTANCE.createBaseArtefactRel();
                    baseArtefactRel.setMaxMultiplicitySource(0);
                    baseArtefactRel.setMaxMultiplicityTarget(0);
                    baseArtefactRel.setMinMultiplicitySource(0);
                    baseArtefactRel.setMinMultiplicityTarget(0);
                    baseArtefactRel.setDescription("Added by OPENCERT GUI");
                    baseArtefactRel.setName("Added by OPENCERT GUI");
                    baseArtefactRel.setRevocationEffect(ProjectAdministrationPage.THE_EDITOR_SUPPORTED_IA_EFFECT);
                    baseArtefactRel.setModificationEffect(ProjectAdministrationPage.THE_EDITOR_SUPPORTED_IA_EFFECT);
                    baseArtefactRel.setTarget(cdoTheChangedArtefact);
                    baseArtefactRel.setSource(cdoAffectedArtefact);
                    
                    final Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                    resource.getContents().add(baseArtefactRel);
                    
                    // insert the relation into owner artefact
                    cdoTheChangedArtefact.getOwnedRel().add(baseArtefactRel);
                    resource.setModified(true);
                }
            }.executeReadWriteOperation();
        }
    }

    public List<BaseArtefact> getAfftectedBaseArtefacts(final long baseArtefactID, ChangeEffectKind changeEffectKind)
    {
        final List<BaseArtefact> result = new ArrayList<BaseArtefact>();

        new HttpRequestDrivenInTransactionExecutor() {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                CDOQuery query = cdoTransaction.createQuery("sql", "select * from baseline_baseartefactrel where"
                        + " target = " + baseArtefactID
                        + " and modificationeffect = " + changeEffectKind.getValue()
                        + CDOStorageUtil.getMandatoryCDOQuerySuffix());
                
                List<BaseArtefactRel> affectedBaseArtefactRelations = query.getResult(BaseArtefactRel.class);
                
                for (BaseArtefactRel baseArtefactRel : affectedBaseArtefactRelations) 
                {
                    BaseArtefact source = baseArtefactRel.getSource();
                    if (source != null) 
                    {
                        // ensure that the BaseArtefact is not broken (because of the target/source BaseArtefact being deleted)
                        try {
                            source.getName();
                            result.add(source);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        }.executeReadOnlyOperation();
        
        return result;
    }
    
    public BaseArtefactRel getBaseArtefactRelation(final long targetID, final long sourceID, ChangeEffectKind changeEffectKind)
    {
        List<BaseArtefactRel> result = new LinkedList<>();

        new HttpRequestDrivenInTransactionExecutor() 
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                CDOQuery query = cdoTransaction.createQuery("sql", "select * from baseline_baseartefactrel where"
                        + " target = " + targetID
                        + " and source = " + sourceID
                        + " and modificationeffect = " + changeEffectKind.getValue()
                        + CDOStorageUtil.getMandatoryCDOQuerySuffix());
                
                List<BaseArtefactRel> baseArtefactRelations = query.getResult(BaseArtefactRel.class);
                result.addAll(baseArtefactRelations);
            }
        }.executeReadOnlyOperation();

        return result.stream().findFirst().get();
    }
    
    private String createBaseArtefactSelect(final long baseArtefactUid)
    {
        return "select * from baseline_baseartefact where cdo_id = '" + baseArtefactUid + "'"
                + CDOStorageUtil.getMandatoryCDOQuerySuffix();
    }

    public void deleteBaseArtefactRelsForBaseArtefact(Long baseArtefactID)
    {
        new HttpRequestDrivenInTransactionExecutor() 
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                CDOQuery query = cdoTransaction.createQuery("sql", "select * from baseline_baseartefactrel where"
                        + " (target = " + baseArtefactID
                        + " or source = " + baseArtefactID
                        + ") "
                        + CDOStorageUtil.getMandatoryCDOQuerySuffix());
                
                List<BaseArtefactRel> baseArtefactRelations = query.getResult(BaseArtefactRel.class);
                
                for (BaseArtefactRel baseArtefactRel : baseArtefactRelations)
                {
                    BaseArtefactRel cdoBaseArtefactRel = (BaseArtefactRel)cdoTransaction.getObject(CDOUtil.getCDOObject(baseArtefactRel));
                    EObject eContainer = baseArtefactRel.eContainer();
                    if (!(eContainer instanceof BaseArtefact)) {
                        OpencertLogger.warn("Illegal state!");
                        continue;
                    }
                    EList<BaseArtefactRel> ownedRels = ((BaseArtefact)eContainer).getOwnedRel();
                    if (ownedRels == null) {
                        throw new IllegalStateException();
                    }
                    boolean bRemoved = ownedRels.remove(cdoBaseArtefactRel);
                    if (!bRemoved) {
                        OpencertLogger.warn("Illegal state cannot remove baseArtefactRel: " + cdoBaseArtefactRel);
                    }
                }
            }
        }.executeReadWriteOperation();
    }
}
