/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.exttools.connectors;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.opencert.webapp.reports.exttools.processors.util.CountProperty;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ParasoftDTPSAConnector
        extends AbstractParasoftDTPFilterConnector
{
    private static final String SEVERITY_KEY = "Severity";

    @Override
    public List<String> getSettingKeys()
    {
        List<String> result = super.getSettingKeys();
        result.add(SEVERITY_KEY);
        
        return result;
    }
    
    @Override
    public Map<String, String> getSettingDefaultValues()
    {
        Map <String, String> result = super.getSettingDefaultValues();
        result.put(SEVERITY_KEY, "1");
        
        return result;
    }
    
    @Override
    public String getName()
    {
        return "Parasoft DTP SA Connector";
    }

    @Override
    public String getDescription()
    {
        return "Connects to Parasoft DTP server and retrieves a number of Static Analysis violations "
                + "reported for the specific DTP project filter. "
                + "Additional parameters, like Severity, can be specified. "
                + "Note: it uses DTP REST API /staticAnalysisViolations";
    }

    @Override
    public String getUrlSuffix()
    {
        final String result = "/grs/api/v1/staticAnalysisViolations/count?showFixed=false&showSuppressed=false&filterId="
                + getSettingsValue(FILTER_KEY)
                + "&analysisType=FA&analysisType=CS"
                + "&severity="
                + getSettingsValue(SEVERITY_KEY);
        return result;
    }

    @Override
    public int processHttpResponse(String httpResponse) throws IOException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        CountProperty cc = objectMapper.readValue(httpResponse, CountProperty.class);
        
        return cc.getCount();
    }
}
