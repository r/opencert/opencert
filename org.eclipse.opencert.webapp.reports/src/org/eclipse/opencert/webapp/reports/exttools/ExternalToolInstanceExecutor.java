/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.exttools;

import java.io.IOException;

import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.externaltools.api.ExternalToolQuery;
import org.eclipse.opencert.externaltools.api.ExternalToolQueryResult;
import org.eclipse.opencert.externaltools.api.IExternalToolConnector;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQueryDAO;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQueryDAOImpl;

public class ExternalToolInstanceExecutor
{
    public boolean executeAndSaveExternalToolResult(long currentBaseElementCdoId) throws IOException, InstantiationException,
            IllegalAccessException, ClassNotFoundException
    {
        ExternalToolQueryDAO externalToolQueryDAO = (ExternalToolQueryDAO) SpringContextHelper.getBeanFromWebappContext(ExternalToolQueryDAOImpl.SPRING_NAME);

        ExternalToolQuery externalToolQuery = externalToolQueryDAO.findRecentByBaseElementId(currentBaseElementCdoId);

        if (externalToolQuery == null) {
            return false;
        }

        IExternalToolConnector connector = (IExternalToolConnector) Class.forName(externalToolQuery.getExternalToolConnectorId()).newInstance();

        int result = connector.connectAndProcessResponse(externalToolQuery);

        if (result == -1) { // for dummy connectors like 'Not Needed Connector', 'Not define Connector'
            return true;
        }

        ExternalToolQueryResult externalToolQueryResult = new ExternalToolQueryResult(result, externalToolQuery);

        OpencertLogger.info("External tool result retrieved: " + externalToolQueryResult.getResult());

        externalToolQuery.getExternalToolQueryResults().add(externalToolQueryResult);

        externalToolQueryDAO.save(externalToolQuery);

        return true;
    }

}
