/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.property;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public final class OpencertPropertiesReader
    implements IOpencertClientConfigurationAdapter
{
    private String dbHost;
    private String dbUser;
    private String dbPassword;
    private int dbPort;
    private String dbName;
    private String serverAddress;

    
    private static final String DB_USER = "dbUser";
    private static final String DB_PASSWORD = "dbPassword";
    private static final String SERVER_ADDRESS = "serverAddress";
    private static final String DB_NAME = "dbName";
    private static final String DB_PORT = "dbPort";
    private static final String DB_HOST = "dbHost";
   
    
    private static final String IS_SHOW_NEW_CLONE_PROJECT_BUTTONS = "isShowNewCloneProjectButtons";
    private static final String IS_IMPACT_ANALYSIS_VIA_BASELINE_ELEMENTS_ENABLED = "isImpactAnalysisViaBaselineElementsEnabled";
    
    private static final String IS_IMPACT_ANALYSIS_TRIGERRING_FROM_WEB_ENABLED = "isImpactAnalysisTrigerringFromWebEnabled";
    private static final String IS_CDOSECURITY_ENABLED = "isCDOSecurityEnabled";
    
    
    private static final String IS_SUPPORTING_AUDITS_ENABLED = "isSupportingAudits";
    
    private static volatile OpencertPropertiesReader theInstance = null;
    
    private final static String PROPERTIES_FILE = System.getProperty("user.home") + File.separator + "opencert-properties.xml";
 
    public static OpencertPropertiesReader getInstance() 
    {
        if (theInstance == null) {
            synchronized (OpencertPropertiesReader.class) {
                if (theInstance == null) {
                    theInstance = new OpencertPropertiesReader();                    
                }
            }
        }
        return theInstance;
    }

    private OpencertPropertiesReader() 
    {
        System.out.println("OpencertPropertiesReader() opencert-properties.xml path: " + PROPERTIES_FILE);
        
        Properties props = readPropertiesFromFile();
        readProperiesFromUserHome(props);
    }

    private void readProperiesFromUserHome(Properties props)
    {
        dbHost = props.getProperty(DB_HOST);
        dbUser = props.getProperty(DB_USER);
        dbPassword = props.getProperty(DB_PASSWORD);
        serverAddress = props.getProperty(SERVER_ADDRESS);
        dbPort = Integer.parseInt(props.getProperty(DB_PORT));
        dbName = props.getProperty(DB_NAME);
       
        System.out.println("dbHost: " + dbHost);
        System.out.println("dbUser: " + dbUser);
        System.out.println("dbPassword: " + "*");
        System.out.println("serverAddress: " + serverAddress);
        System.out.println("dbPort: " + dbPort);
        System.out.println("dbName: " + dbName);
    }

    private Properties readPropertiesFromFile()
    {
        Properties result = new Properties();
    
        File file = new File(PROPERTIES_FILE);
        if (!file.exists() || file.isDirectory()) 
        {
            System.out.println("Properties file not found.\nCreating \"default properties\"");
            createDefaultopencertProperties();
        }
        
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(PROPERTIES_FILE);
            result.loadFromXML(fis);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(fis);
        }
        
        return result;
    }
        
    private static void createDefaultopencertProperties()
    {
        Properties prop = new Properties();
        prop.setProperty(DB_HOST, "localhost");
        prop.setProperty(DB_PORT, "5432");
        prop.setProperty(DB_NAME, "cdo-opencert");
        prop.setProperty(DB_USER, "opencertdbms");
        prop.setProperty(DB_PASSWORD, "opencertdbms");
        prop.setProperty(SERVER_ADDRESS, "localhost:2036");
        
        prop.setProperty(IS_SHOW_NEW_CLONE_PROJECT_BUTTONS, "false");
        prop.setProperty(IS_IMPACT_ANALYSIS_VIA_BASELINE_ELEMENTS_ENABLED, "false");        
        
        prop.setProperty(IS_SUPPORTING_AUDITS_ENABLED,"true");
        
        prop.setProperty(IS_CDOSECURITY_ENABLED, "false");
        
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(PROPERTIES_FILE);
            prop.storeToXML(fos, null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(fos);
        }
    }


    public String getDbHost()
    {
        return dbHost;
    }


    public String getDbUser()
    {
        return dbUser;
    }


    public String getDbPassword()
    {
        return dbPassword;
    }
 

    public int getDbPort()
    {
        return dbPort;
    }


    public String getDbName()
    {
        return dbName;
    }

    

    @Override
    public String getCDOServerAddress()
    {
        return serverAddress;
    }

    public boolean isShowNewCloneProjectButtons()
    {
        return readBooleanProperty(IS_SHOW_NEW_CLONE_PROJECT_BUTTONS);
    }
    
    public boolean isImpactAnalysisViaBaselineElementsEnabled()
    {
        return readBooleanProperty(IS_IMPACT_ANALYSIS_VIA_BASELINE_ELEMENTS_ENABLED);
    }

    public boolean isImpactAnalysisTrigerringFromWebEnabled()
    {
        return readBooleanProperty(IS_IMPACT_ANALYSIS_TRIGERRING_FROM_WEB_ENABLED);
    }
    
    public boolean isSupportingAuditsEnabled()
    {
        return readBooleanProperty(IS_SUPPORTING_AUDITS_ENABLED);
    }
    
    public boolean isCDOSecurityEnabled(){
    	return readBooleanProperty(IS_CDOSECURITY_ENABLED);
    }
    
    private boolean readBooleanProperty(String propertyKey)
    {
        // read the property *live* (no need to restart the JVM)
        Properties props = readPropertiesFromFile();
        final boolean result = Boolean.parseBoolean(props.getProperty(propertyKey));
        
        return result;
    }
    
    public static void close(InputStream in)
    {
        if (in == null) {
            return;
        }
        try {
            in.close();
        } catch (IOException e) {
        }
    }

    public static void close(OutputStream os)
    {
        if (os == null) {
            return;
        }
        try {
            os.close();
        } catch (IOException ex) {
        }
    }
}
