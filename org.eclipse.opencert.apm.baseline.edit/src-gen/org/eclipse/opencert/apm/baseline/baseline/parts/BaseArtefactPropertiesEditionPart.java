/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface BaseArtefactPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);


	/**
	 * @return the reference
	 * 
	 */
	public String getReference();

	/**
	 * Defines a new reference
	 * @param newValue the new reference to set
	 * 
	 */
	public void setReference(String newValue);




	/**
	 * Init the constrainingRequirement
	 * @param settings settings for the constrainingRequirement ReferencesTable 
	 */
	public void initConstrainingRequirement(ReferencesTableSettings settings);

	/**
	 * Update the constrainingRequirement
	 * @param newValue the constrainingRequirement to update
	 * 
	 */
	public void updateConstrainingRequirement();

	/**
	 * Adds the given filter to the constrainingRequirement edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToConstrainingRequirement(ViewerFilter filter);

	/**
	 * Adds the given filter to the constrainingRequirement edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToConstrainingRequirement(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the constrainingRequirement table
	 * 
	 */
	public boolean isContainedInConstrainingRequirementTable(EObject element);




	/**
	 * Init the applicableTechnique
	 * @param settings settings for the applicableTechnique ReferencesTable 
	 */
	public void initApplicableTechnique(ReferencesTableSettings settings);

	/**
	 * Update the applicableTechnique
	 * @param newValue the applicableTechnique to update
	 * 
	 */
	public void updateApplicableTechnique();

	/**
	 * Adds the given filter to the applicableTechnique edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToApplicableTechnique(ViewerFilter filter);

	/**
	 * Adds the given filter to the applicableTechnique edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToApplicableTechnique(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the applicableTechnique table
	 * 
	 */
	public boolean isContainedInApplicableTechniqueTable(EObject element);




	/**
	 * Init the ownedRel
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedRel(ReferencesTableSettings settings);

	/**
	 * Update the ownedRel
	 * @param newValue the ownedRel to update
	 * 
	 */
	public void updateOwnedRel();

	/**
	 * Adds the given filter to the ownedRel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedRel(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedRel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedRel(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedRel table
	 * 
	 */
	public boolean isContainedInOwnedRelTable(EObject element);




	/**
	 * Init the property
	 * @param settings settings for the property ReferencesTable 
	 */
	public void initProperty(ReferencesTableSettings settings);

	/**
	 * Update the property
	 * @param newValue the property to update
	 * 
	 */
	public void updateProperty();

	/**
	 * Adds the given filter to the property edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToProperty(ViewerFilter filter);

	/**
	 * Adds the given filter to the property edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToProperty(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the property table
	 * 
	 */
	public boolean isContainedInPropertyTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
