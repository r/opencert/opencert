/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface ArtefactVersionPropertiesEditionPart {

	/**
	 * @return the versionID
	 * 
	 */
	public String getVersionID();

	/**
	 * Defines a new versionID
	 * @param newValue the new versionID to set
	 * 
	 */
	public void setVersionID(String newValue);


	/**
	 * @return the date
	 * 
	 */
	public String getDate();

	/**
	 * Defines a new date
	 * @param newValue the new date to set
	 * 
	 */
	public void setDate(String newValue);


	/**
	 * @return the changes
	 * 
	 */
	public String getChanges();

	/**
	 * Defines a new changes
	 * @param newValue the new changes to set
	 * 
	 */
	public void setChanges(String newValue);


	/**
	 * @return the isLastVersion
	 * 
	 */
	public Boolean getIsLastVersion();

	/**
	 * Defines a new isLastVersion
	 * @param newValue the new isLastVersion to set
	 * 
	 */
	public void setIsLastVersion(Boolean newValue);


	/**
	 * @return the isTemplate
	 * 
	 */
	public Boolean getIsTemplate();

	/**
	 * Defines a new isTemplate
	 * @param newValue the new isTemplate to set
	 * 
	 */
	public void setIsTemplate(Boolean newValue);


	/**
	 * @return the isConfigurable
	 * 
	 */
	public Boolean getIsConfigurable();

	/**
	 * Defines a new isConfigurable
	 * @param newValue the new isConfigurable to set
	 * 
	 */
	public void setIsConfigurable(Boolean newValue);




	/**
	 * Init the resource
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initResource(ReferencesTableSettings settings);

	/**
	 * Update the resource
	 * @param newValue the resource to update
	 * 
	 */
	public void updateResource();

	/**
	 * Adds the given filter to the resource edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToResource(ViewerFilter filter);

	/**
	 * Adds the given filter to the resource edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToResource(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the resource table
	 * 
	 */
	public boolean isContainedInResourceTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
