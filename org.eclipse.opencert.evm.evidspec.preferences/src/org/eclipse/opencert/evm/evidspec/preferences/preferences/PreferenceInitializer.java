/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.evm.evidspec.preferences.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.PlatformUI;

import org.eclipse.opencert.evm.evidspec.preferences.Activator;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = PlatformUI.getPreferenceStore();	
		store.setDefault(PreferenceConstants.SVNInfo_REPOSITORY_TYPE, "");
		store.setDefault(PreferenceConstants.SVNInfo_LOCAL_REPOSITORY_URL, "");
		store.setDefault(PreferenceConstants.SVNInfo_REMOTE_REPOSITORY_URL, "");
		store.setDefault(PreferenceConstants.SVNInfo_USER, "");
		store.setDefault(PreferenceConstants.SVNInfo_PASS, "");	
	}

}
