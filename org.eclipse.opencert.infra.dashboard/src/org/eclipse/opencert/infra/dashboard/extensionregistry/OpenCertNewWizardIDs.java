package org.eclipse.opencert.infra.dashboard.extensionregistry;

/**
 * Class contains common wizard IDs.
 */
public final class OpenCertNewWizardIDs {

	/** ID for dawn refframework new wizard. */
	public static final String DAWN_REFFRAMEWORK_NEW_WIZARD_ID = "org.eclipse.opencert.pkm.refframework.refframework.diagram.part.DawnRefframeworkCreationWizard"; //$NON-NLS-1$

	/** ID for method library new wizard. */
	public static final String METHOD_LIBRARY_NEW_WIZARD_ID = "org.eclipse.epf.authoring.ui.wizards.NewLibraryWizard"; //$NON-NLS-1$

	/** ID for assurance project new wizard. */
	public static final String ASSURANCE_PROJECT_NEW_WIZARD_ID = "org.eclipse.opencert.apm.assurproj.wizards.NewAssurProjWizard"; //$NON-NLS-1$

	/** ID for CHESS project new wizard. */
	public static final String CHESS_PROJECT_NEW_WIZARD_ID = "it.unipd.chess.wizards.createproject"; //$NON-NLS-1$
}
