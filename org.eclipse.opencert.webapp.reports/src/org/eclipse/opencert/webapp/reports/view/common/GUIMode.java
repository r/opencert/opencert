/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

public enum GUIMode
{
    NORMAL(0),      // represents ordinary web layout 
    COMPACT(1);     // compact layout targetted to ba diplayed in small resolutions eg 400px wide as Custom Widget on Parasoft DTP Dashboard
    
    private int mode;
    
    GUIMode(int mode)
    {
        this.mode = mode;
    }
    
    int getMode()
    {
        return mode;
    }
}
