/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.diagram.edit.parts;

import org.eclipse.emf.cdo.dawn.gmf.synchronize.DawnConflictHelper;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.View;

public class DawnVocabularyEditPart extends VocabularyEditPart {

	public DawnVocabularyEditPart(View view) {
		super(view);
	}

	@Override
	protected void removeChild(EditPart child) {
		if (DawnConflictHelper.isConflicted((EObject) child.getModel())) {
			return;
		}
		super.removeChild(child);
	}
}
