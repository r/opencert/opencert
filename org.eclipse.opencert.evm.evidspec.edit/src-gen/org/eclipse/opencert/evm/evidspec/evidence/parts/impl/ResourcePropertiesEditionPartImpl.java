/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.impl;

// Start of user code for imports
import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;
import org.eclipse.opencert.evm.evidspec.evidence.util.ImageUtils;
import org.eclipse.opencert.evm.evidspec.evidence.util.SVNTreeDialog;
import org.eclipse.opencert.infra.svnkit.MainClass;
import org.osgi.framework.Bundle;

// End of user code

/**
 * 
 * 
 */
public class ResourcePropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ResourcePropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text location;
	protected Text format;

	// StartALC
	protected Label repoURL;
	MainClass validateInstance = null;
	// End ALC

	// Start IRR
	protected Button editLocation;
	public ImageUtils imageUtil;

	protected Button committButton;
	protected Button asignButton;
	protected Button deleteButton;
	protected Button openButton;

	protected Table tableHistory;

	protected Program programExplorer;

	protected String sURL="";
    protected String sUser="";
    protected String sPassword="";
    protected String sLocalPath="";
    protected boolean useLocalRepository;
	// End IRR

	

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ResourcePropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		
		// Start IRR **/
		imageUtil = new ImageUtils();
		// End IRR **/

		CompositionSequence resourceStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = resourceStep.addStep(EvidenceViewsRepository.Resource.Properties.class);
		propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.id);
		propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.name);
		propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.description);
		//Start IRR
		propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.format);
		//End IRR
		propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.location);
		//Start IRR
		//propertiesStep.addStep(EvidenceViewsRepository.Resource.Properties.format);
		//End IRR
		
		composer = new PartComposer(resourceStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.Resource.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == EvidenceViewsRepository.Resource.Properties.id) {
					return createIdText(parent);
				}
				if (key == EvidenceViewsRepository.Resource.Properties.name) {
					return createNameText(parent);
				}
				if (key == EvidenceViewsRepository.Resource.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == EvidenceViewsRepository.Resource.Properties.location) {
					return createLocationText(parent);
				}
				if (key == EvidenceViewsRepository.Resource.Properties.format) {
					return createFormatText(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(EvidenceMessages.ResourcePropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 4;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		
		createDescription(parent, EvidenceViewsRepository.Resource.Properties.id, EvidenceMessages.ResourcePropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		// Start IRR
		idData.horizontalSpan = 3;
		// End IRR
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, EvidenceViewsRepository.Resource.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		
		// Start IRR
		// SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Resource.Properties.id, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// End IRR
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		
		createDescription(parent, EvidenceViewsRepository.Resource.Properties.name, EvidenceMessages.ResourcePropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		// Start IRR
		nameData.horizontalSpan = 3;
		// End IRR
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, EvidenceViewsRepository.Resource.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		// Start IRR
		// SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Resource.Properties.name, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// End IRR
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.Resource.Properties.description, EvidenceMessages.ResourcePropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		// Start IRR
		descriptionData.horizontalSpan = 3;
		// End IRR
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, EvidenceViewsRepository.Resource.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		// Start IRR
		// SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Resource.Properties.description, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// End IRR
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createLocationText(Composite parent) {
		
		// Start ALC
		repoURL = SWTUtils.createPartLabel(parent, sURL, false);
		Font font = new Font(null, "Arial", 11, SWT.BOLD);
		repoURL.setFont(font);

		GridData labelData = new GridData(GridData.FILL_HORIZONTAL);
		labelData.horizontalSpan = 4;
		repoURL.setLayoutData(labelData);
		// End ALC
		
		
		createDescription(parent, EvidenceViewsRepository.Resource.Properties.location, EvidenceMessages.ResourcePropertiesEditionPart_LocationLabel);
		location = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData locationData = new GridData(GridData.FILL_HORIZONTAL);
		// Start IRR
		locationData.horizontalSpan = 2;
		// End IRR
		location.setLayoutData(locationData);
		location.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
			}

		});
		location.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
				}
			}

		});
		
		// Start ALC
		location.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				// TODO Auto-generated method stub
				// getRepositoryHandler();
				readHistory();
			}
		});
		// End ALC

		// Start IRR
		editLocation = new Button(parent, SWT.NONE);
		editLocation.setText("...");
		GridData editLocationData = new GridData();
		editLocation.setLayoutData(editLocationData);

		editLocation.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */

			public void widgetSelected(SelectionEvent e) {

				openFileBrowser();
			}

		});

		// Create buttond for the repository
		createSVNReportButtons(parent);

		// Create table for repository history
		createSVNReportTableHistory(parent);

		// End IRR

		EditingUtils.setID(location,
				EvidenceViewsRepository.Resource.Properties.location);
		EditingUtils.setEEFtype(location, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent
				.getHelpContent(
						EvidenceViewsRepository.Resource.Properties.location,
						EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createLocationText

		// End of user code
		return parent;
	}

	
	protected Composite createFormatText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.Resource.Properties.format, EvidenceMessages.ResourcePropertiesEditionPart_FormatLabel);
		format = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData formatData = new GridData(GridData.FILL_HORIZONTAL);
		// Start IRR
		formatData.horizontalSpan = 3;
		// End IRR
		format.setLayoutData(formatData);
		format.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.format, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, format.getText()));
			}

		});
		format.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.format, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, format.getText()));
				}
			}

		});
		EditingUtils.setID(format, EvidenceViewsRepository.Resource.Properties.format);
		EditingUtils.setEEFtype(format, "eef::Text"); //$NON-NLS-1$
		// Start IRR
		// SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Resource.Properties.format, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// End IRR
		// Start of user code for createFormatText

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		Shell thisS= view.getShell();
		MessageDialog.openInformation(thisS, "Confirm", "Artefact Resource Changed");
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Resource.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(EvidenceMessages.Resource_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Resource.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(EvidenceMessages.Resource_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Resource.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(EvidenceMessages.Resource_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#getLocation()
	 * 
	 */
	public String getLocation() {
		return location.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#setLocation(String newValue)
	 * 
	 */
	public void setLocation(String newValue) {
		if (newValue != null) {
			location.setText(newValue);
		} else {
			location.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Resource.Properties.location);
		if (eefElementEditorReadOnlyState && location.isEnabled()) {
			location.setEnabled(false);
			location.setToolTipText(EvidenceMessages.Resource_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !location.isEnabled()) {
			location.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#getFormat()
	 * 
	 */
	public String getFormat() {
		return format.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ResourcePropertiesEditionPart#setFormat(String newValue)
	 * 
	 */
	public void setFormat(String newValue) {
		if (newValue != null) {
			format.setText(newValue);
		} else {
			format.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Resource.Properties.format);
		if (eefElementEditorReadOnlyState && format.isEnabled()) {
			format.setEnabled(false);
			format.setToolTipText(EvidenceMessages.Resource_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !format.isEnabled()) {
			format.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.Resource_Part_Title;
	}

	//Start IRR
			@Override
		    public void setUrl(String newvalue) {
		          if (newvalue != null) {
		                 sURL= newvalue;
		          } else {
		                 sURL = "";
		          }
		    }

		    @Override
		    public String getUrl() {
		          return sURL;
		    }

		    @Override
		    public void setUser(String newvalue) {
		          if (newvalue != null) {
		                 sUser= newvalue;
		          } else {
		                 sUser = "";
		          }
		    }

		    @Override
		    public String getUser() {
		          return sUser;
		    }

		    @Override
		    public void setPassword(String newvalue) {
		          if (newvalue != null) {
		                 sPassword= newvalue;
		          } else {
		                 sPassword = "";
		          }
		    }

		    @Override
		    public String getPassword() {
		          return sPassword;
		    }

		    @Override
		    public void setLocalPath(String newvalue) {
		          if (newvalue != null) {
		                 sLocalPath= newvalue;
		          } else {
		                 sLocalPath = "";
		          }      
		    }

		    @Override
		    public String getLocalPath() {
		                 return sLocalPath;
		    }

		    @Override
		    public void setUseLocalRepository(boolean newvalue) {         
		    	useLocalRepository= newvalue;            
		    }

		    @Override
		    public boolean getUseLocalRepository() {
		           return useLocalRepository;
		    }
		    
		    @Override
			public void connectRepo() {
				//Begin ALC
				try {
					if(useLocalRepository){
						repoURL.setText(getLocalPath());
					}
					else{
						repoURL.setText(getUrl());
					}
					//Avoid reconecctions
					if(validateInstance==null && !useLocalRepository){
						Bundle svnBundle = Platform.getBundle("org.eclipse.opencert.infra.svnkit"); 
						Class validateClass = svnBundle.loadClass("org.eclipse.opencert.infra.svnkit.MainClass"); 
						validateInstance = (MainClass) validateClass.newInstance(); 
						validateInstance.connectRepository(getUrl(),getUser(),getPassword());
						readHistory();			
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block			
					e.printStackTrace();
				}
				//End ALC
			}		
			//End IRR
	// Start of user code additional methods
	
	private void openFileBrowser() {
		// File standard dialog
	    FileDialog fileDialog = new FileDialog(Display.getDefault().getActiveShell());
	    // Set the text
	    fileDialog.setText("Select Artefact File");
	    // Set filter path
	    if(useLocalRepository){
	    	fileDialog.setFilterPath(getLocalPath());
	    }
	    // Set filter on .txt files
	    //fileDialog.setFilterExtensions(new String[] { "*.txt" });
	    // Put in a readable name for the filter
	    //fileDialog.setFilterNames(new String[] { "Textfiles(*.txt)" });
	    // Open Dialog and save result of selection
	    String selected = fileDialog.open();
	    if (selected != null) 
	    	location.setText(selected);
	    
	    if (propertiesEditionComponent != null)
			propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
		setHasChanged(true);
	}
	
	
	private void readHistory() {
		//Begin ALC		
		if(location!= null && !useLocalRepository && validateInstance!=null){
			if(location.getText()!=null && !location.getText().equals("")){
				tableHistory.removeAll();
				ArrayList<String> entriesList=validateInstance.getFileHistory(location.getText());
				for (int i = 0; i < entriesList.size(); i++) {
					String allData = entriesList.get(i);
					TableItem tableItem = new TableItem(tableHistory,i);
					String []parts=allData.split("#");
					for (int j = 0; j < parts.length; j++) {
						tableItem.setText(j,parts[j]);
					}							
				}
			}	
			else{
				tableHistory.removeAll();
				tableHistory.setItemCount (10);
			}
		}
		//End ALC
	}
	
	public void launchExplorer(String param) {
		Program.launch(param);
	}
	
	protected Composite createSVNReportTableHistory(Composite parent) {
		
		tableHistory = new Table (parent,  SWT.BORDER | SWT.MULTI);					
		
		tableHistory.setLinesVisible (true);
		tableHistory.setHeaderVisible (true);
		
				
		GridData tableData = new GridData(GridData.FILL_BOTH);
		tableData.horizontalSpan = 4;
		tableHistory.setLayoutData(tableData);
		tableHistory.setItemCount (10);
		
		TableColumn col1 = new TableColumn(tableHistory, SWT.NONE);
		col1.setWidth(80);
		col1.setText("Revision"); //$NON-NLS-1$

		TableColumn col2 = new TableColumn(tableHistory, SWT.NONE);
		col2.setWidth(120);
		col2.setText("Date"); //$NON-NLS-1$
	
		
		TableColumn col3 = new TableColumn(tableHistory, SWT.NONE);
		col3.setWidth(80);
		col3.setText("Author");
		
		TableColumn col4 = new TableColumn(tableHistory, SWT.NONE);
		col4.setWidth(300);
		col4.setText("Comment");
		
		
		return parent;
	}

	protected Composite createSVNReportButtons(Composite parent) {
		
		committButton = new Button(parent, SWT.NONE);
		committButton.setText("Commit");
		asignButton = new Button(parent, SWT.NONE);
		asignButton.setText("Asign");
		deleteButton = new Button(parent, SWT.NONE);
		deleteButton.setText("Delete");
		openButton = new Button(parent, SWT.NONE);
		openButton.setText("Open");
		
		GridData committButtonData = new GridData(GridData.FILL_HORIZONTAL);
		committButtonData.horizontalSpan = 1;
		committButton.setLayoutData(committButtonData);
		asignButton.setLayoutData(committButtonData);
		deleteButton.setLayoutData(committButtonData);
		openButton.setLayoutData(committButtonData);
		
		committButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
			
				//Begin ALC
			/*	EObject thisEvidence = PropertiesContextService.getInstance()
						.lastElement();*/
				
				if(location.getText()!=null && !location.getText().equals("") && !useLocalRepository){
				
					//String resultMessage="";
					//Check the file is in local file sytem
					File local = new File(location.getText());
					if(local.exists()){
						SVNTreeDialog dialog = new SVNTreeDialog(
								Display.getDefault().getActiveShell(),
								new AdapterFactoryLabelProvider(adapterFactory),
								validateInstance,
								true,
								location.getText(), "Select the destination folder", imageUtil);
						String destinationFolder="";
						if (dialog.open() == Window.OK) {

							//Upload the file to the folder
							destinationFolder = dialog.getResult();
							

							String result= validateInstance.addFileToRepo(location.getText(), destinationFolder, "New artefact " + name.getText());
							if (result.contains("exists")){
								int ax = JOptionPane.showConfirmDialog(null, "Selected file exists in the repository. Do you want to update it?");
								if(ax == JOptionPane.YES_OPTION){
									result=validateInstance.changeRepoFile(location.getText(), destinationFolder, "Update artefact " + name.getText());
									if(!result.contains("error")){
										location.setText(result);
										result="Update Done!";
										if (propertiesEditionComponent != null)
											propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
										setHasChanged(true);
									}								
								}
								else{
									result="Commit Canceled!";
								}
							}
							else if(!result.contains("error")){
								location.setText(result);	
								result="Commit Done!";
								if (propertiesEditionComponent != null)
									propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
								setHasChanged(true);
							}						

							JOptionPane.showMessageDialog(null,
									result,
									"Commit",
									JOptionPane.INFORMATION_MESSAGE);							
						}					
					}
				}
				//End ALC								
			}
			
			
		});
		
		asignButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				//getRepositoryHandler();
				if(useLocalRepository){
					openFileBrowser();									
				}
				else{
					SVNTreeDialog dialog = new SVNTreeDialog(
							Display.getDefault().getActiveShell(),
							new AdapterFactoryLabelProvider(adapterFactory),
							validateInstance,
							false,
							location.getText(), "Select file from repository to assign", imageUtil);
					//String destinationFolder="";
					if (dialog.open() == Window.OK) {

						//Upload the file to the folder
						location.setText(dialog.getResult());					
						if (propertiesEditionComponent != null)
							propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
						setHasChanged(true);
					}				
				}
			}

		});
		
		
		deleteButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if(location.getText()!=null && !location.getText().equals("")){
					if(useLocalRepository){
						int ax = JOptionPane.showConfirmDialog(null, "The file will be deleted from the local hard disk. Are you sure?");
						if(ax == JOptionPane.YES_OPTION){
							File localFile= new File(location.getText());
							localFile.delete();
							location.setText("");
							if (propertiesEditionComponent != null)
								propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
							setHasChanged(true);
						}
					}
					else{

						int ax = JOptionPane.showConfirmDialog(null, "The file will be deleted from the repository. Are you sure?");
						if(ax == JOptionPane.YES_OPTION){
							validateInstance.deleteFile(location.getText());
							location.setText("");	
							if (propertiesEditionComponent != null)
								propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ResourcePropertiesEditionPartImpl.this, EvidenceViewsRepository.Resource.Properties.location, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, location.getText()));
							setHasChanged(true);

						}		



						/*JOptionPane.showMessageDialog(null,
								"delete",
								"delete",
								JOptionPane.INFORMATION_MESSAGE); */
					}
				}			
			}
			

		});
		
		
		openButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				String selection ="";
				if(location.getText()!=null && !location.getText().equals("")){
					if(useLocalRepository){					
						selection = location.getText();											
					}
					else{					
						selection= validateInstance.getFileFromRepo(location.getText());				
					}
					int dot = selection.lastIndexOf('.');
					if (dot != -1) {
						String extension = selection.substring(dot);
						Program program = Program.findProgram(extension);
						if (program != null) {
							Program.launch(selection);
						} else {
							launchExplorer(selection);
						}
					} else {
						launchExplorer(selection);
					}
				}
			}
		});
		
		
		return parent;
	}

	
	
	
	// End of user code


}
