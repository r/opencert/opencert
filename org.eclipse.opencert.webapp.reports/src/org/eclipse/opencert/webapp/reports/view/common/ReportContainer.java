/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.listeners.IProjectChangeListener;
import org.eclipse.opencert.webapp.reports.listeners.IReportChangeListener;
import org.eclipse.opencert.webapp.reports.view.url.URLParamHandler;
import org.eclipse.opencert.webapp.reports.view.url.URLParamsContainer;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.PopupView.Content;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ReportContainer extends CustomComponent implements IProjectChangeListener, IReportChangeListener
{
	private IReport currentReport;
	
	private final ProjectComboWrapper projectComboWrapper; 
	
	private final Label titleLabel;
	private final Panel parametersPanel;
	private final Panel reportPanel;
	private EmbedLinkPopupComponent embedLinkPopupComponent;
	

    private GUIMode guiMode;
	
	public ReportContainer(ProjectComboWrapper projectComboWrapper, URLParamsContainer urlParamsInfo) 
	{		
		this.projectComboWrapper = projectComboWrapper;
		this.guiMode = urlParamsInfo.getGUIMode();
		
		setSizeFull();
		setStyleName("reportContainer");
		
		VerticalLayout mainPanel = new VerticalLayout();
		setCompositionRoot(mainPanel);
		mainPanel.setSizeFull();
		
		HorizontalLayout reportTopPanel = new HorizontalLayout();
		reportTopPanel.setStyleName("reportTopPanel");
		
		titleLabel = new Label();
		if (guiMode == GUIMode.NORMAL) 
		{
		    titleLabel.setStyleName("reportTitleLabel");
		    reportTopPanel.addComponent(titleLabel);
		} 
		else if (guiMode == GUIMode.COMPACT) 
		{
            Link opencertLogo = TopPanel.createOpencertLogo(urlParamsInfo, GUIMode.NORMAL);
            reportTopPanel.addComponent(opencertLogo);

		    Component projectsPanel = projectComboWrapper.getProjectsPanel();
		    reportTopPanel.addComponent(projectsPanel);
		}
		
		parametersPanel = new Panel();
		parametersPanel.setStyleName("reportParametersComponent");
		reportTopPanel.addComponent(parametersPanel);
		
		if (guiMode == GUIMode.NORMAL) 
		{
            Label spacer = new Label();
            spacer.setWidth("100%");
            reportTopPanel.addComponent(spacer);
            reportTopPanel.setExpandRatio(spacer, 1.0f);
            
            PopupView embeddingLinkPopup = initEmbedLinkPopup(urlParamsInfo);
            reportTopPanel.addComponent(embeddingLinkPopup);
		}
		
		reportPanel = new Panel();
		reportPanel.setStyleName("reportMainComponent");
		reportPanel.setSizeFull();
		
		mainPanel.addComponent(reportTopPanel);
		mainPanel.addComponent(reportPanel);
		mainPanel.setExpandRatio(reportPanel, 1.0f);
	}

    private PopupView initEmbedLinkPopup(URLParamsContainer urlParamsInfo)
    {
        Content popupContent = new Content() {
            
            @Override
            public Component getPopupComponent()
            {
                if (embedLinkPopupComponent == null)
                {
                    embedLinkPopupComponent = new EmbedLinkPopupComponent(urlParamsInfo);
                }
                return embedLinkPopupComponent;
            }
            
            @Override
            public String getMinimizedValueAsHTML()
            {
                return "<a href=\"#\" title=\"Show URL to this page\">"
                        + "<img class=\"embeddingLinkIcon\" src=\"VAADIN/themes/opencerttheme/images/scrollDown.gif\">"
                        + "</a>";
            }
        };
        PopupView popupView = new PopupView(popupContent);
        popupView.setImmediate(true);
        popupView.setStyleName("embedLinkPopup");
        
        popupView.addPopupVisibilityListener((e) ->
        {
            if (e.isPopupVisible()) {
                if (embedLinkPopupComponent != null) {
                    embedLinkPopupComponent.selectMainURL();
                }
            }
        });
        
        return popupView;
    }
	
	@Override
	public void projectChanged(AssuranceProjectWrapper newProject) 
	{
		if (currentReport != null) {
			currentReport.projectChanged(newProject);
		}
	}
	
	@Override
	public void reportChanged(IReport report) 
	{
		currentReport = report;
		
		final String title = report.getTitle();
		if (title != null) {
			titleLabel.setValue(title);
		}
		Component parametersComponent = report.getParametersComponent(guiMode);
		parametersPanel.setContent(parametersComponent);
		Component mainComponent = report.getMainComponent(guiMode);
		reportPanel.setContent(mainComponent);
		
		final AssuranceProjectWrapper selectedProject = projectComboWrapper.getSelectedProject();
		projectChanged(selectedProject);
	}
	
	private final static class EmbedLinkPopupComponent
	    extends VerticalLayout
	{
	    private final TextField normalModeLink;

        public EmbedLinkPopupComponent(URLParamsContainer urlParamsInfo)
        {
            normalModeLink = new TextField("Link to this page: ", URLParamHandler.generateWebURL(urlParamsInfo, GUIMode.NORMAL));
            normalModeLink.setSizeFull();
            final TextField compactModeLink = new TextField("Link to compact view:", URLParamHandler.generateWebURL(urlParamsInfo, GUIMode.COMPACT));
            compactModeLink.setSizeFull();
            urlParamsInfo.addURLParamsChangeListener(() -> 
            {
                normalModeLink.setValue(URLParamHandler.generateWebURL(urlParamsInfo, GUIMode.NORMAL));
                compactModeLink.setValue(URLParamHandler.generateWebURL(urlParamsInfo, GUIMode.COMPACT));
            });
            addComponent(normalModeLink);
            addComponent(compactModeLink);
        }

        public void selectMainURL()
        {
            normalModeLink.focus();
            normalModeLink.selectAll();
        }
	}
}