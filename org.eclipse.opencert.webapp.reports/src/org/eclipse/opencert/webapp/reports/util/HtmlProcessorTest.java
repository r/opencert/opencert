/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

import static org.junit.Assert.fail;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

public class HtmlProcessorTest
{
    private final static Map<String, String> TEXT_WITH_LINKS_MAP = new LinkedHashMap<String, String>();
    private final String LS = System.getProperty("line.separator");
    
    static {
        TEXT_WITH_LINKS_MAP.put("", "");
        TEXT_WITH_LINKS_MAP.put("atext", "atext");
        TEXT_WITH_LINKS_MAP.put("atext www.a.pl", "atext <a href=\"http://www.a.pl\" title=\"Open the link\">www.a.pl</a>");
        TEXT_WITH_LINKS_MAP.put("atext www.a2.pl btext", "atext <a href=\"http://www.a2.pl\" title=\"Open the link\">www.a2.pl</a> btext");
        TEXT_WITH_LINKS_MAP.put("atext www.a2.pl btext www.a4.pl ctext", "atext <a href=\"http://www.a2.pl\" title=\"Open the link\">www.a2.pl</a> btext <a href=\"http://www.a4.pl\" title=\"Open the link\">www.a4.pl</a> ctext");
        
        
        TEXT_WITH_LINKS_MAP.put("\natext www.a.pl\n", "\natext <a href=\"http://www.a.pl\" title=\"Open the link\">www.a.pl</a>\n");
        TEXT_WITH_LINKS_MAP.put("atext www.a.pl ala\n"
                                    + "atext\n"
                                    + "ela www.a.pl", 
                                    "atext <a href=\"http://www.a.pl\" title=\"Open the link\">www.a.pl</a> ala\n"
                                                        + "atext\n"
                                                        + "ela <a href=\"http://www.a.pl\" title=\"Open the link\">www.a.pl</a>");
        
        TEXT_WITH_LINKS_MAP.put("atext HTTP://a.pl", "atext <a href=\"HTTP://a.pl\" title=\"Open the link\">HTTP://a.pl</a>");
        
        TEXT_WITH_LINKS_MAP.put("atext HTTP://a.pl ala\n"
                + "atext\n"
                + "ela https://a.pl", 
                "atext <a href=\"HTTP://a.pl\" title=\"Open the link\">HTTP://a.pl</a> ala\n"
                                    + "atext\n"
                                    + "ela <a href=\"https://a.pl\" title=\"Open the link\">https://a.pl</a>");
        
        TEXT_WITH_LINKS_MAP.put("atext HTTP://a.pl\n"
                + "atext\n"
                + "ela https://a.pl", 
                "atext <a href=\"HTTP://a.pl\" title=\"Open the link\">HTTP://a.pl</a>\n"
                                    + "atext\n"
                                    + "ela <a href=\"https://a.pl\" title=\"Open the link\">https://a.pl</a>");
    }

    @Test    
    public void test1()
    {
        Iterator<Entry<String, String>> iterator = TEXT_WITH_LINKS_MAP.entrySet().iterator();
        int counter = 0;
        while (iterator.hasNext())
        {
            System.err.println("########################## counter: " + counter++ + " ##########################");
            Entry<String, String> next = iterator.next();
            final String input = next.getKey();
            String outputOK = next.getValue();
            String outputActual = HtmlProcessor.processHtmlLinks(input);
            if (!outputOK.equalsIgnoreCase(outputActual))
            {
                String msg = String.format("Test failed! Links not processed correctly." + LS
                        + "input: %s " + LS
                        + "outputActual: %s " + LS
                        + "outputOK: %s" + LS,
                        input, outputActual, outputOK);
                System.err.println(msg);
                fail(msg);
            } else {
                System.err.println("[OK] for input: " + input);
            }
        }
    }
}
