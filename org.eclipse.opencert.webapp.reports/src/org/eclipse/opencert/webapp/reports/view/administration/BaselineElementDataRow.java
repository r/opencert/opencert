/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.administration;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.opencert.webapp.reports.util.StringUtil;

import com.google.gwt.dev.util.collect.HashSet;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

public class BaselineElementDataRow
    implements IRelationChangedLister
{
    private final Long cdoID;
    private final String name;
    private final String description;
    private final EditRowPanel buttonsPanel;
    private BaseArtefactRelationsField relationsField;

    private final TextField editedName;
    private final TextArea editedDescription;
    private List<Long> editedRelations;
    
    private Long oldCDOID = -1l;

    public BaselineElementDataRow(Long cdoID, String name, String description, EditRowPanel buttonsPanel, BaseArtefactRelationsField relationsField)
    {
        this.cdoID = cdoID;
        this.name = StringUtil.ensureNotNull(name);;
        this.description = StringUtil.ensureNotNull(description);
        
        this.buttonsPanel = buttonsPanel;
        this.relationsField = relationsField;
        
        this.editedName = new TextField(null, getName());
        this.editedName.addFocusListener((e) -> this.buttonsPanel.setButtonsShortcuts(true));                        
        this.editedName.addBlurListener((e) -> this.buttonsPanel.setButtonsShortcuts(false));
        this.editedName.setBuffered(true);

        this.editedDescription = new TextArea(null, getDescription());
        this.editedDescription.setRows(5);
        this.editedDescription.setWidth("100%");
        this.editedDescription.addFocusListener((e) -> this.buttonsPanel.setButtonsShortcuts(true));                        
        this.editedDescription.addBlurListener((e) -> this.buttonsPanel.setButtonsShortcuts(false));
        this.editedDescription.setBuffered(true);
        
        resetEditedRelations();    //no relations changed
    }

    public TextField getEditedNameField()
    {
        return editedName;
    }

    public TextArea getEditedDescriptionField()
    {
        return editedDescription;
    }

    public boolean isChangedInGUI()
    {
        boolean isChanged = false;
        if (getEditedName() != null)   // there was some editing
        {
            isChanged = !StringUtil.equal(name, getEditedName(), true);
            if (isChanged) {
                return isChanged;
            }
        }
        if (getEditedDescription() != null)   // there was some editing
        {
            isChanged = !StringUtil.equal(description, getEditedDescription(), true);
            if (isChanged) {
                return isChanged;
            }
        }
        if (editedRelations != null)    // the relations have been edited
        {
            final Set<Long> editedSet = new HashSet<>(editedRelations); 
            final Set<Long> relationsSet = new HashSet<>(relationsField.getRelationIDs());
            
            isChanged = !editedSet.equals(relationsSet);
            if (isChanged) {
                return isChanged;
            }
        }
        
        return false;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public Long getCdoID()
    {
        return cdoID;
    }
    
    public String getCdoIDForGUI()
    {
        if (ProjectBaselineArtefactTable.RowTempIDGenerator.isTempID(cdoID))
        {
            return "";
        } 
        else 
        {
            return String.valueOf(cdoID);
        }
    }

    public EditRowPanel getEditRowPanel()
    {
        return buttonsPanel;
    }

    String getEditedName()
    {
        return (editedName != null ? editedName.getValue() : null);
    }

    String getEditedDescription()
    {
        return (editedDescription != null ? editedDescription.getValue() : null);
    }

    public BaseArtefactRelationsField getRelationsField()
    {
        return relationsField;
    }

    public void resetEditedRelations()
    {
        relationsField.resetRelatedIdLabels();
        editedRelations = new LinkedList<>();
        editedRelations.addAll(relationsField.getRelationIDs());
    }
    

    public List<Long> getEditedRelations()
    {
        return editedRelations;
    }

    @Override
    public void relationMarked(boolean setMarked, long relatedID)
    {
        if (editedRelations == null) {
            editedRelations = new LinkedList<>();
        }
        if (setMarked) {
            if (!editedRelations.contains(relatedID)) {
                editedRelations.add(relatedID);
                relationsField.markEditedRelatedIDLabel(relatedID, setMarked);
            }
        } else {
            if (editedRelations.contains(relatedID)) {
                relationsField.markEditedRelatedIDLabel(relatedID, setMarked);
            }
            while (editedRelations.contains(relatedID)) {
                editedRelations.remove(relatedID);
            }
        }
    }

    public void setRelationsField(BaseArtefactRelationsField baseArtefactRelationsField)
    {
        relationsField = baseArtefactRelationsField;
    }

    public void setOldCDOID(Long oldCDOID)
    {
        this.oldCDOID = oldCDOID;
    }

    public Long getOldCDOID()
    {
        return oldCDOID;
    }
    
}
