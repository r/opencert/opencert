/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.utils.image;

import java.io.File;

import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

public class ImageUtils {
	private ImageRegistry imageRegister;
	public final String IMGS_DIR_OPEN = "dir_open";
	public final String IMGS_DIR_CLOSED = "dir_closed";
	public final String IMGS_FILE = "file";
	
	public ImageUtils(){
		// creates an image registry and adds icons to the image registry.
		imageRegister = new ImageRegistry();
		initImageRegister();
	}
	public void initImageRegister (){
		imageRegister.put(IMGS_DIR_CLOSED, PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER));
		imageRegister.put(IMGS_DIR_OPEN, PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER));
		imageRegister.put(IMGS_FILE, PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FILE));
	}

	public void PutImage (Item item, String theFile){
		Image itemImage = null;
		Program program = null;
		File root = new File(theFile);
		if (root.isDirectory())
			itemImage = imageRegister.get(IMGS_DIR_CLOSED);
		else {
    		int	dot	=	root.getName().lastIndexOf('.');
    		if (dot	!= -1) {
    			String extension = root.getName().substring(dot);
    			if (imageRegister.get(extension)== null){
    				program	=	Program.findProgram(extension);
        			if (program	!= null) {
        				ImageData	imageData	=	program.getImageData();
        				if (imageData	!= null) {
        					itemImage	=	new	Image(null,	imageData, imageData.getTransparencyMask());
        				    imageRegister.put(extension, itemImage);
        				}
        			}
        			else{
        				itemImage = imageRegister.get(IMGS_FILE);
        			}
    			}else{
    				itemImage = imageRegister.get(extension);
    			}
    		}
		}
		if (itemImage == null) {
			itemImage = imageRegister.get(IMGS_FILE);
		}
    	item.setImage(itemImage);
	}

}
