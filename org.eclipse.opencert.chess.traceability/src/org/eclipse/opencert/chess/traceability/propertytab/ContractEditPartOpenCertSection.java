/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.opencert.chess.traceability.propertytab;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.opencert.chess.traceability.util.Utils;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl;

import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.sysml.diagram.common.edit.part.ConstraintBlockEditPart;
import org.eclipse.papyrus.uml.diagram.clazz.edit.parts.ClassEditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Button;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.uml2.uml.Class;
import org.polarsys.chess.contracts.profile.chesscontract.Contract;



public class ContractEditPartOpenCertSection extends AbstractPropertySection{
	
	public static final String CONTRACT = "CHESSContract::Contract";
	public static final String CONTRACT_PROP = "CHESSContract::ContractProperty";

	private Contract contractStereo;
		
	private Button removeClaimButton;
	private ClaimTreeDropAdapter claimDropAdapter;
	private TableViewer claimViewer;
	
	private Button removeSupportedArtefactButton;
	private ArtefactTreeDropAdapter artefactDropAdapter;
	private TableViewer supportedByArtefactViewer;
	
	private Button removeAgreeButton;
	private AgreementTreeDropAdapter agreementDropAdapter;
	private TableViewer agreementViewer;

	
	public ContractEditPartOpenCertSection(){

	}
	

	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
		
		super.createControls(parent, aTabbedPropertySheetPage);		
		
		Composite composite = getWidgetFactory().createComposite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(4, false));
				
		GridData gd = new GridData(SWT.FILL, SWT.FILL, true, false);
		
		//CLAIM PROPERTY
		gd = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd.horizontalSpan = 3;
		Label claimLabel = getWidgetFactory().createLabel(composite, "Claim", SWT.NONE);
		claimLabel.setLayoutData(gd);
		
		removeClaimButton = getWidgetFactory().createButton(composite, "", SWT.NONE);
		
		ImageDescriptor remImageDes= AbstractUIPlugin.imageDescriptorFromPlugin("org.polarsys.chess.contracts.chessextension", "/icons/rem_co.gif");
		removeClaimButton.setImage(remImageDes.createImage());
		
		
		removeClaimButton.addSelectionListener(new SelectionListener() {
		      
			@Override
			public void widgetSelected(SelectionEvent e) {

				ISelection selection = claimViewer.getSelection();

				Claim claim = (Claim) ((StructuredSelection) selection).getFirstElement();

				Utils.removeContractClaimTrace(contractStereo, claim);
				claimViewer.remove(selection);
				claimViewer.refresh();
				
				/*
				@SuppressWarnings("deprecation")
				ReferenceSelector selector = new ReferenceSelector(true);
				org.eclipse.papyrus.infra.widgets.providers.StaticContentProvider prov = new org.eclipse.papyrus.infra.widgets.providers.StaticContentProvider(null);
				
				
				org.eclipse.papyrus.uml.tools.providers.UMLContentProvider prov2 = new org.eclipse.papyrus.uml.tools.providers.UMLContentProvider(contract,
						UMLPackage.eINSTANCE.getPackage_PackagedElement());
				
				Object obj =CDOUtil.getCDOObject(contract);
				
				
				selector.setContentProvider(prov2);
				MultipleValueSelectionDialog vDialog = new MultipleValueSelectionDialog(shell, selector, "zzzz");
				int result = vDialog.open();*/
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
		    });
		   
		 //
		gd = new GridData(SWT.END, SWT.FILL, false, false);
		gd.horizontalSpan = 1;
		
		removeClaimButton.setLayoutData(gd);
		
		claimViewer = new TableViewer(composite, SWT.MULTI | SWT.H_SCROLL
                | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		// create the columns
		createClaimColumns(parent,claimViewer);
		
		// make lines and header visible
		final Table table = claimViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		claimViewer.setContentProvider(ClaimTreeContentProvider.getInstance());
		claimDropAdapter = new ClaimTreeDropAdapter(claimViewer);
		Transfer[] transfers = new Transfer[] { LocalSelectionTransfer.getTransfer() };
		int ops = DND.DROP_COPY | DND.DROP_MOVE;
		claimViewer.addDropSupport(ops, transfers, claimDropAdapter);
		gd = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd.horizontalSpan = 4;
		
		claimViewer.getControl().setLayoutData(gd);
		
		claimViewer.addDoubleClickListener(new ClickListener());
		
		//////////////////////
		//supportedBy property
		gd = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd.horizontalSpan = 3;
		Label supportedByLabel = getWidgetFactory().createLabel(composite, "SupportedBy", SWT.NONE);
		supportedByLabel.setLayoutData(gd);
		
		removeSupportedArtefactButton = getWidgetFactory().createButton(composite, "", SWT.NONE);
		
		remImageDes= AbstractUIPlugin.imageDescriptorFromPlugin("org.polarsys.chess.contracts.chessextension", "/icons/rem_co.gif");
		removeSupportedArtefactButton.setImage(remImageDes.createImage());
		
		
		removeSupportedArtefactButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				ISelection selection = supportedByArtefactViewer.getSelection();
				final Artefact artefact = (Artefact) ((StructuredSelection) selection).getFirstElement();
				Utils.removeContractEvidenceTrace(contractStereo, artefact);
				supportedByArtefactViewer.remove(selection);
				supportedByArtefactViewer.refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}

		    });
		   
		 //
		gd = new GridData(SWT.END, SWT.FILL, false, false);
		gd.horizontalSpan = 1;
		
		removeSupportedArtefactButton.setLayoutData(gd);
		
		supportedByArtefactViewer = new TableViewer(composite, SWT.MULTI | SWT.H_SCROLL
                | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		// create the columns
		createArtefactColumns(parent,supportedByArtefactViewer);
		
		final Table tableart = supportedByArtefactViewer.getTable();
		tableart.setHeaderVisible(true);
		tableart.setLinesVisible(true);
		
		//claimViewer.setContentProvider(ArrayContentProvider.getInstance());
		supportedByArtefactViewer.setContentProvider(ClaimTreeContentProvider.getInstance());
		
		artefactDropAdapter= new ArtefactTreeDropAdapter(supportedByArtefactViewer);
		//Transfer[] transfers = new Transfer[] { ResourceTransfer.getInstance(), LocalSelectionTransfer.getTransfer(), PluginTransfer.getInstance() };
		transfers = new Transfer[] { LocalSelectionTransfer.getTransfer() };
		ops = DND.DROP_COPY | DND.DROP_MOVE;
		supportedByArtefactViewer.addDropSupport(ops, transfers, artefactDropAdapter);
		gd = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd.horizontalSpan = 4;
		
		supportedByArtefactViewer.getControl().setLayoutData(gd);
		supportedByArtefactViewer.addDoubleClickListener(new ClickListener());
		///
		

		///////AGREEMENT PROPERTY
		gd = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd.horizontalSpan = 3;
		Label agreementLabel = getWidgetFactory().createLabel(composite, "Agreement", SWT.NONE);
		agreementLabel.setLayoutData(gd);
		
		removeAgreeButton = getWidgetFactory().createButton(composite, "", SWT.NONE);
		
		remImageDes= AbstractUIPlugin.imageDescriptorFromPlugin("org.polarsys.chess.contracts.chessextension", "/icons/rem_co.gif");
		removeAgreeButton.setImage(remImageDes.createImage());
		
		
		removeAgreeButton.addSelectionListener(new SelectionListener() {
		      
			@Override
			public void widgetSelected(SelectionEvent e) {

				ISelection selection = agreementViewer.getSelection();
				Agreement agree = (Agreement) ((StructuredSelection) selection).getFirstElement();
				Utils.removeContractAgreementTrace(contractStereo, agree);
				agreementViewer.remove(selection);
				agreementViewer.refresh();
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
		    });
		   
		 //
		gd = new GridData(SWT.END, SWT.FILL, false, false);
		gd.horizontalSpan = 1;
		
		removeAgreeButton.setLayoutData(gd);

		agreementViewer = new TableViewer(composite, SWT.MULTI | SWT.H_SCROLL
                | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		// create the columns
		createAgreementColumns(parent,agreementViewer);
		
		// make lines and header visible
		final Table tableAgree = agreementViewer.getTable();
		tableAgree.setHeaderVisible(true);
		tableAgree.setLinesVisible(true);
		
		//TODO
		agreementViewer.setContentProvider(ClaimTreeContentProvider.getInstance());
	
		agreementDropAdapter = new AgreementTreeDropAdapter(agreementViewer);
		transfers = new Transfer[] { LocalSelectionTransfer.getTransfer() };
		ops = DND.DROP_COPY | DND.DROP_MOVE;
		agreementViewer.addDropSupport(ops, transfers, agreementDropAdapter);
		gd = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd.horizontalSpan = 4;
		
		agreementViewer.getControl().setLayoutData(gd);
		agreementViewer.addDoubleClickListener(new ClickListener());
		///////end agreement
				
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		cleanPropertyTab();
		if (!(selection instanceof IStructuredSelection)){	
			 return;
		}
		List<?> selectionList = ((IStructuredSelection) selection).toList();
		if (selectionList.size() == 1) {
			Object selected = selectionList.get(0);
			
			EObject selectedEObject = EMFHelper.getEObject(selected);
			if (selectedEObject != null){
			    //do something
			}
			if(selected instanceof ConstraintBlockEditPart){
				Class clazz = (Class) (((ConstraintBlockEditPart)selected).resolveSemanticElement());
				//check if it's a contract
				if(clazz.getAppliedStereotype(CONTRACT) != null){
					contractStereo = (Contract) clazz.getStereotypeApplication(clazz.getAppliedStereotype(CONTRACT));
				}
			}
		}
		
		
		//fill claims, artefact, agreement list
		if(contractStereo != null){
			
			//Map<GenericTraceLinkImpl, Claim> map = Utils.getTracedClaim(contractStereo);
			claimViewer.setInput(Utils.getTracedClaims(contractStereo));
			
			supportedByArtefactViewer.setInput(Utils.getTracedArtefacts(contractStereo));
			
			agreementViewer.setInput(Utils.getTracedAgreements(contractStereo));
		
		}
		
		claimDropAdapter.setContract(contractStereo);
		artefactDropAdapter.setContract(contractStereo);
		agreementDropAdapter.setContract(contractStereo);

	}
	

//	private void setContractPropertyTab(Class clazz) {
//		contract = clazz;
//
//		Stereotype contrStereo = clazz.getAppliedStereotype(CONTRACT);
//				
//		Contract contractStereo = (Contract) contract.getStereotypeApplication(contrStereo);
//		
//		claimViewer.setInput(contractStereo.getClaim());
//		supportedByArtefactViewer.setInput(contractStereo.getSupportedBy());
//		
//		claimDropAdapter.setContractClass(clazz);
//		artefactDropAdapter.setContractClass(clazz);
//	}

	private void cleanPropertyTab() {

		contractStereo = null;
		claimViewer.setInput(null);
		supportedByArtefactViewer.setInput(null);
		agreementViewer.setInput(null);
		claimDropAdapter.setContract(null);
		artefactDropAdapter.setContract(null);
		agreementDropAdapter.setContract(null);
	}

	
	// create the columns for the claim table
    private void createClaimColumns(final Composite parent, final TableViewer viewer) {
            String[] titles = { "Name", "Argumentation Model"};
            int[] bounds = { 400, 200};

            // first column is for the claim description
            TableViewerColumn col = createTableViewerColumn(viewer, titles[0], bounds[0], 0);
            col.setLabelProvider(new ColumnLabelProvider() {
                    @Override
                    public String getText(Object element) {
                            Claim claim = (Claim) element;
                            return claim.getId()+" "+claim.getName();
                    }
            });
            
            // second column is for the claim resource
            col = createTableViewerColumn(viewer, titles[1], bounds[1], 1);
            col.setLabelProvider(new ColumnLabelProvider() {
                    @Override
                    public String getText(Object element) {
                            ClaimImpl claim = (ClaimImpl) element;
                            return UIutils.getPathStringFor(claim);
                    }
            });

    }

    private TableViewerColumn createTableViewerColumn(TableViewer viewer, String title, int bound, final int colNumber) {
            final TableViewerColumn viewerColumn = new TableViewerColumn(viewer,
                            SWT.NONE);
            final TableColumn column = viewerColumn.getColumn();
            column.setText(title);
            column.setWidth(bound);
            column.setResizable(true);
            column.setMoveable(true);
            return viewerColumn;
    }
    
 // create the columns for the artefact table
    //TODO to be factorized together with the claim...
    private void createArtefactColumns(final Composite parent, final TableViewer viewer) {
            String[] titles = { "Name", "Artefact Model"};
            int[] bounds = { 400, 200};

            // first column is for the artefact description
            TableViewerColumn col = createTableViewerColumn(viewer, titles[0], bounds[0], 0);
  
            col.setLabelProvider(new ColumnLabelProvider() {
                    @Override
                    public String getText(Object element) {
                            Artefact artefact = (Artefact) element;
                            return artefact.getId()+" "+artefact.getName();
                    }
            });
            
         // second column is for the artefact resource
            col = createTableViewerColumn(viewer, titles[1], bounds[1], 1);
            col.setLabelProvider(new ColumnLabelProvider() {
                    @Override
                    public String getText(Object element) {
                    		Artefact artefact = (Artefact) element;
                            return UIutils.getPathStringFor(artefact);
                    }
            });

    }
    
    
    
    //creates the columns for the artefact table
    //TODO to be factorized together with the claim...
    private void createAgreementColumns(final Composite parent, final TableViewer viewer) {
            String[] titles = { "Name", "Argumentation Model"};
            int[] bounds = { 400, 200};

            // first column is for the artefact description
            TableViewerColumn col = createTableViewerColumn(viewer, titles[0], bounds[0], 0);
  
            col.setLabelProvider(new ColumnLabelProvider() {
                    @Override
                    public String getText(Object element) {
                    	Agreement agree = (Agreement) element;
                            return agree.getId()+" "+agree.getName();
                    }
            });
            
         // second column is for the artefact resource
            col = createTableViewerColumn(viewer, titles[1], bounds[1], 1);
            col.setLabelProvider(new ColumnLabelProvider() {
                    @Override
                    public String getText(Object element) {
                    	Agreement agree = (Agreement) element;
                            return UIutils.getPathStringFor(agree);
                    }
            });

    }
    
    
    private class ClickListener implements IDoubleClickListener {
		@Override
		public void doubleClick(DoubleClickEvent event) {
			
			EObject sel = (EObject) ((StructuredSelection) event.getSelection()).getFirstElement();
			UIutils.selectCDOObjectInProjectExplorer(sel);	
		}
	}
}
