/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.properties.property.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Artefact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getConstrainingRequirement <em>Constraining Requirement</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getApplicableTechnique <em>Applicable Technique</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getProperty <em>Property</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefact()
 * @model annotation="gmf.node label='name' figure='figures.ArtefactFigure' label.icon='false' label.placement='external'"
 * @generated
 */
public interface BaseArtefact extends BaseAssurableElement, BaselineElement, DescribableElement {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' attribute.
	 * @see #setReference(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefact_Reference()
	 * @model
	 * @generated
	 */
	String getReference();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getReference <em>Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' attribute.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(String value);

	/**
	 * Returns the value of the '<em><b>Constraining Requirement</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraining Requirement</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraining Requirement</em>' reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefact_ConstrainingRequirement()
	 * @model
	 * @generated
	 */
	EList<BaseRequirement> getConstrainingRequirement();

	/**
	 * Returns the value of the '<em><b>Applicable Technique</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseTechnique}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applicable Technique</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applicable Technique</em>' reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefact_ApplicableTechnique()
	 * @model
	 * @generated
	 */
	EList<BaseTechnique> getApplicableTechnique();

	/**
	 * Returns the value of the '<em><b>Owned Rel</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Rel</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Rel</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefact_OwnedRel()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseArtefactRel> getOwnedRel();

	/**
	 * Returns the value of the '<em><b>Property</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.infra.properties.property.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefact_Property()
	 * @model
	 * @generated
	 */
	EList<Property> getProperty();

} // BaseArtefact
