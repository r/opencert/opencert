/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface RequirementApplicabilityPropertiesEditionPart {



	/**
	 * Init the applicability
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initApplicability(ReferencesTableSettings settings);

	/**
	 * Update the applicability
	 * @param newValue the applicability to update
	 * 
	 */
	public void updateApplicability();

	/**
	 * Adds the given filter to the applicability edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToApplicability(ViewerFilter filter);

	/**
	 * Adds the given filter to the applicability edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToApplicability(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the applicability table
	 * 
	 */
	public boolean isContainedInApplicabilityTable(EObject element);




	/**
	 * Init the applicabilityTable
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initApplicabilityTable(ReferencesTableSettings settings);

	/**
	 * Update the applicabilityTable
	 * @param newValue the applicabilityTable to update
	 * 
	 */
	public void updateApplicabilityTable();

	/**
	 * Adds the given filter to the applicabilityTable edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToApplicabilityTable(ViewerFilter filter);

	/**
	 * Adds the given filter to the applicabilityTable edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToApplicabilityTable(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the applicabilityTable table
	 * 
	 */
	public boolean isContainedInApplicabilityTableTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
