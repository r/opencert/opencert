/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;

import org.eclipse.opencert.pam.procspec.process.providers.ProcessMessages;

// End of user code

/**
 * 
 * 
 */
public class ProcessModelPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ProcessModelPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected ReferencesTable ownedActivity;
	protected List<ViewerFilter> ownedActivityBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedActivityFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedParticipant;
	protected List<ViewerFilter> ownedParticipantBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedParticipantFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedTechnique;
	protected List<ViewerFilter> ownedTechniqueBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedTechniqueFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public ProcessModelPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ProcessModelPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence processModelStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = processModelStep.addStep(ProcessViewsRepository.ProcessModel.Properties.class);
		propertiesStep.addStep(ProcessViewsRepository.ProcessModel.Properties.id);
		propertiesStep.addStep(ProcessViewsRepository.ProcessModel.Properties.name);
		propertiesStep.addStep(ProcessViewsRepository.ProcessModel.Properties.description);
		propertiesStep.addStep(ProcessViewsRepository.ProcessModel.Properties.ownedActivity);
		propertiesStep.addStep(ProcessViewsRepository.ProcessModel.Properties.ownedParticipant);
		propertiesStep.addStep(ProcessViewsRepository.ProcessModel.Properties.ownedTechnique);
		
		
		composer = new PartComposer(processModelStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ProcessViewsRepository.ProcessModel.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.ProcessModel.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.ProcessModel.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.ProcessModel.Properties.description) {
					return createDescriptionTextarea(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.ProcessModel.Properties.ownedActivity) {
					return createOwnedActivityTableComposition(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.ProcessModel.Properties.ownedParticipant) {
					return createOwnedParticipantTableComposition(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.ProcessModel.Properties.ownedTechnique) {
					return createOwnedTechniqueTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(ProcessMessages.ProcessModelPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.ProcessModel.Properties.id, ProcessMessages.ProcessModelPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ProcessModelPropertiesEditionPartForm.this,
							ProcessViewsRepository.ProcessModel.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ProcessModelPropertiesEditionPartForm.this,
									ProcessViewsRepository.ProcessModel.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ProcessModelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, ProcessViewsRepository.ProcessModel.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ProcessModel.Properties.id, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.ProcessModel.Properties.name, ProcessMessages.ProcessModelPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ProcessModelPropertiesEditionPartForm.this,
							ProcessViewsRepository.ProcessModel.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ProcessModelPropertiesEditionPartForm.this,
									ProcessViewsRepository.ProcessModel.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ProcessModelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, ProcessViewsRepository.ProcessModel.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ProcessModel.Properties.name, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionTextarea(FormToolkit widgetFactory, Composite parent) {
		Label descriptionLabel = createDescription(parent, ProcessViewsRepository.ProcessModel.Properties.description, ProcessMessages.ProcessModelPropertiesEditionPart_DescriptionLabel);
		GridData descriptionLabelData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionLabelData.horizontalSpan = 3;
		descriptionLabel.setLayoutData(descriptionLabelData);
		description = widgetFactory.createText(parent, "", SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL); //$NON-NLS-1$
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionData.horizontalSpan = 2;
		descriptionData.heightHint = 80;
		descriptionData.widthHint = 200;
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ProcessModelPropertiesEditionPartForm.this,
							ProcessViewsRepository.ProcessModel.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ProcessModelPropertiesEditionPartForm.this,
									ProcessViewsRepository.ProcessModel.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ProcessModelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		EditingUtils.setID(description, ProcessViewsRepository.ProcessModel.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Textarea"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ProcessModel.Properties.description, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionTextArea

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedActivityTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedActivity = new ReferencesTable(getDescription(ProcessViewsRepository.ProcessModel.Properties.ownedActivity, ProcessMessages.ProcessModelPropertiesEditionPart_OwnedActivityLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedActivity.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedActivity.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedActivity.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedActivity.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedActivityFilters) {
			this.ownedActivity.addFilter(filter);
		}
		this.ownedActivity.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ProcessModel.Properties.ownedActivity, ProcessViewsRepository.FORM_KIND));
		this.ownedActivity.createControls(parent, widgetFactory);
		this.ownedActivity.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedActivity, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedActivityData = new GridData(GridData.FILL_HORIZONTAL);
		ownedActivityData.horizontalSpan = 3;
		this.ownedActivity.setLayoutData(ownedActivityData);
		this.ownedActivity.setLowerBound(0);
		this.ownedActivity.setUpperBound(-1);
		ownedActivity.setID(ProcessViewsRepository.ProcessModel.Properties.ownedActivity);
		ownedActivity.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedActivityTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedParticipantTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedParticipant = new ReferencesTable(getDescription(ProcessViewsRepository.ProcessModel.Properties.ownedParticipant, ProcessMessages.ProcessModelPropertiesEditionPart_OwnedParticipantLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedParticipant, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedParticipant.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedParticipant, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedParticipant.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedParticipant, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedParticipant.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedParticipant, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedParticipant.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedParticipantFilters) {
			this.ownedParticipant.addFilter(filter);
		}
		this.ownedParticipant.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ProcessModel.Properties.ownedParticipant, ProcessViewsRepository.FORM_KIND));
		this.ownedParticipant.createControls(parent, widgetFactory);
		this.ownedParticipant.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedParticipant, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedParticipantData = new GridData(GridData.FILL_HORIZONTAL);
		ownedParticipantData.horizontalSpan = 3;
		this.ownedParticipant.setLayoutData(ownedParticipantData);
		this.ownedParticipant.setLowerBound(0);
		this.ownedParticipant.setUpperBound(-1);
		ownedParticipant.setID(ProcessViewsRepository.ProcessModel.Properties.ownedParticipant);
		ownedParticipant.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedParticipantTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedTechniqueTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedTechnique = new ReferencesTable(getDescription(ProcessViewsRepository.ProcessModel.Properties.ownedTechnique, ProcessMessages.ProcessModelPropertiesEditionPart_OwnedTechniqueLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedTechnique.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedTechnique.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedTechnique.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedTechnique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedTechnique.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedTechniqueFilters) {
			this.ownedTechnique.addFilter(filter);
		}
		this.ownedTechnique.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ProcessModel.Properties.ownedTechnique, ProcessViewsRepository.FORM_KIND));
		this.ownedTechnique.createControls(parent, widgetFactory);
		this.ownedTechnique.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProcessModelPropertiesEditionPartForm.this, ProcessViewsRepository.ProcessModel.Properties.ownedTechnique, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedTechniqueData = new GridData(GridData.FILL_HORIZONTAL);
		ownedTechniqueData.horizontalSpan = 3;
		this.ownedTechnique.setLayoutData(ownedTechniqueData);
		this.ownedTechnique.setLowerBound(0);
		this.ownedTechnique.setUpperBound(-1);
		ownedTechnique.setID(ProcessViewsRepository.ProcessModel.Properties.ownedTechnique);
		ownedTechnique.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedTechniqueTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ProcessModel.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(ProcessMessages.ProcessModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ProcessModel.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(ProcessMessages.ProcessModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ProcessModel.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setBackground(description.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			description.setToolTipText(ProcessMessages.ProcessModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#initOwnedActivity(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedActivity(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedActivity.setContentProvider(contentProvider);
		ownedActivity.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ProcessModel.Properties.ownedActivity);
		if (eefElementEditorReadOnlyState && ownedActivity.isEnabled()) {
			ownedActivity.setEnabled(false);
			ownedActivity.setToolTipText(ProcessMessages.ProcessModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedActivity.isEnabled()) {
			ownedActivity.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#updateOwnedActivity()
	 * 
	 */
	public void updateOwnedActivity() {
	ownedActivity.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#addFilterOwnedActivity(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedActivity(ViewerFilter filter) {
		ownedActivityFilters.add(filter);
		if (this.ownedActivity != null) {
			this.ownedActivity.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#addBusinessFilterOwnedActivity(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedActivity(ViewerFilter filter) {
		ownedActivityBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#isContainedInOwnedActivityTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedActivityTable(EObject element) {
		return ((ReferencesTableSettings)ownedActivity.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#initOwnedParticipant(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedParticipant(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedParticipant.setContentProvider(contentProvider);
		ownedParticipant.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ProcessModel.Properties.ownedParticipant);
		if (eefElementEditorReadOnlyState && ownedParticipant.isEnabled()) {
			ownedParticipant.setEnabled(false);
			ownedParticipant.setToolTipText(ProcessMessages.ProcessModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedParticipant.isEnabled()) {
			ownedParticipant.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#updateOwnedParticipant()
	 * 
	 */
	public void updateOwnedParticipant() {
	ownedParticipant.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#addFilterOwnedParticipant(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedParticipant(ViewerFilter filter) {
		ownedParticipantFilters.add(filter);
		if (this.ownedParticipant != null) {
			this.ownedParticipant.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#addBusinessFilterOwnedParticipant(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedParticipant(ViewerFilter filter) {
		ownedParticipantBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#isContainedInOwnedParticipantTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedParticipantTable(EObject element) {
		return ((ReferencesTableSettings)ownedParticipant.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#initOwnedTechnique(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedTechnique(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedTechnique.setContentProvider(contentProvider);
		ownedTechnique.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ProcessModel.Properties.ownedTechnique);
		if (eefElementEditorReadOnlyState && ownedTechnique.isEnabled()) {
			ownedTechnique.setEnabled(false);
			ownedTechnique.setToolTipText(ProcessMessages.ProcessModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedTechnique.isEnabled()) {
			ownedTechnique.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#updateOwnedTechnique()
	 * 
	 */
	public void updateOwnedTechnique() {
	ownedTechnique.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#addFilterOwnedTechnique(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedTechnique(ViewerFilter filter) {
		ownedTechniqueFilters.add(filter);
		if (this.ownedTechnique != null) {
			this.ownedTechnique.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#addBusinessFilterOwnedTechnique(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedTechnique(ViewerFilter filter) {
		ownedTechniqueBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ProcessModelPropertiesEditionPart#isContainedInOwnedTechniqueTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedTechniqueTable(EObject element) {
		return ((ReferencesTableSettings)ownedTechnique.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ProcessMessages.ProcessModel_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
