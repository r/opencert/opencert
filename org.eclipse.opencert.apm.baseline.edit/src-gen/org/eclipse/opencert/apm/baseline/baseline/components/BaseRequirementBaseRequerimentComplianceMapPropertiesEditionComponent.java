/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class BaseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASEREQUERIMENTCOMPLIANCEMAP_PART = "BaseRequerimentComplianceMap"; //$NON-NLS-1$

	
	/**
	 * Settings for complianceMap ReferencesTable
	 */
	protected ReferencesTableSettings complianceMapSettings;
	
	/**
	 * Settings for requirementComplianceMap ReferencesTable
	 */
	protected ReferencesTableSettings requirementComplianceMapSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaseRequirementBaseRequerimentComplianceMapPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseRequirement, String editing_mode) {
		super(editingContext, baseRequirement, editing_mode);
		parts = new String[] { BASEREQUERIMENTCOMPLIANCEMAP_PART };
		repositoryKey = BaselineViewsRepository.class;
		partKey = BaselineViewsRepository.BaseRequerimentComplianceMap.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaseRequirement baseRequirement = (BaseRequirement)elt;
			final BaseRequerimentComplianceMapPropertiesEditionPart baseRequerimentComplianceMapPart = (BaseRequerimentComplianceMapPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap)) {
				complianceMapSettings = new ReferencesTableSettings(baseRequirement, BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap());
				baseRequerimentComplianceMapPart.initComplianceMap(complianceMapSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable)) {
				requirementComplianceMapSettings = new ReferencesTableSettings(baseRequirement, BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap());
				baseRequerimentComplianceMapPart.initRequirementComplianceMapTable(requirementComplianceMapSettings);
			}
			// init filters
			if (isAccessible(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap)) {
				baseRequerimentComplianceMapPart.addFilterToComplianceMap(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseComplianceMap); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for complianceMap
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable)) {
				baseRequerimentComplianceMapPart.addFilterToRequirementComplianceMapTable(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseComplianceMap); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for requirementComplianceMap
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}





	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap) {
			return BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap();
		}
		if (editorKey == BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable) {
			return BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaseRequirement baseRequirement = (BaseRequirement)semanticObject;
		if (BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, complianceMapSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				complianceMapSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				complianceMapSettings.move(event.getNewIndex(), (BaseComplianceMap) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, requirementComplianceMapSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				requirementComplianceMapSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				requirementComplianceMapSettings.move(event.getNewIndex(), (BaseComplianceMap) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaseRequerimentComplianceMapPropertiesEditionPart baseRequerimentComplianceMapPart = (BaseRequerimentComplianceMapPropertiesEditionPart)editingPart;
			if (BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap))
				baseRequerimentComplianceMapPart.updateComplianceMap();
			if (BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable))
				baseRequerimentComplianceMapPart.updateRequirementComplianceMapTable();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap(),
			BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
