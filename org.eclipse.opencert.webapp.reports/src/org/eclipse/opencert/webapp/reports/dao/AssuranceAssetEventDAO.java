/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.executors.HttpRequestDrivenInTransactionExecutor;
import org.springframework.stereotype.Component;

@Component
public class AssuranceAssetEventDAO {

	public List<AssuranceAssetEvent> getEvaluationAssetEvent(final long evaluationUid) 
	{
        final List<AssuranceAssetEvent> result = new ArrayList<AssuranceAssetEvent>();
        
        new HttpRequestDrivenInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                String assetEventSQL = "select * from assuranceasset_assuranceassetevent where resultingevaluation = '"
                		+ evaluationUid + "' and type = '2' and "
                		+ CDOStorageUtil.getMandatoryCDOQuerySuffix(false);
                CDOQuery projectsQuery = cdoTransaction.createQuery("sql", assetEventSQL);
                
                result.addAll(projectsQuery.getResult(AssuranceAssetEvent.class));         
            }
        }.executeReadOnlyOperation();
        
        return result;
	}

    public void setDateToNow(Long eventCdoId)
    {
        new HttpRequestDrivenInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                String assetEventSQL = "select * from assuranceasset_assuranceassetevent where cdo_id = "
                        + eventCdoId + " and "
                        + CDOStorageUtil.getMandatoryCDOQuerySuffix(false);
                CDOQuery projectsQuery = cdoTransaction.createQuery("sql", assetEventSQL);
                
                AssuranceAssetEvent resultValue = projectsQuery.getResultValue(AssuranceAssetEvent.class);
                resultValue.setTime(new Date());
            }
        }.executeReadWriteOperation();
    }
}
