/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;
import org.eclipse.opencert.pam.procspec.process.parts.ActivityAssuranceAssetPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class ActivityActivityAssuranceAssetPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String ACTIVITYASSURANCEASSET_PART = "Activity AssuranceAsset"; //$NON-NLS-1$

	
	/**
	 * Settings for assetEvent ReferencesTable
	 */
	private ReferencesTableSettings assetEventSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ActivityActivityAssuranceAssetPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject activity, String editing_mode) {
		super(editingContext, activity, editing_mode);
		parts = new String[] { ACTIVITYASSURANCEASSET_PART };
		repositoryKey = ProcessViewsRepository.class;
		partKey = ProcessViewsRepository.ActivityAssuranceAsset.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Activity activity = (Activity)elt;
			final ActivityAssuranceAssetPropertiesEditionPart activityAssuranceAssetPart = (ActivityAssuranceAssetPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent)) {
				assetEventSettings = new ReferencesTableSettings(activity, ProcessPackage.eINSTANCE.getActivity_AssetEvent());
				activityAssuranceAssetPart.initAssetEvent(assetEventSettings);
			}
			// init filters
			if (isAccessible(ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent)) {
				activityAssuranceAssetPart.addFilterToAssetEvent(new EObjectFilter(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVENT));
				// Start of user code for additional businessfilters for assetEvent
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}




	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent) {
			return ProcessPackage.eINSTANCE.getActivity_AssetEvent();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Activity activity = (Activity)semanticObject;
		if (ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof AssuranceAssetEvent) {
					assetEventSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				assetEventSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				assetEventSettings.move(event.getNewIndex(), (AssuranceAssetEvent) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ActivityAssuranceAssetPropertiesEditionPart activityAssuranceAssetPart = (ActivityAssuranceAssetPropertiesEditionPart)editingPart;
			if (ProcessPackage.eINSTANCE.getActivity_AssetEvent().equals(msg.getFeature())  && isAccessible(ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent))
				activityAssuranceAssetPart.updateAssetEvent();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			ProcessPackage.eINSTANCE.getActivity_AssetEvent()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
