/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;
import org.eclipse.opencert.gsn.figures.GSNStrategy;
import org.eclipse.opencert.sam.arg.arg.ArgumentReasoning;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.ArgumentReasoningItemSemanticEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;
import org.eclipse.opencert.userguidance.label.WrappingLabel2;

/**
 * @generated
 */
public class ArgumentReasoningEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2003;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public ArgumentReasoningEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new ArgumentReasoningItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated NOT
	 */
	protected IFigure createNodeShape() {
		// Start MCP
		//return primaryShape = new ArgumentReasoningFigure();
		if (!(((View) this.getModel()).getElement() instanceof ArgumentReasoning)) {
			System.out.println("createNodeShape(): Error model not allowed");
			return primaryShape = new ArgumentReasoningFigure();
		}
		ArgumentReasoning newElem = (ArgumentReasoning) ((Node) this.getModel())
				.getElement();
		primaryShape = new ArgumentReasoningFigure();
		if (newElem.getToBeInstantiated() == true
				&& newElem.getToBeSupported() == true) {
			((ArgumentReasoningFigure) (primaryShape)).setShape(3);
		} else if (newElem.getToBeInstantiated() == true) {
			((ArgumentReasoningFigure) (primaryShape)).setShape(2);
		} else if (newElem.getToBeSupported() == true) {
			((ArgumentReasoningFigure) (primaryShape)).setShape(1);
		} else {
			((ArgumentReasoningFigure) (primaryShape)).setShape(0);
		}

		return primaryShape;
		// End MCP
	}

	/**
	 * @generated
	 */
	public ArgumentReasoningFigure getPrimaryShape() {
		return (ArgumentReasoningFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof ArgumentReasoningIdEditPart) {
			((ArgumentReasoningIdEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureArgumentReasoningLabelFigure());
			return true;
		}
		if (childEditPart instanceof ArgumentReasoningDescriptionEditPart) {
			((ArgumentReasoningDescriptionEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureArgumentationElementDescriptionLabelFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof ArgumentReasoningIdEditPart) {
			return true;
		}
		if (childEditPart instanceof ArgumentReasoningDescriptionEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(120, 80);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(ArgVisualIDRegistry
				.getType(ArgumentReasoningIdEditPart.VISUAL_ID));
	}

	/**
	 * @generated NOT
	 */
	protected void handleNotificationEvent(Notification event) {
		// Start MCP
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {
			if (event.getNotifier() instanceof ArgumentReasoning) {
				ArgumentReasoning elem = (ArgumentReasoning) ((Node) this
						.getModel()).getElement();
				ArgumentReasoningFigure figure = (ArgumentReasoningFigure) this
						.getPrimaryShape();

				if (elem.getToBeInstantiated() == true
						&& elem.getToBeSupported() == true) {
					((ArgumentReasoningFigure) (primaryShape)).setShape(3);
				} else if (elem.getToBeInstantiated() == true) {
					((ArgumentReasoningFigure) (primaryShape)).setShape(2);
				} else if (elem.getToBeSupported() == true) {
					((ArgumentReasoningFigure) (primaryShape)).setShape(1);
				} else {
					((ArgumentReasoningFigure) (primaryShape)).setShape(0);
				}
			}

			super.handleNotificationEvent(event);
		}
		// End MCP
	}

	/**
	 * @generated
	 */
	public class ArgumentReasoningFigure extends GSNStrategy {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureArgumentReasoningLabelFigure;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureArgumentationElementDescriptionLabelFigure;

		/**
		 * @generated
		 */
		public ArgumentReasoningFigure() {
			this.setPreferredSize(new Dimension(getMapMode().DPtoLP(120),
					getMapMode().DPtoLP(80)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureArgumentReasoningLabelFigure = new WrappingLabel();

			fFigureArgumentReasoningLabelFigure.setText("Strategy");

			fFigureArgumentReasoningLabelFigure
					.setFont(FFIGUREARGUMENTREASONINGLABELFIGURE_FONT);

			this.add(fFigureArgumentReasoningLabelFigure);

			fFigureArgumentationElementDescriptionLabelFigure = new WrappingLabel2();
			//fFigureArgumentationElementDescriptionLabelFigure = new WrappingLabel();

			fFigureArgumentationElementDescriptionLabelFigure.setText("");

			fFigureArgumentationElementDescriptionLabelFigure
					.setFont(FFIGUREARGUMENTATIONELEMENTDESCRIPTIONLABELFIGURE_FONT);

			this.add(fFigureArgumentationElementDescriptionLabelFigure);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureArgumentReasoningLabelFigure() {
			return fFigureArgumentReasoningLabelFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureArgumentationElementDescriptionLabelFigure() {
			return fFigureArgumentationElementDescriptionLabelFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Font FFIGUREARGUMENTREASONINGLABELFIGURE_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

	/**
	 * @generated
	 */
	static final Font FFIGUREARGUMENTATIONELEMENTDESCRIPTIONLABELFIGURE_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

}
