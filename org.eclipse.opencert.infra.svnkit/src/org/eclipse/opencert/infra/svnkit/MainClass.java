/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation and KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza(Tecnalia) - initial API and implementation
 *   Alejandra Ru�z(Tecnalia) - initial API and implementation
 *   Idoya Del R�o(Tecnalia) - initial API and implementation
 *   Mari Carmen Palacios(Tecnalia) - initial API and implementation
 *   Angel L�pez(Tecnalia) - initial API and implementation
 *   Jan Mauersberger(KPIT)- Improvements implementation
 *   Sascha Baumgart(KPIT)- Improvements implementation
 *******************************************************************************/

package org.eclipse.opencert.infra.svnkit;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.ISVNEditor;

import org.tmatesoft.svn.core.io.SVNRepository;

import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.io.diff.SVNDeltaGenerator;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.*;

public class MainClass {


	//private static final String LOCAL_REPO_PROTOCOL = "file:///";
	private static final int EXCEPTION = 3;
	private static final int URL_NOT_FILE = 2;
	private static final int INCORRECT_URL = 1;
	private static final int NO_ERROR = 0;	

	private SVNRepository repository=null;


	public MainClass() {		
	}

	public void deleteFile(String filePath) {

		try {
			ISVNEditor editor = repository.getCommitEditor( "Delete file" , null );
			editor.openRoot(-1); 
			editor.deleteEntry(filePath, -1);														
			editor.closeEdit();
		} catch (SVNException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  

	}


	public int connectRepository(String urlCon,String name, String password){
		//boolean useLocalRepository = PlatformUI.getPreferenceStore().getBoolean(SVNInfo_REPOSITORY_TYPE);

		//String name ="";
		//String password ="";



		//if(useLocalRepository){
		//	urlCon = LOCAL_REPO_PROTOCOL + urlCon;
		//}

		/*
		 * initializes the library (it must be done before ever using the
		 * library itself)
		 */
		setupLibrary();

		try {

			repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(urlCon));
		} catch (SVNException svne) {
			/*
			 * Perhaps a malformed URL is the cause of this exception
			 */
			System.err.println("error while creating an SVNRepository for location '"
					+ urlCon + "': " + svne.getMessage());
			return EXCEPTION;
		}

		ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(name, password);
		repository.setAuthenticationManager(authManager);									


		return NO_ERROR;
	}

	public int checkPath(String urlCon){

		/*
		 * Checks up if the specified path/to/repository part of the URL
		 * really corresponds to a directory. If doesn't the program exits.
		 * SVNNodeKind is that one who says what is located at a path in a
		 * revision. -1 means the latest revision.
		 */			
		try {
			SVNNodeKind nodeKind = repository.checkPath("", -1);
			if (nodeKind == SVNNodeKind.NONE) {
				System.err.println("There is no entry at '" + urlCon + "'.");
				return INCORRECT_URL;
			} else if (nodeKind == SVNNodeKind.FILE) {
				System.err.println("The entry at '" + urlCon + "' is a file while a directory was expected.");
				return URL_NOT_FILE;
			}
			return NO_ERROR;
		} catch (SVNException e) {
			// TODO Auto-generated catch block
			return EXCEPTION;
		}

	}


	public boolean existsFileRepo(String localPath){

		// paranoia
		if (repository == null) {
			return false;
		}

		//Read the file Bytes
		//File file = new File(localPath);
		//String name=file.getName();

		try {
			SVNNodeKind nodeKind = repository.checkPath(localPath, -1);
			if (nodeKind == SVNNodeKind.FILE) {				
				return true;
			} else {				
				return false;
			}

		} catch (SVNException e) {
			// TODO Auto-generated catch block
			return true;
		}

	}

	public boolean existsFolderRepo(String localPath){

		//Read the file Bytes
		//File file = new File(localPath);
		//String name=file.getName();

		try {
			SVNNodeKind nodeKind = repository.checkPath(localPath, -1);
			if (nodeKind == SVNNodeKind.DIR) {				
				return true;
			} else {				
				return false;
			}

		} catch (SVNException e) {
			// TODO Auto-generated catch block
			return false;
		}

	}

	public  ArrayList<String> getFileHistory(String filePath) {
		//Get file de history
		ArrayList<String> entriesList=new ArrayList<String>();
		try {
			if(existsFileRepo(filePath)){
				Collection logEntries = repository.log(new String[] {filePath}, null,0,-1, true, true);

				for (Iterator entries = logEntries.iterator(); entries.hasNext();) {
					/*
					 * gets a next SVNLogEntry
					 */
					SVNLogEntry logEntry = (SVNLogEntry) entries.next();
					DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
					String historyRev=logEntry.getRevision() + "#" + df.format(logEntry.getDate())+ "#" + logEntry.getAuthor() + "#" +logEntry.getMessage();
					entriesList.add(historyRev);			
				}			
			}

		} catch (SVNException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return entriesList;

	}

	public String getFileFromRepo(String filePath) {
		///Get File From Repository

		try {
			if(existsFileRepo(filePath)){
				SVNProperties fileProperties = new SVNProperties();

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				repository.getFile(filePath, -1, fileProperties, baos);

				String tempDir =System.getProperty("java.io.tmpdir");
				String filename= filePath.substring(filePath.lastIndexOf("/")+1);
				String fullPath = tempDir + filename;

				OutputStream outputStream = new FileOutputStream (fullPath); 
				baos.writeTo(outputStream);
				baos.flush();
				outputStream.close();

				return fullPath;
			}
			return "";

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}    

	}

	public String copyFileFromRepo(String sourceURI, String targetFolderURI, String comment) {
		String tempFile = getFileFromRepo(sourceURI);
		if(tempFile.equals("exists")){
			return tempFile;
		}
		String finalLocation=  addFileToRepo(tempFile, targetFolderURI,comment); 

		return finalLocation;
	}


	public String updateFileFromRepo(String sourceURI, String targetFolderURI, String comment) {
		String tempFile = getFileFromRepo(sourceURI);
		if(tempFile.equals("exists")){
			return tempFile;
		}
		String finalLocation=  changeRepoFile(tempFile, targetFolderURI,comment); 

		return finalLocation;
	}

	public String changeRepoFile(String localPath,String destinationDir, String comment)  {
		ISVNEditor editor = null;
		try {	
			editor = repository.getCommitEditor( comment , null );

			//Read the file Bytes
			File file = new File(localPath);
			byte [] contentsNew = new byte[(int)file.length()];
			DataInputStream dis = new DataInputStream(new FileInputStream(file));
			dis.readFully(contentsNew);
			dis.close();
			String remotePath=destinationDir + "/" + file.getName();

			editor.openRoot(-1);
			//editor.openDir(destinationDir, -1);
			editor.openFile(remotePath, -1);
			editor.applyTextDelta(remotePath, null);

			SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
			String     checksum = deltaGenerator.sendDelta(remotePath, new ByteArrayInputStream(new byte[0]) , 0, new ByteArrayInputStream(contentsNew), editor, true);

			/*
			 * Closes the new added file.
			 */
			editor.closeFile(remotePath, checksum);
			editor.closeDir();

			editor.closeEdit();

			return remotePath;


		} catch ( Exception svne ) {
			svne.printStackTrace();
			try {
				editor.abortEdit( );
			} catch (SVNException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return  "An error has occurred updating the file to the Repository";
		}

	} 

	public String addFolderToRepo(String destinationDir, String newFolderName, String comment) {


		ISVNEditor editor = null;
		try {									

			editor = repository.getCommitEditor(comment , null );			

			editor.openRoot(-1);
			editor.openDir(destinationDir, -1);
			//final long latestRevision = repository.getLatestRevision();
			editor.addDir(newFolderName,null, -1);		

			editor.closeDir();

			//Suuuper importante, sino se cierra no se genera nada en el Repo
			editor.closeEdit();

			//Erase the first / character
			String returnString = destinationDir + "/" + newFolderName;
			if(returnString.startsWith("/")){
				returnString= returnString.substring(1);
			}
			return returnString;
		} catch ( Exception svne ) {
			svne.printStackTrace();
			try {
				editor.abortEdit( );
			} catch (SVNException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "An error has occurred adding the folder to the Repository";
		}
	}

	public String addFileToRepo(String localPath, String destinationDir, String comment) {

		ISVNEditor editor = null;
		try {	
			File file = new File(localPath);
			String remotePath= file.getName();

			if(existsFileRepo(destinationDir + "/" + remotePath)){
				return "exists";
			}

			editor = repository.getCommitEditor( comment , null );

			//Read the file Bytes
			//File file = new File(localPath);
			byte [] fileData = new byte[(int)file.length()];
			DataInputStream dis = new DataInputStream(new FileInputStream(file));
			dis.readFully(fileData);
			dis.close();
			//String remotePath= file.getName();

			//byte[] contents = "This is a new file".getBytes();

			editor.openRoot(-1);
			editor.openDir(destinationDir, -1);
			//editor.addDir(dir,null, -1);
			editor.addFile( remotePath, null, -1);
			editor.applyTextDelta(remotePath, null);

			SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
			String checksum = deltaGenerator.sendDelta(remotePath, new ByteArrayInputStream(fileData), editor, true);

			/*
			 * Closes the new added file.
			 */
			editor.closeFile(remotePath, checksum);

			editor.closeDir();


			//Suuuper importante, sino se cierra no se genera nada en el Repo
			editor.closeEdit();

			//Erase the first / character
			return destinationDir + "/" + remotePath;
		} catch ( Exception svne ) {
			svne.printStackTrace();
			try {
				editor.abortEdit( );
			} catch (SVNException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "An error has occurred adding the file to the Repository";
		}
	}

	/*
	 * Called recursively to obtain all entries that make up the repository tree
	 * repository - an SVNRepository which interface is used to carry out the
	 * request, in this case it's a request to get all entries in the directory
	 * located at the path parameter;
	 * 
	 * path is a directory path relative to the repository location path (that
	 * is a part of the URL used to create an SVNRepository instance);
	 *  
	 */
	public  ArrayList<SVNDirEntry> listEntries( String path, boolean onlyFolders)			 {
		ArrayList<SVNDirEntry> entriesList=new ArrayList<SVNDirEntry>();
		// paranoia: no repository yet
		if (repository == null) {
			return entriesList;
		}
		try {
			Collection entries = repository.getDir(path, -1, null,(Collection) null);
			Iterator iterator = entries.iterator();
			while (iterator.hasNext()) {
				SVNDirEntry entry = (SVNDirEntry) iterator.next();
				if (entry.getKind() == SVNNodeKind.DIR) {				
					entriesList.add(entry);
					//entriesList.add( entry.getName());
				}
				else if(!onlyFolders){
					entriesList.add(entry);
				}
			}
		} catch (SVNException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  entriesList;
	}


	/*
	 * Initializes the library to work with a repository via 
	 * different protocols.
	 */
	private void setupLibrary() {
		/*
		 * For using over http:// and https://
		 */
		DAVRepositoryFactory.setup();
		/*
		 * For using over svn:// and svn+xxx://
		 */
		SVNRepositoryFactoryImpl.setup();

		/*
		 * For using over file:///
		 */
		FSRepositoryFactory.setup();
	}

}
