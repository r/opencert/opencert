/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package claimtypes.provider.color;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.IPropertySourceProvider;
 
public class ClaimTypeColorPropertySourceProvider implements
		IPropertySourceProvider {
 
	private AdapterFactory adapterFactory;
 
	public ClaimTypeColorPropertySourceProvider(AdapterFactory adapterFactory) {
		this.adapterFactory = adapterFactory;
	}
 
	@Override
	public IPropertySource getPropertySource(Object object) {
		if (object instanceof IPropertySource) {
			return (IPropertySource) object;
		} else {
			IItemPropertySource itemPropertySource = (IItemPropertySource) (object instanceof EObject
					&& ((EObject) object).eClass() == null ? null
					: adapterFactory.adapt(object, IItemPropertySource.class));
 
			return itemPropertySource != null ? createPropertySource(object,
					itemPropertySource) : null;
		}
	}
 
	protected IPropertySource createPropertySource(Object object,
			IItemPropertySource itemPropertySource) {
		// Returns the custom property source
		return new ClaimTypeColorPropertySource(object, itemPropertySource);
	}
 
}