/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package claimtypes.provider.color;

import org.eclipse.emf.common.ui.celleditor.ExtendedDialogCellEditor;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.PlatformUI;

import claimtypes.ClaimType;

public class ClaimTypeColorPropertyDescriptor extends PropertyDescriptor {

	public ClaimTypeColorPropertyDescriptor(Object object,
			IItemPropertyDescriptor itemPropertyDescriptor) {
		super(object, itemPropertyDescriptor);
	}

	@Override
	public CellEditor createPropertyEditor(Composite parent) {
		return new ExtendedDialogCellEditor(parent, getLabelProvider()) {
			@Override
			protected Object openDialogBox(Control cellEditorWindow) {
				ClaimType claimType = (ClaimType) object;
				RGB oldColor = convertoToRGB(claimType.getColor());
				ColorDialog dialog = new ColorDialog(PlatformUI.getWorkbench()
						.getDisplay().getActiveShell());
				dialog.setText("Claim Type Color");
				dialog.setRGBs(createPresets());
				dialog.setRGB(oldColor);
				RGB newColor = dialog.open();
				String result = convertToString(newColor);
				return result;
			}

			private String convertToString(RGB newColor) {
				StringBuilder sb = new StringBuilder();
				sb.append(newColor.red);
				sb.append(',');
				sb.append(newColor.green);
				sb.append(',');
				sb.append(newColor.blue);
				String result = sb.toString();
				return result;
			}

			private RGB convertoToRGB(String s) {
				RGB result = null;
				try {
					String[] split = ((String) s).split(",");
					if (split.length == 3) {
						int red = Integer.parseInt(split[0]);
						int green = Integer.parseInt(split[1]);
						int blue = Integer.parseInt(split[2]);
						result = new RGB(red, green, blue);
					}
				} catch (Exception e) {
					// Nothing
				}
				if (result == null) {
					result = new RGB(255, 255, 255);
				}
				return result;
			}
		};
	}
	
	private RGB[] createPresets() {
		RGB[] result = new RGB[9];
		
		result[0] = new RGB(251, 180, 174);
		result[1] = new RGB(179, 205, 227);
		result[2] = new RGB(204, 235, 197);
		result[3] = new RGB(222, 203, 228);
		result[4] = new RGB(254, 217, 166);
		result[5] = new RGB(253, 253, 204);
		result[6] = new RGB(229, 216, 189);
		result[7] = new RGB(253, 218, 236);
		result[8] = new RGB(210, 210, 210);
		
		return result;
	}

}