/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.autocomplete;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.bindings.keys.ParseException;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.opencert.userguidance.autocomplete.proposalproviders.IdProposalProvider;
import org.eclipse.opencert.userguidance.autocomplete.proposalproviders.VocabularyProposalProvider;

public class AutoCompleteField2 {
	private ContentProposalAdapter2 proposalAdapter;

	public AutoCompleteField2(StyledText control,
			IControlContentAdapter controlContentAdapter, EObject eobj,
			int featureID) {
		ParentProposalProvider parentProposalProvider = new ParentProposalProvider();
		for (IContentProposalProvider child : createChildrenProposalProviders(
				eobj, featureID)) {
			parentProposalProvider.add(child);
		}

		configureControlContentAdapter(control, controlContentAdapter,
				parentProposalProvider);
	}

	public boolean isPopupOpen() {
		if (proposalAdapter != null) {
			return proposalAdapter.isProposalPopupOpen();
		}

		return false;
	}

	// For the future: Get the proposal providers from an extension point.
	private List<IContentProposalProvider> createChildrenProposalProviders(
			EObject eobj, int featureID) {
		List<IContentProposalProvider> result = new ArrayList<IContentProposalProvider>();

		result.add(new VocabularyProposalProvider(eobj, featureID));
		result.add(new IdProposalProvider(eobj, featureID));
//		result.add(new VarProposalProvider(eobj, featureID));
//		result.add(new UriProposalProvider(eobj, featureID));

		return result;
	}

	private void configureControlContentAdapter(StyledText control,
			IControlContentAdapter controlContentAdapter,
			ParentProposalProvider parentProposalProvider) {
		KeyStroke keyStroke = null;
		try {
			keyStroke = KeyStroke.getInstance("Ctrl+Space");
		} catch (ParseException e) {
		}

		proposalAdapter = new ContentProposalAdapter2(control,
				controlContentAdapter, parentProposalProvider, keyStroke, null);
		proposalAdapter.setPropagateKeys(true);
		proposalAdapter
				.setProposalAcceptanceStyle(ContentProposalAdapter.PROPOSAL_IGNORE);
		proposalAdapter.addContentProposalListener(new ContentProposalListener(
				control));
		proposalAdapter.setLabelProvider(new ProposalLabelProvider());
	}

	private class ParentProposalProvider implements IContentProposalProvider {

		private List<IContentProposalProvider> children;

		public ParentProposalProvider() {
			children = new ArrayList<IContentProposalProvider>();
		}

		public void add(IContentProposalProvider child) {
			children.add(child);
		}

		@Override
		public IContentProposal[] getProposals(String contents, int caretOffset) {
			List<IContentProposal[]> allProposals = new ArrayList<IContentProposal[]>();
			int totalProposalSize = 0;
			for (int i = 0; i < children.size(); i++) {
				IContentProposal[] childProposals = children.get(i)
						.getProposals(contents, caretOffset);
				allProposals.add(childProposals);
				totalProposalSize += childProposals.length;
			}

			IContentProposal[] result = new IContentProposal[totalProposalSize];
			for (int i = 0, j = 0; j < allProposals.size(); j++) {
				IContentProposal[] proposals = allProposals.get(j);
				for (int k = 0; k < proposals.length; k++, i++) {
					result[i] = proposals[k];
				}
			}

			return result;
		}

	}
}
