/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.administration;

import java.util.Collection;
import java.util.List;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.containers.FrameworkWrapper;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.manager.BaseArtefactManager;
import org.eclipse.opencert.webapp.reports.manager.BaselineManager;
import org.eclipse.opencert.webapp.reports.util.IdNameDesc;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.ProjectAdminPageMode;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.util.StringUtil;
import org.eclipse.opencert.webapp.reports.view.administration.ProjectBaselineArtefactTable.BaselineArtafectChangePack;
import org.eclipse.opencert.webapp.reports.view.common.AbstractBaselineFrameworkReport;
import org.eclipse.opencert.webapp.reports.view.common.ConfirmationWindow;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.common.IBaselineFrameworkChangeListener;
import org.eclipse.opencert.webapp.reports.view.common.INotificationPresenter;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;
import org.eclipse.opencert.webapp.reports.webapp.IProjectComboSelector;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;

import com.vaadin.server.Page;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class ProjectAdministrationPage
    extends AbstractBaselineFrameworkReport
    implements IBaselineFrameworkChangeListener
{
    private AssuranceProjectWrapper currentProject;

    private TextField projectNameField = new TextField("Assurance Project Name");
    private TextArea projectDescField = new TextArea("Assurance Project Description");
    private Label projectResourceLabel = new Label("Assurance Project Resource");
    private Label projectResourceValueLabel = new Label();
    private ProjectBaselineArtefactTable baselineArtefactTable;
    
    private SaveCancelButtonPanel saveCancelButtonPanel;
    
    private VerticalLayout mainPanel;

    private INotificationPresenter notificationPresenter;
    
    private ProjectAdminPageMode projectMode;

    private IProjectComboSelector projectComboSelector;

    private final static String TITLE = "Project Administration";
    public final static String PROJECT_DATA_CHANGED_MSG = "Press [Save] in order to preserve your changes or [Cancel] to revoke them.";
    public final static String NEW_PROJECT_DATA_CREATED_MSG = "Press [Create] to create a new Project or [Cancel] to revoke the changes.";
    
    public final static ChangeEffectKind THE_EDITOR_SUPPORTED_IA_EFFECT = ChangeEffectKind.MODIFY;
    
    public ProjectAdministrationPage(IProjectComboSelector projectComboSelector)
    {
        this.projectComboSelector = projectComboSelector;
    }

    @Override
    public ReportID getReportID()
    {
        return ReportID.ADMIN_PROJECT;
    }

    @Override
    public String getTitle()
    {
        return TITLE;
    }

    @Override
    public void projectChanged(AssuranceProjectWrapper newProject)
    {
        if (newProject != null) {
            currentProject = newProject;
            mainPanel.setEnabled(true);
        } else {
        	mainPanel.setEnabled(false);
        }

        notificationPresenter.clearNotification();
        super.projectChanged(newProject);
        projectNameField.setStyleName("");
    }

    @Override
    public Component getMainComponent(GUIMode guiMode)
    {
        addBaselineChangeListener(this);
        
        mainPanel = new VerticalLayout();
        mainPanel.setStyleName("ProjectAdminPage");
        
        saveCancelButtonPanel = new SaveCancelButtonPanel(); 
        notificationPresenter = saveCancelButtonPanel;
        addButtonsToSaveCancelButtonPanel(saveCancelButtonPanel, notificationPresenter);
        saveCancelButtonPanel.setStyleName("ProjectAdminPanel projectAdministrationButtonsLayout");
        mainPanel.addComponent(saveCancelButtonPanel);

        HorizontalLayout projectNameDescPanel = createProjectNameDescPanel();
        mainPanel.addComponent(projectNameDescPanel);

        baselineArtefactTable = new ProjectBaselineArtefactTable();
        baselineArtefactTable.setStyleName("ProjectAdminPanel");
        baselineArtefactTable.setNotificationPresenter(notificationPresenter);
        addBaselineChangeListener(baselineArtefactTable);
        mainPanel.addComponent(baselineArtefactTable);

        return mainPanel;
    }

    private HorizontalLayout createProjectNameDescPanel()
    {
        HorizontalLayout nameDescPanel = new HorizontalLayout();
        nameDescPanel.setStyleName("ProjectAdminPanel");
        
        HorizontalLayout centerLayout = new HorizontalLayout();
        centerLayout.setStyleName("ProjectAdminCenterPanel");
        appendNameDescFieldListeners(projectNameField, true);
        appendNameDescFieldListeners(projectDescField, false);
        
        projectNameField.setWidth("200px");
        
        projectDescField.setWidth("300px");
        projectDescField.setWordwrap(true);
        projectDescField.setStyleName("ProjectAdminProjectDesc");
        
        centerLayout.addComponent(projectNameField);
        centerLayout.addComponent(projectDescField);
        
        VerticalLayout resourceLayout = new VerticalLayout();
        resourceLayout.addComponent(projectResourceLabel);
        projectResourceValueLabel.setStyleName("projectResourceLabel");
        resourceLayout.addComponent(projectResourceValueLabel);
        
        centerLayout.addComponent(resourceLayout);

        nameDescPanel.addComponent(centerLayout);
                
        return nameDescPanel;
    }

    private void appendNameDescFieldListeners(AbstractTextField nameOrDescField, boolean isNameField)
    {
        nameOrDescField.addBlurListener(e -> 
        {
            String origValue = (isNameField ? currentProject.getName() : currentProject.getDescription());
            
            final boolean isChanged = !StringUtil.equal(origValue, nameOrDescField.getValue(), true);
            if (isChanged) {
            	if (projectMode == ProjectAdminPageMode.CREATE_PROJECT) {
            		notificationPresenter.showNotification(NEW_PROJECT_DATA_CREATED_MSG);
            	} else {
            		notificationPresenter.showNotification(PROJECT_DATA_CHANGED_MSG);
            	}
            }
        });
    }

    private SaveCancelButtonPanel addButtonsToSaveCancelButtonPanel(SaveCancelButtonPanel buttonsPanel, INotificationPresenter notifPresenter)
    {
        saveCancelButtonPanel.addDeleteAction((e) -> 
        {
        	String currentProjectName = currentProject.getName() != null ? currentProject.getName() : "";
        	createConfirmationDeleteWindow(currentProjectName);
        });

        saveCancelButtonPanel.addCancelAction((e) -> 
        {
            // refreshes the presented data
            projectChanged(currentProject);
            setProjectAdminPageMode(ProjectAdminPageMode.EDIT_PROJECT);
        });
        
        saveCancelButtonPanel.addSaveAction((e) -> 
        {
        	if (checkProjectFields()) {
	        	if (projectMode == ProjectAdminPageMode.CREATE_PROJECT) 
	        	{
	        		long newProjectID = createNewAssuranceProject();
	        		if (newProjectID > 0)
	        		{
	        			setProjectAdminPageMode(ProjectAdminPageMode.EDIT_PROJECT);
	        			
	        		    // refresh the current view
	        		    String message = projectComboSelector.reloadAndSelectProjectCombo(newProjectID);
	        		    
	        		    showCreateProjectNotification(message);
	        		    
	        		    OpencertLogger.info("New project created: \"" + getProjectName(newProjectID) + "\"");
	        		}
	        	} else if (projectMode == ProjectAdminPageMode.EDIT_PROJECT) 
	        	{
	        		updateCurrentAssuranceProject();
	        		Notification.show("The project definition has been updated successfully.", Notification.Type.HUMANIZED_MESSAGE);
	        		
	        		// refresh the current view
	        		projectComboSelector.reloadAndSelectProjectCombo(currentProject.getId()); 
	        		
	        		OpencertLogger.info("Project definition updated: \"" + currentProject.getName() + "\"");
	        	}
        	}
        });
        
        saveCancelButtonPanel.addNewAction((e) -> 
        {
        	clearProjectAdministrationPage();
        });
        
        saveCancelButtonPanel.addCloneAction((e) -> 
        {
        	cloneAssuranceProject();
        });

        return saveCancelButtonPanel;
    }
    
    private void setProjectAdminPageMode(ProjectAdminPageMode mode) {
    	if (mode == ProjectAdminPageMode.CREATE_PROJECT) {
    		projectMode = ProjectAdminPageMode.CREATE_PROJECT;
    		saveCancelButtonPanel.updateSaveButtonCaption("Create");
    		saveCancelButtonPanel.updateSaveButtonDescription(SaveCancelButtonPanel.CREATE_BUTTON_DESCRIPTION);
    		saveCancelButtonPanel.deleteButtonEnabled(false);
    		saveCancelButtonPanel.updateDeleteButtonDescription("");
    		saveCancelButtonPanel.newButtonEnabled(false);
    		saveCancelButtonPanel.updateNewButtonDescription("");
    		saveCancelButtonPanel.cloneButtonEnabled(false);
    		saveCancelButtonPanel.updateCloneButtonDescription("");
    		projectResourceLabel.setVisible(false);
    		projectResourceValueLabel.setVisible(false);
    	} else if (mode == ProjectAdminPageMode.EDIT_PROJECT) {
    		projectMode = ProjectAdminPageMode.EDIT_PROJECT;
    		saveCancelButtonPanel.updateSaveButtonCaption("Save");
    		saveCancelButtonPanel.updateSaveButtonDescription(SaveCancelButtonPanel.SAVE_BUTTON_DESCRIPTION);
    		saveCancelButtonPanel.deleteButtonEnabled(true);
    		saveCancelButtonPanel.updateDeleteButtonDescription(SaveCancelButtonPanel.DELETE_BUTTON_DESCRIPTION);
    		saveCancelButtonPanel.newButtonEnabled(true);
    		saveCancelButtonPanel.updateNewButtonDescription(SaveCancelButtonPanel.NEW_BUTTON_DESCRIPTION);
    		saveCancelButtonPanel.cloneButtonEnabled(true);
    		saveCancelButtonPanel.updateCloneButtonDescription(SaveCancelButtonPanel.CLONE_BUTTON_DESCRIPTION);
    		projectResourceLabel.setVisible(true);
    		projectResourceValueLabel.setVisible(true);
    	} else {
    		throw new IllegalArgumentException("No valid Project Administration Page mode");
    	}
    	baselineArtefactTable.setProjectMode(projectMode);
		notificationPresenter.showNotification("");
    }
    
    private void clearProjectAdministrationPage() {
    	setProjectAdminPageMode(ProjectAdminPageMode.CREATE_PROJECT);
    	projectNameField.setValue("");
    	projectDescField.setValue("");

    	baselineArtefactTable.setBaselineFrameworkName("");
    	baselineArtefactTable.setBaselineFramework(-1);
    	notificationPresenter.showNotification("");
    }
    
    private void showCreateProjectNotification(String message) 
    {
    	 Notification notification;
		    if ("".equals(message)) {
		    	notification = new Notification("The project definition has been created successfully. ",
		    		Notification.Type.ERROR_MESSAGE);
		    } else {
		    	notification = new Notification("The project definition has been created successfully. ", message,
 		    		Notification.Type.ERROR_MESSAGE);
		    }
		    notification.setHtmlContentAllowed(true);
		    notification.setStyleName("green_error");
		    notification.show(Page.getCurrent());
    }
    
    private String getProjectName(long projectID)
    {
    	AssuranceProjectManager projectManager = 
                (AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
	    AssuranceProject project = projectManager.getProject(projectID);
    	String projectName = project.getName();
    	
    	return projectName;
    }
    
    private void createConfirmationDeleteWindow(String projectName) 
    {
    	String windowCaption = "WARNING!";
    	String confirmQuestion = "Are you sure you want to remove the following Project: <b><i>\"" + projectName + "\"</b></i>?";
    	String bottomNote = "WARNING! This action will remove your project and its data from the database permanently!";
    	
    	ConfirmationWindow confirmDeleteProjectWindow = new ConfirmationWindow(windowCaption, confirmQuestion, bottomNote);
    	confirmDeleteProjectWindow.addYesAction((e) ->
        {
        	AssuranceProjectManager projectManager = (AssuranceProjectManager) SpringContextHelper
        			.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
        	if (projectManager.getProjects().size() > 1) 
        	{
        		deleteCurrentAssuranceProject();
        		
        		Notification.show(String.format("\"%s\" project has been deleted successfully.", projectName), Notification.Type.HUMANIZED_MESSAGE);
        		
        		//refresh the current view
        		projectComboSelector.reloadAndSelectProjectCombo(-1);
        		
        		OpencertLogger.info("Project deleted:" + projectName);
        	} else {
        		Notification.show("Cannot delete the last Assurance Project");
        	}
        	confirmDeleteProjectWindow.close();							
        });
    	
        if (!(UI.getCurrent().getWindows().contains(confirmDeleteProjectWindow))) {
            UI.getCurrent().addWindow(confirmDeleteProjectWindow);  
        }
    }
    
    public void deleteCurrentAssuranceProject()
    {
        AssuranceProjectManager projectManager = (AssuranceProjectManager) SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
        projectManager.delete(currentProject);
    }
    
    public void updateCurrentAssuranceProject()
    {
        AssuranceProjectManager projectManager = (AssuranceProjectManager) SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
        projectManager.update(currentProject, projectNameField.getValue(), projectDescField.getValue());
        
        long baselineCDOId = baselineArtefactTable.getBaselineCDOId();
        BaseArtefactManager baseArtefactManager = (BaseArtefactManager)SpringContextHelper.getBeanFromWebappContext(BaseArtefactManager.SPRING_NAME);
        baseArtefactManager.updateBaseArtefacts(baselineCDOId, baselineArtefactTable.getCurrentElementChange());
        
        BaselineManager baselineManager = (BaselineManager)SpringContextHelper.getBeanFromWebappContext(BaselineManager.SPRING_NAME);
        baselineManager.updateBaselineFrameworkName(baselineCDOId, baselineArtefactTable.getBaselineFrameworkName(), currentProject.getAssuranceProject());
    }
    
    public long createNewAssuranceProject()
    {	
    	BaselineArtafectChangePack changePack = baselineArtefactTable.getCurrentElementChange();
    	Collection<IdNameDesc> addedBaseArtefacts = changePack.getAddedBaseArtefacts();
    	
    	try {
    		AssuranceProjectManager projectManager = (AssuranceProjectManager) SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
    		long projectID = projectManager.createNewProject(projectNameField.getValue(), projectDescField.getValue(), baselineArtefactTable.getBaselineFrameworkName(), addedBaseArtefacts);
    		return projectID;
    		
    	} catch (RuntimeException ex) 
    	{
    	    ex.printStackTrace();
    		if (ex.getMessage().contains("Duplicate resource node")) {
    			Notification.show("Cannot create new Assurance Project: " + projectNameField.getValue() + ". This Assurance Project Name already exists.", Notification.Type.WARNING_MESSAGE);
    		} else {
    			Notification.show("Error creating new Project: " + projectNameField.getValue(), Notification.Type.WARNING_MESSAGE);
    		}
    		return -1;
    	}
    }
    
    private void cloneAssuranceProject() 
    {
    	setProjectAdminPageMode(ProjectAdminPageMode.CREATE_PROJECT);
    	String projectName = currentProject.getName() != null ? currentProject.getName() : "";
    	projectNameField.setValue(projectName + "_cloned");
    	setBaselineFrameworkDesc();
    	setBaselineFrameworkName(baselineArtefactTable.getBaselineCDOId());
    	projectNameField.focus();
    	projectNameField.selectAll();
    	notificationPresenter.showNotification("");
    	baselineArtefactTable.cloneRows();
    }
    
    private boolean checkProjectFields() {
    	if ("".equals(projectNameField.getValue())) {
    		Notification.show("No Assurance Project Name has been specified. Please enter Assurance Project Name.", Notification.Type.WARNING_MESSAGE);
    		return false;
    	}
    	if ("".equals(baselineArtefactTable.getBaselineFrameworkName())) {
    		Notification.show("No Baseline Framework Name has been specified. Please enter Baseline Framework Name.", Notification.Type.WARNING_MESSAGE);
    		return false;
    	}
    	
    	return true;
    }
    
    @Override
    public Component getExportToDocxComponent()
    {
        return null;
    }

    @Override
    public void setBaselineFramework(long baselineFrameworkID)
    {
        projectNameField.setValue(currentProject.getName() != null ? currentProject.getName() : "");
        setBaselineFrameworkDesc();
        
        setBaselineFrameworkName(baselineFrameworkID);
        
        setProjectResourceLabel();
        setProjectAdminPageMode(ProjectAdminPageMode.EDIT_PROJECT);
    }
    
    private void setProjectResourceLabel()
    {
    	AssuranceProjectManager projectManager = (AssuranceProjectManager) SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
        String projectResourceName = projectManager.getAssuranceProjectResourcePath(currentProject.getAssuranceProject());
        
        projectResourceValueLabel.setValue(projectResourceName);
    }
    
    private void setBaselineFrameworkName(long baselineFrameworkID)
    {
    	String baselineFrameworkName = getBaselineFrameworkName(baselineFrameworkID);
        baselineArtefactTable.setBaselineFrameworkName(baselineFrameworkName != null ? baselineFrameworkName : "");
    }
    
    private void setBaselineFrameworkDesc() 
    {
    	projectDescField.setValue(currentProject.getDescription() != null ? currentProject.getDescription() : "");
    }
    
    private String getBaselineFrameworkName(long baselineFrameworkID)
    {
        List<FrameworkWrapper> frameworks = currentProject.getBaseFrameworks();
        for (FrameworkWrapper framework : frameworks) {
        	if (framework.getId() == baselineFrameworkID) {
        		return framework.getName();
        	}
        }
        return "";
    }

}
