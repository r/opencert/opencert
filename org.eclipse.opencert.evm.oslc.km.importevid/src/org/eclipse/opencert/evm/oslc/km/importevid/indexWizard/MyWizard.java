/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.evm.oslc.km.importevid.indexWizard;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Scanner;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;

public class MyWizard extends Wizard {
    
	// General constants  
	private static final String StringEmpty = "";

	// Default Remote Uri configuration:
	 public static final String TrcAmassWebServiceUrl = "http://authoring.reusecompany.com:9999/OslcKmService.svc/IndexRequirement";
	 public static final String TrcAmassWebServiceUrl_Retrieval = "http://authoring.reusecompany.com:9999/OslcKmService.svc/RetrieveSimilarRequirements";
	 
	// Debug: 
	 //public static final String TrcAmassWebServiceUrl = "http://localhost:38306/OslcKmService.svc/IndexRequirement";
	 //public static final String TrcAmassWebServiceUrl_Retrieval = "http://localhost:38306/OslcKmService.svc/RetrieveSimilarRequirements";
	
	// File to be checked
	// private static final String PapyrusModel = "D:\\Projects\\RQA v17\\Rqa.Face.OslcKm\\Oslc.Km.Service.Amass.Tests\\Tests\\Papyrus\\model.uml";
	private static final int PapyrusModelType = 4;
	private static final String PapyrusAdditionalParameters = StringEmpty;
	
	// Messages
    private static final String Title =  "OSLC-KM Reuse Discovery - Index (1)";
	private static final String SuccessMessage = "Model indexed successfully!";
	private static final String FailureMessage = "Error when trying to index the file!";
	private static final String WsNotRunningMessage = "OSLC-KM Web Service Not Running at: ";
	
    protected MyPageOne one;
    protected MyPageTwo two;

    public MyWizard() {
        super();
        setNeedsProgressMonitor(true);
    }

    @Override
    public String getWindowTitle() {
        return Title;
    }

    @Override
    public void addPages() {
        one = new MyPageOne();
        addPage(one);
        two = new MyPageTwo(TrcAmassWebServiceUrl);
        addPage(two);
    }

    @Override
    public boolean performFinish() {
    	boolean success;
    	success = false;
		try {
			success = indexFileInOslcKmRepository(PapyrusModelType, 
														one.getFileFullPath(), 
														Charset.forName(Utf8Format), 
														PapyrusAdditionalParameters, 
														two.getOslcKmWebServiceUri());
			
			if (success) {
				MessageDialog.openInformation(getShell(), Title, SuccessMessage);
			} else {
				MessageDialog.openInformation(getShell(), Title, FailureMessage);
			}
		} catch (java.net.ConnectException e) {
			MessageDialog.openInformation(getShell(), Title, WsNotRunningMessage + two.getOslcKmWebServiceUri());
		}
        return success;
    }
    
	
	// Web request constants
	//   Web request header properties and values:
	private static final String HttpPostProtocolName = "POST";
	private static final String ContentTypeRequestProperty = "Content-Type";
	private static final String JsonContentTypeRequestPropertyValue = "application/json";
	private static final String ContentLengthRequestProperty = "Content-Length";
	private static final String AsciiFormat = "ASCII";
	private static final String Utf8Format = "UTF-8";
	//   Parameters:
	private static final String OslcKmTypeParam = "type";
	private static final String AdditionalParametersParam = "additionalParameters";
	private static final String FileContentParam = "fileContent";
	//   Json parts:
	private static final String JsonStartWebRequest = "{\"";
	private static final String JsonEndWebRequest = "\"}";
	private static final String JsonAttributeValueSeparatorWebRequest = "\":\"";
	private static final String JsonElementSeparatorWebRequest = "\",\"";
	//   Debug messages:
	private static final String HttpWebRequestErrorMessage = "Failed : HTTP error code : ";

	
	public boolean indexFileInOslcKmRepository(int oslcKmType, String fileName, Charset encoding, String additionalParameters, String oslcKmWsUri) throws java.net.ConnectException {
		boolean result = false;
		HttpURLConnection conn;
		conn = null; 
		DataInputStream doi;
		doi = null;
		InputStream inputStream;
		inputStream = null;
		Scanner s ;
		s = null;
		try {
			String fileContent;
			fileContent = readFile(fileName, encoding);
			if (fileContent != null && !fileContent.equals(StringEmpty)) {
				
				// Building POST parameters
				StringBuilder postData = new StringBuilder();
				postData.append(JsonStartWebRequest);
				postData.append(OslcKmTypeParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(oslcKmType);
				postData.append(JsonElementSeparatorWebRequest);
				postData.append(AdditionalParametersParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(GetBase64StringFromString(additionalParameters));
				postData.append(JsonElementSeparatorWebRequest);
				postData.append(FileContentParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(GetBase64StringFromString(fileContent));
				postData.append(JsonEndWebRequest);
				
		        byte[] postDataBytes = postData.toString().getBytes(AsciiFormat);
	
				URL url = new URL(oslcKmWsUri);
				
		        conn = (HttpURLConnection)url.openConnection();
		        conn.setRequestMethod(HttpPostProtocolName);
		        conn.setRequestProperty(ContentTypeRequestProperty, JsonContentTypeRequestPropertyValue);
		        conn.setRequestProperty(ContentLengthRequestProperty, String.valueOf(postDataBytes.length));
		        conn.setDoOutput(true);
		        conn.getOutputStream().write(postDataBytes);
	
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					System.out.println(HttpWebRequestErrorMessage + conn.getResponseCode());
					
				} else {
					inputStream = conn.getInputStream();
					doi = new DataInputStream(inputStream);
					result = doi.readBoolean();
					
					//s = new Scanner(inputStream);
					//s.useDelimiter(Delimiter);
					//result = s.hasNextBoolean() ? s.nextBoolean() : false;					
				}
			}
		} catch (java.net.ConnectException e) {
			String errorMessage;
			errorMessage = e.getMessage() + " @ " + TrcAmassWebServiceUrl;
			System.out.println(errorMessage);
			throw e;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (s != null) {
				s.close();
				s = null;
			}
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch(Exception ex1) {
					
				}
				inputStream = null;
			}
			if (conn != null) {
				conn.disconnect();
				conn = null;
			} 
		}
		return result;
	}
	
	private static String readFile(String path, Charset encoding) 
			  throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
	
	private static String GetBase64StringFromString(String text){
		String value;
		value = StringEmpty;
		if (text != null && !text.equals(StringEmpty)) {
			value = Base64.getEncoder().encodeToString(text.getBytes());
		}
		return value;
	}

    
}