/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;
import org.eclipse.opencert.sam.arg.arg.parts.ClaimPropertiesEditionPart;


// End of user code

/**
 * 
 * 
 */
public class ClaimPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for evaluation ReferencesTable
	 */
	protected ReferencesTableSettings evaluationSettings;
	
	/**
	 * Settings for lifecycleEvent ReferencesTable
	 */
	protected ReferencesTableSettings lifecycleEventSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ClaimPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject claim, String editing_mode) {
		super(editingContext, claim, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = ArgViewsRepository.class;
		partKey = ArgViewsRepository.Claim.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Claim claim = (Claim)elt;
			final ClaimPropertiesEditionPart basePart = (ClaimPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ArgViewsRepository.Claim.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, claim.getId()));
			
			if (isAccessible(ArgViewsRepository.Claim.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, claim.getName()));
			
			if (isAccessible(ArgViewsRepository.Claim.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, claim.getDescription()));
			
			if (isAccessible(ArgViewsRepository.Claim.Properties.content))
				basePart.setContent(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, claim.getContent()));
			
			if (isAccessible(ArgViewsRepository.Claim.Properties.evaluation)) {
				evaluationSettings = new ReferencesTableSettings(claim, AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation());
				basePart.initEvaluation(evaluationSettings);
			}
			if (isAccessible(ArgViewsRepository.Claim.Properties.lifecycleEvent)) {
				lifecycleEventSettings = new ReferencesTableSettings(claim, AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent());
				basePart.initLifecycleEvent(lifecycleEventSettings);
			}
			if (isAccessible(ArgViewsRepository.Claim.Properties.public_))
				basePart.setPublic_(EEFConverterUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, claim.getPublic()));
			
			if (isAccessible(ArgViewsRepository.Claim.Properties.assumed))
				basePart.setAssumed(EEFConverterUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, claim.getAssumed()));
			
			if (isAccessible(ArgViewsRepository.Claim.Properties.toBeSupported))
				basePart.setToBeSupported(EEFConverterUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, claim.getToBeSupported()));
			
			if (isAccessible(ArgViewsRepository.Claim.Properties.toBeInstantiated))
				basePart.setToBeInstantiated(EEFConverterUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, claim.getToBeInstantiated()));
			
			// init filters
			
			
			
			
			if (isAccessible(ArgViewsRepository.Claim.Properties.evaluation)) {
				basePart.addFilterToEvaluation(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof AssuranceAssetEvaluation); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for evaluation
				// End of user code
			}
			if (isAccessible(ArgViewsRepository.Claim.Properties.lifecycleEvent)) {
				basePart.addFilterToLifecycleEvent(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof AssuranceAssetEvent); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for lifecycleEvent
				// End of user code
			}
			
			
			
			
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}













	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ArgViewsRepository.Claim.Properties.id) {
			return ArgPackage.eINSTANCE.getModelElement_Id();
		}
		if (editorKey == ArgViewsRepository.Claim.Properties.name) {
			return ArgPackage.eINSTANCE.getModelElement_Name();
		}
		if (editorKey == ArgViewsRepository.Claim.Properties.description) {
			return ArgPackage.eINSTANCE.getArgumentationElement_Description();
		}
		if (editorKey == ArgViewsRepository.Claim.Properties.content) {
			return ArgPackage.eINSTANCE.getArgumentationElement_Content();
		}
		if (editorKey == ArgViewsRepository.Claim.Properties.evaluation) {
			return AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation();
		}
		if (editorKey == ArgViewsRepository.Claim.Properties.lifecycleEvent) {
			return AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent();
		}
		if (editorKey == ArgViewsRepository.Claim.Properties.public_) {
			return ArgPackage.eINSTANCE.getClaim_Public();
		}
		if (editorKey == ArgViewsRepository.Claim.Properties.assumed) {
			return ArgPackage.eINSTANCE.getClaim_Assumed();
		}
		if (editorKey == ArgViewsRepository.Claim.Properties.toBeSupported) {
			return ArgPackage.eINSTANCE.getClaim_ToBeSupported();
		}
		if (editorKey == ArgViewsRepository.Claim.Properties.toBeInstantiated) {
			return ArgPackage.eINSTANCE.getClaim_ToBeInstantiated();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Claim claim = (Claim)semanticObject;
		if (ArgViewsRepository.Claim.Properties.id == event.getAffectedEditor()) {
			claim.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Claim.Properties.name == event.getAffectedEditor()) {
			claim.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Claim.Properties.description == event.getAffectedEditor()) {
			claim.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Claim.Properties.content == event.getAffectedEditor()) {
			claim.setContent((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Claim.Properties.evaluation == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, evaluationSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				evaluationSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				evaluationSettings.move(event.getNewIndex(), (AssuranceAssetEvaluation) event.getNewValue());
			}
		}
		if (ArgViewsRepository.Claim.Properties.lifecycleEvent == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, lifecycleEventSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				lifecycleEventSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				lifecycleEventSettings.move(event.getNewIndex(), (AssuranceAssetEvent) event.getNewValue());
			}
		}
		if (ArgViewsRepository.Claim.Properties.public_ == event.getAffectedEditor()) {
			claim.setPublic((java.lang.Boolean)EEFConverterUtil.createFromString(EcorePackage.Literals.EBOOLEAN_OBJECT, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Claim.Properties.assumed == event.getAffectedEditor()) {
			claim.setAssumed((java.lang.Boolean)EEFConverterUtil.createFromString(EcorePackage.Literals.EBOOLEAN_OBJECT, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Claim.Properties.toBeSupported == event.getAffectedEditor()) {
			claim.setToBeSupported((java.lang.Boolean)EEFConverterUtil.createFromString(EcorePackage.Literals.EBOOLEAN_OBJECT, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.Claim.Properties.toBeInstantiated == event.getAffectedEditor()) {
			claim.setToBeInstantiated((java.lang.Boolean)EEFConverterUtil.createFromString(EcorePackage.Literals.EBOOLEAN_OBJECT, (String)event.getNewValue()));
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ClaimPropertiesEditionPart basePart = (ClaimPropertiesEditionPart)editingPart;
			if (ArgPackage.eINSTANCE.getModelElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Claim.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (ArgPackage.eINSTANCE.getModelElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Claim.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentationElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Claim.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentationElement_Content().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Claim.Properties.content)) {
				if (msg.getNewValue() != null) {
					basePart.setContent(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setContent("");
				}
			}
			if (AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.Claim.Properties.evaluation))
				basePart.updateEvaluation();
			if (AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.Claim.Properties.lifecycleEvent))
				basePart.updateLifecycleEvent();
			if (ArgPackage.eINSTANCE.getClaim_Public().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Claim.Properties.public_)) {
				if (msg.getNewValue() != null) {
					basePart.setPublic_(EcoreUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, msg.getNewValue()));
				} else {
					basePart.setPublic_("");
				}
			}
			if (ArgPackage.eINSTANCE.getClaim_Assumed().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Claim.Properties.assumed)) {
				if (msg.getNewValue() != null) {
					basePart.setAssumed(EcoreUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, msg.getNewValue()));
				} else {
					basePart.setAssumed("");
				}
			}
			if (ArgPackage.eINSTANCE.getClaim_ToBeSupported().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Claim.Properties.toBeSupported)) {
				if (msg.getNewValue() != null) {
					basePart.setToBeSupported(EcoreUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, msg.getNewValue()));
				} else {
					basePart.setToBeSupported("");
				}
			}
			if (ArgPackage.eINSTANCE.getClaim_ToBeInstantiated().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.Claim.Properties.toBeInstantiated)) {
				if (msg.getNewValue() != null) {
					basePart.setToBeInstantiated(EcoreUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, msg.getNewValue()));
				} else {
					basePart.setToBeInstantiated("");
				}
			}
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			ArgPackage.eINSTANCE.getModelElement_Id(),
			ArgPackage.eINSTANCE.getModelElement_Name(),
			ArgPackage.eINSTANCE.getArgumentationElement_Description(),
			ArgPackage.eINSTANCE.getArgumentationElement_Content(),
			AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation(),
			AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent(),
			ArgPackage.eINSTANCE.getClaim_Public(),
			ArgPackage.eINSTANCE.getClaim_Assumed(),
			ArgPackage.eINSTANCE.getClaim_ToBeSupported(),
			ArgPackage.eINSTANCE.getClaim_ToBeInstantiated()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#isRequired(java.lang.Object, int)
	 * 
	 */
	public boolean isRequired(Object key, int kind) {
		return key == ArgViewsRepository.Claim.Properties.public_ || key == ArgViewsRepository.Claim.Properties.assumed || key == ArgViewsRepository.Claim.Properties.toBeSupported || key == ArgViewsRepository.Claim.Properties.toBeInstantiated;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (ArgViewsRepository.Claim.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Claim.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Claim.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentationElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentationElement_Description().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Claim.Properties.content == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentationElement_Content().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentationElement_Content().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Claim.Properties.public_ == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getClaim_Public().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getClaim_Public().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Claim.Properties.assumed == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getClaim_Assumed().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getClaim_Assumed().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Claim.Properties.toBeSupported == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getClaim_ToBeSupported().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getClaim_ToBeSupported().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.Claim.Properties.toBeInstantiated == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getClaim_ToBeInstantiated().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getClaim_ToBeInstantiated().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
