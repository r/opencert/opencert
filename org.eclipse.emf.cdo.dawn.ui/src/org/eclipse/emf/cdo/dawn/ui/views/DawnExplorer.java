/*
 * Copyright (c) 2010-2012 Eike Stepper (Berlin, Germany) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Martin Fluegge - initial API and implementation
 *     Angel L�pez (Tecnalia) - Modified according Opencert needs
 */
package org.eclipse.emf.cdo.dawn.ui.views;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.cdo.CDOObjectReference;
import org.eclipse.emf.cdo.dawn.internal.ui.bundle.OM;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.DawnEditorInput;
import org.eclipse.emf.cdo.dawn.ui.helper.EditorDescriptionHelper;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.internal.ui.views.CDOSessionsView;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.ui.CDOEditorInput;
import org.eclipse.emf.cdo.ui.CDOEditorUtil;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.net4j.Net4jUtil;
import org.eclipse.net4j.connector.ConnectorException;
import org.eclipse.net4j.connector.IConnector;
import org.eclipse.net4j.util.container.IContainer;
import org.eclipse.net4j.util.container.IPluginContainer;
import org.eclipse.net4j.util.om.trace.ContextTracer;
import org.eclipse.net4j.util.security.NotAuthenticatedException;
import org.eclipse.net4j.util.ui.views.ContainerItemProvider;
import org.eclipse.net4j.util.ui.views.IElementFilter;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

/**
 * @author Martin Fluegge
 * Modified by Tecnalia
 */
public class DawnExplorer extends CDOSessionsView
{
  private static final String OPENCERT_REPO = "cdo://opencert";

private static final String EXTENSION_DIAGRAM = "_diagram";

private static final String PLEASE_DELETE_MANUALLY_THE_DIAGRAMS = "";

private static final String DELETE_MODEL = "DELETE Model";

  private static final String DELETE_FOLDER = "DELETE Folder";

private static final String PROBLEM_DELETING_THE_MODEL = "Problem deleting the model";

private static final String PROBLEM_DELETING_THE_FOLDER = "Problem deleting the folder";

private static final int DEFAULT_SLEEP_INTERVAL = 500;


  private static final ContextTracer TRACER = new ContextTracer(OM.DEBUG, DawnExplorer.class);

  public static final String ID = "org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer";

  private CDOView view;

  private CDOResource selectedModel;
  private CDOResourceFolder selectedFolder; 
  
  
  private String messageAboutDiagrams="Please Delete Manually the diagrams:\n";
  private	boolean hasDiagrams=false;
  
  private CDOSession session;
  public CDOView getView()
  {
    return view;
  }

  /**
   * The constructor.
   */
  public DawnExplorer()
  {
	  
	 /* CDOConnectionUtil.instance.init(PreferenceConstants.getRepositoryName(), PreferenceConstants.getProtocol(),
	          PreferenceConstants.getServerName());
	       session = CDOConnectionUtil.instance.getCurrentSession();*/
	      //view = CDOConnectionUtil.instance.openView(session);
	      
	  
	  
      Thread thread = new Thread(new Runnable()
      {
        public void run()
        {                            
            try
            {            	
            	Thread.sleep(DEFAULT_SLEEP_INTERVAL);
            	if (initialize()){
            		
            		/*Display.getDefault().asyncExec(new Runnable() {
            		    public void run() {
            		    	MessageDialog.openInformation(new Shell(),"OK","OK");
            		    }
            		});    */        		            		
            	}            	            
            }
           /* catch (NotAuthenticatedException ex)
            {
            	Display.getDefault().asyncExec(new Runnable() {
        		    public void run() {
        		    	MessageDialog.openInformation(new Shell(),"Error 1",ex.getMessage() + "-" + ex.toString());
        		    }
        		});   
                ex.printStackTrace();
            }*/
            catch (Exception ex)
            {
            	Display.getDefault().asyncExec(new Runnable() {
        		    public void run() {
        		    	MessageDialog.openInformation(new Shell(),"Error ", "Error connecting to the server. Please review the configuration of the server connection.");
        		    }
        		});  
                ex.printStackTrace(); 
            }         
        }
      });
      thread.start();
    }

  /**
   * Initializes the view of the DawnExplorer
   */
  private boolean initialize()
  {
    try
    {
     CDOConnectionUtil.instance.init(PreferenceConstants.getRepositoryName(), PreferenceConstants.getProtocol(),
          PreferenceConstants.getServerName());
     
     CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
     if(sessionCDO == null){
			try {
				//To wait the connection establishment in Repository Explorer Thread
				Thread.sleep(1500);
				sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
				sessionCDO = CDOConnectionUtil.instance.openSession();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
      //CDOSession session = CDOConnectionUtil.instance.openSession();
      view = CDOConnectionUtil.instance.openView(sessionCDO);
	
    }
    catch (ConnectorException ex)
    {
      return false;
    }
    return true;
  }

  @Override
  public void dispose()
  {
    // actually no one else should use this view
    if(view != null){
    	view.close();
    }
  }

  @Override
  protected ContainerItemProvider<IContainer<Object>> createContainerItemProvider()
  {
    return new DawnItemProvider(getSite().getPage(), this, new IElementFilter()
    {
      public boolean filter(Object element)
      {
        return element instanceof CDOSession;
      }
    });
  }

  @Override
  protected void hookDoubleClick()
  {
    getViewer().addDoubleClickListener(new IDoubleClickListener()
    {
      public void doubleClick(DoubleClickEvent event)
      {
        Object obj = ((IStructuredSelection)event.getSelection()).getFirstElement();
        if (obj instanceof CDOResource)
        {
          CDOResource resource = (CDOResource)obj;

          if (TRACER.isEnabled())
          {
            TRACER.format("Opening CDOResource {0} ", resource); //$NON-NLS-1$
          }

          String editorID = EditorDescriptionHelper.getEditorIdForDawnEditor(resource.getName());

          if (editorID != null && !editorID.equals(""))
          {
            try
            {
              DawnEditorInput editorInput = new DawnEditorInput(resource.getURI());
              
              DawnExplorer.this.getSite().getPage().openEditor(editorInput, editorID);
            }
            catch (PartInitException e)
            {
              e.printStackTrace();
            }
          }
          else
          {
            CDOTransaction transaction = view.getSession().openTransaction();
            CDOEditorInput editorInput = CDOEditorUtil.createCDOEditorInput(transaction, ((CDOResource)obj).getPath(),
                true);
            try
            {
              DawnExplorer.this.getSite().getPage().openEditor(editorInput, CDOEditorUtil.getEditorID());
            }
            catch (PartInitException e)
            {
              e.printStackTrace();
            }
          }
        }
      }
    });
  }

/*
@Override
protected void hookContextMenu() {
	//super.hookContextMenu();
	// TODO Auto-generated method stub
	final TreeViewer viewer= getViewer();
	MenuManager menuMgr = new MenuManager("#PopupMenu");
	menuMgr.setRemoveAllWhenShown(true);
	menuMgr.addMenuListener(new IMenuListener() {
		public void menuAboutToShow(IMenuManager manager) {
			if (viewer.getSelection() instanceof IStructuredSelection) {
				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();               

                if (selection.getFirstElement() instanceof CDOResource) {                	
                	selectedModel=(CDOResource)selection.getFirstElement();
                	
                		DawnExplorer.this.fillContextMenu(manager);
                	
                }
                if (selection.getFirstElement() instanceof CDOResourceFolder) {                	
                	selectedFolder=(CDOResourceFolder)selection.getFirstElement();
                	//If it is a "standard" model                	
                		DawnExplorer.this.fillContextMenuFolder(manager);                	
                }
                
			}
			
		}
	});
	Menu menu = menuMgr.createContextMenu(super.getViewer().getControl());
	super.getViewer().getControl().setMenu(menu);
	getSite().registerContextMenu(menuMgr, super.getViewer());
}
   /*
//Modified by Tecnalia
private void fillContextMenu(IMenuManager manager) {
	Action action1 = new Action() {
		public void run() {	
			MessageDialog dialog = new MessageDialog(Display.getDefault().getActiveShell(), "Delete Model Confirmation ", null,  "Do you want to delete the selected model?\r\n\r\n" , MessageDialog.CONFIRM, new String[] { "Accept",
      	    "Refuse"}, 0);
			int response = dialog.open();
	        
	        if(response==0){			        	
	        	deleteModel();	
	        	//showMessage(DELETE_EXECUTED_SUCCESSFULY);
	        }							
		}		
	};
	
	action1.setText(DELETE_MODEL);
	action1.setToolTipText(DELETE_MODEL);
	action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_ETOOL_DELETE));
	manager.add(new Separator());
	manager.add(action1);
	
	//drillDownAdapter.addNavigationActions(manager);
	// Other plug-ins can contribute there actions here
	manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	//super.hookContextMenu();
}

//Modified by Tecnalia
private void fillContextMenuFolder(IMenuManager manager) {
	Action action1 = new Action() {
		public void run() {			
			try {	
				MessageDialog dialog = new MessageDialog(Display.getDefault().getActiveShell(), "Delete Folder Confirmation ", null,  "Do you want to delete the selected folder and all its contents?\r\n\r\n" , MessageDialog.CONFIRM, new String[] { "Accept",
	      	    "Refuse"}, 0);
				int response = dialog.open();
		        
		        if(response==0){			        			        						
					CDOTransaction transaction = view.getSession().openTransaction();
					CDOResourceNode folderToDelete= transaction.getResourceNode(selectedFolder.getPath());
					selectedFolder=(CDOResourceFolder)folderToDelete;
					deleteFolderContents(selectedFolder);

					//folderToDelete.delete(null);
					transaction.commit();
					transaction.close();
					//showMessage(DELETE_EXECUTED_SUCCESSFULY);
					
		        }
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				
				e.printStackTrace();
				showMessage(PROBLEM_DELETING_THE_FOLDER);		
			}
				
		}

		private  void deleteFolderContents(CDOResourceFolder cdoResourceFolder) throws IOException {
			
			
			EList<CDOResourceNode> listN = cdoResourceFolder.getNodes();						
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeAllEditors(true);
			while ( listN.size() > 0) {
				if (listN.get(0) instanceof CDOResourceFolder) {
					deleteFolderContents((CDOResourceFolder) listN.get(0));					
				} else if (listN.get(0) instanceof CDOResource) {					
					selectedModel = (CDOResource)listN.get(0);
					if (!deleteModel()){
						//break the while in case of error deleting one model
						return;
					}
				}
			}
			cdoResourceFolder.delete(null);			
		}
	};
	
	action1.setText(DELETE_FOLDER);
	action1.setToolTipText(DELETE_FOLDER);
	action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_ETOOL_DELETE));
	manager.add(new Separator());
	manager.add(action1);
	
	//drillDownAdapter.addNavigationActions(manager);
	// Other plug-ins can contribute there actions here
	manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	//super.hookContextMenu();
}


/*private void checkSubFolderContents(CDOResourceFolder cdoResourceFolder) throws IOException {
	EList<CDOResourceNode> listN = cdoResourceFolder.getNodes();
		
	for(int i=0; i<listN.size();i++){
		if (listN.get(i) instanceof CDOResource) {
			String modelName = ((CDOResource)listN.get(i)).getPath();
			if(modelName.contains(EXTENSION_DIAGRAM)){
				messageAboutDiagrams += "   " + modelName + "\n";
				hasDiagrams=true;
				//listN.move(0, i);
			}					
		}
		else if (listN.get(0) instanceof CDOResourceFolder) {
			checkSubFolderContents((CDOResourceFolder)listN.get(i));
		}
	}
	
}*/


private boolean deleteModel() {
	try {	
		
		//Close the editor of the model to delete if opened to avoid after Error messages	
		IEditorReference[] edi = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences();
		for (IEditorReference editor : edi) {
			IEditorInput input = editor.getEditorInput();
			if(input !=null){
				//When editors are re-opened automatically are URIEditorInput
				String path = "";
				if (input instanceof DawnEditorInput || input instanceof URIEditorInput) {
					if (input instanceof DawnEditorInput){
						path =((DawnEditorInput) input).getURI().toString();
					}
					else{
						path =((URIEditorInput) input).getURI().toString();
					}
					if(path.equals(OPENCERT_REPO + selectedModel.getPath())){
						//System.out.println("CERRAR " + path);
						//boolean cerrado = activePage.closeEditor(editor, false);	
						//editor						
						//IEditorPart editorClose= editor.getEditor(false);	
						//editorClose.getEditorSite().getPage().closeEditor(editorClose, false);
						IEditorReference[] editorReferencesClose= new IEditorReference[1];
						editorReferencesClose[0]=editor;
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditors(editorReferencesClose,false);
						//Closed, exit for					
						break;
					}						
				}
								
			}
		}
		
		
		CDOTransaction transaction = view.getSession().openTransaction();
		CDOResource modelToDelete= transaction.getResource(selectedModel.getPath());
		
		//If the model is a diagram, delete directly because won't be related to other models
		if(!modelToDelete.getPath().contains(EXTENSION_DIAGRAM)){			
			for (Iterator<EObject> iter=modelToDelete.getContents().iterator();iter.hasNext();) {					
				EObject aEObject= iter.next();
				List<CDOObjectReference> rc = transaction.queryXRefs(CDOUtil.getCDOObject(aEObject), new EReference[] {});
				for(CDOObjectReference oneRef:rc){
					EcoreUtil.remove(oneRef.getSourceObject(),oneRef.getSourceFeature(),oneRef.getTargetObject());
				}
				if(aEObject.eContents().size()!=0){
					deleteSubContents(aEObject,transaction);
				}						
			}				
			modelToDelete.save(null);
		}				
		modelToDelete.delete(null);
		transaction.commit();
		transaction.close();
				
		return true;
	} catch (Exception e) {
		// TODO Auto-generated catch block
		
		e.printStackTrace();
		showMessage(PROBLEM_DELETING_THE_MODEL + " " + OPENCERT_REPO + selectedModel.getPath());
		return false;
	}
}

private void deleteSubContents(EObject aEObject,CDOTransaction transaction) {						
	try {			
		//for (Iterator<EObject> iter=aEObject.eContents().iterator();iter.hasNext();) {
		while(aEObject.eContents().size()>0){
			EObject bEObject=aEObject.eContents().get(0);
			List<CDOObjectReference> rc = transaction.queryXRefs(CDOUtil.getCDOObject(bEObject), new EReference[] {});
			//Remove the reference in the source objects to avoid inconsistencies
			for(CDOObjectReference oneRef:rc){
				EcoreUtil.remove(oneRef.getSourceObject(),oneRef.getSourceFeature(),oneRef.getTargetObject());
			}					
			if(bEObject.eContents().size()!=0){
				deleteSubContents(bEObject,transaction);
			}		
			//iter.remove();
			EcoreUtil.delete(bEObject);
		}
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}
