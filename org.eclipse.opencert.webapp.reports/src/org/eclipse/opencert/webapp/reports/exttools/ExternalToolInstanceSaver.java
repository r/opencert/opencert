/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.exttools;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.externaltools.api.ExternalToolQuery;
import org.eclipse.opencert.externaltools.api.ExternalToolQuerySetting;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQueryDAO;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQueryDAOImpl;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQuerySettingDAO;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQuerySettingDAOImpl;

public class ExternalToolInstanceSaver
{
    public void saveExternalToolConnectorProperties(ExternalToolQuery externalToolQuery)
    {
        ExternalToolQueryDAO externalToolQueryDAO = (ExternalToolQueryDAO) SpringContextHelper.getBeanFromWebappContext(ExternalToolQueryDAOImpl.SPRING_NAME);

        ExternalToolQuerySettingDAO externalToolQuerySettingDAO = (ExternalToolQuerySettingDAO) SpringContextHelper
                .getBeanFromWebappContext(ExternalToolQuerySettingDAOImpl.SPRING_NAME);

        List<ExternalToolQuerySetting> finalExternalToolQuerySettings = new ArrayList<ExternalToolQuerySetting>();

        for (ExternalToolQuerySetting externalToolQuerySetting : externalToolQuery.getExternalToolQuerySettings()) {
            ExternalToolQuerySetting oldSetting = externalToolQuerySettingDAO.findByNameAndValue(externalToolQuerySetting.getName(),
                    externalToolQuerySetting.getValue());
            if (oldSetting == null) {
                externalToolQuerySettingDAO.save(new ExternalToolQuerySetting(externalToolQuerySetting.getName(), externalToolQuerySetting.getValue()));
                finalExternalToolQuerySettings.add(externalToolQuerySettingDAO.findByNameAndValue(externalToolQuerySetting.getName(),
                        externalToolQuerySetting.getValue()));
            } else {
                finalExternalToolQuerySettings.add(oldSetting);
            }
        }

        externalToolQuery.setExternalToolQuerySettings(new LinkedHashSet<ExternalToolQuerySetting>(finalExternalToolQuerySettings));

        externalToolQuery.setEditDate(new Date());

        externalToolQueryDAO.save(externalToolQuery);
    }
}
