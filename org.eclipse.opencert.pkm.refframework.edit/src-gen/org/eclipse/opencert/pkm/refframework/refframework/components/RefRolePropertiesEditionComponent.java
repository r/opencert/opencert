/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.pkm.refframework.refframework.RefRole;

import org.eclipse.opencert.pkm.refframework.refframework.parts.RefRolePropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart;

// End of user code

/**
 * 
 * 
 */
public class RefRolePropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private RefRolePropertiesEditionPart basePart;

	/**
	 * The RefRoleBasePropertiesEditionComponent sub component
	 * 
	 */
	protected RefRoleBasePropertiesEditionComponent refRoleBasePropertiesEditionComponent;

	/**
	 * The RoleEquivalenceMap part
	 * 
	 */
	private RoleEquivalenceMapPropertiesEditionPart roleEquivalenceMapPart;

	/**
	 * The RefRoleRoleEquivalenceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected RefRoleRoleEquivalenceMapPropertiesEditionComponent refRoleRoleEquivalenceMapPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param refRole the EObject to edit
	 * 
	 */
	public RefRolePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refRole, String editing_mode) {
		super(editingContext, editing_mode);
		if (refRole instanceof RefRole) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refRole, PropertiesEditingProvider.class);
			refRoleBasePropertiesEditionComponent = (RefRoleBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefRoleBasePropertiesEditionComponent.BASE_PART, RefRoleBasePropertiesEditionComponent.class);
			addSubComponent(refRoleBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refRole, PropertiesEditingProvider.class);
			refRoleRoleEquivalenceMapPropertiesEditionComponent = (RefRoleRoleEquivalenceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefRoleRoleEquivalenceMapPropertiesEditionComponent.ROLEEQUIVALENCEMAP_PART, RefRoleRoleEquivalenceMapPropertiesEditionComponent.class);
			addSubComponent(refRoleRoleEquivalenceMapPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (RefRoleBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (RefRolePropertiesEditionPart)refRoleBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (RefRoleRoleEquivalenceMapPropertiesEditionComponent.ROLEEQUIVALENCEMAP_PART.equals(key)) {
			roleEquivalenceMapPart = (RoleEquivalenceMapPropertiesEditionPart)refRoleRoleEquivalenceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)roleEquivalenceMapPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (RefframeworkViewsRepository.RefRole.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (RefRolePropertiesEditionPart)propertiesEditionPart;
		}
		if (RefframeworkViewsRepository.RoleEquivalenceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			roleEquivalenceMapPart = (RoleEquivalenceMapPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == RefframeworkViewsRepository.RefRole.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == RefframeworkViewsRepository.RoleEquivalenceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
