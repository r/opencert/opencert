/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assurproj.assuranceproject.PermissionConfig;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.providers.AssuranceprojectMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

// End of user code

/**
 * 
 * 
 */
public class PermissionConfigurationsPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, PermissionConfigurationsPropertiesEditionPart {

	protected ReferencesTable permissionConf;
	protected List<ViewerFilter> permissionConfBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> permissionConfFilters = new ArrayList<ViewerFilter>();
	protected TableViewer permissionConfTable;
	protected List<ViewerFilter> permissionConfTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> permissionConfTableFilters = new ArrayList<ViewerFilter>();
	protected Button addPermissionConfTable;
	protected Button removePermissionConfTable;
	protected Button editPermissionConfTable;



	/**
	 * For {@link ISection} use only.
	 */
	public PermissionConfigurationsPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public PermissionConfigurationsPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence permissionConfigurationsStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = permissionConfigurationsStep.addStep(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.class);
		// Start IRR
		// propertiesStep.addStep(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf);
		// End IRR
		propertiesStep.addStep(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable);
		
		
		composer = new PartComposer(permissionConfigurationsStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceprojectViewsRepository.PermissionConfigurations.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf) {
					return createPermissionConfTableComposition(widgetFactory, parent);
				}
				if (key == AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable) {
					return createPermissionConfTableTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(AssuranceprojectMessages.PermissionConfigurationsPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createPermissionConfTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.permissionConf = new ReferencesTable(getDescription(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf, AssuranceprojectMessages.PermissionConfigurationsPropertiesEditionPart_PermissionConfLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PermissionConfigurationsPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				permissionConf.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PermissionConfigurationsPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				permissionConf.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PermissionConfigurationsPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				permissionConf.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PermissionConfigurationsPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				permissionConf.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.permissionConfFilters) {
			this.permissionConf.addFilter(filter);
		}
		this.permissionConf.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf, AssuranceprojectViewsRepository.FORM_KIND));
		this.permissionConf.createControls(parent, widgetFactory);
		this.permissionConf.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PermissionConfigurationsPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData permissionConfData = new GridData(GridData.FILL_HORIZONTAL);
		permissionConfData.horizontalSpan = 3;
		this.permissionConf.setLayoutData(permissionConfData);
		this.permissionConf.setLowerBound(0);
		this.permissionConf.setUpperBound(-1);
		permissionConf.setID(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf);
		permissionConf.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createPermissionConfTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createPermissionConfTableTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tablePermissionConfTable = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tablePermissionConfTable.setHeaderVisible(true);
		GridData gdPermissionConfTable = new GridData();
		gdPermissionConfTable.grabExcessHorizontalSpace = true;
		gdPermissionConfTable.horizontalAlignment = GridData.FILL;
		gdPermissionConfTable.grabExcessVerticalSpace = true;
		gdPermissionConfTable.verticalAlignment = GridData.FILL;
		tablePermissionConfTable.setLayoutData(gdPermissionConfTable);
		tablePermissionConfTable.setLinesVisible(true);

		// Start of user code for columns definition for PermissionConfTable
		// Start IRR
		// TableColumn name = new TableColumn(tablePermissionConfTable,
		// SWT.NONE);
		// name.setWidth(80);
		//name.setText("Label"); //$NON-NLS-1$
		TableColumn name = new TableColumn(tablePermissionConfTable, SWT.NONE);
		name.setWidth(80);
		name.setText("Name"); //$NON-NLS-1$

		TableColumn name1 = new TableColumn(tablePermissionConfTable, SWT.NONE);
		name1.setWidth(150);
		name1.setText("Is Active"); //$NON-NLS-1$ 

		// End IRR
		// End of user code

		permissionConfTable = new TableViewer(tablePermissionConfTable);
		permissionConfTable.setContentProvider(new ArrayContentProvider());
		permissionConfTable.setLabelProvider(new ITableLabelProvider() {
			// Start of user code for label provider definition for
			// PermissionConfTable
			public String getColumnText(Object object, int columnIndex) {
				// Start IRR
				/*
				 * AdapterFactoryLabelProvider labelProvider = new
				 * AdapterFactoryLabelProvider(adapterFactory); if (object
				 * instanceof EObject) { switch (columnIndex) { case 0: return
				 * labelProvider.getText(object); } }
				 */

				if (object instanceof EObject) {
					PermissionConfig permissionConf = (PermissionConfig) object;
					switch (columnIndex) {
					case 0:
						return permissionConf.getName();
					case 1:
						if (permissionConf.isIsActive())
							return "Yes";
						else
							return "No";

					}
				}
				// End IRR

				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		permissionConfTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (permissionConfTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) permissionConfTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PermissionConfigurationsPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						permissionConfTable.refresh();
					}
				}
			}

		});
		GridData permissionConfTableData = new GridData(GridData.FILL_HORIZONTAL);
		permissionConfTableData.minimumHeight = 120;
		permissionConfTableData.heightHint = 120;
		permissionConfTable.getTable().setLayoutData(permissionConfTableData);
		for (ViewerFilter filter : this.permissionConfTableFilters) {
			permissionConfTable.addFilter(filter);
		}
		EditingUtils.setID(permissionConfTable.getTable(), AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable);
		EditingUtils.setEEFtype(permissionConfTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createPermissionConfTablePanel(widgetFactory, tableContainer);
		// Start of user code for createPermissionConfTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createPermissionConfTablePanel(FormToolkit widgetFactory, Composite container) {
		Composite permissionConfTablePanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout permissionConfTablePanelLayout = new GridLayout();
		permissionConfTablePanelLayout.numColumns = 1;
		permissionConfTablePanel.setLayout(permissionConfTablePanelLayout);
		addPermissionConfTable = widgetFactory.createButton(permissionConfTablePanel, AssuranceprojectMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addPermissionConfTableData = new GridData(GridData.FILL_HORIZONTAL);
		addPermissionConfTable.setLayoutData(addPermissionConfTableData);
		addPermissionConfTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PermissionConfigurationsPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				permissionConfTable.refresh();
			}
		});
		EditingUtils.setID(addPermissionConfTable, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable);
		EditingUtils.setEEFtype(addPermissionConfTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removePermissionConfTable = widgetFactory.createButton(permissionConfTablePanel, AssuranceprojectMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removePermissionConfTableData = new GridData(GridData.FILL_HORIZONTAL);
		removePermissionConfTable.setLayoutData(removePermissionConfTableData);
		removePermissionConfTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (permissionConfTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) permissionConfTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PermissionConfigurationsPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						permissionConfTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removePermissionConfTable, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable);
		EditingUtils.setEEFtype(removePermissionConfTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editPermissionConfTable = widgetFactory.createButton(permissionConfTablePanel, AssuranceprojectMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editPermissionConfTableData = new GridData(GridData.FILL_HORIZONTAL);
		editPermissionConfTable.setLayoutData(editPermissionConfTableData);
		editPermissionConfTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (permissionConfTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) permissionConfTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PermissionConfigurationsPropertiesEditionPartForm.this, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						permissionConfTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editPermissionConfTable, AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable);
		EditingUtils.setEEFtype(editPermissionConfTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createPermissionConfTablePanel

		// End of user code
		return permissionConfTablePanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart#initPermissionConf(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initPermissionConf(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		permissionConf.setContentProvider(contentProvider);
		permissionConf.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConf);
		if (eefElementEditorReadOnlyState && permissionConf.isEnabled()) {
			permissionConf.setEnabled(false);
			permissionConf.setToolTipText(AssuranceprojectMessages.PermissionConfigurations_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !permissionConf.isEnabled()) {
			permissionConf.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart#updatePermissionConf()
	 * 
	 */
	public void updatePermissionConf() {
	permissionConf.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart#addFilterPermissionConf(ViewerFilter filter)
	 * 
	 */
	public void addFilterToPermissionConf(ViewerFilter filter) {
		permissionConfFilters.add(filter);
		if (this.permissionConf != null) {
			this.permissionConf.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart#addBusinessFilterPermissionConf(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToPermissionConf(ViewerFilter filter) {
		permissionConfBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart#isContainedInPermissionConfTable(EObject element)
	 * 
	 */
	public boolean isContainedInPermissionConfTable(EObject element) {
		return ((ReferencesTableSettings)permissionConf.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart#initPermissionConfTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initPermissionConfTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		permissionConfTable.setContentProvider(contentProvider);
		permissionConfTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.PermissionConfigurations.Properties.permissionConfTable);
		if (eefElementEditorReadOnlyState && permissionConfTable.getTable().isEnabled()) {
			permissionConfTable.getTable().setEnabled(false);
			permissionConfTable.getTable().setToolTipText(AssuranceprojectMessages.PermissionConfigurations_ReadOnly);
			addPermissionConfTable.setEnabled(false);
			addPermissionConfTable.setToolTipText(AssuranceprojectMessages.PermissionConfigurations_ReadOnly);
			removePermissionConfTable.setEnabled(false);
			removePermissionConfTable.setToolTipText(AssuranceprojectMessages.PermissionConfigurations_ReadOnly);
			editPermissionConfTable.setEnabled(false);
			editPermissionConfTable.setToolTipText(AssuranceprojectMessages.PermissionConfigurations_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !permissionConfTable.getTable().isEnabled()) {
			permissionConfTable.getTable().setEnabled(true);
			addPermissionConfTable.setEnabled(true);
			removePermissionConfTable.setEnabled(true);
			editPermissionConfTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart#updatePermissionConfTable()
	 * 
	 */
	public void updatePermissionConfTable() {
	permissionConfTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart#addFilterPermissionConfTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToPermissionConfTable(ViewerFilter filter) {
		permissionConfTableFilters.add(filter);
		if (this.permissionConfTable != null) {
			this.permissionConfTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart#addBusinessFilterPermissionConfTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToPermissionConfTable(ViewerFilter filter) {
		permissionConfTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart#isContainedInPermissionConfTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInPermissionConfTableTable(EObject element) {
		return ((ReferencesTableSettings)permissionConfTable.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceprojectMessages.PermissionConfigurations_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
