/*******************************************************************************
 *
 * Copyright (c) 2017 Intecs SpA 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.contracts.validation;

import org.eclipse.emf.validation.model.IClientSelector;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;

public class ValidationDelegateClientSelector implements IClientSelector {

	
	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.model.IClientSelector#selects(java.lang.Object)
	 */
	public boolean selects(Object object) {
		if (!(object instanceof Element))
			return false;
		if (((Element) object) instanceof Model){
			Object obj = ((Model) object).getAppliedProfile("CHESS");
			if (obj != null)
				return true;
		}
		if ( ((Element) object).getModel() == null)
			return false;
		
		if (((Element) object).getModel().getAppliedProfile("CHESS") != null)
			return true;
	
		return false;
	}

}
