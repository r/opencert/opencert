/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;

/**
 * @generated
 */
public class ArgEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (ArgVisualIDRegistry.getVisualID(view)) {

			case CaseEditPart.VISUAL_ID:
				return new CaseEditPart(view);

			case ClaimEditPart.VISUAL_ID:
				return new ClaimEditPart(view);

			case ClaimIdEditPart.VISUAL_ID:
				return new ClaimIdEditPart(view);

			case ClaimDescriptionEditPart.VISUAL_ID:
				return new ClaimDescriptionEditPart(view);

			case AgreementEditPart.VISUAL_ID:
				return new AgreementEditPart(view);

			case AgreementIdEditPart.VISUAL_ID:
				return new AgreementIdEditPart(view);

			case AgreementDescriptionEditPart.VISUAL_ID:
				return new AgreementDescriptionEditPart(view);

			case ArgumentReasoningEditPart.VISUAL_ID:
				return new ArgumentReasoningEditPart(view);

			case ArgumentReasoningIdEditPart.VISUAL_ID:
				return new ArgumentReasoningIdEditPart(view);

			case ArgumentReasoningDescriptionEditPart.VISUAL_ID:
				return new ArgumentReasoningDescriptionEditPart(view);

			case ArgumentationEditPart.VISUAL_ID:
				return new ArgumentationEditPart(view);

			case ArgumentationIdEditPart.VISUAL_ID:
				return new ArgumentationIdEditPart(view);

			case ArgumentationDescriptionEditPart.VISUAL_ID:
				return new ArgumentationDescriptionEditPart(view);

			case InformationElementCitationEditPart.VISUAL_ID:
				return new InformationElementCitationEditPart(view);

			case InformationElementCitationIdEditPart.VISUAL_ID:
				return new InformationElementCitationIdEditPart(view);

			case InformationElementCitationDescriptionEditPart.VISUAL_ID:
				return new InformationElementCitationDescriptionEditPart(view);

			case ArgumentElementCitationEditPart.VISUAL_ID:
				return new ArgumentElementCitationEditPart(view);

			case ArgumentElementCitationIdEditPart.VISUAL_ID:
				return new ArgumentElementCitationIdEditPart(view);

			case ArgumentElementCitationDescriptionEditPart.VISUAL_ID:
				return new ArgumentElementCitationDescriptionEditPart(view);

			case ArgumentElementCitationArgumentationReferenceEditPart.VISUAL_ID:
				return new ArgumentElementCitationArgumentationReferenceEditPart(
						view);

			case ChoiceEditPart.VISUAL_ID:
				return new ChoiceEditPart(view);

			case ChoiceOptionalityEditPart.VISUAL_ID:
				return new ChoiceOptionalityEditPart(view);

			case ChoiceDescriptionEditPart.VISUAL_ID:
				return new ChoiceDescriptionEditPart(view);

			case Choice2EditPart.VISUAL_ID:
				return new Choice2EditPart(view);

			case ChoiceOptionality2EditPart.VISUAL_ID:
				return new ChoiceOptionality2EditPart(view);

			case ChoiceDescription2EditPart.VISUAL_ID:
				return new ChoiceDescription2EditPart(view);

			case AssertedInferenceEditPart.VISUAL_ID:
				return new AssertedInferenceEditPart(view);

			case AssertedInferenceCardinalityEditPart.VISUAL_ID:
				return new AssertedInferenceCardinalityEditPart(view);

			case AssertedEvidenceEditPart.VISUAL_ID:
				return new AssertedEvidenceEditPart(view);

			case AssertedEvidenceCardinalityEditPart.VISUAL_ID:
				return new AssertedEvidenceCardinalityEditPart(view);

			case AssertedContextEditPart.VISUAL_ID:
				return new AssertedContextEditPart(view);

			case AssertedContextCardinalityEditPart.VISUAL_ID:
				return new AssertedContextCardinalityEditPart(view);

			case AssertedChallengeEditPart.VISUAL_ID:
				return new AssertedChallengeEditPart(view);

			case AssertedCounterEvidenceEditPart.VISUAL_ID:
				return new AssertedCounterEvidenceEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}

}
