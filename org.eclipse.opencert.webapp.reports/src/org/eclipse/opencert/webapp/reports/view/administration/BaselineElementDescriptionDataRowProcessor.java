/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.administration;

import java.util.List;

import org.eclipse.opencert.webapp.reports.manager.BaseArtefactManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.IComplianceConsts;
import org.eclipse.opencert.webapp.reports.manager.compliance.IDataRowProcessor;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.Image;

public class BaselineElementDescriptionDataRowProcessor
        implements IDataRowProcessor<BaselineElementDataRow>
{
    private ProjectBaselineArtefactTable projectBaselineArtefactTable;

    public BaselineElementDescriptionDataRowProcessor(ProjectBaselineArtefactTable projectBaselineArtefactTable)
    {
        this.projectBaselineArtefactTable = projectBaselineArtefactTable;
    }

   @Override
    public void initEmptyTableDataContainer(HierarchicalContainer hc)
    {
        hc.addContainerProperty(IComplianceConsts.CDO_PROPERTY_ID, String.class, "");
        hc.addContainerProperty(IComplianceConsts.NAME_PROPERTY_ID, String.class, "");
        hc.addContainerProperty(IComplianceConsts.DESCRIPTION_PROPERTY_ID, String.class, "");
        hc.addContainerProperty(IComplianceConsts.RELATIONS_PROPERTY_ID, BaseArtefactRelationsField.class, new BaseArtefactRelationsField());
        hc.addContainerProperty(IComplianceConsts.BUTTON_PANEL_PROPERTY_ID, EditRowPanel.class, "");
    } 

    @Override
    public BaselineElementDataRow createBaseAssetDataRow(long itemId, BaseAssurableElement baseAsset, BaselineElementDataRow parentDataRow, Image artefactImage)
    {
        if (baseAsset instanceof BaseActivity || (baseAsset instanceof BaseRequirement)) {
            return null;
        }
        final BaseArtefactManager baseArtefactManager = (BaseArtefactManager)SpringContextHelper.getBeanFromWebappContext(BaseArtefactManager.SPRING_NAME);
        final List<BaseArtefact> affectedBaseArtefacts = baseArtefactManager.getAfftectedBaseArtefacts(itemId, ProjectAdministrationPage.THE_EDITOR_SUPPORTED_IA_EFFECT);
        final BaseArtefactRelationsField baseArtefactRelationsField = new BaseArtefactRelationsField(itemId, affectedBaseArtefacts);
        
        final EditRowPanel editRowPanel = new EditRowPanel();
        BaseArtefact bArtefact = (BaseArtefact)baseAsset;
        
        final BaselineElementDataRow dataRow = new BaselineElementDataRow(itemId, bArtefact.getName(), bArtefact.getDescription(), editRowPanel, baseArtefactRelationsField);
        editRowPanel.setButtonListeners(dataRow, projectBaselineArtefactTable);
        
        return dataRow;
    }

    @Override
    public void setItemPropsFromDataRow(Item item, BaselineElementDataRow baseAssetDataRow)
    {
        item.getItemProperty(IComplianceConsts.CDO_PROPERTY_ID).setValue(baseAssetDataRow.getCdoIDForGUI());
        item.getItemProperty(IComplianceConsts.NAME_PROPERTY_ID).setValue(baseAssetDataRow.getName());
        item.getItemProperty(IComplianceConsts.DESCRIPTION_PROPERTY_ID).setValue(baseAssetDataRow.getDescription());
        item.getItemProperty(IComplianceConsts.RELATIONS_PROPERTY_ID).setValue(baseAssetDataRow.getRelationsField());
        item.getItemProperty(IComplianceConsts.BUTTON_PANEL_PROPERTY_ID).setValue(baseAssetDataRow.getEditRowPanel());
    }

    @Override
    public void generateParentChildRelationsInContainer(HierarchicalContainer hierarchicalContainer)
    {
        // do nothing
    }

    @Override
    public boolean supportsActivities()
    {
        return false;
    }
}