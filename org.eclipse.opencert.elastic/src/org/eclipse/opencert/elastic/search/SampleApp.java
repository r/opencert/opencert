/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.search;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

/**
 * Sample application that shows how to use the API - it's NOT designed to
 * really work, just to show the API usage.
 *
 * @author mauersberger
 */
public class SampleApp {

	public void testAPI() throws Exception {
		// the user input
		String query = "Hazard Ana*";

		// have some fake data
		EObject[] fakeData = { null }; // TODO use some structure in CDO as fake data

		// get the finder
		ElasticFinder finder = ElasticFinderImpl.onDummy(fakeData);

		Map<String, Object> filters = new HashMap<>();
		filters.put("_type", "Artifact");
		filters.put("asil", "A");

		Set<Hit> hits = finder.search(query, filters);
		HitResolution<EObject> resolution = HitResolution.on(fakeData);
		resolution.using(new EMFObjectResolver());

		// resolve all hits with score >= 2
		Set<EObject> resolved = resolution.resolve(hits, 2);
		System.out.println(resolved);
	}
}
