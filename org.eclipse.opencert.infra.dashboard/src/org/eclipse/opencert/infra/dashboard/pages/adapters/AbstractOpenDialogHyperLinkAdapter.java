/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages.adapters;

import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;

/**
 * A {@link HyperlinkAdapter} to open a dialog.
 */
public abstract class AbstractOpenDialogHyperLinkAdapter extends HyperlinkAdapter {

	/** ID of the project explorer view. */
	protected static final String PROJECT_EXPLORER_VIEW_ID = "org.eclipse.ui.navigator.ProjectExplorer"; //$NON-NLS-1$

	/** ID of the page standard compliance definition. */
	protected static final String STANDARD_COMPLIANCE_DEFINITION_PAGE_ID = "org.eclipse.opencert.infra.dashboard.pages.standard.compliance.definition.page"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	public AbstractOpenDialogHyperLinkAdapter() {
		super();
	}

	/**
	 * Open a dialog when the link is pressed.
	 */
	protected abstract void openDialog();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void linkActivated(final HyperlinkEvent pEvent) {
		openDialog();
	}
}
