/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.graphics.Image;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;

// End of user code

/**
 * 
 * 
 */
public class BaseRequerimentComplianceMapPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, BaseRequerimentComplianceMapPropertiesEditionPart {

	protected ReferencesTable complianceMap;
	protected List<ViewerFilter> complianceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> complianceMapFilters = new ArrayList<ViewerFilter>();
	protected TableViewer requirementComplianceMapTable;
	protected List<ViewerFilter> requirementComplianceMapTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> requirementComplianceMapTableFilters = new ArrayList<ViewerFilter>();
	protected Button addRequirementComplianceMapTable;
	protected Button removeRequirementComplianceMapTable;
	protected Button editRequirementComplianceMapTable;



	/**
	 * For {@link ISection} use only.
	 */
	public BaseRequerimentComplianceMapPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseRequerimentComplianceMapPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence baseRequerimentComplianceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseRequerimentComplianceMapStep.addStep(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable);
		
		
		composer = new PartComposer(baseRequerimentComplianceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap) {
					return createComplianceMapTableComposition(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable) {
					return createRequirementComplianceMapTableTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(BaselineMessages.BaseRequerimentComplianceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createComplianceMapTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.complianceMap = new ReferencesTable(getDescription(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap, BaselineMessages.BaseRequerimentComplianceMapPropertiesEditionPart_ComplianceMapLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequerimentComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				complianceMap.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequerimentComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				complianceMap.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequerimentComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				complianceMap.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequerimentComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				complianceMap.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.complianceMapFilters) {
			this.complianceMap.addFilter(filter);
		}
		this.complianceMap.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap, BaselineViewsRepository.FORM_KIND));
		this.complianceMap.createControls(parent, widgetFactory);
		this.complianceMap.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequerimentComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData complianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		complianceMapData.horizontalSpan = 3;
		this.complianceMap.setLayoutData(complianceMapData);
		this.complianceMap.setLowerBound(0);
		this.complianceMap.setUpperBound(-1);
		complianceMap.setID(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap);
		complianceMap.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createComplianceMapTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRequirementComplianceMapTableTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableRequirementComplianceMapTable = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableRequirementComplianceMapTable.setHeaderVisible(true);
		GridData gdRequirementComplianceMapTable = new GridData();
		gdRequirementComplianceMapTable.grabExcessHorizontalSpace = true;
		gdRequirementComplianceMapTable.horizontalAlignment = GridData.FILL;
		gdRequirementComplianceMapTable.grabExcessVerticalSpace = true;
		gdRequirementComplianceMapTable.verticalAlignment = GridData.FILL;
		tableRequirementComplianceMapTable.setLayoutData(gdRequirementComplianceMapTable);
		tableRequirementComplianceMapTable.setLinesVisible(true);

		// Start of user code for columns definition for RequirementComplianceMapTable
				TableColumn name = new TableColumn(tableRequirementComplianceMapTable, SWT.NONE);
				name.setWidth(80);
				name.setText("Label"); //$NON-NLS-1$
		// End of user code

		requirementComplianceMapTable = new TableViewer(tableRequirementComplianceMapTable);
		requirementComplianceMapTable.setContentProvider(new ArrayContentProvider());
		requirementComplianceMapTable.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for RequirementComplianceMapTable
						public String getColumnText(Object object, int columnIndex) {
							AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
							if (object instanceof EObject) {
								switch (columnIndex) {
								case 0:
									return labelProvider.getText(object);
								}
							}
							return ""; //$NON-NLS-1$
						}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		requirementComplianceMapTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (requirementComplianceMapTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) requirementComplianceMapTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequerimentComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						requirementComplianceMapTable.refresh();
					}
				}
			}

		});
		GridData requirementComplianceMapTableData = new GridData(GridData.FILL_HORIZONTAL);
		requirementComplianceMapTableData.minimumHeight = 120;
		requirementComplianceMapTableData.heightHint = 120;
		requirementComplianceMapTable.getTable().setLayoutData(requirementComplianceMapTableData);
		for (ViewerFilter filter : this.requirementComplianceMapTableFilters) {
			requirementComplianceMapTable.addFilter(filter);
		}
		EditingUtils.setID(requirementComplianceMapTable.getTable(), BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable);
		EditingUtils.setEEFtype(requirementComplianceMapTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createRequirementComplianceMapTablePanel(widgetFactory, tableContainer);
		// Start of user code for createRequirementComplianceMapTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRequirementComplianceMapTablePanel(FormToolkit widgetFactory, Composite container) {
		Composite requirementComplianceMapTablePanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout requirementComplianceMapTablePanelLayout = new GridLayout();
		requirementComplianceMapTablePanelLayout.numColumns = 1;
		requirementComplianceMapTablePanel.setLayout(requirementComplianceMapTablePanelLayout);
		addRequirementComplianceMapTable = widgetFactory.createButton(requirementComplianceMapTablePanel, BaselineMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addRequirementComplianceMapTableData = new GridData(GridData.FILL_HORIZONTAL);
		addRequirementComplianceMapTable.setLayoutData(addRequirementComplianceMapTableData);
		addRequirementComplianceMapTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequerimentComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				requirementComplianceMapTable.refresh();
			}
		});
		EditingUtils.setID(addRequirementComplianceMapTable, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable);
		EditingUtils.setEEFtype(addRequirementComplianceMapTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeRequirementComplianceMapTable = widgetFactory.createButton(requirementComplianceMapTablePanel, BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeRequirementComplianceMapTableData = new GridData(GridData.FILL_HORIZONTAL);
		removeRequirementComplianceMapTable.setLayoutData(removeRequirementComplianceMapTableData);
		removeRequirementComplianceMapTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (requirementComplianceMapTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) requirementComplianceMapTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequerimentComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						requirementComplianceMapTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeRequirementComplianceMapTable, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable);
		EditingUtils.setEEFtype(removeRequirementComplianceMapTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editRequirementComplianceMapTable = widgetFactory.createButton(requirementComplianceMapTablePanel, BaselineMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editRequirementComplianceMapTableData = new GridData(GridData.FILL_HORIZONTAL);
		editRequirementComplianceMapTable.setLayoutData(editRequirementComplianceMapTableData);
		editRequirementComplianceMapTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (requirementComplianceMapTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) requirementComplianceMapTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequerimentComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						requirementComplianceMapTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editRequirementComplianceMapTable, BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable);
		EditingUtils.setEEFtype(editRequirementComplianceMapTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createRequirementComplianceMapTablePanel

		// End of user code
		return requirementComplianceMapTablePanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart#initComplianceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initComplianceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		complianceMap.setContentProvider(contentProvider);
		complianceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.complianceMap);
		if (eefElementEditorReadOnlyState && complianceMap.isEnabled()) {
			complianceMap.setEnabled(false);
			complianceMap.setToolTipText(BaselineMessages.BaseRequerimentComplianceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !complianceMap.isEnabled()) {
			complianceMap.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart#updateComplianceMap()
	 * 
	 */
	public void updateComplianceMap() {
	complianceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart#addFilterComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToComplianceMap(ViewerFilter filter) {
		complianceMapFilters.add(filter);
		if (this.complianceMap != null) {
			this.complianceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart#addBusinessFilterComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToComplianceMap(ViewerFilter filter) {
		complianceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart#isContainedInComplianceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInComplianceMapTable(EObject element) {
		return ((ReferencesTableSettings)complianceMap.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart#initRequirementComplianceMapTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initRequirementComplianceMapTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		requirementComplianceMapTable.setContentProvider(contentProvider);
		requirementComplianceMapTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequerimentComplianceMap.Properties.requirementComplianceMapTable);
		if (eefElementEditorReadOnlyState && requirementComplianceMapTable.getTable().isEnabled()) {
			requirementComplianceMapTable.getTable().setEnabled(false);
			requirementComplianceMapTable.getTable().setToolTipText(BaselineMessages.BaseRequerimentComplianceMap_ReadOnly);
			addRequirementComplianceMapTable.setEnabled(false);
			addRequirementComplianceMapTable.setToolTipText(BaselineMessages.BaseRequerimentComplianceMap_ReadOnly);
			removeRequirementComplianceMapTable.setEnabled(false);
			removeRequirementComplianceMapTable.setToolTipText(BaselineMessages.BaseRequerimentComplianceMap_ReadOnly);
			editRequirementComplianceMapTable.setEnabled(false);
			editRequirementComplianceMapTable.setToolTipText(BaselineMessages.BaseRequerimentComplianceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !requirementComplianceMapTable.getTable().isEnabled()) {
			requirementComplianceMapTable.getTable().setEnabled(true);
			addRequirementComplianceMapTable.setEnabled(true);
			removeRequirementComplianceMapTable.setEnabled(true);
			editRequirementComplianceMapTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart#updateRequirementComplianceMapTable()
	 * 
	 */
	public void updateRequirementComplianceMapTable() {
	requirementComplianceMapTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart#addFilterRequirementComplianceMapTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToRequirementComplianceMapTable(ViewerFilter filter) {
		requirementComplianceMapTableFilters.add(filter);
		if (this.requirementComplianceMapTable != null) {
			this.requirementComplianceMapTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart#addBusinessFilterRequirementComplianceMapTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToRequirementComplianceMapTable(ViewerFilter filter) {
		requirementComplianceMapTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequerimentComplianceMapPropertiesEditionPart#isContainedInRequirementComplianceMapTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInRequirementComplianceMapTableTable(EObject element) {
		return ((ReferencesTableSettings)requirementComplianceMapTable.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseRequerimentComplianceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
