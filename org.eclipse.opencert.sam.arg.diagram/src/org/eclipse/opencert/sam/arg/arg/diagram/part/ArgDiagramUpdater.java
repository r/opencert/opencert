/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.ArgumentReasoning;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.AssertedChallenge;
import org.eclipse.opencert.sam.arg.arg.AssertedContext;
import org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence;
import org.eclipse.opencert.sam.arg.arg.AssertedEvidence;
import org.eclipse.opencert.sam.arg.arg.AssertedInference;
import org.eclipse.opencert.sam.arg.arg.Assertion;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.Choice;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedChallengeEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedContextEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedCounterEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedInferenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.CaseEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.Choice2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;

/**
 * @generated
 */
public class ArgDiagramUpdater {

	/**
	 * @generated
	 */
	public static boolean isShortcutOrphaned(View view) {
		return !view.isSetElement() || view.getElement() == null
				|| view.getElement().eIsProxy();
	}

	/**
	 * @generated
	 */
	public static List<ArgNodeDescriptor> getSemanticChildren(View view) {
		switch (ArgVisualIDRegistry.getVisualID(view)) {
		case CaseEditPart.VISUAL_ID:
			return getCase_1000SemanticChildren(view);
		case ClaimEditPart.VISUAL_ID:
			return getClaim_2001SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgNodeDescriptor> getCase_1000SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		Case modelElement = (Case) view.getElement();
		LinkedList<ArgNodeDescriptor> result = new LinkedList<ArgNodeDescriptor>();
		for (Iterator<?> it = modelElement.getArgument().iterator(); it
				.hasNext();) {
			ArgumentElement childElement = (ArgumentElement) it.next();
			int visualID = ArgVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ClaimEditPart.VISUAL_ID) {
				result.add(new ArgNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ArgumentReasoningEditPart.VISUAL_ID) {
				result.add(new ArgNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == InformationElementCitationEditPart.VISUAL_ID) {
				result.add(new ArgNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ArgumentElementCitationEditPart.VISUAL_ID) {
				result.add(new ArgNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ChoiceEditPart.VISUAL_ID) {
				result.add(new ArgNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getArgumentation().iterator(); it
				.hasNext();) {
			Argumentation childElement = (Argumentation) it.next();
			int visualID = ArgVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == AgreementEditPart.VISUAL_ID) {
				result.add(new ArgNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ArgumentationEditPart.VISUAL_ID) {
				result.add(new ArgNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgNodeDescriptor> getClaim_2001SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		Claim modelElement = (Claim) view.getElement();
		LinkedList<ArgNodeDescriptor> result = new LinkedList<ArgNodeDescriptor>();
		{
			Choice childElement = modelElement.getChoice();
			int visualID = ArgVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Choice2EditPart.VISUAL_ID) {
				result.add(new ArgNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getContainedLinks(View view) {
		switch (ArgVisualIDRegistry.getVisualID(view)) {
		case CaseEditPart.VISUAL_ID:
			return getCase_1000ContainedLinks(view);
		case ClaimEditPart.VISUAL_ID:
			return getClaim_2001ContainedLinks(view);
		case AgreementEditPart.VISUAL_ID:
			return getAgreement_2002ContainedLinks(view);
		case ArgumentReasoningEditPart.VISUAL_ID:
			return getArgumentReasoning_2003ContainedLinks(view);
		case ArgumentationEditPart.VISUAL_ID:
			return getArgumentation_2004ContainedLinks(view);
		case InformationElementCitationEditPart.VISUAL_ID:
			return getInformationElementCitation_2005ContainedLinks(view);
		case ArgumentElementCitationEditPart.VISUAL_ID:
			return getArgumentElementCitation_2006ContainedLinks(view);
		case ChoiceEditPart.VISUAL_ID:
			return getChoice_2007ContainedLinks(view);
		case Choice2EditPart.VISUAL_ID:
			return getChoice_3001ContainedLinks(view);
		case AssertedInferenceEditPart.VISUAL_ID:
			return getAssertedInference_4001ContainedLinks(view);
		case AssertedEvidenceEditPart.VISUAL_ID:
			return getAssertedEvidence_4002ContainedLinks(view);
		case AssertedContextEditPart.VISUAL_ID:
			return getAssertedContext_4003ContainedLinks(view);
		case AssertedChallengeEditPart.VISUAL_ID:
			return getAssertedChallenge_4004ContainedLinks(view);
		case AssertedCounterEvidenceEditPart.VISUAL_ID:
			return getAssertedCounterEvidence_4005ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getIncomingLinks(View view) {
		switch (ArgVisualIDRegistry.getVisualID(view)) {
		case ClaimEditPart.VISUAL_ID:
			return getClaim_2001IncomingLinks(view);
		case AgreementEditPart.VISUAL_ID:
			return getAgreement_2002IncomingLinks(view);
		case ArgumentReasoningEditPart.VISUAL_ID:
			return getArgumentReasoning_2003IncomingLinks(view);
		case ArgumentationEditPart.VISUAL_ID:
			return getArgumentation_2004IncomingLinks(view);
		case InformationElementCitationEditPart.VISUAL_ID:
			return getInformationElementCitation_2005IncomingLinks(view);
		case ArgumentElementCitationEditPart.VISUAL_ID:
			return getArgumentElementCitation_2006IncomingLinks(view);
		case ChoiceEditPart.VISUAL_ID:
			return getChoice_2007IncomingLinks(view);
		case Choice2EditPart.VISUAL_ID:
			return getChoice_3001IncomingLinks(view);
		case AssertedInferenceEditPart.VISUAL_ID:
			return getAssertedInference_4001IncomingLinks(view);
		case AssertedEvidenceEditPart.VISUAL_ID:
			return getAssertedEvidence_4002IncomingLinks(view);
		case AssertedContextEditPart.VISUAL_ID:
			return getAssertedContext_4003IncomingLinks(view);
		case AssertedChallengeEditPart.VISUAL_ID:
			return getAssertedChallenge_4004IncomingLinks(view);
		case AssertedCounterEvidenceEditPart.VISUAL_ID:
			return getAssertedCounterEvidence_4005IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getOutgoingLinks(View view) {
		switch (ArgVisualIDRegistry.getVisualID(view)) {
		case ClaimEditPart.VISUAL_ID:
			return getClaim_2001OutgoingLinks(view);
		case AgreementEditPart.VISUAL_ID:
			return getAgreement_2002OutgoingLinks(view);
		case ArgumentReasoningEditPart.VISUAL_ID:
			return getArgumentReasoning_2003OutgoingLinks(view);
		case ArgumentationEditPart.VISUAL_ID:
			return getArgumentation_2004OutgoingLinks(view);
		case InformationElementCitationEditPart.VISUAL_ID:
			return getInformationElementCitation_2005OutgoingLinks(view);
		case ArgumentElementCitationEditPart.VISUAL_ID:
			return getArgumentElementCitation_2006OutgoingLinks(view);
		case ChoiceEditPart.VISUAL_ID:
			return getChoice_2007OutgoingLinks(view);
		case Choice2EditPart.VISUAL_ID:
			return getChoice_3001OutgoingLinks(view);
		case AssertedInferenceEditPart.VISUAL_ID:
			return getAssertedInference_4001OutgoingLinks(view);
		case AssertedEvidenceEditPart.VISUAL_ID:
			return getAssertedEvidence_4002OutgoingLinks(view);
		case AssertedContextEditPart.VISUAL_ID:
			return getAssertedContext_4003OutgoingLinks(view);
		case AssertedChallengeEditPart.VISUAL_ID:
			return getAssertedChallenge_4004OutgoingLinks(view);
		case AssertedCounterEvidenceEditPart.VISUAL_ID:
			return getAssertedCounterEvidence_4005OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getCase_1000ContainedLinks(View view) {
		Case modelElement = (Case) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_AssertedContext_4003(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_AssertedChallenge_4004(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_AssertedCounterEvidence_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getClaim_2001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAgreement_2002ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getArgumentReasoning_2003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getArgumentation_2004ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getInformationElementCitation_2005ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getArgumentElementCitation_2006ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getChoice_2007ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getChoice_3001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedInference_4001ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedEvidence_4002ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedContext_4003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedChallenge_4004ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedCounterEvidence_4005ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getClaim_2001IncomingLinks(View view) {
		Claim modelElement = (Claim) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedChallenge_4004(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedCounterEvidence_4005(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAgreement_2002IncomingLinks(
			View view) {
		Agreement modelElement = (Agreement) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getArgumentReasoning_2003IncomingLinks(
			View view) {
		ArgumentReasoning modelElement = (ArgumentReasoning) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getArgumentation_2004IncomingLinks(
			View view) {
		Argumentation modelElement = (Argumentation) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getInformationElementCitation_2005IncomingLinks(
			View view) {
		InformationElementCitation modelElement = (InformationElementCitation) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getArgumentElementCitation_2006IncomingLinks(
			View view) {
		ArgumentElementCitation modelElement = (ArgumentElementCitation) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getChoice_2007IncomingLinks(View view) {
		Choice modelElement = (Choice) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getChoice_3001IncomingLinks(View view) {
		Choice modelElement = (Choice) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedInference_4001IncomingLinks(
			View view) {
		AssertedInference modelElement = (AssertedInference) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedChallenge_4004(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedCounterEvidence_4005(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedEvidence_4002IncomingLinks(
			View view) {
		AssertedEvidence modelElement = (AssertedEvidence) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedChallenge_4004(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedCounterEvidence_4005(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedContext_4003IncomingLinks(
			View view) {
		AssertedContext modelElement = (AssertedContext) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedChallenge_4004(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedCounterEvidence_4005(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedChallenge_4004IncomingLinks(
			View view) {
		AssertedChallenge modelElement = (AssertedChallenge) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedChallenge_4004(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedCounterEvidence_4005(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedCounterEvidence_4005IncomingLinks(
			View view) {
		AssertedCounterEvidence modelElement = (AssertedCounterEvidence) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_AssertedInference_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedContext_4003(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedChallenge_4004(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AssertedCounterEvidence_4005(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getClaim_2001OutgoingLinks(View view) {
		Claim modelElement = (Claim) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedChallenge_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAgreement_2002OutgoingLinks(
			View view) {
		Agreement modelElement = (Agreement) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getArgumentReasoning_2003OutgoingLinks(
			View view) {
		ArgumentReasoning modelElement = (ArgumentReasoning) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getArgumentation_2004OutgoingLinks(
			View view) {
		Argumentation modelElement = (Argumentation) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getInformationElementCitation_2005OutgoingLinks(
			View view) {
		InformationElementCitation modelElement = (InformationElementCitation) view
				.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedCounterEvidence_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getArgumentElementCitation_2006OutgoingLinks(
			View view) {
		ArgumentElementCitation modelElement = (ArgumentElementCitation) view
				.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getChoice_2007OutgoingLinks(View view) {
		Choice modelElement = (Choice) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getChoice_3001OutgoingLinks(View view) {
		Choice modelElement = (Choice) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedInference_4001OutgoingLinks(
			View view) {
		AssertedInference modelElement = (AssertedInference) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedEvidence_4002OutgoingLinks(
			View view) {
		AssertedEvidence modelElement = (AssertedEvidence) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedContext_4003OutgoingLinks(
			View view) {
		AssertedContext modelElement = (AssertedContext) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedChallenge_4004OutgoingLinks(
			View view) {
		AssertedChallenge modelElement = (AssertedChallenge) view.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<ArgLinkDescriptor> getAssertedCounterEvidence_4005OutgoingLinks(
			View view) {
		AssertedCounterEvidence modelElement = (AssertedCounterEvidence) view
				.getElement();
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedInference_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AssertedContext_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getContainedTypeModelFacetLinks_AssertedInference_4001(
			Case container) {
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		for (Iterator<?> links = container.getArgument().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof AssertedInference) {
				continue;
			}
			AssertedInference link = (AssertedInference) linkObject;
			if (AssertedInferenceEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List targets = link.getTarget();
			Object theTarget = targets.size() == 1 ? targets.get(0) : null;
			if (false == theTarget instanceof ArgumentationElement) {
				continue;
			}
			ArgumentationElement dst = (ArgumentationElement) theTarget;
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof ArgumentationElement) {
				continue;
			}
			ArgumentationElement src = (ArgumentationElement) theSource;
			result.add(new ArgLinkDescriptor(src, dst, link,
					ArgElementTypes.AssertedInference_4001,
					AssertedInferenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getContainedTypeModelFacetLinks_AssertedEvidence_4002(
			Case container) {
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		for (Iterator<?> links = container.getArgument().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof AssertedEvidence) {
				continue;
			}
			AssertedEvidence link = (AssertedEvidence) linkObject;
			if (AssertedEvidenceEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List targets = link.getTarget();
			Object theTarget = targets.size() == 1 ? targets.get(0) : null;
			if (false == theTarget instanceof ArgumentElement) {
				continue;
			}
			ArgumentElement dst = (ArgumentElement) theTarget;
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof ArgumentElement) {
				continue;
			}
			ArgumentElement src = (ArgumentElement) theSource;
			result.add(new ArgLinkDescriptor(src, dst, link,
					ArgElementTypes.AssertedEvidence_4002,
					AssertedEvidenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getContainedTypeModelFacetLinks_AssertedContext_4003(
			Case container) {
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		for (Iterator<?> links = container.getArgument().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof AssertedContext) {
				continue;
			}
			AssertedContext link = (AssertedContext) linkObject;
			if (AssertedContextEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List targets = link.getTarget();
			Object theTarget = targets.size() == 1 ? targets.get(0) : null;
			if (false == theTarget instanceof ArgumentationElement) {
				continue;
			}
			ArgumentationElement dst = (ArgumentationElement) theTarget;
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof ArgumentationElement) {
				continue;
			}
			ArgumentationElement src = (ArgumentationElement) theSource;
			result.add(new ArgLinkDescriptor(src, dst, link,
					ArgElementTypes.AssertedContext_4003,
					AssertedContextEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getContainedTypeModelFacetLinks_AssertedChallenge_4004(
			Case container) {
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		for (Iterator<?> links = container.getArgument().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof AssertedChallenge) {
				continue;
			}
			AssertedChallenge link = (AssertedChallenge) linkObject;
			if (AssertedChallengeEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List targets = link.getTarget();
			Object theTarget = targets.size() == 1 ? targets.get(0) : null;
			if (false == theTarget instanceof Assertion) {
				continue;
			}
			Assertion dst = (Assertion) theTarget;
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof Claim) {
				continue;
			}
			Claim src = (Claim) theSource;
			result.add(new ArgLinkDescriptor(src, dst, link,
					ArgElementTypes.AssertedChallenge_4004,
					AssertedChallengeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getContainedTypeModelFacetLinks_AssertedCounterEvidence_4005(
			Case container) {
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		for (Iterator<?> links = container.getArgument().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof AssertedCounterEvidence) {
				continue;
			}
			AssertedCounterEvidence link = (AssertedCounterEvidence) linkObject;
			if (AssertedCounterEvidenceEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List targets = link.getTarget();
			Object theTarget = targets.size() == 1 ? targets.get(0) : null;
			if (false == theTarget instanceof Assertion) {
				continue;
			}
			Assertion dst = (Assertion) theTarget;
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof InformationElementCitation) {
				continue;
			}
			InformationElementCitation src = (InformationElementCitation) theSource;
			result.add(new ArgLinkDescriptor(src, dst, link,
					ArgElementTypes.AssertedCounterEvidence_4005,
					AssertedCounterEvidenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getIncomingTypeModelFacetLinks_AssertedInference_4001(
			ArgumentationElement target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != ArgPackage.eINSTANCE
					.getAssertedInference_Target()
					|| false == setting.getEObject() instanceof AssertedInference) {
				continue;
			}
			AssertedInference link = (AssertedInference) setting.getEObject();
			if (AssertedInferenceEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof ArgumentationElement) {
				continue;
			}
			ArgumentationElement src = (ArgumentationElement) theSource;
			result.add(new ArgLinkDescriptor(src, target, link,
					ArgElementTypes.AssertedInference_4001,
					AssertedInferenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getIncomingTypeModelFacetLinks_AssertedEvidence_4002(
			ArgumentElement target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != ArgPackage.eINSTANCE
					.getAssertedEvidence_Target()
					|| false == setting.getEObject() instanceof AssertedEvidence) {
				continue;
			}
			AssertedEvidence link = (AssertedEvidence) setting.getEObject();
			if (AssertedEvidenceEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof ArgumentElement) {
				continue;
			}
			ArgumentElement src = (ArgumentElement) theSource;
			result.add(new ArgLinkDescriptor(src, target, link,
					ArgElementTypes.AssertedEvidence_4002,
					AssertedEvidenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getIncomingTypeModelFacetLinks_AssertedContext_4003(
			ArgumentationElement target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != ArgPackage.eINSTANCE
					.getAssertedContext_Target()
					|| false == setting.getEObject() instanceof AssertedContext) {
				continue;
			}
			AssertedContext link = (AssertedContext) setting.getEObject();
			if (AssertedContextEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof ArgumentationElement) {
				continue;
			}
			ArgumentationElement src = (ArgumentationElement) theSource;
			result.add(new ArgLinkDescriptor(src, target, link,
					ArgElementTypes.AssertedContext_4003,
					AssertedContextEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getIncomingTypeModelFacetLinks_AssertedChallenge_4004(
			Assertion target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != ArgPackage.eINSTANCE
					.getAssertedChallenge_Target()
					|| false == setting.getEObject() instanceof AssertedChallenge) {
				continue;
			}
			AssertedChallenge link = (AssertedChallenge) setting.getEObject();
			if (AssertedChallengeEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof Claim) {
				continue;
			}
			Claim src = (Claim) theSource;
			result.add(new ArgLinkDescriptor(src, target, link,
					ArgElementTypes.AssertedChallenge_4004,
					AssertedChallengeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getIncomingTypeModelFacetLinks_AssertedCounterEvidence_4005(
			Assertion target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != ArgPackage.eINSTANCE
					.getAssertedCounterEvidence_Target()
					|| false == setting.getEObject() instanceof AssertedCounterEvidence) {
				continue;
			}
			AssertedCounterEvidence link = (AssertedCounterEvidence) setting
					.getEObject();
			if (AssertedCounterEvidenceEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof InformationElementCitation) {
				continue;
			}
			InformationElementCitation src = (InformationElementCitation) theSource;
			result.add(new ArgLinkDescriptor(src, target, link,
					ArgElementTypes.AssertedCounterEvidence_4005,
					AssertedCounterEvidenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getOutgoingTypeModelFacetLinks_AssertedInference_4001(
			ArgumentationElement source) {
		Case container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Case) {
				container = (Case) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		for (Iterator<?> links = container.getArgument().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof AssertedInference) {
				continue;
			}
			AssertedInference link = (AssertedInference) linkObject;
			if (AssertedInferenceEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List targets = link.getTarget();
			Object theTarget = targets.size() == 1 ? targets.get(0) : null;
			if (false == theTarget instanceof ArgumentationElement) {
				continue;
			}
			ArgumentationElement dst = (ArgumentationElement) theTarget;
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof ArgumentationElement) {
				continue;
			}
			ArgumentationElement src = (ArgumentationElement) theSource;
			if (src != source) {
				continue;
			}
			result.add(new ArgLinkDescriptor(src, dst, link,
					ArgElementTypes.AssertedInference_4001,
					AssertedInferenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getOutgoingTypeModelFacetLinks_AssertedEvidence_4002(
			ArgumentElement source) {
		Case container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Case) {
				container = (Case) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		for (Iterator<?> links = container.getArgument().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof AssertedEvidence) {
				continue;
			}
			AssertedEvidence link = (AssertedEvidence) linkObject;
			if (AssertedEvidenceEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List targets = link.getTarget();
			Object theTarget = targets.size() == 1 ? targets.get(0) : null;
			if (false == theTarget instanceof ArgumentElement) {
				continue;
			}
			ArgumentElement dst = (ArgumentElement) theTarget;
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof ArgumentElement) {
				continue;
			}
			ArgumentElement src = (ArgumentElement) theSource;
			if (src != source) {
				continue;
			}
			result.add(new ArgLinkDescriptor(src, dst, link,
					ArgElementTypes.AssertedEvidence_4002,
					AssertedEvidenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getOutgoingTypeModelFacetLinks_AssertedContext_4003(
			ArgumentationElement source) {
		Case container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Case) {
				container = (Case) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		for (Iterator<?> links = container.getArgument().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof AssertedContext) {
				continue;
			}
			AssertedContext link = (AssertedContext) linkObject;
			if (AssertedContextEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List targets = link.getTarget();
			Object theTarget = targets.size() == 1 ? targets.get(0) : null;
			if (false == theTarget instanceof ArgumentationElement) {
				continue;
			}
			ArgumentationElement dst = (ArgumentationElement) theTarget;
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof ArgumentationElement) {
				continue;
			}
			ArgumentationElement src = (ArgumentationElement) theSource;
			if (src != source) {
				continue;
			}
			result.add(new ArgLinkDescriptor(src, dst, link,
					ArgElementTypes.AssertedContext_4003,
					AssertedContextEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getOutgoingTypeModelFacetLinks_AssertedChallenge_4004(
			Claim source) {
		Case container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Case) {
				container = (Case) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		for (Iterator<?> links = container.getArgument().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof AssertedChallenge) {
				continue;
			}
			AssertedChallenge link = (AssertedChallenge) linkObject;
			if (AssertedChallengeEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List targets = link.getTarget();
			Object theTarget = targets.size() == 1 ? targets.get(0) : null;
			if (false == theTarget instanceof Assertion) {
				continue;
			}
			Assertion dst = (Assertion) theTarget;
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof Claim) {
				continue;
			}
			Claim src = (Claim) theSource;
			if (src != source) {
				continue;
			}
			result.add(new ArgLinkDescriptor(src, dst, link,
					ArgElementTypes.AssertedChallenge_4004,
					AssertedChallengeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<ArgLinkDescriptor> getOutgoingTypeModelFacetLinks_AssertedCounterEvidence_4005(
			InformationElementCitation source) {
		Case container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Case) {
				container = (Case) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		for (Iterator<?> links = container.getArgument().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof AssertedCounterEvidence) {
				continue;
			}
			AssertedCounterEvidence link = (AssertedCounterEvidence) linkObject;
			if (AssertedCounterEvidenceEditPart.VISUAL_ID != ArgVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			List targets = link.getTarget();
			Object theTarget = targets.size() == 1 ? targets.get(0) : null;
			if (false == theTarget instanceof Assertion) {
				continue;
			}
			Assertion dst = (Assertion) theTarget;
			List sources = link.getSource();
			Object theSource = sources.size() == 1 ? sources.get(0) : null;
			if (false == theSource instanceof InformationElementCitation) {
				continue;
			}
			InformationElementCitation src = (InformationElementCitation) theSource;
			if (src != source) {
				continue;
			}
			result.add(new ArgLinkDescriptor(src, dst, link,
					ArgElementTypes.AssertedCounterEvidence_4005,
					AssertedCounterEvidenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<ArgNodeDescriptor> getSemanticChildren(View view) {
			return ArgDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<ArgLinkDescriptor> getContainedLinks(View view) {
			return ArgDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<ArgLinkDescriptor> getIncomingLinks(View view) {
			return ArgDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<ArgLinkDescriptor> getOutgoingLinks(View view) {
			return ArgDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
