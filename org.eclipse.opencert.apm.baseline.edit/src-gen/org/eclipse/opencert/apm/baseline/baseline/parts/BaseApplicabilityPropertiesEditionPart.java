/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;

import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface BaseApplicabilityPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);




	/**
	 * Init the applicCritic
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initApplicCritic(ReferencesTableSettings settings);

	/**
	 * Update the applicCritic
	 * @param newValue the applicCritic to update
	 * 
	 */
	public void updateApplicCritic();

	/**
	 * Adds the given filter to the applicCritic edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToApplicCritic(ViewerFilter filter);

	/**
	 * Adds the given filter to the applicCritic edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToApplicCritic(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the applicCritic table
	 * 
	 */
	public boolean isContainedInApplicCriticTable(EObject element);




	/**
	 * Init the applicCriticTable
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initApplicCriticTable(ReferencesTableSettings settings);

	/**
	 * Update the applicCriticTable
	 * @param newValue the applicCriticTable to update
	 * 
	 */
	public void updateApplicCriticTable();

	/**
	 * Adds the given filter to the applicCriticTable edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToApplicCriticTable(ViewerFilter filter);

	/**
	 * Adds the given filter to the applicCriticTable edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToApplicCriticTable(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the applicCriticTable table
	 * 
	 */
	public boolean isContainedInApplicCriticTableTable(EObject element);


	/**
	 * @return the comments
	 * 
	 */
	public String getComments();

	/**
	 * Defines a new comments
	 * @param newValue the new comments to set
	 * 
	 */
	public void setComments(String newValue);


	/**
	 * @return the applicTarget
	 * 
	 */
	public EObject getApplicTarget();

	/**
	 * Init the applicTarget
	 * @param settings the combo setting
	 */
	public void initApplicTarget(EObjectFlatComboSettings settings);

	/**
	 * Defines a new applicTarget
	 * @param newValue the new applicTarget to set
	 * 
	 */
	public void setApplicTarget(EObject newValue);

	/**
	 * Defines the button mode
	 * @param newValue the new mode to set
	 * 
	 */
	public void setApplicTargetButtonMode(ButtonsModeEnum newValue);

	/**
	 * Adds the given filter to the applicTarget edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToApplicTarget(ViewerFilter filter);

	/**
	 * Adds the given filter to the applicTarget edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToApplicTarget(ViewerFilter filter);




	/**
	 * Init the ownedRel
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedRel(ReferencesTableSettings settings);

	/**
	 * Update the ownedRel
	 * @param newValue the ownedRel to update
	 * 
	 */
	public void updateOwnedRel();

	/**
	 * Adds the given filter to the ownedRel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedRel(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedRel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedRel(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedRel table
	 * 
	 */
	public boolean isContainedInOwnedRelTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
