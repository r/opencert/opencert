/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.executors;

import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.opencert.storage.cdo.session.ICDOSessionProvider;
import org.eclipse.opencert.storage.cdo.session.ThreadLocalCDOSessionProvider;


/**
 * Note that this class is not thread-safe on purpose and is meant to be executed in a thread-safe manner.
 * Just create and instance every time you need to interact with storage   
 * 
 */
public abstract class TemplatedHttpRequestDrivenInTransactionExecutor<T>
{
    private final ICDOSessionProvider cdoSessionProvider;
    
    public abstract T executeInTransaction(CDOTransaction cdoTransaction);
    
    public TemplatedHttpRequestDrivenInTransactionExecutor() {
        this.cdoSessionProvider = new ThreadLocalCDOSessionProvider();
    }
    
    
    public T executeReadOnlyOperation()
            throws RuntimeException
    {
        CDOSession activeSession = cdoSessionProvider.getActiveSession();
        CDOTransaction cdoTransaction = activeSession.openTransaction();
            
        T returnedValue = executeInTransaction(cdoTransaction);
        
        //this is a workaround preventing some odd CDO behavior, when entities were magically detached
        //and as a result removed from the database (!!!)
        cdoTransaction.rollback();
        
        return returnedValue;
    }
    
    
    public T executeReadWriteOperation()
        throws RuntimeException
    {
        CDOSession activeSession = cdoSessionProvider.getActiveSession();
        CDOTransaction cdoTransaction = activeSession.openTransaction();
            
        T returnedValue = executeInTransaction(cdoTransaction);
        
        try {
            cdoTransaction.commit();
        } catch (CommitException e) {
            throw new RuntimeException(e);
        }
        
        return returnedValue;
    }
}
