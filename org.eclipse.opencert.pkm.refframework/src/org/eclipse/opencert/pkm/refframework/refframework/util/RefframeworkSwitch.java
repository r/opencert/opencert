/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap;
import org.eclipse.opencert.infra.mappings.mapping.Map;
import org.eclipse.opencert.pkm.refframework.refframework.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage
 * @generated
 */
public class RefframeworkSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RefframeworkPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefframeworkSwitch() {
		if (modelPackage == null) {
			modelPackage = RefframeworkPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case RefframeworkPackage.REF_FRAMEWORK: {
				RefFramework refFramework = (RefFramework)theEObject;
				T result = caseRefFramework(refFramework);
				if (result == null) result = caseDescribableElement(refFramework);
				if (result == null) result = caseNamedElement(refFramework);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_REQUIREMENT: {
				RefRequirement refRequirement = (RefRequirement)theEObject;
				T result = caseRefRequirement(refRequirement);
				if (result == null) result = caseDescribableElement(refRequirement);
				if (result == null) result = caseRefAssurableElement(refRequirement);
				if (result == null) result = caseNamedElement(refRequirement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_ARTEFACT: {
				RefArtefact refArtefact = (RefArtefact)theEObject;
				T result = caseRefArtefact(refArtefact);
				if (result == null) result = caseDescribableElement(refArtefact);
				if (result == null) result = caseRefAssurableElement(refArtefact);
				if (result == null) result = caseNamedElement(refArtefact);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_ACTIVITY: {
				RefActivity refActivity = (RefActivity)theEObject;
				T result = caseRefActivity(refActivity);
				if (result == null) result = caseDescribableElement(refActivity);
				if (result == null) result = caseRefAssurableElement(refActivity);
				if (result == null) result = caseNamedElement(refActivity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_REQUIREMENT_REL: {
				RefRequirementRel refRequirementRel = (RefRequirementRel)theEObject;
				T result = caseRefRequirementRel(refRequirementRel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_ROLE: {
				RefRole refRole = (RefRole)theEObject;
				T result = caseRefRole(refRole);
				if (result == null) result = caseDescribableElement(refRole);
				if (result == null) result = caseRefAssurableElement(refRole);
				if (result == null) result = caseNamedElement(refRole);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_APPLICABILITY_LEVEL: {
				RefApplicabilityLevel refApplicabilityLevel = (RefApplicabilityLevel)theEObject;
				T result = caseRefApplicabilityLevel(refApplicabilityLevel);
				if (result == null) result = caseDescribableElement(refApplicabilityLevel);
				if (result == null) result = caseNamedElement(refApplicabilityLevel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_CRITICALITY_LEVEL: {
				RefCriticalityLevel refCriticalityLevel = (RefCriticalityLevel)theEObject;
				T result = caseRefCriticalityLevel(refCriticalityLevel);
				if (result == null) result = caseDescribableElement(refCriticalityLevel);
				if (result == null) result = caseNamedElement(refCriticalityLevel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_TECHNIQUE: {
				RefTechnique refTechnique = (RefTechnique)theEObject;
				T result = caseRefTechnique(refTechnique);
				if (result == null) result = caseDescribableElement(refTechnique);
				if (result == null) result = caseRefAssurableElement(refTechnique);
				if (result == null) result = caseNamedElement(refTechnique);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_ARTEFACT_REL: {
				RefArtefactRel refArtefactRel = (RefArtefactRel)theEObject;
				T result = caseRefArtefactRel(refArtefactRel);
				if (result == null) result = caseDescribableElement(refArtefactRel);
				if (result == null) result = caseNamedElement(refArtefactRel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY: {
				RefCriticalityApplicability refCriticalityApplicability = (RefCriticalityApplicability)theEObject;
				T result = caseRefCriticalityApplicability(refCriticalityApplicability);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_ACTIVITY_REL: {
				RefActivityRel refActivityRel = (RefActivityRel)theEObject;
				T result = caseRefActivityRel(refActivityRel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_ASSURABLE_ELEMENT: {
				RefAssurableElement refAssurableElement = (RefAssurableElement)theEObject;
				T result = caseRefAssurableElement(refAssurableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_INDEPENDENCY_LEVEL: {
				RefIndependencyLevel refIndependencyLevel = (RefIndependencyLevel)theEObject;
				T result = caseRefIndependencyLevel(refIndependencyLevel);
				if (result == null) result = caseRefApplicabilityLevel(refIndependencyLevel);
				if (result == null) result = caseDescribableElement(refIndependencyLevel);
				if (result == null) result = caseNamedElement(refIndependencyLevel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_RECOMMENDATION_LEVEL: {
				RefRecommendationLevel refRecommendationLevel = (RefRecommendationLevel)theEObject;
				T result = caseRefRecommendationLevel(refRecommendationLevel);
				if (result == null) result = caseRefApplicabilityLevel(refRecommendationLevel);
				if (result == null) result = caseDescribableElement(refRecommendationLevel);
				if (result == null) result = caseNamedElement(refRecommendationLevel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_CONTROL_CATEGORY: {
				RefControlCategory refControlCategory = (RefControlCategory)theEObject;
				T result = caseRefControlCategory(refControlCategory);
				if (result == null) result = caseRefApplicabilityLevel(refControlCategory);
				if (result == null) result = caseDescribableElement(refControlCategory);
				if (result == null) result = caseNamedElement(refControlCategory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_APPLICABILITY: {
				RefApplicability refApplicability = (RefApplicability)theEObject;
				T result = caseRefApplicability(refApplicability);
				if (result == null) result = caseNamedElement(refApplicability);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_APPLICABILITY_REL: {
				RefApplicabilityRel refApplicabilityRel = (RefApplicabilityRel)theEObject;
				T result = caseRefApplicabilityRel(refApplicabilityRel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RefframeworkPackage.REF_EQUIVALENCE_MAP: {
				RefEquivalenceMap refEquivalenceMap = (RefEquivalenceMap)theEObject;
				T result = caseRefEquivalenceMap(refEquivalenceMap);
				if (result == null) result = caseEquivalenceMap(refEquivalenceMap);
				if (result == null) result = caseMap(refEquivalenceMap);
				if (result == null) result = caseNamedElement(refEquivalenceMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Framework</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Framework</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefFramework(RefFramework object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Requirement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefRequirement(RefRequirement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Artefact</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefArtefact(RefArtefact object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Activity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefActivity(RefActivity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Requirement Rel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Requirement Rel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefRequirementRel(RefRequirementRel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefRole(RefRole object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Applicability Level</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Applicability Level</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefApplicabilityLevel(RefApplicabilityLevel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Criticality Level</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Criticality Level</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefCriticalityLevel(RefCriticalityLevel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Technique</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Technique</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefTechnique(RefTechnique object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Artefact Rel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Artefact Rel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefArtefactRel(RefArtefactRel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Criticality Applicability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Criticality Applicability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefCriticalityApplicability(RefCriticalityApplicability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Activity Rel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Activity Rel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefActivityRel(RefActivityRel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Assurable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Assurable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefAssurableElement(RefAssurableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Independency Level</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Independency Level</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefIndependencyLevel(RefIndependencyLevel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Recommendation Level</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Recommendation Level</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefRecommendationLevel(RefRecommendationLevel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Control Category</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Control Category</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefControlCategory(RefControlCategory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Applicability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Applicability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefApplicability(RefApplicability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Applicability Rel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Applicability Rel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefApplicabilityRel(RefApplicabilityRel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Equivalence Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Equivalence Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefEquivalenceMap(RefEquivalenceMap object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Describable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Describable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDescribableElement(DescribableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMap(Map object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equivalence Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equivalence Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquivalenceMap(EquivalenceMap object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //RefframeworkSwitch
