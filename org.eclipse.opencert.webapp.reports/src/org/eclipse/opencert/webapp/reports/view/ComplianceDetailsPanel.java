/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetails;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetailsItem;
import org.eclipse.opencert.webapp.reports.containers.visitors.ComplianceDetailsTraverser;
import org.eclipse.opencert.webapp.reports.listeners.ProjectBaselineComplianceTableListener;
import org.eclipse.opencert.webapp.reports.listeners.util.ClickType;
import org.eclipse.opencert.webapp.reports.listeners.util.SelectionEvent;
import org.eclipse.opencert.webapp.reports.manager.BaseActivityManager;
import org.eclipse.opencert.webapp.reports.manager.BaseArtefactManager;
import org.eclipse.opencert.webapp.reports.manager.BaseRequirementManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceDetailsManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceDetailsPanelVisitor;
import org.eclipse.opencert.webapp.reports.util.BaselineElementType;
import org.eclipse.opencert.webapp.reports.util.ComplianceType;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.util.StringUtil;
import org.eclipse.opencert.webapp.reports.view.common.AbstractComplianceDetailsPanel;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;

import com.vaadin.annotations.Theme;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.AbstractSelect.ItemDescriptionGenerator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;

@Theme("opencerttheme")
public class ComplianceDetailsPanel
        extends AbstractComplianceDetailsPanel
        implements ProjectBaselineComplianceTableListener
{

    private static final long serialVersionUID = 9024369941872471361L;

    private static final String COMPLINACE_LABEL_STYLE = "compliance";
    private static final String COMPLINACE_BUTTON_STYLE = "complianceSettings";
    private static final String COMPLIANCE_DETAILS_TREE_STYLE = "complianceDetails";
    private static final String COMPLINACE_DETAILS_TREE_NODE_JUST = "justification";
    private static final String COMPLINACE_DETAILS_TREE_NODE_ASSET = "asset";
    private static final String COMPLINACE_DETAILS_TREE_NODE_EVAL = "evaluation";
    private static final String COMPLINACE_DETAILS_TREE_NODE_EVAL_CHILD = "evaluationChild";

    private static final String COMPLIANCE_DETAILS = "Details:";
    private static final String COMPLIANT_ARTEFACTS_DESC = "Artefacts";
    private static final String COMPLIANT_ACTIVITIES_DESC = "Activities";
    private static final String BASE_ARTEFACT = "baseArtefact";
    private static final String BASE_ACTIVITY = "baseActivity";
    private static final String BASE_REQUIREMENT = "baseRequirement";
    private static final String EXPAND_JUSTIFICATION = "Expand to Justification";
    private static final String EXPAND_ASSET = "Expand to Asset";
    private static final String EXPAND_EVALUATION = "Expand to Evaluation";
    private static final String TREE_LEVEL = "level";
    private static final String TREE_LEVEL_JUST = "levelJust";
    private static final String TREE_LEVEL_ASSET = "levelAsset";
    private static final String TREE_LEVEL_EVAL = "levelEval";
    private static final String TREE_LEVEL_EVAL_CHILD = "levelEvalChild";
    private static final String DESCRIPTION = "description";
    private static final String ICON = "icon";
    private static final String NAME = "name";

    private VerticalLayout complianceSettingsLayout;
    private VerticalLayout mainComplianceLayout;
    private VerticalLayout treeLayout;
    private Tree complianceDetailsTree;
    private Label baselineAssetLabel;
    private Label baselineFrameworkLabel;
    private Label fullyCompliantAssetLabel;
    private Label partiallyCompliantAssetLabel;
    private Label selectBaseAssetLabel;
    
    private HorizontalLayout expandLinksLayout;

    public ComplianceDetailsPanel()
    {
        init();
    }

    @Override
    public void baselineElementClicked(SelectionEvent selectionEvent)
    {
    	
        if (selectionEvent.getClickType().equals(ClickType.UNSELECTED)) {
        	if (selectionEvent.getNumberOfBaseAssets() != null && selectionEvent.getNumberOfBaseAssets() == 0) {
        		selectBaseAssetLabel.setVisible(false);
        	} else {
        		selectBaseAssetLabel.setVisible(true);
        	}
            mainComplianceLayout.setVisible(false);
            treeLayout.setVisible(false);
            return;
        }

        if (selectionEvent.getNumberOfAssets() != null) {
            selectBaseAssetLabel.setVisible(false);
            expandLinksLayout.setVisible(true);
            if (ComplianceType.COMPLIANCE_FULLY.equals(selectionEvent.getComplianceType())) {
                setComplianceDetailsData(selectionEvent.getCdoId(), selectionEvent.getBaselineElementType(), selectionEvent.getComplianceType(),
                        selectionEvent.getNumberOfAssets(), 0);
            } else {
                setComplianceDetailsData(selectionEvent.getCdoId(), selectionEvent.getBaselineElementType(), selectionEvent.getComplianceType(), 0,
                        selectionEvent.getNumberOfAssets());
            }
            return;
        }

        selectBaseAssetLabel.setVisible(false);
        expandLinksLayout.setVisible(true);
        setComplianceDetailsData(selectionEvent.getCdoId(), selectionEvent.getBaselineElementType(), selectionEvent.getComplianceType(),
                selectionEvent.getNumberOfFullyAssets(), selectionEvent.getNumberOfPartialAssets());

    }

    private void setComplianceDetailsData(long cdoId, String baselineElementType, String complianceType, int numberOfFullyAssets, int numberOfPartiallyAssets)
    {
        complianceSettingsLayout.setVisible(true);
        mainComplianceLayout.setVisible(true);

        ComplianceDetailsManager complianceDetailsManager = new ComplianceDetailsManager();
        if (BaselineElementType.BASE_ARTEFACT.equals(baselineElementType)) {
            BaseArtefactManager baseArtefactManager = (BaseArtefactManager) SpringContextHelper.getBeanFromWebappContext(BaseArtefactManager.SPRING_NAME);
            BaseArtefact baseArtefact = baseArtefactManager.getBaseArtefact(cdoId);

            if (baseArtefact != null) {
                // Main info:
                createMainComplianceInfoData(baseArtefact, complianceType, numberOfFullyAssets, numberOfPartiallyAssets);
                // Compliance Details:
                List<ComplianceDetails> complianceDetailsData = complianceDetailsManager.readComplianceDetailsData(baseArtefact, complianceType, cdoId,
                        baseArtefact.getName(), false);
                createComplianceDetailsTree(complianceDetailsData);
            }
        }

        if (BaselineElementType.BASE_ACTIVITY.equals(baselineElementType)) {
            BaseActivityManager baseActivityManager = (BaseActivityManager) SpringContextHelper.getBeanFromWebappContext(BaseActivityManager.SPRING_NAME);
            BaseActivity baseActivity = baseActivityManager.getBaseActivity(cdoId);

            if (baseActivity != null) {
                // Main info:
                createMainComplianceInfoData(baseActivity, complianceType, numberOfFullyAssets, numberOfPartiallyAssets);
                // Compliance Details:
                List<ComplianceDetails> complianceDetailsData = complianceDetailsManager.readComplianceDetailsData(baseActivity, complianceType, cdoId,
                        baseActivity.getName(), false);
                createComplianceDetailsTree(complianceDetailsData);
            }
        }
        
        if (BaselineElementType.BASE_REQUIREMENT.equals(baselineElementType)) {
            BaseRequirementManager baseRequirementManager = (BaseRequirementManager) SpringContextHelper.getBeanFromWebappContext(BaseRequirementManager.SPRING_NAME);
            BaseRequirement baseRequirement = baseRequirementManager.getBaseRequirement(cdoId);

            if (baseRequirement != null) {
                // Main info:
                createMainComplianceInfoData(baseRequirement, complianceType, numberOfFullyAssets, numberOfPartiallyAssets);
                // Compliance Details:
                List<ComplianceDetails> complianceDetailsData = complianceDetailsManager.readComplianceDetailsData(baseRequirement, complianceType, cdoId,
                        baseRequirement.getName(), false);
                createComplianceDetailsTree(complianceDetailsData);
            }
        }

        if (complianceDetailsTree.getItemIds().size() > 0) {
            treeLayout.setVisible(true);
        } else {
            treeLayout.setVisible(false);
        }

    }

    public void init()
    {
        VerticalLayout mainPanel = new VerticalLayout();
        mainPanel.setWidth("100%");
        mainPanel.setStyleName("layoutSpacing");
        mainPanel.setSpacing(true);

        // Compliance settings:
        complianceSettingsLayout = complianceSettingsLayout();
        mainPanel.addComponent(complianceSettingsLayout);

        createSelectBaseAssetLabel();
        mainPanel.addComponent(selectBaseAssetLabel);

        // Compliance main info:
        mainComplianceLayout = createMainComplianceInfoLayout();
        mainPanel.addComponent(mainComplianceLayout);
        mainComplianceLayout.setVisible(false);

        // Compliance data:
        treeLayout = new VerticalLayout();
        treeLayout.setMargin(true);
        Label detailsLabel = new Label(COMPLIANCE_DETAILS);
        detailsLabel.setStyleName("details");
        treeLayout.addComponent(detailsLabel);
        complianceDetailsTree = new Tree();
        complianceDetailsTree.setStyleName(COMPLIANCE_DETAILS_TREE_STYLE);
        treeLayout.addComponent(complianceDetailsTree);
        if (complianceDetailsTree.getItemIds().size() > 0) {
            treeLayout.setVisible(true);
            selectBaseAssetLabel.setVisible(true);
        } else {
            treeLayout.setVisible(false);
            selectBaseAssetLabel.setVisible(false);
        }
        
        expandLinksLayout.setVisible(false);

        mainPanel.addComponent(treeLayout);

        // add mainPanel component
        setCompositionRoot(mainPanel);
    }

    private void createSelectBaseAssetLabel()
    {
        selectBaseAssetLabel = new Label("Please select specific <b>Base Asset Name</b> in <b>Project Baseline "
                + "Compliance</b> panel to see <b>Compliance Details</b>.", ContentMode.HTML);
        selectBaseAssetLabel.setStyleName("baseAssetHelp");
    }

    private VerticalLayout createMainComplianceInfoLayout()
    {
        VerticalLayout mainComplianceInfoLayout = new VerticalLayout();
        mainComplianceInfoLayout.setWidth("100px");
        baselineAssetLabel = new Label("", ContentMode.HTML);
        baselineFrameworkLabel = new Label("", ContentMode.HTML);
        fullyCompliantAssetLabel = new Label("", ContentMode.HTML);
        partiallyCompliantAssetLabel = new Label("", ContentMode.HTML);

        baselineAssetLabel.setStyleName(COMPLINACE_LABEL_STYLE);
        baselineFrameworkLabel.setStyleName(COMPLINACE_LABEL_STYLE);
        fullyCompliantAssetLabel.setStyleName(COMPLINACE_LABEL_STYLE);
        partiallyCompliantAssetLabel.setStyleName(COMPLINACE_LABEL_STYLE);

        mainComplianceInfoLayout.addComponent(baselineAssetLabel);
        mainComplianceInfoLayout.addComponent(baselineFrameworkLabel);
        mainComplianceInfoLayout.addComponent(fullyCompliantAssetLabel);
        mainComplianceInfoLayout.addComponent(partiallyCompliantAssetLabel);

        mainComplianceInfoLayout.setStyleName("complianceDetailsLayout");

        return mainComplianceInfoLayout;
    }

    private VerticalLayout complianceSettingsLayout()
    {
        VerticalLayout compliaceLayout = new VerticalLayout();
        GridLayout complianceSettingsLayout = new GridLayout(2, 1);
        complianceSettingsLayout.setWidth("500px");

        VerticalLayout complianceDetailsLayout = new VerticalLayout();
        complianceDetailsLayout.setWidth("190px");
        complianceDetailsLayout.setHeight("6px");
        Label complianceLabel = new Label("Compliance Details");
        complianceLabel.addStyleName("complianceDetailsTitle");
        complianceDetailsLayout.addComponent(complianceLabel);
        // complianceLabel.setWidth("250px");

        expandLinksLayout = new HorizontalLayout();
        Button expandToJustificationLink = new Button("[Expand to Justification]");
        expandToJustificationLink.setStyleName(COMPLINACE_BUTTON_STYLE);
        Button expandToAssetLink = new Button("[Expand to Asset]");
        expandToAssetLink.setStyleName(COMPLINACE_BUTTON_STYLE);
        Button expandToEvaluationLink = new Button("[Expand to Evaluation]");
        expandToEvaluationLink.setStyleName(COMPLINACE_BUTTON_STYLE);

        expandToJustificationLink.addClickListener((e) -> expandComplianceDetails(EXPAND_JUSTIFICATION));
        expandToAssetLink.addClickListener((e) -> expandComplianceDetails(EXPAND_ASSET));
        expandToEvaluationLink.addClickListener((e) -> expandComplianceDetails(EXPAND_EVALUATION));
        
        expandLinksLayout.addComponent(expandToJustificationLink);
        expandLinksLayout.addComponent(expandToAssetLink);
        expandLinksLayout.addComponent(expandToEvaluationLink);

        complianceSettingsLayout.addComponent(complianceDetailsLayout);
        complianceSettingsLayout.addComponent(expandLinksLayout);
        complianceSettingsLayout.setComponentAlignment(complianceDetailsLayout, Alignment.TOP_LEFT);
        complianceSettingsLayout.setComponentAlignment(expandLinksLayout, Alignment.TOP_LEFT);

        compliaceLayout.addComponent(complianceSettingsLayout);
        compliaceLayout.setWidth("100%");
        return compliaceLayout;
    }

    protected void expandComplianceDetails(String expand)
    {
        for (Object itemId : complianceDetailsTree.getItemIds()) {
            String treeLevel = (String) complianceDetailsTree.getItem(itemId).getItemProperty(TREE_LEVEL).getValue();
            if (TREE_LEVEL_JUST.equals(treeLevel)) {
                complianceDetailsTree.collapseItemsRecursively(itemId);
            }
        }

        if (!EXPAND_JUSTIFICATION.equals(expand)) {
            for (Object itemId : complianceDetailsTree.getItemIds()) {
                String treeLevel = (String) complianceDetailsTree.getItem(itemId).getItemProperty(TREE_LEVEL).getValue();

                if (TREE_LEVEL_JUST.equals(treeLevel)) {
                    if (EXPAND_EVALUATION.equals(expand)) {
                        complianceDetailsTree.expandItemsRecursively(itemId);
                    } else if (EXPAND_ASSET.equals(expand)) {
                        complianceDetailsTree.expandItem(itemId);
                    }
                }
            }
        }
    }

    private void createMainComplianceInfoData(BaseArtefact baseArtefact, String complianceType, int numberOfFully, int numberOfPartially)
    {
        String baseArtefactName = StringUtil.ensureNotNull(baseArtefact.getName());
        String baseFrameworkName = "";
        BaseFramework baseFramework = null;
        if (baseArtefact.eContainer() instanceof BaseFramework) {
            baseFramework = (BaseFramework) baseArtefact.eContainer();
            baseFrameworkName = baseFramework.getName();
        }

        createMainComplianceInfoData(baseArtefactName, baseFrameworkName, numberOfFully, numberOfPartially, BASE_ARTEFACT, complianceType);
    }

    private void createMainComplianceInfoData(BaseActivity baseActivity, String complianceType, int numberOfFully, int numberOfPartial)
    {
        String baseActivityName = baseActivity.getName();
        String baseFrameworkName = getBaseFrameworkName(baseActivity);

        createMainComplianceInfoData(baseActivityName, baseFrameworkName, numberOfFully, numberOfPartial, BASE_ACTIVITY, complianceType);
    }
    
    private String getBaseFrameworkName(EObject baseAssurableElement) {
    	EObject object = baseAssurableElement.eContainer();
    	
    	if (object instanceof BaseFramework) {
    		BaseFramework baseFramework = (BaseFramework) object;
    		return baseFramework.getName();
    	} else if (object instanceof BaseActivity) {
    		return getBaseFrameworkName(object);
    	} else if (object instanceof BaseRequirement) {
    		return getBaseFrameworkName(object);
    	}
    	
    	return "";
    }
    
    private void createMainComplianceInfoData(BaseRequirement baseRequirement, String complianceType, int numberOfFully, int numberOfPartial)
    {
        String baseRequirementName = baseRequirement.getName();
        String baseFrameworkName = getBaseFrameworkName(baseRequirement);
        
        createMainComplianceInfoData(baseRequirementName, baseFrameworkName, numberOfFully, numberOfPartial, BASE_REQUIREMENT, complianceType);
    }

    private void createMainComplianceInfoData(String baseAssetName, String baseFrameworkName, int numberOfFully, int numberOfPartially, String baseAssetType,
            String complianceType)
    {
        String baseAssetTypeDesc = "";
        if (BASE_ARTEFACT.equals(baseAssetType)) {
            baseAssetTypeDesc = COMPLIANT_ARTEFACTS_DESC;
        } else if (BASE_ACTIVITY.equals(baseAssetType)) {
            baseAssetTypeDesc = COMPLIANT_ACTIVITIES_DESC;
        }

        if (baseFrameworkName == null) {
            baseFrameworkName = "";
        }

        baselineAssetLabel.setValue("Selected baseline Asset: <b>" + baseAssetName + "</b>");
        baselineFrameworkLabel.setValue("Baseline Framework: <b>" + baseFrameworkName + "</b>");

        if (ComplianceType.COMPLIANCE_FULLY.equals(complianceType) || ComplianceType.COMPLIANCE_ALL.equals(complianceType)) {
            fullyCompliantAssetLabel.setValue("Number of Fully Compliant " + baseAssetTypeDesc + ": <b>" + numberOfFully + "</b>");
        } else {
            fullyCompliantAssetLabel.setValue("");
        }
        if (ComplianceType.COMPLIANCE_PARTIALLY.equals(complianceType) || ComplianceType.COMPLIANCE_ALL.equals(complianceType)) {
            partiallyCompliantAssetLabel.setValue("Number of Partially Compliant " + baseAssetTypeDesc + ": <b>" + numberOfPartially + "</b>");
        } else {
            partiallyCompliantAssetLabel.setValue("");
        }
    }

    @SuppressWarnings("unchecked")
    private void createComplianceDetailsTree(List<ComplianceDetails> complianceDetailsAll)
    {

        HierarchicalContainer complianceDetailsContainer = createComplianceDetailsContainer();

        final ComplianceDetailsPanelVisitor complianceDetailsPanelVisitor = new ComplianceDetailsPanelVisitor(complianceDetailsContainer, new String[] { NAME,
                DESCRIPTION, TREE_LEVEL });
        ComplianceDetailsTraverser<ComplianceDetailsItem> cdTraverser = new ComplianceDetailsTraverser<ComplianceDetailsItem>(complianceDetailsPanelVisitor);
        cdTraverser.traverseComplianceDetails(complianceDetailsAll);

        for (Object itemId : complianceDetailsContainer.getItemIds()) {
            if (complianceDetailsContainer.hasChildren(itemId)) {
                complianceDetailsContainer.setChildrenAllowed(itemId, true);
            } else {
                complianceDetailsContainer.setChildrenAllowed(itemId, false);
            }
        }

        complianceDetailsTree.setContainerDataSource(complianceDetailsContainer);
        complianceDetailsTree.setItemCaptionMode(ItemCaptionMode.PROPERTY);
        complianceDetailsTree.setItemCaptionPropertyId(NAME);
        complianceDetailsTree.setItemIconPropertyId(ICON);
        expandComplianceDetails(EXPAND_ASSET);

        complianceDetailsTree.setItemStyleGenerator(new Tree.ItemStyleGenerator() {

            private static final long serialVersionUID = 3995655196430517388L;

            @Override
            public String getStyle(Tree source, Object itemId)
            {
                String treeLevel = (String) source.getItem(itemId).getItemProperty(TREE_LEVEL).getValue();
                if (TREE_LEVEL_JUST.equals(treeLevel)) {
                    return COMPLINACE_DETAILS_TREE_NODE_JUST;
                } else if (TREE_LEVEL_ASSET.equals(treeLevel)) {
                    return COMPLINACE_DETAILS_TREE_NODE_ASSET;
                } else if (TREE_LEVEL_EVAL.equals(treeLevel)) {
                    return COMPLINACE_DETAILS_TREE_NODE_EVAL;
                } else if (TREE_LEVEL_EVAL_CHILD.equals(treeLevel)) {
                    return COMPLINACE_DETAILS_TREE_NODE_EVAL_CHILD;
                } else {
                    return "";
                }
            }

        });

        complianceDetailsTree.setItemDescriptionGenerator(new ItemDescriptionGenerator() {

            private static final long serialVersionUID = -113400442275025542L;

            @Override
            public String generateDescription(Component source, Object itemId, Object propertyId)
            {

                if (complianceDetailsTree.getContainerProperty(itemId, TREE_LEVEL).getValue().equals(TREE_LEVEL_ASSET)) {
                    if (complianceDetailsTree.getContainerProperty(itemId, DESCRIPTION).getValue() != null) {
                        return (String) complianceDetailsTree.getContainerProperty(itemId, DESCRIPTION).getValue();
                    }
                }
                return null;
            }

        });

        for (Object itemId : complianceDetailsContainer.getItemIds()) {
            if (TREE_LEVEL_JUST.equals(complianceDetailsTree.getContainerProperty(itemId, TREE_LEVEL).getValue())) {
                complianceDetailsTree.getContainerProperty(itemId, ICON).setValue(new ThemeResource("images/Justification.png"));
            } else if (TREE_LEVEL_EVAL.equals(complianceDetailsTree.getContainerProperty(itemId, TREE_LEVEL).getValue())) {
                complianceDetailsTree.getContainerProperty(itemId, ICON).setValue(new ThemeResource("images/Evaluation.png"));
            } else if (TREE_LEVEL_ASSET.equals(complianceDetailsTree.getContainerProperty(itemId, TREE_LEVEL).getValue())) {
                String assetDesc = (String) complianceDetailsTree.getContainerProperty(itemId, DESCRIPTION).getValue();
                if (assetDesc != null) {
                    if (assetDesc.startsWith("<b>Artefact")) {
                        complianceDetailsTree.getContainerProperty(itemId, ICON).setValue(new ThemeResource("images/Artefact.png"));
                    } else if (assetDesc.startsWith("<b>Activity")) {
                        complianceDetailsTree.getContainerProperty(itemId, ICON).setValue(new ThemeResource("images/Activity.png"));
                    }
                }
            }
        }

    }

    private HierarchicalContainer createComplianceDetailsContainer()
    {
        HierarchicalContainer hc = new HierarchicalContainer();
        hc.addContainerProperty(NAME, String.class, "");
        hc.addContainerProperty(DESCRIPTION, String.class, "");
        hc.addContainerProperty(TREE_LEVEL, String.class, "");
        hc.addContainerProperty(ICON, Resource.class, null);
        return hc;
    }
}