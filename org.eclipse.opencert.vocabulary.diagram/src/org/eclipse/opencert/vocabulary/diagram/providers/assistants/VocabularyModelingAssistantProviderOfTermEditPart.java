/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermEditPart;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyElementTypes;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyModelingAssistantProvider;

/**
 * @generated
 */
public class VocabularyModelingAssistantProviderOfTermEditPart extends
		VocabularyModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSource((TermEditPart) sourceEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSource(TermEditPart source) {
		List<IElementType> types = new ArrayList<IElementType>(4);
		types.add(VocabularyElementTypes.TermDefinedBy_4003);
		types.add(VocabularyElementTypes.TermIsA_4004);
		types.add(VocabularyElementTypes.TermHasA_4005);
		types.add(VocabularyElementTypes.TermRefersTo_4006);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSourceAndTarget((TermEditPart) sourceEditPart,
				targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSourceAndTarget(
			TermEditPart source, IGraphicalEditPart targetEditPart) {
		List<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof SourceOfDefinitionEditPart) {
			types.add(VocabularyElementTypes.TermDefinedBy_4003);
		}
		if (targetEditPart instanceof TermEditPart) {
			types.add(VocabularyElementTypes.TermIsA_4004);
		}
		if (targetEditPart instanceof TermEditPart) {
			types.add(VocabularyElementTypes.TermHasA_4005);
		}
		if (targetEditPart instanceof TermEditPart) {
			types.add(VocabularyElementTypes.TermRefersTo_4006);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForTarget((TermEditPart) sourceEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForTarget(TermEditPart source,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == VocabularyElementTypes.TermDefinedBy_4003) {
			types.add(VocabularyElementTypes.SourceOfDefinition_2003);
		} else if (relationshipType == VocabularyElementTypes.TermIsA_4004) {
			types.add(VocabularyElementTypes.Term_2001);
		} else if (relationshipType == VocabularyElementTypes.TermHasA_4005) {
			types.add(VocabularyElementTypes.Term_2001);
		} else if (relationshipType == VocabularyElementTypes.TermRefersTo_4006) {
			types.add(VocabularyElementTypes.Term_2001);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((TermEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(TermEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(4);
		types.add(VocabularyElementTypes.CategoryTerms_4001);
		types.add(VocabularyElementTypes.TermIsA_4004);
		types.add(VocabularyElementTypes.TermHasA_4005);
		types.add(VocabularyElementTypes.TermRefersTo_4006);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((TermEditPart) targetEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(TermEditPart target,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == VocabularyElementTypes.CategoryTerms_4001) {
			types.add(VocabularyElementTypes.Category_2002);
		} else if (relationshipType == VocabularyElementTypes.TermIsA_4004) {
			types.add(VocabularyElementTypes.Term_2001);
		} else if (relationshipType == VocabularyElementTypes.TermHasA_4005) {
			types.add(VocabularyElementTypes.Term_2001);
		} else if (relationshipType == VocabularyElementTypes.TermRefersTo_4006) {
			types.add(VocabularyElementTypes.Term_2001);
		}
		return types;
	}

}
