/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.pkm.refframework.refframework.utils;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.common.util.CDOException;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.emf.eef.runtime.ui.notify.OpenWizardOnDoubleClick;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.infra.mappings.mapping.MapJustification;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.infra.mappings.mapping.MapModel;
import org.eclipse.opencert.infra.mappings.mapping.MappingFactory;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkFactory;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.provider.RefframeworkEditPlugin;
import org.eclipse.opencert.pkm.refframework.refframework.util.ModelStorage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;


public class MappingSet extends Dialog {
	
	
	
	private static final String UTF_8 = "UTF-8";
	
	private static final String ClassDiagram = "org.eclipse.opencert.pkm.refframework.refframework.diagram.part.DawnRefframeworkDiagramEditor";
	
	protected static final String REFFRAMEWORK = ".refframework";
	protected static final String MAPPING = ".mapping";
	
	protected String qref;
	
	protected CDOView mCDOView = null;
	protected CDOTransaction mCDOTransaction = null;
	protected CDOSession sessionCDO = null;
	
	protected CDOResource resourceFrom = null;
	
	protected org.eclipse.swt.widgets.List refList;
	protected ArrayList<String> refListDirRef;
	protected ArrayList<String> refListDirMap;
	
	protected IEditorPart editor;
	protected URI resourceURI;
	
	protected URI mappingURI;
	protected URI refURI;
	
	protected TreeViewerExt modelViewerSource;
	protected CheckboxTreeViewerExt modelViewerTarget;
	protected CheckboxTreeViewerExt modelViewerSourceAll;
	

	protected Label sourceLabel;
	protected Combo cbTarget;
	protected Combo cbMapGroup;
	protected Combo cbEquivalenceMap;
	protected Combo cbFilterElementMap;
	protected Text txtJustification;
	protected Text idText;
	protected Text nameText;
	protected Text typeText;
	protected Combo cbType;
	
	
	protected boolean bElementTreeSelected = false;
	protected boolean bNewEquivalenceMap = false;
	protected boolean bEqualMapGroup = false;
	
	
	protected Composite sourceComposite = null;
	
	protected EObject object;
	protected EObject objectSourceAll;
	protected EObject objectTarget;
	
	//List Equivalence Map of the selected element tree
	protected EList<RefEquivalenceMap> lstrefEquiMapFrom;
	
	Map<Object, Object> options = new HashMap<Object, Object>();
	
	protected EList<RefEquivalenceMap> lstEquivalenceMap = new BasicEList<RefEquivalenceMap>();
	protected EList<MapGroup> lstMapGroup;
	protected EList<RefActivity> lstrefActivity;
	EList <RefArtefact> lstrefArtefact;
	EList <RefRequirement> lstrefRequirement;
	EList <RefRole> lstrefRole;
	EList <RefTechnique> lstrefTechnique;
	
	EList <RefActivity> lstrefActivitySourceAll;
	EList <RefArtefact> lstrefArtefactSourceAll;
	EList <RefRequirement> lstrefRequirementSourceAll;
	EList <RefRole> lstrefRoleSourceAll;
	EList <RefTechnique> lstrefTechniqueSourceAll;
	
	
	protected MapGroup mapGroup;
	protected MapKind mapKind;
	protected MapJustification mapJustification;
	protected MapModel mapModel; 
	public String MapModelPath; 
	protected RefEquivalenceMap refEquivalenceMap; 
	
	protected String sElementFilter;
	
	private ReferencesTableSettings targetSettings;
	

	public MappingSet(Shell parentShell) {
		super(parentShell);

		
		
		editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		

		
		if (editor.getClass().getName().contentEquals(ClassDiagram)) {
			
			ModelStorage ms=ModelStorage.getInstance();
			resourceURI = ms.getResourceURI();
				
		} else {
			resourceURI = EditUIUtil.getURI(editor.getEditorInput());
			
		}
		
	//	System.out.println("MODELO _usar: " + resourceURI.toString());
		
		
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		mCDOView = CDOConnectionUtil.instance.openView(sessionCDO);
		mCDOTransaction = sessionCDO.openTransaction();
		 
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX );
		
	}

	public MappingSet(IShellProvider parentShell) {
		super(parentShell);
	}


	@Override
	protected Control createDialogArea(final Composite parent) {

		refListDirRef = new ArrayList<String>();
		refListDirMap = new ArrayList<String>();
		
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		final Composite contents = (Composite)super.createDialogArea(parent);
		
		GridLayout contentsGridLayout = (GridLayout)contents.getLayout();
		contentsGridLayout.numColumns = 3;

		GridData contentsGridData = (GridData)contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;
		
		//Default filter
		sElementFilter = "Activity";
		
		createFromDialogArea(contents,adapterFactory);
		createMiddleDialogArea(contents);
		createTargetDialogArea(contents,adapterFactory);

		createMSGDialogArea(parent,adapterFactory);
		
		
		return contents;
	}


	protected void createMSGDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {

		Composite msgComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			msgComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 10;
			layout.numColumns = 1;
			msgComposite.setLayout(layout);
		}

		// Label
		Label msgLabel = new Label(sourceComposite, SWT.NONE);
		msgLabel.setText("Steps:\n\t- Select the mapping model, the map group and the target framework model.\n\t- "
				+ "Finally, select the object from the source framework model and the equivalence map");
		GridData msgLabelGridData = new GridData();
		msgLabelGridData.horizontalAlignment = SWT.FILL;
		msgLabelGridData.verticalAlignment = SWT.FILL;
		msgLabel.setLayoutData(msgLabelGridData);

	}
	
	
	protected void createFromDialogArea(final Composite contents,ComposedAdapterFactory adapterFactory) {
		
		String path = "";
		
		sourceComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			sourceComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 10;
			layout.numColumns = 1;
			sourceComposite.setLayout(layout);
		}

		sourceComposite.setEnabled(false);
		
		// Label
		sourceLabel = new Label(sourceComposite, SWT.NONE);
		sourceLabel.setText("From");
		GridData sourceLabelGridData = new GridData();
		sourceLabelGridData.horizontalAlignment = SWT.FILL;
		sourceLabelGridData.verticalAlignment = SWT.FILL;
		sourceLabel.setLayoutData(sourceLabelGridData);
		
		//TreeViewer from refframework
		modelViewerSource = new TreeViewerExt(sourceComposite,SWT.SINGLE| SWT.H_SCROLL|SWT.V_SCROLL|SWT.BORDER);
		
		final GridData modelViewerGridData = new GridData();
		modelViewerGridData.verticalAlignment = SWT.FILL;
		modelViewerGridData.horizontalAlignment = SWT.FILL;
		modelViewerGridData.widthHint = Display.getCurrent().getBounds().width / 3;
		modelViewerGridData.heightHint = Display.getCurrent().getBounds().height / 3;
		modelViewerGridData.grabExcessHorizontalSpace = false;
		modelViewerGridData.grabExcessVerticalSpace = true;
		
		modelViewerSource.getTree().setLayoutData(modelViewerGridData);
		modelViewerSource.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		modelViewerSource.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		//Load the model of the resource
		resourceFrom = null;
				
		for (int i = 0; i< resourceURI.segmentCount(); i++ ) {
			path = path + resourceURI.segment(i) + "/";
		}
		
		path = path.substring(0,path.length()-1);
		
		try {
			resourceFrom = mCDOTransaction.getResource(path);
			
			
		} catch (CDOException e) {
			
			e.printStackTrace();
		}
		
		
		createModelFromCDOResource(modelViewerSource, resourceFrom);
		modelViewerSource.expandAll();
		
		bElementTreeSelected = false;
		
		modelViewerSource.addSelectionChangedListener(new ISelectionChangedListener() {
			@SuppressWarnings("deprecation")
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				
				cbEquivalenceMap.removeAll();
				txtJustification.setText("");
				idText.setText("");
				nameText.setText("");
				cbType.select(0);
				
				bElementTreeSelected = true;
				bNewEquivalenceMap = false; 
				
				//Unselected all the elements of the target tree
				modelViewerTarget.expandAll();
				modelViewerTarget.setAllChecked(false);
				
				modelViewerSourceAll.setAllChecked(false);
				
				if(event.getSelection().isEmpty()) {
					System.out.println("Empty");
					return;
				}
				
				object = null;
				
				if(event.getSelection() instanceof IStructuredSelection) {
					
					IStructuredSelection sel = (IStructuredSelection) event.getSelection();
					object = (EObject) sel.getFirstElement();
					// Para descartar la selecci�n del framework comprobamos si su container es null
					if (object instanceof RefFramework || object instanceof RefEquivalenceMap) {
						bElementTreeSelected = false;
						return;
					}

					if (sElementFilter.contentEquals("Requirement")) {
						if (object instanceof RefActivity) {
							MessageDialog.openError(getShell(),	"Equivalence Map ","It's neccessary to select a requirement, not an activity. ");
							return;
							
						}
					}
					
					
					if (object instanceof EObject) {
						//Verify selection mapgroup and cbtarget
						
						mapGroup = null; 
						if ((cbMapGroup.getSelectionIndex() == -1) || (cbTarget.getSelectionIndex() == -1)) {
							MessageDialog.openError(getShell(), "Map Group ", "It's neccessary to select a map group and a target reference framework");
							
						} else {
							//mapGroup selected
							
							mapGroup = lstMapGroup.get(cbMapGroup.getSelectionIndex());
							
							//List Equivalence Map of the selected element tree
							lstrefEquiMapFrom = null;
							
							sElementFilter = cbFilterElementMap.getItem(cbFilterElementMap.getSelectionIndex());
							
							//Object is the assurableElement selected from the source Tree
							if (sElementFilter.contentEquals("Activity")) {
								RefActivity refElemtFrom = (RefActivity) object;
								lstrefEquiMapFrom = refElemtFrom.getEquivalence();
								refElemtFrom = null;
							} else if (sElementFilter.contentEquals("Artefact")) {
								RefArtefact refElemtFrom = (RefArtefact) object;
								lstrefEquiMapFrom = refElemtFrom.getEquivalence();
								refElemtFrom = null;
							} else if (sElementFilter.contentEquals("Requirement")) {
								RefRequirement refElemtFrom = (RefRequirement) object;
								lstrefEquiMapFrom = refElemtFrom.getEquivalence();
								refElemtFrom = null;
							} else if (sElementFilter.contentEquals("Role")) {
								RefRole refElemtFrom = (RefRole) object;
								lstrefEquiMapFrom = refElemtFrom.getEquivalence();
								refElemtFrom = null;
							} else {
								RefTechnique refElemtFrom = (RefTechnique) object;
								lstrefEquiMapFrom = refElemtFrom.getEquivalence();
								refElemtFrom = null;
							}
							
							lstEquivalenceMap.clear();
							if (lstrefEquiMapFrom.isEmpty()) {
								bNewEquivalenceMap = true;
								// lo voy a hacer tras chequear un target, ya wue si luego no selecciona target se queda vacio el
								// equivalence Map
								//addEquivalenceMap(mapGroup, object);
							} else {
								Iterator<RefEquivalenceMap> iter1 = lstrefEquiMapFrom.iterator();
								bEqualMapGroup = false;
								boolean isFirst= true;
								while (iter1.hasNext()) {		
									MapGroup mapGroupFrom = null;									
									final RefEquivalenceMap refEquiMapFrom = (RefEquivalenceMap) iter1.next();							
									
									if (refEquiMapFrom.getMapGroup() != null){
										mapGroupFrom = refEquiMapFrom.getMapGroup();
										if (CDOUtil.getCDOObject(mapGroupFrom).cdoID().equals(CDOUtil.getCDOObject(mapGroup).cdoID())){
											bEqualMapGroup = true;
											
											//A�ado el objeto complianceMap a la combo
											//Cunado cree uno nuevo debere a�adirlo a la lista
											lstEquivalenceMap.add(refEquiMapFrom);
											cbEquivalenceMap.add(refEquiMapFrom.getName());
											if(isFirst){
												cbEquivalenceMap.select(0);
												isFirst=false;
												cbEquivalenceMap.notifyListeners(SWT.Selection, new Event());
											}
											mapGroupFrom = null;
										} 
									} else {
										mapGroupFrom = null;
									}
									
									
								} // End of while iter1
								iter1 = null;
								if (!bEqualMapGroup) {
									//addEquivalenceMap(mapGroup, object);
									// lo hago cuando seleccione un targer
								}
							}
						}
					}
				}
			}
		});
	}

	protected void createMiddleDialogArea(final Composite contents) {
		Composite controlMapping = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			controlMapping.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 10;
			layout.numColumns = 1;
			controlMapping.setLayout(layout);
		}

		Group groupFilter = new Group(controlMapping, SWT.NULL);
		groupFilter.setLayout(new GridLayout(1,false));
		groupFilter.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,2,1));
		groupFilter.setText("Filtering");
		
		refList = new org.eclipse.swt.widgets.List(groupFilter, SWT.FILL | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);
		//refList.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false));
		
		
		GridData refListGridData = new GridData();
		refListGridData.verticalAlignment = SWT.FILL;
		refListGridData.horizontalAlignment = SWT.FILL;
		//refListGridData.widthHint = Display.getCurrent().getBounds().width / 7;
		refListGridData.heightHint =200;
		refListGridData.grabExcessHorizontalSpace = true;
		refListGridData.grabExcessVerticalSpace = false;
		refList.setLayoutData(refListGridData);
		
		
		//cbFilterElementMapGridData.heightHint = Display.getCurrent().getBounds().height / 7;
		refListDirMap = new ArrayList<String>();
		
		CDOResourceNode[] listR=  mCDOView.getElements();			
		
		for (int i = 0; i < listR.length; i++) {
			if (listR[i] instanceof CDOResourceFolder) {
				checkFolderContents((CDOResourceFolder) listR[i],MAPPING );
			} else if (listR[i].getName().endsWith(MAPPING)) {
				refList.add(listR[i].getName());
				refListDirMap.add(listR[i].getPath());
			}
		}
		
		qref = "";
		refList.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent e) {
			  		qref = refListDirMap.get(refList.getSelectionIndex()).toString();
			  		CreateModelFromFiletocombo(cbMapGroup, qref);
		      }
		});
		
		Label mapLabel = new Label(groupFilter, SWT.NONE);
		mapLabel.setText("Map Group");
		GridData mapLabelGridData = new GridData();
		mapLabelGridData.horizontalSpan = 2;
		mapLabelGridData.horizontalAlignment = SWT.FILL;
		mapLabelGridData.verticalAlignment = SWT.FILL;
		mapLabel.setLayoutData(mapLabelGridData);

		final Composite controlMappingNew = new Composite(groupFilter, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.END;
			controlMappingNew.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			controlMappingNew.setLayout(layoutNew);
		}
		
		cbMapGroup = new Combo (controlMappingNew, SWT.READ_ONLY);
		GridData cbMapGroupGridData = new GridData();
		cbMapGroupGridData.verticalAlignment = SWT.FILL;
		cbMapGroupGridData.horizontalAlignment = SWT.FILL;
		cbMapGroupGridData.grabExcessHorizontalSpace = true;
		cbMapGroupGridData.grabExcessVerticalSpace = true;
		cbMapGroup.setLayoutData(cbMapGroupGridData);
		
		// if we select a map group, verify selected element from sourcetree and cbtarget
		// show the equivalence from the element selected
		
		cbMapGroup.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (cbTarget.getSelectionIndex() != -1) {
					sourceComposite.setEnabled(true);
				}
				txtJustification.setText("");
				idText.setText("");
				nameText.setText("");
				cbType.select(-1);
				mapGroup = lstMapGroup.get(cbMapGroup.getSelectionIndex());
			}
		});
		
		Button NewGroupMap = new Button(controlMappingNew,SWT.NONE);
		NewGroupMap.setText("New Group");
		GridData NewGroupMapGridData = new GridData();
		NewGroupMapGridData.verticalAlignment = SWT.BEGINNING;
		NewGroupMapGridData.horizontalAlignment = SWT.FILL;
		NewGroupMap.setLayoutData(NewGroupMapGridData);
		
		NewGroupMap.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (qref != "") { 
					GroupMapNew groupMapSet = new GroupMapNew(controlMappingNew.getShell(),sessionCDO, MapModelPath);

					if (groupMapSet.open() == Window.OK) {
						if (groupMapSet.getResult()) {
							MessageDialog.openError(getShell(), "New Map Group", "New Map Group created.");
							
							//Reload Mapping
							cbMapGroup.removeAll();
							CreateModelFromFiletocombo(cbMapGroup, qref);
									
						} else {
							MessageDialog.openError(getShell(), "New Map Group", "Fail to create a new map group. There is another map group with these values.");
						}
					}
				} else {
					MessageDialog.openError(getShell(), "New Map Group", "It's neccesary select a Mapping Model.");
				}
			}
		});
		
		
		Label filtermapLabel = new Label(groupFilter, SWT.NONE);
		filtermapLabel.setText("Filter Map Element");
		GridData filtermapLabelGridData = new GridData();
		filtermapLabelGridData.horizontalSpan = 2;
		filtermapLabelGridData.horizontalAlignment = SWT.FILL;
		filtermapLabelGridData.verticalAlignment = SWT.FILL;
		filtermapLabel.setLayoutData(filtermapLabelGridData);
		
		cbFilterElementMap = new Combo (groupFilter, SWT.READ_ONLY);
		GridData cbFilterElementMapGridData = new GridData();
		cbFilterElementMapGridData.verticalAlignment = SWT.FILL;
		cbFilterElementMapGridData.horizontalAlignment = SWT.FILL;
		cbFilterElementMapGridData.widthHint = Display.getCurrent().getBounds().width / 7;
		cbFilterElementMapGridData.heightHint = Display.getCurrent().getBounds().height / 7;
		cbFilterElementMapGridData.grabExcessHorizontalSpace = true;
		cbFilterElementMapGridData.grabExcessVerticalSpace = true;
		cbFilterElementMap.setLayoutData(cbFilterElementMapGridData);
		
		cbFilterElementMap.add("Activity");
		cbFilterElementMap.add("Artefact");
		cbFilterElementMap.add("Requirement");
		cbFilterElementMap.add("Technique");
		cbFilterElementMap.add("Role");
		cbFilterElementMap.select(0);
		
		
		cbFilterElementMap.addSelectionListener(new SelectionAdapter() {

			@SuppressWarnings("deprecation")
			public void widgetSelected(SelectionEvent e) {

				if (cbFilterElementMap.getSelectionIndex() != -1 ) {
					sElementFilter = cbFilterElementMap.getItem(cbFilterElementMap.getSelectionIndex());
					createModelFromCDOResource(modelViewerSource, resourceFrom);
					modelViewerSource.expandAll();
					createModelFromFilePath(modelViewerTarget, refListDirRef.get(cbTarget.getSelectionIndex()));
					modelViewerTarget.expandAll();
					
					modelViewerSourceAll.setAllChecked(false);
				}
			}
		});
		
		
		Group groupEquMap = new Group(controlMapping, SWT.NULL);
		groupEquMap.setLayout(new GridLayout(1,true));
		groupEquMap.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,1,1));
		groupEquMap.setText("Equivalence Map");

		
		final Composite controlEquivMap = new Composite(groupEquMap, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.END;
			controlEquivMap.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			dataNew.verticalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			controlEquivMap.setLayout(layoutNew);
		}
		
		cbEquivalenceMap = new Combo (controlEquivMap, SWT.READ_ONLY);
		GridData cbComplianceMapData = new GridData();
		cbComplianceMapData.verticalAlignment = SWT.FILL;
		cbComplianceMapData.horizontalAlignment = SWT.FILL;
		cbComplianceMapData.grabExcessHorizontalSpace = true;
		cbComplianceMapData.grabExcessVerticalSpace = true;
		cbEquivalenceMap.setLayoutData(cbComplianceMapData);
		
		// if we select a map group, verify selected element from sourcetree and cbtarget
		// show the equivalence from the element selected
		cbEquivalenceMap.addSelectionListener(new SelectionAdapter() {
			@SuppressWarnings("deprecation")
			public void widgetSelected(SelectionEvent e) {
			
				modelViewerTarget.setAllChecked(false);
				txtJustification.setText("");
				idText.setText("");
				nameText.setText("");
				cbType.select(-1);
				
				if ((cbTarget.getSelectionIndex() == -1) || !bElementTreeSelected) {
					MessageDialog.openError(getShell(), "Equivalence Map","It's neccesary select a target");			
				} else {
					refEquivalenceMap = lstEquivalenceMap.get(cbEquivalenceMap.getSelectionIndex());
					
					//id
					if (refEquivalenceMap.getId() != null)
						idText.setText(refEquivalenceMap.getId());
					if (idText.getListeners(SWT.CHANGED).length > 0)
						idText.removeListener(SWT.CHANGED,idText.getListeners(SWT.CHANGED)[0]);
					idText.addListener(SWT.CHANGED,new Listener() {
						@Override
						public void handleEvent(Event event) {
							Text text = (Text) event.widget;
							refEquivalenceMap.setId(text.getText());
							try {
								if (!bNewEquivalenceMap) {
									refEquivalenceMap.eResource().save(options);
								}
							} catch (IOException e) { 
								
								e.printStackTrace();
							}
						}
					});
					
					//name
					if (refEquivalenceMap.getName() != null)
						nameText.setText(refEquivalenceMap.getName());
					if (nameText.getListeners(SWT.CHANGED).length > 0)
						nameText.removeListener(SWT.CHANGED,nameText.getListeners(SWT.CHANGED)[0]);
					nameText.addListener(SWT.CHANGED,new Listener() {
						@Override
						public void handleEvent(Event event) {
							Text text1 = (Text) event.widget;

							refEquivalenceMap.setName(text1.getText());
							try {
								if (!bNewEquivalenceMap) {
									refEquivalenceMap.eResource().save(options);
								}
							} catch (IOException e) { 
								e.printStackTrace();
							}
						}
					});
					
					//type
					mapKind = refEquivalenceMap.getType();
					if (mapKind != null && mapKind.getName() != null) {
						// Recorrer la combo para seleccionar el valor que coincida
						int i = 0;
						for (i = 0; i < cbType.getItemCount(); i++) {
							if (cbType.getItem(i).contentEquals(mapKind.getName()))
								cbType.select(i);
						}
					}
					if (cbType.getListeners(SWT.Selection).length > 0)
						cbType.removeListener(SWT.Selection,cbType.getListeners(SWT.Selection)[0]);
					cbType.addListener(SWT.Selection,new Listener() {
						@Override
						public void handleEvent(Event event) {
							Combo combo = (Combo) event.widget;
							String value = combo.getItem(combo.getSelectionIndex());
							refEquivalenceMap.setType(MapKind.get(value));
							try {
								if (!bNewEquivalenceMap) {
									refEquivalenceMap.eResource().save(options);
								}
							} catch (IOException e) { 
								e.printStackTrace();
							}
						}
					});
					
					//justification
					mapJustification = refEquivalenceMap.getMapJustification();
					if (mapJustification != null && mapJustification.getExplanation() != null)
						txtJustification.setText(mapJustification.getExplanation());
					if (txtJustification.getListeners(SWT.CHANGED).length > 0)
						txtJustification.removeListener(SWT.CHANGED,txtJustification.getListeners(SWT.CHANGED)[0]);
					txtJustification.addListener(SWT.CHANGED,new Listener() {
						@Override
						public void handleEvent(Event event) {
							Text text = (Text) event.widget;
							if (mapJustification == null){
								mapJustification = MappingFactory.eINSTANCE.createMapJustification();															
							}
							mapJustification.setExplanation(text.getText());
							refEquivalenceMap.setMapJustification(mapJustification);
							try {
								//if (!bnewEquivalenceMap) {
									mapJustification.eResource().save(options);
								//}
							} catch (IOException e) { 
								e.printStackTrace();
							}
						}
					});
					// Start 20141204 IRR
					//Post Conditions
					
					objectTarget = null;
					
					EList <RefAssurableElement> lstassElement =refEquivalenceMap.getTarget();
					targetSettings = new ReferencesTableSettings(refEquivalenceMap, RefframeworkPackage.eINSTANCE.getRefEquivalenceMap_Target());
					
					Iterator<RefAssurableElement> iter2 = lstassElement.iterator();
					while (iter2.hasNext()) {
						
						objectTarget = iter2.next();
						
						if (sElementFilter.contentEquals("Activity")) {
							if (objectTarget instanceof RefActivity)
							{
								RefActivity assElement = (RefActivity) objectTarget;
								Iterator<RefActivity> iter3 = lstrefActivity.iterator();
								while (iter3.hasNext()) {
									RefActivity refElementTo = (RefActivity) iter3.next();
									if (CDOUtil.getCDOObject(assElement).cdoID().equals(CDOUtil.getCDOObject(refElementTo).cdoID())){
										modelViewerTarget.CheckedElement(refElementTo);
									}
								}
								assElement = null;
							}
							
						} else if (sElementFilter.contentEquals("Artefact")) {
							if (objectTarget instanceof RefArtefact)
							{
								RefArtefact assElement = (RefArtefact) objectTarget;
								Iterator<RefArtefact> iter3 = lstrefArtefact.iterator();
								while (iter3.hasNext()) {
									RefArtefact refElementTo = (RefArtefact) iter3.next();
									if (CDOUtil.getCDOObject(assElement).cdoID().equals(CDOUtil.getCDOObject(refElementTo).cdoID())){
										modelViewerTarget.CheckedElement(refElementTo);
									}
								}
								assElement = null;
							}
							
						} else if (sElementFilter.contentEquals("Requirement")) {
							if (objectTarget instanceof RefRequirement)
							{
								RefRequirement assElement = (RefRequirement) objectTarget;
								Iterator<RefRequirement> iter3 = lstrefRequirement.iterator();
								while (iter3.hasNext()) {
									RefRequirement refElementTo = (RefRequirement) iter3.next();
									if (CDOUtil.getCDOObject(assElement).cdoID().equals(CDOUtil.getCDOObject(refElementTo).cdoID())){
										modelViewerTarget.CheckedElement(refElementTo);
									}
								}
								assElement = null;	
							}
							
						} else if (sElementFilter.contentEquals("Role")) {
							if (objectTarget instanceof RefRole)
							{
								RefRole assElement = (RefRole) objectTarget;
								Iterator<RefRole> iter3 = lstrefRole.iterator();
								while (iter3.hasNext()) {
									RefRole refElementTo = (RefRole) iter3.next();
									if (CDOUtil.getCDOObject(assElement).cdoID().equals(CDOUtil.getCDOObject(refElementTo).cdoID())){
										modelViewerTarget.CheckedElement(refElementTo);
									}
								}
								assElement = null;
							}
							
						} else {
							if (objectTarget instanceof RefTechnique)
							{
								RefTechnique assElement = (RefTechnique) objectTarget;
								Iterator<RefTechnique> iter3 = lstrefTechnique.iterator();
								while (iter3.hasNext()) {
									RefTechnique refElementTo = (RefTechnique) iter3.next();
									if (CDOUtil.getCDOObject(assElement).cdoID().equals(CDOUtil.getCDOObject(refElementTo).cdoID())){
										modelViewerTarget.CheckedElement(refElementTo);
									}
								}
								assElement = null;	
							}
							
						}
					}
					
					
				
					//Tree Source All
					
					objectSourceAll = null;
					
					Iterator<RefAssurableElement> iterSA1 = lstassElement.iterator();
					while (iterSA1.hasNext()) {
						
						objectSourceAll = iterSA1.next();
						
						if (objectSourceAll instanceof RefActivity) {
							RefActivity assElement = (RefActivity) objectSourceAll;
							Iterator<RefActivity> iter3 = lstrefActivitySourceAll.iterator();
							while (iter3.hasNext()) {
								RefActivity refElementTo = (RefActivity) iter3.next();
								if (CDOUtil.getCDOObject(assElement).cdoID().equals(CDOUtil.getCDOObject(refElementTo).cdoID())){
									modelViewerSourceAll.CheckedElement(refElementTo);
								}
							}
							assElement = null;
							
						} else if (objectSourceAll instanceof RefArtefact) {
							RefArtefact assElement = (RefArtefact) objectSourceAll;
							Iterator<RefArtefact> iter3 = lstrefArtefactSourceAll.iterator();
							while (iter3.hasNext()) {
								RefArtefact refElementTo = (RefArtefact) iter3.next();
								if (CDOUtil.getCDOObject(assElement).cdoID().equals(CDOUtil.getCDOObject(refElementTo).cdoID())){
									modelViewerSourceAll.CheckedElement(refElementTo);
								}
							}
							assElement = null;
						} else if (objectSourceAll instanceof RefRequirement) {
							RefRequirement assElement = (RefRequirement) objectSourceAll;
							Iterator<RefRequirement> iter3 = lstrefRequirementSourceAll.iterator();
							while (iter3.hasNext()) {
								RefRequirement refElementTo = (RefRequirement) iter3.next();
								if (CDOUtil.getCDOObject(assElement).cdoID().equals(CDOUtil.getCDOObject(refElementTo).cdoID())){
									modelViewerSourceAll.CheckedElement(refElementTo);
								}
							}
							assElement = null;
						} else if (objectSourceAll instanceof RefRole) {
							RefRole assElement = (RefRole) objectSourceAll;
							Iterator<RefRole> iter3 = lstrefRoleSourceAll.iterator();
							while (iter3.hasNext()) {
								RefRole refElementTo = (RefRole) iter3.next();
								if (CDOUtil.getCDOObject(assElement).cdoID().equals(CDOUtil.getCDOObject(refElementTo).cdoID())){
									modelViewerSourceAll.CheckedElement(refElementTo);
								}
							}
							assElement = null;
						} else {
							RefTechnique assElement = (RefTechnique) objectSourceAll;
							Iterator<RefTechnique> iter3 = lstrefTechniqueSourceAll.iterator();
							while (iter3.hasNext()) {
								RefTechnique refElementTo = (RefTechnique) iter3.next();
								if (CDOUtil.getCDOObject(assElement).cdoID().equals(CDOUtil.getCDOObject(refElementTo).cdoID())){
									modelViewerSourceAll.CheckedElement(refElementTo);
								}
							}
							assElement = null;
						}
					}
				}
			}
		});
		
		Button NewComplianceMap = new Button(controlEquivMap, SWT.NONE);
		NewComplianceMap.setText("New Map");
		GridData NewComplianceMapData = new GridData();
		NewComplianceMapData.verticalAlignment = SWT.BEGINNING;
		NewComplianceMapData.horizontalAlignment = SWT.FILL;
		NewComplianceMap.setLayoutData(NewComplianceMapData);

		NewComplianceMap.addSelectionListener(new SelectionAdapter() {
			@SuppressWarnings("deprecation")
			public void widgetSelected(SelectionEvent e) {

				boolean bSelectedNode = false;
				boolean bSelectedMapGroup = false;
				//Verify the selection of one node of the from tree and a mapgroup. 
								
				IStructuredSelection sel = (IStructuredSelection) modelViewerSource.getSelection();
				object = (EObject) sel.getFirstElement();
				
				sElementFilter = cbFilterElementMap.getItem(cbFilterElementMap.getSelectionIndex());

				// descartar la selecci�n del framework
				if ((object instanceof RefFramework) || (object == null)) {
					bSelectedNode = false;
					MessageDialog.openError(getShell(), "New Equivalence Map",
							"It's neccesary to select a node from the framework tree.");
					return;
				} else {
					// Object is the assurableElement selected from the source Tree
					bSelectedNode = true;
					if (sElementFilter.contentEquals("Activity")) {
						RefActivity refElemtFrom = (RefActivity) object;
						System.out.println("Selected Element: " + refElemtFrom.getName());
					} else if (sElementFilter.contentEquals("Artefact")) {
						RefArtefact refElemtFrom = (RefArtefact) object;
						System.out.println("Selected Element: " + refElemtFrom.getName());
					} else if (sElementFilter.contentEquals("Requirement")) {
						RefRequirement refElemtFrom = (RefRequirement) object;
						System.out.println("Selected Element: " + refElemtFrom.getName());
					} else if (sElementFilter.contentEquals("Role")) {
						RefRole refElemtFrom = (RefRole) object;
						System.out.println(refElemtFrom.getName());
					} else {
						RefTechnique refElemtFrom = (RefTechnique) object;
						System.out.println("Selected Element: " + refElemtFrom.getName());
					}		
				}
				
				if ((cbMapGroup.getSelectionIndex() == -1)) { 
					bSelectedMapGroup = false;
				} else {
					bSelectedMapGroup = true;
				}
				
				if (!bSelectedMapGroup) {
					MessageDialog.openError(getShell(), "New Equivalence Map",
							"It's neccesary to select a map group.");
				} else if (!bSelectedNode) {
					MessageDialog.openError(getShell(), "New Equivalence Map",
							"It's neccesary to select a node from the framework tree.");
				} else {
					ComplianceMapNew complianceMapSet = new ComplianceMapNew(controlMappingNew
							.getShell(), object, mapGroup);
					
					if (complianceMapSet.open() == Window.OK) {
						
						if (complianceMapSet.getResult()) {
							
							//MessageDialog.openError(getShell(),"New Compliance Map", "New Compliance Map create.");							
							// Reload combo compliance map
							createModelFromFileToComboObject(cbEquivalenceMap,object);
							modelViewerTarget.setAllChecked(false);
							txtJustification.setText("");
							idText.setText("");
							nameText.setText("");
							
							cbEquivalenceMap.select(cbEquivalenceMap.getItemCount()-1);
							cbEquivalenceMap.notifyListeners(SWT.Selection, new Event());

						} else {
							MessageDialog
									.openError(getShell(), "New Map Group",
											"Fail to create a new map group. There is another map group with these values.");
						}
					}	
				}
			}
		});

		
		// Label
		Label idLabel = new Label(controlEquivMap, SWT.NONE);
		idLabel.setText("ID");
		GridData idLabelGridData = new GridData();
		idLabelGridData.horizontalAlignment = SWT.FILL;
		idLabelGridData.verticalAlignment = SWT.FILL;
		idLabel.setLayoutData(idLabelGridData);

		// Text
		idText = new Text(controlEquivMap, SWT.NONE);
		GridData idTextGridData = new GridData();
		idTextGridData.horizontalAlignment = SWT.FILL;
		idTextGridData.verticalAlignment = SWT.FILL;
		idText.setLayoutData(idTextGridData);
		
		
		// Label
		Label nameLabel = new Label(controlEquivMap, SWT.NONE);
		nameLabel.setText("Name");
		GridData nameLabelGridData = new GridData();
		nameLabelGridData.grabExcessHorizontalSpace = true;
		nameLabelGridData.minimumWidth = 20;
		nameLabelGridData.horizontalAlignment = SWT.FILL;
		nameLabelGridData.verticalAlignment = SWT.FILL;
		nameLabel.setLayoutData(nameLabelGridData);

		// Text
		nameText = new Text(controlEquivMap, SWT.NONE);
		GridData nameTextGridData = new GridData();
		nameTextGridData.grabExcessHorizontalSpace = true;
		nameTextGridData.minimumWidth = 20;
		nameTextGridData.horizontalAlignment = SWT.FILL;
		nameTextGridData.verticalAlignment = SWT.FILL;
		nameText.setLayoutData(nameTextGridData);
		
		
		// Text
		Label typeLabel = new Label(controlEquivMap, SWT.NONE);
		typeLabel.setText("Type");
		GridData typeLabelGridData = new GridData();
		typeLabelGridData.grabExcessHorizontalSpace = true;
		typeLabelGridData.minimumWidth = 20;
		typeLabelGridData.horizontalAlignment = SWT.FILL;
		typeLabelGridData.verticalAlignment = SWT.FILL;
		typeLabel.setLayoutData(typeLabelGridData);
		
		cbType = new Combo (controlEquivMap, SWT.READ_ONLY);
		GridData cbTypeGridData = new GridData();
		cbTypeGridData.verticalAlignment = SWT.FILL;
		cbTypeGridData.horizontalAlignment = SWT.FILL;
		cbTypeGridData.widthHint = Display.getCurrent().getBounds().width / 7;
		cbTypeGridData.heightHint = Display.getCurrent().getBounds().height / 7;
		cbTypeGridData.grabExcessHorizontalSpace = true;
		cbTypeGridData.grabExcessVerticalSpace = true;
		cbType.setLayoutData(cbTypeGridData);
		
		EEnum enumMapKind = MappingFactory.eINSTANCE.getMappingPackage().getMapKind();
		EList<EEnumLiteral> mapkindList= enumMapKind.getELiterals();
		
		Iterator<EEnumLiteral> iterTo = mapkindList.iterator();
		while (iterTo.hasNext()) {
			cbType.add(iterTo.next().getName());
		}
		cbType.select(0);
		
		Label justificationLabel = new Label(groupEquMap, SWT.NONE);
		justificationLabel.setText("Justification");
		GridData justificationLabelGridData = new GridData();
		justificationLabelGridData.horizontalAlignment = SWT.FILL;
		justificationLabelGridData.verticalAlignment = SWT.FILL;
		justificationLabel.setLayoutData(justificationLabelGridData);
		
		txtJustification = new Text(groupEquMap, SWT.MULTI);
		GridData txtJustificationGridData = new GridData();
		txtJustificationGridData.horizontalSpan = 2;
		txtJustificationGridData.horizontalAlignment = SWT.FILL;
		txtJustificationGridData.verticalAlignment = SWT.FILL;
		//txtJustificationGridData.heightHint = Display.getCurrent().getBounds().height / 9;
		txtJustificationGridData.heightHint = 150;//Display.getCurrent().getBounds().height / 3;
		txtJustification.setLayoutData(txtJustificationGridData);
	}


	protected void createTargetDialogArea(final Composite contents,ComposedAdapterFactory adapterFactory) {
		
				
		Composite targetComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			targetComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1;
			targetComposite.setLayout(layout);
		}

		//Label
		Label targetLabel = new Label(targetComposite, SWT.NONE);
		targetLabel.setText("To");
		GridData targetLabelGridData = new GridData();
		targetLabelGridData.horizontalSpan = 2;
		targetLabelGridData.horizontalAlignment = SWT.FILL;
		targetLabelGridData.verticalAlignment = SWT.FILL;
		targetLabel.setLayoutData(targetLabelGridData);

		//Combo
		cbTarget = new Combo (targetComposite, SWT.READ_ONLY);
		GridData cbTargetGridData = new GridData();
		cbTargetGridData.verticalAlignment = SWT.FILL;
		cbTargetGridData.horizontalAlignment = SWT.FILL;
		cbTargetGridData.widthHint = Display.getCurrent().getBounds().width / 5;
		cbTargetGridData.heightHint = Display.getCurrent().getBounds().height / 5;
		cbTargetGridData.grabExcessHorizontalSpace = true;
		cbTargetGridData.grabExcessVerticalSpace = true;
		cbTarget.setLayoutData(cbTargetGridData);		
		
		CDOResourceNode[] listR=  mCDOView.getElements();			
		for (int i = 0; i < listR.length; i++){
			if (listR[i] instanceof CDOResourceFolder) {
				checkFolderContentsREF((CDOResourceFolder) listR[i], REFFRAMEWORK);
			} else if (listR[i].getName().endsWith(REFFRAMEWORK)) {
				refListDirRef.add(listR[i].getPath());
			}	
		}		
		
		//Add the refframework's name to the combo
		Iterator<String> iterTo = refListDirRef.iterator();
		String sValorTo = "";
		while (iterTo.hasNext()) {
			sValorTo =  iterTo.next().toString();
			cbTarget.add(sValorTo);
		}	

		cbTarget.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (cbMapGroup.getSelectionIndex() != -1) { 
					sourceComposite.setEnabled(true);
				}
				createModelFromFilePath(modelViewerTarget, refListDirRef.get(cbTarget.getSelectionIndex()));
				modelViewerTarget.expandAll();
			}
		});

		modelViewerTarget = new CheckboxTreeViewerExt(targetComposite, 
				SWT.MULTI| SWT.H_SCROLL|SWT.V_SCROLL|SWT.BORDER);

		GridData modelViewerGridData1 = new GridData();
		modelViewerGridData1.verticalAlignment = SWT.FILL;
		modelViewerGridData1.horizontalAlignment = SWT.FILL;
		modelViewerGridData1.widthHint = Display.getCurrent().getBounds().width / 3;
		modelViewerGridData1.heightHint = Display.getCurrent().getBounds().height / 3;
		modelViewerGridData1.grabExcessHorizontalSpace = true;
		modelViewerGridData1.grabExcessVerticalSpace = true;
		modelViewerTarget.getTree().setLayoutData(modelViewerGridData1);
		modelViewerTarget.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		modelViewerTarget.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		options = new HashMap<Object, Object>();
		options.put(XMLResource.OPTION_ENCODING, UTF_8);
				
		ICheckStateListener checkStateListener = new ICheckStateListener(){
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				
				Object changedElement =  event.getElement();
				boolean status = modelViewerTarget.getChecked(changedElement);
		
				// Para descartar la selecci�n del framework comprobamos si su container es null
				if ((status) && (changedElement instanceof RefFramework)) {
					modelViewerTarget.setChecked(changedElement, false);
					MessageDialog.openError(getShell(), "Equivalence Map",
							"It's not possible to do an equivalence map against a root node. Select another node. ");
					return;
				}
				
				
				//Before check the selection of a mapping model, a map group, an equivalence map and a source
				
				if ((qref == "") || (cbMapGroup.getSelectionIndex() == -1) ||  (cbEquivalenceMap.getSelectionIndex() == -1))   { 

					MessageDialog.openError(getShell(), "Equivalence Map",
							"Check that you have selected: a map group, an equivalence map and a node from the source framework tree.");
					
					if (status) {
						modelViewerTarget.setChecked(changedElement, false);
					} else {
						modelViewerTarget.setChecked(changedElement, true);
					}		
				} else {
					
					try {
										
						if (status) {						
							targetSettings.addToReference((EObject) changedElement);
							boolean pp = PlatformUI.getWorkbench().saveAllEditors(true);

							if (pp) {
								
								targetSettings.getSource().eResource().save(options);
								mCDOTransaction.commit();
							}
						} else {
							boolean pp = PlatformUI.getWorkbench().saveAllEditors(true);
							if (pp)
								removeFromReferenceOpencert((EObject) changedElement);
							targetSettings.getSource().eResource().save(options);
						}
						
					} catch (IOException e) {
						e.printStackTrace();
					} catch (ConcurrentAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (CommitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
				}
			}
		};
		
		modelViewerTarget.addCheckStateListener(checkStateListener);
		
		
		// Start 20141204 IRR
		final Composite compPostConditions = new Composite(targetComposite, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.FILL;
			dataNew.verticalAlignment = SWT.CENTER;
			compPostConditions.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			layoutNew.makeColumnsEqualWidth = true;
			
			compPostConditions.setLayout(layoutNew);
		}

		Label lblPostConditions = new Label(compPostConditions, SWT.NONE);
		lblPostConditions.setText("PostConditions");
		GridData lblPostConditionsGridData = new GridData();
		lblPostConditionsGridData.horizontalAlignment = SWT.FILL;
		lblPostConditionsGridData.verticalAlignment = SWT.CENTER;
		lblPostConditionsGridData.widthHint =  Display.getCurrent().getBounds().width / 8;
		lblPostConditionsGridData.heightHint = Display.getCurrent().getBounds().height / 30;
		lblPostConditionsGridData.grabExcessHorizontalSpace = true;
		lblPostConditionsGridData.grabExcessVerticalSpace = true;
		lblPostConditions.setLayoutData(lblPostConditionsGridData);
		
		Button btSave = new Button(compPostConditions,SWT.NONE);
		btSave.setText("Save");
		
		//Image imageSave = new Image(compPostConditions.getDisplay(),"Save.png");
		//btSave.setImage(imageSave);
		
		
		
		GridData NewGroupMapGridData = new GridData();
		NewGroupMapGridData.horizontalAlignment = SWT.RIGHT;
		NewGroupMapGridData.verticalAlignment = SWT.CENTER;
		//NewGroupMapGridData.widthHint =  Display.getCurrent().getBounds().width / 10;
		NewGroupMapGridData.heightHint = Display.getCurrent().getBounds().height / 30;
		System.out.println("height : " + Display.getCurrent().getBounds().height);
		NewGroupMapGridData.grabExcessHorizontalSpace = true;
		NewGroupMapGridData.grabExcessVerticalSpace = true;
		
		btSave.setLayoutData(NewGroupMapGridData);
		
		
		
		btSave.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					PlatformUI.getWorkbench().saveAllEditors(true);
					mCDOTransaction.commit();
				} catch (ConcurrentAccessException e1) {
					e1.printStackTrace();
				} catch (CommitException e1) {
					e1.printStackTrace();
				} 
			}
		});
		
		modelViewerSourceAll  = new CheckboxTreeViewerExt(targetComposite, 
				SWT.MULTI| SWT.H_SCROLL|SWT.V_SCROLL|SWT.BORDER);

		GridData modelViewerSourceAllGridData = new GridData();
		modelViewerSourceAllGridData.verticalAlignment = SWT.FILL;
		modelViewerSourceAllGridData.horizontalAlignment = SWT.FILL;
		modelViewerSourceAllGridData.widthHint = Display.getCurrent().getBounds().width / 3;
		modelViewerSourceAllGridData.heightHint = Display.getCurrent().getBounds().height / 3;
		modelViewerSourceAllGridData.grabExcessHorizontalSpace = true;
		modelViewerSourceAllGridData.grabExcessVerticalSpace = true;
		modelViewerSourceAll.getTree().setLayoutData(modelViewerSourceAllGridData);
		modelViewerSourceAll.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		modelViewerSourceAll.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		createModelSourceAll(modelViewerSourceAll, resourceFrom);
		modelViewerSourceAll.expandAll();

		ICheckStateListener checkStateListenerSourceAll = new ICheckStateListener(){
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				Object changedElement =  event.getElement();
				boolean status = modelViewerSourceAll.getChecked(changedElement);
		
				// Para descartar la selecci�n del framework comprobamos si su container es null
				if ((status) && (changedElement instanceof RefFramework)) {
					modelViewerSourceAll.setChecked(changedElement, false);
					MessageDialog.openError(getShell(), "Equivalence Map",
							"It's not possible to do an equivalence map against a root node. Select another node. ");
					return;
				}
						
				if ((status) && (changedElement instanceof RefEquivalenceMap)) {
					modelViewerSourceAll.setChecked(changedElement, false);				
					return;
				}
				
				//Before check the selection of a mapping model, a map group, an equivalence map and a source
				if ((qref == "") || (cbMapGroup.getSelectionIndex() == -1) ||  (cbEquivalenceMap.getSelectionIndex() == -1))   { 

					MessageDialog.openError(getShell(), "Equivalence Map",
							"Check that you have selected: a map group, an equivalence map and a node from the source framework tree.");
					
					if (status) {
						modelViewerSourceAll.setChecked(changedElement, false);
					} else {
						modelViewerSourceAll.setChecked(changedElement, true);
					}		
				} else {
					
					try {					
						if (status) {						
							targetSettings.addToReference((EObject) changedElement);
							boolean pp = PlatformUI.getWorkbench().saveAllEditors(true);
							if (pp) {
								targetSettings.getSource().eResource().save(options);
								mCDOTransaction.commit();
							}
						} else {
							boolean pp = PlatformUI.getWorkbench().saveAllEditors(true);
							if (pp)
								removeFromReferenceOpencert((EObject) changedElement);
							targetSettings.getSource().eResource().save(options);
						}
					} catch (IOException e) {
						e.printStackTrace();
					} catch (ConcurrentAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (CommitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
				}
			}
		};
		
		modelViewerSourceAll.addCheckStateListener(checkStateListenerSourceAll);

		BasicCommandStack commandStack = new BasicCommandStack();
		EditingDomain editingDomain = new AdapterFactoryEditingDomain(adapterFactory, commandStack, new HashMap<Resource, Boolean>());
		modelViewerSourceAll.addDoubleClickListener(new OpenWizardOnDoubleClick(editingDomain, adapterFactory));
		
		
		// End IRR
	}

	
	@SuppressWarnings("unchecked")
	public void removeFromReferenceOpencert(EObject valueToRemove) {
		
		EReference ref = targetSettings.getLastReference();
		Object value1 = targetSettings.getSource().eGet(ref);
		
		int index = -1;
		int i = 0;
		
		for(Iterator<EObject> itr = ((List<EObject>)value1).iterator();itr.hasNext();)  
	        {  
	            EObject element = itr.next();
	            i ++;
	            
	            if (valueToRemove instanceof RefAssurableElement) {
	            	RefAssurableElement refActivitytoRemove = (RefAssurableElement)valueToRemove;
					RefAssurableElement refElem = (RefAssurableElement) element;
					
					if (CDOUtil.getCDOObject(refActivitytoRemove).cdoID().equals(CDOUtil.getCDOObject(refElem).cdoID())) {
						index = i;
					}
				}
	        }
		if (index != -1) {
			index--;
			((List<EObject>) value1).remove(index);
		}
	}
	

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Equivalence Map");
	}

	
	public void createModelFromFileToComboObject(Combo combo, Object object) {

		lstEquivalenceMap.clear();
		cbEquivalenceMap.removeAll();
		
		Iterator<RefEquivalenceMap> iter1 = ((RefAssurableElement)object).getEquivalence().iterator();
		while (iter1.hasNext()) {	
			
			MapGroup mapGroupFrom = null;									
			final RefEquivalenceMap refEquiMapFrom = (RefEquivalenceMap) iter1.next();

			if (refEquiMapFrom.getMapGroup() != null){
				mapGroupFrom = refEquiMapFrom.getMapGroup();
			} else {
				mapGroupFrom = null;
			}
			if (CDOUtil.getCDOObject(mapGroupFrom).cdoID().equals(CDOUtil.getCDOObject(mapGroup).cdoID())){
				bEqualMapGroup = true;
				
				//A�ado el objeto complianceMap a la combo
				//Cunado cree uno nuevo debere a�adirlo a la lista
				lstEquivalenceMap.add(refEquiMapFrom);
				cbEquivalenceMap.add(refEquiMapFrom.getName());
				mapGroupFrom = null;
			} 
		}
	}
	
	
	/**
	 * CreateModelFromFile: load the resource of the model
	 * @param tviewer widget in which the model will be visualized
	 * @param model string with the path and name of the model in the file system
	 */
	public void CreateModelFromFiletocombo(Combo combo, String spath){
		if (spath != null) {
			
			Resource resource = null;
			try {
				
				resource = mCDOView.getResource(spath, true);
				
				EList<EObject> rsobject  = resource.getContents();
				
				Iterator<EObject> iterTo = rsobject.iterator();
				
				combo.removeAll();
				
				while (iterTo.hasNext()) {
					mapModel = (MapModel)iterTo.next();
					MapModelPath = spath;
					lstMapGroup = mapModel.getMapGroupModel();
					Iterator<MapGroup> itermapGroup = lstMapGroup.iterator();
					while (itermapGroup.hasNext()) {
						combo.add(itermapGroup.next().getName());
					}		
				}	
			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
			}
		}
	}


	/**
	 * Load the resource of the model from a {@link CDOResource}.
	 *
	 * @param pTreeViewer
	 *            The tree viewer in which the model will be visualized
	 * @param pCDOResource
	 *            The CDO resource
	 */
	public void createModelFromCDOResource(TreeViewer pTreeViewer, CDOResource pCDOResource) {
		if (resourceURI != null) {
			try {
				pTreeViewer.setInput(null);
				pTreeViewer.setInput(pCDOResource);
				final ViewerFilter modelFilter = new ViewerFilter() {
					public boolean select(
							Viewer viewer,
							Object parentElement,
							Object element) {
						if (element instanceof RefFramework)
							return true;
						
						if (element instanceof RefEquivalenceMap) {							
							return true;
						}
						if (element instanceof RefActivity) {
							
							if (sElementFilter.contentEquals("Requirement")) {
								
								RefActivity refactivity = (RefActivity) element;
								//Verificar si esta actividad o alguna de sus hijas tiene requisitos, y si es as� devolver true
								
								return bVerifyReActivity(refactivity);
								
							} else {
								if (sElementFilter.contentEquals("Activity"))
									return true;
								else
									return false;
							}
							
						}
						else if (element instanceof RefArtefact)
							if (sElementFilter.contentEquals("Artefact"))
								return true;
							else
								return false;
						else if (element instanceof RefRole)
							if (sElementFilter.contentEquals("Role"))
								return true;
							else
								return false;
						else if (element instanceof RefRequirement)
							if (sElementFilter.contentEquals("Requirement"))
								return true;
							else
								return false;
						else if (element instanceof RefTechnique)
							if (sElementFilter.contentEquals("Technique"))
								return true;
							else
								return false;
						else
							return false;
					}
				};
				
				pTreeViewer.addFilter(modelFilter);

			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
				pTreeViewer.setInput(null);
			}
		}
	}

	
	/**
	 * Load a model from a CDO resource without filtering.
	 * 
	 * @param tviewer
	 *            widget in which the model will be visualized
	 * @param resource
	 *            The CDO resource
	 */	
	public void createModelSourceAll(TreeViewer tviewer, CDOResource resource){
		
		lstrefActivitySourceAll = new BasicEList<RefActivity>();
		lstrefArtefactSourceAll = new BasicEList<RefArtefact>();
		lstrefRequirementSourceAll = new BasicEList<RefRequirement>();
		lstrefRoleSourceAll = new BasicEList<RefRole>();
		lstrefTechniqueSourceAll = new BasicEList<RefTechnique>();
		
		if (resourceURI != null) {
			try {
				tviewer.setInput(null);
				tviewer.setInput(resource);
				final ViewerFilter modelFilter = new ViewerFilter() {
					public boolean select(
							Viewer viewer,
							Object parentElement,
							Object element) {
						if (element instanceof RefFramework)
							return true;
						if (element instanceof RefActivity) {
							lstrefActivitySourceAll.add((RefActivity) element);
							return true;
						}
						else if (element instanceof RefArtefact){
							lstrefArtefactSourceAll.add((RefArtefact) element);	
							return true;
						}
						else if (element instanceof RefRole){
							lstrefRoleSourceAll.add((RefRole) element);
							return true;
						}
						else if (element instanceof RefRequirement){
							lstrefRequirementSourceAll.add((RefRequirement) element);
							return true;
						}
						else if (element instanceof RefTechnique){
							lstrefTechniqueSourceAll.add((RefTechnique) element);
							return true;
						}
							
						else
							return false;
					}
				};
				
				tviewer.addFilter(modelFilter);

			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
				tviewer.setInput(null);
			}
		}
	}
	
	/**
	 * Load the resource of the model from a file path:
	 * 
	 * @param tviewer
	 *            widget in which the model will be visualized
	 * @param sModel
	 *            string with the path and name of the model in the file system
	 */
	public void createModelFromFilePath(TreeViewer tviewer, String sModel) {
		
		lstrefActivity = new BasicEList<RefActivity>();
		lstrefArtefact= new BasicEList<RefArtefact>();
		lstrefRequirement = new BasicEList<RefRequirement>();
		lstrefRole= new BasicEList<RefRole>();
		lstrefTechnique= new BasicEList<RefTechnique>();
		
		if (sModel != null) {
			
			Resource resource = null;
			try {
				resource = mCDOView.getResource(sModel, true);
				tviewer.setInput(null);
				tviewer.setInput(resource);
				final ViewerFilter modelFilter = new ViewerFilter() {
					public boolean select(
							Viewer viewer,
							Object parentElement,
							Object element) {
						if (element instanceof RefFramework)
							return true;
						if (element instanceof RefActivity)
							
							if (sElementFilter.contentEquals("Requirement")) {
								
								RefActivity refactivity = (RefActivity) element;
								
								return bVerifyReActivity(refactivity);
								
							} else {
								if (sElementFilter.contentEquals("Activity")){
									lstrefActivity.add((RefActivity) element);
									return true;
								}
								else
									return false;
							}
							
						else if (element instanceof RefArtefact)
							if (sElementFilter.contentEquals("Artefact")){
								lstrefArtefact.add((RefArtefact) element);	
								return true;
							}
							else
								return false;
						else if (element instanceof RefRole)
							if (sElementFilter.contentEquals("Role")){
								lstrefRole.add((RefRole) element);
								return true;
							}
							else
								return false;
						else if (element instanceof RefRequirement)
							if (sElementFilter.contentEquals("Requirement")){
								lstrefRequirement.add((RefRequirement) element);	
								return true;
							}
							else
								return false;
						else if (element instanceof RefTechnique)
							if (sElementFilter.contentEquals("Technique")){
								lstrefTechnique.add((RefTechnique) element);
								return true;
							}
							else
								return false;
						else
							return false;
					}
				};
				tviewer.addFilter(modelFilter);

			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
				tviewer.setInput(null);
			}
		}
	}

	
	protected void addEquivalenceMap(MapGroup mapGroup,EObject object) throws ConcurrentAccessException, CommitException{
		
		//Comprobrar que se haya seleccionado un elemento source
		
		if (object != null) {
			RefEquivalenceMap refEquivalenceMap =  RefframeworkFactory.eINSTANCE.createRefEquivalenceMap(); 	
			refEquivalenceMap.setMapGroup(mapGroup);

			if (idText.getText() != null)
				refEquivalenceMap.setId(idText.getText());
			if (nameText.getText() != null)
				refEquivalenceMap.setName(nameText.getText());
			if (txtJustification.getText() != null) {
				mapJustification = MappingFactory.eINSTANCE.createMapJustification();
				mapJustification.setExplanation(txtJustification.getText());
				refEquivalenceMap.setMapJustification(mapJustification);
			}
			if (cbType.getSelectionIndex() != -1) {
				String value = cbType.getItem(cbType.getSelectionIndex()); 
				refEquivalenceMap.setType(MapKind.get(value));
			}
			

			targetSettings = new ReferencesTableSettings(refEquivalenceMap, RefframeworkPackage.eINSTANCE.getRefEquivalenceMap_Target());

			ReferencesTableSettings objectEquivalenceMapSettings;
			
			//Object is the assurableElement selected from the source Tree
			if (sElementFilter.contentEquals("Activity")) {
				RefActivity refElemtFrom = (RefActivity) object;
				objectEquivalenceMapSettings = new ReferencesTableSettings(refElemtFrom, RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence());
				refElemtFrom = null;
			} else if (sElementFilter.contentEquals("Artefact")) {
				RefArtefact refElemtFrom = (RefArtefact) object;
				objectEquivalenceMapSettings = new ReferencesTableSettings(refElemtFrom, RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence());
				refElemtFrom = null;
			} else if (sElementFilter.contentEquals("Requirement")) {
				RefRequirement refElemtFrom = (RefRequirement) object;
				objectEquivalenceMapSettings = new ReferencesTableSettings(refElemtFrom, RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence());
				refElemtFrom = null;
			} else if (sElementFilter.contentEquals("Role")) {
				RefRole refElemtFrom = (RefRole) object;
				objectEquivalenceMapSettings = new ReferencesTableSettings(refElemtFrom, RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence());
				refElemtFrom = null;
			} else {
				RefTechnique refElemtFrom = (RefTechnique) object;
				objectEquivalenceMapSettings = new ReferencesTableSettings(refElemtFrom, RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence());
				refElemtFrom = null;
			}
			
			

			if (objectEquivalenceMapSettings.contains(refEquivalenceMap)) {

			} else {
				objectEquivalenceMapSettings.addToReference(refEquivalenceMap);	
			}
			
			try {				
				objectEquivalenceMapSettings.getSource().eResource().save(options);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//Update the list of equivalenceMap
			if (sElementFilter.contentEquals("Activity")) {
				RefActivity refElemtFrom = (RefActivity) object;
				lstrefEquiMapFrom = refElemtFrom.getEquivalence();
			} else if (sElementFilter.contentEquals("Artefact")) {
				RefArtefact refElemtFrom = (RefArtefact) object;
				lstrefEquiMapFrom = refElemtFrom.getEquivalence();
			} else if (sElementFilter.contentEquals("Requirement")) {
				RefRequirement refElemtFrom = (RefRequirement) object;
				lstrefEquiMapFrom = refElemtFrom.getEquivalence();
			} else if (sElementFilter.contentEquals("Role")) {
				RefRole refElemtFrom = (RefRole) object;
				lstrefEquiMapFrom = refElemtFrom.getEquivalence();
			} else {
				RefTechnique refElemtFrom = (RefTechnique) object;
				lstrefEquiMapFrom = refElemtFrom.getEquivalence();
			}
		} else {
			MessageDialog.openError(getShell(), "Not selected element", "It's neccesary to select a source element");
		}
	}
	
	@Override
	protected void okPressed()
	{
		super.okPressed();
	}

	@Override
	public boolean close()
	{
		return super.close();
	}
	

	@Override
	protected Button createButton(Composite parent, int id,
	        String label, boolean defaultButton) {
	    if (id == IDialogConstants.CANCEL_ID) return null;
	    return super.createButton(parent, id, label, defaultButton);
	}
	
	public Boolean getResult()
	{
		return true;
	}
	
	public ResourceLocator getResourceLocator() {
		return RefframeworkEditPlugin.INSTANCE;
	}
	
	private boolean bVerifyReActivity(RefActivity activity) {	
		boolean bResultado = false;
		
		// Verify if requirement
		if (activity.getOwnedRequirement().size() > 0) {
			return true;
		} else {
			// Verify it childs
			if (activity.getSubActivity().size() > 0) {
				EList<RefActivity> lstRefActivity = activity.getSubActivity();
				Iterator<RefActivity> iter1 = lstRefActivity.iterator();
				while (iter1.hasNext() && !bResultado) {
					RefActivity refActivity = (RefActivity) iter1.next();
					bResultado = bVerifyReActivity(refActivity);
				}
			}
		}
		return bResultado;
	}

	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	protected void checkFolderContents(CDOResourceFolder cdoResourceFolder, String sCadena) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN = cdoResourceFolder.getNodes();
			for (int i = 0; i < listN.size(); i++) {
				if (listN.get(i) instanceof CDOResourceFolder) {
					checkFolderContents((CDOResourceFolder) listN.get(i), sCadena);
				} else if (listN.get(i).getName().endsWith(sCadena)) {
					if (Objects.nonNull(refList)) {
						refList.add(listN.get(i).getName());
					}
					refListDirMap.add(listN.get(i).getPath());
				}
			}
		}
	}
	
	protected void checkFolderContentsREF(CDOResourceFolder cdoResourceFolder, String sCadena) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN = cdoResourceFolder.getNodes();
			for (int i = 0; i < listN.size(); i++) {
				if (listN.get(i) instanceof CDOResourceFolder) {
					checkFolderContents((CDOResourceFolder) listN.get(i), sCadena);
				} else if (listN.get(i).getName().endsWith(sCadena)) {
					refListDirRef.add(listN.get(i).getPath());
				}
			}
		}
	}
}
