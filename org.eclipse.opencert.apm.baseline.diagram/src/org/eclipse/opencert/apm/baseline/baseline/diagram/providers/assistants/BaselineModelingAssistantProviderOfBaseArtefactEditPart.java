/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineModelingAssistantProvider;

/**
 * @generated
 */
public class BaselineModelingAssistantProviderOfBaseArtefactEditPart extends
		BaselineModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((BaseArtefactEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(BaseArtefactEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(2);
		types.add(BaselineElementTypes.BaseActivityRequiredArtefact_4001);
		types.add(BaselineElementTypes.BaseActivityProducedArtefact_4002);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((BaseArtefactEditPart) targetEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(BaseArtefactEditPart target,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == BaselineElementTypes.BaseActivityRequiredArtefact_4001) {
			types.add(BaselineElementTypes.BaseActivity_2001);
			types.add(BaselineElementTypes.BaseActivity_3001);
		} else if (relationshipType == BaselineElementTypes.BaseActivityProducedArtefact_4002) {
			types.add(BaselineElementTypes.BaseActivity_2001);
			types.add(BaselineElementTypes.BaseActivity_3001);
		}
		return types;
	}

}
