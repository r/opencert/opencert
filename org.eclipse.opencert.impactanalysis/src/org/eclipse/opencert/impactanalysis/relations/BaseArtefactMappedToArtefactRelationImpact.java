/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.relations;

import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

public class BaseArtefactMappedToArtefactRelationImpact
    extends AbstractArtefactsRelationImpact
{
    private ChangeEffectKind impactingBaseArtefactRelModificationEffect;
    private ChangeEffectKind impactingBaseArtefactRelRevocationEffect;
    
    private String userFriendlyImpactedArtefactName;
    private String userFriendlyImpactingArtefactName;
    
    
    public BaseArtefactMappedToArtefactRelationImpact(int recursionDepth,
            ChangeEffectKind impactingBaseArtefactRelModificationEffect,
            ChangeEffectKind impactingBaseArtefactRelRevocationEffect,
            BaseArtefact impactingBaseArtefact, Artefact impactedMappedArtefact,
            ChangeEffectKind impactedChangeEffectKind,
            EventKind impactingEventKind)
    {
        super(recursionDepth, ArtefactRelationType.BASELINE_TO_EVIDENCE_VIA_MAP_RELATION, 
                impactedChangeEffectKind, impactingEventKind, 
                CDOStorageUtil.getCDOId(impactingBaseArtefact) + "->" + CDOStorageUtil.getCDOId(impactedMappedArtefact),
                CDOStorageUtil.getCDOId(impactingBaseArtefact),
                CDOStorageUtil.getCDOId(impactedMappedArtefact));
        
        this.impactingBaseArtefactRelModificationEffect = impactingBaseArtefactRelModificationEffect;
        this.impactingBaseArtefactRelRevocationEffect = impactingBaseArtefactRelRevocationEffect;
        
        userFriendlyImpactedArtefactName = impactedMappedArtefact.getName();
        userFriendlyImpactingArtefactName = impactingBaseArtefact.getName();
    }
    
    
    /**
     * This is proxy relation, it should forward "received" event and mimic original modificationChangeEffectKind and revocationChangeEffectKind
     */
    @Override
    public EventKind generateNewEventBasedOnChangeEffectKindOrForwardCurrent(
            EventKind impactingEventKind)
    {
        return  impactingEventKind;
    }
    
    
    protected String getRelationTypeExtraInfo()
    {
        return "[Leaving baseline via compliance map]";
    }
    
    
    @Override
    public String getUserFriendlyRelationName()
    {
        return "compliance map[" + getImpactingArtefactRelationUniqueId() + "]";
    }

    
    @Override
    public String getUserFriendlyImpactedArtefactName()
    {
        return userFriendlyImpactedArtefactName;
    }

    
    @Override
    public String getUserFriendlyImpactingArtefactName()
    {
        return userFriendlyImpactingArtefactName;
    }


    public ChangeEffectKind getImpactingBaseArtefactRelModificationEffect()
    {
        return impactingBaseArtefactRelModificationEffect;
    }


    public ChangeEffectKind getImpactingBaseArtefactRelRevocationEffect()
    {
        return impactingBaseArtefactRelRevocationEffect;
    }
}
