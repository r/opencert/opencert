@rem ***************************************************************************
@rem Copyright (c) 2016 KPIT Technologies.
@rem
@rem All rights reserved. This program and the accompanying materials
@rem are made available under the terms of the Eclipse Public License v1.0
@rem which accompanies this distribution, and is available at
@rem http://www.eclipse.org/legal/epl-v10.html
@rem
@rem Contributors:
@rem   Jan Mauersberger- initial API and implementation
@rem   Sascha Baumgart- initial API and implementation
@rem ***************************************************************************
cd workspace\workspace\org.eclipse.opencert.userguidance\src\org\opencert\userguidance\labelparser

rd /s /q generated
mkdir generated

java -jar antlr-4.1-complete.jar -o generated Label.g4


pause