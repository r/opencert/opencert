/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.exttools.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.opencert.externaltools.api.AbstractExternalToolConnector;
import org.eclipse.opencert.externaltools.api.ExternalToolQuery;

import com.j2bugzilla.base.Bug;
import com.j2bugzilla.base.BugzillaConnector;
import com.j2bugzilla.base.BugzillaException;
import com.j2bugzilla.base.ConnectionException;
import com.j2bugzilla.rpc.BugSearch;
import com.j2bugzilla.rpc.BugzillaVersion;
import com.j2bugzilla.rpc.LogIn;
import com.j2bugzilla.rpc.BugSearch.SearchLimiter;
import com.j2bugzilla.rpc.BugSearch.SearchQuery;
import com.j2bugzilla.rpc.LogOut;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class ComplianceBugzillaConnector
        extends AbstractExternalToolConnector
{
    private static final String PASSWORD_KEY = "Password";
    private static final String PRODUCT_KEY = "Product";
    private static final String SEVERITY_KEY = "Severity";
    private static final String PRIORITY_KEY = "Priority";
    private static final String RESOLUTION_KEY = "Resolution";
    private static final String STATUS_KEY = "Status";
    private static final String TARGET_MILESTONE_KEY = "Target Milestone";

    @Override
    public String getName()
    {
        return "Bugzilla Connector";
    }

    @Override
    public String getDescription()
    {
        return "Connects to Bugzilla server and retrieves a number of defects " + "reported for the specific Bugzilla product. "
                + "Additional parameters, like Severity, Status, Resolution, can be specified. " + "Note: it uses Bugzilla XML-RPC API.";
    }

    @Override
    public List<String> getSettingKeys()
    {
        List<String> keys = new ArrayList<String>();

        keys.add(URL_KEY);
        keys.add(USER_KEY);
        keys.add(PASSWORD_KEY);
        keys.add(PRODUCT_KEY);
        keys.add(SEVERITY_KEY);
        keys.add(PRIORITY_KEY);
        keys.add(RESOLUTION_KEY);
        keys.add(STATUS_KEY);
        keys.add(TARGET_MILESTONE_KEY);

        return keys;
    }

    @Override
    public Map<String, String> getSettingDefaultValues()
    {
        Map<String, String> keysAndValuesMap = new LinkedHashMap<String, String>();

        // keysAndValuesMap.put(URL_KEY, "https://bugzilla.parasoft.com/");
        // keysAndValuesMap.put(LOGIN_KEY, ""); //put your login
        // keysAndValuesMap.put(PASSWORD_KEY, ""); //put your password
        // keysAndValuesMap.put(PRODUCT_KEY, "C++Test");
        // keysAndValuesMap.put(SEVERITY_KEY, "Critical"); //Critical, High,
        // Medium, Feature_Req
        // keysAndValuesMap.put(PRIORITY_KEY, "high");
        // keysAndValuesMap.put(RESOLUTION_KEY, "INVALID"); //INVALID, FIXED,
        // FIXED, , DUPLICATE, RETIRED,
        // keysAndValuesMap.put(STATUS_KEY, "RESOLVED"); //RESOLVED, VERIFIED,
        // RESOLVED, NEW, RESOLVED, RESOLVED,
        // keysAndValuesMap.put(TARGET_MILESTONE_KEY, "9.0.0.118");

        // keysAndValuesMap.put(URL_KEY, "https://bugzilla.mozilla.org/");
        // keysAndValuesMap.put(LOGIN_KEY, "bugzillaocquery@gmail.com");
        // keysAndValuesMap.put(PASSWORD_KEY, "bugzillaOC1");
        // keysAndValuesMap.put(PRODUCT_KEY, "Firefox");
        // keysAndValuesMap.put(SEVERITY_KEY, "");
        // keysAndValuesMap.put(PRIORITY_KEY, "");
        // keysAndValuesMap.put(RESOLUTION_KEY, "");
        // keysAndValuesMap.put(STATUS_KEY, "REOPENED");
        // keysAndValuesMap.put(TARGET_MILESTONE_KEY, "");

        keysAndValuesMap.put(URL_KEY, "https://issues.apache.org/bugzilla/");
        keysAndValuesMap.put(USER_KEY, "");
        keysAndValuesMap.put(PASSWORD_KEY, "");
        keysAndValuesMap.put(PRODUCT_KEY, "Ant");
        keysAndValuesMap.put(SEVERITY_KEY, "critical");
        keysAndValuesMap.put(PRIORITY_KEY, "P3");
        keysAndValuesMap.put(RESOLUTION_KEY, "");
        keysAndValuesMap.put(STATUS_KEY, "NEW");
        keysAndValuesMap.put(TARGET_MILESTONE_KEY, "");

        return keysAndValuesMap;
    }

    @Override
    public int connectAndProcessResponse(ExternalToolQuery externalToolQuery) throws IOException
    {
        // temporary resultCount = -2 : when problem occurred during connection
        // or query
        int resultCount = -2;

        putSettingsIntoConnectorSettingsMap(externalToolQuery.getSettingsAsMap());

        BugzillaConnector bugzillaConnector = createBugzillaConnector();
        if (bugzillaConnector == null) {
            return resultCount;
        }

        resultCount = queryBugzilla(bugzillaConnector, resultCount);

        return resultCount;
    }

    private BugzillaConnector createBugzillaConnector()
    {
        BugzillaConnector bugzillaConnector = new BugzillaConnector();
        String url = getSettingsValue(URL_KEY);
        String login = getSettingsValue(USER_KEY);
        String password = getSettingsValue(PASSWORD_KEY);

        try {
            bugzillaConnector.connectTo(url);
            // empty login: when login to Bugzilla is not needed
            if (login != null && !"".equals(login)) {
                LogIn logInToBugzilla = new LogIn(login, password);
                bugzillaConnector.executeMethod(logInToBugzilla);
            }
        } catch (BugzillaException | ConnectionException ex) {
            Notification.show("Problem with Bugzilla connection: " + ex.getMessage(), Type.ERROR_MESSAGE);
            ex.printStackTrace();
            return null;
        }

        return bugzillaConnector;
    }

    private int queryBugzilla(BugzillaConnector bugzillaConnector, int resultCount) throws IOException
    {
        try {
            BugzillaVersion versionCheck = new BugzillaVersion();
            bugzillaConnector.executeMethod(versionCheck);
            String version = versionCheck.getVersion();

            BugSearch bugSearch = createSearchQuery();
            if (bugSearch != null) { // one search query should be defined from:
                                     // PRODUCT, PRIORITY, RESOLUTION, STATUS
                bugzillaConnector.executeMethod(bugSearch);
                List<? extends Bug> bugs = bugSearch.getSearchResults();
                resultCount = 0;

                for (Bug bug : bugs) 
                {
                    boolean checkSeverity = checkResult(SEVERITY_KEY, bug, version);
                    boolean checkTargetMilestone = checkResult(TARGET_MILESTONE_KEY, bug, version);
                    if (checkSeverity && checkTargetMilestone) {
                        resultCount++;
                    }
                }

                String login = getSettingsValue(USER_KEY);
                if (login != null && !"".equals(login)) {
                    bugzillaConnector.executeMethod(new LogOut());
                }
            } else {
                throw new IOException("one search query: Product, Priority, Resolution or Status should be defined.");
            }
        } catch (BugzillaException ex) {
            Notification.show("Problem with Bugzilla connection: " + ex.getMessage(), Type.ERROR_MESSAGE);
            ex.printStackTrace();
        }

        return resultCount;
    }

    private BugSearch createSearchQuery()
    {

        List<? super SearchQuery> searchList = new ArrayList<SearchQuery>();

        addSettingsValueToSearchQuery(PRODUCT_KEY, SearchLimiter.PRODUCT, searchList);
        addSettingsValueToSearchQuery(PRIORITY_KEY, SearchLimiter.PRIORITY, searchList);
        addSettingsValueToSearchQuery(RESOLUTION_KEY, SearchLimiter.RESOLUTION, searchList);
        addSettingsValueToSearchQuery(STATUS_KEY, SearchLimiter.STATUS, searchList);

        if (searchList.size() > 0) {
            SearchQuery[] searchQueries = new SearchQuery[searchList.size()];
            BugSearch bugSearch = new BugSearch(searchList.toArray(searchQueries));
            return bugSearch;
        }

        return null;
    }

    private void addSettingsValueToSearchQuery(String settingsValue, SearchLimiter limiter, List<? super SearchQuery> searchList)
    {
        String settingValue = getSettingsValue(settingsValue);
        if (settingValue != null && !"".equals(settingValue)) {
            searchList.add(new SearchQuery(limiter, settingValue));
        }
    }

    private boolean checkResult(String parameterName, Bug searchResult, String version) throws IOException
    {
        String parameterValue = getSettingsValue(parameterName);
        if (parameterValue != null && !"".equals(parameterValue)) {
            if (parameterName.equals(SEVERITY_KEY)) {
                if (parameterValue.equals(searchResult.getSeverity())) {
                    return true;
                }
            }
            if (parameterName.equals(TARGET_MILESTONE_KEY)) {
                Map<Object, Object> resultMap = searchResult.getParameterMap();

                // it depends on Bugzilla version: from version 4.0 no returns
                // "internals" hash
                if (version.startsWith("3")) {
                    Map<String, Object> internalsValue = (HashMap<String, Object>) resultMap.get("internals");
                    if (internalsValue != null) {
                        Object targetMilestone = internalsValue.get("target_milestone");

                        if (parameterValue.equals(targetMilestone)) {
                            return true;
                        }
                    } else {
                        return checkTargetMilestoneResult(parameterValue, resultMap);
                    }
                } else {
                    return checkTargetMilestoneResult(parameterValue, resultMap);
                }
            }
        } else {
            // when parameter value is empty (not defined)
            return true;
        }

        return false;
    }

    private boolean checkTargetMilestoneResult(String parameterValue, Map<Object, Object> resultMap) throws IOException
    {
        String targetMilestone = (String) resultMap.get("target_milestone");
        if (targetMilestone != null) {
            if (parameterValue.equals(targetMilestone)) {
                return true;
            }
        } else {
            throw new IOException("problem with checking target_milestone field");
        }

        return false;
    }
}
