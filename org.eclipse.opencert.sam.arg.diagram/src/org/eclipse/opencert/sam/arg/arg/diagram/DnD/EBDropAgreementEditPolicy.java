/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.DnD;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;




/**
 * Creates the models and views, based on the dropped template file
 * @author M� Carmen Palacios
 */
public class EBDropAgreementEditPolicy {

	public EBDropAgreementEditPolicy() {

	}

	public Agreement createModel(Agreement el) {
		try{
			  // crear modelo
			  Agreement newElem = ArgFactory.eINSTANCE.createAgreement();
			  newElem.eSet(ArgPackage.Literals.MODEL_ELEMENT__ID, el.getId());
			  newElem.eSet(ArgPackage.Literals.MODEL_ELEMENT__NAME, el.getName());
			  newElem.eSet(ArgPackage.Literals.ARGUMENTATION_ELEMENT__DESCRIPTION, el.getDescription());
			  newElem.eSet(ArgPackage.Literals.ARGUMENTATION_ELEMENT__CONTENT, el.getContent());
			  newElem.eSet(ArgPackage.Literals.ARGUMENTATION__LOCATION, el.getLocation());

			  //NO!!!ref Argumentation[*] between;
			  //NO!!!ref Argumentation[*] argumentation;
			  //NO!!!ref ArgumentElement[*] consistOf;
			  
			  return newElem;

		}catch(Exception e){
			  e.printStackTrace();
		}
		
		return null;
	}
	
	/*
	public CreateViewRequest createViewRequest (DiagramEditor ed, DiagramImpl dg, Agreement el, Point location) {

		try{
			final DiagramEditor editor = ed;
			final DiagramImpl diagr = dg;
			final Agreement elem = el;
			
			// crear la vista
			CreateViewRequest.ViewDescriptor viewDescriptor = new CreateViewRequest.ViewDescriptor(
						new EObjectAdapter(elem), Node.class, ((IHintedType)ArgElementTypes.Agreement_2002).getSemanticHint(), true,
						editor.getDiagramEditPart().getDiagramPreferencesHint());
	
			viewDescriptor.setPersisted(true);
			  
			CreateViewRequest createRequest = new CreateViewRequest(viewDescriptor);
			createRequest.setLocation(location);
	
			return createRequest;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	*/

}
