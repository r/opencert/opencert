/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface AssuranceAssetsPropertiesEditionPart {



	/**
	 * Init the assetsPackage
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initAssetsPackage(ReferencesTableSettings settings);

	/**
	 * Update the assetsPackage
	 * @param newValue the assetsPackage to update
	 * 
	 */
	public void updateAssetsPackage();

	/**
	 * Adds the given filter to the assetsPackage edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToAssetsPackage(ViewerFilter filter);

	/**
	 * Adds the given filter to the assetsPackage edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToAssetsPackage(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the assetsPackage table
	 * 
	 */
	public boolean isContainedInAssetsPackageTable(EObject element);




	/**
	 * Init the assetsPackageTable
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initAssetsPackageTable(ReferencesTableSettings settings);

	/**
	 * Update the assetsPackageTable
	 * @param newValue the assetsPackageTable to update
	 * 
	 */
	public void updateAssetsPackageTable();

	/**
	 * Adds the given filter to the assetsPackageTable edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToAssetsPackageTable(ViewerFilter filter);

	/**
	 * Adds the given filter to the assetsPackageTable edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToAssetsPackageTable(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the assetsPackageTable table
	 * 
	 */
	public boolean isContainedInAssetsPackageTableTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
