/*
 * <copyright>
 * Copyright (c) 2013 TECNALIA Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the term conditions of the Eclipse Public Licen v1.0
 * which accompanish this distribution, and is available at
  * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *  
 * Contributors 
 *  Alejandra Ruiz (TECNALIA): meta model design, development and implementation
 *  M� Carmen Palacios (TECNALIA): Development, implementation and maintenance
 * </copyright>
 * 
 * 
 */
/*****************************************************************************
 * Copyright (c) 2010 Atos Origin.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mathieu Velten (Atos Origin) mathieu.velten@atosorigin.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.DnD;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.CompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DiagramDragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.diagram.ui.requests.ArrangeRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.RefreshConnectionsRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.ConnectorImpl;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.gmf.runtime.notation.impl.EdgeImpl;
import org.eclipse.gmf.runtime.notation.impl.ShapeImpl;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.ArgumentReasoning;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.AssertedContext;
import org.eclipse.opencert.sam.arg.arg.AssertedEvidence;
import org.eclipse.opencert.sam.arg.arg.AssertedInference;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Choice;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.ModelElement;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorUtil;
import org.eclipse.opencert.sam.arg.arg.impl.ArgumentReasoningImpl;
import org.eclipse.opencert.sam.arg.arg.impl.AgreementImpl;
import org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl;
import org.eclipse.opencert.sam.arg.arg.impl.AssertedEvidenceImpl;
import org.eclipse.opencert.sam.arg.arg.impl.AssertedInferenceImpl;
import org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl;
import org.eclipse.opencert.sam.arg.arg.impl.ChoiceImpl;
import org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementCitationImpl;
import org.eclipse.opencert.sam.arg.arg.impl.ClaimImpl;
import org.eclipse.opencert.sam.arg.arg.impl.InformationElementCitationImpl;
import org.eclipse.opencert.sam.arg.ui.util.UtilConstants;



import org.eclipse.papyrus.diagram.common.commands.UpdateDiagramCommand;
import org.eclipse.papyrus.diagram.common.commands.SemanticAdapter;
import org.eclipse.papyrus.diagram.common.commands.CommonDeferredCreateConnectionViewCommand;
import org.eclipse.papyrus.diagram.common.util.DiagramEditPartsUtil;
import org.eclipse.papyrus.diagram.common.wrappers.CommandProxyWithResult;

/**
 * @generated NOT
 */
/**
 * 
 * @author M� Carmen Palacios
 */
// Start MCP
//public class ExpandBarDropEditPolicy extends DragDropEditPolicy {
public class ExpandBarDropEditPolicy extends DiagramDragDropEditPolicy {
	// MCP: OJO la key no puede ser el objeto porque en las relaciones hace copias
	private Map<String,EObject> mpIDs = new HashMap<String, EObject>();
	private Map<String,IAdaptable> editPartAdaptableSet = new HashMap<String, IAdaptable>(); 
	//private DiagramImpl diagramImpl = (DiagramImpl)getHost().getModel();	
	//private DiagramImpl diagramImpl;

	private List<EObject> choiceObjects = new ArrayList();
	
	private List<EObject> nodes = new ArrayList();
	
	
	public ExpandBarDropEditPolicy(EditPart hostep) {
		super();
		setHost(hostep);
	}
	

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gmf.runtime.diagram.ui.editpolicies.DiagramDragDropEditPolicy
	 * #getDropObjectsCommand (org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest)
	 */
	@Override
	public Command getDropObjectsCommand(DropObjectsRequest dropRequest) {
		String modulesDir = Platform.getPreferencesService().
				  getString("org.eclipse.opencert.sam.preferences", UtilConstants.Module_PATH, "Modules Dir wrong", null);
		Command res=null;
		Iterator elements = dropRequest.getObjects().iterator();
		while (elements.hasNext()) {
			Object obj = elements.next();
			
			if(obj.toString().compareTo("")==0) continue;
					
			if(obj.toString().contains(modulesDir))
				res = getDropModuleObjectsCommand(dropRequest);				
			else
				res = getDropPatternObjectsCommand(dropRequest);
		}
		
		return res;
	}
	
	public List<EObject> getChoiceNodesInDrop()
	{
	   return choiceObjects;
	}
	
	public List<EObject> getNodesInDrop() 
	{
		return nodes;
	}


	public Command getDropModuleObjectsCommand(DropObjectsRequest dropRequest) {
		String nameDiagramFile ="";
	    List<EObject> nodeObjects = new ArrayList<EObject>();		

		Iterator elements = dropRequest.getObjects().iterator();
		while (elements.hasNext()) {
			Object obj = elements.next();
			
			if(obj.toString().compareTo("")==0) continue;
			
			nameDiagramFile = obj.toString();
			File f = new File(obj.toString());
			String name = f.getName().substring(0, f.getName().lastIndexOf("."));

		    // crear modelo
		    Argumentation newElem = ArgFactory.eINSTANCE.createArgumentation();
			newElem.eSet(ArgPackage.Literals.MODEL_ELEMENT__ID, name);
			/**
			 * aqui mete el location pero quitamos la parte de la direcci�n de preferencias de modulo
			 * As� aseguramos que al copiar en otro ordenador y definir las preferencias en otro lado, todo vaya
			 */
			String modulesDir = Platform.getPreferencesService().
					  getString("org.eclipse.opencert.sam.preferences", UtilConstants.Module_PATH, "Modules Dir wrong", null);
			String location = obj.toString().replace(modulesDir, "");
		    newElem.eSet(ArgPackage.Literals.ARGUMENTATION__LOCATION, location);
		    // aqui a�ade el elemento al modelo
		    nodeObjects.add(newElem);
		}
		
			// build commands that add references to the diagram.
			Command command = buildAddEObjectsReferencesCommand(nodeObjects);
			// if no nodes or edges are to be added, there is nothing to do.
			if(command == null) {
				return null;
			}
			
			//MCP  build the create views commands (alone Node objects)
			List<ViewDescriptor> viewDescriptors = createViewDescriptors(nodeObjects);
			Command viewsCommand = createViewsAndArrangeCommand2(dropRequest.getLocation(), viewDescriptors);
			if(viewsCommand != null && viewsCommand.canExecute()) {
				command = command.chain(viewsCommand);
			}
			
			// MCP falta arrange. Parece que no funciona y puede ser porque he eliminado 
			// dropRequest.setResult(result); en createViewsAndArrangeCommand2 
			
			/*
			EBDropArgumentationLocationEditPolicy agp = new EBDropArgumentationLocationEditPolicy();
			Command cmd = agp.getOpenCommand(view, nameDiagramFile);
			if(cmd != null && cmd.canExecute()) {
				command = command.chain(cmd);
			}
			*/
			
			
			// update diagram.
			command = command.chain(new UpdateDiagramCommand(getGraphicalHost()));
			// return command
			return command;
	}
	
	public Command getDropPatternObjectsCommand(DropObjectsRequest dropRequest) {		
		// get nodes and edges to add to this Diagram.
		List<EObject> nodeObjects = findNodesInDrop(dropRequest);
		//MCP
        /*
		List<ViewDescriptor> viewDescriptors = createViewDescriptors(nodeObjects);
        */
		List<EObject> edgeObjects2 = findEdgesInDrop(dropRequest);
		// build commands that add references to the diagram.
		Command command = buildAddEObjectsReferencesCommand(nodeObjects);
		Command edgesCommand = buildAddEObjectsReferencesCommand(edgeObjects2);
		command = command == null ? edgesCommand : command.chain(edgesCommand);
		// if no nodes or edges are to be added, there is nothing to do.
		if(command == null) {
			return null;
		}
		// Start MCP
		/* Dawn cdo */
		// Eliminar relaciones con elementos Choice una vez copiadas al modelo
		List<EObject> edgeObjects = new ArrayList<EObject>();
		for(EObject element : edgeObjects2)
		{
			EObject obj = getSource(element);
			if(false == obj instanceof Choice) { 
				edgeObjects.add(element); 
			}
		}
		// End MCP		
		
		// build the create views commands.
		//MCP
        /*
		Command viewsCommand = createViewsAndArrangeCommand(dropRequest, viewDescriptors);
		if(viewsCommand != null && viewsCommand.canExecute()) {
			command = command.chain(viewsCommand);
		}
        */
		//MCP build the views of nodes with links and create adapters for edges
		Point location = new Point(dropRequest.getLocation());
		CompositeCommand viewsEdgeCommand = createConnectionViewCommand(location, edgeObjects);
		if(viewsEdgeCommand != null && viewsEdgeCommand.canExecute()) {
			command = command.chain(new ICommandProxy(viewsEdgeCommand));
		}
		
		//MCP  build the create views commands (alone Node objects)
		List<EObject> aloneNodeObjects =  findAloneNodesInDrop(nodeObjects, edgeObjects);
		List<ViewDescriptor> viewDescriptors = createViewDescriptors(aloneNodeObjects);
		Command viewsCommand = createViewsAndArrangeCommand2(dropRequest.getLocation(), viewDescriptors);
		if(viewsCommand != null && viewsCommand.canExecute()) {
			command = command.chain(viewsCommand);
		}
		
		// MCP falta arrange. Parece que no funciona y puede ser porque he eliminado 
		// dropRequest.setResult(result); en createViewsAndArrangeCommand2 
		
		// update diagram.
		command = command.chain(new UpdateDiagramCommand(getGraphicalHost()));
		
		// obtener Choices creados en este comando
		choiceObjects = findChoiceNodesInDrop(nodeObjects);
		
		// obtener Nodes creados en este comando
		nodes = nodeObjects;
		
		// return command
		return command;
	}
	
	private List<EObject> findChoiceNodesInDrop(List<EObject> nodeObjects)
	{
		List<EObject> res = new ArrayList();
		
		for(EObject element : nodeObjects)
		{
    	    if(true == element instanceof ClaimImpl)
    	    {
    	    	ClaimImpl obj = (ClaimImpl)element;
    	    	if(obj.getChoice() != null)
    	    		res.add(element);
    	    }
    	    else if(true == element instanceof ChoiceImpl) // no entra aqui
    	    {
    	    	res.add(element);
    	    }

		}
		return res;
	}

	/**
	 * Find nodes in drop.
	 * 
	 * @param request
	 *        the request
	 * 
	 * @return the list< e object>
	 */
	protected List<EObject> findNodesInDrop(DropObjectsRequest request) {
		List<EObject> nodes = new ArrayList<EObject>();

		// create nodes
		Iterator elements = request.getObjects().iterator();
		while (elements.hasNext()) {
			Object obj = elements.next();


			try{
				if(obj.toString().compareTo("")==0) continue;
					
				final File f = new File(obj.toString());


				//final DiagramEditor editor = (DiagramEditor) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
				//final DiagramImpl diagr = (DiagramImpl)getHost().getModel();


				DropLoadModelFromFile lmod = new DropLoadModelFromFile();
				EList<EObject> dropO = lmod.load(f.getPath());
			    EObject obj2 = dropO.get(0);
			    if(true == obj2 instanceof Diagram)
			    {
					Diagram dia = (Diagram)obj2;
					//OJO tb. existe getChildren()
					for (Object obj21 :  dia.getPersistedChildren()) 
					{
						System.out.println("ExpandBarDropEditPolicy.findNodesInDrop(): "+obj21.getClass().getCanonicalName());
						CreateViewRequest.ViewDescriptor viewDescriptor = null; 
						ShapeImpl	obj3 = (ShapeImpl)obj21;

						Point location = new Point(request.getLocation());
						if(true == obj3.getElement() instanceof AgreementImpl) // MCP: OJO debe ir antes de ArgumentationImpl porque es una superclase
						{
							AgreementImpl obj4 = (AgreementImpl)obj3.getElement();
							EBDropAgreementEditPolicy el = new EBDropAgreementEditPolicy();
							final Agreement newElem = el.createModel(obj4);
							mpIDs.put(((XMLResource) obj4.eResource()).getID(obj4), newElem);
							nodes.add(newElem);
						}
						else if(true == obj3.getElement() instanceof ArgumentationImpl)
						{
							ArgumentationImpl obj4 = (ArgumentationImpl)obj3.getElement();
							EBDropArgumentationEditPolicy el = new EBDropArgumentationEditPolicy();
							final Argumentation newElem = el.createModel(obj4);
							mpIDs.put(((XMLResource) obj4.eResource()).getID(obj4), newElem);
							nodes.add(newElem);
						}
						else if(true == obj3.getElement() instanceof InformationElementCitationImpl)
						{
							InformationElementCitationImpl obj4 = (InformationElementCitationImpl)obj3.getElement();
							EBDropInformationElementCitationEditPolicy el = new EBDropInformationElementCitationEditPolicy();
							final InformationElementCitation newElem = el.createModel(obj4);
							mpIDs.put(((XMLResource) obj4.eResource()).getID(obj4), newElem);
							nodes.add(newElem);
						}
						else if(true == obj3.getElement() instanceof ArgumentElementCitationImpl)
						{
							ArgumentElementCitationImpl obj4 = (ArgumentElementCitationImpl)obj3.getElement();
							EBDropArgumentElementCitationEditPolicy el = new EBDropArgumentElementCitationEditPolicy();
							final ArgumentElementCitation newElem = el.createModel(obj4);
							mpIDs.put(((XMLResource) obj4.eResource()).getID(obj4), newElem);
							nodes.add(newElem);
						}
						else if(true == obj3.getElement() instanceof ArgumentReasoningImpl)
						{
							ArgumentReasoningImpl obj4 = (ArgumentReasoningImpl)obj3.getElement();
							EBDropArgumentReasoningEditPolicy el = new EBDropArgumentReasoningEditPolicy();
							final ArgumentReasoning newElem = el.createModel(obj4);
							mpIDs.put(((XMLResource) obj4.eResource()).getID(obj4), newElem);
							nodes.add(newElem);
						}
						else if(true == obj3.getElement() instanceof ClaimImpl)
						{
							ClaimImpl obj4 = (ClaimImpl)obj3.getElement();
							EBDropClaimEditPolicy el = new EBDropClaimEditPolicy();
							final Claim newElem = el.createModel(obj4);
							mpIDs.put(((XMLResource) obj4.eResource()).getID(obj4), newElem);
							nodes.add(newElem);
							
							if(obj4.getChoice() != null)
							{
								Choice obj5 = obj4.getChoice();
								EBDropChoice2EditPolicy el2 = new EBDropChoice2EditPolicy();
								final Choice newElem2 = el2.createModel(obj5);
								mpIDs.put(((XMLResource) obj5.eResource()).getID(obj5), newElem2);
								//nodes.add(newElem2);???
							}

						}
						else
						{
							System.out.println("ExpandBarDropEditPolicy.findNodesInDrop(): Error unkonwn Node type");
						}
			          }//for (Object obj21 :  dia.getPersistedChildren())
					
					
					// reassign relationships of nodes
					for (Object obj21 :  dia.getPersistedChildren()) 
					{
						System.out.println("ExpandBarDropEditPolicy.findNodesInDrop(): "+obj21.getClass().getCanonicalName());
						ShapeImpl	obj3 = (ShapeImpl)obj21;
						if(true == obj3.getElement() instanceof Agreement) // MCP: OJO debe ir antes de ArgumentationImpl porque es una superclase
						{
							Agreement elem = (Agreement)obj3.getElement();
							Agreement newElem = (Agreement)mpIDs.get(((XMLResource) elem.eResource()).getID(elem));
							
							  //NO!!!ref Argumentation[*] between;
							  //NO!!!ref Argumentation[*] argumentation;
							  //NO!!!ref ArgumentElement[*] consistOf;
							if(mpIDs.get(elem.getBetween()) != null)
								newElem.eSet(ArgPackage.Literals.AGREEMENT__BETWEEN, mpIDs.get(elem.getBetween()));
							if(mpIDs.get(elem.getArgumentation()) != null)
								newElem.eSet(ArgPackage.Literals.ARGUMENTATION__ARGUMENTATION, mpIDs.get(elem.getArgumentation()));
							if(mpIDs.get(elem.getConsistOf()) != null)
								newElem.eSet(ArgPackage.Literals.ARGUMENTATION__CONSIST_OF, mpIDs.get(elem.getConsistOf()));
						}
						else if(true == obj3.getElement() instanceof ArgumentationImpl)
						{
							Argumentation elem = (Argumentation)obj3.getElement();
							Argumentation newElem = (Argumentation)mpIDs.get(((XMLResource) elem.eResource()).getID(elem));

							  //NO!!!ref Argumentation[*] argumentation;
							  //NO!!!ref ArgumentElement[*] consistOf;
							if(mpIDs.get(elem.getArgumentation()) != null)
								newElem.eSet(ArgPackage.Literals.ARGUMENTATION__ARGUMENTATION, mpIDs.get(elem.getArgumentation()));
							if(mpIDs.get(elem.getConsistOf()) != null)
								newElem.eSet(ArgPackage.Literals.ARGUMENTATION__CONSIST_OF, mpIDs.get(elem.getConsistOf()));
						}
						else if(true == obj3.getElement() instanceof InformationElementCitationImpl)
						{
							InformationElementCitation elem = (InformationElementCitation)obj3.getElement();
							InformationElementCitation newElem = (InformationElementCitation)mpIDs.get(((XMLResource) elem.eResource()).getID(elem));
							
							//NO!!!ref evidence.Artefact[*] artefact;
							//MCP???No duplicamos el ARTEFACT
							if(mpIDs.get(elem.getArtefact()) != null)
								newElem.eSet(ArgPackage.Literals.INFORMATION_ELEMENT_CITATION__ARTEFACT, elem.getArtefact());
						}
						else if(true == obj3.getElement() instanceof ArgumentElementCitation)
						{
							ArgumentElementCitation elem = (ArgumentElementCitation)obj3.getElement();
							ArgumentElementCitation newElem = (ArgumentElementCitation)mpIDs.get(((XMLResource) elem.eResource()).getID(elem));
							
							//NO!!!ref String argumentationReference;
							if(mpIDs.get(elem.getArgumentationReference()) != null)
								newElem.eSet(ArgPackage.Literals.ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE, mpIDs.get(elem.getArgumentationReference()));
						}
						else if(true == obj3.getElement() instanceof ArgumentReasoningImpl)
						{
							ArgumentReasoning elem = (ArgumentReasoning)obj3.getElement();
							ArgumentReasoning newElem = (ArgumentReasoning)mpIDs.get(((XMLResource) elem.eResource()).getID(elem));
							
							  //NO!!!ref Argumentation[*] hasStructure;
							if(mpIDs.get(elem.getHasStructure()) != null)
								newElem.eSet(ArgPackage.Literals.ARGUMENT_REASONING__HAS_STRUCTURE, mpIDs.get(elem.getHasStructure()));
						}
						else if(true == obj3.getElement() instanceof ClaimImpl)
						{
							Claim elem = (Claim)obj3.getElement();
							Claim newElem = (Claim)mpIDs.get(((XMLResource) elem.eResource()).getID(elem));
							
							//NO!!!val Choice[0..1] choice;
							if(elem.getChoice() != null && mpIDs.get(((XMLResource) elem.getChoice().eResource()).getID(elem.getChoice())) != null) 
							   newElem.eSet(ArgPackage.Literals.CLAIM__CHOICE, mpIDs.get(((XMLResource) elem.getChoice().eResource()).getID(elem.getChoice())));
						}
						else
						{
							System.out.println("ExpandBarDropEditPolicy.findNodesInDrop(): Error unkonwn Node type");
						}
			          }//for (Object obj21 :  dia.getPersistedChildren())
			    }//if(true == obj2 instanceof Diagram)
			}catch(Exception e){
				e.printStackTrace();
			}
		}//while (elements.hasNext()) 
		
		
		return nodes;
	}

	/**
	 * Find edges in drop.
	 * 
	 * @param request
	 *        the request
	 * 
	 * @return the list< e object>
	 */
	protected List<EObject> findEdgesInDrop(DropObjectsRequest request) {
		List<EObject> edges = new ArrayList<EObject>();
		Iterator elements = request.getObjects().iterator();

		while (elements.hasNext()) {
			Object obj = elements.next();


			try{
				if(obj.toString().compareTo("")==0) continue;
					
				final File f = new File(obj.toString());


				final DiagramEditor editor = (DiagramEditor) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
				final DiagramImpl diagr = (DiagramImpl)getHost().getModel();


				DropLoadModelFromFile lmod = new DropLoadModelFromFile();
				EList<EObject> dropO = lmod.load(f.getPath());
			    EObject obj2 = dropO.get(0);
			    if(true == obj2 instanceof Diagram)
			    {
					Diagram dia = (Diagram)obj2;
					//OJO tb. existe getChildren()
					
					System.out.println("ExpandBarDropEditPolicy.findEdgesInDrop(): mpIDs = " + mpIDs.toString());
					  for (Object obj22 :  dia.getPersistedEdges()) 
					  {
							System.out.println("ExpandBarDropEditPolicy.findEdgesInDrop(): "+obj22.getClass().getCanonicalName());
							CreateViewRequest.ViewDescriptor viewDescriptor = null; 
							//ConnectorImpl obj3 = (ConnectorImpl)obj22;
							EdgeImpl obj3 = (EdgeImpl)obj22;

							
							//String idsource = ((ModelElement)obj3.getSource().getElement()).getId();
							//String idtarget = ((ModelElement)obj3.getTarget().getElement()).getId();
							String idsource = ((XMLResource) obj3.getSource().getElement().eResource()).getID(obj3.getSource().getElement());
							String idtarget = ((XMLResource) obj3.getTarget().getElement().eResource()).getID(obj3.getTarget().getElement());
							EObject source = mpIDs.get(idsource);
							EObject target = mpIDs.get(idtarget);
							
							TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();									
							//String idsource = ((AssertedRelationship)obj3.getSource().getElement()).getId();
							//String idtarget = ((ModelElement)obj3.getTarget().getElement()).getId();
							Point location = new Point(request.getLocation());							
							if(true == obj3.getElement() instanceof AssertedInferenceImpl)
							{
								EBDropAssertedInferenceEditPolicy nw = new EBDropAssertedInferenceEditPolicy(editingDomain, source, target);
								final AssertedInference newElem = nw.createModel(obj3);

								edges.add(newElem);
							}
							else if(true == obj3.getElement() instanceof AssertedEvidenceImpl)
							{
								EBDropAssertedEvidenceEditPolicy nw = new EBDropAssertedEvidenceEditPolicy(editingDomain, source, target);
								final AssertedEvidence newElem = nw.createModel(obj3);
								  //NO!!!ref EvidenceEvaluation[0..*] evaluation;
								  //NO!!!val EvidenceImpact[0..*] owned;
					
								edges.add(newElem);
							}
							else if(true == obj3.getElement() instanceof AssertedContextImpl)
							{
								EBDropAssertedContextEditPolicy nw = new EBDropAssertedContextEditPolicy(editingDomain, source, target);
								final AssertedContext newElem = nw.createModel(obj3);

								edges.add(newElem);
							}
							else
							{
								System.out.println("ExpandBarDropEditPolicy.findEdgesInDrop(): Error unkonwn Edge type");
							}
							
					  }//for (Object obj22 :  dia.getPersistedEdges()) 

				    }//if(true == obj2 instanceof Diagram)
				}catch(Exception e){
					e.printStackTrace();
				}

			}//while (elements.hasNext()) {
					
		return edges;
	}

	/**
	 * Creates the view descriptors.
	 * 
	 * @param elements
	 *        the elements
	 * 
	 * @return the list< view descriptor>
	 */
	protected List<ViewDescriptor> createViewDescriptors(List<EObject> elements) {
		List<ViewDescriptor> viewDescriptors = new ArrayList<ViewDescriptor>();
		final DiagramEditor editor = (DiagramEditor) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		for(EObject element : elements) {
			
			// crear la vista
			CreateViewRequest.ViewDescriptor viewDescriptor = new CreateViewRequest.ViewDescriptor(
						new EObjectAdapter(element), Node.class, EBDropUtil.getEObjectSemanticHint(element), true,
						editor.getDiagramEditPart().getDiagramPreferencesHint());
			//viewDescriptor.setPersisted(true);
			
			viewDescriptors.add(viewDescriptor);
		}
		return viewDescriptors;

	}

	protected CompositeCommand createConnectionViewCommand(Point absoluteLocation, List<EObject> elements) {
		final DiagramEditor editor = (DiagramEditor) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		CompositeCommand cc = new CompositeCommand("Drop");
		
		Map<String,EObject> subListNodes = new HashMap<String, EObject>();
		List<EObject> multiLinks = new ArrayList<EObject>();
		for(EObject element : elements) {
			if(isMultiLink(element, elements))
			{
				multiLinks.add(element);
				String identSource = (String) getSource(element).eGet(ArgPackage.Literals.MODEL_ELEMENT__ID);
				subListNodes.put(identSource, getSource(element));
				String identTarget = (String) getTarget(element).eGet(ArgPackage.Literals.MODEL_ELEMENT__ID);				
				subListNodes.put(identTarget, getTarget(element));
			}
			else
			{
				EObject source = getSource(element);
				EObject target = getTarget(element);

				cc = dropBinaryLink(cc, source, target, absoluteLocation, element);				
			}
		}
		
		cc = dropMultiLinks(cc, subListNodes, multiLinks, absoluteLocation);
 
		return cc;
	}

	

	/**
	 * Builds the add e objects references command.
	 * 
	 * @param elements
	 *        the elements
	 * 
	 * @return the command
	 */
	protected Command buildAddEObjectsReferencesCommand(List<EObject> elements) {
		if(elements != null && elements.size() > 0) {
			TransactionalEditingDomain domain = getGraphicalHost().getEditingDomain();
			View view = getGraphicalHost().getNotationView();
			Diagram diagram = view instanceof Diagram ? (Diagram)view : null;
			if(diagram != null) {
				DiagramImpl diagramImpl = (DiagramImpl)getHost().getModel();
				return new ICommandProxy(new AddEObjectReferencesToDiagram(domain, diagram, diagramImpl, elements));
			}
		}
		return null;
	}

	
	
	/**
	 * Gets the graphical host.
	 * 
	 * @return the graphical host
	 */
	public IGraphicalEditPart getGraphicalHost() {
		if(getHost() instanceof IGraphicalEditPart) {
			return (IGraphicalEditPart)getHost();
		}
		return null;
	}


	
	/**
	 * Builds the add e objects references command.
	 * 
	 * @param elements
	 *        the elements
	 * 
	 * @return the command
	 */
	protected Command buildAddEObjectsReferencesCommand(TransactionalEditingDomain domain, Diagram diagram, List<EObject> elements) {
		DiagramImpl diagramImpl = (DiagramImpl)getHost().getModel();
		return new ICommandProxy(new AddEObjectReferencesToDiagram(domain, diagram, diagramImpl, elements));
	}

	
	/* MCP: from here adapted fom papyrus */
	/**
	 * the method provides command to create the binary link into the diagram. If the source and the
	 * target views do not exist, these views will be created.
	 * 
	 * @param cc
	 *        the composite command that will contain the set of command to create the binary
	 *        link
	 * @param source
	 *        the source the element source of the link
	 * @param target
	 *        the target the element target of the link
	 * @param linkVISUALID
	 *        the link VISUALID used to create the view
	 * @param location
	 *        the location the location where the view will be be created
	 * @param semanticLink
	 *        the semantic link that will be attached to the view
	 * 
	 * @return the composite command
	 */
	public CompositeCommand dropBinaryLink(CompositeCommand cc, EObject source, EObject target, Point absoluteLocation, EObject semanticLink) {
		// MCP
		// look for editpart
		GraphicalEditPart sourceEditPart = (GraphicalEditPart)lookForEditPart(source);
		GraphicalEditPart targetEditPart = (GraphicalEditPart)lookForEditPart(target);

		// descriptor of the link
		CreateConnectionViewRequest.ConnectionViewDescriptor linkdescriptor = new CreateConnectionViewRequest.ConnectionViewDescriptor(EBDropUtil.getElementType(semanticLink), ((IHintedType)EBDropUtil.getElementType(semanticLink)).getSemanticHint(), getDiagramPreferencesHint());

		//MCP
		IAdaptable sourceAdapter = null;
		IAdaptable targetAdapter = null;
		//IAdaptable sourceAdapter = lookForIAdaptable(source);
		//IAdaptable targetAdapter = lookForIAdaptable(target);
		
		//MCP
		if(sourceEditPart == null) {
		  if(sourceAdapter == null) {
			ICommand createCommand = getDefaultDropNodeCommand(getLinkSourceDropLocation(absoluteLocation,source, target), source);
			cc.add(createCommand);

			sourceAdapter = (IAdaptable)createCommand.getCommandResult().getReturnValue();
			String ident = (String) source.eGet(ArgPackage.Literals.MODEL_ELEMENT__ID);
			editPartAdaptableSet.put(ident, sourceAdapter);
		  }
		} 
		else {
			sourceAdapter = new SemanticAdapter(null, sourceEditPart.getModel());
		}
		//MCP
		if(targetEditPart == null) {
		  if(targetAdapter == null) {		
			ICommand createCommand = getDefaultDropNodeCommand(getLinkTargetDropLocation(absoluteLocation,source, target), target);
			cc.add(createCommand);

			targetAdapter = (IAdaptable)createCommand.getCommandResult().getReturnValue();
			String ident = (String) target.eGet(ArgPackage.Literals.MODEL_ELEMENT__ID);
			editPartAdaptableSet.put(ident, targetAdapter);
		  }
		} 
		else {
			targetAdapter = new SemanticAdapter(null, targetEditPart.getModel());
		}

		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();
		CommonDeferredCreateConnectionViewCommand aLinkCommand = new CommonDeferredCreateConnectionViewCommand(editingDomain, ((IHintedType)EBDropUtil.getElementType(semanticLink)).getSemanticHint(), sourceAdapter, targetAdapter, ((IGraphicalEditPart) getHost()).getViewer(), getDiagramPreferencesHint(), linkdescriptor, null);
		aLinkCommand.setElement(semanticLink);
		cc.compose(aLinkCommand);		
		
		return cc;
	}

	public CompositeCommand dropMultiLinks(CompositeCommand cc, Map<String,EObject> listNodes, List<EObject> listEdges, Point absoluteLocation) {
		// MCP
		Map<String,IAdaptable> listNodeAdapters = new HashMap<String, IAdaptable>();		
		Iterator<EObject> iterator = listNodes.values().iterator();
		while(iterator.hasNext())
		{
			EObject element =  iterator.next();
			String ident = (String) element.eGet(ArgPackage.Literals.MODEL_ELEMENT__ID);
			
			// look for editpart
			GraphicalEditPart elementEditPart = (GraphicalEditPart)lookForEditPart(element);
	
			IAdaptable elementAdapter = null;
			if(elementEditPart == null) {
				ICommand createCommand = getDefaultDropNodeCommand(absoluteLocation, element);
				cc.add(createCommand);
	
				elementAdapter = (IAdaptable)createCommand.getCommandResult().getReturnValue();

			} 
			else {
				elementAdapter = new SemanticAdapter(null, elementEditPart.getModel());
			}
			
			listNodeAdapters.put(ident, elementAdapter);
		}

		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();
		for(int i=0;i<listEdges.size();i++)
		{
			EObject semanticLink = listEdges.get(i);
			String identSource = (String) getSource(semanticLink).eGet(ArgPackage.Literals.MODEL_ELEMENT__ID);
			IAdaptable sourceAdapter = listNodeAdapters.get(identSource);
			String identTarget = (String) getTarget(semanticLink).eGet(ArgPackage.Literals.MODEL_ELEMENT__ID);
			IAdaptable targetAdapter = listNodeAdapters.get(identTarget);			

			// descriptor of the link
			CreateConnectionViewRequest.ConnectionViewDescriptor linkdescriptor = new CreateConnectionViewRequest.ConnectionViewDescriptor(EBDropUtil.getElementType(semanticLink), ((IHintedType)EBDropUtil.getElementType(semanticLink)).getSemanticHint(), getDiagramPreferencesHint());
			CommonDeferredCreateConnectionViewCommand aLinkCommand = new CommonDeferredCreateConnectionViewCommand(editingDomain, ((IHintedType)EBDropUtil.getElementType(semanticLink)).getSemanticHint(), sourceAdapter, targetAdapter, ((IGraphicalEditPart) getHost()).getViewer(), getDiagramPreferencesHint(), linkdescriptor, null);
			aLinkCommand.setElement(semanticLink);
			cc.compose(aLinkCommand);
		}
		
		return cc;
	}

	
	/**
	 * Look for editPart from its semantic.
	 * 
	 * @param semantic
	 *        the semantic
	 * 
	 * @return the edits the part or null if not found
	 */
	protected EditPart lookForEditPart(EObject semantic) {
		Collection<EditPart> editPartSet = ((IGraphicalEditPart) getHost()).getViewer().getEditPartRegistry().values();
		Iterator<EditPart> editPartIterator = editPartSet.iterator();
		EditPart existedEditPart = null;
		while(editPartIterator.hasNext() && existedEditPart == null) {
			EditPart currentEditPart = editPartIterator.next();

			if(isEditPartTypeAdapted(currentEditPart.getClass(), semantic.eClass()) && semantic.equals(((GraphicalEditPart)currentEditPart).resolveSemanticElement())) {
				existedEditPart = currentEditPart;
			}
		}
		return existedEditPart;
	}
	/**
	 * This method returns the default drop command for node. It create the view at the specified location,
	 * using the gmf command framework so the policies are used.
	 * 
	 * @param location
	 *        the drop location
	 * @param droppedObject
	 *        the object to drop
	 * @return the creation node command
	 */
	protected ICommand getDefaultDropNodeCommand(Point absoluteLocation, EObject droppedObject) {
		return getDefaultDropNodeCommand((IGraphicalEditPart) getHost(), null, absoluteLocation, droppedObject);
	}

	/**
	 * This method allows to specify a location for the creation of a node at the source of a dropped link.
	 * Overriding implementations must not modify the absoluteLocation parameter (use {@link Point#getCopy()})
	 * 
	 * @param absoluteLocation the request's drop location
	 * @param source the source of the dropped link
	 * @param target the target of the dropped link
	 * @return the new location for the node
	 */
	protected Point getLinkSourceDropLocation(Point absoluteLocation, EObject source, EObject target) {
		return absoluteLocation;
	}

	/**
	 * This method allows to specify a location for the creation of a node at the target of a dropped link.
	 * Overriding implementations must not modify the absoluteLocation parameter (use {@link Point#getCopy()})
	 * 
	 * @param absoluteLocation the request's drop location
	 * @param source the source of the dropped link
	 * @param target the target of the dropped link
	 * @return the new location for the node
	 */
	protected Point getLinkTargetDropLocation(Point absoluteLocation, EObject source, EObject target) {
		if (lookForEditPart(source) == null && lookForEditPart(target) == null) {
			return absoluteLocation.getTranslated(100, 0);
		}
		return absoluteLocation;
	}

	/**
	 * Gets the diagram preferences hint.
	 * 
	 * @return the diagram preferences hint
	 */
	protected PreferencesHint getDiagramPreferencesHint() {
		return ((IGraphicalEditPart) getHost()).getDiagramPreferencesHint();
	}
	
	/**
	 * This method returns the default drop command for node. It create the view at the specified location,
	 * using the gmf command framework so the policies are used.
	 * 
	 * @param hostEP
	 *        The host edit part which will be the parent of the new node
	 * @param semanticHint
	 *        the semantic hint of the view to create
	 * @param location
	 *        the drop location
	 * @param droppedObject
	 *        the object to drop
	 * @return the creation node command
	 */
	protected ICommand getDefaultDropNodeCommand(EditPart hostEP, String semanticHint, Point absoluteLocation, EObject droppedObject) {
		List<View> existingViews = DiagramEditPartsUtil.findViews(droppedObject, ((IGraphicalEditPart) getHost()).getViewer());

		// only allow one view instance of a single element by diagram
		if(existingViews.isEmpty()) {
			IAdaptable elementAdapter = new EObjectAdapter(droppedObject);

			ViewDescriptor descriptor = new ViewDescriptor(elementAdapter, Node.class, semanticHint, ViewUtil.APPEND, false, getDiagramPreferencesHint());
			//MCP DUDA????
			//descriptor.setPersisted(true);
			CreateViewRequest createViewRequest = new CreateViewRequest(descriptor);
			createViewRequest.setLocation(absoluteLocation);

			// "ask" the host for a command associated with the CreateViewRequest
			Command command = hostEP.getCommand(createViewRequest);
			// set the viewdescriptor as result
			// it then can be used as an adaptable to retrieve the View
			return (ICommand) new CommandProxyWithResult(command, descriptor);
		}

		return org.eclipse.gmf.runtime.common.core.command.UnexecutableCommand.INSTANCE;
	}

	/**
	 * Check if the edit part type is the best one to represent an object of the given EClass type
	 * 
	 * @param editPartClass
	 *        the type of EditPart which may represent a semantic element
	 * @param eClass
	 *        the EClass type of the represented semantic element
	 * @return true if an edit part of this type should be selected
	 */
	private boolean isEditPartTypeAdapted(Class<? extends EditPart> editPartClass, EClass eClass) {
		if(DiagramEditPart.class.isAssignableFrom(editPartClass) || CompartmentEditPart.class.isAssignableFrom(editPartClass)) {
			// the edit part is disqualified, as a compartment or a diagram can not be dropped
			return false;
		} else if(GraphicalEditPart.class.isAssignableFrom(editPartClass)) {
			// check the edit part type against advised ones
			return isEditPartTypeSuitableForEClass(editPartClass.asSubclass(GraphicalEditPart.class), eClass);
		} else {
			// only a GraphicalEditPart must be selected
			return false;
		}
	}

	/**
	 * Check if an edit part type correctly represent a semantic element of the given EClass.
	 * Subclasses should implement this method to restrict the possibilities during drop of a link.
	 * If an edit part is not of a suitable type, returning false will eliminate it to represent the
	 * element as a source or target edit part. This can be used for example to disable label edit
	 * parts, which may represent the same model element as the main node.
	 * 
	 * @param editPartClass
	 *        the type of EditPart which must be checked
	 * @param eClass
	 *        the EClass type of the element which the edit part must represent
	 * @return the only edit part type which can be selected (return a common super type if several
	 *         edit parts can be chosen)
	 */
	protected boolean isEditPartTypeSuitableForEClass(Class<? extends GraphicalEditPart> editPartClass, EClass eClass) {
		return true;
	}


    protected Boolean isMultiLink(EObject element, List<EObject> elements)
    {
    	Boolean res=false;
		EObject source = getSource(element);
		EObject target = getTarget(element);
    	int cont = 0;
    	for(int i=0;i<elements.size(); i++)
    	{
    		if(elements.get(i).equals(element)) continue;
    		
    		EObject sourceElem = getSource(elements.get(i));
    		EObject targetElem = getTarget(elements.get(i));
    	    if(sourceElem.equals(source) || targetElem.equals(source) ||
    	       sourceElem.equals(target) || targetElem.equals(target) )
    	    {
    	    	cont++;
    	    }
    	}
    	if(cont>0) res = true;
    	
    	return res;
    }
    
    
    protected List<EObject> findAloneNodesInDrop(List<EObject> nodeObjects, List<EObject> edgeObjects)
    {
    		List<EObject> res = new ArrayList<EObject>();

    		for(EObject nodo : nodeObjects) {
    			boolean alone = true;
    			for(EObject edge : edgeObjects)
    			{
    	    		EObject sourceElem = getSource(edge);
    	    		EObject targetElem = getTarget(edge);    	    		
    	    	    if(sourceElem.equals(nodo) || targetElem.equals(nodo))
    	    	    {
    	    	    	alone = false;
    	    	    	break;
    	    	    }
    			}
    			
    			// si no est� relacionado con ning�n link.... 
    			if(alone) res.add(nodo);
    		}  
    		
    		return res;
    }

	/**
	 * createViewsAndArrangeCommand
	 * Method to create all the view based on the viewDescriptors list and provide
	 * a default arrangement of them.
	 * 
	 * @param dropRequest
	 * @param viewDescriptors
	 * @return command
	 */
	protected Command createViewsAndArrangeCommand2(Point locat, List viewDescriptors) {
		CreateViewRequest createViewRequest =
			new CreateViewRequest(viewDescriptors);
		createViewRequest.setLocation(locat);
		Command createCommand = getHost().getCommand(createViewRequest);

		if (createCommand != null) {
			List result = (List)createViewRequest.getNewObject();
			//MCPdropRequest.setResult(result);

			RefreshConnectionsRequest refreshRequest =
				new RefreshConnectionsRequest(result);
			Command refreshCommand = getHost().getCommand(refreshRequest);

			ArrangeRequest arrangeRequest =
				new ArrangeRequest(RequestConstants.REQ_ARRANGE_DEFERRED);
			arrangeRequest.setViewAdaptersToArrange(result);
			Command arrangeCommand = getHost().getCommand(arrangeRequest);

			CompoundCommand cc = new CompoundCommand(createCommand.getLabel());
			cc.add(createCommand.chain(refreshCommand));
			cc.add(arrangeCommand);
			
			return cc;
		}
		return null;
	}
	
	

    private EObject getSource(EObject element)
    {
    	EObject res = null;
		if(element instanceof AssertedInferenceImpl)
		{
			AssertedInferenceImpl el = (AssertedInferenceImpl)element;
			res = (EObject)el.getSource().get(0);
			System.out.println("ExpandBarDropEditPolicy.getSource(): Warning only returned one source element");
		}
		else if(element instanceof AssertedEvidenceImpl)
		{
			AssertedEvidenceImpl el = (AssertedEvidenceImpl)element;
			res = (EObject)el.getSource().get(0);
			System.out.println("ExpandBarDropEditPolicy.getSource(): Warning only returned one source element");
		}
		else if(element instanceof AssertedContextImpl)
		{
			AssertedContextImpl el = (AssertedContextImpl)element;
			res = (EObject)el.getSource().get(0);
			System.out.println("ExpandBarDropEditPolicy.getSource(): Warning only returned one source element");			
		}
		else
		{
			System.out.println("ExpandBarDropEditPolicy.getSource(): Error unkonwn Edge type");
		}
		
		return res;
    }
    private EObject getTarget(EObject element)
    {
    	EObject res = null;
    	
		if(element instanceof AssertedInferenceImpl)
		{
			AssertedInferenceImpl el = (AssertedInferenceImpl)element;
			res = (EObject)el.getTarget().get(0);
			System.out.println("ExpandBarDropEditPolicy.getTarget(): Warning only returned one target element");
		}
		else if(element instanceof AssertedEvidenceImpl)
		{
			AssertedEvidenceImpl el = (AssertedEvidenceImpl)element;			
			res = (EObject)el.getTarget().get(0);
			System.out.println("ExpandBarDropEditPolicy.getTarget(): Warning only returned one target element");				
		}
		else if(element instanceof AssertedContextImpl)
		{
			AssertedContextImpl el = (AssertedContextImpl)element;			
			res = (EObject)el.getTarget().get(0);
			System.out.println("ExpandBarDropEditPolicy.getTarget(): Warning only returned one target element");			
		}
		else
		{
			System.out.println("ExpandBarDropEditPolicy.getTarget(): Error unkonwn Edge type");
		}
		
		return res;
    }

}
// End MCP
