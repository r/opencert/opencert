/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.ui;

import java.net.URL;

import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.opencert.elastic.ElasticClientImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.forms.widgets.FormText;

/**
 * Bundle preference page, no specials.
 */
@SuppressWarnings({ "nls", "javadoc" })
public class ElasticPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	private StringFieldEditor serverField;

	private StringFieldEditor indexField;

	public ElasticPreferencePage() {
		super(FieldEditorPreferencePage.GRID);
		this.setDescription("Index your workspace to Elasticsearch");
	}

	@Override
	public void init(IWorkbench workbench) {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		this.setPreferenceStore(store);
	}

	@Override
	protected void createFieldEditors() {
		Composite parent = this.getFieldEditorParent();

		FormText hint = new FormText(parent, SWT.NO_FOCUS);
		hint.setText("<form><p>Configure indexing into <b>Elasticsearch</b> including server setting. "
				+ "Note that <b>credentials</b> must be part of the server URL.</p></form>",
			true, false);
		GridDataFactory.swtDefaults().align(SWT.FILL, SWT.TOP).hint(200, SWT.DEFAULT).grab(true, false).span(3, 1)
				.indent(0, 12).applyTo(hint);

		serverField = new StringFieldEditor(ElasticPreferences.SERVER_ADDRESS, "Server", parent);
		this.addField(serverField);
		serverField.getTextControl(parent).setMessage("http://localhost:9200"); //$NON-NLS-1$

		indexField = new StringFieldEditor(ElasticPreferences.INDEX_NAME, "Index", parent);
		this.addField(indexField);
		indexField.getTextControl(parent).setMessage("amass"); //$NON-NLS-1$

		Link link1 = new Link(parent, SWT.NONE);
		link1.setText("You can <a>probe the connection to the server</a>");
		link1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				probeTheConnection();
			}
		});
		GridDataFactory.swtDefaults().align(SWT.FILL, SWT.TOP).grab(true, false).span(2, 1).align(SWT.RIGHT, SWT.CENTER)
				.applyTo(link1);

		Link link2 = new Link(parent, SWT.NONE);
		link2.setText("You can also <a>delete the remote index</a>");
		link2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				deleteIndex();
			}
		});
		GridDataFactory.swtDefaults().align(SWT.FILL, SWT.TOP).grab(true, false).span(2, 1).align(SWT.RIGHT, SWT.CENTER)
				.applyTo(link2);

	}

	/*
	 * Probes the configured server settings.
	 */
	private void probeTheConnection() {
		setMessage(null);
		setErrorMessage(null);
		try {
			String value = serverField.getStringValue();
			URL url = new URL(value);
			String version = ElasticClientImpl.on(url.getHost(), url.getPort(), url.getProtocol()).ping().version();
			setMessage("Server OK: Version " + version + " found", DialogPage.INFORMATION);
		} catch (Throwable e) {
			String message = e.getMessage();
			if (message == null) {
				message = e.toString();
			}
			setErrorMessage("Failed: " + message);
		}
	}

	/*
	 * Deletes the configured index.
	 */
	private void deleteIndex() {
		setMessage(null);
		setErrorMessage(null);
		try {
			String value = serverField.getStringValue();
			URL url = new URL(value);
			String index = indexField.getStringValue();
			ElasticClientImpl.on(url.getHost(), url.getPort(), url.getProtocol()).ping().delete(index);
			setMessage("Index deleted", DialogPage.INFORMATION);
		} catch (Throwable e) {
			String message = e.getMessage();
			if (message == null) {
				message = e.toString();
			}
			setErrorMessage("Failed: " + message);
		}
	}
}
