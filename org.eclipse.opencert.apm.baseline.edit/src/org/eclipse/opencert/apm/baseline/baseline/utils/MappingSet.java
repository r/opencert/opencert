/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.baseline.baseline.utils;

import java.awt.color.CMMException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.util.CDOException;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.util.ModelStorage;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.infra.mappings.mapping.MapJustification;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.infra.mappings.mapping.MapModel;
import org.eclipse.opencert.infra.mappings.mapping.MappingFactory;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.Participant;
import org.eclipse.opencert.pam.procspec.process.Person;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.pam.procspec.process.Technique;
import org.eclipse.opencert.pam.procspec.process.Tool;
import org.eclipse.opencert.pkm.refframework.refframework.provider.RefframeworkEditPlugin;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.Claim;

public class MappingSet extends Dialog {

	private static final String ClassDiagram = "org.eclipse.opencert.apm.baseline.baseline.diagram.part.DawnBaselineDiagramEditor";
	
	private static final String UTF_8 = "UTF-8";

	private static final String PROCESS = ".process";
	private static final String EVIDENCE = ".evidence";
	private static final String ARG = ".arg";

	protected String qref;
	
	private String sNameModelo = "";
	
	
	protected String pathModelMapping;

	private CDOView viewCDO = null;
	private CDOTransaction transaction = null;
	private CDOSession sessionCDO = null;

	private CDOResource resourceFrom = null;
	private CDOResource resourceAssurance = null;
	private CDOResource resourceMapModel = null;
	private CDOResourceFolder assuranceprojectFolder = null;
	private CDOResourceFolder projectFolder = null;

	protected org.eclipse.swt.widgets.List refList;
	protected ArrayList<String> refListDirRef;
	protected ArrayList<String> refListDirMap;
	protected ArrayList<String> refListDirModel;

	protected IEditorPart editor;
	protected URI resourceURI;

	protected URI mappingURI;
	protected URI refURI;

	protected TreeViewerExt modelViewerSource;
	protected CheckboxTreeViewerExt modelViewerTarget;

	protected Label sourceLabel;
	protected Combo cbTarget;
	protected Combo cbMapGroup;
	protected Combo cbComplianceMap;
	protected Combo cbFilterElementMap;
	protected Text txtJustification;
	protected Text idText;
	protected Text nameText;
	protected Text typeText;
	protected Combo cbType;

	boolean nelementTreeSelected = false;
	boolean bnewEquivalenceMap = false;
	boolean bEqualMapGroup = false;

	Composite sourceComposite = null;
	
	EObject object;

	// List Equivalence Map of the selected element tree
	EList<BaseComplianceMap> lstrefCompMapFrom;

	Map<Object, Object> options = new HashMap<Object, Object>();

	EList<BaseComplianceMap> lstComplianceMap;
	EList<MapGroup> lstMapGroup;
	EList<Activity> lstrefActivity;
	EList<Artefact> lstrefArtefact;
	EList<Claim> lstrefRequirement;
	EList<Participant> lstrefRole;
	EList<Technique> lstrefTechnique;

	protected AssuranceProject assuranceProject;
	protected BaseFramework baseFrameworkFrom;
	protected BaseFramework baseFramework;
	protected MapGroup mapGroupActive;
	protected MapGroup mapGroup;
	protected MapKind mapKind;
	protected MapJustification mapJustification;
	protected MapModel mapModel;
	protected BaseComplianceMap baseComplianceMap; 

	String sElementFilter;

	private ReferencesTableSettings targetSettings;

	public MappingSet(Shell parentShell) {
		super(parentShell);

		editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage().getActiveEditor();
		
		if (editor.getClass().getName().contentEquals(ClassDiagram)) {
			
			ModelStorage ms=ModelStorage.getInstance();
			resourceURI = ms.getResourceURI();
				
		} else {
			resourceURI = EditUIUtil.getURI(editor.getEditorInput());
			
		}
		
		System.out.println("MODELO _usar: " + resourceURI.toString());
		
		
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		CDOView [] views = sessionCDO.getViews();
		if (views.length!=0) {
			viewCDO = views[0];				
		}
		transaction = sessionCDO.openTransaction();

		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
	}
	
	public MappingSet(Shell parentShell,URI resourceURI) {
		super(parentShell);

		
		this.resourceURI = resourceURI;
		
		//System.out.println("MODELO _usar: " + resourceURI.toString());
		
		
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		viewCDO = CDOConnectionUtil.instance.openView(sessionCDO);
		transaction = sessionCDO.openTransaction();

		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
	}

	public MappingSet(IShellProvider parentShell) {
		super(parentShell);
	}

	@Override
	protected Control createDialogArea(final Composite parent) {

		refListDirRef = new ArrayList<String>();
		refListDirMap = new ArrayList<String>();
		refListDirModel = new ArrayList<String>();
		
	
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		final Composite contents = (Composite) super.createDialogArea(parent);

		GridLayout contentsGridLayout = (GridLayout) contents.getLayout();
		contentsGridLayout.numColumns = 3;

		GridData contentsGridData = (GridData) contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;

		// Default filter
		sElementFilter = "Activity";

		createFromDialogArea(contents, adapterFactory);
		createMiddleDialogArea(contents);
		createTargetDialogArea(contents,adapterFactory);

		createMSGDialogArea(parent,adapterFactory);
		
		return contents;
	}
	
	
	private boolean searchInTreeNodes(CDOObject searchObject, TreeItem root) {
		for(int x=0; x <root.getItems().length;x++){
			TreeItem  oneItem = root.getItems()[x];
			
			Object dataObject = oneItem.getData();
			if(dataObject instanceof CDOObject){
				if(((CDOObject)dataObject).cdoID()==searchObject.cdoID()){					
					oneItem.setChecked(true);		
					IStructuredSelection selection = new StructuredSelection(dataObject);
					modelViewerSource.setSelection(selection, true); 
					return true;
				}
			}
			if(oneItem.getItems().length>0){
				boolean found=searchInTreeNodes( searchObject,  oneItem);				
				if (found) return true;
			}					
		}
		return false;
	}	
	
	public void selectControlValues(String elementType,BaseAssurableElement elementFrom, BaseComplianceMap bCM, String mapName, String targetModel){
		
		
		
		int index=0;
		if( elementType.equals("Activity")) index=0;
		else if( elementType.equals("Artefact")) index=1;
		else if( elementType.equals("Requirement")) index=2;
		else if( elementType.equals("Technique")) index=3;
		else if( elementType.equals("Role")) index=4;
		//cbFilterElementMap.select(0);
					
		cbFilterElementMap.select(index);
		sElementFilter = elementType;
		
		elementTypeChanged();
		
		String [] elements= null;
		
		
		
		
		if(bCM!= null){
			//Seleccion del mapGroup y the fichero de Mapping 
			qref= bCM.getMapGroup().eResource().getURI().path();

			elements= refList.getItems();

			for(int x=0; x< elements.length; x++){
				if(qref.contains(elements[x])) {
					refList.select(x);
					refList.notifyListeners(SWT.Selection, new Event());
					break;
				}
			}		


			String selectedGroup = bCM.getMapGroup().getName();
			mapGroupActive= bCM.getMapGroup();
			elements= cbMapGroup.getItems();

			for(int x=0; x< elements.length; x++){
				if(selectedGroup.equals(elements[x])) {
					cbMapGroup.select(x);
					cbMapGroup.notifyListeners(SWT.Selection, new Event());
					break;
				}
			}



			//Select the correct target.				
			elements= cbTarget.getItems();

			
			for(int x=0; x< elements.length; x++){
				//Has compliance with no target elements
				if(targetModel!=null){
					if(targetModel.contains(elements[x])) {
						cbTarget.select(x);
						cbTarget.notifyListeners(SWT.Selection, new Event());
						break;
					}
				}else{
					cbTarget.select(0);
					cbTarget.notifyListeners(SWT.Selection, new Event());
					break;
				}
			}			





			//Select the refereced element from tree.
			Tree fromTree =modelViewerSource.getTree();
			TreeItem[] rootNodes = fromTree.getItems();
			boolean found =false;
			//For each target in compliance search in evidence tree

			for(TreeItem root:rootNodes){
				//for(int x=0; x <root.getItems().length;x++){
					//TreeItem  oneItem = root.getItems()[x];										
					found = searchInTreeNodes(elementFrom,root);				
					if (found) break;
				//}

				//if (found) break;
			}


			elements= cbComplianceMap.getItems();

			for(int x=0; x< elements.length; x++){
				if(mapName.equals(elements[x])) {
					cbComplianceMap.select(x);
					cbComplianceMap.notifyListeners(SWT.Selection, new Event());
					break;
				}
			}


		}
		else{
			//Select the refereced element from tree.
			Tree fromTree =modelViewerSource.getTree();
			TreeItem[] rootNodes = fromTree.getItems();
			boolean found =false;
			//For each target in compliance search in evidence tree
			
			for(TreeItem root:rootNodes){
				for(int x=0; x <root.getItems().length;x++){
					TreeItem  oneItem = root.getItems()[x];
					found = searchInTreeNodes(elementFrom,oneItem);				
					if (found) break;
				}

				if (found) break;
			}
		}
		
		//pathModelMapping = resourceURI.segment(0)+ "/" + resourceURI.segment(1)+ "/" + sName;
		//resourceMapModel = transaction.getResource(pathModelMapping);
		
		//mapModel = (MapModel) resourceMapModel.getContents().get(0);
		
		//CreateModelFromFiletocombo(cbMapGroup,pathModelMapping);
		
		//qref = "";
		/*refList.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent e) {
		    	  if (refList.getSelectionIndex() != -1) {
			  		qref = refListDirMap.get(refList.getSelectionIndex()).toString();
			  		CreateModelFromFiletocombo(cbMapGroup,pathModelMapping);
		    	  }
		      }
		});*/
		
		//IFile file = (IFile) input.getAdapter(IFile.class);
		//StructuredSelection selection = new StructuredSelection(elementFrom);
		
		
		
	}
	
	

	protected void createMSGDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {

		Composite msgComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			msgComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 10;
			layout.numColumns = 1;
			msgComposite.setLayout(layout);
		}

		// Label
		Label msgLabel = new Label(sourceComposite, SWT.NONE);
		msgLabel.setText("Steps:\n\t- Select the mapping model, the map group and the target model.\n\t- "
				+ "Finally, select the object from the source framework model and the compliance map");
		GridData msgLabelGridData = new GridData();
		msgLabelGridData.horizontalAlignment = SWT.FILL;
		msgLabelGridData.verticalAlignment = SWT.FILL;
		msgLabel.setLayoutData(msgLabelGridData);

	}
			
	private void complianceMapSelected() {
		modelViewerTarget.setAllChecked(false);
		txtJustification.setText("");
		idText.setText("");
		nameText.setText("");
		cbType.select(-1);

		if ((cbTarget.getSelectionIndex() == -1) || !nelementTreeSelected) {

			//if(cbTarget.getSelectionIndex() == -1){
				MessageDialog.openError(getShell(), "Compliance Map",
						"It's neccesary select a target");
			//}

		} else {
			// mapGroup selected
			baseComplianceMap = lstComplianceMap.get(cbComplianceMap.getSelectionIndex());
			if (baseComplianceMap.getId() != null)
				idText.setText(baseComplianceMap.getId());

			if (idText.getListeners(SWT.CHANGED).length > 0)
				idText.removeListener(SWT.CHANGED,idText.getListeners(SWT.CHANGED)[0]);

			idText.addListener(SWT.CHANGED,new Listener() {
				@Override
				public void handleEvent(Event event) {
					Text text = (Text) event.widget;
					baseComplianceMap.setId(text.getText());
					try {
						if (!bnewEquivalenceMap) {
							baseComplianceMap.eResource().save(options);
						}
					} catch (IOException e) { 

						e.printStackTrace();
					}
				}
			});

			if (baseComplianceMap.getName() != null)
				nameText.setText(baseComplianceMap.getName());

			if (nameText.getListeners(SWT.CHANGED).length > 0)
				nameText.removeListener(SWT.CHANGED,nameText.getListeners(SWT.CHANGED)[0]);

			nameText.addListener(SWT.CHANGED,new Listener() {
				@Override
				public void handleEvent(Event event) {
					Text text1 = (Text) event.widget;

					baseComplianceMap.setName(text1.getText());
					try {
						if (!bnewEquivalenceMap) {
							baseComplianceMap.eResource().save(options);
						}
					} catch (IOException e) { 
						e.printStackTrace();
					}
				}
			});

			mapKind = baseComplianceMap.getType();
			if (mapKind != null && mapKind.getName() != null) {
				// Recorrer la combo para seleccionar el valor que coincida
				int i = 0;
				for (i = 0; i < cbType.getItemCount(); i++) {
					if (cbType.getItem(i).contentEquals(mapKind.getName()))
						cbType.select(i);
				}
			}

			if (cbType.getListeners(SWT.Selection).length > 0)
				cbType.removeListener(SWT.Selection,cbType.getListeners(SWT.Selection)[0]);

			cbType.addListener(SWT.Selection,new Listener() {
				@Override
				public void handleEvent(Event event) {
					Combo combo = (Combo) event.widget;
					String value = combo.getItem(combo.getSelectionIndex());
					baseComplianceMap.setType(MapKind.get(value));
					try {
						if (!bnewEquivalenceMap) {
							baseComplianceMap.eResource().save(options);
						}
					} catch (IOException e) { 
						e.printStackTrace();
					}
				}
			});

			mapJustification = baseComplianceMap.getMapJustification();
			if (mapJustification != null && mapJustification.getExplanation() != null)
				txtJustification.setText(mapJustification.getExplanation());

			if (txtJustification.getListeners(SWT.CHANGED).length > 0)
				txtJustification.removeListener(SWT.CHANGED,txtJustification.getListeners(SWT.CHANGED)[0]);

			txtJustification.addListener(SWT.CHANGED,new Listener() {
				@Override
				public void handleEvent(Event event) {
					Text text = (Text) event.widget;
					if (mapJustification == null){
						mapJustification = MappingFactory.eINSTANCE.createMapJustification();
						
					}
					mapJustification.setExplanation(text.getText());
					baseComplianceMap.setMapJustification(mapJustification);
					try {

						//if (!bnewEquivalenceMap) {
							mapJustification.eResource().save(options);
						//}
					} catch (IOException e) { 
						e.printStackTrace();
					}
				}
			});


			String sTargetSelect =cbTarget.getItem(cbTarget.getSelectionIndex());
			System.out.println(sTargetSelect);

			EList<AssuranceAsset> lstassElement = baseComplianceMap.getTarget();
			// Es necesario inicializar aqui targetSettings para a�adir o eliminar actividades del target.
			targetSettings = new ReferencesTableSettings(baseComplianceMap,BaselinePackage.eINSTANCE.getBaseComplianceMap_Target());

			Iterator<AssuranceAsset> iter2 = lstassElement.iterator();
			while (iter2.hasNext()) {
				if (sElementFilter.contentEquals("Activity")) {
					Activity assElement = (Activity) iter2.next();
					Iterator<Activity> iter3 = lstrefActivity.iterator();
					while (iter3.hasNext()) {
						Activity ActivityTo = (Activity) iter3.next();

						/*if (assElement.getId().equals(ActivityTo.getId())) {
						modelViewerTarget.CheckedElement(ActivityTo);
					}*/

						if (CDOUtil.getCDOObject(ActivityTo).cdoID().equals(CDOUtil.getCDOObject(assElement).cdoID()))
							modelViewerTarget.CheckedElement(ActivityTo);
						ActivityTo = null;
					}
					assElement = null;
					iter3 = null;
				} else if (sElementFilter.contentEquals("Artefact")) {
					Artefact assElement = (Artefact) iter2.next();
					Iterator<Artefact> iter3 = lstrefArtefact.iterator();
					while (iter3.hasNext()) {
						Artefact refElementTo = (Artefact) iter3.next();
						if (CDOUtil.getCDOObject(refElementTo).cdoID().equals(CDOUtil.getCDOObject(assElement).cdoID()))
							modelViewerTarget.CheckedElement(refElementTo);
						refElementTo = null;
					}
					assElement = null;
					iter3 = null;
				} else if (sElementFilter.contentEquals("Requirement")) {

					//Cambios

					if (sTargetSelect.endsWith(ARG)) {
						Claim assElement = (Claim) iter2.next();
						Iterator<Claim> iter3 = lstrefRequirement.iterator();
						while (iter3.hasNext()) {
							Claim refElementTo = (Claim) iter3.next();
							if (CDOUtil.getCDOObject(refElementTo).cdoID().equals(CDOUtil.getCDOObject(assElement).cdoID()))
								modelViewerTarget.CheckedElement(refElementTo);
							refElementTo = null;
						}
						assElement = null;
						iter3 = null;
					}

					if (sTargetSelect.endsWith(EVIDENCE)) {
						Artefact assElement = (Artefact) iter2.next();
						Iterator<Artefact> iter3 = lstrefArtefact.iterator();
						while (iter3.hasNext()) {
							Artefact refElementTo = (Artefact) iter3.next();
							if (CDOUtil.getCDOObject(refElementTo).cdoID().equals(CDOUtil.getCDOObject(assElement).cdoID()))
								modelViewerTarget.CheckedElement(refElementTo);
							refElementTo = null;
						}
						assElement = null;
						iter3 = null;
					}

					if (sTargetSelect.endsWith(PROCESS)) {
						Activity assElement = (Activity) iter2.next();
						Iterator<Activity> iter3 = lstrefActivity.iterator();
						while (iter3.hasNext()) {
							Activity ActivityTo = (Activity) iter3.next();
							if (CDOUtil.getCDOObject(ActivityTo).cdoID().equals(CDOUtil.getCDOObject(assElement).cdoID()))
								modelViewerTarget.CheckedElement(ActivityTo);
							ActivityTo = null;
						}
						assElement = null;
						iter3 = null;
					}




				} else if (sElementFilter.contentEquals("Role")) {
					Participant assElement = (Participant) iter2.next();
					Iterator<Participant> iter3 = lstrefRole.iterator();
					while (iter3.hasNext()) {
						Participant refElementTo = (Participant) iter3.next();
						if (CDOUtil.getCDOObject(refElementTo).cdoID().equals(CDOUtil.getCDOObject(assElement).cdoID()))
							modelViewerTarget.CheckedElement(refElementTo);
						refElementTo = null;
					}
					assElement = null;
					iter3 = null;
				} else {
					Technique assElement = (Technique) iter2.next();
					Iterator<Technique> iter3 = lstrefTechnique.iterator();
					while (iter3.hasNext()) {
						Technique refElementTo = (Technique) iter3.next();
						if (CDOUtil.getCDOObject(refElementTo).cdoID().equals(CDOUtil.getCDOObject(assElement).cdoID()))
							modelViewerTarget.CheckedElement(refElementTo);
						refElementTo = null;
					}
					assElement = null;
					iter3 = null;
				}
			} // End of while iter2
			iter2 = null;
		}
	}
	
	protected void createFromDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {

		String path = "";
		
		sourceComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			sourceComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 10;
			layout.numColumns = 1;
			sourceComposite.setLayout(layout);
		}

		sourceComposite.setEnabled(false);
		
		// Label
		sourceLabel = new Label(sourceComposite, SWT.NONE);
		sourceLabel.setText("From");
		GridData sourceLabelGridData = new GridData();
		sourceLabelGridData.horizontalAlignment = SWT.FILL;
		sourceLabelGridData.verticalAlignment = SWT.FILL;
		sourceLabel.setLayoutData(sourceLabelGridData);

		// TreeViewer for BaseLine
		modelViewerSource = new TreeViewerExt(sourceComposite, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);

		final GridData modelViewerGridData = new GridData();
		modelViewerGridData.verticalAlignment = SWT.FILL;
		modelViewerGridData.horizontalAlignment = SWT.FILL;
		modelViewerGridData.widthHint = Display.getCurrent().getBounds().width / 3;
		modelViewerGridData.heightHint = Display.getCurrent().getBounds().height / 3;
		modelViewerGridData.grabExcessHorizontalSpace = false;
		modelViewerGridData.grabExcessVerticalSpace = true;

		modelViewerSource.getTree().setLayoutData(modelViewerGridData);
		modelViewerSource.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		modelViewerSource.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		resourceFrom = transaction.getResource(resourceURI.path());	
		
		baseFrameworkFrom = (BaseFramework) resourceFrom.getContents().get(0);

		CreateModelFromFile(modelViewerSource, resourceFrom);
		modelViewerSource.expandAll();

		nelementTreeSelected = false;

		// Look for assusance project model
		assuranceprojectFolder = resourceFrom.getFolder();
		projectFolder = assuranceprojectFolder.getFolder();
		EList<CDOResourceNode> lstResourceNodes = assuranceprojectFolder.getNodes();
		Iterator<CDOResourceNode> resourceNode = lstResourceNodes.iterator();

		boolean bFind = false;
		String sName = "";
		CDOResourceNode node=null;
		while (resourceNode.hasNext() && !bFind) {
			node = resourceNode.next();
			sName = node.getName();
			System.out.println(sName);
			if (sName.endsWith(".assuranceproject")) {
				bFind = true;
			}
		}

		if (!bFind) {
			//Assurance project model doesn�t find
			MessageDialog.openError(getShell(),	"Error Compliance Map ","Assurance Project doesn't find");
			
		} else {
			
			//path = resourceURI.segment(0) + "/" + resourceURI.segment(1) + "/"+ sName;
			resourceAssurance = (CDOResource) node; //transaction.getResource(node.getPath());

			assuranceProject = (AssuranceProject) resourceAssurance.getContents().get(0);

			assuranceProject.getBaselineConfig();

			EList<BaselineConfig> lstBaselineConfig = assuranceProject.getBaselineConfig();
			Iterator<BaselineConfig> baselineConfig = lstBaselineConfig.iterator();

			bFind = false;
			mapGroupActive = null;
			while (baselineConfig.hasNext() && !bFind) {
				BaselineConfig bconfig = baselineConfig.next();
				if (bconfig.isIsActive()) {
					mapGroupActive = bconfig.getComplianceMapGroup();
					EList<BaseFramework> lstBaseFramework = bconfig.getRefFramework();
					Iterator<BaseFramework> baseFramework = lstBaseFramework.iterator();
					while (baseFramework.hasNext() && !bFind) {
						BaseFramework bFramework = baseFramework.next();
						
						CDOUtil.getCDOObject(bFramework).cdoID().equals(CDOUtil.getCDOObject(baseFrameworkFrom).cdoID());
						
						if (CDOUtil.getCDOObject(bFramework).cdoID().equals(CDOUtil.getCDOObject(baseFrameworkFrom).cdoID()))
							bFind = true;
					}
				}
			}

			if (!bFind) {
				// Mensaje de error comunicando que el baseline no es el activo
				MessageDialog.openError(getShell(),	"Error Compliance Map ","Selected Baseline isn�t the active one of the assurance project");
			} else {
				modelViewerSource.addSelectionChangedListener(new ISelectionChangedListener() {

					@SuppressWarnings({ "deprecation" })
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						
						cbComplianceMap.removeAll();
						txtJustification.setText("");
						idText.setText("");
						nameText.setText("");
						cbType.select(0);

						nelementTreeSelected = true;
						bnewEquivalenceMap = false;

						// Unselected all the elements of the target tree
						modelViewerTarget.expandAll();
						modelViewerTarget.setAllChecked(false);

						if (event.getSelection().isEmpty()) {
							System.out.println("Empty");
							return;
						}

						object = null;

						if (event.getSelection() instanceof IStructuredSelection) {
							
							IStructuredSelection sel = (IStructuredSelection) event.getSelection();
							object = (EObject) sel.getFirstElement();
							// Para descartar la selecci�n del framework comprobamos si su containeres null
							if (object instanceof BaseFramework || object instanceof BaseComplianceMap) {
								nelementTreeSelected = false;
								return;
							}
							
							if (sElementFilter.contentEquals("Requirement")) {
								if (object instanceof BaseActivity) {
									MessageDialog.openError(getShell(),	"Compliance Map ","It's neccessary to select a requirement, not an activity. ");
									return;
								}
							}
								
							

							if (object instanceof EObject) {
								// Verify selection mapgroup and cbtarget
								mapGroup = null;
								if ((cbMapGroup.getSelectionIndex() == -1) || (cbTarget.getSelectionIndex() == -1)) {
									MessageDialog.openError(getShell(),	"Compliance Map ","It's neccessary to select a map group and a target ");
								} else {
									// mapGroup selected
									mapGroup = lstMapGroup.get(cbMapGroup.getSelectionIndex());
									// List Compliance Map of the selected element tree
									lstrefCompMapFrom = null;
			
									sElementFilter = cbFilterElementMap.getItem(cbFilterElementMap.getSelectionIndex());
			
									// Object is the assurableElement selected
									// from the source Tree
									if (sElementFilter.contentEquals("Activity")) {
										BaseActivity refElemtFrom = (BaseActivity) object;
										lstrefCompMapFrom = refElemtFrom.getComplianceMap();
										refElemtFrom = null;
									} else if (sElementFilter.contentEquals("Artefact")) {
										BaseArtefact refElemtFrom = (BaseArtefact) object;
										lstrefCompMapFrom = refElemtFrom.getComplianceMap();
										refElemtFrom = null;
									} else if (sElementFilter.contentEquals("Requirement")) {
										
										
										
										BaseRequirement refElemtFrom = (BaseRequirement) object;
										lstrefCompMapFrom = refElemtFrom.getComplianceMap();
										refElemtFrom = null;
									} else if (sElementFilter.contentEquals("Role")) {
										BaseRole refElemtFrom = (BaseRole) object;
										lstrefCompMapFrom = refElemtFrom.getComplianceMap();
										refElemtFrom = null;
									} else {
										BaseTechnique refElemtFrom = (BaseTechnique) object;
										lstrefCompMapFrom = refElemtFrom.getComplianceMap();
										refElemtFrom = null;
									}
									lstComplianceMap = (EList<BaseComplianceMap>) new BasicEList<BaseComplianceMap>();
									if (lstrefCompMapFrom.isEmpty()) {		
										bnewEquivalenceMap = true;
									} else {
										
										//Relleno la combo de compliance Map
										Iterator<BaseComplianceMap> iter1 = lstrefCompMapFrom.iterator();
										bEqualMapGroup = false;
										boolean isFirst= true;
										while (iter1.hasNext()) {
											MapGroup mapGroupFrom = null;
											final BaseComplianceMap baseCompMapFrom = (BaseComplianceMap) iter1.next();
			
											if (baseCompMapFrom.getMapGroup() != null) {
												mapGroupFrom = baseCompMapFrom.getMapGroup();
												
												if (CDOUtil.getCDOObject(mapGroupFrom).cdoID().equals(CDOUtil.getCDOObject(mapGroup).cdoID())){
													
													//if (mapGroupFrom.getId().equals(mapGroup.getId()) || mapGroupFrom.getName()	.equals(mapGroup.getName())) {
													bEqualMapGroup = true;
													
													//A�ado el objeto complianceMap a la combo
													//Cunado cree uno nuevo debere a�adirlo a la lista
													lstComplianceMap.add(baseCompMapFrom);													
													cbComplianceMap.add(baseCompMapFrom.getName());
													if(isFirst){
														cbComplianceMap.select(0);
														isFirst=false;
														cbComplianceMap.notifyListeners(SWT.Selection, new Event());
													}
													mapGroupFrom = null;
												}
												
												
											} else {
												mapGroupFrom = null;
											}
											
											
			
										} // End of while iter1 
										iter1 = null;
										if (!bEqualMapGroup) {
											// addEquivalenceMap(mapGroup,
											// object);
											// lo hago cuando seleccioneun
											// targer
										}
									}
								}
							}
						}
					}
				});
			}
		}
	}

	protected void createMiddleDialogArea(final Composite contents) {
		
		Composite controlMapping = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			controlMapping.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1;
			controlMapping.setLayout(layout);
		}

		Group groupFilter = new Group(controlMapping, SWT.NULL);
		groupFilter.setLayout(new GridLayout(1,false));
		groupFilter.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,2,1));
		groupFilter.setText("Filtering");
		
		refList = new org.eclipse.swt.widgets.List(groupFilter, SWT.FILL | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);
		refList.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		refListDirMap = new ArrayList<String>();
		
		/*En este caso solo incluimos el mapping del mapgroup relacionado con el baseFramework a traves del baselineconfig*/	
		System.out.println(resourceFrom.getFolder().getPath());
		
		EList<CDOResourceNode> lstResourceNodes = assuranceprojectFolder.getNodes();
		Iterator<CDOResourceNode> resourceNode = lstResourceNodes.iterator();
		
		boolean bFind = false;
		String sName = "";
		CDOResourceNode node=null;
		while (resourceNode.hasNext() && !bFind) {
			node = resourceNode.next();
			sName = node.getName();
			System.out.println(sName);
			if (sName.endsWith(".mapping")) {
				refList.add(sName);
				refListDirMap.add(node.getPath());
				bFind = true;
			}
		}
				
		
		//pathModelMapping = resourceURI.segment(0)+ "/" + resourceURI.segment(1)+ "/" + sName;
		resourceMapModel = (CDOResource) node;//transaction.getResource(pathModelMapping);
		
		mapModel = (MapModel) resourceMapModel.getContents().get(0);
		
		qref = "";
		refList.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent e) {
		    	  if (refList.getSelectionIndex() != -1) {
		  	  		qref = refListDirMap.get(refList.getSelectionIndex()).toString();
		  	  		resourceMapModel = viewCDO.getResource(qref, true);
		  	  		CreateModelFromFiletocombo(cbMapGroup,resourceMapModel);
		      	  }
		      }
			
		});
		
		
		Label mapLabel = new Label(groupFilter, SWT.NONE);
		mapLabel.setText("Map Group");
		GridData mapLabelGridData = new GridData();
		mapLabelGridData.horizontalSpan = 2;
		mapLabelGridData.horizontalAlignment = SWT.FILL;
		mapLabelGridData.verticalAlignment = SWT.FILL;
		mapLabel.setLayoutData(mapLabelGridData);

		final Composite controlMappingNew = new Composite(groupFilter, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.END;
			controlMappingNew.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			controlMappingNew.setLayout(layoutNew);
		}
		
		
		cbMapGroup = new Combo (controlMappingNew, SWT.READ_ONLY);
		GridData cbMapGroupGridData = new GridData();
		cbMapGroupGridData.verticalAlignment = SWT.FILL;
		cbMapGroupGridData.horizontalAlignment = SWT.FILL;
		cbMapGroupGridData.grabExcessHorizontalSpace = true;
		cbMapGroupGridData.grabExcessVerticalSpace = true;
		cbMapGroup.setLayoutData(cbMapGroupGridData);
		
		// if we select a map group, verify selected element from sourcetree and cbtarget
		// show the equivalence from the element selected
		
		cbMapGroup.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {		
				
				if (cbTarget.getSelectionIndex() != -1) {
					sourceComposite.setEnabled(true);
				}
				
				txtJustification.setText("");
				idText.setText("");
				nameText.setText("");
				cbType.select(-1);		
				// mapGroup selected
				mapGroup = lstMapGroup.get(cbMapGroup.getSelectionIndex());
				
				if (CDOUtil.getCDOObject(mapGroup).cdoID().equals(CDOUtil.getCDOObject(mapGroupActive).cdoID())){
					//if (mapGroup.getName().contentEquals(mapGroupActive.getName())) {
				} else {
					MessageDialog.openError(getShell(), "Selected Map Group",
							"The seleted map group is not the map group of the active base line.");
				}
			}
		});

		Button NewGroupMap = new Button(controlMappingNew, SWT.NONE);
		NewGroupMap.setText("New Group");
		GridData NewGroupMapGridData = new GridData();
		NewGroupMapGridData.verticalAlignment = SWT.BEGINNING;
		NewGroupMapGridData.horizontalAlignment = SWT.FILL;
		NewGroupMap.setLayoutData(NewGroupMapGridData);

		NewGroupMap.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {

				if (qref != "") {
					GroupMapNew groupMapSet = new GroupMapNew(controlMappingNew.getShell(), sessionCDO, resourceMapModel.getPath());

					if (groupMapSet.open() == Window.OK) {
						if (groupMapSet.getResult()) {
							MessageDialog.openError(getShell(),
									"New Map Group", "New Map Group created.");

							// Reload Mapping
							cbMapGroup.removeAll();
							CreateModelFromFiletocombo(cbMapGroup, resourceMapModel);

						} else {
							MessageDialog
									.openError(getShell(), "New Map Group",
											"Fail to create a new map group. There is another map group with these values.");
						}
					}
				} else {
					MessageDialog.openError(getShell(), "New Map Group",
							"It's neccesary select a Mapping model");
				}

			}
		});
		
		Label filtermapLabel = new Label(groupFilter, SWT.NONE);
		filtermapLabel.setText("Filter Map Element");
		GridData filtermapLabelGridData = new GridData();
		filtermapLabelGridData.horizontalSpan = 2;
		filtermapLabelGridData.horizontalAlignment = SWT.FILL;
		filtermapLabelGridData.verticalAlignment = SWT.FILL;
		filtermapLabel.setLayoutData(filtermapLabelGridData);
		
		cbFilterElementMap = new Combo (groupFilter, SWT.READ_ONLY);
		GridData cbFilterElementMapGridData = new GridData();
		cbFilterElementMapGridData.verticalAlignment = SWT.FILL;
		cbFilterElementMapGridData.horizontalAlignment = SWT.FILL;
		cbFilterElementMapGridData.widthHint = Display.getCurrent().getBounds().width / 7;
		cbFilterElementMapGridData.heightHint = Display.getCurrent().getBounds().height / 7;
		cbFilterElementMapGridData.grabExcessHorizontalSpace = true;
		cbFilterElementMapGridData.grabExcessVerticalSpace = true;
		cbFilterElementMap.setLayoutData(cbFilterElementMapGridData);
		
		cbFilterElementMap.add("Activity");
		cbFilterElementMap.add("Artefact");
		cbFilterElementMap.add("Requirement");
		cbFilterElementMap.add("Technique");
		cbFilterElementMap.add("Role");
		cbFilterElementMap.select(0);
		
		
		cbFilterElementMap.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				elementTypeChanged();
			}

			
		});
	
		Group groupEquMap = new Group(controlMapping, SWT.NULL);
		groupEquMap.setLayout(new GridLayout(1,true));
		groupEquMap.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,1,1));
		groupEquMap.setText("Compliance Map");

		
		final Composite controlEquivMap = new Composite(groupEquMap, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.END;
			controlEquivMap.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			dataNew.verticalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			controlEquivMap.setLayout(layoutNew);
		}
		
		
		cbComplianceMap = new Combo (controlEquivMap, SWT.READ_ONLY);
		GridData cbComplianceMapData = new GridData();
		cbComplianceMapData.verticalAlignment = SWT.FILL;
		cbComplianceMapData.horizontalAlignment = SWT.FILL;
		cbComplianceMapData.grabExcessHorizontalSpace = true;
		cbComplianceMapData.grabExcessVerticalSpace = true;
		cbComplianceMap.setLayoutData(cbComplianceMapData);
		
		// if we select a map group, verify selected element from sourcetree and cbtarget
		// show the equivalence from the element selected
		
		cbComplianceMap.addSelectionListener(new SelectionAdapter() {
			@SuppressWarnings("deprecation")
			public void widgetSelected(SelectionEvent e) {				
				complianceMapSelected();				
			}
		});
		
		Button NewComplianceMap = new Button(controlEquivMap, SWT.NONE);
		NewComplianceMap.setText("New Map");
		GridData NewComplianceMapData = new GridData();
		NewComplianceMapData.verticalAlignment = SWT.BEGINNING;
		NewComplianceMapData.horizontalAlignment = SWT.FILL;
		NewComplianceMap.setLayoutData(NewComplianceMapData);

		NewComplianceMap.addSelectionListener(new SelectionAdapter() {
			@SuppressWarnings("deprecation")
			public void widgetSelected(SelectionEvent e) {

				boolean bSelectedNode = false;
				boolean bSelectedMapGroup = false;
				//Verify the selection of one node of the from tree and a mapgroup. 
								
				IStructuredSelection sel = (IStructuredSelection) modelViewerSource.getSelection();
				object = (EObject) sel.getFirstElement();
				
				sElementFilter = cbFilterElementMap.getItem(cbFilterElementMap.getSelectionIndex());

				// descartar la selecci�n del framework
				if ((object instanceof BaseFramework) || (object == null)) {
					bSelectedNode = false;
					MessageDialog.openError(getShell(), "New Compliance Map",
							"It's neccesary to select a node from the framework tree.");
					return;
				} else {
					// Object is the assurableElement selected from the source Tree
					bSelectedNode = true;
					if (sElementFilter.contentEquals("Activity")) {
						BaseActivity refElemtFrom = (BaseActivity) object;
						System.out.println("Selected Element: " + refElemtFrom.getName());
					} else if (sElementFilter.contentEquals("Artefact")) {
						BaseArtefact refElemtFrom = (BaseArtefact) object;
						System.out.println("Selected Element: " + refElemtFrom.getName());
					} else if (sElementFilter.contentEquals("Requirement")) {
						BaseRequirement refElemtFrom = (BaseRequirement) object;
						System.out.println("Selected Element: " + refElemtFrom.getName());
					} else if (sElementFilter.contentEquals("Role")) {
						BaseRole refElemtFrom = (BaseRole) object;
						System.out.println(refElemtFrom.getName());
					} else {
						BaseTechnique refElemtFrom = (BaseTechnique) object;
						System.out.println("Selected Element: " + refElemtFrom.getName());
					}		
				}
				
				
			
				
				if ((cbMapGroup.getSelectionIndex() == -1)) { 
					bSelectedMapGroup = false;
				} else {
					bSelectedMapGroup = true;
				}
				
				if (!bSelectedMapGroup) {
					MessageDialog.openError(getShell(), "New Compliance Map",
							"It's neccesary to select a map group.");
				} else if (!bSelectedNode) {
					MessageDialog.openError(getShell(), "New Compliance Map",
							"It's neccesary to select a node from the baseline tree.");
				} else {
					ComplianceMapNew complianceMapSet = new ComplianceMapNew(controlMappingNew
							.getShell(), object, mapGroup, baseFrameworkFrom);
					
					if (complianceMapSet.open() == Window.OK) {
						
						if (complianceMapSet.getResult()) {
							/*MessageDialog.openError(getShell(),
									"New Compliance Map", "New Compliance Map create.");*/
							
							// Reload combo compliance map
							CreateModelFromFiletocomboObject(cbComplianceMap,object);
							cbComplianceMap.select(cbComplianceMap.getItemCount()-1);
							cbComplianceMap.notifyListeners(SWT.Selection, new Event());
							modelViewerTarget.setAllChecked(false);
							txtJustification.setText("");
							idText.setText("");
							nameText.setText("");
							
							
						} else {
							MessageDialog
									.openError(getShell(), "New Map Group",
											"Fail to create a new map group. There is another map group with these values.");
						}
					}	
				}
			}
		});
		
		
		// Label
		Label idLabel = new Label(controlEquivMap, SWT.NONE);
		idLabel.setText("ID");
		GridData idLabelGridData = new GridData();
		idLabelGridData.horizontalAlignment = SWT.FILL;
		idLabelGridData.verticalAlignment = SWT.FILL;
		idLabel.setLayoutData(idLabelGridData);

		// Text
		idText = new Text(controlEquivMap, SWT.NONE);
		GridData idTextGridData = new GridData();
		idTextGridData.horizontalAlignment = SWT.FILL;
		idTextGridData.verticalAlignment = SWT.FILL;
		idText.setLayoutData(idTextGridData);
		
		
		// Label
		Label nameLabel = new Label(controlEquivMap, SWT.NONE);
		nameLabel.setText("Name");
		GridData nameLabelGridData = new GridData();
		nameLabelGridData.grabExcessHorizontalSpace = true;
		nameLabelGridData.minimumWidth = 20;
		nameLabelGridData.horizontalAlignment = SWT.FILL;
		nameLabelGridData.verticalAlignment = SWT.FILL;
		nameLabel.setLayoutData(nameLabelGridData);

		// Text
		nameText = new Text(controlEquivMap, SWT.NONE);
		GridData nameTextGridData = new GridData();
		nameTextGridData.grabExcessHorizontalSpace = true;
		nameTextGridData.minimumWidth = 20;
		nameTextGridData.horizontalAlignment = SWT.FILL;
		nameTextGridData.verticalAlignment = SWT.FILL;
		nameText.setLayoutData(nameTextGridData);
		
		
		// Text
		Label typeLabel = new Label(controlEquivMap, SWT.NONE);
		typeLabel.setText("Type");
		GridData typeLabelGridData = new GridData();
		typeLabelGridData.grabExcessHorizontalSpace = true;
		typeLabelGridData.minimumWidth = 20;
		typeLabelGridData.horizontalAlignment = SWT.FILL;
		typeLabelGridData.verticalAlignment = SWT.FILL;
		typeLabel.setLayoutData(typeLabelGridData);
		
		cbType = new Combo (controlEquivMap, SWT.READ_ONLY);
		GridData cbTypeGridData = new GridData();
		cbTypeGridData.verticalAlignment = SWT.FILL;
		cbTypeGridData.horizontalAlignment = SWT.FILL;
		cbTypeGridData.widthHint = Display.getCurrent().getBounds().width / 7;
		cbTypeGridData.heightHint = Display.getCurrent().getBounds().height / 7;
		cbTypeGridData.grabExcessHorizontalSpace = true;
		cbTypeGridData.grabExcessVerticalSpace = true;
		cbType.setLayoutData(cbTypeGridData);
		
		EEnum enumMapKind = MappingFactory.eINSTANCE.getMappingPackage().getMapKind();
		EList<EEnumLiteral> mapkindList= enumMapKind.getELiterals();
		
		Iterator<EEnumLiteral> iterTo = mapkindList.iterator();
		while (iterTo.hasNext()) {
			cbType.add(iterTo.next().getName());
		}
		cbType.select(0);
		
		Label justificationLabel = new Label(groupEquMap, SWT.NONE);
		justificationLabel.setText("Justification");
		GridData justificationLabelGridData = new GridData();
		justificationLabelGridData.horizontalAlignment = SWT.FILL;
		justificationLabelGridData.verticalAlignment = SWT.FILL;
		justificationLabel.setLayoutData(justificationLabelGridData);
		
		txtJustification = new Text(groupEquMap, SWT.MULTI);
		GridData txtJustificationGridData = new GridData();
		txtJustificationGridData.horizontalSpan = 2;
		txtJustificationGridData.horizontalAlignment = SWT.FILL;
		txtJustificationGridData.verticalAlignment = SWT.FILL;
		txtJustificationGridData.heightHint = Display.getCurrent().getBounds().height / 9;
		txtJustification.setLayoutData(txtJustificationGridData);
	}

	private void elementTypeChanged() {
		if (cbFilterElementMap.getSelectionIndex() != -1 ) {
			sElementFilter = cbFilterElementMap.getItem(cbFilterElementMap.getSelectionIndex());
			CreateModelFromFile(modelViewerSource,resourceFrom);
			modelViewerSource.expandAll();

			String sSufijo = null;
			String sSufijo1 = null;
			String sSufijo2 = null;

			if (sElementFilter.contentEquals("Activity")) {
				sSufijo = PROCESS;
				sSufijo1 = PROCESS;
				sSufijo2 = PROCESS;
			} else if (sElementFilter.contentEquals("Artefact")) {
				sSufijo = EVIDENCE;
				sSufijo1 = EVIDENCE;
				sSufijo2 = EVIDENCE;
			} else if (sElementFilter.contentEquals("Requirement")) {
				sSufijo = ARG;
				sSufijo1 = EVIDENCE;
				sSufijo2 = PROCESS;
			} else if (sElementFilter.contentEquals("Role")) {
				sSufijo = PROCESS;
				sSufijo1 = PROCESS;
				sSufijo2 = PROCESS;
			} else {
				sSufijo = PROCESS;
				sSufijo1 = PROCESS;
				sSufijo2 = PROCESS;
			}

			
			cbTarget.removeAll();
			
			modelViewerTarget.setInput(null);
			
			refListDirModel.clear();

			EList<CDOResourceNode> lstResourceNodes = projectFolder.getNodes();
			Iterator<CDOResourceNode> resourceNode = lstResourceNodes.iterator();

			while (resourceNode.hasNext()) {
				CDOResourceNode node = resourceNode.next();
				sNameModelo = node.getName();
				System.out.println(sNameModelo);

				if (node instanceof CDOResourceFolder )
				{
					CDOResourceFolder nodeFolder = (CDOResourceFolder)node;
					EList<CDOResourceNode> lstnodeFolder = nodeFolder.getNodes();
					Iterator<CDOResourceNode> nodeFolderNode = lstnodeFolder.iterator();
					while (nodeFolderNode.hasNext()) {
						CDOResourceNode node1 = nodeFolderNode.next();
						sNameModelo = node1.getName();
						System.out.println(sNameModelo);
						if ((node1.getName().endsWith(sSufijo)) || (node1.getName().endsWith(sSufijo1)) || (node1.getName().endsWith(sSufijo2)) ){

							//Finish
							//Verify if the model is part of the assetsPackage active
							EList<AssetsPackage> lstAssetsPackage = assuranceProject.getAssetsPackage();
							Iterator<AssetsPackage> assetsPackage = lstAssetsPackage.iterator();

							//mapGroupActive = null;
							while (assetsPackage.hasNext()) {
								AssetsPackage aPackage = assetsPackage.next();
								if (aPackage.isIsActive()) {

									Resource resource1 = viewCDO.getResource(node1.getPath(), true);
									EObject eo = (EObject)resource1.getContents().get(0);

									//Verify the type of the node
									if (eo instanceof ArtefactModel ) {																					
										EList<ArtefactModel> lstArtefactModel = aPackage.getArtefactsModel();
										Iterator<ArtefactModel> artefactModel = lstArtefactModel.iterator();
										while (artefactModel.hasNext()) {
											ArtefactModel artefactModel1 = artefactModel.next();
											if (CDOUtil.getCDOObject(eo).cdoID().equals(CDOUtil.getCDOObject(artefactModel1).cdoID())) {
												cbTarget.add(node1.getName());
												refListDirModel.add(node1.getPath());
											}
										}
									} else if  (eo instanceof Case) { 
										EList<Case> lstCase = aPackage.getArgumentationModel();
										Iterator<Case> caseModel = lstCase.iterator();
										while (caseModel.hasNext()) {
											Case case1 = caseModel.next();
											if (CDOUtil.getCDOObject(eo).cdoID().equals(CDOUtil.getCDOObject(case1).cdoID())) {
												cbTarget.add(node1.getName());
												refListDirModel.add(node1.getPath());
											}
										}
									} else if (eo instanceof ProcessModel) { 
										EList<ProcessModel> lstProcessModel = aPackage.getProcessModel();
										Iterator<ProcessModel> processModel = lstProcessModel.iterator();																									
										while (processModel.hasNext()) {
											ProcessModel processModel1 = processModel.next();
											if (CDOUtil.getCDOObject(eo).cdoID().equals(CDOUtil.getCDOObject(processModel1).cdoID())) {
												cbTarget.add(node1.getName());
												refListDirModel.add(node1.getPath());
											}
										}
									}											
								}
							}
						}
					}	
				}
			}
			if (cbTarget.getItemCount() > 0 ) {
				cbTarget.select(0);


				modelViewerTarget.setInput(null);
				CreateModelFromFile1(modelViewerTarget,refListDirModel.get(cbTarget.getSelectionIndex()));
				modelViewerTarget.expandAll();

				if (cbMapGroup.getSelectionIndex() != -1) { 
					sourceComposite.setEnabled(true);
				}

			} else {
				MessageDialog
				.openError(getShell(), "Target Model",
						"There aren`t model of the type selected defined in the project or the models are not part of the assets package.");
			}
		}
	}

	protected void createTargetDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {

		Composite targetComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			targetComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1;
			targetComposite.setLayout(layout);
		}

		// Label
		Label targetLabel = new Label(targetComposite, SWT.NONE);
		targetLabel.setText("To");
		GridData targetLabelGridData = new GridData();
		targetLabelGridData.horizontalSpan = 2;
		targetLabelGridData.horizontalAlignment = SWT.FILL;
		targetLabelGridData.verticalAlignment = SWT.FILL;
		targetLabel.setLayoutData(targetLabelGridData);
		
		// Combo
		cbTarget = new Combo(targetComposite, SWT.READ_ONLY);
		GridData cbTargetGridData = new GridData();
		cbTargetGridData.verticalAlignment = SWT.FILL;
		cbTargetGridData.horizontalAlignment = SWT.FILL;
		cbTargetGridData.widthHint = Display.getCurrent().getBounds().width / 5;
		cbTargetGridData.heightHint = Display.getCurrent().getBounds().height / 5;
		cbTargetGridData.grabExcessHorizontalSpace = true;
		cbTargetGridData.grabExcessVerticalSpace = true;
		cbTarget.setLayoutData(cbTargetGridData);
		
		
		String sSufijo = null;
		if (sElementFilter.contentEquals("Activity")) {
			sSufijo = PROCESS;
		} else if (sElementFilter.contentEquals("Artefact")) {
			sSufijo = EVIDENCE;
		} else if (sElementFilter.contentEquals("Requirement")) {
			sSufijo = ARG;
		} else if (sElementFilter.contentEquals("Role")) {
			sSufijo = PROCESS;
		} else {
			sSufijo = PROCESS;
		}
		
		cbTarget.removeAll();
		refListDirModel.clear();
		CDOResourceNode[] listR=  viewCDO.getElements();			
		for (int i=0; i<listR.length; i++){
			
				if (listR[i].getName().endsWith(sSufijo)){
					cbTarget.add(listR[i].getName());
					refListDirModel.add(listR[i].getPath());
				}		
		}
				
		cbTarget.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				
				CreateModelFromFile1(modelViewerTarget,
						refListDirModel.get(cbTarget.getSelectionIndex()));
				modelViewerTarget.expandAll();
				
				if (cbMapGroup.getSelectionIndex() != -1) { 
					sourceComposite.setEnabled(true);
				}
			}		
		});

		modelViewerTarget = new CheckboxTreeViewerExt(targetComposite,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);

		GridData modelViewerGridData1 = new GridData();
		modelViewerGridData1.verticalAlignment = SWT.FILL;
		modelViewerGridData1.horizontalAlignment = SWT.FILL;
		modelViewerGridData1.widthHint = Display.getCurrent().getBounds().width / 3;
		modelViewerGridData1.heightHint = Display.getCurrent().getBounds().height / 3;
		modelViewerGridData1.grabExcessHorizontalSpace = true;
		modelViewerGridData1.grabExcessVerticalSpace = true;
		modelViewerTarget.getTree().setLayoutData(modelViewerGridData1);
		modelViewerTarget.setContentProvider(new AdapterFactoryContentProvider(
				adapterFactory));
		modelViewerTarget.setLabelProvider(new AdapterFactoryLabelProvider(
				adapterFactory));

		options = new HashMap<Object, Object>();
		options.put(XMLResource.OPTION_ENCODING, UTF_8);

		ICheckStateListener checkStateListener = new ICheckStateListener() {
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				Object changedElement = event.getElement();
				boolean status = modelViewerTarget.getChecked(changedElement);

				
				
				// Para descartar la selecci�n del framework comprobamos si su container es null
				if ((status) && ((changedElement instanceof BaseFramework) || (changedElement instanceof ArtefactModel) || (changedElement instanceof Case) || (changedElement instanceof ProcessModel) )) {
					modelViewerTarget.setChecked(changedElement, false);
					MessageDialog.openError(getShell(), "Compliance Map",
							"It's not possible to do a compliance map against a root node. Select another node. ");
					return;
				}
				
				// En el modelo de evidencias tampoco se puede realizar compliance contra artefact definition.
				if ((status) && (changedElement instanceof ArtefactDefinition)) {
					modelViewerTarget.setChecked(changedElement, false);
					MessageDialog.openError(getShell(), "Compliance Map",
							"It's not possible to do a compliance map against an Artefact Definition. Select an Artefact node. ");
					return;
				}
				
				
				//Before check the selection of a mapping model, a map group, an equivalence map and a source
				
				if ((qref == "") || (cbMapGroup.getSelectionIndex() == -1) ||  (cbComplianceMap.getSelectionIndex() == -1))   { 

					/*
					System.out.println("Modelo de mapping " + qref);
					System.out.println("MapGroup " + cbMapGroup.getSelectionIndex());
					System.out.println("EquivalenceMap " + cbEquivalenceMap.getSelectionIndex());
					*/

					MessageDialog.openError(getShell(), "Compliance Map",
							"Check that you have selected: a map group, a compliance map and a node from the source framework tree.");
					
					if (status) {
						modelViewerTarget.setChecked(changedElement, false);
					} else {
						modelViewerTarget.setChecked(changedElement, true);
					}		
				} else {

					try {
						if (status) {

							targetSettings.addToReference((EObject) changedElement);
							boolean pp = PlatformUI.getWorkbench().saveAllEditors(true);

							
							if (pp) {

								targetSettings.getSource().eResource()
										.save(options);
								transaction.commit();
							}
						} else {
							boolean pp = PlatformUI.getWorkbench().saveAllEditors(
									true);
							if (pp)
								removeFromReferenceOpencert((EObject) changedElement);
							targetSettings.getSource().eResource().save(options);
						}

					} catch (IOException e) {
						e.printStackTrace();
					} catch (ConcurrentAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (CommitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		};

		modelViewerTarget.addCheckStateListener(checkStateListener);

	}

	@SuppressWarnings("unchecked")
	public void removeFromReferenceOpencert(EObject valueToRemove) {

		EReference ref = targetSettings.getLastReference();
		Object value1 = targetSettings.getSource().eGet(ref);

		int index = -1;
		int i = 0;

		for (Iterator<EObject> itr = ((List<EObject>) value1).iterator(); itr.hasNext();) {
			Object element = itr.next();
			i++;
			if (valueToRemove instanceof Activity) {
				Activity ElemttoRemove = (Activity) valueToRemove;
				Activity refElem = (Activity) element;

				if (CDOUtil.getCDOObject(ElemttoRemove).cdoID().equals(CDOUtil.getCDOObject(refElem).cdoID())) {
					index = i;
					
				}
			}
			if (valueToRemove instanceof Artefact) {
				Artefact ElemttoRemove = (Artefact) valueToRemove;
				Artefact refElem = (Artefact) element;

				if (CDOUtil.getCDOObject(ElemttoRemove).cdoID().equals(CDOUtil.getCDOObject(refElem).cdoID())) {
					index = i;
				}
			}
			if (valueToRemove instanceof Claim) {
				Claim ElemttoRemove = (Claim) valueToRemove;
				Claim refElem = (Claim) element;

				if (CDOUtil.getCDOObject(ElemttoRemove).cdoID().equals(CDOUtil.getCDOObject(refElem).cdoID())) {
					index = i;
				}
			}
			
			if ((valueToRemove instanceof Person) && ((element instanceof Person)))  {
				Person ElemttoRemove = (Person) valueToRemove;
				Person refElem = (Person) element;

				if (CDOUtil.getCDOObject(ElemttoRemove).cdoID().equals(CDOUtil.getCDOObject(refElem).cdoID())) {
					index = i;
				}
			}
			if ((valueToRemove instanceof Tool) && ((element instanceof Tool))) {
				Tool ElemttoRemove = (Tool) valueToRemove;
				Tool refElem = (Tool) element;

				if (CDOUtil.getCDOObject(ElemttoRemove).cdoID().equals(CDOUtil.getCDOObject(refElem).cdoID())) {
					index = i;
				}
			}
			if (valueToRemove instanceof Participant) {
				Participant ElemttoRemove = (Participant) valueToRemove;
				Participant refElem = (Participant) element;

				if (CDOUtil.getCDOObject(ElemttoRemove).cdoID().equals(CDOUtil.getCDOObject(refElem).cdoID())) {
					index = i;
				}
			}
			
			if (valueToRemove instanceof Technique) {
				Technique ElemttoRemove = (Technique) valueToRemove;
				Technique refElem = (Technique) element;

				if (CDOUtil.getCDOObject(ElemttoRemove).cdoID().equals(CDOUtil.getCDOObject(refElem).cdoID())) {
					index = i;
				}
			}
			
			
		}
		if (index != -1) {
			index--;
			((List<EObject>) value1).remove(index);
		}
	}


	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Selection dialog");
	}

	public void CreateModelFromFiletocombo(Combo combo,CDOResource resource) {
		/*lstMapGroup = mapModel.getMapGroupModel();
		Iterator<MapGroup> itermapGroup = lstMapGroup.iterator();
		while (itermapGroup.hasNext()) {
			// MapModel puede contener varios MapGroup, pero solo deber�
			// visualizar el correspondiente al baseline activo relacionado
			// a su vez con el baseline.
			MapGroup mGroup = itermapGroup.next();
			combo.add(mGroup.getName());
		} */
		
		
		if (resource != null) {	
			
			try {
												
				//resource = transaction.getResource(spath);
				
				mapModel = null;
				mapModel = (MapModel) resource.getContents().get(0);
				
				combo.removeAll();
				lstMapGroup = mapModel.getMapGroupModel();

				Iterator<MapGroup> itermapGroup = lstMapGroup.iterator();
				while (itermapGroup.hasNext()) {
					combo.add(itermapGroup.next().getName());
				}
			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
			}
		}
	}
	
	
	public void CreateModelFromFiletocomboObject(Combo combo, Object object) {

		/*lstComplianceMap.removeAll(lstComplianceMap);
		cbComplianceMap.removeAll();
		lstComplianceMap = ((BaseAssurableElement)object).getComplianceMap();
		
		Iterator<BaseComplianceMap> itercomplianceMap = lstComplianceMap.iterator();
		while (itercomplianceMap.hasNext()) {
			BaseComplianceMap compMap = itercomplianceMap.next();
			combo.add(compMap.getName());	
		}
		*/
		
		if(lstComplianceMap!=null){
			lstComplianceMap.removeAll(lstComplianceMap);
		}else{
			lstComplianceMap = (EList<BaseComplianceMap>) new BasicEList<BaseComplianceMap>();
			bnewEquivalenceMap = true;			
		}
		cbComplianceMap.removeAll();
		
		
		Iterator<BaseComplianceMap> iter1 = ((BaseAssurableElement)object).getComplianceMap().iterator();
		while (iter1.hasNext()) {	
			
			MapGroup mapGroupFrom = null;									
			final BaseComplianceMap baseComplMapFrom = (BaseComplianceMap) iter1.next();

			if (baseComplMapFrom.getMapGroup() != null){
				mapGroupFrom = baseComplMapFrom.getMapGroup();
			} else {
				mapGroupFrom = null;
			}
			if (CDOUtil.getCDOObject(mapGroupFrom).cdoID().equals(CDOUtil.getCDOObject(mapGroup).cdoID())){
				bEqualMapGroup = true;
				
				//A�ado el objeto complianceMap a la combo
				//Cunado cree uno nuevo debere a�adirlo a la lista
				lstComplianceMap.add(baseComplMapFrom);
				cbComplianceMap.add(baseComplMapFrom.getName());
				mapGroupFrom = null;
			} 
			
		}
		
	}
	

	
	public void CreateModelFromFile(TreeViewer tviewer, CDOResource resource) {
		if (resourceURI != null) {

			try {
				tviewer.setInput(null);
				tviewer.setInput(resource);
				final ViewerFilter modelFilter = new ViewerFilter() {
					public boolean select(Viewer viewer, Object parentElement,
							Object element) {
						if (element instanceof BaseFramework) {
							baseFramework = (BaseFramework) element;
							return true;
						}
						
						if (element instanceof BaseComplianceMap) {							
							return true;
						}
						
						if (element instanceof BaseActivity) {
							
							if (sElementFilter.contentEquals("Requirement")) {
								
								BaseActivity baseActivity = (BaseActivity) element;
								//Verificar si esta actividad o alguna de sus hijas tiene requisitos, y si es as� devolver true
								return bVerifyReActivity(baseActivity);
								
							} else {
								if (sElementFilter.contentEquals("Activity"))
									return true;
								else
									return false;
							}
							
						}
						
						
						//if (element instanceof BaseActivity)
						//	if (sElementFilter.contentEquals("Activity"))
						//		return true;
						//	else
						//		return false;
						else if (element instanceof BaseArtefact)
							if (sElementFilter.contentEquals("Artefact"))
								return true;
							else
								return false;
						else if (element instanceof BaseRole)
							if (sElementFilter.contentEquals("Role"))
								return true;
							else
								return false;
						else if (element instanceof BaseRequirement)
							if (sElementFilter.contentEquals("Requirement"))
								return true;
							else
								return false;
						else if (element instanceof BaseTechnique)
							if (sElementFilter.contentEquals("Technique"))
								return true;
							else
								return false;
						else
							return false;
					}
				};
				tviewer.addFilter(modelFilter);

			} catch (Exception ex) {
				MessageDialog
						.openError(getShell(), "Not valid model file",
								"The provided model file couldn't be parsed as an EMF resource");
				tviewer.setInput(null);
			}
		}
	}
	
	
	
	private boolean bVerifyReActivity(BaseActivity activity) {	
		boolean bResultado = false;
		
		// Verify if requirement
		if (activity.getOwnedRequirement().size() > 0) {
			return true;
		} else {
			// Verify it childs
			if (activity.getSubActivity().size() > 0) {
				EList<BaseActivity> lstRefActivity = activity.getSubActivity();
				Iterator<BaseActivity> iter1 = lstRefActivity.iterator();
				while (iter1.hasNext() && !bResultado) {
					BaseActivity baseActivity = (BaseActivity) iter1.next();
					bResultado = bVerifyReActivity(baseActivity);
				}
			}
		}
		return bResultado;
	}
	

	/**
	 * CreateModelFromFile: load the resource of the model
	 * 
	 * @param tviewer
	 *            widget in which the model will be visualized
	 * @param model
	 *            string with the path and name of the model in the file system
	 */
	public void CreateModelFromFile1(TreeViewer tviewer, String sModel) {

		lstrefActivity = new BasicEList<Activity>();
		lstrefArtefact = new BasicEList<Artefact>();
		lstrefRequirement = new BasicEList<Claim>();
		lstrefRole = new BasicEList<Participant>();
		lstrefTechnique = new BasicEList<Technique>();

		if (sModel != null) {

			Resource resource = null;
			try {
				resource = viewCDO.getResource(sModel, true);
				tviewer.setInput(null);
				tviewer.setInput(resource);
				final ViewerFilter modelFilter = new ViewerFilter() {
					public boolean select(Viewer viewer, Object parentElement,
							Object element) {
						
						if (sElementFilter.contentEquals("Activity") || sElementFilter.contentEquals("Role") || sElementFilter.contentEquals("Technique")) {
							if (element instanceof ProcessModel)
								return true;
							if (element instanceof Activity)
								if (sElementFilter.contentEquals("Activity")) {
									lstrefActivity.add((Activity) element);
									return true;
								} else
									return false;
							else if (element instanceof Participant)
								if (sElementFilter.contentEquals("Role")) {
									lstrefRole.add((Participant) element);
									return true;
								} else
									return false;
							else if (element instanceof Technique)
								if (sElementFilter.contentEquals("Technique")) {
									lstrefTechnique.add((Technique) element);
									return true;
								} else
									return false;
							else
								return false;
							
						} else if (sElementFilter.contentEquals("Artefact")) {
							if (element instanceof ArtefactModel)
								return true;
							if (element instanceof ArtefactDefinition){
								//lstrefArtefact.add((ArtefactDefinition) element);
								return true;
							} else if (element instanceof Artefact){
								lstrefArtefact.add((Artefact) element);
								return true;
							} else
								return false;
							
							
						} else if (sElementFilter.contentEquals("Requirement")) {
							if (element instanceof Case)
								return true;
							if (element instanceof Claim) {
								lstrefRequirement.add((Claim) element);
								return true;
							} 
							
							///New
							if (element instanceof ArtefactModel)
								return true;
							if (element instanceof ArtefactDefinition){
								return true;
							} 
							if (element instanceof Artefact){
								lstrefArtefact.add((Artefact) element);
								return true;
							} 
							
							if (element instanceof ProcessModel)
								return true;
							if (element instanceof Activity){
									lstrefActivity.add((Activity) element);
									return true;
							} else 
								return false;
							
							// End new
							
						} else {
							return false;
						}
					}
				
				};
				tviewer.addFilter(modelFilter);

			} catch (Exception ex) {
				MessageDialog
						.openError(getShell(), "Not valid model file",
								"The provided model file couldn't be parsed as an EMF resource");
				tviewer.setInput(null);
			}
		}
	}

	protected void addEquivalenceMap(MapGroup mapGroup, Object object)
			throws ConcurrentAccessException, CommitException {

		// Comprobrar que se haya seleccionado un elemento source

		if (object != null) {
			
			BaseComplianceMap baseComplianceMap = BaselineFactory.eINSTANCE.createBaseComplianceMap();
			
			//RefEquivalenceMap refEquivalenceMap = RefframeworkFactory.eINSTANCE.createRefEquivalenceMap();
			baseComplianceMap.setMapGroup(mapGroup);

			if (idText.getText() != null)
				baseComplianceMap.setId(idText.getText());
			if (nameText.getText() != null)
				baseComplianceMap.setName(nameText.getText());
			if (txtJustification.getText() != null) {
				mapJustification = MappingFactory.eINSTANCE
						.createMapJustification();
				mapJustification.setExplanation(txtJustification.getText());
				baseComplianceMap.setMapJustification(mapJustification);
			}
			if (cbType.getSelectionIndex() != -1) {
				String value = cbType.getItem(cbType.getSelectionIndex());
				baseComplianceMap.setType(MapKind.get(value));
			}

			targetSettings = new ReferencesTableSettings(baseComplianceMap,BaselinePackage.eINSTANCE.getBaseComplianceMap_Target());

			ReferencesTableSettings objectComplianceMapSettings;

			// Object is the assurableElement selected from the source Tree
			if (sElementFilter.contentEquals("Activity")) {
				Activity refElemtFrom = (Activity) object;
				objectComplianceMapSettings = new ReferencesTableSettings(
						refElemtFrom,
						BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap());
				refElemtFrom = null;
			} else if (sElementFilter.contentEquals("Artefact")) {
				Artefact refElemtFrom = (Artefact) object;
				objectComplianceMapSettings = new ReferencesTableSettings(
						refElemtFrom,
						BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap());
				refElemtFrom = null;
			} else if (sElementFilter.contentEquals("Requirement")) {
				Claim refElemtFrom = (Claim) object;
				objectComplianceMapSettings = new ReferencesTableSettings(
						refElemtFrom,
						BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap());
				refElemtFrom = null;
			} else if (sElementFilter.contentEquals("Role")) {
				Participant refElemtFrom = (Participant) object;
				objectComplianceMapSettings = new ReferencesTableSettings(
						refElemtFrom,
						BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap());
				refElemtFrom = null;
			} else {
				Technique refElemtFrom = (Technique) object;
				objectComplianceMapSettings = new ReferencesTableSettings(
						refElemtFrom,
						BaselinePackage.eINSTANCE.getBaseAssurableElement_ComplianceMap());
				refElemtFrom = null;
			}

			if (objectComplianceMapSettings.contains(baseComplianceMap)) {

			} else {
				objectComplianceMapSettings.addToReference(baseComplianceMap);
			}

			try {
				objectComplianceMapSettings.getSource().eResource()
						.save(options);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			MessageDialog.openError(getShell(), "Not selected element",
					"It's neccesary to select a source element");
		}
	}

	@Override
	protected void okPressed() {
		super.okPressed();
	}

	@Override
	public boolean close() {
		if (!transaction.isClosed()) transaction.close();
		return super.close();
	}

	@Override
	protected Button createButton(Composite parent, int id,
	        String label, boolean defaultButton) {
	    if (id == IDialogConstants.CANCEL_ID) return null;
	    return super.createButton(parent, id, label, defaultButton);
	}
	
	public Boolean getResult() {
		return true;
	}

	public ResourceLocator getResourceLocator() {
		return RefframeworkEditPlugin.INSTANCE;
	}

}
