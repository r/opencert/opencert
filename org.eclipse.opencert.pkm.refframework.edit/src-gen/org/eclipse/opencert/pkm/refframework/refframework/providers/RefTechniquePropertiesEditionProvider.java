/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.providers;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.providers.impl.PropertiesEditingProviderImpl;

import org.eclipse.jface.viewers.IFilter;

import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

import org.eclipse.opencert.pkm.refframework.refframework.components.RefTechniqueBasePropertiesEditionComponent;
import org.eclipse.opencert.pkm.refframework.refframework.components.RefTechniquePropertiesEditionComponent;
import org.eclipse.opencert.pkm.refframework.refframework.components.RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent;

/**
 * 
 * 
 */
public class RefTechniquePropertiesEditionProvider extends PropertiesEditingProviderImpl {

	/**
	 * Constructor without provider for super types.
	 */
	public RefTechniquePropertiesEditionProvider() {
		super();
	}

	/**
	 * Constructor with providers for super types.
	 * @param superProviders providers to use for super types.
	 */
	public RefTechniquePropertiesEditionProvider(List<PropertiesEditingProvider> superProviders) {
		super(superProviders);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext) {
		return (editingContext.getEObject() instanceof RefTechnique) 
					&& (RefframeworkPackage.Literals.REF_TECHNIQUE == editingContext.getEObject().eClass());
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext, String part) {
		return (editingContext.getEObject() instanceof RefTechnique) && (RefTechniqueBasePropertiesEditionComponent.BASE_PART.equals(part) || RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.TECHNIQUEEQUIVALENCEMAP_PART.equals(part));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof RefTechnique) && (refinement == RefTechniqueBasePropertiesEditionComponent.class || refinement == RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.class);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, String part, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof RefTechnique) && ((RefTechniqueBasePropertiesEditionComponent.BASE_PART.equals(part) && refinement == RefTechniqueBasePropertiesEditionComponent.class) || (RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.TECHNIQUEEQUIVALENCEMAP_PART.equals(part) && refinement == RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.class));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode) {
		if (editingContext.getEObject() instanceof RefTechnique) {
			return new RefTechniquePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part) {
		if (editingContext.getEObject() instanceof RefTechnique) {
			if (RefTechniqueBasePropertiesEditionComponent.BASE_PART.equals(part))
				return new RefTechniqueBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.TECHNIQUEEQUIVALENCEMAP_PART.equals(part))
				return new RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String, java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part, java.lang.Class refinement) {
		if (editingContext.getEObject() instanceof RefTechnique) {
			if (RefTechniqueBasePropertiesEditionComponent.BASE_PART.equals(part)
				&& refinement == RefTechniqueBasePropertiesEditionComponent.class)
				return new RefTechniqueBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.TECHNIQUEEQUIVALENCEMAP_PART.equals(part)
				&& refinement == RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.class)
				return new RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part, refinement);
	}

	/**
	 * Provides the filter used by the plugin.xml to assign part forms.
	 */
	public static class EditionFilter implements IFilter {
		
		/**
		 * {@inheritDoc}
		 * 
		 * @see org.eclipse.jface.viewers.IFilter#select(java.lang.Object)
		 */
		public boolean select(Object toTest) {
			EObject eObj = EEFUtils.resolveSemanticObject(toTest);
			return eObj != null && RefframeworkPackage.Literals.REF_TECHNIQUE == eObj.eClass();
		}
		
	}

}
