/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package claimtypes.impl;

import claimtypes.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClaimtypesFactoryImpl extends EFactoryImpl implements ClaimtypesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ClaimtypesFactory init() {
		try {
			ClaimtypesFactory theClaimtypesFactory = (ClaimtypesFactory)EPackage.Registry.INSTANCE.getEFactory(ClaimtypesPackage.eNS_URI);
			if (theClaimtypesFactory != null) {
				return theClaimtypesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ClaimtypesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClaimtypesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ClaimtypesPackage.CLAIM_TYPE: return (EObject)createClaimType();
			case ClaimtypesPackage.CLAIM_TYPE_REPOSITORY: return (EObject)createClaimTypeRepository();
			case ClaimtypesPackage.SENTENCE_STRUCTURE: return (EObject)createSentenceStructure();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClaimType createClaimType() {
		ClaimTypeImpl claimType = new ClaimTypeImpl();
		return claimType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClaimTypeRepository createClaimTypeRepository() {
		ClaimTypeRepositoryImpl claimTypeRepository = new ClaimTypeRepositoryImpl();
		return claimTypeRepository;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SentenceStructure createSentenceStructure() {
		SentenceStructureImpl sentenceStructure = new SentenceStructureImpl();
		return sentenceStructure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClaimtypesPackage getClaimtypesPackage() {
		return (ClaimtypesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ClaimtypesPackage getPackage() {
		return ClaimtypesPackage.eINSTANCE;
	}

} //ClaimtypesFactoryImpl
