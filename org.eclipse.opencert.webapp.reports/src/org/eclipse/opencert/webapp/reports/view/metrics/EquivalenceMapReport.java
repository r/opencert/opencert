/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;

import org.eclipse.opencert.webapp.reports.export.docx.IDocxGenerator;
import org.eclipse.opencert.webapp.reports.export.docx.MetricsEstimationReportDocxGenerator;
import org.eclipse.opencert.webapp.reports.view.common.AbstractEquivalenceFrameworkReport;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.common.IReport;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;
import org.eclipse.opencert.webapp.reports.view.export.ExportToDocxPanel;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@Theme("opencerttheme")
public class EquivalenceMapReport extends AbstractEquivalenceFrameworkReport implements IReport
{

	private EquivalenceChartsMetrics equivalenceChartsMetrics;
	private final ExportToDocxPanel _exportTopDocxPanel;
    private static final String EQUIVALENCE_MAP_TITLE = "Equivalence Map Metrics";
    private static final String EQUIVALENCE_MAP_GOAL = "Goal: Maintenance of Mappings";
    private static final String EQUIVALENCE_MAP_TIP = "Tip: This metric is used to represent the status of each type of equivalence mappings.";
	
	public EquivalenceMapReport()
    {
		
		final String basePath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
		final IDocxGenerator docxGenerator = new MetricsEstimationReportDocxGenerator(basePath);
		_exportTopDocxPanel = new ExportToDocxPanel(docxGenerator);
		addFromRefframeworkChangeListener(_exportTopDocxPanel);
		addToRefframeworkChangeListener(_exportTopDocxPanel);
    }
	
    @Override
    public String getTitle() 
    {
        return "Equivalence Map report";
    }

    @Override
    public Component getMainComponent(GUIMode guiMode) 
    {	 
    	VerticalLayout mainComponent = new VerticalLayout();
    	 
    	 
    	Label equivalenceMapTitle = new Label(EQUIVALENCE_MAP_TITLE);
    	equivalenceMapTitle.addStyleName("metricsMainTitle");
		mainComponent.addComponent(equivalenceMapTitle);
		
		Label equivalenceMapGoal = new Label(EQUIVALENCE_MAP_GOAL);
		equivalenceMapGoal.addStyleName("metricsGoalTitle");
		mainComponent.addComponent(equivalenceMapGoal);
		
		Label equivalenceMapTip = new Label(EQUIVALENCE_MAP_TIP);
		equivalenceMapTip.addStyleName("metricsTipTitle");
		mainComponent.addComponent(equivalenceMapTip);
		
		equivalenceChartsMetrics = new EquivalenceChartsMetrics();
		addFromRefframeworkChangeListener(equivalenceChartsMetrics);
		addToRefframeworkChangeListener(equivalenceChartsMetrics);
		super.wakeUp();
        
		mainComponent.addComponent(equivalenceChartsMetrics);
		
        return mainComponent;
        
    }


	@Override
    public ReportID getReportID() 
    {
        return ReportID.EQUIVALENCE_MAP;
    }

	@Override
	public Component getExportToDocxComponent() 
	{
		 HorizontalLayout panel = new HorizontalLayout();
	     panel.addComponent(_exportTopDocxPanel.getButton());
	        
	     return panel;
	}


	
	
	
}