/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.administration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.opencert.webapp.reports.view.administration.ProjectBaselineArtefactTable.RowTempIDGenerator;

import com.vaadin.ui.Table;

public class RelationCellsController
{
    private final Table parentTable;

    public RelationCellsController(Table table)
    {
        this.parentTable = table;
    }

    public void relationsCellEdit(boolean setEditable, final BaselineElementDataRow editedRow)
    {
        Long editedRowCdoID = editedRow.getCdoID();
        if (RowTempIDGenerator.isTempID(editedRowCdoID)) {
            return;
        }
        
        final List<Long> relationIDs = editedRow.getRelationsField().getRelationIDs();
        final List<Long> editedRelations = editedRow.getEditedRelations();
        List<Long> relationIDsOfTheEditedRow = new ArrayList<>(); 
        relationIDsOfTheEditedRow.addAll(relationIDs);
        relationIDsOfTheEditedRow.addAll(editedRelations);
        
        final List<Long> relationIDsUncheckedNotSavedYet = relationIDs.stream().filter(e -> !editedRelations.contains(e)).collect(Collectors.toList());
        
        Collection<?> dataRows = parentTable.getItemIds();
        
        for (Object item : dataRows)
        {
            BaselineElementDataRow row = (BaselineElementDataRow)item;
            final BaseArtefactRelationsField relationsField = row.getRelationsField();
            
            // show/hide the relations checkbox
            final Long processedRowID = row.getCdoID();
            final boolean isTheEditedRow = (processedRowID != null && processedRowID.equals(editedRowCdoID));
            if (isTheEditedRow) 
            {
                relationsField.setCheckboxVisible(false);
            } else {
                relationsField.setCheckboxVisible(true);
                relationsField.setRelationChangedListener(editedRow);
            }
            
            // 1. mark the check-boxes which are related to the currently edited one
            if (relationIDsOfTheEditedRow.contains(processedRowID)) {
                relationsField.setCheckboxMarked(true, String.format("This base artefact (%d) depends on the currently edited artefact ID: %d ", processedRowID, editedRowCdoID));
            } else {
                relationsField.setCheckboxMarked(false, String.format("This base artefact (%d) does not depend on the currenty edited artefact ID: %d ", processedRowID, editedRowCdoID));
            }
            // 2. and then unmark the ones which had been unmarked
            if (relationIDsUncheckedNotSavedYet.contains(processedRowID)) {
                relationsField.setCheckboxMarked(false, String.format("This base artefact (%d) does not depend on the currenty edited artefact ID: %d ", processedRowID, editedRowCdoID));
            }
        }
    }
    
    public void relationsCellUnedit()
    {
        Collection<?> dataRows = parentTable.getItemIds();
        for (Object item : dataRows)
        {
            BaselineElementDataRow row = (BaselineElementDataRow)item;
            final BaseArtefactRelationsField relationsField = row.getRelationsField();
            relationsField.setCheckboxVisible(false);
            relationsField.setRelationChangedListener(null);
        }
    }
}