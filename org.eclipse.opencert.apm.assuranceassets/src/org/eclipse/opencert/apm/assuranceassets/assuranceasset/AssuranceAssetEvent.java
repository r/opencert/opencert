/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset;

import java.util.Date;

import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assurance Asset Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getResultingEvaluation <em>Resulting Evaluation</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getTime <em>Time</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getAssuranceAssetEvent()
 * @model
 * @generated
 */
public interface AssuranceAssetEvent extends AssuranceAsset, DescribableElement {
	/**
	 * Returns the value of the '<em><b>Resulting Evaluation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resulting Evaluation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resulting Evaluation</em>' reference.
	 * @see #setResultingEvaluation(AssuranceAssetEvaluation)
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getAssuranceAssetEvent_ResultingEvaluation()
	 * @model
	 * @generated
	 */
	AssuranceAssetEvaluation getResultingEvaluation();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getResultingEvaluation <em>Resulting Evaluation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resulting Evaluation</em>' reference.
	 * @see #getResultingEvaluation()
	 * @generated
	 */
	void setResultingEvaluation(AssuranceAssetEvaluation value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind
	 * @see #setType(EventKind)
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getAssuranceAssetEvent_Type()
	 * @model
	 * @generated
	 */
	EventKind getType();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind
	 * @see #getType()
	 * @generated
	 */
	void setType(EventKind value);

	/**
	 * Returns the value of the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time</em>' attribute.
	 * @see #setTime(Date)
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getAssuranceAssetEvent_Time()
	 * @model
	 * @generated
	 */
	Date getTime();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getTime <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time</em>' attribute.
	 * @see #getTime()
	 * @generated
	 */
	void setTime(Date value);

} // AssuranceAssetEvent
